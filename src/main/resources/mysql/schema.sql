CREATE TABLE IF NOT EXISTS `trigger_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `frequency` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'inactive',
  `last_processed_date` date DEFAULT NULL,
  `last_processed_time` time DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `trigger_id` int(11) DEFAULT NULL,
  `trigger_config` json DEFAULT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `channel` varchar(100) DEFAULT NULL,
  `message` text,
  `subject`  varchar(500) DEFAULT NULL,
  `contacts` json DEFAULT NULL,
  `add_feedback` int(1) DEFAULT NULL,
  `schedule_uuid` varchar(100) DEFAULT NULL,
  `host` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--CREATE TABLE IF NOT EXISTS `trigger_notification` (
--  `id` int(11) NOT NULL,
--  `start_time` timestamp NULL DEFAULT NULL,
--  `channels` json DEFAULT NULL,
--  `message` text,
--  `status` int(1) DEFAULT NULL,
--  `contacts` json DEFAULT NULL,
--  `add_feedback` int(11) DEFAULT NULL,
--  `schedule_id` int(11) DEFAULT NULL,
--  `created_time` timestamp NULL DEFAULT NULL,
--  `modified_time` timestamp NULL DEFAULT NULL,
--  PRIMARY KEY (`id`)
--) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



CREATE TABLE IF NOT EXISTS `trigger_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(100) DEFAULT NULL,
  `message` text,
  `start_time` timestamp NULL DEFAULT NULL,
  `subject`  varchar(500) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `contacts` json DEFAULT NULL,
  `add_feedback` int(1) DEFAULT NULL,
  `condition_query` text NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `notification_uuid` varchar(100) DEFAULT NULL,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `modified_time` timestamp NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `host` VARCHAR(100) NULL DEFAULT NULL AFTER `schedule_uuid`;

ALTER TABLE `dt`.`trigger_5bfb7b97_4b55_479e_90ce_d606904bda4d`
ADD COLUMN `readable_query` TEXT NULL AFTER `trigger_uuid`;


ALTER TABLE `dt`.`trigger_notification`
ADD COLUMN `submission_token` VARCHAR(45) NULL DEFAULT NULL AFTER `condition_query`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `aggregate_status` INT(1) NULL DEFAULT NULL AFTER `host`,
CHANGE COLUMN `add_feedback` `add_feedback` INT(1) NULL DEFAULT NULL ;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `exec_start_time` VARCHAR(24) NULL DEFAULT NULL AFTER `host`,
ADD COLUMN `exec_end_time` VARCHAR(24) NULL DEFAULT NULL AFTER `exec_start_time`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `sent_status` VARCHAR(45) NULL DEFAULT NULL AFTER `aggregate_status`;
ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `created_time` TIMESTAMP NULL DEFAULT current_timestamp AFTER `sent_status`;
ALTER TABLE `dt`.`trigger_notification`
ADD COLUMN `sent_status` VARCHAR(45) NULL DEFAULT NULL AFTER `submission_token`;

ALTER TABLE `dt`.`trigger_notification`
ADD COLUMN `deactivated_contacts` json DEFAULT NULL AFTER `modified_time`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `deactivated_contacts` json DEFAULT NULL AFTER `created_time`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `participant_group_id` INT NULL AFTER `deactivated_contacts`,
ADD COLUMN `header_data` JSON NULL AFTER `participant_group_id`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `webhook_config` JSON NULL AFTER `header_data`;

ALTER TABLE `dt`.`trigger_schedule`
ADD COLUMN `reply_respondent` VARCHAR(400) NULL AFTER `webhook_config`;


CREATE TABLE IF NOT EXISTS trigger_templates (
`id` int(11) NOT NULL AUTO_INCREMENT,
`trigger_uuid` varchar(100) NULL DEFAULT NULL,
`trigger_name` varchar(300) NULL DEFAULT NULL,
`trigger_condition` text,
`deactivated_conditions` json DEFAULT NULL,
`readable_query` text,
`status` int(1) DEFAULT '0',
`template_name` varchar(300) DEFAULT NULL,
`template_id` varchar(100) DEFAULT NULL,
`business_id` int(11) NOT NULL,
`frequency` varchar(100) DEFAULT NULL,
`type` varchar(100) DEFAULT NULL,
`created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`created_by` int(11) DEFAULT NULL,
`modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
`modified_by` int(11) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `trigger_template_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `frequency` varchar(100) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'inactive',
  `business_id` int(11) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `trigger_id` int(11) DEFAULT NULL,
  `trigger_config` json DEFAULT NULL,
  `timezone` varchar(100) DEFAULT NULL,
  `channel` varchar(100) DEFAULT NULL,
  `message` text,
  `subject`  varchar(500) DEFAULT NULL,
  `contacts` json DEFAULT NULL,
  `add_feedback` int(1) DEFAULT NULL,
  `header_data` JSON DEFAULT NULL,
  `webhook_config` json DEFAULT NULL,
  `schedule_uuid` varchar(100) DEFAULT NULL,
  `host` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

ALTER TABLE `DT`.`trigger_notification`
ADD COLUMN `trigger_status` ENUM('0', '1')  DEFAULT '1' AFTER `deactivated_contacts`;

ALTER TABLE `dt`.`trigger_template_schedule`
ADD COLUMN `reply_respondent` VARCHAR(400) NULL AFTER `host`;