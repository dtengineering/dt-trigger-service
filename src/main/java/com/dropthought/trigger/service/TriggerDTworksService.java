package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.model.DTWorksRecordResponseBean;
import com.dropthought.trigger.model.TriggerNotification;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class TriggerDTworksService {

    @Autowired
    TriggerDAO triggerDAO;
    @Autowired
    DTWorksService dtWorksService;
    @Autowired
    TriggerService triggerService;
    @Autowired
    RestTemplate restTemplate;

    @Value("${dt.event.url}")
    private String eventUrl;
    @Value("${dtworks.multitenant}")
    private boolean isMultiTenant;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * method to send trigger notification to dtworks
     * @param triggerNotification
     */
    public void sendTriggerDTWorksWebhook(TriggerNotification triggerNotification, boolean resend) {
        try {
            logger.info("trigger dtworks webhook");
            int scheduleId = Integer.parseInt(triggerNotification.getScheduleId());
            String channel = triggerNotification.getChannel();

            int businessId = triggerNotification.getBusinessId();
            int surveyId = triggerNotification.getSurveyId();

            Map businessMap = triggerDAO.getBusinessDetailsById(businessId);
            String businessUniqueId = businessMap.containsKey("business_uuid") ? (String) businessMap.get("business_uuid") : "";
            boolean isHippaEnabled = triggerDAO.isHippaEnableForBusiness(businessId);

            //surveyUUID
            Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
            String surveyUUID = surveyMap.containsKey("survey_uuid") ? surveyMap.get("survey_uuid").toString() : "";

            //get schedule webhook_config
            Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
            Map integrationsConfigMap = scheduleMap.containsKey("integrations_config") && Utils.isNotNull(scheduleMap.get("integrations_config")) ? mapper.readValue(scheduleMap.get("integrations_config").toString(), HashMap.class) : new HashMap();
            Map dtworksAction = integrationsConfigMap.containsKey("dtworks") ? (Map) integrationsConfigMap.get("dtworks") : new HashMap();
            String actionStr = dtworksAction.containsKey("recipeType") ? dtworksAction.get("recipeType").toString() : "";
            integrationsConfigMap.put("businessUniqueId", businessUniqueId);
            integrationsConfigMap.put("surveyUUID", surveyUUID);
            integrationsConfigMap.put("isHippaEnabled", isHippaEnabled);

            if (channel.equalsIgnoreCase(Constants.CHANNEL_DTWORKS)) {
                if (actionStr.equalsIgnoreCase("createTicket")) {
                    logger.info("create Interaction");
                    this.createDTWorksTicket(triggerNotification, integrationsConfigMap, resend);
                } else if (actionStr.equalsIgnoreCase("recordResponse")) {
                    logger.info("Record response in dtworks");
                    this.createResponseInDTworks(triggerNotification, integrationsConfigMap);
                }
            }else{
                logger.info("channel is not dtworks");
            }
        } catch (Exception e) {
            logger.error("Exception at sendTriggerDTWorksWebhook. Msg {}", e.getMessage());
        }
    }

    /**
     * method to create tickets (interaction) in dtworks
     * @param triggerNotification
     * @param integrationsConfigMap
     */
    private void createDTWorksTicket(TriggerNotification triggerNotification, Map integrationsConfigMap, boolean resend){
        try {
            int notifyId = triggerNotification.getNotifyId();
            String token = triggerNotification.getSubmissionToken();
            String channel = triggerNotification.getChannel();
            boolean addFeedback = triggerNotification.isAddFeedback();
            int triggerId = triggerNotification.getTriggerId();

            String businessUniqueId = integrationsConfigMap.containsKey("businessUniqueId") ? (String) integrationsConfigMap.get("businessUniqueId") : "";
            String surveyUUID = integrationsConfigMap.containsKey("surveyUUID") ? integrationsConfigMap.get("surveyUUID").toString() : "";
            boolean isHippaEnabled = integrationsConfigMap.containsKey("isHippaEnabled") ? (boolean) integrationsConfigMap.get("isHippaEnabled") : false;

            Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
//            String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
//            String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
            int createdBy = (int)triggerMap.get(Constants.CREATED_BY);

            Map respondentInfo = triggerService.getParticipantInfo(token, businessUniqueId); //respondent info
            Map personalInfoMap = this.getRespondentPersonalInfo(respondentInfo, isHippaEnabled); //respondent personal info

            //add feedback response in the ticket
            String feedback = "";
            if (addFeedback) {
                Map eventResponse = triggerService.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
                feedback = addFeedbackInTrigger(eventResponse);
            }

            //get customer id to create a ticket
            if (personalInfoMap != null) {
                int dtWorksBusinessId = isMultiTenant ? triggerNotification.getBusinessId() : 0;
                personalInfoMap.put("token", token);
                personalInfoMap.put("businessUniqueId", businessUniqueId);
                //get dtwork tenantId and auth details and set it in the header
                Map<String, Object> authMap = dtWorksService.verifyAndRenewToken(dtWorksBusinessId);//implementing tenant based token
                if(!authMap.containsKey("accessToken") || Utils.emptyString( (String)authMap.get("accessToken"))){
                    logger.info("Access token is invalid for business {}", businessUniqueId);
                    /*if(!resend) { //for resend case, no need to create failed notification again
                        triggerDAO.createFailedTriggerNotification(triggerNotification, "invalid access token."); //create failed notification
                        //mark failed status in trigger notification table
                        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
                    }else{
                        //mark failed status in failed trigger notification table
                        triggerDAO.updateFailedNotificationSentStatusByNotifyId(triggerNotification.getNotificationId(), Constants.STATUS_FAILED, businessUniqueId);
                    }*/
                    createFailedNotification(triggerNotification, "invalid access token.", businessUniqueId, notifyId, resend);
                    return;
                }
                // customer / profile is mandatory to create a ticket
                Map profileMap = dtWorksService.returnDTWorksProfileId(personalInfoMap, authMap, channel); // changing customer to profile as requested by dtworks

                if (!profileMap.isEmpty()) {
                    Map<String, String> metadataMap = Utils.isNotNull(integrationsConfigMap.get("metadata")) ?
                            (Map<String, String>) integrationsConfigMap.get("metadata") : new HashMap();
                    Map<String, String> dtworksMetaData = Utils.isNotNull(integrationsConfigMap.get("dtMetadata")) ?
                            (Map<String, String>) integrationsConfigMap.get("dtMetadata") : new HashMap();
                    //get reporter details
                    Map userMap = dtWorksService.getDtworksUserDetails(createdBy, authMap);
                    if(userMap == null){
                        createFailedNotification(triggerNotification, "invalid access token.", businessUniqueId, notifyId, resend);
                        return;
                    }
                    //get event source
                    String source = triggerDAO.getEventSourceByToken(surveyUUID, token);
                    //form dtworks interaction(ticket) api request
                    Map<String, Object> interactionRequest = dtWorksService.formDtworksInteractionRequest(profileMap, metadataMap, feedback, source, personalInfoMap, authMap, dtworksMetaData);
                    //create ticket in dtworks
                    Map resultMap = dtWorksService.createDTWorksInteraction(interactionRequest, authMap, userMap);
                    //update status to sent once email sent successfully
                    if (resultMap.containsKey("status") && Integer.parseInt(resultMap.get("status").toString()) == 200) {
                        logger.info("update notification");
                        if(resend){
                            triggerDAO.updateFailedNotificationSentStatusByNotifyId(triggerNotification.getNotificationId(), Constants.STATUS_COMPLETED, businessUniqueId);
                        }
                          // udpate ticket Id from dtworks to DT table
                        String dTworksTicketId = this.updateParticipantsWithDTworksTicketId(resultMap, respondentInfo);
                        //update sent status to completed in trigger_notification_{buuid} table
                        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                        //Delete the notification in trigger_notification table by notifyId
                        triggerDAO.deleteTriggerNotificationById(notifyId);
                        //push the updated ticket details to insights
                        this.pushTikcetDetailsToInsights(token, businessUniqueId, surveyUUID);
                        //update ticket details in event metadata
                        triggerDAO.updateEventMetadataByToken(dTworksTicketId, token, surveyUUID, Constants.DTWORKS_TICKET_ID);

                    }else{
                        //mark failed status in trigger notification table
                        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
                        //create an entry in failed notification table
                        triggerDAO.createFailedTriggerNotification(triggerNotification, new JSONObject(resultMap).toString());
                    }
                } else {
                    logger.info("Profile id is not present in dtworks");
                }
            } else {
                logger.info("Respondent info not available for the token {}", token);
            }

        } catch (Exception e) {
            logger.error("Exception at createDTWorksTicket. Msg {}", e.getMessage());
        }
    }

    private void createFailedNotification(TriggerNotification triggerNotification, String message, String businessUniqueId, int notifyId, boolean resend){
        if(!resend) { //for resend case, no need to create failed notification again
            triggerDAO.createFailedTriggerNotification(triggerNotification, message); //create failed notification
            //mark failed status in trigger notification table
            triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
        }else{
            //mark failed status in failed trigger notification table
            triggerDAO.updateFailedNotificationSentStatusByNotifyId(triggerNotification.getNotificationId(), Constants.STATUS_FAILED, businessUniqueId);
        }
    }

    /**
     * method to record the feedback response in dtworks under a profile
     * @param triggerNotification
     * @param integrationsConfigMap
     */
    private void createResponseInDTworks(TriggerNotification triggerNotification, Map integrationsConfigMap) {
        try {
//            int triggerId = triggerNotification.getTriggerId();
            String token = triggerNotification.getSubmissionToken();
//            String channel = triggerNotification.getChannel();

            String businessUniqueId = integrationsConfigMap.containsKey("businessUniqueId") ? (String) integrationsConfigMap.get("businessUniqueId") : "";
            boolean isHippaEnabled = integrationsConfigMap.containsKey("isHippaEnabled") ? (boolean) integrationsConfigMap.get("isHippaEnabled") : false;
            String surveyUUID = integrationsConfigMap.containsKey("surveyUUID") ? integrationsConfigMap.get("surveyUUID").toString() : "";

            Map respondentInfo = triggerService.getParticipantInfo(token, businessUniqueId); //respondent info
            Map personalInfoMap = this.getRespondentPersonalInfo(respondentInfo, isHippaEnabled); //respondent personal info
            //add feedback response in the ticket
            Map eventResponse = triggerService.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
            String feedback = addJsonFeedbackInTrigger(eventResponse);

            //get profile id to record a response
            if (personalInfoMap != null && Utils.notEmptyString(feedback)) {
                /*//get dtwork tenantId and auth details
                Map<String, Object> authMap = this.getUserAccessFromDtworks(businessUniqueId, triggerId);
                // customer / profile is mandatory to create a ticket
                Map profileMap = dtWorksService.returnDTWorksProfileId(personalInfoMap, authMap, channel); // changing customer to profile as requested by dtworks
                //TODO map the response to dtworks*/
                DTWorksRecordResponseBean dtWorksRecordResponseBean = new DTWorksRecordResponseBean();
                dtWorksRecordResponseBean.setToken(token);
                dtWorksRecordResponseBean.setBusinessId(triggerNotification.getBusinessId());
                dtWorksRecordResponseBean.setBusinesssUniqueId(businessUniqueId);
                dtWorksRecordResponseBean.setSurveyId(triggerNotification.getSurveyId());
                dtWorksRecordResponseBean.setScheduleId(triggerNotification.getScheduleId());
                dtWorksRecordResponseBean.setResponseData(feedback);
                dtWorksRecordResponseBean.setTriggerScheduleId(triggerNotification.getScheduleId());
                if(personalInfoMap.containsKey(Constants.EMAIL) || personalInfoMap.containsKey(Constants.PHONE)){
                    dtWorksRecordResponseBean.setMetaDataType(personalInfoMap.containsKey(Constants.EMAIL) ? "EMAIL" : "PHONE");
                    dtWorksRecordResponseBean.setMetaDataValue(personalInfoMap.containsKey(Constants.EMAIL) ? (String) personalInfoMap.get(Constants.EMAIL) : (String) personalInfoMap.get(Constants.PHONE));
                }else{
                    dtWorksRecordResponseBean.setMetaDataType("EMAIL");
                    dtWorksRecordResponseBean.setMetaDataValue("dttrigger@yopmail.com");//response with no email or phone
                }
                dtWorksService.recordResponseInDTworks(dtWorksRecordResponseBean);

            } else {
                logger.info("Respondent info not available for the token {}", token);
            }

        } catch (Exception e) {
            logger.error("Exception at createResponseInDTworks. Msg {}", e.getMessage());
        }
    }


    /**
     * method to update ticket id in participant table
     * @param resultMap
     * @param respondentInfo
     * @return  updated ticket value
     */
    private String updateParticipantsWithDTworksTicketId(Map resultMap, Map respondentInfo){
        String ticketDetails = null;
        try {
            Map ticketRespMap = (resultMap.containsKey("data")) ? (Map) resultMap.get("data") : new HashMap();
            String ticketId = ticketRespMap.containsKey("intxnNo") ? (String) ticketRespMap.get("intxnNo") : "";
            if (Utils.notEmptyString(ticketId) && !respondentInfo.isEmpty()) {
                logger.info("ticketId {} ", ticketId);
                String participantGroupId = respondentInfo.containsKey("participantGroupId") ? (String) respondentInfo.get("participantGroupId") : "";
                int participantId = respondentInfo.containsKey("participantId") ? (int) respondentInfo.get("participantId") : 0;
                String tableName = "participant_" + participantGroupId.replaceAll("-", "_");
                //check if ticketId exists in the participant table header
                Integer index = triggerDAO.checkIfHeaderExistElseCreate(Constants.DTWORKS_TICKET_ID, tableName);
                //update ticket id in the participant table
                triggerDAO.updateParticipantHeaderById(ticketId, participantId, tableName, index);
                //get updated ticket details
                ticketDetails = triggerDAO.getRespondentTicketDetailsById(participantId, tableName, index);

            }
        }catch (Exception e){
            logger.info("Exception at updateTicketIdInParticipant. Msg {}", e.getMessage());
        }
        return ticketDetails;
    }

    private void pushTikcetDetailsToInsights(String token, String businessUniqueId, String surveyUniqueId) {
        Map<String, Object> insightRequestMap = new HashMap();
        insightRequestMap.put("businessId", businessUniqueId);
        insightRequestMap.put("surveyId", surveyUniqueId);
        insightRequestMap.put("tokens", Collections.singletonList(token));
        callInsightsRestApi(eventUrl +"/event/insights",insightRequestMap, HttpMethod.POST);
    }

    private Map getRespondentPersonalInfo(Map respondentInfo, boolean isHippaEnabled){
        Map<String, Object> personalInfoMap = new HashMap();
        Iterator<String> itr = respondentInfo.keySet().iterator();
        while (itr.hasNext()) {
            String metaDataField =  itr.next();
            if (!isHippaEnabled) {
                String metaDataVal =  Utils.isNotNull(respondentInfo.get(metaDataField)) ? respondentInfo.get(metaDataField).toString() : "";
                int index = metaDataField.indexOf(Constants.DELIMITER);
                if(index > -1 && Utils.notEmptyString(metaDataVal)) {
                    String metaDataType = metaDataField.substring(index + 2, metaDataField.length()); //eg: Email~~EMAIL to EMAIL
                    String metaDataName = metaDataField.substring(0, index);
                    if (metaDataType.equalsIgnoreCase("NAME")  || metaDataType.equalsIgnoreCase("LINKNAME")) {
                        personalInfoMap.put("name", metaDataVal);
                    } else if (metaDataType.equalsIgnoreCase("EMAIL")) {
                        personalInfoMap.put("email", metaDataVal);
                    } else if (metaDataType.equalsIgnoreCase("PHONE")) {
                        personalInfoMap.put("phone", metaDataVal);
                    }else{
                        personalInfoMap.put(metaDataName, metaDataVal);
                    }
                }
            }
        }
        return personalInfoMap;
    }

   /* private Map formDtworksInteractionRequest(Map customerMap, Map integrationMetadata, String feedback, String channel, Map personalInfoMap) {
        Map interactionRequest = new HashMap();
        // all are mandatory fields
        interactionRequest.put("customerId", customerMap.containsKey("customerId") ? customerMap.get("customerId") : "");
        interactionRequest.put("referenceCategory", "CUSTOMER_SERVICE"); //mapping customer to the interaction
        //Needs to be dynamic statment and statementId
        interactionRequest.put("statement", "Sewage waters are coming out need to be cleaned"); //Needed for analytics
        interactionRequest.put("statementId", 17);//Needed for analytics
        interactionRequest.put("project","DTWORKS"); //needed this to add the ticket in the pooled interaction
        interactionRequest.put("referenceValue", customerMap.containsKey("customerNo") ? customerMap.get("customerNo") : "");
        interactionRequest.put("serviceType", Utils.notEmptyString((String) integrationMetadata.get("serviceType")) ? integrationMetadata.get("serviceType") : "ST_APPMAINTENANCE");
        interactionRequest.put("interactionCategory", Utils.notEmptyString((String) integrationMetadata.get("interactionCategory")) ? integrationMetadata.get("interactionCategory") : "SERVICE_RELATED");
        interactionRequest.put("serviceCategory", Utils.notEmptyString((String) integrationMetadata.get("serviceCategory")) ? integrationMetadata.get("serviceCategory") : "PST_ADMINSRV");
        interactionRequest.put("interactionType", Utils.notEmptyString((String) integrationMetadata.get("interactionType")) ? integrationMetadata.get("interactionType") : "GENERAL");
        interactionRequest.put("channel", "DROPTHOUGHT");
        interactionRequest.put("contactPreference", Collections.singletonList(Constants.DTWORSK_CONTACTPREFERENCE.containsKey(channel.toLowerCase()) ? Constants.DTWORSK_CONTACTPREFERENCE.get(channel.toLowerCase()) : "CNT_PREF_EMAIL")); //optional
        interactionRequest.put("priorityCode", Utils.notEmptyString((String) integrationMetadata.get("priority")) ? integrationMetadata.get("priority") : "PRTYLOW");
        interactionRequest.put("remarks", Utils.emptyString(feedback) ? "Ticket from DT" : feedback);
        interactionRequest.put("currUser", Utils.isNotNull(integrationMetadata.get("assignee")) ? integrationMetadata.get("assignee") : "");
        interactionRequest.put("appointAddress", dtWorksService.setAddress(personalInfoMap));
        return interactionRequest;
    }*/


    public String addFeedbackInTrigger(Map eventMap) {
        StringBuilder feedbackStr = new StringBuilder();
        List responses = (List) eventMap.get("responses");
        List questionText = (List) eventMap.get("questions");
        Iterator itrResponse = responses.iterator();
        Iterator itrQuestion = questionText.iterator();
        int cnt = 1;
        while (itrResponse.hasNext()) {
            String response = (String) itrResponse.next();
            String question = (String) itrQuestion.next();
            feedbackStr.append("Q"+cnt+": ");
            feedbackStr.append(question).append("<br/>");
            feedbackStr.append("A"+cnt+": ");
            feedbackStr.append(Utils.emptyString(response) ? "NA" : response).append("<br/>");
            cnt++;
        }
        return feedbackStr.toString();
    }

    /**
     * method to construct response in json format
     * @param eventMap
     * @return
     */
    public String addJsonFeedbackInTrigger(Map eventMap) {
        LinkedHashMap questionMap = new LinkedHashMap();
        List responses = (List) eventMap.get("responses");
        List questionText = (List) eventMap.get("questions");
        Iterator itrResponse = responses.iterator();
        Iterator itrQuestion = questionText.iterator();
        int cnt = 1;
        while (itrResponse.hasNext()) {
            String response = (String) itrResponse.next();
            String question = (String) itrQuestion.next();
            questionMap.put(cnt+"."+question, Utils.emptyString(response) ? "NA" : response);//count is added to avoid duplicate key getting overlapped
            cnt++;
        }
        try {
            return mapper.writeValueAsString(questionMap);
        } catch (JsonProcessingException e) {
            logger.error("Exception at addJsonFeedbackInTrigger. Msg {}", e.getMessage());
        }
        return "";
    }

    /**
     * rest api call to dtworks
     * @param uri
     * @param inputMap
     * @param method
     * @return
     */
    public Map callInsightsRestApi(String uri, Map<String, Object> inputMap, HttpMethod method){

        logger.info("begin insights rest api");
        Map respMap = new HashMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject(inputMap);
        String inputJson = jsonObject.toString();
        logger.info("input json {}", inputJson);
        logger.info("uri {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    logger.info("success response");
                }
            }else{
                respMap.put("success", false);
            }
            logger.info("end insights rest api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 500);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 500);
            e.printStackTrace();
        }
        return  respMap;
    }

    /**
     * method to get user specific access from dtworks
     * @param businessUniqueId
     * @param triggerId
     * @return
     */
    /*private Map getUserAccessFromDtworks(String businessUniqueId, int triggerId) {
        int createdBy = triggerDAO.getUserByTriggerId(businessUniqueId, triggerId);
        int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
        Map userAccessMap = new HashMap();
        if (createdBy > 0) {
            Map userMap = dtWorksService.getUserAccessByUserId(createdBy);
            userMap.put("userId", createdBy);
            userAccessMap = dtWorksService.verifyAndRenewUserToken(userMap, businessId);
        }
        if(createdBy == 0 || !userAccessMap.containsKey("accessToken")){ //TODO in case of user not migrated
            userAccessMap = dtWorksService.retrieveDTWorksConfigs(businessId);
        }
        return userAccessMap;
    }*/

    //NOT used anywhere
    public void updateAccessTokenForUser() {
        //dtWorksService.updateExpiredUserAccessTokens();
    }

    /**
     * method to resend failed dtworks triggers
     * @param programId
     * @param businessUniqueId
     * @return
     */
    public boolean resendFailedDtworksTriggers(String businessUniqueId, int programId, int scheduleId) {
        int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
        // get list of active notification details to send
        List notifyTriggerList = triggerDAO.getFailedDtworksTriggerNotifications(businessId, programId, scheduleId);
        Iterator iterator = notifyTriggerList.iterator();
        // loop through each notification records
        while (iterator.hasNext()) {
            Map triggerNotifyMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(triggerNotifyMap)) {
                    TriggerNotification triggerNotification = new TriggerNotification();
                    triggerNotification.setChannel(triggerNotifyMap.get(Constants.CHANNEL).toString());
                    if(Utils.isNotNull(triggerNotifyMap.get(Constants.SUBJECT))) {
                        triggerNotification.setSubject(triggerNotifyMap.get(Constants.SUBJECT).toString());
                    }
                    if(Utils.isNotNull(triggerNotifyMap.get(Constants.MESSAGE))) {
                        triggerNotification.setMessage(triggerNotifyMap.get(Constants.MESSAGE).toString());
                    }
                    /*int notifyId = (int) triggerNotifyMap.get(Constants.ID);
                    triggerNotification.setNotifyId(notifyId);*/
                    triggerNotification.setNotificationId(triggerNotifyMap.get(Constants.NOTIFICATION_UUID).toString());
                    triggerNotification.setContacts(Utils.isNotNull(triggerNotifyMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerNotifyMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    triggerNotification.setAddFeedback((int) triggerNotifyMap.get(Constants.ADD_FEEDBACK) == 0 ? false : true);
                    triggerNotification.setConditionQuery(triggerNotifyMap.containsKey(Constants.CONDITION_QUERY) ? triggerNotifyMap.get(Constants.CONDITION_QUERY).toString() : "");
                    triggerNotification.setBusinessId((int) triggerNotifyMap.get(Constants.BUSINESS_ID));
                    triggerNotification.setSurveyId((int) triggerNotifyMap.get(Constants.SURVEY_ID));
                    triggerNotification.setTriggerId((int) triggerNotifyMap.get(Constants.TRIGGER_ID));
                    triggerNotification.setHost(Utils.isNotNull(triggerNotifyMap.get(Constants.HOST)) ? triggerNotifyMap.get(Constants.HOST).toString() : "http://localhost:4200");
                    triggerNotification.setSubmissionToken(Utils.isNotNull(triggerNotifyMap.get(Constants.SUBMISSION_TOKEN)) ? triggerNotifyMap.get(Constants.SUBMISSION_TOKEN).toString() : "");
                    triggerNotification.setTimezone(Utils.isNotNull(triggerNotifyMap.get(Constants.TIMEZONE)) ? triggerNotifyMap.get(Constants.TIMEZONE).toString() : Constants.TIMEZONE_AMERICA_LOSANGELES);
                    triggerNotification.setScheduleId(triggerNotifyMap.get(Constants.SCHEDULE_ID).toString());
                    if(triggerNotifyMap.containsKey(Constants.REPLYRESPONDENT) && Utils.isNotNull(triggerNotifyMap.get(Constants.REPLYRESPONDENT))) {
                        triggerNotification.setReplyToRespondent(triggerNotifyMap.get(Constants.REPLYRESPONDENT).toString());
                    }
                    if(triggerNotifyMap.containsKey(Constants.ACTION_TYPE) && Utils.isNotNull(triggerNotifyMap.get(Constants.ACTION_TYPE))) {
                        triggerNotification.setActionType(triggerNotifyMap.get(Constants.ACTION_TYPE).toString());
                    }
                    if(triggerNotifyMap.containsKey(Constants.START_TIME) && Utils.isNotNull(triggerNotifyMap.get(Constants.START_TIME))) {
                        triggerNotification.setStartTime(triggerNotifyMap.get(Constants.START_TIME).toString().substring(0,19));
                    }
                    /* for resend case: both failed_trigger_notifications & trigger_notifications tables have same values in notification_uuid.
                     * So fetch the primary key of trigger_notification using notification_uuId from failed_trigger_notifications table (to proceed with the existing flow)*/
                    Map notificationMap = triggerDAO.getTriggerNotificationById(triggerNotification.getNotificationId());
                    int notifyId = (int) notificationMap.get(Constants.ID);
                    triggerNotification.setNotifyId(notifyId);
                    this.sendTriggerDTWorksWebhook(triggerNotification, true);
                    return true;
                }
            } catch (Exception e) {
                logger.error("Exception at resendFailedDtworksTriggers. Msg {}", e.getMessage());
                e.printStackTrace();
                }
        }
        return false;
    }

}
