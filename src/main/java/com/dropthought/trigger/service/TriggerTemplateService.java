package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.dao.TriggerTemplateDAO;
import com.dropthought.trigger.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class TriggerTemplateService {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    TriggerTemplateDAO triggerTemplateDAO;

    @Autowired
    TriggerDAO triggerDAO;

    @Autowired
    TriggerWorkatoService triggerWorkatoService;

    /**
     * method to create trigger by passing trigger bean
     *
     * @param bean
     */
    public Map createTriggerTemplate(TriggerTemplate bean) {

        Map result = new HashMap();
        // check if trigger name already exists
        if (triggerTemplateDAO.isTriggerTemplateNameExists(bean.getTriggerName(), bean.getBusinessUniqueId(), bean.getUserId())) {
            // assign and return error response
            result.put(Constants.SUCCESS, false);
            result.put(Constants.MESSAGE, Constants.TRIGGER_NAME_EXIST);
        } else {
            // create a new entry for trigger conditions
            String triggerUniqueId = triggerTemplateDAO.createTriggerTemplate(bean);
            // assign success / failure response based on the intcount
            result.put(Constants.SUCCESS, triggerUniqueId.length() > 0 ? true : false);
            result.put(Constants.RESULT, triggerUniqueId);
        }
        return result;
    }

    /**
     * method to update trigger by passing trigger bean and primary Id
     *
     * @param bean
     * @param triggerId
     */
    public int updateTrigger(TriggerTemplate bean, String triggerId) {
        int result = triggerTemplateDAO.updateTriggerTemplate(bean, triggerId);
        return result;
    }

    /**
     * method to get trigger template details by passing triggerId
     *
     * @param triggerTemplateId
     * @param businessUniqueId
     */
    public TriggerTemplate getTriggerTemplateById(String triggerTemplateId, String businessUniqueId, String timezone) {

        TriggerTemplate trigger = new TriggerTemplate();
        try {
            Map triggerMap = triggerTemplateDAO.getTriggerTemplateMapByUUId(triggerTemplateId);
            if (Utils.isNotNull(triggerMap) && triggerMap.containsKey(Constants.TRIGGER_UUID)) {
                timezone = Utils.notEmptyString(timezone) ? timezone : Constants.TIMEZONE_AMERICA_LOSANGELES;
                trigger.setTemplateName(triggerMap.get(Constants.TEMPLATE_NAME).toString());
                trigger.setStatus(((int) triggerMap.get(Constants.STATUS)) == 0 ? Constants.STATUS_INACTIVE : Constants.STATUS_ACTIVE);
                trigger.setTemplateId(triggerTemplateDAO.returnTemplateUniqueIdById(Integer.parseInt(triggerMap.get(Constants.TEMPLATE_ID).toString()), businessUniqueId, "dropthought"));
                trigger.setTriggerName(triggerMap.get(Constants.TRIGGER_NAME).toString());
                trigger.setTriggerCondition(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : Constants.EMPTY);
                trigger.setLastModified(Utils.isNotNull(triggerMap.get(Constants.MODIFIED_TIME)) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.MODIFIED_TIME).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerId(triggerMap.get(Constants.TRIGGER_UUID).toString());
//                trigger.setBusinessUniqueId(businessUniqueId);
//                trigger.setSurveyStartDate(triggerMap.containsKey(Constants.FROM_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.FROM_DATE).toString(), timezone) : Constants.EMPTY);
//                trigger.setSurveyEndDate(triggerMap.containsKey(Constants.TO_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.TO_DATE).toString(), timezone) : Constants.EMPTY);
//                trigger.setTriggerFilterUUID(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_FILTER_UUID)) ? triggerMap.get(Constants.TRIGGER_FILTER_UUID).toString() : Constants.EMPTY);
                int anonymousValue = (Utils.isNotNull(triggerMap.get(Constants.ANONYMOUS)) && Utils.isStringNumber(triggerMap.get(Constants.ANONYMOUS).toString())) ? Integer.parseInt(triggerMap.get(Constants.ANONYMOUS).toString()) : 0;
                trigger.setAnonymous((anonymousValue==1) ? true : false);
            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return trigger;
    }

    /**
     * method to delete trigger template by passing triggerId
     *
     * @param triggerId
     */
    public int deleteTriggerTemplate(String triggerId) {
        return triggerTemplateDAO.deleteTriggerTemplate(triggerId);
    }

    /**
     * method to get trigger templates name list by templateId
     * @param templateUniqueId
     * @param businessUniqueId
     * @return
     */
    public Map getTriggerTemplatesNameListById(String templateUniqueId, String businessUniqueId){
        List templateTriggersList = new ArrayList();
        Map result = new HashMap();
        try {
            int templateId = triggerTemplateDAO.returnTemplateIdByUniqueId(templateUniqueId, businessUniqueId, "dropthought");
            int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
            List templateList = triggerTemplateDAO.getTriggerTemplateMapByTemplateId(templateId, businessId);
            Iterator itr = templateList.iterator();
            while (itr.hasNext()) {
                Map templateMap = new HashMap();
                Map eachTemplateMap = (Map) itr.next();
                int triggerId = eachTemplateMap.containsKey("id") ? (int) eachTemplateMap.get("id") : 0;
                templateMap.put("templateId", templateUniqueId);
                templateMap.put("templateName", eachTemplateMap.get(Constants.TEMPLATE_NAME));
                templateMap.put("triggerName", eachTemplateMap.get(Constants.TRIGGER_NAME));
                templateMap.put("triggerId", eachTemplateMap.get(Constants.TRIGGER_UUID));
                templateMap.put("status", eachTemplateMap.get(Constants.STATUS));
                templateMap.put("businessId", businessUniqueId);
                templateMap.put("frequency", eachTemplateMap.get(Constants.FREQUENCY));
                templateMap.put("triggerCondition", eachTemplateMap.get(Constants.TRIGGER_CONDITION));
                if (triggerId > 0) {
                    List scheduleList = triggerTemplateDAO.getTriggerTemplateScheduleByTriggerID(triggerId, businessId);
                    Iterator scheduleItr = scheduleList.iterator();
                    List scheduleMapList = new ArrayList();
                    while (scheduleItr.hasNext()) {
                        try {
                            Map scheduleMap = (Map) scheduleItr.next();
                            TriggerTemplateSchedule eachScheduleMap = convertToScheduleResponseMap(scheduleMap);
                            scheduleMapList.add(eachScheduleMap);
                        } catch (Exception e) {
                            logger.error("error at getTriggerTemplatesNameListById1 {}", e.getMessage());
                        }
                    }
                    templateMap.put("schedule", scheduleMapList);
                }
                templateTriggersList.add(templateMap);
            }
            result.put("triggers", templateTriggersList);

            //workflows
            List workflowList = triggerTemplateDAO.getTriggerTemplateWorkflowByTemplateId(templateUniqueId, businessId);
            if(!workflowList.isEmpty()){
                Iterator workflowItr = workflowList.iterator();
                while (workflowItr.hasNext()) {
                    try {
                        Map workflowMap = (Map) workflowItr.next();
                        List workFlows = workflowMap.containsKey("workflows") ? mapper.readValue(workflowMap.get("workflows").toString(), ArrayList.class) : new ArrayList();
                        Iterator workFlowItr = workFlows.iterator();
                        List workFlowList = new ArrayList();
                        while (workFlowItr.hasNext()) {
                            try {
                                Map workFlowMap = (Map) workFlowItr.next();
                                String integrationName = new ArrayList<>(workFlowMap.keySet()).get(0).toString();
                                int integrationId = triggerDAO.getIntegrationIdByName(integrationName);
                                List recipeList = (List) workFlowMap.get(integrationName);
                                workFlowMap.put("integrationId", integrationId);
                                workFlowMap.put("integrationName", integrationName);
                                List recipeListWithId = triggerWorkatoService.getListOfRecipesWithId("", integrationId, businessUniqueId, recipeList);
                                workFlowMap.put("recipes", recipeListWithId);
                                workFlowMap.remove(integrationName);
                                workFlowList.add(workFlowMap);
                            } catch (Exception e) {
                                logger.error("error at workflow map {}", e.getMessage());
                            }
                        }
                        result.put("workflows", workFlowList);
                    } catch (Exception e) {
                        logger.error("error at workflow map {}", e.getMessage());
                    }
                }
            }
        }catch (Exception e){
            logger.error("error at getTriggerTemplatesNameListById {}", e.getMessage());}
        return result;
    }

    /**
     * method to convert schedule table column to response
     * @param scheduleMap
     * @return
     */
    private TriggerTemplateSchedule convertToScheduleResponseMap(Map scheduleMap) {
        TriggerTemplateSchedule schedule = new TriggerTemplateSchedule();
        try {
            schedule.setStatus((String) scheduleMap.get(Constants.STATUS));
            schedule.setFrequency(Utils.isNotNull(scheduleMap.get(Constants.FREQUENCY)) ? scheduleMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
            schedule.setScheduleId(scheduleMap.get(Constants.SCHEDULE_UUID).toString());
            schedule.setTimezone(Utils.isNotNull(scheduleMap.get(Constants.TIMEZONE)) ? scheduleMap.get(Constants.TIMEZONE).toString() : null);
            schedule.setChannel(Utils.isNotNull(scheduleMap.get(Constants.CHANNEL)) ? scheduleMap.get(Constants.CHANNEL).toString() : Constants.EMPTY);
            schedule.setMessage(Utils.isNotNull(scheduleMap.get(Constants.MESSAGE)) ? scheduleMap.get(Constants.MESSAGE).toString() : Constants.EMPTY);
            schedule.setReplyToRespondent(Utils.isNotNull(scheduleMap.get(Constants.REPLYRESPONDENT)) ? scheduleMap.get(Constants.REPLYRESPONDENT).toString() : Constants.EMPTY);
            schedule.setSubject(Utils.isNotNull(scheduleMap.get(Constants.SUBJECT)) ? scheduleMap.get(Constants.SUBJECT).toString() : Constants.EMPTY);
            schedule.setContacts(Utils.isNotNull(scheduleMap.get(Constants.CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
            schedule.setAddFeedback((int) (scheduleMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
            schedule.setTriggerConfig(Utils.isNotNull(scheduleMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig());
            schedule.setWebhookConfig(Utils.isNotNull(scheduleMap.get(Constants.WEBHOOK_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.WEBHOOK_CONFIG).toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData());
            schedule.setIntegrationsConfig(Utils.isNotNull(scheduleMap.get(Constants.INTEGRATIONS_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.INTEGRATIONS_CONFIG).toString(), HashMap.class) : null);
            schedule.setHeaderData(Utils.isNotNull(scheduleMap.get(Constants.HEADER_DATA)) ? mapper.readValue(scheduleMap.get(Constants.HEADER_DATA).toString(), TriggerListHeaderData.class) : null);
        }catch (Exception e){
            logger.error("exception at TriggerTemplateSchedule {}", e.getMessage());
        }
        return schedule;
    }

    /**
     * method to create trigger Template schedule by passing schedule bean
     *
     * @param bean
     */
    public Map createTriggerTemplateSchedule(TriggerTemplateSchedule bean) {
        // get business primary id
        int businessId = triggerDAO.getBusinessIdByUUID(bean.getBusinessId());
        // get surveyId, triggerid and assign it to request bean
        Map trigger = triggerTemplateDAO.getTriggerTemplateMapByUUId(bean.getTriggerId());
        bean.setTemplateId(trigger.get(Constants.TEMPLATE_ID).toString());
        bean.setTriggerId(trigger.get(Constants.ID).toString());
        bean.setBusinessId(String.valueOf(businessId));

        // create a new entry for trigger schedule
        String intCount = triggerTemplateDAO.createTriggerTemplateSchedule(bean);

        // assign success / failure response based on the intcount
        Map result = new HashMap();
        result.put(Constants.SUCCESS, intCount.length() > 0 ? true : false);
        result.put(Constants.RESULT, intCount);
        return result;
    }

    /**
     * method to update Template schedule by passing schedule bean and primary Id
     *
     * @param bean
     * @param scheduleId
     */
    public int updateTriggerSchedule(TriggerTemplateSchedule bean, String scheduleId) {
        return triggerTemplateDAO.updateTriggerTemplateSchedule(bean, scheduleId);
    }

    /**
     * method to delete trigger Template schedule by passing triggerId
     *
     * @param scheduleId
     */
    public int deleteTriggerSchedule(String scheduleId) {
        return triggerTemplateDAO.deleteTriggerTemplateSchedule(scheduleId);
    }

    /**
     * method to get trigger Template schedule details by passing schedule uniqueId
     *
     * @param scheduleId
     */
    public TriggerTemplateSchedule getTriggerTemplateScheduleById(String scheduleId, String timezone) {
        TriggerTemplateSchedule schedule = new TriggerTemplateSchedule();
        timezone = Utils.notEmptyString(timezone) ? timezone : Constants.TIMEZONE_AMERICA_LOSANGELES;
        try {
            Map scheduleMap = triggerTemplateDAO.getTriggerTemplateScheduleByUniqueID(scheduleId);
            if (Utils.isNotNull(scheduleMap)) {
                schedule = convertToScheduleResponseMap(scheduleMap);
              /*  schedule.setStatus((String) scheduleMap.get(Constants.STATUS));
                schedule.setFrequency(Utils.isNotNull(scheduleMap.get(Constants.FREQUENCY)) ? scheduleMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
                schedule.setScheduleId(scheduleMap.get(Constants.SCHEDULE_UUID).toString());
//                schedule.setTimezone(scheduleMap.get(Constants.TIMEZONE).toString());
                schedule.setChannel(Utils.isNotNull(scheduleMap.get(Constants.CHANNEL)) ? scheduleMap.get(Constants.CHANNEL).toString() : Constants.EMPTY);
                schedule.setMessage(Utils.isNotNull(scheduleMap.get(Constants.MESSAGE)) ? scheduleMap.get(Constants.MESSAGE).toString() : Constants.EMPTY);
                schedule.setSubject(Utils.isNotNull(scheduleMap.get(Constants.SUBJECT)) ? scheduleMap.get(Constants.SUBJECT).toString() : Constants.EMPTY);
                schedule.setContacts(Utils.isNotNull(scheduleMap.get(Constants.CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                schedule.setAddFeedback((int) (scheduleMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                schedule.setTriggerConfig(Utils.isNotNull(scheduleMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig());
                schedule.setHeaderData(Utils.isNotNull(scheduleMap.get(Constants.HEADER_DATA)) ? mapper.readValue( scheduleMap.get(Constants.HEADER_DATA).toString(), TriggerListHeaderData.class) : null);
                schedule.setWebhookConfig(Utils.isNotNull(scheduleMap.get(Constants.WEBHOOK_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.WEBHOOK_CONFIG).toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData());
*/            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return schedule;
    }

    /**
     * method to get trigger schedule details by passing triggerId
     *
     * @param triggerUniqueId
     * @param businessUniqueId
     */
    public List<TriggerTemplateSchedule> getTriggerScheduleByTriggerId(String triggerUniqueId, String timezone, String businessUniqueId) {
        List<TriggerTemplateSchedule> triggerScheduleList = new ArrayList<>();
        try {
            int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
            Map triggerMap = triggerTemplateDAO.getTriggerTemplateMapByUUId(triggerUniqueId);
            if(businessId > 0 && triggerMap.containsKey("id")) {
                int triggerId = (int) triggerMap.get("id");
                List scheduleList = triggerTemplateDAO.getTriggerTemplateScheduleByTriggerID(triggerId, businessId);
                Iterator itr = scheduleList.iterator();
                while (itr.hasNext()) {
                    try {
                        Map scheduleMap = (Map) itr.next();
                        TriggerTemplateSchedule schedule = convertToScheduleResponseMap(scheduleMap);
                        triggerScheduleList.add(schedule);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleList;
    }

}
