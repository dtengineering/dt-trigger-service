package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.dao.TriggerNotesDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TriggerNotesService {

    @Value("${fcm}")
    private String fcmKey;
    @Value("${dt.pushnotification.url}")
    private String pushNotificationUrl;

    @Value("${app.name}")
    private String appName;

    @Autowired
    RestTemplate restTemplate;

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    private TriggerNotesDAO triggerNotesDAO;
    public TriggerNotesService(TriggerNotesDAO triggerNotesDAO) {
        this.triggerNotesDAO = triggerNotesDAO;
    }
    public List getActiveNotesNotifications(){
        return triggerNotesDAO.getActiveNotesNotifications();
    }

    public void updateNotesNotificationSentStatusByNotifyId(int notifyId, String status){
        triggerNotesDAO.updateNotesNotificationStatusByNotifyId(notifyId, status);
    }

    @Transactional(rollbackFor = Exception.class)
    public void performNotesNotificationPostProcessing(int notifyId, String status){
        //insert entry into notes_notification_archive table and delete entry into notes_notification table
        try {
            triggerNotesDAO.createEntryInNotesNotificationArchive(notifyId, status);
            triggerNotesDAO.deleteEntryFromNotesNotification(notifyId);
        } catch (RuntimeException e) {
            //update status in notes_notification table
            logger.info("Error in transaction, rolling back & updating status in notes_notification table");
            triggerNotesDAO.updateNotesNotificationStatusByNotifyId(notifyId, status);
        }catch (Exception e){
            logger.error("Error in performNotesNotificationPostProcessing service", e);
        }
    }

    public String processNotesNotification(Map notesNotificationMap){
        String status = "";
        try{
            if(!notesNotificationMap.isEmpty()){
                String notesUUID = notesNotificationMap.getOrDefault("notes_uuid","").toString();
                List<String> contacts = mapper.readValue(notesNotificationMap.getOrDefault("contacts", "[]").toString(), ArrayList.class);
                String title = notesNotificationMap.getOrDefault("subject","").toString();
                String message = notesNotificationMap.getOrDefault("message","").toString();

                Map payload = new HashMap();
                payload.put("emails", contacts);
                Map data = new HashMap();
                data.put("deepLink", "dtenterprise://notification/note/"+notesUUID);
                payload.put("data", data);
                payload.put("title", title);
                payload.put("message", message);
                payload.put("appName", appName);


                Map responseMap = callRestApi(new JSONObject(payload).toString(), HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);
                status = responseMap.isEmpty() ? "failed" : "sent";
            }
        }catch (Exception e){
            logger.error("Error in processNotesNotification", e);
        }
        return status;
    }

    /**
     * Method to call rest api
     */
    private Map callRestApi(String inputJson, HttpMethod method, String channel, String uri) {
        Map responseMap = new HashMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        logger.info("uri {}",uri);
        logger.info("inputJson {}",inputJson);
        if (channel.equals(Constants.CHANNEL_PUSH))
            headers.set("x-api-key", fcmKey);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("Notification request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    responseMap = (Map) responseEntity.getBody();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseMap;
    }

}
