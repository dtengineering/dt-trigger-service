package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.model.TwoFortySevenRequestBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class TriggerTwoFortySevenSoftwareService {

    @Autowired
    TriggerDAO triggerDAO;
    @Autowired
    TwoFortySevenSoftwareService softwareService;
    @Autowired
    TriggerService triggerService;
    @Autowired
    RestTemplate restTemplate;

    @Value("${247software.url}")
    private String softwareUrl;

    @Value("${software247.webhook.endpoint}")
    private String softwareWebhookEndpoint;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    public void sendTrigger247Software(TriggerNotification triggerNotification) {

        try {
            logger.info("trigger 24/7 software webhook");
            int scheduleId = Integer.parseInt(triggerNotification.getScheduleId());
            String channel = triggerNotification.getChannel();

            int businessId = triggerNotification.getBusinessId();
            int surveyId = triggerNotification.getSurveyId();
            String timeZone = triggerNotification.getTimezone();
            String notifyTime = Utils.isNotNull(triggerNotification.getStartTime()) ? triggerNotification.getStartTime() : "";

            Map businessMap = triggerDAO.getBusinessDetailsById(businessId);
            String businessUniqueId = businessMap.containsKey("business_uuid") ? (String) businessMap.get("business_uuid") : "";
            boolean isHippaEnabled = triggerDAO.isHippaEnableForBusiness(businessId);

            //surveyUUID
            Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
            String surveyUUID = surveyMap.containsKey("survey_uuid") ? surveyMap.get("survey_uuid").toString() : "";
            boolean surveyAnonymous = surveyMap.containsKey("anonymous") ? (int) surveyMap.get("anonymous") == 1 ? true : false : false;

            //get schedule webhook_config
            Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
            Map integrationsConfigMap = scheduleMap.containsKey("integrations_config") && Utils.isNotNull(scheduleMap.get("integrations_config")) ? mapper.readValue(scheduleMap.get("integrations_config").toString(), HashMap.class) : new HashMap();
            Map action = integrationsConfigMap.containsKey(Constants.CHANNEL_247SOFTWARE) ? (Map) integrationsConfigMap.get(Constants.CHANNEL_247SOFTWARE) : new HashMap();
            String actionStr = action.containsKey("recipeType") ? action.get("recipeType").toString() : "";
            integrationsConfigMap.put("businessUniqueId", businessUniqueId);
            integrationsConfigMap.put("surveyUUID", surveyUUID);
            integrationsConfigMap.put("isHippaEnabled", isHippaEnabled);

            if (channel.equalsIgnoreCase(Constants.CHANNEL_247SOFTWARE)) {
                if (actionStr.equalsIgnoreCase("createIncident")) {
                    logger.info("create Incident in 24/7 Software");
                    this.createIncident(triggerNotification, integrationsConfigMap, notifyTime, timeZone, surveyAnonymous);

                } else if (actionStr.equalsIgnoreCase("createRequest")) {
                    logger.info("create request in 24/7 Software");
                    this.createRequest(triggerNotification, integrationsConfigMap);

                } else if (actionStr.equalsIgnoreCase("createWorkOrder")) {
                    logger.info("create work order in 24/7 Software");
                    this.createWorkOrder(triggerNotification, integrationsConfigMap, notifyTime, timeZone, surveyAnonymous);
                }
            } else{
                logger.info("channel is not 24/7 software");
            }
        } catch (Exception e) {
            logger.error("Exception at sendTrigger247Software. Msg {}", e.getMessage());
        }

    }

    /**
     * function to create work order in 24/7 software
     * @param triggerNotification
     * @param integrationsConfigMap
     * @param notifyTime
     * @param timeZone
     * @param surveyAnonymous
     */
    private void createWorkOrder(TriggerNotification triggerNotification, Map integrationsConfigMap, String notifyTime,
                                 String timeZone, boolean surveyAnonymous) {

        try {
            String baseUrl = softwareUrl + "cmms/work-order/v2/insert";
            int notifyId = triggerNotification.getNotifyId();
            String token = triggerNotification.getSubmissionToken();
            String channel = triggerNotification.getChannel();
            boolean addFeedback = triggerNotification.isAddFeedback();
            int triggerId = triggerNotification.getTriggerId();
            int businessId = triggerNotification.getBusinessId();

            String businessUniqueId = integrationsConfigMap.containsKey("businessUniqueId") ? (String) integrationsConfigMap.get("businessUniqueId") : "";
            String surveyUUID = integrationsConfigMap.containsKey("surveyUUID") ? integrationsConfigMap.get("surveyUUID").toString() : "";
            boolean isHippaEnabled = integrationsConfigMap.containsKey("isHippaEnabled") ? (boolean) integrationsConfigMap.get("isHippaEnabled") : false;

            String eventId = integrationsConfigMap.containsKey("eventId") ? integrationsConfigMap.get("eventId").toString() : "";
            String facilityId = integrationsConfigMap.containsKey("facilityId") ? integrationsConfigMap.get("facilityId").toString() : "";
            String departmentId = integrationsConfigMap.containsKey("departmentId") ? integrationsConfigMap.get("departmentId").toString() : "";
            String workOrderTypeId = integrationsConfigMap.containsKey("workOrderTypeId") ? integrationsConfigMap.get("workOrderTypeId").toString() : "";
            String locationId = integrationsConfigMap.containsKey("locationId") ? integrationsConfigMap.get("locationId").toString() : "";
            String sectionId = integrationsConfigMap.containsKey("sectionId") ? integrationsConfigMap.get("sectionId").toString() : "";
            String requestTypeId = integrationsConfigMap.containsKey("requestTypeId") ? integrationsConfigMap.get("requestTypeId").toString() : "";
            String userId = integrationsConfigMap.containsKey("userId") ? integrationsConfigMap.get("userId").toString() : "";
            Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
            String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
//          String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
            int createdBy = (int)triggerMap.get(Constants.CREATED_BY);

            Map respondentInfo = triggerService.getParticipantInfo(token, businessUniqueId); //respondent info
            Map<String, Object> personalInfoMap = this.getRespondentPersonalInfo(respondentInfo, isHippaEnabled); //respondent personal info

            //add feedback response in the ticket
            String feedback = "";
            if (addFeedback) {
                Map eventResponse = triggerService.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
                feedback = addFeedbackInTwentyFourBySeven(eventResponse, surveyName);
                if (!surveyAnonymous && !isHippaEnabled && personalInfoMap != null && !personalInfoMap.isEmpty()) {
                    StringBuilder respondentInfoStr = new StringBuilder();

                    respondentInfoStr.append("Respondent details:\n");

                    // iterate PerpersonalInfoMap
                    for (Map.Entry<String, Object> entry : personalInfoMap.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue().toString();
                        respondentInfoStr.append(key).append(": ").append(value).append("\n");
                    }

                    feedback = respondentInfoStr.toString() + "\n" + feedback;
                }
            }

            if (personalInfoMap != null) {
                //get 24/7 auth token and set it in the header
                Map<String, Object> authMap = softwareService.getConfigMap(businessId);
                String twoFortySeven_token = !authMap.isEmpty() && authMap.containsKey("token") ?
                        authMap.get("token").toString() : "";

                if (Utils.notEmptyString(twoFortySeven_token)) {

                    Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
                    Map<String, String> metadataMap = Utils.isNotNull(integrationsConfigMap.get("metadata")) ?
                            (Map<String, String>) integrationsConfigMap.get("metadata") : new HashMap();

                    String dateStr = triggerService.getDateByCloseDateConfig(new HashMap(), notifyTime, timeZone, false);
                    metadataMap.put("submitted", dateStr);

                    Map<String, Object> scheduleMap  = integrationMap.containsKey("schedule") ? integrationMap.get("schedule") : new HashMap();
                    boolean immediateSchedule = scheduleMap.containsKey("immediate") ? (boolean) scheduleMap.get("immediate") : false;
                    if(immediateSchedule) {
                        metadataMap.put("schedule", dateStr);
                    } else {
                        triggerService.setDateInMetaData(integrationMap, metadataMap, notifyTime, timeZone,
                                "schedule", "schedule", false);
                    }

                    Map<String, Object> reminderMap  = integrationMap.containsKey("reminder") ? integrationMap.get("reminder") : new HashMap();
                    boolean immediateReminder = reminderMap.containsKey("immediate") ? (boolean) reminderMap.get("immediate") : false;
                    if(immediateReminder) {
                        metadataMap.put("reminder", dateStr);
                    } else {
                        triggerService.setDateInMetaData(integrationMap, metadataMap, notifyTime, timeZone,
                                "reminder", "reminder", false);
                    }

                    Map<String, Object> requiredByMap  = integrationMap.containsKey("requiredBy") ? integrationMap.get("requiredBy") : new HashMap();
                    boolean immediateRequiredBy = requiredByMap.containsKey("immediate") ? (boolean) requiredByMap.get("immediate") : false;
                    if(immediateRequiredBy) {
                        metadataMap.put("requiredBy", dateStr);
                    } else {
                        triggerService.setDateInMetaData(integrationMap, metadataMap, notifyTime, timeZone,
                                "requiredBy", "requiredBy", false);
                    }

                    //get event source
                    String source = triggerDAO.getEventSourceByToken(surveyUUID, token);
                    //construct request to create incident in 24/7 software
                    TwoFortySevenRequestBean requestBean = softwareService.constructRequestBean(
                            twoFortySeven_token, metadataMap, requestTypeId, "WorkOrder", businessUniqueId,
                            facilityId, departmentId, "", locationId, sectionId, personalInfoMap, workOrderTypeId, userId, eventId);

                    if (Utils.notEmptyString(requestBean.getFacilityId()) && Utils.notEmptyString(requestBean.getDepartmentId()) &&
                            Utils.notEmptyString(requestBean.getWorkOrderId()) && Utils.notEmptyString(requestBean.getUserId()) ) {

                        //create work order in 24/7 software
                        Map resultMap = softwareService.createWorkOrderInTwoFortySevenSoftware(requestBean, twoFortySeven_token,
                                businessUniqueId, feedback, baseUrl);
                        //update status to sent once email sent successfully
                        if (resultMap.containsKey("success") && resultMap.get("success").toString().equalsIgnoreCase("true")) {
                            logger.info("update notification");
                            // To Do : udpate work order Id from 24/7 to DT table
                            //String softwareIncidentId = this.updateParticipantsWithSoftwareIncidentId(resultMap, respondentInfo);
                            //update sent status to completed in trigger_notification_{buuid} table
                            triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                            //Delete the notification in trigger_notification table by notifyId
                            triggerDAO.deleteTriggerNotificationById(notifyId);
                            //Need clarification : push the updated ticket details to insights
                            //this.pushTikcetDetailsToInsights(token, businessUniqueId, surveyUUID);
                            //update ticket details in event metadata
                            //triggerDAO.updateEventMetadataByToken(softwareIncidentId, token, surveyUUID, Constants.SOFTWARE_INCIDENT_ID);
                        } else{
                            triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
                        }
                    } else {
                        logger.error("Failed to create work order in 24/7 software");
                    }
                } else {
                    logger.error("24/7 software token not available for the business. Need to login and try again {}", businessUniqueId);
                    softwareService.createFailureData(baseUrl, token, "Token is empty. Need to login", businessUniqueId, "POST",
                            null);
                }
            } else {
                logger.info("Respondent info not available for the token {}", token);
            }

        } catch (Exception e) {
            logger.error("Exception at createWorkOrder. Msg {}", e.getMessage());
        }

    }

    /**
     * function to create request in 24/7 software
     * @param triggerNotification
     * @param integrationsConfigMap
     */
    private void createRequest(TriggerNotification triggerNotification, Map integrationsConfigMap) {
        try {
            int notifyId = triggerNotification.getNotifyId();
            String token = triggerNotification.getSubmissionToken();
            String channel = triggerNotification.getChannel();
            boolean addFeedback = triggerNotification.isAddFeedback();
            int triggerId = triggerNotification.getTriggerId();
            int businessId = triggerNotification.getBusinessId();

            String businessUniqueId = integrationsConfigMap.containsKey("businessUniqueId") ? (String) integrationsConfigMap.get("businessUniqueId") : "";
            String surveyUUID = integrationsConfigMap.containsKey("surveyUUID") ? integrationsConfigMap.get("surveyUUID").toString() : "";
            boolean isHippaEnabled = integrationsConfigMap.containsKey("isHippaEnabled") ? (boolean) integrationsConfigMap.get("isHippaEnabled") : false;

            String eventId = integrationsConfigMap.containsKey("eventId") ? integrationsConfigMap.get("eventId").toString() : "";
            String requestTypeId = integrationsConfigMap.containsKey("requestTypeId") ? integrationsConfigMap.get("requestTypeId").toString() : "";
            String facilityId = integrationsConfigMap.containsKey("facilityId") ? integrationsConfigMap.get("facilityId").toString() : "";
            String departmentId = integrationsConfigMap.containsKey("departmentId") ? integrationsConfigMap.get("departmentId").toString() : "";
            Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
//            String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
//            String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
            int createdBy = (int)triggerMap.get(Constants.CREATED_BY);

            Map respondentInfo = triggerService.getParticipantInfo(token, businessUniqueId); //respondent info
            Map personalInfoMap = this.getRespondentPersonalInfo(respondentInfo, isHippaEnabled); //respondent personal info

            //add feedback response in the ticket
            String feedback = "";
            /*if (addFeedback) {
                Map eventResponse = triggerService.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
                feedback = addFeedbackInTrigger(eventResponse);
            }*/

            //get customer id to create a ticket
            if (personalInfoMap != null) {

                //get 24/7 auth token and set it in the header
                Map<String, Object> authMap = softwareService.getConfigMap(businessId);
                String twoFortySeven_token = !authMap.isEmpty() && authMap.containsKey("token") ?
                        authMap.get("token").toString() : "";

                Map<String, String> metadataMap = Utils.isNotNull(integrationsConfigMap.get("metadata")) ?
                        (Map<String, String>) integrationsConfigMap.get("metadata") : new HashMap();
                //get event source
                String source = triggerDAO.getEventSourceByToken(surveyUUID, token);
            //construct request bean to create request in 24/7 software
                TwoFortySevenRequestBean requestBean = softwareService.constructRequestBean(twoFortySeven_token, metadataMap,
                        requestTypeId, "Request", businessUniqueId, facilityId, departmentId, "",
                        "", "", personalInfoMap, "", "", eventId);
                if (Utils.notEmptyString(requestBean.getFacilityId()) && Utils.notEmptyString(requestBean.getEventId()) &&
                        Utils.notEmptyString(requestBean.getDepartmentId()) && Utils.notEmptyString(requestBean.getRequestTypeId())){

                    //create request in 24/7 software
                    Map resultMap = softwareService.createRequestInTwoFortySevenSoftware(requestBean, token, businessUniqueId);
                    //update status to sent once email sent successfully
                    if (resultMap.containsKey("status") && Integer.parseInt(resultMap.get("status").toString()) == 200) {
                        logger.info("update notification");
                        // To Do : udpate incident Id from 24/7 to DT table
                        //String softwareIncidentId = this.updateParticipantsWithSoftwareIncidentId(resultMap, respondentInfo);
                        //update sent status to completed in trigger_notification_{buuid} table
                        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                        //Delete the notification in trigger_notification table by notifyId
                        triggerDAO.deleteTriggerNotificationById(notifyId);
                        //Need clarification : push the updated ticket details to insights
                        //this.pushTikcetDetailsToInsights(token, businessUniqueId, surveyUUID);
                        //update ticket details in event metadata
                        //triggerDAO.updateEventMetadataByToken(softwareIncidentId, token, surveyUUID, Constants.SOFTWARE_INCIDENT_ID);
                    } else{
                        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
                    }
                } else {
                    logger.error("Failed to create request in 24/7 software. FacilityId, EventId, DepartmentId, RequestTypeId are mandatory fields.");
                }
            } else {
                logger.info("Respondent info not available for the token {}", token);
            }

        } catch (Exception e) {
            logger.error("Exception at createRequest. Msg {}", e.getMessage());
        }
    }

    /**
     * function to create incident in 24/7 software
     *
     * @param triggerNotification
     * @param integrationsConfigMap
     * @param notifyTime
     * @param timeZone
     * @param surveyAnonymous
     */
    private void createIncident(TriggerNotification triggerNotification, Map integrationsConfigMap, String notifyTime,
                                String timeZone, boolean surveyAnonymous) {
        try {
            int notifyId = triggerNotification.getNotifyId();
            String token = triggerNotification.getSubmissionToken();
            String channel = triggerNotification.getChannel();
            boolean addFeedback = triggerNotification.isAddFeedback();
            int triggerId = triggerNotification.getTriggerId();
            int businessId = triggerNotification.getBusinessId();

            String businessUniqueId = integrationsConfigMap.containsKey("businessUniqueId") ? (String) integrationsConfigMap.get("businessUniqueId") : "";
            String surveyUUID = integrationsConfigMap.containsKey("surveyUUID") ? integrationsConfigMap.get("surveyUUID").toString() : "";
            boolean isHippaEnabled = integrationsConfigMap.containsKey("isHippaEnabled") ? (boolean) integrationsConfigMap.get("isHippaEnabled") : false;

            String eventId = integrationsConfigMap.containsKey("eventId") ? integrationsConfigMap.get("eventId").toString() : "";
            String facilityId = integrationsConfigMap.containsKey("facilityId") ? integrationsConfigMap.get("facilityId").toString() : "";
            String departmentId = integrationsConfigMap.containsKey("departmentId") ? integrationsConfigMap.get("departmentId").toString() : "";
            String incidentTypeId = integrationsConfigMap.containsKey("incidentTypeId") ? integrationsConfigMap.get("incidentTypeId").toString() : "";
            String locationId = integrationsConfigMap.containsKey("locationId") ? integrationsConfigMap.get("locationId").toString() : "";
            String sectionId = integrationsConfigMap.containsKey("sectionId") ? integrationsConfigMap.get("sectionId").toString() : "";
            Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
            String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
//            String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
            int createdBy = (int)triggerMap.get(Constants.CREATED_BY);


            Map respondentInfo = triggerService.getParticipantInfo(token, businessUniqueId); //respondent info
            Map<String, Object> personalInfoMap = this.getRespondentPersonalInfo(respondentInfo, isHippaEnabled); //respondent personal info

            //add feedback response in the ticket
            String feedback = "";
            if (addFeedback) {
                Map eventResponse = triggerService.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
                //DTV-13631 : modified feedback format in 24/7 software
                feedback = addFeedbackInTwentyFourBySeven(eventResponse, surveyName);
                if (!surveyAnonymous && !isHippaEnabled && personalInfoMap != null && !personalInfoMap.isEmpty()) {
                    StringBuilder respondentInfoStr = new StringBuilder();
                    respondentInfoStr.append("Respondent details: \n");
                    // iterate PerpersonalInfoMap
                    for (Map.Entry<String, Object> entry : personalInfoMap.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue().toString();
                        respondentInfoStr.append(key).append(": ").append(value).append("\n");
                    }
                    feedback = respondentInfoStr.toString() + "\n" + feedback;
                }
            }

            //get customer id to create a ticket
            if (personalInfoMap != null) {
                //get 24/7 auth token and set it in the header
                Map<String, Object> authMap = softwareService.getConfigMap(businessId);
                String twoFortySeven_token = !authMap.isEmpty() && authMap.containsKey("token") ?
                        authMap.get("token").toString() : "";

                String baseUrl = softwareUrl + "IncidentWrapper";
                //String baseUrl = softwareWebhookEndpoint;
                if (Utils.notEmptyString(twoFortySeven_token)) {

                    Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
                    //DTV-12559 : set incident date in metadata
                    Map<String, String> metadataMap = Utils.isNotNull(integrationsConfigMap.get("metadata")) ?
                            (Map<String, String>) integrationsConfigMap.get("metadata") : new HashMap();
                    triggerService.setDateInMetaData(integrationMap, metadataMap, notifyTime, timeZone,
                            "incidentDate", "incidentDate", true);
                    //get event source
                    String source = triggerDAO.getEventSourceByToken(surveyUUID, token);
                    //construct request to create incident in 24/7 software
                    TwoFortySevenRequestBean requestBean = softwareService.constructRequestBean(
                            twoFortySeven_token, metadataMap, "", "Incident", businessUniqueId,
                            facilityId, departmentId, incidentTypeId, locationId, sectionId, personalInfoMap, "", "", eventId);

                    if (Utils.notEmptyString(requestBean.getFacilityId()) && Utils.notEmptyString(requestBean.getDepartmentId()) &&
                            Utils.notEmptyString(requestBean.getIncidentTypeId())){

                        //create incident in 24/7 software
                        Map resultMap = softwareService.createIncidentInTwoFortySevenSoftware(requestBean, twoFortySeven_token,
                                businessUniqueId, feedback, baseUrl);
                        //update status to sent once email sent successfully
                        if (resultMap.containsKey("success") && resultMap.get("success").toString().equalsIgnoreCase("true")) {
                            logger.info("update notification");
                            // To Do : udpate incident Id from 24/7 to DT table
                            //String softwareIncidentId = this.updateParticipantsWithSoftwareIncidentId(resultMap, respondentInfo);
                            //update sent status to completed in trigger_notification_{buuid} table
                            triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                            //Delete the notification in trigger_notification table by notifyId
                            triggerDAO.deleteTriggerNotificationById(notifyId);
                            //Need clarification : push the updated ticket details to insights
                            //this.pushTikcetDetailsToInsights(token, businessUniqueId, surveyUUID);
                            //update ticket details in event metadata
                            //triggerDAO.updateEventMetadataByToken(softwareIncidentId, token, surveyUUID, Constants.SOFTWARE_INCIDENT_ID);
                        } else{
                            triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_FAILED, businessUniqueId);
                        }
                    } else {
                        logger.error("Failed to create incident in 24/7 software. FacilityId, EventId, DepartmentId, IncidentTypeId are mandatory fields.");
                    }
                } else {
                    logger.error("24/7 software token not available for the business. Need to login and try again {}", businessUniqueId);
                    softwareService.createFailureData(baseUrl, token, "Token is empty. Need to login", businessUniqueId, "POST",
                            null);
                }
            } else {
                logger.info("Respondent info not available for the token {}", token);
            }

        } catch (Exception e) {
            logger.error("Exception at createIncident. Msg {}", e.getMessage());
        }
    }

    /**
     * function to get respondent personal info
     * @param respondentInfo
     * @param isHippaEnabled
     * @return
     */
    private Map getRespondentPersonalInfo(Map respondentInfo, boolean isHippaEnabled){

        Map<String, Object> personalInfoMap = new HashMap();
        Iterator<String> itr = respondentInfo.keySet().iterator();
        while (itr.hasNext()) {
            String metaDataField =  itr.next();
            if (!isHippaEnabled) {
                String metaDataVal =  Utils.isNotNull(respondentInfo.get(metaDataField)) ? respondentInfo.get(metaDataField).toString() : "";
                int index = metaDataField.indexOf(Constants.DELIMITER);
                if(index > -1 && Utils.notEmptyString(metaDataVal)) {
                    String metaDataType = metaDataField.substring(index + 2, metaDataField.length()); //eg: Email~~EMAIL to EMAIL
                    String metaDataName = metaDataField.substring(0, index);
                    if (metaDataType.equalsIgnoreCase("NAME")  || metaDataType.equalsIgnoreCase("LINKNAME")) {
                        personalInfoMap.put("name", metaDataVal);
                    } else if (metaDataType.equalsIgnoreCase("EMAIL")) {
                        personalInfoMap.put("email", metaDataVal);
                    } else if (metaDataType.equalsIgnoreCase("PHONE")) {
                        personalInfoMap.put("phone", metaDataVal);
                    }else{
                        personalInfoMap.put(metaDataName, metaDataVal);
                    }
                }
            }
        }
        return personalInfoMap;
    }

    /**
     * function to add feedback in 24/7 software
     * @param eventMap
     * @return
     */
    public String addFeedbackInTwentyFourBySeven(Map eventMap, String surveyname) {

        StringBuilder feedbackStr = new StringBuilder();
        feedbackStr.append("Feedback received from the survey named ‘" + surveyname + "’\n");
        List responses = (List) eventMap.get("responses");
        List questionText = (List) eventMap.get("questions");
        Iterator itrResponse = responses.iterator();
        Iterator itrQuestion = questionText.iterator();
        int cnt = 1;
        while (itrResponse.hasNext()) {
            String response = (String) itrResponse.next();
            String question = (String) itrQuestion.next();
            //DTV-13654 removed HTML tags from the question text
            question = Jsoup.parse(question).text();
            feedbackStr.append("Q"+cnt+": ");
            feedbackStr.append(question).append("\n");
            feedbackStr.append("A"+cnt+": ");
            feedbackStr.append(Utils.emptyString(response) ? "NA" : response).append("\n");
            cnt++;
        }
        return feedbackStr.toString();
    }
}
