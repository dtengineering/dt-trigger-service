package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.dao.TriggerTemplateDAO;
import com.dropthought.trigger.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class TriggerService {

    @Autowired
    TriggerDAO triggerDAO;

    @Autowired
    TriggerTemplateDAO triggerTemplateDAO;

    @Autowired
    TriggerTemplateService triggerTemplateService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    DTWorksService dtWorksService;

    @Autowired
    @Qualifier("asyncExecutor")
    private Executor executor;


    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${deeplink.trigger}")
    private String deeplinkTrigger;
    @Value("${app.name}")
    private String appName;
    @Value("${notification.from.email}")
    private String fromEmail;
    @Value("${fcm}")
    private String fcmKey;
    @Value("${dt.pushnotification.url}")
    private String pushNotificationUrl;
    @Value("${dt.email.url}")
    private String emailNotificationUrl;
    @Value("${dt.eliza.url}")
    private String elizaUrl;
    @Value("${dt.portal.usage.url}")
    private String portalUsageUrl;
    @Value("${dtp.domain}")
    private String dtpDomain;

    @Value("${onprem}")
    private boolean onprem;
    @Value("${notification.from.name}")
    private String fromName;

    @Value("${dt.eliza.summary.url}")
    private String elizaSummaryUrl;


     /* @Autowired
    public ScheduleRunner(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.topicExchange = topicExchange;
    }*/

    /**
     * method to create trigger by passing trigger bean
     *
     * @param bean
     */
    public Map createTrigger(Trigger bean) {

        Map result = new HashMap();
        boolean isTriggerTableExist = true;
        Map triggersUsageMap = new HashMap();
        // check for trigger table, if not exists create one
        if (!triggerDAO.isTriggerTableExists(bean.getBusinessUniqueId())) {
            // create trigger table for given business unique id
            triggerDAO.createTriggerTable(bean.getBusinessUniqueId());
            isTriggerTableExist = false;
        }

        int surveyId = triggerDAO.getSurveyIdByUUID(bean.getSurveyId(), bean.getBusinessUniqueId());
        // check if trigger name already exists
        if (isTriggerTableExist && triggerDAO.isTriggerNameExists(bean.getTriggerName(), bean.getBusinessUniqueId(), bean.getUserId(), surveyId)) {
            // assign and return error response
            result.put(Constants.SUCCESS, false);
            result.put(Constants.MESSAGE, Constants.TRIGGER_NAME_EXIST);
        } else {
            int userId = triggerDAO.getUserIdByUUID(bean.getUserId());
            //int surveyId = this.getSurveyIdByUUID(trigger.getSurveyId(), trigger.getBusinessUniqueId());
            int roleId = triggerDAO.getRoleIdByUserId(userId);
            // create a new entry for trigger conditions
            String triggerUniqueId = triggerDAO.createTrigger(bean, surveyId, userId, roleId);
            //srping boot upgrade - circular dependency changes
            if (triggerUniqueId.length() > 0) {
                bean.setTriggerId(triggerUniqueId);
                bean.setTriggerFilterUUID("");
                //DTV-11303 - spring boot upgrade removing circular dependencies
                createDynamicFilterFromTriggerCondition(bean, userId, roleId, surveyId);
            }
            // assign success / failure response based on the intcount
            result.put(Constants.SUCCESS, triggerUniqueId.length() > 0 ? true : false);
            result.put(Constants.RESULT, triggerUniqueId);
        }
        return result;
    }

    /**
     * method to update trigger by passing trigger bean and primary Id
     *
     * @param bean
     * @param triggerId
     */
    public int updateTrigger(Trigger bean, String triggerId) {
        // get survey id for given survey unique id
        //CSUP-1657, static variables to hold start & end dates to calculate execution start & end times
        final String startDate = bean.getTriggerStartDate();
        final String startTime = bean.getTriggerStartTime();
        final String endDate = bean.getTriggerEndDate();
        final String endTime = bean.getTriggerEndTime();

        int surveyPrimaryId = triggerDAO.getSurveyIdByUUID(bean.getSurveyId(), bean.getBusinessUniqueId());
        bean.setSurveyId(String.valueOf(surveyPrimaryId));
        int result = updateTriggerById(bean, triggerId);
        // get business primary id by uniqueId
        int businessId = triggerDAO.getBusinessIdByUUID(bean.getBusinessUniqueId());
        // get trigger details by trigger uuid
        Map triggerMap = triggerDAO.getTriggerMapByUUId(bean.getBusinessUniqueId(), triggerId);
        if (Utils.isNotNull(triggerMap) && triggerMap.containsKey(Constants.ID) && businessId > 0) {
            // assign trigger primary id
            int triggerIdInt = triggerMap.containsKey(Constants.ID) ? (int) (triggerMap.get(Constants.ID)) : 0;

            String executionStartTime = "";
            String executionEndTime = "";

            /** DTV-3636, if trigger startdate, starttime, enddate, endtime is empty from UI
             * start date will be current date
             * end date will be adjusted to survey end date
             * CSUP-1657 commenting below part as startDateTime & endDateTIme will be updated in bean during update
              */

/*
            if(Utils.emptyString(bean.getTriggerStartDate()) && Utils.emptyString(bean.getTriggerStartTime()) && Utils.emptyString(bean.getTriggerEndDate()) && Utils.emptyString(bean.getTriggerEndTime())){

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                String currentDateString = simpleDateFormat.format(timestamp);
                String surveyEndDate = triggerMap.containsKey(Constants.TO_DATE) ? (triggerMap.get(Constants.TO_DATE).toString()) : "";


                String utcStartTime = Utils.getUTCDateStringFromDateTime(currentDateString.substring(0, 10), "00:00:00", bean.getTimezone());
                startDate = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(0, 10) : "";
                startTime = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(11, 19) : "";

                String utcEndTime = Utils.getUTCDateStringFromDateTime(surveyEndDate.substring(0, 10), "23:59:59", bean.getTimezone());
                endDate = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(0, 10) : "";
                endTime = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(11, 19) : "";

            }else {

                String utcStartTime = Utils.notEmptyString(bean.getTriggerStartDate()) ? Utils.getUTCDateStringFromDateTime(bean.getTriggerStartDate(), bean.getTriggerStartTime(), bean.getTimezone()) : "";
                startDate = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(0, 10) : "";
                startTime = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(11, 19) : "";

                String utcEndTime = Utils.notEmptyString(bean.getTriggerEndDate()) ? Utils.getUTCDateStringFromDateTime(bean.getTriggerEndDate(), bean.getTriggerEndTime(), bean.getTimezone()) : "";
                endDate = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(0, 10) : "";
                endTime = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(11, 19) : "";
            }
*/

            /**
             * CSUP-1657 -> Adjusting start & end times to DST (day light saving time) for other timezones only for execution start & end time
             */
            String surveyEndDate = triggerMap.containsKey(Constants.TO_DATE) ? (triggerMap.get(Constants.TO_DATE).toString()) : Constants.EMPTY;
            //get execution start & end dates to update all the schedules for the given trigger
            Map resultMap = getExecutionStartEndTime(startDate, endDate,
                    startTime, endTime, surveyEndDate, bean.getTimezone());

            executionStartTime = resultMap.get(Constants.EXEC_START_TIME).toString();
            executionEndTime = resultMap.get(Constants.EXEC_END_TIME).toString();

            triggerDAO.updateScheduleValuesByTriggerId(businessId, surveyPrimaryId, triggerIdInt, bean.getStatus(),
                    bean.getTriggerStartDate(), bean.getTriggerStartTime(), bean.getTriggerEndDate(), bean.getTriggerEndTime(),
                    bean.getFrequency(), executionStartTime, executionEndTime);

            //DTV-5331 --> Adding triggerStatus flag to check trigger is in active or inactive state
            //When trigger status is inactive, updating trigger_status = '0' for active trigger notifications
            int updateStatus = triggerDAO.updateTriggerNoficiationTriggerStatus(triggerId, bean.getBusinessUniqueId(), bean.getStatus());

        }
        return result;
    }

    /**
     * method to update trigger by passing triggerId and state
     *
     * @param
     */
    public Map updateTriggerStatus(String businesUniqueId, String triggerId, String state) {
        Map resultMap = new HashMap();
        int result = 0;
        boolean activeTriggersLimitReached = false;
        // get business primaryid by uniqueId
        int businessId = triggerDAO.getBusinessIdByUUID(businesUniqueId);
        // get trigger details by triggeruuid
        Map triggerMap = triggerDAO.getTriggerMapByUUId(businesUniqueId, triggerId);
        if (Utils.isNotNull(triggerMap) && triggerMap.containsKey(Constants.SURVEY_ID)) {

            /**
             * while changing inactive trigger to active, check for triggers limit usage
             */
            Map triggersUsageMap = new HashMap();
            int userId = triggerMap.containsKey(Constants.CREATED_BY) ? Integer.parseInt(triggerMap.get(Constants.CREATED_BY).toString()) : 0;
            if(state.equalsIgnoreCase("active") && userId > 0){
                triggersUsageMap = this.checkTriggersUsageLimit(businesUniqueId, userId);
                activeTriggersLimitReached = triggersUsageMap.containsKey("limitReached") && Utils.isNotNull(triggersUsageMap.get("limitReached")) ? (Boolean) triggersUsageMap.get("limitReached") : false;
            }

            //triggers limit not reached
            if(!activeTriggersLimitReached) {
                // assign survey id
                int surveyId = triggerMap.containsKey(Constants.SURVEY_ID) ? Integer.parseInt(triggerMap.get(Constants.SURVEY_ID).toString()) : 0;
                // assign trigger primary id
                int triggerIdInt = triggerMap.containsKey(Constants.ID) ? (int) (triggerMap.get(Constants.ID)) : 0;
                if (triggerDAO.getScheduledIdIfExists(businessId, surveyId, triggerIdInt)) {
                    // update trigger condition status
                    result = triggerDAO.updateTriggerStatusByTriggerId(businesUniqueId, triggerId, state);
                    if (result > 0) {
                        /**adding usage count to business usage table
                         * status is active means count = 1
                         * status is active means count = -1
                         */
                        int addCount = state.equalsIgnoreCase(Constants.STATUS_ACTIVE) ? 1 : -1;
                        this.updateTriggersUsageCount(businessId, addCount);
                    }
                    // update trigger schedule by using business, survey and trigger primary ids
                    result = triggerDAO.updateScheduleStatusById(businessId, surveyId, triggerIdInt, state);

                    //DTV-5331 --> Adding triggerStatus flag to check trigger is in active or inactive state
                    //When trigger status is inactive, updating trigger_status = '0' for active trigger notifications
                    int updateStatus = triggerDAO.updateTriggerNoficiationTriggerStatus(triggerId, businesUniqueId, state);

                    /**
                     * Updating Trigger schedules exec_start_time & exec_end_time after activating trigger to avoid delay in sending trigger notifications
                     * Job will be there in runner it will update exec_start_time & exec_end_time every 30 mins for active schedules
                     */
                    if(state.equalsIgnoreCase("active"))
                        CompletableFuture.runAsync(() -> this.updateTriggerScheduleExecStartEndTimeByTriggerId(businesUniqueId, triggerId), executor);

                    resultMap.put(Constants.SUCCESS, true);
                } else {
                    //result = -2; // scheduler details not exists
                    //Only for sprint 19 for testing purpose.
                    //Needs to be commented out
                    // update trigger condition status
                    result = triggerDAO.updateTriggerStatusByTriggerId(businesUniqueId, triggerId, state);
                    if (result > 0) {
                        /**adding usage count to business usage table
                         * status is active means count = 1
                         * status is active means count = -1
                         */
                        int addCount = state.equalsIgnoreCase(Constants.STATUS_ACTIVE) ? 1 : -1;
                        this.updateTriggersUsageCount(businessId, addCount);
                    }
                }
            }else {
                logger.info("active triggers usage limit reached");
                return triggersUsageMap;
            }
        }


        if(result == 0){
            resultMap.put(Constants.SUCCESS, true);
            resultMap.put(Constants.MESSAGE, Constants.NOT_FOUND);
        }else if( result == -2){
            resultMap.put(Constants.SUCCESS, false);
            resultMap.put(Constants.MESSAGE, "Scheduler details not available");
        }
        return resultMap;
    }

    /**
     * method to delete trigger by passing triggerId
     *
     * @param triggerId
     * @param businessUniqueId
     */
    public int deleteTrigger(String triggerId, String businessUniqueId) {
        int result = 0;
        try {
            int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
            result = triggerDAO.deletTrigger(businessUniqueId, triggerId, businessId);
            //DTV-11303 - spring boot upgrade removing circular dependencies
            if (result > 0) {
                this.updateTriggersUsageCount(businessId, -1);
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return result;
    }


    /**
     * method to get all triggers details by using filter conditions like sorting, searching, viewing
     *
     * @param businessUniqueId
     * @param sortBy
     * @param viewBy
     * @param state
     * @param search
     * @param isMatch
     * @param userIdStr
     * @return
     */
    public Map getAllTriggersByBusiness(String businessUniqueId, String sortBy, String viewBy, String state, String search, boolean isMatch, String userIdStr, int pageNo, String timezone) {
        int userId = triggerDAO.getUserIdByUUID(userIdStr);
        List<Trigger> resultList = new ArrayList<>();
        List triggerList = triggerDAO.getTriggers(businessUniqueId, sortBy, viewBy, state, search, isMatch, userId, pageNo);
        int roleId = triggerDAO.getRoleIdByUserId(userId);
        Iterator iterator = triggerList.iterator();
        while (iterator.hasNext()) {
            try {
                Map eachMap = (Map) iterator.next();
                Trigger eachTrigger = new Trigger();
                eachTrigger.setSurveyName(eachMap.get(Constants.SURVEY_NAME).toString());
                //DTV-8132 : If survey is deactivated, then trigger should be deactivated (status = 2)
                eachTrigger.setStatus((((int) eachMap.get(Constants.STATUS)) == 0 || ((int) eachMap.get(Constants.STATUS)) == 2)  ? Constants.STATUS_INACTIVE : Constants.STATUS_ACTIVE);
                eachTrigger.setSurveyId(triggerDAO.getSurveyUniqueIdById(Integer.parseInt(eachMap.get(Constants.SURVEY_ID).toString()), businessUniqueId));
                eachTrigger.setTriggerName(eachMap.get(Constants.TRIGGER_NAME).toString());
                eachTrigger.setTriggerCondition(Utils.isNotNull(eachMap.get(Constants.TRIGGER_CONDITION)) ? eachMap.get(Constants.TRIGGER_CONDITION).toString() : Constants.EMPTY);
                eachTrigger.setLastModified(Utils.isNotNull(eachMap.get(Constants.MODIFIED_TIME)) ? Utils.getLocalDateTimeByTimezone(eachMap.get(Constants.MODIFIED_TIME).toString(), timezone) : Constants.EMPTY);
                eachTrigger.setBusinessUniqueId(businessUniqueId);
                eachTrigger.setTriggerId(eachMap.get(Constants.TRIGGER_UUID).toString());
                eachTrigger.setSurveyStartDate(Utils.getLocalDateTimeByTimezone(eachMap.get(Constants.FROM_DATE).toString(), timezone));
                eachTrigger.setSurveyEndDate(Utils.getLocalDateTimeByTimezone(eachMap.get(Constants.TO_DATE).toString(), timezone));
                /**
                 * Anonymous value of the program
                 */
                int eachSurveyAnonymousValue = (Utils.isNotNull(eachMap.get(Constants.ANONYMOUS)) && Utils.isStringNumber(eachMap.get(Constants.ANONYMOUS).toString())) ? Integer.parseInt(eachMap.get(Constants.ANONYMOUS).toString()) : 0;
                eachTrigger.setAnonymous( (eachSurveyAnonymousValue==1) ? true: false);

                // get start and end date from schedule table
                //Map triggerScheduleDates = triggerDAO.getTriggerScheduleDate(eachTrigger.getTriggerId(), businessUniqueId);

                String startDate = "";
                String startTime = "";
                String endDate = "";
                String endTime = "";
                if((eachMap.containsKey(Constants.START_DATE) && Utils.isNotNull(eachMap.get(Constants.START_DATE))) && (eachMap.containsKey(Constants.START_TIME) && (Utils.isNotNull(eachMap.get(Constants.START_TIME))))){

                    String convertedStartDate = Utils.getLocalDateStringFromUTCDateAndTime(eachMap.get(Constants.START_DATE).toString(), eachMap.get(Constants.START_TIME).toString(), timezone);
                    startDate = convertedStartDate.substring(0,10);
                    startTime = convertedStartDate.substring(11,19);
                    eachTrigger.setTriggerStartDate(startDate);
                    eachTrigger.setTriggerStartTime(startTime);
                }else{
                    eachTrigger.setTriggerStartDate(Constants.EMPTY);
                    eachTrigger.setTriggerStartTime(Constants.EMPTY);
                }

                if((eachMap.containsKey(Constants.END_DATE) && Utils.isNotNull(eachMap.get(Constants.END_DATE))) && (eachMap.containsKey(Constants.END_TIME) && (Utils.isNotNull(eachMap.get(Constants.END_TIME))))){

                    String convertedEndDate = Utils.getLocalDateStringFromUTCDateAndTime(eachMap.get(Constants.END_DATE).toString(), eachMap.get(Constants.END_TIME).toString(), timezone);
                    endDate = convertedEndDate.substring(0,10);
                    endTime = convertedEndDate.substring(11,19);
                    eachTrigger.setTriggerEndDate(endDate);
                    eachTrigger.setTriggerEndTime(endTime);
                }else{
                    eachTrigger.setTriggerEndDate(Constants.EMPTY);
                    eachTrigger.setTriggerEndTime(Constants.EMPTY);
                }


                /*eachTrigger.setTriggerStartDate((eachMap.containsKey(Constants.START_DATE) && Utils.isNotNull(eachMap.get(Constants.START_DATE)) )? Utils.getLocalDateStringFromUTC(eachMap.get(Constants.START_DATE).toString(), timezone) : Constants.EMPTY);
                eachTrigger.setTriggerEndDate((eachMap.containsKey(Constants.END_DATE) && Utils.isNotNull(eachMap.get(Constants.END_DATE))) ? Utils.getLocalDateStringFromUTC(eachMap.get(Constants.END_DATE).toString(), timezone) : Constants.EMPTY);
                eachTrigger.setTriggerStartTime((eachMap.containsKey(Constants.START_TIME) && Utils.isNotNull(eachMap.get(Constants.START_TIME))) ? Utils.getLocalTimeStringFromUTC(eachMap.get(Constants.START_TIME).toString(), timezone) : Constants.EMPTY);
                eachTrigger.setTriggerEndTime((eachMap.containsKey(Constants.END_TIME) && Utils.isNotNull(eachMap.get(Constants.START_TIME))) ? Utils.getLocalTimeStringFromUTC(eachMap.get(Constants.END_TIME).toString(), timezone) : Constants.EMPTY);*/


                eachTrigger.setFrequency((eachMap.containsKey(Constants.FREQUENCY) && Utils.isNotNull(eachMap.get(Constants.FREQUENCY))) ? eachMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
                //setting triggerParentCondition for read/read-write users
                if(roleId != 1 && roleId != 4){
                    Map userSurveyFilterMap = triggerDAO.getMappedSurveyFiltersByUserId(userId, Integer.parseInt(eachMap.get(Constants.SURVEY_ID).toString()) , businessUniqueId);
                    if(userSurveyFilterMap.size() > 0){
                        String filterQuery = Utils.isNotNull(userSurveyFilterMap.get("filter_query")) ? userSurveyFilterMap.get("filter_query").toString() : "";
                        String modifiedQuery = triggerDAO.adjustFilterQuery(filterQuery);
                        eachTrigger.setTriggerParentCondition(modifiedQuery);

                        eachTrigger.setSurveyFilterId(Utils.isNotNull(userSurveyFilterMap.get("survey_filter_uuid")) ? userSurveyFilterMap.get("survey_filter_uuid").toString() : "");
                    }
                }
                resultList.add(eachTrigger);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        int totalRecordsCount = triggerDAO.getTotalTriggersCountByCondition(businessUniqueId, sortBy, viewBy, state, search, isMatch, userId);

        Map result = new HashMap();
        result.put(Constants.RESULT, resultList);
        result.put(Constants.TOTAL_RECORDS, totalRecordsCount);
        result.put(Constants.ALL_RECORDS, triggerDAO.getTotalTriggersCountByCondition(businessUniqueId, sortBy, "", Constants.ALL, "", false, userId));
        result.put(Constants.TOTAL_PAGES, Constants.TOTAL_RECORDS.length() > 0 ? Utils.computeTotalPages(totalRecordsCount) : 0);
        result.put(Constants.CURRENT_PAGE_RECORDS, triggerList.size());
        result.put(Constants.SUCCESS, true);
        return result;
    }

    /**
     * method to create schedule by passing schedule bean
     *
     * @param bean
     */
    public Map createTriggerSchedule(TriggerSchedule bean) {

        // get business primary id
        TriggerSchedule scheduleCopy = bean.deepCopy();
        int businessId = triggerDAO.getBusinessIdByUUID(bean.getBusinessId());
        String businesssUniqueId = bean.getBusinessId();
        // get surveyId, triggerid and assign it to request bean
        Map trigger = triggerDAO.getTriggerMapByUUId(bean.getBusinessId(), bean.getTriggerId());

        /**
         * Retrieving the start and end dates from trigger_{business_uuid}.SE-782, CSUP-498.
         * Dates are not modified by the schedule. To prevent browser issues obtaining the dates from the DB
         */

        bean.setStartDate( (trigger.containsKey("start_date") && Utils.isNotNull(trigger.get("start_date"))) ? trigger.get("start_date").toString() :bean.getStartDate() );
        bean.setStartTime((trigger.containsKey("start_time") && Utils.isNotNull(trigger.get("start_time"))) ? trigger.get("start_time").toString() :bean.getStartTime() );
        bean.setEndDate((trigger.containsKey("end_date") && Utils.isNotNull(trigger.get("end_date"))) ? trigger.get("end_date").toString() :bean.getEndDate() );
        bean.setEndTime((trigger.containsKey("end_time") && Utils.isNotNull(trigger.get("end_time"))) ? trigger.get("end_time").toString() :bean.getEndTime() );



        bean.setSurveyId(trigger.get(Constants.SURVEY_ID).toString());
        bean.setTriggerId(trigger.get(Constants.ID).toString());
        bean.setBusinessId(String.valueOf(businessId));


        String channel = bean.getChannel();

        if(channel.equalsIgnoreCase(Constants.LIST) && Utils.isNotNull(bean.getHeaderData())){
            // create a target list name when the channel is list
            TriggerListHeaderData headerData = bean.getHeaderData();
            int createdBy = (int)trigger.get(Constants.CREATED_BY);
            //if(!triggerDAO.checkIfTagNameExists(headerData.getName(), businesssUniqueId)) {
            if(true) {
                logger.info("creating the list");
                // create an entry in tag and participant group table
                int[] resultArr = triggerDAO.createParticipantGroupByBusinessUUID(headerData.getName(), businesssUniqueId, createdBy);

                int participantGroupId = resultArr.length> 0 && Utils.isNotNull(resultArr[0])? resultArr[0] : 0;
                int tagId = resultArr.length> 1 && Utils.isNotNull(resultArr[1])? resultArr[1] : 0;
                //DTV-11303
                if(tagId > 0 ) {
                    this.updateTriggerListUsageCount(businesssUniqueId, 1);
                }
                // get participant groupuuid for that particpantgroupId
                String participantGroupUUID = triggerDAO.getParticipantGroupUUIDById(participantGroupId, businesssUniqueId);
                // create participant table for participant group uid.
                triggerDAO.createParticipantTable(participantGroupUUID);
                bean.setParticipantGroupId(participantGroupId);

            }else{
                logger.info("list name exists already");
                Map result = new HashMap();
                result.put(Constants.SUCCESS, false);
                result.put(Constants.ERROR, "List name already exists");
                return result;
            }
        }
        else if(bean.getChannel().equalsIgnoreCase("reply") && Utils.emptyString(bean.getReplyToRespondent())){
            //If reply to respondent is empty set up the default Dropthought contact information
            bean.setReplyToRespondent("");
        }

        //DTV-5309, fix for inactive surveys schedules
        boolean triggerStatus = trigger.containsKey("status") && Utils.isNotNull(trigger.get("status")) && (Integer.parseInt(trigger.get("status").toString()) == 1);
        if(triggerStatus){
            //trigger is in active state, update create schedule bean with active state
            bean.setStatus(Constants.STATUS_ACTIVE);
        }
        // create a new entry for trigger schedule
        String scheduleUUID = triggerDAO.createTriggerSchedule(bean);


        /**
         * CSUP-1657 -> Adjusting start & end times to DST (day light saving time) for other timezones only for execution start & end time
         */
        String surveyEndDate = trigger.containsKey(Constants.TO_DATE) ? (trigger.get(Constants.TO_DATE).toString()) : Constants.EMPTY;
        Map datesMap = getExecutionStartEndTime(scheduleCopy.getStartDate(), scheduleCopy.getEndDate(), scheduleCopy.getStartTime(), scheduleCopy.getEndTime(),
                surveyEndDate, scheduleCopy.getTimezone());
        String executionStartTime = datesMap.get(Constants.EXEC_START_TIME).toString();
        String executionEndTime = datesMap.get(Constants.EXEC_END_TIME).toString();

        /**
         * update the execution time based upon the schedule uuid
         */
        updateExecTimeByUUID(executionStartTime,executionEndTime, scheduleUUID);

        // assign success / failure response based on the intcount
        Map result = new HashMap();
        result.put(Constants.SUCCESS, !scheduleUUID.isEmpty());
        result.put(Constants.RESULT, scheduleUUID);
        return result;
    }

    /**
     * method to update schedule by passing schedule bean and primary Id
     *
     * @param bean
     * @param scheduleId
     */
    public int updateTriggerSchedule(TriggerSchedule bean, String scheduleId) {
        /**
         * identify execution start and end times
         */

        TriggerSchedule scheduleCopy = bean.deepCopy();
        String triggerUUID = bean.getTriggerId();
        String businessUUID = bean.getBusinessId();
        Map triggerMap = triggerDAO.getTriggerMapByUUId(businessUUID, triggerUUID);

        /**
         * Value obtained from DB to prevent browser issues in UI.
         * CSUP-1657 - if values are coming from UI we should use UI values.
         */
        String startDate = (triggerMap.containsKey("start_date") && Utils.isNotNull(triggerMap.get("start_date"))) ? triggerMap.get("start_date").toString() : "";
        String startTime = (triggerMap.containsKey("start_time") && Utils.isNotNull(triggerMap.get("start_time"))) ? triggerMap.get("start_time").toString() : "";
        String endDate = (triggerMap.containsKey("end_date") && Utils.isNotNull(triggerMap.get("end_date"))) ? triggerMap.get("end_date").toString() : "";
        String endTime = (triggerMap.containsKey("end_time") && Utils.isNotNull(triggerMap.get("end_time"))) ? triggerMap.get("end_time").toString() : "";


        /**
         * if values are empty from db, use the values from UI to get the start date, time
         */
        if(Utils.emptyString(startDate) || Utils.emptyString(endDate)){
            String utcStartTime = Utils.getUTCDateStringFromDateTime(bean.getStartDate(), bean.getStartTime(), bean.getTimezone());
            startDate = utcStartTime.substring(0,10);
            startTime = utcStartTime.substring(11, 19);
            //To prevent adjusting to utc in update trigger schedule
            bean.setStartDate(startDate);
            bean.setStartTime(startTime);

            String utcEndTime = Utils.getUTCDateStringFromDateTime(bean.getEndDate(), bean.getEndTime(), bean.getTimezone());
            endDate = utcEndTime.substring(0,10);
            endTime = utcEndTime.substring(11, 19);
            //To prevent adjusting to utc in update trigger schedule
            bean.setEndDate(endDate);
            bean.setEndTime(endTime);

        }

        String surveyEndDate = triggerMap.containsKey(Constants.TO_DATE) ? (triggerMap.get(Constants.TO_DATE).toString()) : Constants.EMPTY;
        Map datesMap = getExecutionStartEndTime(scheduleCopy.getStartDate(), scheduleCopy.getEndDate(), scheduleCopy.getStartTime(), scheduleCopy.getEndTime(),
                surveyEndDate, scheduleCopy.getTimezone());

        String executionStartTime = datesMap.get(Constants.EXEC_START_TIME).toString();
        String executionEndTime = datesMap.get(Constants.EXEC_END_TIME).toString();
        /**
         * update the execution time based upon the schedule uuid
         */
        updateExecTimeByUUID(executionStartTime,executionEndTime,scheduleId);
        try {
            if (bean.getChannel().equalsIgnoreCase(Constants.LIST) && Utils.isNotNull(bean.getHeaderData())) {
                TriggerListHeaderData header = bean.getHeaderData();
                // udate list name
                triggerDAO.UpdateTagListNameByParticipantGroupId(bean.getParticipantGroupId(),bean.getBusinessId(), header.getName());
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }

        return triggerDAO.updateTriggerSchedule(bean, scheduleId);
    }

    /**
     * method to delete trigger schedule by passing triggerId
     *
     * @param scheduleId
     */
    public int deleteTriggerSchedule(String scheduleId) {
        return triggerDAO.deleteTriggerSchedule(scheduleId);
    }

    /**
     * method to get trigger schedule details by passing triggerId
     *
     * @param scheduleId
     */
    public TriggerSchedule getTriggerScheduleById(String scheduleId, String timezone) {
        TriggerSchedule schedule = new TriggerSchedule();
        timezone = Utils.notEmptyString(timezone) ? timezone : Constants.TIMEZONE_AMERICA_LOSANGELES;
        try {
            Map scheduleMap = triggerDAO.getTriggerScheduleByUniqueID(scheduleId);
            if (Utils.isNotNull(scheduleMap)) {
                schedule.setStartDate(Utils.isNotNull(scheduleMap.get(Constants.START_DATE)) ? Utils.getLocalDateStringFromUTC(scheduleMap.get(Constants.START_DATE).toString(), timezone) : null);
                schedule.setStatus((String) scheduleMap.get(Constants.STATUS));
                schedule.setEndDate(Utils.isNotNull(scheduleMap.get(Constants.END_DATE)) ? Utils.getLocalDateStringFromUTC(scheduleMap.get(Constants.END_DATE).toString(), timezone) : null);
                schedule.setStartTime(Utils.isNotNull(scheduleMap.get(Constants.START_TIME)) ? Utils.getLocalTimeStringFromUTC(scheduleMap.get(Constants.START_TIME).toString(), timezone) : null);
                schedule.setEndTime(Utils.isNotNull(scheduleMap.get(Constants.END_TIME)) ? Utils.getLocalTimeStringFromUTC(scheduleMap.get(Constants.END_TIME).toString(), timezone) : null);
                schedule.setFrequency(Utils.isNotNull(scheduleMap.get(Constants.FREQUENCY)) ? scheduleMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
                schedule.setScheduleId(scheduleMap.get(Constants.SCHEDULE_UUID).toString());
                schedule.setTimezone(scheduleMap.get(Constants.TIMEZONE).toString());
                schedule.setChannel(Utils.isNotNull(scheduleMap.get(Constants.CHANNEL)) ? scheduleMap.get(Constants.CHANNEL).toString() : Constants.EMPTY);
                schedule.setMessage(Utils.isNotNull(scheduleMap.get(Constants.MESSAGE)) ? scheduleMap.get(Constants.MESSAGE).toString() : Constants.EMPTY);
                schedule.setSubject(Utils.isNotNull(scheduleMap.get(Constants.SUBJECT)) ? scheduleMap.get(Constants.SUBJECT).toString() : Constants.EMPTY);
                schedule.setContacts(Utils.isNotNull(scheduleMap.get(Constants.CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                schedule.setDeAcitvatedContacts(Utils.isNotNull(scheduleMap.get(Constants.DEACTIVATED_CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.DEACTIVATED_CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                schedule.setAddFeedback((int) (scheduleMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                schedule.setTriggerConfig(Utils.isNotNull(scheduleMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig());
                schedule.setParticipantGroupId(Utils.isNotNull(scheduleMap.get(Constants.PARTICIPANT_GROUP_ID)) ? (int)scheduleMap.get(Constants.PARTICIPANT_GROUP_ID) : 0);
                schedule.setHeaderData(Utils.isNotNull(scheduleMap.get(Constants.HEADER_DATA)) ? mapper.readValue( scheduleMap.get(Constants.HEADER_DATA).toString(), TriggerListHeaderData.class) : null);
                schedule.setWebhookConfig(Utils.isNotNull(scheduleMap.get(Constants.WEBHOOK_CONFIG)) ? mapper.readValue( scheduleMap.get(Constants.WEBHOOK_CONFIG).toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData());
                schedule.setIntegrationsConfig(Utils.isNotNull(scheduleMap.get(Constants.INTEGRATIONS_CONFIG)) ? mapper.readValue( scheduleMap.get(Constants.INTEGRATIONS_CONFIG).toString(), HashMap.class) : new HashMap());
                schedule.setReplyToRespondent(Utils.isNotNull(scheduleMap.get(Constants.REPLYRESPONDENT)) ? scheduleMap.get(Constants.REPLYRESPONDENT).toString() : Constants.EMPTY);
                //schedule.setDeactivatedConditions(Utils.isNotNull(scheduleMap.get(Constants.DEACTIVATED_CONDITIONS)) ? mapper.readValue(scheduleMap.get(Constants.DEACTIVATED_CONDITIONS).toString(), ArrayList.class) : new ArrayList<>());
                //DTV-9132 - set trigger action message
                if(schedule.getChannel().equalsIgnoreCase(Constants.CHANNEL_FOCUS_AREA)){
                    int businessId = scheduleMap.containsKey(Constants.BUSINESS_ID) && Utils.isNotNull(scheduleMap.get(Constants.BUSINESS_ID)) ? (int) scheduleMap.get(Constants.BUSINESS_ID) : 0;
                    String actionMessage = businessId > 0 ? this.getFocusAreaActionMessage(businessId, schedule.getIntegrationsConfig()): "";
                    schedule.setActionMessage(actionMessage);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return schedule;
    }

    /**
     * method to get trigger schedule details by passing triggerId
     *
     * @param scheduleId
     */
    public List<TriggerSchedule> getTriggerScheduleByTriggerId(String scheduleId, String timezone, String businessUniqueId) {
        List<TriggerSchedule> triggerScheduleList = new ArrayList<>();
        timezone = Utils.notEmptyString(timezone) ? timezone : Constants.TIMEZONE_AMERICA_LOSANGELES;
        try {
            List scheduleList = triggerDAO.getTriggerScheduleByTriggerID(scheduleId, businessUniqueId);
            Iterator itr = scheduleList.iterator();
            while (itr.hasNext()) {
                try{
                    Map scheduleMap = (Map) itr.next();
                    TriggerSchedule schedule = new TriggerSchedule();
                    if(Utils.notEmptyString(scheduleMap.get(Constants.START_DATE).toString()) && Utils.notEmptyString(scheduleMap.get(Constants.START_TIME).toString())) {
                        String convertedStartDate = Utils.getLocalDateStringFromUTCDateAndTime(scheduleMap.get(Constants.START_DATE).toString(), scheduleMap.get(Constants.START_TIME).toString(), timezone);
                        schedule.setStartDate(convertedStartDate.substring(0, 10));
                        schedule.setStartTime(convertedStartDate.substring(11, 19));
                    } else {
                        schedule.setStartDate(Constants.EMPTY);
                        schedule.setStartTime(Constants.EMPTY);
                    }
                    if(Utils.notEmptyString(scheduleMap.get(Constants.END_DATE).toString()) && Utils.notEmptyString(scheduleMap.get(Constants.END_TIME).toString())) {
                        String convertedEndDate = Utils.getLocalDateStringFromUTCDateAndTime(scheduleMap.get(Constants.END_DATE).toString(), scheduleMap.get(Constants.END_TIME).toString(), timezone);
                        schedule.setEndDate(convertedEndDate.substring(0, 10));
                        schedule.setEndTime(convertedEndDate.substring(11, 19));
                    } else {
                        schedule.setStartDate(Constants.EMPTY);
                        schedule.setStartTime(Constants.EMPTY);
                    }
                    schedule.setStatus((String) scheduleMap.get(Constants.STATUS));
                    schedule.setFrequency(Utils.isNotNull(scheduleMap.get(Constants.FREQUENCY)) ? scheduleMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
                    schedule.setScheduleId(scheduleMap.get(Constants.SCHEDULE_UUID).toString());
                    schedule.setTimezone(scheduleMap.get(Constants.TIMEZONE).toString());
                    schedule.setChannel(Utils.isNotNull(scheduleMap.get(Constants.CHANNEL)) ? scheduleMap.get(Constants.CHANNEL).toString() : Constants.EMPTY);
                    schedule.setMessage(Utils.isNotNull(scheduleMap.get(Constants.MESSAGE)) ? scheduleMap.get(Constants.MESSAGE).toString() : Constants.EMPTY);
                    schedule.setSubject(Utils.isNotNull(scheduleMap.get(Constants.SUBJECT)) ? scheduleMap.get(Constants.SUBJECT).toString() : Constants.EMPTY);
                    schedule.setContacts(Utils.isNotNull(scheduleMap.get(Constants.CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    schedule.setDeAcitvatedContacts(Utils.isNotNull(scheduleMap.get(Constants.DEACTIVATED_CONTACTS)) ? mapper.readValue(scheduleMap.get(Constants.DEACTIVATED_CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    schedule.setAddFeedback((int) (scheduleMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                    schedule.setTriggerConfig(Utils.isNotNull(scheduleMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig());
                    schedule.setParticipantGroupId(Utils.isNotNull(scheduleMap.get(Constants.PARTICIPANT_GROUP_ID)) ? (int)scheduleMap.get(Constants.PARTICIPANT_GROUP_ID) : 0);
                    schedule.setHeaderData(Utils.isNotNull(scheduleMap.get(Constants.HEADER_DATA)) ? mapper.readValue( scheduleMap.get(Constants.HEADER_DATA).toString(), TriggerListHeaderData.class) : null);
                    schedule.setWebhookConfig(Utils.isNotNull(scheduleMap.get(Constants.WEBHOOK_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.WEBHOOK_CONFIG).toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData());
                    schedule.setIntegrationsConfig(Utils.isNotNull(scheduleMap.get(Constants.INTEGRATIONS_CONFIG)) ? mapper.readValue(scheduleMap.get(Constants.INTEGRATIONS_CONFIG).toString(), HashMap.class) : new HashMap());
                    //schedule.setDeactivatedConditions(Utils.isNotNull(scheduleMap.get(Constants.DEACTIVATED_CONDITIONS)) ? mapper.readValue(scheduleMap.get(Constants.DEACTIVATED_CONDITIONS).toString(), ArrayList.class) : new ArrayList<>());
                    schedule.setReplyToRespondent(Utils.isNotNull(scheduleMap.get(Constants.REPLYRESPONDENT)) ? scheduleMap.get(Constants.REPLYRESPONDENT).toString() : Constants.EMPTY);
                    //DTV-9132 - set trigger action message
                    if(schedule.getChannel().equalsIgnoreCase(Constants.CHANNEL_FOCUS_AREA)){
                        int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
                        String actionMessage = businessId > 0 ? this.getFocusAreaActionMessage(businessId, schedule.getIntegrationsConfig()) : "";
                        schedule.setActionMessage(actionMessage);
                    }
                    triggerScheduleList.add(schedule);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleList;
    }

    /********* notification ************/
    /**
     * method to create schedule by passing schedule bean
     *
     * @param bean
     */
    public Map createTriggerNotification(TriggerNotification bean) {
        Map result = new HashMap();
        int businessId = 0;
        if (Utils.isNotNull(bean.getScheduleId())) {
            if(bean.getScheduleId().length() == 36) {//if schedule unique id passed in the param, get its integer value.
                Map scheduleMap = triggerDAO.getScheduledIdByUniqueId(bean.getScheduleId());
                int scheduleId = scheduleMap.containsKey("id") && Utils.isNotNull(scheduleMap.get("id")) ? Integer.parseInt(scheduleMap.get("id").toString()) : 0;
                businessId = scheduleMap.containsKey("business_id") && Utils.isNotNull(scheduleMap.get("business_id")) ? Integer.parseInt(scheduleMap.get("business_id").toString()) : 0;
                bean.setScheduleId(String.valueOf(scheduleId));
            }
            // create a new entry for trigger schedule
            String triggerNotificationUUID = triggerDAO.createTriggerNotification(bean);
            //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
            String businessUUID = triggerDAO.getBusinessUniqueIdById(businessId);
            triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUUID);
            // assign success / failure response based on the int count
            result.put(Constants.SUCCESS, triggerNotificationUUID.length() > 0 ? true : false);
            result.put(Constants.RESULT, triggerNotificationUUID);
        }
        return result;
    }

    /**
     * method to update schedule by passing schedule bean and primary Id
     *
     * @param bean
     * @param notifyId
     */
    public int updateTriggerNotification(TriggerNotification bean, String notifyId) {
        return triggerDAO.updateTriggerNotification(bean, notifyId);
    }

    /**
     * method to delete trigger schedule by passing triggerId
     *
     * @param notifyId
     */
    public int deleteTriggerNotification(String notifyId) {
        return triggerDAO.deleteTriggerNotification(notifyId);
    }

    /**
     * method to get trigger schedule details by passing triggerId
     *
     * @param notifyId
     */
    public TriggerNotification getTriggerNotificationById(String notifyId) {
        TriggerNotification trigger = new TriggerNotification();
        Map triggerMap = triggerDAO.getTriggerNotificationById(notifyId);
        if (triggerMap.size() > 0) {
            try {
                trigger.setChannel(triggerMap.get(Constants.CHANNEL).toString());
                trigger.setMessage(triggerMap.get(Constants.MESSAGE).toString());
                trigger.setSubject(triggerMap.get(Constants.SUBJECT).toString());
                trigger.setContacts(Utils.isNotNull(triggerMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                trigger.setAddFeedback((int) (triggerMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
                trigger.setNotificationId(triggerMap.get(Constants.NOTIFICATION_UUID).toString());
                trigger.setConditionQuery(triggerMap.containsKey(Constants.CONDITION_QUERY) ? triggerMap.get(Constants.CONDITION_QUERY).toString() : "");
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return trigger;
    }

    /*************************************************Scheduler******************************************/

    /**
     * method to get active scheduled triggers from trigger_schedule table
     *
     * @return
     */
   /* public void identifyTriggersToPublish() {
        List scheduledList = triggerDAO.getActiveTriggerScheduled();

        if (Utils.isNotNull(scheduledList)) {
            Iterator iterator = scheduledList.iterator();
            while (iterator.hasNext()) {
                try {
                    Map schedule = (Map) iterator.next();
                    logger.info("schedule id {} ",schedule.get(Constants.SCHEDULE_ID).toString());
                    int surveyId = schedule.containsKey(Constants.SURVEY_ID) ? Integer.parseInt(schedule.get(Constants.SURVEY_ID).toString()) : 0;
                    int businessId = schedule.containsKey(Constants.BUSINESS_ID) ? Integer.parseInt(schedule.get(Constants.BUSINESS_ID).toString()) : 0;
                    int triggerId = schedule.containsKey(Constants.TRIGGER_ID) ? Integer.parseInt(schedule.get(Constants.TRIGGER_ID).toString()) : 0;
                    int scheduleId = schedule.containsKey(Constants.SCHEDULE_ID) ? Integer.parseInt(schedule.get(Constants.SCHEDULE_ID).toString()) : 0;
                    String lastProcessDate = schedule.get(Constants.LAST_PROCCESSED_DATE) + " " + schedule.get(Constants.LAST_PROCCESSED_TIME);
                    String timezone = schedule.containsKey(Constants.TIMEZONE) ? schedule.get(Constants.TIMEZONE).toString() : Constants.EMPTY;
                    String frequency = schedule.get(Constants.FREQUENCY).toString();
                    String endDate = schedule.containsKey(Constants.END_DATE) ? schedule.get(Constants.END_DATE).toString() : Constants.EMPTY;
                    String endTime = schedule.containsKey(Constants.END_TIME) ? schedule.get(Constants.END_TIME).toString() : Constants.EMPTY;

                    // get businessUniqueId by businessId
                    String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);
                    // get surveyUniqueId
                    String surveyUinqueId = triggerDAO.getSurveyUniqueIdById(surveyId, businessUniqueId);
                    if (Utils.notEmptyString(businessUniqueId)) {
                        // get trigger condition for the triggerId
                        Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
                        if (Utils.isNotNull(triggerMap)) {
                            String triggerCondition = triggerMap.containsKey(Constants.TRIGGER_CONDITION) && Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : "";
                            String readableQuery = triggerMap.containsKey(Constants.READABLE_QUERY) && Utils.isNotNull(triggerMap.get(Constants.READABLE_QUERY)) ? triggerMap.get(Constants.READABLE_QUERY).toString() : "";
                            // method to get formatted date based on trigger config and notification type
                            Map dateInterval = ComputeDateWithIntervals(schedule.get(Constants.TRIGGER_CONFIG).toString(), timezone);
                            // get notification type
                            boolean isAggregateType = (boolean)dateInterval.get(Constants.ISAGGREGATE);
                            // start time to notify
                            String triggerNotifyTime = (String)dateInterval.get(Constants.FORMATTED_DATE);

                            Map processedConditionMap;
                            if (Utils.emptyString(readableQuery)) {
                                // get matching token for the trigger condition
//                                processedConditionMap = processTriggerCondition(triggerCondition, surveyUinqueId, surveyId, businessUniqueId, lastProcessDate, isAggregateType);
                                processedConditionMap = processTriggerCondition(triggerCondition, surveyUinqueId, surveyId, businessUniqueId, lastProcessDate, isAggregateType, scheduleId, triggerNotifyTime);
                                if (Utils.isNotNull(processedConditionMap) && processedConditionMap.containsKey(Constants.READABLE_QUERY)) {
                                    // update the readable query in trigger table
                                    triggerDAO.updateTriggerReadableQuery(businessUniqueId, processedConditionMap.get(Constants.READABLE_QUERY).toString(), triggerId);
                                }
                            } else {
                                // readable query exists, execute and get the result
//                                processedConditionMap = this.executeReableQuery(surveyUinqueId, readableQuery, lastProcessDate, isAggregateType);
                                processedConditionMap = this.executeReableQuery(surveyUinqueId, readableQuery, isAggregateType, scheduleId, triggerNotifyTime);
                            }

                            //check if processedConditionMap results contains tokens list
                            if (Utils.isNotNull(processedConditionMap) && processedConditionMap.containsKey(Constants.CONDITIONQUERY)) {
                                List contactList = schedule.containsKey(Constants.CONTACTS) ? mapper.readValue(schedule.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList();
                                //  create notification to send
                                TriggerNotification triggerNotification = new TriggerNotification();
                                triggerNotification.setContacts(contactList);
                                triggerNotification.setSubject(schedule.get(Constants.SUBJECT).toString());
                                triggerNotification.setMessage(schedule.get(Constants.MESSAGE).toString());
                                triggerNotification.setAddFeedback(schedule.get(Constants.ADD_FEEDBACK).toString().equals("1") ? true : false);
                                triggerNotification.setChannel(schedule.get(Constants.CHANNEL).toString());
                                triggerNotification.setStartTime(triggerNotifyTime);
                                triggerNotification.setScheduleId(schedule.get(Constants.SCHEDULE_ID).toString());
                                triggerNotification.setConditionQuery(processedConditionMap.get(Constants.CONDITIONQUERY).toString());
                                // aggregate type : The aggregate alerts will not have meta data and feedbacks attached. Instead it will provide a link to the respondents page.
                                // compare current date with last processed date to avoid duplicate entry for same day

                                *//*System.out.println("aggregate "+isAggregateType);
                                System.out.println(Utils.getCurrentDateAsString());
                                System.out.println(schedule.get(Constants.LAST_PROCCESSED_DATE));
                                System.out.println(Utils.compareOnlyDates(Utils.getCurrentDateAsString(), schedule.get(Constants.LAST_PROCCESSED_DATE).toString()));*//*

                                //if(isAggregateType && (Utils.compareOnlyDates(Utils.getCurrentDateAsString(), schedule.get(Constants.LAST_PROCCESSED_DATE).toString()))){
                                if(isAggregateType && !(checkNotificationExistsForTheSchedule((int)schedule.get(Constants.SCHEDULE_ID)))){
                                    logger.info("aggregate case");
                                    triggerNotification.setSubmissionToken(null);
                                    triggerDAO.createTriggerNotification(triggerNotification);
                                }else if(!isAggregateType ){
                                    // get submission token list
                                    List tokenList = processedConditionMap.containsKey(Constants.TOKENS) ? (List)processedConditionMap.get(Constants.TOKENS) : new ArrayList();
                                    logger.info("delayed or immediate");
                                    // Delayed or Immediate type: one email for each back will be sent. Apply meta data replacement. Attach feedback if necessary.
                                    Iterator itr = tokenList.iterator();
                                    while(itr.hasNext()) {
                                        triggerNotification.setSubmissionToken((String)itr.next());
                                        triggerDAO.createTriggerNotification(triggerNotification);
                                    }
                                }
                            }

                            if (frequency.equals(Constants.FREQUENCY_SINGLE)) {
                                // update the status to completed when single frequency trigger condition satisfied.
                                triggerDAO.updateScheduleStatusByScheduleId(scheduleId, Constants.STATUS_COMPLETED);
                            } else if (frequency.equals(Constants.FREQUENCY_MULTIPLE)) {
                                // update last processed date and time of each scheduleId in schedule table
                                triggerDAO.updateLastProcessedDateTime(scheduleId);
                                // check currentUtcTime is greater or equal to scheduleEndTime for multiple frequency and update the status to completed
                                String currentUTCTime = Utils.getCurrentUTCDateAsString();
                                String scheduleEndDateTime = endDate +" "+endTime;
                                if(Utils.compareDates(currentUTCTime, scheduleEndDateTime)){
                                    // update the status to completed when multiple frequency trigger condition end date and time reached.
                                    triggerDAO.updateScheduleStatusByScheduleId(scheduleId, Constants.STATUS_COMPLETED);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }
        }
    }*/

    /**
     * method to process trigger condition and get matched participant token count and corresponding where condition in readable query format
     *
     * @param triggerCondition
     * @param surveyUniqueId
     * @param surveyId
     * @param businessUUID
     * @return
     */
    public Map processTriggerCondition(String triggerCondition, String surveyUniqueId, int surveyId, String businessUUID, String triggerStartDate, boolean isAggregateType, int scheduleId, String triggerNotifyTime, String timezone, boolean isFrequencyType) {
        // initialize operators map
        logger.info("process trigger condition");
        Map operatorMap = new HashMap();
        operatorMap.put(Constants.AND_OPERATOR, Constants.AND);
        operatorMap.put(Constants.OR_OPERATOR, Constants.OR);
        String triggerQuery = triggerCondition;

        List queries = new ArrayList();
        List operators = new ArrayList();
        StringBuilder sqlWhere = new StringBuilder();
        StringBuilder sqlPerfWhere = new StringBuilder();
        StringBuilder sqlAggreWhere = new StringBuilder();


        // split the trigger condition using and / or operator
        String[] subQueries = triggerCondition.split("&&|\\|\\|");
        // loop through sub queries of trigger condition strings
        for (int i = 0; i < subQueries.length; i++) {
            // add each query into queries list
            queries.add(subQueries[i]);
            // replace empty value to each index query String in trigger condition, this will replace all the string values and only operator will be there.
            triggerCondition = triggerCondition.replaceFirst(Pattern.quote(subQueries[i]), "");//Since replaceFirst's 1st parameter is assumed to be a regex, you need to escape special characters. you can use Pattern.quote(string) to escape it.
        }
        // loop through the operator in trigger condition
        for (int k = 0; k < triggerCondition.length(); k = k + 2) {
            // && or || operator length is 2, so substring with length 2
            String eachOperator = triggerCondition.substring(k, k + 2);
            // add the operator to the list
            operators.add(eachOperator);
        }
        Iterator<String> queryIterator = queries.iterator();
        Iterator<String> operatorIterator = operators.iterator();
        boolean isBracketOpen = false;
        boolean isPerfBracketOpen = false;
        boolean isAggeBracketOpen = false;

        Map txtAnalyticsMap = new HashMap();
        int index = 0;
        // loop through query iterator
        while (queryIterator.hasNext()) {
            try {
                String eachOperator = "";
                String eachMysqlQueryArr = "";
                String perfQueryArr = "";
                String aggreQueryArr = "";

                // assign each query
                String eachQuery = queryIterator.next();
                logger.info("each query {}", eachQuery);
                if (operatorIterator.hasNext()) {
                    // get same as query index from operator
                    eachOperator = operatorIterator.next();
                }

                // get each sql query condition for trigger condition
                if (eachQuery.contains(Constants.PERFORMANCE)) {
                    logger.info("performance case");
                    perfQueryArr = this.getPerformanceSubQuery(eachQuery);
                    isPerfBracketOpen = appendTriggerConditions(isPerfBracketOpen, eachOperator, sqlPerfWhere, operatorMap, perfQueryArr);
                }
                else if(eachQuery.contains(Constants.DT_AGGREGATE)){
                    logger.info("aggregate case");
                    aggreQueryArr = this.getAggregateScoreSubQuery(eachQuery, businessUUID);
                    isAggeBracketOpen = appendTriggerConditions(isAggeBracketOpen, eachOperator, sqlAggreWhere, operatorMap, aggreQueryArr);
                }
                else {
                    eachMysqlQueryArr = computeSqlConditionFromTriggerCondition(eachQuery, businessUUID, surveyId, surveyUniqueId);
                    if (Utils.isNotNull(eachMysqlQueryArr)) {
                        if (!eachMysqlQueryArr.contains(Constants.DOLLAR_DELIMITER)) {
                            isBracketOpen = appendTriggerConditions(isBracketOpen, eachOperator, sqlWhere, operatorMap, eachMysqlQueryArr);
                        } else {
                            // set text analytics query for open ended question
                            eachMysqlQueryArr = eachMysqlQueryArr.replaceAll("\\$\\$", "");
                            Map openQuestionMap = mapper.readValue(eachMysqlQueryArr, HashMap.class);
                            if (openQuestionMap.size() > 0) {
                                String analyticsTypeAns = "";
                                String analyticsType = "";
                                if (openQuestionMap.containsKey(Constants.CATEGORY)) {
                                    analyticsTypeAns = openQuestionMap.get(Constants.CATEGORY).toString();
                                    analyticsType = Constants.CATEGORY;

                                } else if (openQuestionMap.containsKey(Constants.SENTIMENT)) {
                                    analyticsTypeAns = openQuestionMap.get(Constants.SENTIMENT).toString();
                                    analyticsType = Constants.SENTIMENT;
                                }
                                if (txtAnalyticsMap.containsKey(analyticsType)) {
                                    List existingList = (List) txtAnalyticsMap.get(analyticsType);
                                    existingList.add(analyticsTypeAns);
                                } else {
                                    List lst = new ArrayList();
                                    lst.add(analyticsTypeAns);
                                    txtAnalyticsMap.put(analyticsType, lst);
                                }
                            }
                        }
                    }
                }
                index++;
            } catch (Exception e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }

        Map queryMap = new HashMap();
        queryMap.put(Constants.DT_QUERY, sqlWhere.toString());
        queryMap.put(Constants.TXT_ANALYTICS, txtAnalyticsMap);
        queryMap.put(Constants.DT_PERFORMANCE, sqlPerfWhere.toString());
        queryMap.put(Constants.DT_AGGREGATE, sqlAggreWhere.toString());
        queryMap.put(Constants.TIMEZONE, timezone);
        String queryStr = new JSONObject(queryMap).toString();
        logger.info("trigger query {}", queryStr);

        Map resultMap = new HashMap();
//        String sqlCondn = "";
        // check if overall trigger sqlwhere condition is available
        if (sqlWhere.length() > 0 || sqlPerfWhere.length() > 0 || sqlAggreWhere.length() > 0) {
            logger.info("sql where condition {}", sqlWhere.toString());
            logger.info("sql performance where condition {}", sqlPerfWhere.toString());
            logger.info("sql aggregate where condition {}", sqlAggreWhere.toString());
            if(sqlPerfWhere.toString().trim().endsWith("and")){
                int lastIndex = sqlPerfWhere.toString().lastIndexOf(Constants.AND);
                String sqlPerf = sqlPerfWhere.toString().substring(0,lastIndex );
                queryMap.put(Constants.DT_PERFORMANCE, sqlPerf);
            }
            if(sqlWhere.toString().trim().endsWith("and")){
                int lastIndex = sqlWhere.toString().lastIndexOf(Constants.AND);
                String sqlCondn = sqlWhere.toString().substring(0,lastIndex );
                queryMap.put(Constants.DT_QUERY, sqlCondn);
            }
            if(sqlAggreWhere.toString().trim().endsWith("and")){
                int lastIndex = sqlAggreWhere.toString().lastIndexOf(Constants.AND);
                String sqlAggr = sqlAggreWhere.toString().substring(0,lastIndex );
                queryMap.put(Constants.DT_AGGREGATE, sqlAggr);
            }
            queryStr = new JSONObject(queryMap).toString();
            resultMap = executeReableQuery(triggerCondition, surveyUniqueId, queryStr, isAggregateType, scheduleId, triggerNotifyTime, triggerStartDate, surveyId, businessUUID, isFrequencyType);

        } else if(!(sqlWhere.length() > 0) && triggerQuery.equalsIgnoreCase("DTANY")){
            resultMap = executeReableQuery(triggerQuery, surveyUniqueId, queryStr, isAggregateType, scheduleId, triggerNotifyTime, triggerStartDate, surveyId, businessUUID, isFrequencyType);
        }
        resultMap.put(Constants.READABLE_QUERY, queryStr);
        return resultMap;
    }

    /**
     * method to append where conditions in proper format
     * @param isPerfBracketOpen
     * @param eachOperator
     * @param sqlPerfWhere
     * @param operatorMap
     * @param perfQueryArr
     * @return
     */
    public boolean appendTriggerConditions(boolean isPerfBracketOpen, String eachOperator, StringBuilder sqlPerfWhere, Map operatorMap, String perfQueryArr){
        // append open bracket in case of OR condition starts
        if (Utils.notEmptyString(eachOperator) && !isPerfBracketOpen && eachOperator.equals(Constants.OR_OPERATOR)) {
            isPerfBracketOpen = true;
            sqlPerfWhere.append("( ");
        }
        // append trigger condition
        sqlPerfWhere.append(perfQueryArr);
        // append close bracket in case of OR condition ends
        if (isPerfBracketOpen && !eachOperator.equals(Constants.OR_OPERATOR)) {
            sqlPerfWhere.append(") ");
            isPerfBracketOpen = false;
        }
        // append and / or operator to the condition
        if ( Utils.notEmptyString(eachOperator)) {
            sqlPerfWhere.append(" ").append(operatorMap.get(eachOperator)).append(" ");
        }
        return isPerfBracketOpen;
    }

  /*  *//**
     * mehod to execute readable sql where query
     * @param surveyUniqueId
     * @param sqlWhere
     * @param lastProcessedDate
     * @return
     *//*
    public Map executeReableQuery(String surveyUniqueId, String sqlWhere, String lastProcessedDate, boolean isAggregateType){
        Map resultMap = new HashMap();
        try {
            Map eventSubmissions = triggerDAO.getEventSubmissionTokens(surveyUniqueId, sqlWhere, lastProcessedDate);
            int tokenCount = eventSubmissions.containsKey("id_count") ? Integer.parseInt(eventSubmissions.get("id_count").toString()) : 0;
//            if (tokenCount > 0) {
                int minId = Utils.isNotNull(eventSubmissions.get("min_id")) ? (int) eventSubmissions.get("min_id") : 0;
                int maxId = Utils.isNotNull(eventSubmissions.get("max_id")) ? (int) eventSubmissions.get("max_id") : 0;
                List tokensList = eventSubmissions.containsKey(Constants.TOKENS) && Utils.isNotNull(eventSubmissions.get(Constants.TOKENS))  ? mapper.readValue(eventSubmissions.get(Constants.TOKENS).toString(), ArrayList.class) : new ArrayList();
                // generate a query by appending the minId and maxId to trigger where condition, this will be helpful during sending notification.
                // since last processedDate will return more records at the time of sending notification when compared to identifying trigger conditions.
                String finalTiggerSql = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where " + sqlWhere ; // TODO we can revisit this
                finalTiggerSql += isAggregateType ?  " and created_time >='" +lastProcessedDate + "'":  " and (id >= " + minId + " and id <= " + maxId + ")";

                // set result map with executable query of trigger condition and it corresponding submission tokens
                resultMap.put(Constants.CONDITIONQUERY, finalTiggerSql);
                resultMap.put(Constants.TOKEN_COUNT, tokenCount);
                resultMap.put(Constants.TOKENS, tokensList);
//            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  resultMap;
    }*/


    /**
     * mehod to execute readable sql where query
     * @param surveyUniqueId
     * @param sqlWhere
     * @param scheduleId
     * @param triggerNotifyTime
     * @return
     */
    public Map executeReableQuery(String triggerCondition, String surveyUniqueId, String sqlWhere, boolean isAggregateType, int scheduleId, String triggerNotifyTime, String triggerStartDate, int surveyId, String businessUniqueId, boolean isFrequencyType){
        Map resultMap = new HashMap();
        try {
            logger.info("excecuable query {}", sqlWhere);
            String finalTiggerSql = "";
            boolean isPerfScoreMet = false;
            boolean isAggrScoreMet = false;
            Map queryMap = Utils.notEmptyString(sqlWhere) ? mapper.readValue(sqlWhere, HashMap.class) : new HashMap();
            String dtQuerySql = queryMap.containsKey(Constants.DT_QUERY) ? queryMap.get(Constants.DT_QUERY).toString() : "";
            String dtPerfSql = queryMap.containsKey(Constants.DT_PERFORMANCE) ? queryMap.get(Constants.DT_PERFORMANCE).toString() : "";
            String dtAggrSql = queryMap.containsKey(Constants.DT_AGGREGATE) ? queryMap.get(Constants.DT_AGGREGATE).toString() : "";

            if (dtQuerySql.length() > 0) {
                //finalTiggerSql = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where created_time > '" + triggerStartDate + "' and" + dtQuerySql;
                finalTiggerSql = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where created_time > '" + triggerStartDate + "' and" + dtQuerySql;
            }
            else if(!(dtQuerySql.length() > 0) && triggerCondition.equalsIgnoreCase("DTANY")) {

                //finalTiggerSql = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where created_time > '" + triggerStartDate + "'";
                finalTiggerSql = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where created_time > '" + triggerStartDate + "'";
            }
            if (isAggregateType && !isFrequencyType) { //DTV-5315
                /**
                 * * *  * Aggregate
                 * - if notifications does not exist for the schedule id then retrieve all tokens matching the condition
                 * - if entry already exists then use start and end date as afilter
                 *     start date will be current start time minus 24 hrs
                 *     end date will be current start time
                 *     apply them as filters for the tokens
                 */
                int recCount = triggerDAO.getNotificationCountByScheduleId(scheduleId, businessUniqueId);
                String startTime = Utils.getFormattedDateTimeAddHours(triggerNotifyTime, -24); // startime -24 hrs to get previous day
                String endTime = Utils.getCurrentDateInTimezone(Constants.UTC); //Current time
                finalTiggerSql = recCount > 0 ? finalTiggerSql + " and created_time >='" + startTime + "' and created_time <= '" + endTime + "'" : finalTiggerSql;

            } else if(isFrequencyType && !isAggregateType){
                /**DTV-5315, Frequency type notification is applicable  for “Aggregate score” or “Program performance" type trigger conditions only
                 */

                /**
                 * * *  * aggregate email is used for frequency notification type.
                 * - if notifications does not exist for the schedule id then retrieve all tokens matching the condition
                 * - if entry already exists then use start and end date as afilter
                 *     start date will be current start time minus 24 hrs
                 *     end date will be current start time
                 *     apply them as filters for the tokens
                 */
                int recCount = triggerDAO.getNotificationCountByScheduleId(scheduleId, businessUniqueId);
                String startTime = Utils.getFormattedDateTimeAddHours(triggerNotifyTime, -24); // startime -24 hrs to get previous day
                String endTime = Utils.getCurrentDateInTimezone(Constants.UTC); //Current time
                finalTiggerSql = recCount > 0 ? finalTiggerSql + " and created_time >='" + startTime + "' and created_time <= '" + endTime + "'" : finalTiggerSql;

                List tokensList = new ArrayList();
                if(Utils.notEmptyString(dtQuerySql)){
                    Map eventSubmissions = triggerDAO.getEventSubmissionTokens(surveyUniqueId, dtQuerySql, scheduleId, triggerStartDate, businessUniqueId);
                    tokensList = eventSubmissions.containsKey(Constants.TOKENS) && Utils.isNotNull(eventSubmissions.get(Constants.TOKENS))
                            ? mapper.readValue(eventSubmissions.get(Constants.TOKENS).toString(), ArrayList.class) : new ArrayList();
                }
                // either metadata query should be empty or metadata with trigger condition should met
                if(Utils.emptyString(dtQuerySql) || (Utils.notEmptyString(dtQuerySql) && tokensList.size() > 0)) {
                    //dtPerformance score
                    List<Map<String, Object>> perfList = new ArrayList<>();
                    if (Utils.notEmptyString(dtPerfSql)) {
                        Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
                        String surveyStartDate = surveyMap.containsKey("from_date") ? surveyMap.get("from_date").toString() : "";
                        String sruveyEndDate = surveyMap.containsKey("to_date") ? surveyMap.get("to_date").toString() : "";
                        perfList = triggerDAO.checkPerformanceTriggerCondition(tokensList, businessUniqueId, surveyUniqueId, scheduleId,
                                surveyId, dtPerfSql, "", surveyStartDate, sruveyEndDate);
                        if (perfList.size() > 0) {
                            resultMap.put(Constants.ISCONDITIONMET, true);
                        }
                    }
                    // either dtPerfSql query should be empty or dtPerfSql with trigger condition should met
                    // Check for both dtPerfSql & dtAggrSql present
                    if(Utils.emptyString(dtPerfSql) || (Utils.notEmptyString(dtPerfSql) && perfList.size() > 0)) {
                        //dtAggregate score
                        if (Utils.notEmptyString(dtAggrSql)) {
                            Map<String, Object> aggregateMap = this.checkAggregateTriggerCondition(tokensList, businessUniqueId, surveyUniqueId, scheduleId,
                                    surveyId, dtAggrSql, true);
                            resultMap.put(Constants.ISCONDITIONMET, aggregateMap.containsKey("token") && Utils.notEmptyString(aggregateMap.get("token").toString()));
                            //DTV-8019, 8038
                            if(aggregateMap.containsKey("isAggregateConditionMatched") && Utils.isNotNull(aggregateMap.get("isAggregateConditionMatched").toString())){
                                resultMap.put("isAggregateConditionMatched", aggregateMap.get("isAggregateConditionMatched"));
                            }
                            //DTV-8019, 8038
                            if(aggregateMap.containsKey("aggregateConditionFailedToken") && Utils.isNotNull(aggregateMap.get("aggregateConditionFailedToken").toString())){
                                resultMap.put("aggregateConditionFailedToken", aggregateMap.get("aggregateConditionFailedToken"));
                            }
                        }
                    }
                }
            }
            else {
                /**
                 * immediate or delay
                 *     - look for tokens present in the notifications for the schedule id
                 *     - query events table which are not already present in notifications
                 *     - insert into notification
                 *     - start time will be computed interval
                 */
//                    List<String> tokens = triggerDAO.getNotificationSubmissionTokens(scheduleId);
                Map eventSubmissions = triggerDAO.getEventSubmissionTokens(surveyUniqueId, dtQuerySql, scheduleId, triggerStartDate, businessUniqueId);
                List tokensList = eventSubmissions.containsKey(Constants.TOKENS) && Utils.isNotNull(eventSubmissions.get(Constants.TOKENS))
                        ? mapper.readValue(eventSubmissions.get(Constants.TOKENS).toString(), ArrayList.class) : new ArrayList();
                resultMap.put(Constants.TOKENS, tokensList);
                // either metadata query should be empty or metadata with trigger condition should met
                if(Utils.emptyString(dtQuerySql) || (Utils.notEmptyString(dtQuerySql) && tokensList.size() > 0)) {
                    //dtPerformance score
                    if (Utils.notEmptyString(dtPerfSql)) {
                        if (Utils.emptyString(dtQuerySql)) {
                            tokensList = new ArrayList();
                        }
                        Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
                        String surveyStartDate = surveyMap.containsKey("from_date") ? surveyMap.get("from_date").toString() : "";
                        String sruveyEndDate = surveyMap.containsKey("to_date") ? surveyMap.get("to_date").toString() : "";
                        List<Map<String, Object>> perfList = triggerDAO.checkPerformanceTriggerCondition(tokensList, businessUniqueId, surveyUniqueId,
                                scheduleId, surveyId, dtPerfSql, "", surveyStartDate, sruveyEndDate);
                        List<String> token = new ArrayList<>();
                        if (perfList.size() > 0) {
                            Map<String, Object> mp = perfList.get(0);
                            token = (List) (mp.get("token"));
                            isPerfScoreMet = true;
                        }
                        resultMap.put(Constants.TOKENS, token);
                    }
                    //dtAggregate score
                    if (Utils.notEmptyString(dtAggrSql)) {
                        if (Utils.emptyString(dtQuerySql)) {
                            tokensList = new ArrayList();
                        }
                        Map<String, Object> aggregateMap = this.checkAggregateTriggerCondition(tokensList, businessUniqueId, surveyUniqueId, scheduleId,
                                surveyId, dtAggrSql, true);
                        List<String> token = new ArrayList<>();
                        if (aggregateMap.containsKey("token")) {
                            token.add(aggregateMap.get("token").toString());
                            isAggrScoreMet = true;
                        }
                        resultMap.put(Constants.TOKENS, token);
                        //DTV-8019, 8038
                        if(aggregateMap.containsKey("isAggregateConditionMatched") && Utils.isNotNull(aggregateMap.get("isAggregateConditionMatched").toString())){
                            resultMap.put("isAggregateConditionMatched", aggregateMap.get("isAggregateConditionMatched"));
                        }
                        if(aggregateMap.containsKey("aggregateConditionFailedToken") && Utils.isNotNull(aggregateMap.get("aggregateConditionFailedToken").toString())){
                            resultMap.put("aggregateConditionFailedToken", aggregateMap.get("aggregateConditionFailedToken"));
                        }
                    }
                    //combination of program score and aggregate score trigger check
                    if (Utils.notEmptyString(dtAggrSql) && Utils.notEmptyString(dtPerfSql)) {
                        if (!isAggrScoreMet || !isPerfScoreMet) {
                            resultMap.put(Constants.TOKENS, Constants.EMPTY);
                        }
                    }
                }

            }
            // set result map with executable query of trigger condition and it corresponding submission tokens
            resultMap.put(Constants.CONDITIONQUERY, finalTiggerSql);

        }catch (Exception e){
            e.printStackTrace();
        }
        return  resultMap;
    }



    /**
     * method to check aggregate score trigger condition with survey feedback
     * @param tokenList
     * @param businessUniqueId
     * @param surveyUniqueId
     * @param scheduleId
     * @param surveyId
     * @param dtAggSql
     * @return
     */
    public Map<String, Object> checkAggregateTriggerCondition(List tokenList, String businessUniqueId, String surveyUniqueId,
                                                              int scheduleId, int surveyId, String dtAggSql, boolean isImmediateType){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
            String surveyStartDate = surveyMap.containsKey("from_date") ? surveyMap.get("from_date").toString() : "";
            String surveyEndDate = surveyMap.containsKey("to_date") ? surveyMap.get("to_date").toString() : "";
            List questionIds = surveyMap.containsKey("question_ids") ? mapper.readValue(surveyMap.get("question_ids").toString(), ArrayList.class) : new ArrayList<>();

            Map surveyScheduleMap = triggerDAO.getSurveyScheduleBySurveyId(surveyId, businessUniqueId);
            String surveyType = surveyScheduleMap.containsKey("type") ? surveyScheduleMap.get("type").toString() : "";

            /**
             * from
             * "{\"2031\":\"nps < 100\"}  and  {\"2032\":\"nps = 0\"}  and  {\"2033\":\"nps >= 90\"}"
             * to
             * {
             * 	"2031": "nps < 100",
             * 	"2032": "nps = 0",
             * 	"2033": "nps >= 90"
             * }
             */
            dtAggSql = dtAggSql.replaceAll("and", ",");
            int first = dtAggSql.indexOf("{") + 1; //ignore first and last index of braces
            int last = dtAggSql.lastIndexOf("}");
            String afterReplace = new StringBuilder()
                    .append(dtAggSql, 0, first)
                    .append(dtAggSql.substring(first, last).replaceAll("\\{|\\}", ""))
                    .append(dtAggSql, last, dtAggSql.length())
                    .toString();

            Map aggrScoreCondnMap = Utils.notEmptyString(afterReplace) ? mapper.readValue(afterReplace, HashMap.class) : new HashMap();
            boolean isTokenExist = false;

            //get last recent submitted token from event
            Map tokenMap = triggerDAO.getLatestResponseTokenAndCount(surveyUniqueId);
            String token = tokenMap.containsKey("token") && Utils.isNotNull( tokenMap.get("token")) ? tokenMap.get("token").toString() : "";
            String responseCount = tokenMap.containsKey("totalResponseCount") && Utils.isNotNull( tokenMap.get("totalResponseCount")) ?
                    tokenMap.get("totalResponseCount").toString() : "";

            //check recent token only for immediate case. For aggregate case, we need to send total count of response based on survey start and end date
            if(isImmediateType) {
                if (Utils.notEmptyString(token)) {
                    //check the token not exists in trigger schedule
                    isTokenExist = triggerDAO.isTokenExistInSchedule(token, scheduleId, businessUniqueId);
                }
            }
            /** check whether (token should not exists in notification table in case of immediate/delayed case) or ( aggregateType).*/
            if ((!isTokenExist && isImmediateType) || !isImmediateType) {
                /**check sentiment trigger condition and add a key named sentiment in aggrScoreCondnMap*/
                checkOverallSentiment(aggrScoreCondnMap, businessUniqueId, surveyUniqueId, surveyStartDate, surveyEndDate);
                Iterator<Map.Entry<String, String>> itr = aggrScoreCondnMap.entrySet().iterator();
                //loop through all trigger conditions
                while (itr.hasNext()) {
                    boolean isAggrCondnMatched = false;
                    Map.Entry<String, String> entry = itr.next();
                    String keyQuetionIdStr = entry.getKey();
                    String queryStr = entry.getValue();

                    if (queryStr.contains(Constants.NPS)) {
                        int questionId = Utils.notEmptyString(keyQuetionIdStr) ? Integer.parseInt(keyQuetionIdStr) : 0;
                        int questionIndex = questionId > 0 ? questionIds.indexOf(questionId) : -1;
                        if(questionIndex > -1) {
                            double npsScore = triggerDAO.getNpsScoreForSurvey(surveyUniqueId, questionIndex, surveyStartDate, surveyEndDate, tokenList, surveyType);
                            /**replace the nps score value in trigger condition:
                             eg: trigger condition: nps > 10 and npsScore: 60 -> replace nps string with npsScore gives 60>10*/
                            queryStr = queryStr.replace(Constants.NPS, String.valueOf(npsScore)).replace("and", "&&"); // nps>20 to 50>20
                            if (npsScore != -500 && evaluateStringCondition(queryStr)) {
                                isAggrCondnMatched = true;
                            }
                        }
                    } else if (queryStr.contains(Constants.METRIC)) {
                        int questionId = Utils.notEmptyString(keyQuetionIdStr) ? Integer.parseInt(keyQuetionIdStr) : 0;
                        //get metricScore and assign to a variable
                        String metricScore = getMetricScoreDetails(surveyMap, businessUniqueId, surveyUniqueId, surveyStartDate, surveyEndDate, tokenList, questionId);
                         /**replace the metric score value in trigger condition:
                         eg: trigger condition: metric > 10 and metricScore: 6 -> replace metric string with metricScore gives 6>10*/
                        // replace metric>10 to 6.5>10 , between case metric > 1 and metric < 9 --> 2 > 1 && 2 < 9
                        queryStr = queryStr.replace(Constants.METRIC, metricScore).replace("and", "&&");
                        if (evaluateStringCondition(queryStr)) {
                            isAggrCondnMatched = true;
                        }
                    } else if (keyQuetionIdStr.contains(Constants.SENTIMENT)) {
                        /**overall sentiment computation done in checkOverallSentiment method and assigned it to sentiment key*/
                        queryStr = queryStr.replace("and", "&&");
                        if (evaluateStringCondition(queryStr)) {
                            isAggrCondnMatched = true;
                        }
                    }

                    if (!isAggrCondnMatched) {
                        /** All conditions should be matched. if one not satisfied then return empty .*/
                        //DTV-8019, 8038
                        resultMap.put("isAggregateConditionMatched", false);
                        /**DTV-8038
                         * if aggregate condition is not matched then removing foucsArea from focus_metric_{buuid} table based on submissionToken
                         */
                        resultMap.put("aggregateConditionFailedToken", token);
                        return resultMap;
                    }
                }
                //set the latest token if it matches the condition
                resultMap.put("token", token);
                resultMap.put("responseCount", responseCount);
                //DTV-8019, 8038
                resultMap.put("isAggregateConditionMatched", true);
            }
        }catch (Exception e){
            logger.error("Exception at checkAggregateTriggerCondition. Msg {}", e.getMessage());
        }
        return resultMap;
    }

    /**
     * method to check overall sentiment trigger condition for given survey
     * @param aggrScoreCondnMap
     * @param businessUniqueId
     * @param surveyUniqueId
     * @return
     */
    public Map checkOverallSentiment(Map aggrScoreCondnMap, String businessUniqueId, String surveyUniqueId, String fromDate, String toDate){
        try {
            StringBuilder sentimentQry = new StringBuilder();
            //get sentiment related conditions from trigger condition
            if (aggrScoreCondnMap.containsKey(Constants.POSITIVE)) {
                sentimentQry.append(aggrScoreCondnMap.get(Constants.POSITIVE));
                aggrScoreCondnMap.remove(Constants.POSITIVE);
            }
            if (aggrScoreCondnMap.containsKey(Constants.NEGATIVE)) {
                if (Utils.notEmptyString(sentimentQry.toString())) {
                    sentimentQry.append(" && ");
                }
                sentimentQry.append(aggrScoreCondnMap.get(Constants.NEGATIVE));
                aggrScoreCondnMap.remove(Constants.NEGATIVE);
            }
            if (aggrScoreCondnMap.containsKey(Constants.NEUTRAL)) {
                if (Utils.notEmptyString(sentimentQry.toString())) {
                    sentimentQry.append(" && ");
                }
                sentimentQry.append(aggrScoreCondnMap.get(Constants.NEUTRAL));
                aggrScoreCondnMap.remove(Constants.NEUTRAL);
            }
            //if sentiment condition available then call the eliza summary api
            if (sentimentQry.length() > 0) {
                Map sentimentMap = getOverallSentimentBySurvey(businessUniqueId, surveyUniqueId, fromDate, toDate);
                //assign values for each sentiment from eliza response
                String positivePer = (sentimentMap.get(Constants.POSITIVE).toString());
                String negativePer = (sentimentMap.get(Constants.NEGATIVE).toString());
                String neutralPer = (sentimentMap.get(Constants.NEUTRAL).toString());
                //compare the results with trigger condition
                String queryStr = sentimentQry.toString().replaceAll(Constants.POSITIVE, positivePer).
                        replaceAll(Constants.NEGATIVE, negativePer).replaceAll(Constants.NEUTRAL, neutralPer);
                //assign the query condition against sentiment key
                aggrScoreCondnMap.put("sentiment", queryStr);

            }
        }catch (Exception e){
            logger.error("exception at checkOverallSentiment. Msg {}", e.getMessage());
        }

        return aggrScoreCondnMap;
    }

    /**
     * method to get overall sentiment for given survey from eliza
     * @param businessUniqueId
     * @param surveyUniqueId
     * @return
     */
    public Map getOverallSentimentBySurvey(String businessUniqueId, String surveyUniqueId, String fromDate, String toDate){
        logger.info("getOverallSentimentBySurvey begin" );
        Map respMap = new HashMap();
        try {
            //String urlToCall = "https://api.dropthought.com/eliza/data/summary/api/v1/3a7b41a2-b294-421a-8502-2ec628e3f2e2/5205050a_9567_4990_94bc_cc82b44cb2d5?fromDate=2021-9-14%2000:00:00&toDate=2021-9-14%2023:59:59&timezone=Asia/Calcutta&type=advanced";
            String urlToCall = elizaSummaryUrl + businessUniqueId + "/" + surveyUniqueId.replace("-", "_") + "?type=advanced&fromDate="+fromDate+"&toDate="+toDate;

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            ResponseEntity<Object> responseEntity = restTemplate.getForEntity(urlToCall, Object.class);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("eliza request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                        if (responseEntity.getBody() != null) {
                            Map responseMap = (Map) responseEntity.getBody();
                            respMap.put(Constants.POSITIVE, responseMap.containsKey("totalPositivePercentage") ? responseMap.get("totalPositivePercentage") : 0);
                            respMap.put(Constants.NEGATIVE, responseMap.containsKey("totalNegativePercentage") ? responseMap.get("totalNegativePercentage") : 0);
                            respMap.put(Constants.NEUTRAL, responseMap.containsKey("totalNeutralPercentage") ? responseMap.get("totalNeutralPercentage") : 0);
                        }
                    }
                }
            }
            logger.info("respMap {}", new JSONObject(respMap).toString() );
        }catch (Exception e){
            logger.error("exception at getOverallSentimentBySurvey. Msg {}", e.getMessage());
        }
        logger.info("getOverallSentimentBySurvey end" );
        return respMap;
    }

    /**
     * method to evaluate string containing math expressions.
     * @param queryStr
     * @return true/false
     */
    public Boolean evaluateStringCondition(String queryStr){
        try {
            if(queryStr.contains("=") && (!queryStr.contains("<") && !queryStr.contains(">"))){
                queryStr  = queryStr.replace("=", "=="); // added this to fix  Invalid left hand side for assignment
            }
            if(Utils.notEmptyString(queryStr)) {
                ScriptEngineManager sem = new ScriptEngineManager();
                ScriptEngine se = sem.getEngineByName("js");
                return Utils.isNotNull(se.eval(queryStr)) ? (Boolean) se.eval(queryStr) : false;
            }
        } catch (ScriptException e) {
            logger.error("evaluateStringCondition -> Invalid Expression: {}", queryStr);
            logger.debug("exception at evaluateStringCondition {}", e);
        }
        return false;
    }

    /**
     * method to compute sql where condition query based on the each trigger condition
     *
     * @param eachQuery
     * @param businessUUID
     * @param surveyId
     * @return
     */
    public String computeSqlConditionFromTriggerCondition(String eachQuery, String businessUUID, int surveyId, String surveyUniqueId) {
        String mysqlSubQuery = "";
        if (eachQuery.contains(Constants.DTMETADATA)||eachQuery.contains(Constants.DTLINK)||eachQuery.contains(Constants.DTKIOSK)||eachQuery.contains(Constants.DTQR)) {
            // case: dtmetadata eg: dtmetadata.Location=chennai
            mysqlSubQuery = this.getMetaDataSubQuery(eachQuery);
        } else if (eachQuery.contains(Constants.QUESTION)) {
            // case: question eg: question.survyeuuid.questionuuid.responses=5
            mysqlSubQuery = this.getQuestionSubQuery(eachQuery, businessUUID, surveyId);
        } else if (eachQuery.contains(Constants.DT_AGGREGATE)) {
            // case: score eg: dtaggregate.surveyuuid.quesitionuuid.nps=75 , dtaggregate.surveyuuid.quesitionuuid.positive>=25
//            mysqlSubQuery = this.getScoreSubQuery(eachQuery, businessUUID);//TODO
        }
        return mysqlSubQuery;
    }

    /**
     * Method to convert   performace.completerate=100 to subquery
     *
     * @param query
     * @return
     */
    public String getPerformanceSubQuery(String query) {
        String perfQuery = "";
        try {
            // case: performace Prefix.criteria.operator.value
            /*eg: performace.responserate=5,  performace.abandoment> 25, performace.completerate=100, performace.responsecount>100*/
            List<String> strings = this.getQueryPrefixSuffix(query);
            if (strings.size() > 0) {
                String prefix = strings.get(0); // condition Type
                String conditionVal = strings.get(1); // condition value
                String operator = strings.get(2); // operator
                String[] prefixArr = prefix.split("\\.");
                if (prefixArr.length > 2) {
                    String performanceType = prefixArr[3]; // perfromance type ie. response rate, completion rate, response count or Abandonment rate
                    if (performanceType.length() > 0) {
                        /**
                         * SELECT perf.completed AS responseCount,  (perf.completed  * 100 / perf.sent) responseRate,
                         * ( perf.completed* 100 / (perf.completed + perf.opened)) completionRate,
                         * ( (perf.opened - perf.completed)* 100 / (perf.completed + perf.opened)) AS 'abandonmentRate'
                         * FROM (
                         * SELECT COUNT(case when last_action='completed' then (dynamic_token_id) END) AS 'completed',
                         * COUNT(case when last_action='opened' then  (dynamic_token_id)  END) AS 'opened',
                         * COUNT(case when last_action!='token' then (dynamic_token_id) END) AS 'sent'
                         * from token_a120c799_948b_4af2_a141_8998b91cf824 WHERE survey_id=50) AS perf
                         * HAVING (abandonmentRate = -100 AND responseCount > 2 AND responseRate>50 AND completionRate > 50);
                         */
                        String perfTypeVal = "";
                        switch (performanceType){
                            case "responserate":
                                /*Response rate = (number of responses received * 100) / No. of invitations sent*/
                                perfTypeVal = " responseRate ";
                                break;
                            case "completionrate":
                                /*Completion rate = (Total number of surveys users completed/Total number of surveys opened by end users)*100*/
                                perfTypeVal =" completionRate " ;
                                break;
                            case "responsecount":
                                perfTypeVal = " responseCount ";
                                break;
                            case "abandonmentrate":
                                /*Abandonment rate = (No. of people who clicked on survey – No. of people who clicked on Submit at the end of the survey) x 100 / No. of people who clicked on survey*/
                                perfTypeVal = " abandonmentRate ";
                                break;
                        }

                        if(Utils.notEmptyString(perfTypeVal)) {
                            if (operator.equals(Constants.BETWEEN_OPERATOR)) {
                                // convert from dtPserformance.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.responserate~3-23 to
                                String[] betweenValues = conditionVal.replaceAll("\\[|\\]", "").split(",");
                                perfQuery = (" ( "+perfTypeVal+" > " + betweenValues[0] + " and   "+perfTypeVal+" < " + betweenValues[1] + ") ");
                            } else {
                                perfQuery = perfTypeVal + " "  + operator + " " + conditionVal;
                            }
                        }

                        logger.info("performance query {}", perfQuery);
                    }

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return perfQuery;
    }

    /**
     * Method to convert   performace.completerate=100 to subquery
     *
     * @param query
     * @return
     */
    public String getAggregateScoreSubQuery(String query, String businessUniqueId) {
        logger.info("getAggregateScoreSubQuery query start");
        String aggrQuery = "";
        Map queryMap = new HashMap();

        try {
            // case: performace Prefix.criteria.operator.value
            /*eg: dtAggregate.surveyuuid.questionuuid.nps=50,  dtAggregate.surveyuuid.metricuuid.metric > 4, dtAggregate.surveyuuid.questionuuid.positive < 25% */
            List<String> strings = this.getQueryPrefixSuffix(query);
            if (strings.size() > 0) {
                String prefix = strings.get(0); // condition Type
                String conditionVal = strings.get(1); // condition value
                String operator = strings.get(2); // operator
                String[] prefixArr = prefix.split("\\.");

                if (prefixArr.length > 2) {
                    String aggregateType = prefixArr[3].trim(); // aggrgate type ie. nps, metric or sentiment
                    String qIdStr = prefixArr[2];
                    String aggTypeVal = "";
                    String questionKey = "";

                    switch (aggregateType){
                        case "nps":
                            questionKey = triggerDAO.getQestionIdForQUniqueId(businessUniqueId, qIdStr, false); //it will return questionId of nps
                            break;
                        case "metric":
                            questionKey = triggerDAO.getQestionIdForQUniqueId(businessUniqueId, qIdStr, true); // it will return keymetricId of metric question
                            break;
                        case "positive":
                        case "negative":
                        case "neutral":
                            questionKey = aggregateType;
                            break;
                    }
                    if (aggregateType.length() > 0) {
                        if (operator.equals(Constants.BETWEEN_OPERATOR)) {
                            String[] betweenValues = conditionVal.replaceAll("\\[|\\]", "").split(",");
                            aggrQuery = (" ( "+aggregateType+" > " + betweenValues[0] + " and   "+aggregateType+" < " + betweenValues[1] + ") ");
                        } else {
                            aggrQuery = aggregateType + " "  + operator + " " + conditionVal;
                        }
                        queryMap.put(questionKey, aggrQuery);
                        logger.info("aggregate score query {}", aggrQuery);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        logger.info("getAggregateScoreSubQuery query end");
        return new JSONObject(queryMap).toString();
    }

    /**
     * Method to convert trigger condition to json readable sub query
     *
     * @param query trigger condition-> dtmetaData.Location=chennai / dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age~3-23
     * @return json readable subquery-> JSON_EXTRACT(event_metadata,'$.metaData.Location') = 'chennai'
     */
    public String getMetaDataSubQuery(String query) {
        String subQuery = "";
        try {
            List<String> strings = this.getQueryPrefixSuffixAdvFilters(query);
            if (strings.size() > 0) {
                String prefix = strings.get(0); // condition Type eg: dtmetadata.Location / dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age
                String metaDataValue = strings.get(1); // condition value eg: chennai
                String operator = strings.get(2); // operator eg: =
                String[] prefixArr = prefix.split("\\.");
                if (prefixArr.length > 1) {
                    String participantGroupUUID = prefixArr[2];
                    String metadataField = prefixArr.length > 3 ? prefixArr[3] : prefixArr[1];
                    if (metadataField.length() > 0) {
                        if(metaDataValue.equalsIgnoreCase(Constants.ALL)) {
                            // ignore filter metadata value with 'ALL' condition in where part, since all records needed for this metadata field.
                            subQuery = (" JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') != '" + metaDataValue + "' ");
                        }
                        else if (operator.equals(Constants.BETWEEN_OPERATOR)) {
                            // convert from dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age~3-23 to
                            String[] betweenValues = metaDataValue.replaceAll("\\[|\\]", "").split(",");
                            subQuery = (" (JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') > '" + betweenValues[0] + "' and JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') < '" + betweenValues[1] + "') ");
                        }
                        else {
                            String numberPattern = "^-?[0-9]\\d*(\\.\\d+)?$";
                            Pattern pattern1 = Pattern.compile(numberPattern);
                            Matcher matcher1 = pattern1.matcher(metaDataValue);
                            boolean isDate = checkValidDate(metaDataValue, metaDataValue);
                            String metaDataType = triggerDAO.getMetaDataTypeForHeader(participantGroupUUID, metadataField);

                            if(metaDataType.toLowerCase().contains("json")){
                                if(metaDataType.equalsIgnoreCase("json_number")){
                                    //JSON_CONTAINS(json_extract(event_metadata,'$.metaData."Technicians ID"), '205')=1
                                    subQuery = (" JSON_CONTAINS(JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"'), '" + metaDataValue + "')=1");
                                }else{
                                     //JSON_CONTAINS(json_extract(event_metadata,'$.metaData."Technicians"'), '"Technician B"')=1
                                        subQuery = (" JSON_CONTAINS(JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"'), '\"" + metaDataValue + "\"')=1");
                                }
                            }
                            else if(matcher1.matches())
                            {
                                Double metaDataValueIntType = Double.parseDouble(metaDataValue);
                                subQuery = (" cast(JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') as signed) " + operator + " " + metaDataValueIntType + " ");
                            }
                            else if(isDate)
                            {
                                subQuery = (" JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') " + operator + " '" + metaDataValue + "' ");
                            }
                            else{
                                subQuery = (" JSON_EXTRACT(event_metadata,'$.metaData.\"" + metadataField + "\"') " + operator + " '" + metaDataValue + "' ");
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subQuery;
    }

    /**
     * Method to convert  question.surveyuuid.questionuuid.operator.value to subquery
     *
     * @param query
     * @return
     */
    public String getQuestionSubQuery(String query, String businessUniqueId, int surveyId) {
        String subQuery = "";
        try {
            List<String> strings = this.getQueryPrefixSuffix(query);
            if (strings.size() > 0) {
                String prefix = strings.get(0); // condition Type
                String conditionVal = strings.get(1); // condition value
                String operator = strings.get(2); // operator
                String[] prefixArr = prefix.split("\\.");
                if (prefixArr.length > 2) {
                    String questionUniqueId = prefixArr[2]; // question unique id
                    if (questionUniqueId.length() > 0) {
                        Map questionData = triggerDAO.getQuestionDataBySurveyId(surveyId, businessUniqueId, questionUniqueId);
                        String type = questionData.containsKey("type") ? questionData.get("type").toString() : "";
                        String categoryType = prefixArr[3]; // get category type like category, sentiment or responses. this way it will not cause issue if user response have text like category or seniment in open question

                        // get question uuid index from survey table
                        //String questionIndex = getQuestionIndexBySurveyIdId(surveyId, businessUniqueId, questionUniqueId);
                        if (type.equals("open") && (categoryType.contains(Constants.CATEGORY) || categoryType.contains(Constants.SENTIMENT))) {
                            // get text analytics query for open ended question
                            Map eachOpenQuestion = new HashMap();
                            eachOpenQuestion.put(categoryType, conditionVal);
//                            eachOpenQuestion.put("questionId", questionUniqueId);
                            // append dollare delimiter to identify it as open ended question
                            subQuery = Constants.DOLLAR_DELIMITER + new JSONObject(eachOpenQuestion).toString();
                        }
                        //DTV-10742
                        else if((type.equals("open") || type.equals(Constants.MULTI_OPENENDED)) && categoryType.contains(Constants.RESPONSES)) {

                            /***
                             * input : question.4d99a3c4-7b10-4fc7-af6f-29727a82f8bd.75bb54d7-776a-48dd-9d66-26cd8ffcdf06.responses=naveen,kumar,keywords,check,trigger
                             * output : (
                             *     data->'$[0]' LIKE '%naveen%'
                             *     OR data->'$[0]' LIKE '%kumar1%'
                             * )
                             *
                             * input2 : question.4d99a3c4-7b10-4fc7-af6f-29727a82f8bd.75bb54d7-776a-48dd-9d66-26cd8ffcdf06.subquestionUUID+responses=naveen,kumar
                             * output : (
                             *      data -> '$[questionIdx][subquestionIdx]' like "%naveen%"
                             *      or data -> '$[questionIdx][subquestionIdx]' like "%kumar%"
                             *      )
                             */

                            String questionIndex = questionData.containsKey("index") ? questionData.get("index").toString() : "";
                            subQuery = getOpenEndedKeywordResponsesSubQuery(questionData, conditionVal, questionIndex, categoryType);
                        }
                        else {
                            String questionIndex = questionData.containsKey("index") ? questionData.get("index").toString() : "";
                            /***
                             * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.response=A
                             * output : json_contains(json_extract(data,'$[1]'),'32') =1
                             *
                             */
                            if (type.equals("singleChoice") || type.equals("multiChoice")
                                    || type.equals(Constants.RANKING) || type.equals(Constants.DROP_DOWN)
                                    || type.equals(Constants.PICTURE_CHOICE) || type.equals(Constants.POLL)) {

                                 /***
                                 * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.response=A
                                 * output : json_contains(json_extract(data,'$[1]'),'32') =1
                                 *
                                 */
                                subQuery = getMultiChoiceSubQuery(questionData, conditionVal, questionIndex, type, categoryType, operator);
                            }
                            else if(type.equals(Constants.FILE)){
                                /***
                                 * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.status=upload
                                 * output : json_extract(data,'$[1]') != ''
                                 *
                                 */
                                subQuery = getFileTypeSubQuery(conditionVal, questionIndex);
                            }
                            else if( type.equals(Constants.MATRIX_CHOICE)){
                                /***
                                 * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.ui546602-1f70-4e1c-9b91-58d0f10ff9c6=3
                                 * output : json_contains((JSON_EXTRACT(JSON_EXTRACT(data, '$[3]'), '$[1]')), '3') = 1
                                 *
                                 */
                                subQuery = getMatrixChoiceQuestionSubQuery(questionData, conditionVal, questionIndex, categoryType);
                            }else if(type.equals(Constants.MATRIX_RATING) ){
                                /***
                                 * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.ui546602-1f70-4e1c-9b91-58d0f10ff9c6<1
                                 * output : JSON_EXTRACT(JSON_EXTRACT(data, '$[3]'), '$[1]') < 1
                                 *
                                 */
                                subQuery = getMatrixRatingQuestionSubQuery(questionData, conditionVal, questionIndex, categoryType, operator);
                            }else if(type.equals(Constants.MULTI_OPENENDED)){
                                /***
                                 * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.cf546602-1f70-4e1c-9b91-58d0f10ff9c6+MULTI_OPEN_OPTIONS=3
                                 * MULTI_OPEN_OPTIONS = sentiment/category/contains/donotcontain/status
                                 * output : json_contains((JSON_EXTRACT(JSON_EXTRACT(data, '$[3]'), '$[1]')), '3') = 1
                                 *
                                 */
                                subQuery = getMultipleOpenEndedQuestionSubQuery(questionData, conditionVal, questionIndex, categoryType);
                            }
                            else {
                                if (operator.equals(Constants.BETWEEN_OPERATOR)) {
                                    // convert from dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age~3-23 to
                                    String[] betweenValues = conditionVal.replaceAll("\\[|\\]", "").split(",");
                                    //DTV-8823, ratingSlider answer value will be n-1 in events table
                                    betweenValues[0] = type.equalsIgnoreCase(Constants.RATINGSLIDER) ? String.valueOf(Integer.parseInt(betweenValues[0])-1) : betweenValues[0];
                                    betweenValues[1] = type.equalsIgnoreCase(Constants.RATINGSLIDER) ? String.valueOf(Integer.parseInt(betweenValues[1])-1) : betweenValues[1];
                                    subQuery = (" ( json_extract(data,'$[" + questionIndex + "]') > " + betweenValues[0] + " and   json_extract(data,'$[" + questionIndex + "]') < " + betweenValues[1] + ") ");
                                } else {
                                    // pass the question index in data array to get its response value
                                    //DTV-8823, ratingSlider answer value will be n-1 in events table
                                    conditionVal = type.equalsIgnoreCase(Constants.RATINGSLIDER) ? String.valueOf(Integer.parseInt(conditionVal)-1) : conditionVal;
                                    subQuery = (" json_extract(data,'$[" + questionIndex + "]') " + operator + " " + conditionVal);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subQuery;
    }

    /**
     * method to construct query for multi choice and single choice question conditions
     * @param questionData
     * @param conditionVal
     * @param questionIndex
     * @param type
     * @return
     */
    public String getMultiChoiceSubQuery(Map questionData, String conditionVal, String questionIndex, String type,
                                         String rankingOptionId, String operator) {
        String subQuery = "";
        /***
         * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.response=A
         * output : json_contains(json_extract(data,'$[1]'),'32') =1
         *
         */
        List options = (List) questionData.get("options");
        List optionIds = (List) questionData.get("optionIds");
        List otherOptionIds = (List) questionData.get("otherOptionIds");
        logger.info("options" + options);
        logger.info("optionIds" + optionIds);

        if (!conditionVal.equalsIgnoreCase("other")) {
            if(type.equals(Constants.RANKING)){
                /** questions.surveyuuid.questionuuid.rankingoptionId operator rankvalue
                 *  questions.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.101 > 1*/
                subQuery = getRankingQuestionSubQuery(questionIndex, operator, conditionVal, options.size(),  Integer.parseInt (rankingOptionId));
            }else {
                int optionIndex = options.indexOf(conditionVal);
                int optionId = (optionIndex > -1 && optionIndex < optionIds.size()) ? (int) optionIds.get(optionIndex) : -1;
                subQuery = (" json_contains(json_extract(data,'$[" + questionIndex + "]'),'" + optionId + "') =1");
            }
        }
        else {
            // get all options and remove the other options from it
            if (optionIds.containsAll(otherOptionIds)) {
                optionIds.removeAll(otherOptionIds);
            }
            // get the max number of optionId. Assume that greater than option max number will be other option
            int maxId = Integer.parseInt(Collections.max(optionIds).toString());

            if (type.equalsIgnoreCase("singleChoice") || type.equalsIgnoreCase(Constants.POLL)) {
                //json_extract(json_extract(data,'$[1]'),'$[0]') > 114;
                subQuery = (" json_extract(json_extract(data,'$[" + questionIndex + "]'),'$[0]') >" + maxId);
            } else {
                //get length of options and add 1 to include other option
                int mutlichoiceOtherLength = optionIds.size() + 1;
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("(");
                // loop through the length of options with other option
                for (int i = 0; i < mutlichoiceOtherLength - 1; i++) {
                    if (i != 0) {
                        stringBuilder.append(" or");
                    }
                    // append query for each index of multi choice options. this is to search other option id in all index of multi choice response in event table
                    stringBuilder.append(" json_extract(json_extract(data,'$[" + questionIndex + "]'),'$[" + i + "]') >" + maxId);
                }
                stringBuilder.append(")");
                subQuery = stringBuilder.toString();
            }
        }
        return subQuery;
    }

    /**
     * method to get sub query for ranking options
     * @param questionIdx
     * @param operator
     * @param rank
     * @param maxRankSize
     * @return
     */
    public String getRankingQuestionSubQuery(String questionIdx, String operator, String rank, int maxRankSize, int optionId) {

        StringBuilder str = new StringBuilder();
        try {
            boolean isBetween = false;
            boolean hasNextVal = false;
            /**
             * event_data value from event_table: [4, [1139, 1145], [1144], 9, "open ended check", [102, 104, 101, 103]]
             * ranking question in index 5
             * optionIds: 101, 102, 103, 104
             * index: 0, 1, 2, 3
             * lets say get feedback for option 101 based on rank 3.
             * eg if <= 3 means we have to take 2,1,0 (rank -1 will get the corresponding index value)
             *    if < 3 means 1,0 -> query format: json_contains(json_extract(JSON_EXTRACT(DATA, '$[5]'), '$[0]', '$[1]'), '3') =1
             *    if >= 3 means 2,3 -> query format: json_contains(json_extract(JSON_EXTRACT(DATA, '$[5]'), '$[2]', '$[3]'), '3') =1
             *    if > 3 means 3 -> query format: json_contains(json_extract(JSON_EXTRACT(DATA, '$[5]'), '$[3]'), '3') =1
             *    if = 3 means 2 -> query format: json_contains(json_extract(JSON_EXTRACT(DATA, '$[5]'), '$[2]'), '3') =1
             *    if 2-4 between case means 1,2,3 -> query format: json_contains(json_extract(JSON_EXTRACT(DATA, '$[5]'), '$[1]', '$[2]', '$[3]'), '3') =1
             **/

            int i = 0;
            if (operator.equals(Constants.BETWEEN_OPERATOR)) {
                // convert from questions.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.101~3-5
                String[] betweenValues = rank.replaceAll("\\[|\\]", "").split(",");
                i = Integer.parseInt(betweenValues[0]) - 1;
                maxRankSize = Integer.parseInt(betweenValues[1]);
                isBetween = true;
            } else {
                i = operator.equals("<") || operator.equals(">=") || operator.equals("=") ?
                        Integer.parseInt(rank) - 1 : Integer.parseInt(rank); // to get index value from the options list based on the rank. Rank 3 means 2 index in the options list
            }
            // start building the query
            // i will be -2 when rank is -1(for not answered case)
            if(i == -2){
                //SELECT * FROM event_fd06001b_760c_458d_8512_224868e8cdf9 where json_contains(JSON_EXTRACT(DATA, '$[1]'), '114') = 0;
                str.append(" json_contains(JSON_EXTRACT(data,").append("'$[").append(questionIdx).append("]'),");
                str.append("'" + optionId + "') = 0");
            }
            else {
                str.append(" json_contains(json_extract(JSON_EXTRACT(data,").append("'$[").append(questionIdx).append("]'),");

                do {
                    if (operator.contains("<") || operator.equals(">=")) {
                        i--;
                        hasNextVal = i <= 0 ? false : true;
                        str.append("'$[" + i + "]'");
                    } else if (operator.contains(">") || isBetween) {
                        str.append("'$[" + i + "]'");
                        //DTV-8332, if the operator is between/ '>' we need to check for the next value, till maximum rank size is reached
                        //otherwise it will go into infinite loop
                        i++;
                        hasNextVal = isBetween ? (i > maxRankSize - 1 ? false : true) : (i > maxRankSize ? false : true);
                    } else {//equal to case
                        str.append("'$[" + i + "]'");
                    }

                    if (hasNextVal)
                        str.append(",");

                } while (hasNextVal);

                str.append("),").append("'" + optionId + "') = 1");
            }
            logger.info("getRankingQuestionSubQuery --- {}", str.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        return str.toString();
    }

    /**
     * method to get sub query for matrix rating options
     * @param questionData
     * @param value
     * @param questionIdx
     * @param subQuestionId
     * @return
     */
    public String getMatrixChoiceQuestionSubQuery(Map questionData, String value, String questionIdx, String subQuestionId) {

        StringBuilder str = new StringBuilder();
        try {
            /**
             * event_data value from event_table: [4, [1159], [89, 90, 91], [0, 1, 2]]
             * questions_group from event_table: ["", "", "", [2823, 2824, 2825]]
             * matrix rating question in index 3
             *
             * subQuestionIds: 2823, 2824, 2825
             * optionIds: Good, Better, Best
             * index: 0, 1, 2
             **/

            List subQuestionIds =  questionData.containsKey("subQuestionUniqueIds") ?
                    (List) questionData.get("subQuestionUniqueIds") : new ArrayList();
            int subQuestionIdx = subQuestionIds.indexOf(subQuestionId);
            if(subQuestionIdx > -1) {
                // start building the query
                str.append(" json_contains(JSON_EXTRACT(JSON_EXTRACT(data,").append("'$[").append(questionIdx).append("]'), ")
                        .append("'$[").append(subQuestionIdx).append("]'),'").append(value).append("')").append("=1");
            }
            logger.info("getMatrixRatingQuestionSubQuery --- {}", str.toString());
        }catch (Exception e){
            e.printStackTrace();
            logger.error("getMatrixRatingQuestionSubQuery --- {}", e.getMessage());
        }
        return str.toString();
    }


    /**
     * method to get sub query for matrix rating options
     * @param questionData
     * @param value
     * @param questionIdx
     * @param subQuestionId
     * @return
     */
    public String getMatrixRatingQuestionSubQuery(Map questionData, String value, String questionIdx, String subQuestionId, String operator) {

        StringBuilder str = new StringBuilder();
        try {
            /**
             * event_data value from event_table: [4, [1159], [89, 90, 91], [0, 1, 2]]
             * questions_group from event_table: ["", "", "", [2823, 2824, 2825]]
             * matrix rating question in index 3
             *
             * subQuestionIds: 2823, 2824, 2825
             * optionIds: Good, Better, Best
             * index: 0, 1, 2
             **/

            List subQuestionIds =  questionData.containsKey("subQuestionUniqueIds") ?
                    (List) questionData.get("subQuestionUniqueIds") : new ArrayList();
            int subQuestionIdx = subQuestionIds.indexOf(subQuestionId);
            if(subQuestionIdx > -1) {
                // start building the query
                if(operator.equals(Constants.BETWEEN_OPERATOR)){
                    String[] betweenValues = value.replaceAll("\\[|\\]", "").split(",");
                    str.append("(");
                    appendQuery(str, questionIdx, subQuestionIdx, " > ", betweenValues[0]);
                    str.append(" and ");
                    appendQuery(str, questionIdx, subQuestionIdx, " < ", betweenValues[1]);
                    str.append(")");
                }else
                    appendQuery(str, questionIdx, subQuestionIdx, operator, value);
            }
            logger.info("getMatrixRatingQuestionSubQuery --- {}", str.toString());
        }catch (Exception e){
            e.printStackTrace();
            logger.error("getMatrixRatingQuestionSubQuery --- {}", e.getMessage());
        }
        return str.toString();
    }

    private StringBuilder appendQuery(StringBuilder str, String questionIdx, int subQuestionIdx, String operator, String value){
        str.append(" JSON_EXTRACT(JSON_EXTRACT(data,").append("'$[").append(questionIdx).append("]'), ")
                .append("'$[").append(subQuestionIdx).append("]')").append(operator).append(value);
        return str;
    }

    /**
     * method to get sub query for multi open ended questions
     * @param questionData
     * @param value
     * @param questionIdx
     * @param categoryType
     * @return
     */
    public String getMultipleOpenEndedQuestionSubQuery(Map questionData, String value, String questionIdx, String categoryType) {

        StringBuilder str = new StringBuilder();
        try {
            /**
             * event_data value from event_table: [7, ["Kaviyarasi", "34", "kaviv@yopmail.com", "", "Chennai"]]
             * questions_group from event_table: ["", [977, 978, 979, 980, 981]]
             * multi open ended question is in index 1
             *
             * subQuestionIds: 977, 978, 979, 980, 981
             * index: 0, 1, 2, 3, 4
             **/

            /**categoryType value will be:- subquestion_uuid+sentiment / subquestion_uuid+contains /
             * subquestion_uuid+donotcontain / subquestion_uuid+category /
             * subquestion_uuid+status / subquestion_uuid+status */
            String[] multiOpenTypeArr = categoryType.split("\\+");
            String subQuestionId = multiOpenTypeArr[0];
            String multiOpenType = multiOpenTypeArr[1];

            //get the list of subquestion uniqueIds
            List subQuestionIds =  questionData.containsKey("subQuestionUniqueIds") ?
                    (List) questionData.get("subQuestionUniqueIds") : new ArrayList();
            //get the index of subQuestionId from the list
            int subQuestionIdx = subQuestionIds.indexOf(subQuestionId);
            if(subQuestionIdx > -1) {
                // start building the query with subQuestionIdx
                String valueWithOperator = getOperatorForMultiTextQuestion(multiOpenType, value);
                if(valueWithOperator.contains(Constants.DOLLAR_DELIMITER)){
                    str.append(valueWithOperator);
                }else {
                    str.append(" JSON_EXTRACT(JSON_EXTRACT(data,").append("'$[").append(questionIdx).append("]'), ")
                            .append("'$[").append(subQuestionIdx).append("]')").append(valueWithOperator);
                }
            }
            logger.info("getMultiOpenEndedQuestionSubQuery --- {}", str);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("Exception getMultiOpenEndedQuestionSubQuery --- {}", e.getMessage());
        }
        return str.toString();
    }

    /**
     * method to form operator and its the condition based on multiOpenType
     * @param multiOpenType
     * @param value
     * @return
     */
    private String getOperatorForMultiTextQuestion(String multiOpenType, String value){
        String condn = "";
        if(multiOpenType.equals(Constants.CONTAINS))
            condn = " like '%"+value+"%' ";
        else if(multiOpenType.equals(Constants.NOTCONTAINS))
            condn = " not like '%"+value+"%' ";
        else if(multiOpenType.equals(Constants.STATUS))
            condn = value.equals(Constants.ANSWERED) ? " != '' " : " = '' ";
        else if(multiOpenType.equals(Constants.CATEGORY) || multiOpenType.equals(Constants.SENTIMENT)) {
            Map eachOpenQuestion = new HashMap();
            eachOpenQuestion.put(multiOpenType, value);
            // append dollar delimiter to identify it as open ended question to perform TA
            condn = Constants.DOLLAR_DELIMITER + new JSONObject(eachOpenQuestion).toString();
        }
        return condn;
    }


    /**
     * Method to get the operator , tagid and metadata/participantid in string[]
     *
     * @param query
     * @return
     */
    public List getQueryPrefixSuffix(String query) {
        List optionList = new ArrayList();
        String operator = "";
        String[] stringLst = new String[2];
        if (query.contains(">=")) {
            stringLst = query.split(">=");
            operator = ">=";
        } else if (query.contains("<=")) {
            stringLst = query.split("<=");
            operator = "<=";
        } else if (query.contains(">")) {
            stringLst = query.split(">");
            operator = ">";

        } else if (query.contains("<")) {
            stringLst = query.split("<");
            operator = "<";

        } else if (query.contains("!=")) {
            stringLst = query.split("!=");
            operator = "<>";

        } else if (query.contains("=")) {
            String[] stringArr = query.split("=");
            stringLst = stringArr;
            operator = "=";
        } else if (query.contains("~")) { // between operator
            String[] stringArr = query.split("~");
            stringLst = stringArr;
            operator = "~";
        }

        Collections.addAll(optionList, stringLst);
        optionList.add(operator);
        return optionList;
    }

    /**
     * New Function to get prefix and suffix for filter query
     * @param query
     * @return
     */
    public List getQueryPrefixSuffixAdvFilters(String query){
        List optionList = new ArrayList();
        List tempList = new ArrayList();
        List tempOprList = new ArrayList();
        String[] stringLst = new String[2];

        if (query.contains(">=")) {
            tempList.add(query.indexOf(">=",0));
            tempOprList.add(">=");
        }if (query.contains("<=")) {
            tempList.add(query.indexOf("<=",0));
            tempOprList.add("<=");
        }if (query.contains(">")) {
            tempList.add(query.indexOf(">",0));
            tempOprList.add(">");
        }if (query.contains("<")) {
            tempList.add(query.indexOf("<",0));
            tempOprList.add("<");
        }if (query.contains("!=")) {
            tempList.add(query.indexOf("!=",0));
            tempOprList.add("<>");
        }if (query.contains("=")) {
            tempList.add(query.indexOf("=",0));
            tempOprList.add("=");
        }if (query.contains("~")) { // between operator
            tempList.add(query.indexOf("~",0));
            tempOprList.add("~");
        }
        sort2Lists(tempList,tempOprList);

        int oprLength = tempOprList.get(0).toString().length();
        int temp = Integer.parseInt(tempList.get(0).toString());
        stringLst[0] = query.substring(0,temp);
        stringLst[1] = query.substring((temp+oprLength),query.length());
        String operator = tempOprList.get(0).toString();

        //When empty values are sent to the filter query
        //Ex. filteruuid.participantgroupuuid.fieldName.operator instead of filteruuid.participantgroupuuid.fieldName.operator.value
        if(stringLst.length<2){
            String[] newStringArray = new String[2];
            newStringArray[0] = stringLst[0];
            newStringArray[1] = "";
            stringLst = newStringArray;
        }

        Collections.addAll(optionList,stringLst);
        optionList.add(operator);
        logger.info("prefix suffix {}", optionList.toString());
        return optionList;
    }

    /**
     * Function to sort two lists based on each other
     * @param dataList
     * @param idList
     */
    public  void sort2Lists(List dataList, List idList){
        for(int is = 0;is<dataList.size()-1;is++) {
            for (int js = is + 1; js < dataList.size(); js++) {
                if(dataList.get(js).toString().compareTo(dataList.get(is).toString()) < 0)
                {
                    String tempString = dataList.get(js).toString();
                    dataList.set(js, dataList.get(is));
                    dataList.set(is, tempString);
                    tempString = idList.get(js).toString();
                    idList.set(js, idList.get(is));
                    idList.set(is, tempString);
                }
            }
        }
    }

    /**
     * Method to compute datetime with interval values in days based on the intervalType
     *
     * @param triggerConfig
     * @return
     */
    public Map ComputeDateWithIntervals(Object triggerConfig, String timezone) {
        logger.info("compute date with intervals");
        Map resultMap = new HashMap();
        String formattedDate = "";
        boolean isAggregateEvent = false;
        boolean isFrequencyEvent = false; //DTV-5315
        long intervalInMinutes = 0; //DTV-5315
        String notifyTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD_HH_MM_SS);
        Calendar ex = Calendar.getInstance();


        /*ex.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println("calendar ");
        System.out.println(ex);*/



        try {
            TriggerConfig config = Utils.isNotNull(triggerConfig) ? mapper.readValue(triggerConfig.toString(), TriggerConfig.class) : null;
            if (Utils.isNotNull(config)) {
                // get interval type, values will be either one of this hours/minutes/days/weeks/months
                String intervalType = config.getIntervalType();

                // get interval value which will hold only integers
                int interval = Utils.isNotNull(config.getInterval()) ? config.getInterval() : 0 ;

                // get notify time, at which time the notification will be sent
                notifyTime = config.getNotifyTime();
                isAggregateEvent = config.getAggregate();
                isFrequencyEvent = config.getFrequency();

                if(Utils.isNotNull(intervalType) && Utils.notEmptyString(intervalType)) {
                    logger.info("delayed notification case");
                    // Delayed notifications
                    switch (intervalType) {
                        case "minutes":
                            ex.add(Calendar.MINUTE, interval);
                            //DTV-5315, calculate reminder minutes
                            intervalInMinutes = interval;
                            break;
                        case "hours":
                            ex.add(Calendar.HOUR, interval);
                            //DTV-5315, calculate reminder minutes
                            intervalInMinutes = TimeUnit.MINUTES.convert(interval, TimeUnit.HOURS);
                            break;
                        case "days":
                            ex.add(Calendar.DATE, interval);
                            //DTV-5315, calculate reminder minutes
                            intervalInMinutes = TimeUnit.MINUTES.convert(interval, TimeUnit.DAYS);
                            break;
                        case "weeks":
                            ex.add(Calendar.WEEK_OF_MONTH, interval);
                            //DTV-5315, calculate reminder minutes
                            intervalInMinutes = TimeUnit.MINUTES.convert(interval * 7, TimeUnit.DAYS);
                            break;
                        case "months":
                            ex.add(Calendar.MONTH, interval);
                            //DTV-5315, calculate reminder minutes
                            Calendar calendar = Calendar.getInstance();
                            Date date1 = calendar.getTime();
                            ex.add(Calendar.MONTH, (int) interval);
                            Date date2 = calendar.getTime();
                            long diff = date2.getTime() - date1.getTime();
                            intervalInMinutes = TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS);
                            break;
                        default:
                            break;
                    }

                    formattedDate = sdf.format(ex.getTime());

                }else{
                    logger.info("immediate case");
                    Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
                    Date currentDate = new Date(currentTimestamp.getTime());
                    formattedDate = sdf.format(currentDate);

                }

                if (Utils.isNotNull(notifyTime) && Utils.notEmptyString(notifyTime)) {
                    logger.info("notify case ");
                    // if notifyTime available, set the values in the calender: aggregate case
                    /*logger.info("aggregate case");
                    String[] timeArr = notifyTime.split(":");

                    if (timeArr.length > 0) {
                        ex.set(Calendar.HOUR, Integer.parseInt(timeArr[0]));
                        ex.set(Calendar.MINUTE, Integer.parseInt(timeArr[1]));
                    }

                    formattedDate = sdf.format(ex.getTime());*/

                    //new logic
                    String currentDateTimezone = Utils.getCurrentDateInTimezone(timezone);
                    String prefix = currentDateTimezone.substring(0,10);
                    formattedDate = prefix + " "+notifyTime;
                    formattedDate = Utils.getUTCDateAsString(formattedDate,timezone);


                }




                // convert local time to UTC
                //Commenting this part. Otherwise the notification start time advances
                //formattedDate = Utils.getUTCDateAsString(formattedDate, timezone);

            }else{
                logger.info("immediate case");
                Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
                Date currentDate = new Date(currentTimestamp.getTime());
                formattedDate = sdf.format(currentDate);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        resultMap.put(Constants.FORMATTED_DATE, formattedDate);
        resultMap.put(Constants.ISAGGREGATE, isAggregateEvent);
        resultMap.put(Constants.ISFREQUENCY, isFrequencyEvent);
        //DTV-5315, calculate reminder minutes
        resultMap.put("intervalInMinutes", (int) intervalInMinutes);
        resultMap.put("notifyTime", notifyTime);
        return resultMap;
    }

    /**
     * method to get active trigger notification and notify via email / push
     */
   /* public void sendNotifications() {
        List notifyEmailList = new ArrayList();
        List<Integer> notifyIdsList = new ArrayList();
        // get list of active notification details to send
        List notifyTriggerList = triggerDAO.getActiveTriggerNotifications();
        Iterator iterator = notifyTriggerList.iterator();
        Map fromEmailMap = new HashMap();
        fromEmailMap.put("email", fromEmail);
        fromEmailMap.put("name", "DropThought-alerts");

        while (iterator.hasNext()) {
            Map triggerNotifyMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(triggerNotifyMap)) {
                    List toEmailList = new ArrayList();
                    String channel = triggerNotifyMap.get(Constants.CHANNEL).toString();
                    String subject = triggerNotifyMap.get(Constants.SUBJECT).toString();
                    String message = triggerNotifyMap.get(Constants.MESSAGE).toString();
                    int notifyId = (int) triggerNotifyMap.get(Constants.ID);
//                    int scheduleId = (int) triggerNotifyMap.get(Constants.SCHEDULE_ID);
                    List emailList = Utils.isNotNull(triggerNotifyMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerNotifyMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>();

                    Iterator itr = emailList.iterator();
                    if (channel.equals(Constants.CHANNEL_EMAIL)) {
                        // email notification param reqeust
                        while (itr.hasNext()) {
                            Map emailMap = new HashMap();
                            emailMap.put("email", itr.next().toString());
                            toEmailList.add(emailMap);
                        }
                        Map emailMap = new HashMap();
                        emailMap.put("from", fromEmailMap);
                        emailMap.put("tos", toEmailList);
                        emailMap.put("subject", subject);
                        emailMap.put("body", message);
                        notifyEmailList.add(emailMap);
                    } else if (channel.equals(Constants.CHANNEL_PUSH)) {
                        // push notification param request
                        //TODO need to set proper request structure
                        Map surveyDataMap = new HashMap();
                        Map dataMap = new HashMap();
                        Map eachUserDataMap = new HashMap();
                        surveyDataMap.put(Constants.PUSHMESSAGE, message);
                        dataMap.put(Constants.DEEP_LINK, deeplinkTrigger);
                        dataMap.put(Constants.DATA, surveyDataMap);
                        JSONArray jsonEmailArray = new JSONArray(emailList);
                        eachUserDataMap.put(Constants.APPNAME, appName);
                        eachUserDataMap.put(Constants.EMAILS, jsonEmailArray);
                        eachUserDataMap.put(Constants.PUSHMESSAGE, message);
                        eachUserDataMap.put(Constants.DATA, dataMap);
                        JSONObject json = new JSONObject(eachUserDataMap);
                        String inputJson = json.toString();
                        callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);
                    }
                    // add ids in the list to send notification
                    notifyIdsList.add(notifyId);
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if (notifyEmailList.size() > 0) {
            String inputJson = org.json.simple.JSONArray.toJSONString(notifyEmailList);
            logger.info("email request {}", inputJson);
            //prevent issues in YAML file harcoding the url
//            emailNotificationUrl = "https://utility-test.dropthought.com/api/v1/notification/email/bulk";
            callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_EMAIL, emailNotificationUrl);
        }
        if (notifyIdsList.size() > 0)
            triggerDAO.updateNotificationStatusByIds(notifyIdsList, Constants.SENT);
    }*/

    /**
     * method to get file content from template/managerNotification.html
     *
     * @param template
     * @return
     */
    private String getFileContent(String template) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("template/" + template);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return result.toString();
    }

    /**
     * method to get active trigger notification and notify via email / push
     */
  /*  public void sendNotificationsV1() {
        List notifyEmailList = new ArrayList();
        List<Integer> notifyIdsList = new ArrayList();
        // get list of active notification details to send
        List notifyTriggerList = triggerDAO.getActiveTriggerNotificationsV1();
        Iterator iterator = notifyTriggerList.iterator();
        Map fromEmailMap = new HashMap();
        fromEmailMap.put("email", fromEmail);
        // loop through each notification records
        while (iterator.hasNext()) {
            Map triggerNotifyMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(triggerNotifyMap)) {
                    List toEmailList = new ArrayList();
                    String channel = triggerNotifyMap.get(Constants.CHANNEL).toString();
                    String subject = triggerNotifyMap.get(Constants.SUBJECT).toString();
                    String messageStr = triggerNotifyMap.get(Constants.MESSAGE).toString();

                    int notifyId = (int) triggerNotifyMap.get(Constants.ID);
                    List emailList = Utils.isNotNull(triggerNotifyMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerNotifyMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>();
                    boolean addFeedback = (int) triggerNotifyMap.get(Constants.ADD_FEEDBACK) == 0 ? false : true;
                    String query = triggerNotifyMap.containsKey(Constants.CONDITION_QUERY) ? triggerNotifyMap.get(Constants.CONDITION_QUERY).toString() : "";
                    int businessId = (int) triggerNotifyMap.get(Constants.BUSINESS_ID);
                    int surveyId = (int) triggerNotifyMap.get(Constants.SURVEY_ID);
                    int triggerId = (int) triggerNotifyMap.get(Constants.TRIGGER_ID);
                    //String hostUrl = Utils.isNotNull(triggerNotifyMap.get(Constants.HOST)) ? triggerNotifyMap.get(Constants.HOST).toString() : "http://localhost:4200";
                    String hostUrl = "https://" + dtpDomain;
                    String token = Utils.isNotNull(triggerNotifyMap.get(Constants.SUBMISSION_TOKEN)) ? triggerNotifyMap.get(Constants.SUBMISSION_TOKEN).toString() : "";

                    String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);
                    String surveyUUID = triggerDAO.getSurveyUniqueIdById(surveyId, businessUniqueId);

                    Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
                    String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
                    String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
                    String triggerCondition = triggerMap.get(Constants.TRIGGER_CONDITION).toString();

                    // replace tag value with corresponding respondent meta data info
                    List tagsToReplace = Utils.mergeListAndEliminateDuplicates(Utils.getTokens(subject), Utils.getTokens(messageStr));
                    if (tagsToReplace.size() > 0) {
                        Map participantRecords = triggerDAO.getEventBySubmissionToken(surveyUUID, token);
                        Map substituteTokens = this.computeParticipantSubstituteTokens(participantRecords, tagsToReplace);
                        subject = Utils.replaceTokens(subject, substituteTokens);
                        messageStr = Utils.replaceTokens(messageStr, substituteTokens);
                    }

                    // set sender email id list
                    if (channel.equals(Constants.CHANNEL_EMAIL)) {
                        // email notification param reqeust
                        Iterator itr = emailList.iterator();
                        while (itr.hasNext()) {
                            Map emailMap = new HashMap();
                            emailMap.put("email", itr.next().toString());
                            toEmailList.add(emailMap);
                        }
                    }
                    if (Utils.notEmptyString(token)) {
                        // immediate or delayed notification
                        String template = addFeedback ? "immediateOrDelayNotifyWithFeedback.html" : "immediateOrDelayedNotify.html";
                        String htmlContent = getFileContent(template);
                        try {

                            Map participantMap = triggerDAO.getParticipantByToken(token, surveyUUID);
                            Map metaDataMap = participantMap.containsKey("metadata") ? mapper.readValue(participantMap.get("metadata").toString(), HashMap.class) : new HashMap();
                            StringBuffer respondentStr = new StringBuffer();
                            Set keySet = metaDataMap.keySet();
                            Iterator itr = keySet.iterator();
                            while (itr.hasNext()) {
                                String metaDataField = (String) itr.next();
                                String metaDataVal = metaDataMap.get(metaDataField).toString();
                                respondentStr.append("<li style='list-style-type: none;margin:0 0 10px 0;padding:0;'><label class='lead' style='color: #000; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 16px; font-weight: 600; line-height: 21px; margin: 0;padding: 0; text-align: left;'>");
                                respondentStr.append(metaDataField).append(": ");
                                respondentStr.append("</label><span style='color:#666666;font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0; padding: 0; text-align: left;'>");
                                respondentStr.append(metaDataVal);
                                respondentStr.append("</span></li>");
                            }
                            String respondentId = token;
                            String respondentName = participantMap.containsKey(Constants.NAME) ? participantMap.get(Constants.NAME).toString() : Constants.EMPTY;
                            String timeStamp = participantMap.containsKey(Constants.CREATED_TIME) ? participantMap.get(Constants.CREATED_TIME).toString() : Constants.EMPTY;

                            StringBuffer feedbackStr = new StringBuffer();
                            if (addFeedback) {
                                Map eventMap = this.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", false);
                                List responses = (List) eventMap.get("responses");
                                List questionText = (List) eventMap.get("questions");
                                Iterator itrResponse = responses.iterator();
                                Iterator itrQuestion = questionText.iterator();

                                while (itrResponse.hasNext()) {
                                    String response = (String) itrResponse.next();
                                    String question = (String) itrQuestion.next();
                                    feedbackStr.append("<li style='list-style-type: decimal;margin:0 0 10px 0;padding:0 0 0 8px;'><p class='lead' style='color: #000000; font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0 0 5px 0; padding: 0; text-align: left;'>");
                                    feedbackStr.append(question);
                                    feedbackStr.append("</p><p style='color:#757989;font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0; padding: 0; text-align: left;'>");
                                    feedbackStr.append(response);
                                    feedbackStr.append("</p></li>");
                                }
                            }
                            String message = "";
                            HashMap templateParam = new HashMap();
                            templateParam.put("feedback", feedbackStr);
                            templateParam.put("message", messageStr);
                            templateParam.put("triggerName", triggerName);
                            templateParam.put("respondentInfo", respondentStr);
                            StrSubstitutor sub = new StrSubstitutor(templateParam);
                            message = sub.replace(htmlContent);

                            if (channel.equals(Constants.CHANNEL_EMAIL)) {
                                Map emailMap = new HashMap();
                                emailMap.put("from", fromEmailMap);
                                emailMap.put("tos", toEmailList);
                                emailMap.put("subject", subject);
                                emailMap.put("body", message);
                                notifyEmailList.add(emailMap);
                            } else if (channel.equals(Constants.CHANNEL_PUSH)) {
                                // push notification param request
                                if(messageStr.contains("\"")){
                                    messageStr = messageStr.replace("\"", "\\\"");
                                }
                                if(messageStr.contains("\t")){
                                    messageStr = messageStr.replace("\t", "\\t");
                                }
                                if(messageStr.contains("\n")){
                                    messageStr = messageStr.replace("\n", "\\n");
                                }
                                Map surveyDataMap = new HashMap();
                                Map dataMap = new HashMap();
                                Map eachUserDataMap = new HashMap();
                                surveyDataMap.put(Constants.SURVEYUUID, surveyUUID);
                                surveyDataMap.put(Constants.RESPONDENT_ID, respondentId);
                                surveyDataMap.put(Constants.TIME_STAMP, timeStamp);
                                surveyDataMap.put(Constants.SURVEY_NAME, surveyName);
                                surveyDataMap.put(Constants.RESPONDENT_NAME, respondentName);
                                surveyDataMap.put(Constants.PUSHMESSAGE, messageStr);
                                dataMap.put(Constants.DEEP_LINK, deeplinkTrigger);
                                dataMap.put(Constants.DATA, surveyDataMap);
                                JSONArray jsonEmailArray = new JSONArray(emailList);
                                eachUserDataMap.put(Constants.APPNAME, appName);
                                eachUserDataMap.put(Constants.EMAILS, jsonEmailArray);
                                eachUserDataMap.put(Constants.TITLE, surveyName);
                                eachUserDataMap.put(Constants.PUSHMESSAGE, messageStr);
                                eachUserDataMap.put(Constants.DATA, dataMap);
                                JSONObject json = new JSONObject(eachUserDataMap);
                                String inputJson = json.toString();
                                callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);
                            }
                            // add ids in the list to send notification
                            notifyIdsList.add(notifyId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        }
                    } else {
                        //aggregate case
                        List participantList = triggerDAO.getMatchedParticipants(query, businessUniqueId);
                        String triggerReadableText = this.getTriggerReadableText(triggerCondition, businessUniqueId);
                        String template = "aggregateNotification.html";
                        String htmlContent = getFileContent(template);
                        String link = hostUrl + "/" + Constants.LANGUAGE_EN + "/programreport/" + surveyUUID+"/1/0"; //TODO link to respondent tab

                        HashMap templateParam = new HashMap();
                        templateParam.put("message", messageStr);
                        templateParam.put("triggerName", triggerName);
                        templateParam.put("triggerCondition", triggerReadableText);
                        templateParam.put("link", link);
                        templateParam.put("tokenCount", participantList.size());
                        StrSubstitutor sub = new StrSubstitutor(templateParam);
                        String message = sub.replace(htmlContent);

                        if (channel.equals(Constants.CHANNEL_EMAIL)) {
                            Map emailMap = new HashMap();
                            emailMap.put("from", fromEmailMap);
                            emailMap.put("tos", toEmailList);
                            emailMap.put("subject", subject);
                            emailMap.put("body", message);
                            notifyEmailList.add(emailMap);
                        } else if (channel.equals(Constants.CHANNEL_PUSH)) {
                            // push notification param request
                            if(messageStr.contains("\"")){
                                messageStr = messageStr.replace("\"", "\\\"");
                            }
                            if(messageStr.contains("\t")){
                                messageStr = messageStr.replace("\t", "\\t");
                            }
                            if(messageStr.contains("\n")){
                                messageStr = messageStr.replace("\n", "\\n");
                            }
                            Map surveyDataMap = new HashMap();
                            Map dataMap = new HashMap();
                            Map eachUserDataMap = new HashMap();
                            surveyDataMap.put(Constants.PUSHMESSAGE, messageStr);
                            dataMap.put(Constants.DEEP_LINK, deeplinkTrigger);
                            dataMap.put(Constants.DATA, surveyDataMap);
                            JSONArray jsonEmailArray = new JSONArray(emailList);
                            eachUserDataMap.put(Constants.APPNAME, appName);
                            eachUserDataMap.put(Constants.EMAILS, jsonEmailArray);
                            eachUserDataMap.put(Constants.PUSHMESSAGE, messageStr);
                            eachUserDataMap.put(Constants.DATA, dataMap);
                            JSONObject json = new JSONObject(eachUserDataMap);
                            String inputJson = json.toString();
                            callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);
                        }
                        // add ids in the list to send notification
                        notifyIdsList.add(notifyId);
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        if (notifyEmailList.size() > 0) {
            String inputJson = org.json.simple.JSONArray.toJSONString(notifyEmailList);
            //prevent issues in YAML file harcoding the url
//            emailNotificationUrl = "https://utility-test.dropthought.com/api/v1/notification/email/bulk";
            logger.info("email request {}", inputJson);
            callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_EMAIL, emailNotificationUrl);
        }
        if (notifyIdsList.size() > 0)
            triggerDAO.updateNotificationStatusByIds(notifyIdsList, Constants.SENT);
    }*/

    /**
     * method to get trigger condition to readable text format
     *
     * @param triggerCondition eg: dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age=23 && question.b93afb71-883a-474e-b19b-0300e91e9fce.Please rate=5
     * @return Age equal to 23 and Please rate equal to 5
     */
    public String getTriggerReadableText(String triggerCondition, String businessUniqueId, Map npsMetricMap) {
        StringBuilder strCondition = new StringBuilder();
        try {
            if (Utils.notEmptyString(triggerCondition)) {
                Map operatorMap = new HashMap();
                operatorMap.put("||", "or");
                operatorMap.put("&&", "and");

                String[] operatorArr = {"<=","=", ">=", "<", ">", "~"};
                String[] replaceTextArr = {" less than equal to ", " equal to ", " greater than equal to ", " less than ", " greater than ", " between "};

                String[] conditionArr = triggerCondition.split("&&|\\|\\|");
                List operators = new ArrayList();
                // loop through sub queries of trigger condition strings
                for (int i = 0; i < conditionArr.length; i++) {
                    // replace empty value to each index query String in trigger condition, this will replace all the string values and only operator will be there.
                    triggerCondition = triggerCondition.replaceFirst(Pattern.quote(conditionArr[i]), "");// replaceFirst used, since string with test1 and test10 both values gets replaced for test1. Since replaceFirst's 1st parameter is assumed to be a regex, we need to escape special characters. we can use Pattern.quote(lock) to escape it.
                }
                // loop through the operator in trigger condition
                for (int k = 0; k < triggerCondition.length(); k = k + 2) {
                    // && or || operator length is 2, so substring with length 2
                    String eachOperator = triggerCondition.substring(k, k + 2);
                    // add the operator to the list
                    operators.add(eachOperator);
                }
                for (int j = 0; j < conditionArr.length; j++) {
                    String eachCondition = conditionArr[j];
                    String eachValue = "";
                    strCondition.append("<strong style=\"font-weight: 700;color:#000000;margin:0;padding:0;\">");
                    if (eachCondition.contains(Constants.DTMETADATA)||eachCondition.contains(Constants.DTQR)||eachCondition.contains(Constants.DTKIOSK)||eachCondition.contains(Constants.DTLINK)) {
                        /**dtmetadata.e78b9484-fb96-42f5-976f-ac31a6a3a484.78969409-800d-4d38-8b16-c03bbbae2734.Age=23*/
                        /* substring eachcondition to get only metadata,operator and value*/
                        eachValue = eachCondition.substring(85);
                        eachValue = StringUtils.replaceEach(eachValue, operatorArr, replaceTextArr);

                    } else if (eachCondition.contains(Constants.QUESTION)) {
                        /**dtperformance.b93afb71-883a-474e-b19b-0300e91e9fce.b93afb71-883a-474e-b19b-0300e91e9fce.Please rate=5*/
                        if(!eachCondition.contains("category") && !eachCondition.contains("sentiment"))
                        {
                            eachValue = this.getRatingString(eachCondition, businessUniqueId);
                        }else{
                            eachValue = eachCondition.substring(83);
                            eachValue = StringUtils.replaceEach(eachValue, operatorArr, replaceTextArr);
                        }
                    } else if (eachCondition.contains(Constants.PERFORMANCE)) {
                        /**dtperformance.b93afb71-883a-474e-b19b-0300e91e9fce.b93afb71-883a-474e-b19b-0300e91e9fce.responserate=5*/
                        eachValue = eachCondition.substring(88);
                        eachValue = StringUtils.replaceEach(eachValue, operatorArr, replaceTextArr);

                    } else if (eachCondition.contains(Constants.DT_AGGREGATE)) {
                         /**dtAggregate.surveyuuid.sentiment.positive operator value
                          * dtAggregate.surveyuuid.metricuuid.metric operator value
                          * dtAggregate.surveyuuid.questionuuid.nps operator value*/
                         if(eachCondition.contains(Constants.SENTIMENT)){
                             eachValue = eachCondition.substring(58);
                         }else { //nps and mertic case
                             String strArr[] = eachCondition.split("\\.");
                             String questionId = strArr.length > 2 ? strArr[2] : "";
                             String eachCondn = strArr.length > 3 ? strArr[3] : "";

                             String npsMetricStr = npsMetricMap.containsKey(questionId) ? npsMetricMap.get(questionId).toString() : "";
                             if (eachCondn.contains(Constants.NPS)) {
                                 eachValue = eachCondn.replaceFirst(Constants.NPS, npsMetricStr);
                             }else if(eachCondn.contains(Constants.METRIC)){
                                 eachValue = eachCondn.replaceFirst(Constants.METRIC, npsMetricStr);
                             }
                         }

                        eachValue = StringUtils.replaceEach(eachValue, operatorArr, replaceTextArr);
                    }
                    strCondition.append(eachValue).append(" ");
                    strCondition.append("</strong>");

                    if(operators.size() > 0 && j < conditionArr.length-1)
                        strCondition.append(" ").append(operatorMap.get(operators.get(j))).append(" ");
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  strCondition.length() > 0 ? strCondition.append(".").toString() : strCondition.toString();
    }

    /**
     * method to get nps question brand for the survey
     * @param surveyMap
     * @return
     */
    public Map getNpsBrandMetricNames(Map surveyMap, String businessUniqueId) {
        Map npsMap = new HashMap();
        try {

            String languagesString = surveyMap.get("languages").toString();
            List languages = mapper.readValue(languagesString, ArrayList.class);
            // get index of language given in the request. as of now default to English
            int languageIndex = (languages.indexOf("en") > -1) ? languages.indexOf("en") : 0;
            // get metricMap String
            String metricMapString = surveyMap.get("metrics_map").toString();
            // convert questions string to list
            Map metricMap =  Utils.notEmptyString(metricMapString) ? mapper.readValue(metricMapString, HashMap.class) : new HashMap();
            List metricMapList = metricMap.size() > 0 ? new ArrayList(metricMap.keySet()) : new ArrayList();
            Map metricMaps = triggerDAO.getKeyMetricByNameForBusiness(businessUniqueId, metricMapList);

            // get questions String
            String multiQuestionString = surveyMap.get("multi_questions").toString();
            // convert questions string to list
            List multiQuestions = mapper.readValue(multiQuestionString, ArrayList.class);
            // get language index based questions from the list
            List questionDatas = (List) multiQuestions.get(languageIndex);
            Iterator questionDataIterator = questionDatas.iterator();
            while (questionDataIterator.hasNext()) {
                try {
                    //begin while loop
                    Map eachQuestionData = (Map) questionDataIterator.next();
                    String eachQuestionType = eachQuestionData.get("type").toString();
                    String eachQuestionUUID = eachQuestionData.get("questionId").toString();
                    String eachQuestionBrand= eachQuestionData.get("questionBrand").toString();

                    if (eachQuestionType.equals("nps")) {
                        npsMap.put(eachQuestionUUID, "NPS - " + eachQuestionBrand);
                    }
                    //end while loop

                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error("exception at getNpsBrandNames inside whileloop. Msg {} ", e.getMessage());
                }
            }
            //merge nps and metics together in the output
            npsMap.putAll(metricMaps);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at getNpsBrandNames outer side. Msg {} ", e.getMessage());
        }
        return npsMap;
    }



    /**
     * method to get string value for rating number
     * @param eachTriggerCondition
     * @param businessUUID
     * @return
     */
    private String getRatingString(String eachTriggerCondition, String businessUUID) {
        String ratingStr = "";
        String[] operatorArr = {"<=","=", ">=", "<", ">", "~"};
        String[] replaceTextArr = {" less than equal to ", " equal to ", " greater than equal to ", " less than ", " greater than ", " between "};
        try {
            List<String> strings = getQueryPrefixSuffixAdvFilters(eachTriggerCondition);

            String prefix = strings.get(0); // condition Type : question.surveyuuid.questionuuid
            String conditionVal = strings.get(1); // condition value
            String operator = strings.get(2); // operator
            String[] prefixArr = prefix.split("\\.");
            if (prefixArr.length > 2) {
                String surveyUniqueId = prefixArr[1]; // question unique id
                String questionUniqueId = prefixArr[2]; // question unique id
                String categoryType = prefixArr[3]; // get category type like category, sentiment or response. this way it will not cause issue if user response have text like category or seniment in open question

                String operatorInText = StringUtils.replaceEach(operator, operatorArr, replaceTextArr);

                Map surveyRecord = triggerDAO.getSurveyDetailsBySurveyUniqueId(surveyUniqueId, businessUUID);
                if (surveyRecord != null && surveyRecord.containsKey("id")) {
                    // get languages of the survey
                    String languagesString = surveyRecord.get("languages").toString();
                    List languages = mapper.readValue(languagesString, ArrayList.class);
                    // get index of language given in the request. as of now default to English
                    int languageIndex = (languages.indexOf("en") > -1) ? languages.indexOf("en") : 0;
                    // get questions String
                    String multiQuestionString = surveyRecord.get("multi_questions").toString();
                    // convert questions string to list
                    List multiQuestions = mapper.readValue(multiQuestionString, ArrayList.class);
                    // get language index based questions from the list
                    List questionDatas = (List) multiQuestions.get(languageIndex);
                    Iterator questionDataIterator = questionDatas.iterator();
                    while (questionDataIterator.hasNext()) {
                        try {
                            //begin while loop
                            Map eachQuestionData = (Map) questionDataIterator.next();
                            String eachQuestionType = eachQuestionData.get("type").toString();
                            String eachQuestionUUID = eachQuestionData.get("questionId").toString();
                            String eachQuestionText = eachQuestionData.get("questionTitle").toString();
                            /**
                             * reversing the options for handling it better in UI
                             */
                            if (eachQuestionUUID.equals(questionUniqueId)) {
                                if (eachQuestionType.contains("rating")) {
                                    List options = (eachQuestionData.containsKey("options") && Utils.isNotNull(eachQuestionData.get("options"))) ? (List) eachQuestionData.get("options") : new ArrayList();
                                    Collections.reverse(options);
                                    int ratingIdx = Integer.parseInt(conditionVal.trim());
                                    ratingStr = options.get(ratingIdx).toString();
                                } else {
                                    ratingStr = conditionVal;
                                }
                                // eg:  Single choice equal to opt3 / On a scale of 0 - 10, how likely are you to recommend us to your colleagues and friends? greater than 1
                                return eachQuestionText + " " + operatorInText + " " + ratingStr;
                            }
                            //end while loop
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return ratingStr;
    }

    /**
     * method to replace tag with is value
     * @param eventMap
     * @param tokens
     * @return
     */
    public Map computeParticipantSubstituteTokens(Map eventMap, List tokens) {
        Map subsituteTokens = new HashMap();
        Map<String, Object> eventDataMap = new HashMap();
        try {
            if (eventMap.containsKey("metaData") && eventMap.get("metaData") instanceof Map) {
                eventDataMap = (Map) eventMap.get("metaData");
            } else {
                eventDataMap = eventMap.containsKey("metaData") ? mapper.readValue(eventMap.get("metaData").toString(), HashMap.class) : new HashMap();
            }

            Iterator iterator = tokens.iterator();
            while (iterator.hasNext()) {
                String eachToken = iterator.next().toString();
                String recipientDataValue =  eventDataMap.containsKey(eachToken) ? eventDataMap.get(eachToken).toString() : "";
                subsituteTokens.put(eachToken, recipientDataValue);
            }

            logger.info("subsituteTokens {} ", subsituteTokens);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return subsituteTokens;
    }

    /**
     * Method to call rest api
     */
    private Map callRestApi(String inputJson, HttpMethod method, String channel, String uri) {
        Map responseMap = new HashMap();
        logger.info(inputJson);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        logger.info("FCM key {}",fcmKey);
        logger.info("channel {}",channel);
        if (channel.equals(Constants.CHANNEL_PUSH))
            headers.set("x-api-key", fcmKey);
        logger.info("headers {}",headers);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("Notification request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    responseMap = (Map) responseEntity.getBody();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseMap;
    }

    /**
     * method to get users details by passing triggerId & businessUUID
     *
     * @param triggerId
     * @param businessUniqueId
     */
    public List getUsersByTriggerId(String triggerId, String businessUniqueId) {
        List users = new ArrayList();
        int KINGUSER = 1;
        int DEFAULTUSER = 0;
        int CSUSER = 2;
        int ADMIN = 1;
        int READONLY = 2;
        int READWRITE = 3;
        int SUPERADMIN = 4;
        try {
            logger.info("listing users by businessUUID");
            Map triggerMap = triggerDAO.getTriggerById(businessUniqueId, triggerId);
            if (Utils.isNotNull(triggerMap) && triggerMap.containsKey(Constants.TRIGGER_UUID)) {
                int surveyId = Utils.isNotNull(triggerMap.get(Constants.SURVEY_ID)) ? Integer.parseInt(triggerMap.get(Constants.SURVEY_ID).toString()) : 0;
                String triggerCondition = Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : "";
                int createdBy = Utils.isNotNull(triggerMap.get(Constants.CREATED_BY)) ? Integer.parseInt(triggerMap.get(Constants.CREATED_BY).toString()) : 0;

                //if trigger is created by read only user, only him/her should be listed as users DTV-2871
                // adding read write user also as per SE-290
                int roleId = triggerDAO.getRoleIdByUserId(createdBy);
                //read-only & read-write users
                if (roleId == READONLY || roleId == READWRITE) {
                    Map userList = triggerDAO.getUsersByBusinessUserId(businessUniqueId, createdBy);
                    users.add(userList);
                    return users;
                } else {
                    //admin user
                    List validUsers = this.getValidUsersByTriggerCondition(businessUniqueId, surveyId, triggerCondition);
                    users.addAll(validUsers);
                }
                if(triggerDAO.isDTWorksEnabledForBusiness(businessUniqueId)) { //DTV-11476
                    //check if all the users are migrated to dtworks and if yes, add them to the list
                    users = dtWorksService.fetchDTworksMigratedUsers(users);
                }
            }
        }catch (Exception e){
            logger.error("Exception at getUsersByTriggerId. Msg {}", e.getMessage());
        }
        return users;
    }



    /**
     * method to get feedback response using submission token
     * @param surveyUUID
     * @param respondentId
     * @param businessUUID
     * @param requestLanguage
     * @return
     */
    public Map getResponsesByTokenId(String surveyUUID,String respondentId, String businessUUID, String requestLanguage,
                                     boolean isHippaEnabled) {
        List responses = new ArrayList();
        List responseTokens = new ArrayList();
        List createdTimes = new ArrayList();
        Map responseMap = new HashMap();
        List questionList = new ArrayList();
        List questionsListForPushNotification = new ArrayList();
        String source = "";

        logger.info("begin retrieving responses by token id");
        try {
            Map mappingData = this.getMappingDataForEvents(businessUUID, requestLanguage, surveyUUID, isHippaEnabled);
            Map questionIdQuestionTypeMap = (Map) mappingData.get("questionIdQuestionTypeMap");
            Map questionRatingLabelMap = (Map) mappingData.get("questionRatingLabelMap");
            Map questionScaleMap = (Map) mappingData.get("questionScaleMap");
            Map questionTextMap = (Map) mappingData.get("questionText");
            Map optionsMap = (Map) mappingData.get("options");
            Map optionIdsMap = (Map) mappingData.get("optionIds");

            logger.info("Retrieving all the survey reports");

            if (Utils.notEmptyString(respondentId)) {
                Map eventMap = triggerDAO.getEventBySubmissionToken(surveyUUID, respondentId);
                String eachToken = eventMap.get("submission_token").toString();
                String data = eventMap.get("data").toString();
                String eventData = eventMap.get("event_data").toString();
                String eachFdBkcreatedTime = eventMap.get("created_time").toString();
                responseTokens.add(eachToken);
                createdTimes.add(eachFdBkcreatedTime);
                JSONArray dataArray = new JSONArray(data);
                JSONArray eventDataArray = new JSONArray(eventData);

                int size = dataArray.length();
                for (int i = 0; i < size; i++) {
                    Map eachPushQuestionMap = new HashMap();
                    int eachQuestionId = (int) eventDataArray.get(i);
                    int eachQuestionType = (questionIdQuestionTypeMap.containsKey(eachQuestionId) && Utils.isNotNull(questionIdQuestionTypeMap.get(eachQuestionId))) ? (int) questionIdQuestionTypeMap.get(eachQuestionId) : 2;
                    List eachRatingLableList = (questionRatingLabelMap.containsKey(eachQuestionId) && Utils.isNotNull(questionRatingLabelMap.get(eachQuestionId))) ? (List) questionRatingLabelMap.get(eachQuestionId) : new ArrayList();
                    int questionScale = (questionScaleMap.containsKey(eachQuestionId) && Utils.isNotNull(questionScaleMap.get(eachQuestionId))) ? (int) questionScaleMap.get(eachQuestionId) : 0;

                    String eachQuestionText = (questionTextMap.containsKey(eachQuestionId) && Utils.isNotNull(questionTextMap.get(eachQuestionId))) ?  questionTextMap.get(eachQuestionId).toString() : "";
                    // for anonymous case: meta data question with personal info should be ignored.
                    if(Utils.emptyString(eachQuestionText)){
                        continue;
                    }

                    questionList.add(eachQuestionText);
                    String eachAnswer = "";

                    switch (eachQuestionType) {
                        case 1:
                            //logger.info("rating/ratingSlider");
                            eachRatingLableList = reverseList(eachRatingLableList);
                            int ratingValue = (int) dataArray.get(i);
                            String ratingResponse = (ratingValue > -1) ? new Integer(ratingValue + 1).toString() : "";
                            Map ratingLable = new HashMap();
                            ratingLable.put(0, eachRatingLableList.get(0));
                            ratingLable.put(questionScale - 1, eachRatingLableList.get(1));
                            String ratingLableStr = "";
                            if (ratingValue > -1) {
                                ratingLableStr = questionScale < 6 ? eachRatingLableList.get(ratingValue).toString()
                                        : (questionScale > 5 && (ratingValue == 0 || ratingValue == questionScale - 1)) ? ratingLable.get(ratingValue).toString() : ratingResponse;
                            }
                            eachAnswer = (ratingLableStr);
                            eachPushQuestionMap.put("scale", questionScale+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", ratingResponse);
                            break;

                        case 2:
                        case 14: //file
                            //logger.info("open");
                            eachAnswer = (dataArray.get(i).toString());
                            eachPushQuestionMap.put("scale", "");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", eachAnswer);
                            break;

                        case 3: //single choice
                        case 4: //multi choice
                        case 9: //drop down
                        case 13: //picture choice
                        case 15: //poll
                            String multiChoiceString = dataArray.get(i).toString();
                            eachAnswer = getMultiChoiceAnswers(businessUUID, eachQuestionId, surveyUUID, multiChoiceString, requestLanguage, eachQuestionType);
                            eachPushQuestionMap.put("scale", "");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", eachAnswer);
                            break;

                        case 5:
                            //logger.info("nps");
                            Integer npsRatingValue = (int) dataArray.get(i);
                            String npsResponse = (npsRatingValue > -1) ? npsRatingValue.toString() : "";
                            eachAnswer = (npsResponse);
                            if (questionScale == 11) {
                                questionScale = 10;
                            }
                            eachPushQuestionMap.put("scale", questionScale+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", eachAnswer);
                            break;

                        case 7:
                            String rankingOptionString = dataArray.get(i).toString();
                            List rankingOptionsList = mapper.readValue(rankingOptionString, ArrayList.class);
                            eachAnswer = getRankingAnswers(businessUUID, eachQuestionId, surveyUUID, rankingOptionString);
                            eachPushQuestionMap.put("scale", rankingOptionsList.size()+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", eachAnswer);
                            break;
                        case 8:
                            //logger.info("ratingSlider");
                            int ratingSliderValue = (int) dataArray.get(i);
                            String ratingSliderResponse = (ratingSliderValue > -1) ? new Integer(ratingSliderValue + 1).toString() : "";
                            eachPushQuestionMap.put("scale", questionScale+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", ratingSliderResponse);
                            eachAnswer = ( ratingSliderValue + 1 + "/" + questionScale );
                            break;
                        case 10: case 11:
                            //logger.info("matrix rating / choice");
                            String matrixChoiceString = dataArray.get(i).toString();
                            String matrixAnswer = getMatrixChoiceAnswers(matrixChoiceString, optionsMap, optionIdsMap, eachQuestionId);
                            eachPushQuestionMap.put("scale", questionScale+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", matrixAnswer);
                            eachAnswer = matrixAnswer;
                            break;
                        case 12://multiple open ended
                            String multiTextAnswers = dataArray.get(i).toString();
                            String finalAnswer = getMultipleTextAnswers(multiTextAnswers, optionsMap, eachQuestionId);
                            eachPushQuestionMap.put("scale", questionScale+"");
                            eachPushQuestionMap.put("title", eachQuestionText);
                            eachPushQuestionMap.put("answer", finalAnswer);
                            eachAnswer = finalAnswer;
                            break;

                    }
                    responses.add(eachAnswer);
                    questionsListForPushNotification.add(eachPushQuestionMap);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end receiving responses by token id");
        responseMap.put("responses",responses);
        responseMap.put("tokens",responseTokens);
        responseMap.put("createdTime",createdTimes);
        responseMap.put("questions",questionList);
        responseMap.put("pushNotificationQuestions", questionsListForPushNotification);
        return responseMap;
    }

    // Java Program to iterate List in reverse order
    public List  reverseList (List listToReverse)
    {
        List reversedList = new ArrayList();
        // use ListIterator to iterate List in reverse order
        ListIterator<String> itr = listToReverse.listIterator(listToReverse.size());

        // hasPrevious() returns true if the list has previous element
        while (itr.hasPrevious()) {
            reversedList.add(itr.previous());
        }
        return reversedList;
    }

    /**
     * method to get multichoice label and its options using questionId
     * @param businessUUID
     * @param surveyUUID
     * @param questionId
     * @param requestLanguage
     * @return
     */
    public Map getMultiChoiceIdsAndLabelsByQuestionId(String businessUUID, String surveyUUID, int questionId, String requestLanguage) {
        Map multiChoiceMap = new HashMap();
        List labelsList = new ArrayList();
        List optionsList = new ArrayList();
        List otherOptionDataList = new ArrayList();

        try {
            Map multiChoiceRecord = triggerDAO.getMultiChoiceOptionQuestion(questionId, businessUUID);
            if (multiChoiceRecord.containsKey("option_ids")) {
                String optionIdString = multiChoiceRecord.get("option_ids").toString();
                String otherOptionDataString = (Utils.isNotNull(multiChoiceRecord.get("other_option_data"))) ? multiChoiceRecord.get("other_option_data").toString() : new JSONArray().toString();
                optionsList = mapper.readValue(optionIdString, ArrayList.class);
                otherOptionDataList = mapper.readValue(otherOptionDataString, ArrayList.class);
            }
            labelsList = getOptionsList(surveyUUID, businessUUID, requestLanguage, questionId);
/*
            Map surveyRecords = triggerDAO.getSurveyDetailsBySurveyUniqueId(surveyUUID, businessUUID);
            if (surveyRecords.containsKey(Constants.ID)) {
                //Map surveyRecord = (Map) surveyRecords.get(0);
                List languages = mapper.readValue(surveyRecords.get("languages").toString(), ArrayList.class);
                List questionIds = mapper.readValue(surveyRecords.get("question_ids").toString(), ArrayList.class);
                List multiQuestions = mapper.readValue(surveyRecords.get("multi_questions").toString(), ArrayList.class);
                int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
                int questionIndex = questionIds.indexOf(questionId);
                List questionDatas = (List) multiQuestions.get(languageIndex);
                Map questionData = (Map) questionDatas.get(questionIndex);
                labelsList = (List) questionData.get("options");
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        multiChoiceMap.put("labels", labelsList);
        multiChoiceMap.put("options", optionsList);
        multiChoiceMap.put("otherLabels", otherOptionDataList);
        return multiChoiceMap;
    }


    /**
     * method to get dropdown label and its options using questionId
     * @param businessUUID
     * @param surveyUUID
     * @param questionId
     * @param requestLanguage
     * @return
     */
    public Map getDropDownLabelsByQuestionId(String businessUUID, String surveyUUID, int questionId, String requestLanguage) {

        List labelsList = new ArrayList();
        List optionsList = new ArrayList();
        List otherOptionDataList = new ArrayList();

        try {
            Map multiChoiceRecord = triggerDAO.getDropDownOptionsByQuestionId(businessUUID, questionId);
            if (multiChoiceRecord.containsKey("option_ids")) {
                String optionIdString = multiChoiceRecord.get("option_ids").toString();
                String otherOptionDataString = (Utils.isNotNull(multiChoiceRecord.get("other_option_data"))) ? multiChoiceRecord.get("other_option_data").toString() : new JSONArray().toString();
                optionsList = mapper.readValue(optionIdString, ArrayList.class);
                otherOptionDataList = mapper.readValue(otherOptionDataString, ArrayList.class);
            }
            labelsList = getOptionsList(surveyUUID, businessUUID, requestLanguage, questionId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map multiChoiceMap = new HashMap();
        multiChoiceMap.put("labels", labelsList);
        multiChoiceMap.put("options", optionsList);
        multiChoiceMap.put("otherLabels", otherOptionDataList);
        return multiChoiceMap;
    }

    /**
     * method to get options list from the survey
     * @param surveyUUID
     * @param businessUUID
     * @param requestLanguage
     * @param questionId
     * @return
     */
    public List getOptionsList(String surveyUUID, String businessUUID, String requestLanguage, int questionId) {
        List labelsList = new ArrayList();
        try {
            Map surveyRecords = triggerDAO.getSurveyDetailsBySurveyUniqueId(surveyUUID, businessUUID);
            if (surveyRecords.containsKey(Constants.ID)) {
                List languages = mapper.readValue(surveyRecords.get("languages").toString(), ArrayList.class);
                List questionIds = mapper.readValue(surveyRecords.get("question_ids").toString(), ArrayList.class);
                List multiQuestions = mapper.readValue(surveyRecords.get("multi_questions").toString(), ArrayList.class);
                int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
                int questionIndex = questionIds.indexOf(questionId);
                List questionDatas = (List) multiQuestions.get(languageIndex);
                Map questionData = (Map) questionDatas.get(questionIndex);
                labelsList = (List) questionData.get("options");
            }
        } catch (Exception e) {
            logger.error("Error in getOptionsList. Msg {} ", e.getMessage());
        }
        return labelsList;
    }

    /**
     * method to get multiple ranking options and its ids
     * @param businessUUID
     * @param questionId
     * @param surveyUUID
     * @return
     */
    public Map getMultiRankingOptionsAndItsLabelsByQuestionId(String businessUUID, int questionId, String surveyUUID) {
        Map multiChoiceMap = new HashMap();
        List labels = new ArrayList();
        List options = new ArrayList();

        try {

            Map multiChoiceRecord = triggerDAO.getRankingOptionsByQuestionId(businessUUID, questionId);
            String optionIdString = multiChoiceRecord.get("option_ids").toString();
            options = mapper.readValue(optionIdString, ArrayList.class);
            Map surveyRecord = triggerDAO.getSurveyDetailsBySurveyUniqueId(surveyUUID, businessUUID);

            if (surveyRecord.containsKey("id")) {
                List languages = mapper.readValue(surveyRecord.get("languages").toString(), ArrayList.class);
                List questionIds = mapper.readValue(surveyRecord.get("question_ids").toString(), ArrayList.class);
                List multiQuestions = mapper.readValue(surveyRecord.get("multi_questions").toString(), ArrayList.class);
                int languageIndex = (languages.indexOf("en") > -1) ? languages.indexOf("en") : 0;
                int questionIndex = questionIds.indexOf(questionId);
                List questionDatas = (List) multiQuestions.get(languageIndex);
                Map questionData = (Map) questionDatas.get(questionIndex);
                labels = (List) questionData.get("options");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        multiChoiceMap.put("labels", labels);
        multiChoiceMap.put("options", options);
        return multiChoiceMap;
    }

    /**
     * method to get survey questions with its questionid mapping
     * @param businessUUID
     * @param requestLanuage
     * @param surveyUUID
     * @return
     */
    public Map getMappingDataForEvents(String businessUUID, String requestLanuage, String surveyUUID, boolean isHippaEnabled) {
        Map surveyQuestionMap = new HashMap();
        try {

            List questions = new ArrayList();
            Map questionIdQuestionTypeMap = new HashMap();
            Map questionRatingLabelMap = new HashMap();
            Map questionScaleMap = new HashMap();
            Map questionIdQuestionTextMap = new HashMap();
            Map optionsMap = new HashMap();
            Map optionIdsMap = new HashMap();

            List surveyQuestions = this.returnSurveyQuestionsFromPrograms(surveyUUID, businessUUID, requestLanuage, isHippaEnabled);
            Iterator<Map> questionsIterator = surveyQuestions.iterator();

            while (questionsIterator.hasNext()){
                Map eachQuestion = questionsIterator.next();

                int eachQuestionId = (int)eachQuestion.get("questionId");
                questions.add(eachQuestionId);

                int eachQuestionType = (int)eachQuestion.get("type");
                List labels = (List)eachQuestion.get("labels");
                questionRatingLabelMap.put(eachQuestionId,labels);

                int eachQuestionScale = (int)eachQuestion.get("scale");
                questionScaleMap.put(eachQuestionId, eachQuestionScale);

                String eachQuestionText = eachQuestion.get("name").toString();
                questionIdQuestionTextMap.put(eachQuestionId, eachQuestionText);

                questionIdQuestionTypeMap.put(eachQuestionId,eachQuestionType);

                optionsMap.put(eachQuestionId, (List)eachQuestion.get("options"));
                optionIdsMap.put(eachQuestionId, (List)eachQuestion.get("optionIds"));
            }

            surveyQuestionMap.put("questions",questions);
            surveyQuestionMap.put("questionIdQuestionTypeMap",questionIdQuestionTypeMap);
            surveyQuestionMap.put("questionRatingLabelMap",questionRatingLabelMap);
            surveyQuestionMap.put("questionScaleMap",questionScaleMap);
            surveyQuestionMap.put("questionText", questionIdQuestionTextMap);
            surveyQuestionMap.put("options", optionsMap);
            surveyQuestionMap.put("optionIds", optionIdsMap);

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return surveyQuestionMap;
    }

    /**
     * Newer function retrieves the questions for json surveys
     * @param surveyUUID
     * @param businessUUID
     * @return
     */
    public List returnSurveyQuestionsFromPrograms(String surveyUUID, String businessUUID, String requestLanguage, boolean isHippaEnabled) {
        List questionsList = new ArrayList();
        try {
            logger.info("begin retrieving the survey questions");
            Map programMap = triggerDAO.getSurveyDetailsBySurveyUniqueId(surveyUUID, businessUUID);
            String languagesString = programMap.get("languages").toString();
            boolean isAnonymous = programMap.get("anonymous").toString().equals("1") ? true : false;
            List languages = mapper.readValue(languagesString, ArrayList.class);
            int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
            String multiQuestionString = programMap.get("multi_questions").toString();
            List multiQuestions = mapper.readValue(multiQuestionString, ArrayList.class);

            List questionData = (List) multiQuestions.get(languageIndex);
            Iterator questionDataIterator = questionData.iterator();
            int j = 0;
            while (questionDataIterator.hasNext()) {
                j = j + 1;
                Map eachQuestionData = (Map) questionDataIterator.next();
                String eachQuestionUUID = eachQuestionData.get("questionId").toString();
                int eachQuestionId = triggerDAO.getQuestionIdByQuestionUUID(eachQuestionUUID, businessUUID);
                /**
                 * slider - 1
                 * smiley  - 2
                 */
                int questionRatingType = 0;
                List labels = new ArrayList();
                List options = new ArrayList();
                List optionIds = new ArrayList();

                String questionType = eachQuestionData.get("type").toString();

                String metaDatTypeQuestion = Utils.isNotNull(eachQuestionData.get("metaDataType")) ? eachQuestionData.get("metaDataType").toString() : "";
                if((isAnonymous || isHippaEnabled) &&  Utils.notEmptyString(metaDatTypeQuestion)){
                    // since in respondent tab we are not displaying metatdata questions for anonymous, commenting this out to maintain consistency
                    /*&& (metaDatTypeQuestion.equalsIgnoreCase("Name") ||
                        metaDatTypeQuestion.equalsIgnoreCase("Email") || metaDatTypeQuestion.equalsIgnoreCase("Phone") ||
                        metaDatTypeQuestion.equalsIgnoreCase("Surname") || metaDatTypeQuestion.equalsIgnoreCase("middlename"))){*/
                    // for anonymous case: skip to next question if meta data question with personal info available
                    continue;
                }

                int type = 0;
                int scale = 0;

                switch (questionType) {

                    case Constants.OPEN_ENDED:
                    case Constants.FILE:
                        type = Constants.questionTypes.get(questionType);
                        break;

                    case Constants.RATINGSLIDER:
                    case Constants.RATING:
                        type = questionType.equals("rating") ? 1 : 8;
                        labels = (List) eachQuestionData.get("options");
                        scale = (eachQuestionData.containsKey("scale") && Utils.isNotNull(eachQuestionData.get("scale"))) ? Integer.parseInt(eachQuestionData.get("scale").toString()) : labels.size();
                        String ratingType = eachQuestionData.containsKey("subType") && Utils.isNotNull(eachQuestionData.get("subType"))? eachQuestionData.get("subType").toString() : "";
                        questionRatingType = (Utils.notEmptyString(ratingType) && ratingType.equals("slider")) ? 1 : 2;
                        break;

                    case Constants.SINGLE_CHOICE:
                    case Constants.MULTI_CHOICE:
                    case Constants.PICTURE_CHOICE:
                    case Constants.POLL:
                        type = Constants.questionTypes.get(questionType);
                        Map multiChoiceMap = triggerDAO.getMultiChoiceOptionQuestion(eachQuestionUUID, businessUUID);
                        if (multiChoiceMap.containsKey("option_ids")) {
                            String optionIdsString = multiChoiceMap.get("option_ids").toString();
                            String optionDataString = multiChoiceMap.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;

                    case Constants.NPS:
                        type = 5;
                        labels = (List) eachQuestionData.get("options");
                        scale = 11;
                        questionRatingType = 1;
                        break;

                    case Constants.STATEMENT:
                        type = 6;
                        break;

                    case Constants.RANKING:
                        type = 7;
                        Map rankingData = triggerDAO.getMultiChoiceOptionByQuestionUUID(eachQuestionUUID, businessUUID);
                        if (rankingData.containsKey("option_ids")) {
                            String optionIdsString = rankingData.get("option_ids").toString();
                            String optionDataString = rankingData.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;
                    case Constants.DROP_DOWN:
                        type = Constants.questionTypes.get(questionType);
                        Map dropDownMap = triggerDAO.getDropDownOptionsByQuestionUniqueId(businessUUID, eachQuestionUUID);
                        if (dropDownMap.containsKey("option_ids")) {
                            String optionIdsString = dropDownMap.get("option_ids").toString();
                            String optionDataString = dropDownMap.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;
                    case Constants.MATRIX_CHOICE:
                    case Constants.MATRIX_RATING:
                        type = Constants.questionTypes.get(questionType);
                        options = (List) eachQuestionData.get("questionTitles");
                        optionIds = (List) ((List) eachQuestionData.get("optionsForMatrix")).get(0); // all the values will be same for now. Hence takting the first index. In future different options can be stroed.
                        logger.info("MATRIX questionTitles : " + options);
                        logger.info("MATRIX optionsForMatrix : " + optionIds);
                        break;
                    case Constants.MULTI_OPENENDED:
                        type = Constants.questionTypes.get(questionType);
                        options = (List) eachQuestionData.get("questionTitles");
                        optionIds = (List) eachQuestionData.get("questionIds");
                        logger.info("MOE questionTitles : " + options);
                        logger.info("MOE questionIds : " + optionIds);
                        break;
                }

                Map eachQuestionMap = new TreeMap();
                eachQuestionMap.put("questionId", eachQuestionId);
                eachQuestionMap.put("questionUUID", eachQuestionUUID);
                eachQuestionMap.put("name", eachQuestionData.get("questionTitle"));
                eachQuestionMap.put("optionIds", optionIds);
                eachQuestionMap.put("options", options);
                eachQuestionMap.put("labels", labels);
                eachQuestionMap.put("scale", scale);
                eachQuestionMap.put("type", type);
                eachQuestionMap.put("order", j);
                eachQuestionMap.put("ratingType", questionRatingType);
                questionsList.add(eachQuestionMap);
            }
            logger.info("end retrieving the survey questions");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionsList;
    }

    /**
     *
     * @param execStartTime
     * @param execEndTime
     * @param scheduleUUID
     */
    public void updateExecTimeByUUID(String execStartTime, String execEndTime, String scheduleUUID){
        logger.info("begin update exec time");
        try{
            triggerDAO.updateExecutionTimesByTriggerUUID(execStartTime, execEndTime,scheduleUUID);
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("end update exec time");
    }

    /**
     *
     * @param execStartTime
     * @param execEndTime
     * @param id
     */
    public void updateExecTime(String execStartTime, String execEndTime, int id){
        logger.debug("begin update exec time");
        try{
            triggerDAO.updateExecutionTimes(execStartTime, execEndTime,id);
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.debug("end update exec time");
    }

    /**
     * update execution start and end times
     */
    public void updateExecutionStartAndEndTime(){
        logger.debug("begin update exec time");
        try{
            List schedules = triggerDAO.getActiveSchedules();
            Iterator iterator = schedules.iterator();
            while (iterator.hasNext()){
                try{
                    Map eachScheduleMap = (Map)iterator.next();
                    String executionStartTime = Utils.isNotNull(eachScheduleMap.get(Constants.EXEC_START_TIME)) ? eachScheduleMap.get(Constants.EXEC_START_TIME).toString() : "";
                    String executionEndTime = Utils.isNotNull(eachScheduleMap.get(Constants.EXEC_END_TIME)) ? eachScheduleMap.get(Constants.EXEC_END_TIME).toString() : "";
                    int id = (int)eachScheduleMap.get(Constants.SCHEDULE_ID);
                    if(Utils.emptyString(executionStartTime)){
                        executionStartTime = eachScheduleMap.get(Constants.START_DATE).toString() + " "+eachScheduleMap.get(Constants.START_TIME).toString();
                    }else{
                        executionStartTime = Utils.getFormattedDateTimeAddHours(executionStartTime,24);
                    }

                    if(Utils.emptyString(executionEndTime)){
                        executionEndTime = eachScheduleMap.get(Constants.START_DATE).toString() + " "+eachScheduleMap.get(Constants.END_TIME).toString();
                        if(Utils.compareTimes(eachScheduleMap.get(Constants.START_TIME).toString(), eachScheduleMap.get(Constants.END_TIME).toString())){
                            executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
                        }
                    }else{
                        executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime,24);
                    }

                    updateExecTime(executionStartTime, executionEndTime, id);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.debug("end update exec time");

    }

    /**
     *
     * @param startDate
     * @param endDate
     * @param startTime
     * @param endTime
     * @return
     */
    public Map getExecutionStartEndTime(String startDate, String endDate, String startTime, String endTime, String surveyEndDate, String timeZone){
     Map datesMap = new HashMap();
     try{

         String executionStartTime = Constants.EMPTY;
         String executionEndTime = Constants.EMPTY;

         Map<String, String> triggerDatesMap = this.getTriggerStartEndDSTAdjustedDates(startDate, startTime,
                 endDate, endTime, surveyEndDate, timeZone);

         startDate = triggerDatesMap.getOrDefault(Constants.START_DATE, Constants.EMPTY);
         endDate = triggerDatesMap.getOrDefault(Constants.END_DATE, Constants.EMPTY);
         startTime = triggerDatesMap.getOrDefault(Constants.START_TIME, Constants.EMPTY);
         endTime = triggerDatesMap.getOrDefault(Constants.END_TIME, Constants.EMPTY);

         if(!startDate.isEmpty() && !endDate.isEmpty() && !startTime.isEmpty() && !endTime.isEmpty()) {

             executionStartTime = startDate + " " + startTime;
             executionEndTime = startDate + " " + endTime;

             String currentDate = Utils.getCurrentUTCDateAsString();

             /**
              * CurrentUTCDate : 2024-10-04 06:00:00 (End time not in DST)
              * Assume UI start & end date : 2024-10-04 00:00:00 & 2024-12-31 23:59:59 (NEW_YORK Timezone)
              * Assume UI start & end date : 2024-10-04 04:00:00 & 2025-01-01 04:59:59 (UTC Timezone)
              *
              * UTC ExecutionStartTime     : 2024-10-04 04:00:00
              * UTC ExecutionEndTime       : 2024-10-04 04:59:59 (after adjusting to DST) --> 2024-10-04 03:59:59
              */

             /**
              * CurrentUTCDate : 2024-10-04 06:00:00 (End time in DST)
              * Assume UI start & end date : 2024-10-04 00:00:00 & 2024-10-30 23:59:59 (NEW_YORK Timezone)
              * Assume UI start & end date : 2024-10-04 04:00:00 & 2024-10-31 03:59:59 (UTC Timezone)
              *
              * UTC ExecutionStartTime     : 2024-10-04 04:00:00
              * UTC ExecutionEndTime       : 2024-10-05 03:59:59
              */


             /**
              * Calculating no of days to add for executionStartTime and executionEndTime
              * We have to calculate the difference between current date and start date and add those many days to executionStartTime and executionEndTime
              *
              * Example : Trigger createdDate : Oct 1 2024
              *                   StartDate : 2024-10-01 04:00:00, EndDate : 2024-11-04 03:59:00
              * If they create a schedule or turned on triggers after 3 or 4 (Example : Oct 7 2024)
              *
              * UTC ExecutionStartTime     : 2024-10-07 04:00:00
              * UTC ExecutionEndTime       : 2024-10-07 03:59:59
              */

             long daysToAdd = (int) Utils.differenceDays(executionStartTime, currentDate);
             if (daysToAdd > 0) {
                 executionStartTime = Utils.getFormattedDateTimeAddHours(executionStartTime, 24 * (int) daysToAdd);
                 executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24 * (int) daysToAdd);
             }


             /**
              * Comparing startTime and endTime
              * if startTime > endTime, then add 24 hours to endTime
              * Example : StartTime : Oct 1 2024 4:00 AM ---------> Oct 1 2024 4:00AM
              *           EndTime   : Oct 1 2024 3:00 AM ---------> Oct 2 2024 3:00AM
              * Then add 24 hours to endTime
              */
             if (Utils.compareTimes(startTime, endTime)) {
                 executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
             }

             /**
              * Comparing currentUTCDate and executionEndTime, to check executionEndTime completed for the day or not
              * If currentUTCDate > executionEndTime : It means trigger duration is completed for the today
              * then add 24 hours to executionStartTime and executionEndTime
              */
             if (Utils.compareDates(currentDate, executionEndTime)) {
                 executionStartTime = Utils.getFormattedDateTimeAddHours(executionStartTime, 24);
                 executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
             }
             logger.info("daysToAdd  " +  daysToAdd);
             logger.info("currentDate  "+ currentDate);
             logger.info("executionStartTime  "+ executionStartTime);
             logger.info("executionEndTime  "+ executionEndTime);
         }

         datesMap.put(Constants.EXEC_START_TIME, executionStartTime);
         datesMap.put(Constants.EXEC_END_TIME, executionEndTime);
     }catch (Exception e){
         e.printStackTrace();
     }
     return datesMap;
    }


    /**
     *
     * @param id
     * @return
     */
    public boolean checkNotificationExistsForTheSchedule(int id, String triggerNotifyTime, String businessUUID){
        boolean isPresent = false;
        try{
            List notifications = triggerDAO.retrieveNotificationsForASchedule(id, triggerNotifyTime, businessUUID);
            isPresent = (notifications.size()>0) ? true : false;
        }catch (Exception e){
            e.printStackTrace();
        }
        return isPresent;
    }

    /**
     * update trigger status to inactive when the survey expires
     * commenting as part of SE-1091, DTV-7349
     */
 /*   public void updateExpiredSurvey(){
        logger.debug("begin update expired surveys' trigger status");
        try {
            List businessList = triggerDAO.getAllBusiness();
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    Map eachBusiness = iterator.next();
                    String businessUUID = eachBusiness.get(Constants.BUSINESS_UUID).toString();
                    List<Map<String, Object>> expiredSurveys = triggerDAO.getExpiredSurveys(businessUUID);
                    Iterator expiredSurveyIterator = expiredSurveys.iterator();
                    while (expiredSurveyIterator.hasNext()) {
                        try {
                            Map eachExpiredSurveyMap = (Map) expiredSurveyIterator.next();
                            int surveyId = (int) eachExpiredSurveyMap.get(Constants.ID);
                            //update trigger status for expired surveys
                            triggerDAO.updateTriggerStatusBySurveyId(surveyId,businessUUID);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.debug("end update expired surveys' trigger status");

    }*/

    /**
     * method to update status of schdule based on frequency
     * @param frequency
     * @param scheduleId
     * @param scheduleEndDateTime
     */
    public void updateTriggerStatusByFrequency(String frequency, int scheduleId, String scheduleEndDateTime) {

        if (frequency.equals(Constants.FREQUENCY_SINGLE)) {
            // update the status to completed when single frequency trigger condition satisfied.
            triggerDAO.updateScheduleStatusByScheduleId(scheduleId, Constants.STATUS_COMPLETED);
        } else if (frequency.equals(Constants.FREQUENCY_MULTIPLE)) {
            // update last processed date and time of each scheduleId in schedule table
            triggerDAO.updateLastProcessedDateTime(scheduleId);
            // check currentUtcTime is greater or equal to scheduleEndTime for multiple frequency and update the status to completed
            String currentUTCTime = Utils.getCurrentUTCDateAsString();
//            String scheduleEndDateTime = endDate +" "+ endTime;
            if(Utils.compareDates(currentUTCTime, scheduleEndDateTime)){
                // update the status to completed when multiple frequency trigger condition end date and time reached.
                triggerDAO.updateScheduleStatusByScheduleId(scheduleId, Constants.STATUS_COMPLETED);
            }
        }
    }

   /* public Map getSurveyAndBusinessUniqueId(int businessId, int surveyId) {
        Map result = new HashMap();
        try {
            // get businessUniqueId by businessId
            String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);
            // get surveyUniqueId
            String surveyUinqueId = triggerDAO.getSurveyUniqueIdById(surveyId, businessUniqueId);
            result.put(Constants.SURVEYUUID, surveyUinqueId);
            result.put(Constants.BUSINESS_ID, businessUniqueId);
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public Map getTriggerMapByTriggerId(String businessUniqueId, int triggerId) {
        return triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
    }

    public void updateTriggerReadableQuery(String businessUniqueId, String readableQuery, int triggerId) {
         triggerDAO.updateTriggerReadableQuery(businessUniqueId, readableQuery, triggerId);
    }*/

    public List getActiveTriggerScheduled() {
        return triggerDAO.getActiveTriggerScheduled();
    }

    public void updateScheduleSentStatusByScheduleId(int scheduleId, String state){
        triggerDAO.updateScheduleSentStatusByScheduleId(scheduleId, state);
    }

    public void updateNotificationSentStatusByNotifyId(int notifyId, String status){
        triggerDAO.updateNotificationSentStatusByNotifyId(notifyId, status);
    }

    //DTV-6955
    public void updateBusinessNotificationSentStatusByNotifyId(int notifyId, String status, int businessId){
        String businessUUID = triggerDAO.getBusinessUniqueIdById(businessId);
        triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, status, businessUUID);
    }

    public List getActiveTriggerNotificationsV1(){
       return triggerDAO.getActiveTriggerNotificationsV1();
    }

    /**
     * method to send trigger notification via email or push channel
     * @param triggerNotification
     */
    public void sendTriggerNotification(TriggerNotification triggerNotification){
        logger.debug("begin send trigger notification");
        try {
//            if(Utils.isNotNull(triggerNotification.getScheduleId())) {
                List toEmailList = new ArrayList();
                List notifyIdsList = new ArrayList();
                List notifyEmailList = new ArrayList();

                String channel = triggerNotification.getChannel();
                String subject = Utils.isNotNull(triggerNotification.getSubject()) ? triggerNotification.getSubject() : "";
                String messageStr = Utils.isNotNull(triggerNotification.getMessage()) ? triggerNotification.getMessage() : "";
                int notifyId = triggerNotification.getNotifyId();
                List emailList = triggerNotification.getContacts();
                boolean addFeedback = triggerNotification.isAddFeedback();
                String query = triggerNotification.getConditionQuery();
                int businessId = triggerNotification.getBusinessId();
                int surveyId = triggerNotification.getSurveyId();
                int triggerId = triggerNotification.getTriggerId();
                //String hostUrl = triggerNotification.getHost();
                String hostUrl = "https://" + dtpDomain;
                Map fromEmailMap = new HashMap();
                Map emailSettingsMap = triggerDAO.getBusinessEmailSettings(businessId); //DTV-5738 //DTV-6025
                Map triggerEmailMap = emailSettingsMap.containsKey("trigger_email") && Utils.notEmptyString(emailSettingsMap.get("trigger_email").toString()) ? (Map) emailSettingsMap.get("trigger_email") : new HashMap();
                //from email
                String fromEmailCustomizedReplyToRespondent = triggerEmailMap.containsKey("replyToRespondent") && Utils.notEmptyString(triggerEmailMap.get("replyToRespondent").toString()) ? triggerEmailMap.get("replyToRespondent").toString() : fromEmail;
                String fromEmailCustomizedEmailNotification = triggerEmailMap.containsKey("emailNotification") && Utils.notEmptyString(triggerEmailMap.get("emailNotification").toString()) ? triggerEmailMap.get("emailNotification").toString() : fromEmail;
                String fromEmailCustomized = channel.equals(Constants.CHANNEL_REPLY) ? fromEmailCustomizedReplyToRespondent : fromEmailCustomizedEmailNotification;
                fromEmailMap.put("email", fromEmailCustomized);
                //from name
                String fromNameCustomizedReplyToRespondent = triggerEmailMap.containsKey("replyToRespondentFromName") && Utils.notEmptyString(triggerEmailMap.get("replyToRespondentFromName").toString()) ? triggerEmailMap.get("replyToRespondentFromName").toString() : fromName;
                String fromNameCustomizedEmailNotification = triggerEmailMap.containsKey("emailNotificationFromName") && Utils.notEmptyString(triggerEmailMap.get("emailNotificationFromName").toString()) ? triggerEmailMap.get("emailNotificationFromName").toString() : fromName;
                String fromNameCustomized = channel.equals(Constants.CHANNEL_REPLY) ? fromNameCustomizedReplyToRespondent : fromNameCustomizedEmailNotification;
                fromEmailMap.put("name", fromNameCustomized);
                String token = triggerNotification.getSubmissionToken();
                int scheduleId = Integer.parseInt(triggerNotification.getScheduleId());
                int emailUsageCount = 0;
                String eventCreatedTime = "";
                String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);
//            String surveyUUID = triggerDAO.getSurveyUniqueIdById(surveyId, businessUniqueId);
                Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
                String surveyStartDate = surveyMap.containsKey("from_date") ? surveyMap.get("from_date").toString() : "";
                String surveyEndDate = surveyMap.containsKey("to_date") ? surveyMap.get("to_date").toString() : "";

                String surveyUUID = surveyMap.containsKey("survey_uuid") ? surveyMap.get("survey_uuid").toString() : "";
                boolean isAnonymous = surveyMap.containsKey("anonymous") ? ((int) surveyMap.get("anonymous") == 1 ? true : false) : false;

                String logo = "";
                //DTV-5582 --> reply to respondent link theme fix
                String replyToRespondent = (Utils.isNotNull(triggerNotification.getReplyToRespondent()) && Utils.notEmptyString(triggerNotification.getReplyToRespondent())) ? triggerNotification.getReplyToRespondent() : "" ;
                String hexCode = "";

                Map surveyPropertyMap = (surveyMap.containsKey("survey_property") && Utils.isNotNull(surveyMap.get("survey_property"))) ? mapper.readValue(surveyMap.get("survey_property").toString(), HashMap.class) : new HashMap();
                if(surveyPropertyMap.containsKey("image") && Utils.isNotNull(surveyPropertyMap.get("image"))){
                    logo = surveyPropertyMap.get("image").toString();
                }
                if(surveyPropertyMap.containsKey("hexCode") && Utils.isNotNull(surveyPropertyMap.get("hexCode"))){
                    hexCode = surveyPropertyMap.get("hexCode").toString();
                }
                logo = logo.length() > 0 ? logo : Constants.DEFAULT_LOGO;
                hexCode = hexCode.length() > 0 ? hexCode : Constants.DEFAULT_HEXCODE;
                logger.info("hexcode  "+hexCode);

                Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
                String surveyName = triggerMap.get(Constants.SURVEY_NAME).toString();
                String triggerName = triggerMap.get(Constants.TRIGGER_NAME).toString();
                String triggerCondition = triggerMap.get(Constants.TRIGGER_CONDITION).toString();
                String readableQuery = triggerMap.get(Constants.READABLE_QUERY).toString();
                String surveyFilterUUID = Utils.isNotNull(triggerMap.get(Constants.TRIGGER_FILTER_UUID)) ? triggerMap.get(Constants.TRIGGER_FILTER_UUID).toString() : "";
                int createdBy = (int)triggerMap.get(Constants.CREATED_BY);

                Map participantRecords = triggerDAO.getEventBySubmissionToken(surveyUUID, token);
                //check, To prevent exceptions for aggregate case
                if(Utils.notEmptyString(token)) {
                    eventCreatedTime = participantRecords.get(Constants.CREATED_TIME).toString();
                }
                boolean isHippaEnabled = triggerDAO.isHippaEnableForBusiness(businessId);

            // replace tag value with corresponding respondent meta data info
                List tagsToReplace = Utils.mergeListAndEliminateDuplicates(Utils.getTokens(subject), Utils.getTokens(messageStr));
                if (tagsToReplace.size() > 0) {
                    Map substituteTokens = this.computeParticipantSubstituteTokens(participantRecords, tagsToReplace);
                    subject = Utils.replaceTokens(subject, substituteTokens);
                    messageStr = Utils.replaceTokens(messageStr, substituteTokens);
                }

                // set sender email id list
                if (channel.equals(Constants.CHANNEL_EMAIL) || channel.equals(Constants.CHANNEL_REPLY)) {
                    // email notification param request
                    Iterator itr = emailList.iterator();
                    while (itr.hasNext()) {
                        Map emailMap = new HashMap();
                        emailMap.put("email", itr.next().toString());
                        toEmailList.add(emailMap);
                    }

                    //adding email format for EMAIL and REPLY actions
                    messageStr = messageStr.replaceAll("(\r\n|\n)", "<br />");
                }
                if (Utils.notEmptyString(token)) {
                    // immediate or delayed notification
                    String template = addFeedback ? "immediateOrDelayNotifyWithFeedback.html" : "immediateOrDelayedNotify.html";

                    String replyRespondent = "";
                    if(channel.equals(Constants.CHANNEL_REPLY)){
                        template = addFeedback ? "immediateOrDelayNotifyWithFeedbackCustomersReply.html" : "immediateOrDelayedNotifyCustomersReply.html";

//                        Map participantRecords = triggerDAO.getEventBySubmissionToken(surveyUUID, token);
                        //String createdTime = Utils.isNotNull(participantRecords.get("created_time")) && Utils.notEmptyString(participantRecords.get("created_time").toString()) ? participantRecords.get("created_time").toString() : "";
                        /*if(createdTime.length() > 0) {
                            Date oldFormat = new SimpleDateFormat(Constants.YYYY_MM_DD_HH_MM_SS).parse(createdTime);
                            createdTime = new SimpleDateFormat("dd MMM yyyy hh.mm a").format(oldFormat);
                            //messageStr = messageStr + "<p> Condition was met at the <strong>"+createdTime+"</strong>. </p>";
                        }*/
                        if(isAnonymous){
                            String anonymousText = "<p> Please be assured that your Personally Identifiable Information is protected and anonymous. This is a system-generated email and your information is not shared with the Surveyor/Pollster. </p>";
                            messageStr = messageStr + anonymousText;
                        }

                        //DTV-5582 --> reply to respondent contact us mail fix
                        if(Utils.notEmptyString(replyToRespondent)){
                            List mails = new ArrayList();
                            List urls = new ArrayList();
                            replyRespondent = replyToRespondent;
                            //regex pattern to find email from string
                            Matcher matcher = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(replyToRespondent);
                            while(matcher.find()){
                                String mailText = matcher.group().trim();
                                mails.add(mailText);
                            }

                            //Email id found in reply to respodnent string
                            for(int j=0; j<mails.size(); j++) {
                                String eachMailText = mails.get(j).toString();
                                if (Utils.notEmptyString(eachMailText)) {
                                    String format = "<a\n" +
                                            "href=\"mailto:" + eachMailText + "\"\n" +
                                            "style=\"color: " + hexCode + "; text-decoration: none;\">" + eachMailText + "</a>";
                                    replyRespondent = replyRespondent.replace(eachMailText, format);
                                    //to avoid email in url validations
                                    //DTV-5682
                                    replyToRespondent = replyToRespondent.replace(eachMailText, "");
                                }
                            }

                            //DTV-5682
                            //regex pattern to find URls from string
                            //String urlRegex = "\\b((?:https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:, .;]*[-a-zA-Z0-9+&@#/%=~_|])";
                            String urlRegex = "((http?|https|ftp|file)://)?((W|w){3}.)?[a-zA-Z0-9]+\\.[a-zA-Z]+";
                            Matcher urlMatcher = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE).matcher(replyToRespondent);
                            while(urlMatcher.find()){
                                String urlText = urlMatcher.group().trim();
                                urls.add(urlText);
                            }


                            //DTV-5682
                            //urls  found in reply to respodnent string
                            for(int k=0; k<urls.size(); k++) {
                                String eachUrlText = urls.get(k).toString();
                                if (Utils.notEmptyString(eachUrlText)) {
                                    String format = "<a\n" +
                                            "href=\"" + eachUrlText + "\"\n" +
                                            "style=\"color: " + hexCode + "; text-decoration: none;\">" + eachUrlText + "</a>";
                                    replyRespondent = replyRespondent.replace(eachUrlText, format);
                                }
                            }
                        }
                    }
                    String htmlContent = getFileContent(template);
                    String respondentName = "";
                    try {
                        StringBuffer respondentStr = new StringBuffer();
                        Map participantMap = this.getParticipantInfo(token, businessUniqueId);//triggerDAO.getParticipantByToken(token, surveyUUID);

                        LinkedList triggerHeaderList = new LinkedList();
                        LinkedList triggermetaDataTypeList = new LinkedList();
                        LinkedList triggerHeaderTypeList = new LinkedList();
                        LinkedList<Boolean> triggerPhiDataList = new LinkedList<>();
                        int participantGroupId = 0;
                        int nameIdx = -1;
                        int emailIdx = -1;
                        int createdTimeIdx = -1;
                        // if channel is list, get particpant_id and header data details
                        if(channel.equals(Constants.LIST)){
                           Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
                           participantGroupId = Utils.isNotNull(scheduleMap.get("participant_group_id")) ? (int)scheduleMap.get("participant_group_id") : 0;
                           TriggerListHeaderData triggerHeader = scheduleMap.containsKey("header_data") && Utils.isNotNull(scheduleMap.get("header_data")) ? mapper.readValue(scheduleMap.get("header_data").toString(), TriggerListHeaderData.class) : new TriggerListHeaderData();
                           triggerHeaderList = new LinkedList(triggerHeader.getHeader());
                           triggermetaDataTypeList = new LinkedList(triggerHeader.getMetaData());
                           triggerHeaderTypeList = appendHeaderwithMetaDataType(triggerHeaderList, triggermetaDataTypeList);
                           triggerPhiDataList = Utils.isNotNull(triggerHeader.getPhiData()) ? new LinkedList<>(triggerHeader.getPhiData()) : new LinkedList<>();

                            /**
                             * as per DTV-2787 pt 3.
                             * Name (checked) (instead of Customer name or Client name)                             *
                             * Email (checked) (instead of Customer email or Client email)
                             *
                             * To get the original header values, and store its values in target list
                             */
                            // get participant header details by token
                            Map participantHeaderMap = triggerDAO.getParticipantEmailNameColByToken(token, businessUniqueId);


                            //nameIdx = triggerHeaderList.indexOf("Name");
                            //emailIdx = triggerHeaderList.indexOf("Email");
                            //Retrieve the name and email from meta data type instead of text Name and Email
                            nameIdx = triggermetaDataTypeList.indexOf("NAME");
                            emailIdx = triggermetaDataTypeList.indexOf("EMAIL");
                            createdTimeIdx = triggerHeaderList.indexOf("Timestamp");

                            // assign original header values for name and email from participant table to target header list
                            //CSUP-715, If target list doesn't contains name or email headers.
                            if(nameIdx > -1 && nameIdx < triggerHeaderList.size()) {
                                triggerHeaderList.set(nameIdx, participantHeaderMap.get("name"));
                            }
                            if(emailIdx > -1 && emailIdx < triggerHeaderList.size()) {
                                triggerHeaderList.set(emailIdx, participantHeaderMap.get("email"));
                            }

                            //CSUP-715, If target list doesn't contains name or email headers.
                            if(nameIdx > -1 && nameIdx < triggerHeaderTypeList.size()) {
                                triggerHeaderTypeList.set(nameIdx, participantHeaderMap.get("name"));
                            }
                            if(emailIdx > -1 && emailIdx < triggerHeaderTypeList.size()) {
                                triggerHeaderTypeList.set(emailIdx, participantHeaderMap.get("email"));
                            }
                            //DTV-4280 to convert timestamp date based on timezone
                            String scheduleTimezone = Utils.notEmptyString(scheduleMap.get("timezone").toString()) ? scheduleMap.get("timezone").toString() : Constants.TIMEZONE_AMERICA_LOSANGELES;
                            eventCreatedTime = Utils.getLocalDateTimeByTimezone(eventCreatedTime, scheduleTimezone);
                        }

                        List dataList = (triggerHeaderList.size() > 0 ) ? new ArrayList(Collections.nCopies(triggerHeaderList.size(), "")) : new ArrayList();
                        String timeStamp = participantMap.containsKey(Constants.CREATED_TIME) ? participantMap.get(Constants.CREATED_TIME).toString() : Constants.EMPTY;
                        participantMap.remove(Constants.CREATED_TIME);
                        //CSUP-903
                        respondentName = participantMap.containsKey(Constants.RESPONDENT_NAME) ? participantMap.get(Constants.RESPONDENT_NAME).toString() : Constants.EMPTY;
                        if(participantMap.containsKey(Constants.RESPONDENT_NAME))
                            participantMap.remove(Constants.RESPONDENT_NAME);
                        if(participantMap.containsKey("participantGroupId"))
                            participantMap.remove("participantGroupId");
                        if(participantMap.containsKey("participantId"))
                            participantMap.remove("participantId");

                        if (!isAnonymous) {
                            //In case the response collected is anonymous, do not show such respondent details in the notification.
                            Map metaDataMap = participantMap;//participantMap.containsKey("metadata") ? mapper.readValue(participantMap.get("metadata").toString(), HashMap.class) : new HashMap();
                            Set keySet = metaDataMap.keySet();
                            Iterator itr = keySet.iterator();
                            // get respondent details
                            respondentStr.append(" <h2 style=\"color: #464646; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 16px; font-weight: 700; line-height: 24px; margin: 0 0 5px 0;  padding: 0; text-align: left;\">");
                            respondentStr.append(" Respondent information </h2>");
                            respondentStr.append("<ul style=\"list-style-type: none;margin:0;padding:0;\">");
                            List personalInfoHeaderList = new ArrayList();
                            if (isHippaEnabled) {
                                personalInfoHeaderList = triggerDAO.getParticipantPersonalInfoByToken(token, businessUniqueId);
                            }
                            while (itr.hasNext()) {
                                String metaDataField = (String) itr.next();
                                if (!isHippaEnabled || !personalInfoHeaderList.contains(metaDataField)) {
                                    String metaDataVal = metaDataMap.get(metaDataField).toString();
                                    respondentStr.append("<li style='list-style-type: none;margin:0 0 10px 0;padding:0;'><label class='lead' style='color: #000; font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 16px; font-weight: 600; line-height: 21px; margin: 0;padding: 0; text-align: left;'>");
                                    int index = metaDataField.indexOf(Constants.DELIMITER);
                                    respondentStr.append(metaDataField.substring(0, index)).append(": "); //eg: Email~~EMAIL to Email
                                    respondentStr.append("</label><span style='color:#666666;font-family: 'Avenir LT W01_85 Heavy1475544',-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0; padding: 0; text-align: left;'>");
                                    respondentStr.append(metaDataVal);
                                    respondentStr.append("</span></li>");
                                    //continue;
                                }
                                if (containsCaseInsensitive(metaDataField, triggerHeaderTypeList)) {
                                    String metaDataVal = metaDataMap.get(metaDataField).toString();
                                    int metaDataTypeIdx = triggerHeaderTypeList.indexOf(metaDataField);
                                    if (metaDataTypeIdx > -1)
                                        dataList.set(metaDataTypeIdx, metaDataVal);
                                }
                            }
                            respondentStr.append("</ul>");
                        }
                        String respondentId = token;

                        StringBuffer feedbackStr = new StringBuffer();
                        Map eventMap = this.getResponsesByTokenId(surveyUUID, token, businessUniqueId, "en", isHippaEnabled);
                        if (addFeedback) {
                            List responses = (List) eventMap.get("responses");
                            List questionText = (List) eventMap.get("questions");
                            Iterator itrResponse = responses.iterator();
                            Iterator itrQuestion = questionText.iterator();

                            while (itrResponse.hasNext()) {
                                String response = (String) itrResponse.next();
                                String question = (String) itrQuestion.next();
                                feedbackStr.append("<li style='list-style-type: decimal;margin:0 0 10px 0;padding:0 0 0 8px;'><p class='lead' style='color: #000000; font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0 0 5px 0; padding: 0; text-align: left;'>");
                                feedbackStr.append(question);
                                feedbackStr.append("</p><p style='color:#757989;font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0; padding: 0; text-align: left;'>");
                                feedbackStr.append(response);
                                feedbackStr.append("</p></li>");
                            }
                        }
                        //begin alerts
                        List tokens = triggerDAO.getAllTokensOfSsurveyId(surveyUUID);
                        int tokenIndex = tokens.indexOf(token);
                        int pageNumber = Utils.computePageNumber(tokenIndex);
                        String link = hostUrl +"/"+ Constants.LANGUAGE_EN + "/programreport/" + surveyUUID + "/" + pageNumber + "/" + token;

                        String message = "";
                        HashMap templateParam = new HashMap();
                        templateParam.put("feedback", feedbackStr);
                        templateParam.put("message", messageStr);
                        templateParam.put("triggerName", triggerName);
                        templateParam.put("respondentInfo", respondentStr);
                        templateParam.put("surveyName", surveyName);
                        templateParam.put("link", link);
                        templateParam.put("logo", logo);
                        templateParam.put("replyRespondent", replyRespondent);
                        logger.info("resopond info {}", respondentStr);
                        StrSubstitutor sub = new StrSubstitutor(templateParam);
                        message = sub.replace(htmlContent);

                        if (channel.equals(Constants.CHANNEL_EMAIL) || channel.equals(Constants.CHANNEL_REPLY)) {
                            emailUsageCount = toEmailList.size();
                            if(!onprem) {
                                Map emailMap = new HashMap();
                                emailMap.put("from", fromEmailMap);
                                emailMap.put("tos", toEmailList);
                                emailMap.put("subject", subject);
                                emailMap.put("body", message);
                                notifyEmailList.add(emailMap);
                            }
                            else{
                                SendEmailNotificationOnprem(toEmailList, subject, message, fromEmailCustomized, fromNameCustomized);
                            }
                        } else if (channel.equals(Constants.CHANNEL_PUSH)) {
                           /* //removing html tags and extracing plain text
                            messageStr = Jsoup.parse(messageStr).text(); */

                            // push notification param request
                            if(messageStr.contains("\"")){
                                messageStr = messageStr.replace("\"", "\\\"");
                            }
                            if(messageStr.contains("\t")){
                                messageStr = messageStr.replace("\t", "\\t");
                            }
//                            if(messageStr.contains("\n")){
//                                messageStr = messageStr.replace("\n", "\\n");
//                            }
//                            Commented the above for CSUP-1697


                            List createdTimeList = (List) eventMap.get("createdTime");
                            List notificationsList = new ArrayList();
                            Map surveyDataMap = new HashMap();
                            Map dataMap = new HashMap();
                            Map eachUserDataMap = new HashMap();
                            surveyDataMap.put(Constants.SURVEYUUID, surveyUUID);
                            surveyDataMap.put(Constants.RESPONDENT_ID, respondentId);
                            surveyDataMap.put(Constants.TIME_STAMP, timeStamp);
                            surveyDataMap.put(Constants.SURVEY_NAME, surveyName);
                            surveyDataMap.put(Constants.RESPONDENT_NAME, respondentName);
                            surveyDataMap.put(Constants.PUSHMESSAGE, messageStr);
                            surveyDataMap.put(Constants.TRIGGER_NAME, triggerName);
                            surveyDataMap.put(Constants.CREATED_TIME, createdTimeList.get(0));
                            dataMap.put(Constants.DEEP_LINK, deeplinkTrigger);
                            dataMap.put(Constants.DATA, surveyDataMap);
                            JSONArray jsonEmailArray = new JSONArray(emailList);
                            eachUserDataMap.put(Constants.APPNAME, appName);
                            eachUserDataMap.put(Constants.EMAILS, jsonEmailArray);
                            eachUserDataMap.put(Constants.TITLE, surveyName);
                            eachUserDataMap.put(Constants.PUSHMESSAGE, Jsoup.parse(messageStr).text()); //CSUP-1607
                            eachUserDataMap.put(Constants.DATA, dataMap);
                            JSONObject json = new JSONObject(eachUserDataMap);
                            String inputJson = json.toString();
                            logger.info("input json {}", inputJson);
                            logger.info("push notification url {}", pushNotificationUrl);
                            callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);

                            surveyDataMap.put(Constants.EMAILS, emailList);
                            surveyDataMap.put(Constants.BUSINESS_ID, businessId);
                            notificationsList.add(surveyDataMap);
                            this.updateTriggerPushNotifications(notificationsList, businessUniqueId);
                        }
                        else if(channel.equals(Constants.LIST ) && !isAnonymous){
                            // add respondent details to target list
                            if(dataList.size() > 0) {
                                //CSUP-715, If target list doesn't contains name or email headers.
                                if(nameIdx > -1 && nameIdx < triggerHeaderList.size()) {
                                    triggerHeaderList.set(nameIdx,"Name"); // setting back the default header values for Name and Email in trigger target list
                                }
                                if(emailIdx > -1 && emailIdx < triggerHeaderList.size()) {
                                    triggerHeaderList.set(emailIdx, "Email"); // setting back the default header values for Name and Email in trigger target list
                                }
                                // including created time in target list
                                if(createdTimeIdx > -1)
                                    dataList.set(createdTimeIdx, eventCreatedTime);
                                triggerDAO.createParticipants(triggerHeaderList, dataList, triggermetaDataTypeList, triggerPhiDataList, participantGroupId, businessUniqueId, createdBy);
                            }
                        }
                        // add ids in the list to send notification
//                    notifyIdsList.add(notifyId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if(channel.equals(Constants.CHANNEL_REPLY) && Utils.emptyString(token)) {
                    //aggregate case for send reply to customers
                    logger.info("reply with aggregate case");
                    List participantList = triggerDAO.getMatchedParticipants(query, businessUniqueId);
                    String timezone = Utils.isNotNull(triggerNotification.getTimezone()) ? triggerNotification.getTimezone() : Constants.TIMEZONE_AMERICA_LOSANGELES;
                    // check for open questions token
                    List txtAnalyticsToken = getCategorySentimentTokenList(readableQuery, surveyUUID, businessUniqueId,timezone, participantList, triggerCondition, businessId);
                    if(txtAnalyticsToken != null) {
                        if (participantList.size() > 0 && txtAnalyticsToken.size() >= 0) {
                            // maintain common submission token in both list
                           /*
                             *  eg. trigger condition:  rating is poor and open question is positive => rating query returns matching token and open question returns empty which mean not matching any token,
                             *  since we are using and operator both condition should get satisfied.                         *
                             *  case1: dtquery matched & returns token and cateogory / sentiment for open question not matches, then empty the list
                             *  case2: dtquery matched & returns token and category / sentiment for open question matches & return token, get the tokens present in both list
                             */
                            participantList.retainAll(txtAnalyticsToken);
                            logger.info("token after eliza call {}", new JSONArray(participantList).toString());
                        } else if (txtAnalyticsToken.size() > 0) {
                            // this condition matches only for open question trigger condition. trigger condition with only open questions.
                            participantList.addAll(txtAnalyticsToken);
                        }
                    }

                    String template = addFeedback ? "immediateOrDelayNotifyWithFeedbackCustomersReply.html" : "immediateOrDelayedNotifyCustomersReply.html";

                    if(participantList.size() > 0) {
                        List tokens = new ArrayList();
                        logger.info("participantList  "+ participantList);
                        Iterator iterator = participantList.iterator();
                        while(iterator.hasNext()){
                            Map eachParticipant = (Map) iterator.next();
                            tokens.add(eachParticipant.get("submission_token").toString());
                        }

                        Map contacts = getContactEmailsByTokens(businessUniqueId, tokens);
                        logger.info("contacts  " + contacts);

                        for (int i = 0; i < tokens.size(); i++) {
                            String eachToken = tokens.get(i).toString();
                            if (contacts.containsKey(eachToken)) {
                                List toMailList = new ArrayList();
                                Map toEmailMap = new HashMap();
                                toEmailMap.put("email", contacts.get(eachToken));
                                toMailList.add(toEmailMap);

                                messageStr = triggerNotification.getMessage();
                                if (Utils.isNotNull(triggerNotification.getSubject())) {
                                    List tagstoReplace = Utils.mergeListAndEliminateDuplicates(Utils.getTokens(subject), Utils.getTokens(messageStr));
                                    if (tagstoReplace.size() > 0) {
//                                        Map participantRecords = triggerDAO.getEventBySubmissionToken(surveyUUID, eachToken);
                                        Map substituteTokens = this.computeParticipantSubstituteTokens(participantRecords, tagstoReplace);
                                        subject = Utils.replaceTokens(subject, substituteTokens);
                                        messageStr = Utils.replaceTokens(messageStr, substituteTokens);
                                    }
                                }

                                if (isAnonymous) {
                                    String anonymousText = "<p> Please be assured that your Personally Identifiable Information is protected and anonymous. This is a system-generated email and your information is not shared with the Surveyor/Pollster. </p>";
                                    messageStr = messageStr + anonymousText;
                                }

                                StringBuffer feedbackStr = new StringBuffer();
                                if (addFeedback) {
                                    Map eventMap = this.getResponsesByTokenId(surveyUUID, eachToken, businessUniqueId, "en", isHippaEnabled );
                                    List responses = (List) eventMap.get("responses");
                                    List questionText = (List) eventMap.get("questions");
                                    Iterator itrResponse = responses.iterator();
                                    Iterator itrQuestion = questionText.iterator();

                                    while (itrResponse.hasNext()) {
                                        String response = (String) itrResponse.next();
                                        String question = (String) itrQuestion.next();
                                        feedbackStr.append("<li style='list-style-type: decimal;margin:0 0 10px 0;padding:0 0 0 8px;'><p class='lead' style='color: #000000; font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0 0 5px 0; padding: 0; text-align: left;'>");
                                        feedbackStr.append(question);
                                        feedbackStr.append("</p><p style='color:#757989;font-family: \"Avenir LT W01_85 Heavy1475544\",-apple-system,system-ui,BlinkMacSystemFont,\"Segoe UI\",Roboto,\"Helvetica Neue\",Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 21px; margin: 0; padding: 0; text-align: left;'>");
                                        feedbackStr.append(response);
                                        feedbackStr.append("</p></li>");
                                    }
                                }

                                //DTV-5582 --> reply to respondent contact us mail fix
                                String replyRespondent = "";
                                if(Utils.notEmptyString(replyToRespondent)){
                                    List mails = new ArrayList();
                                    List urls = new ArrayList();
                                    replyRespondent = replyToRespondent;
                                    //regex pattern to find email from string
                                    Matcher matcher = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(replyToRespondent);
                                    while(matcher.find()){
                                        String mailText = matcher.group().trim();
                                        mails.add(mailText);
                                    }

                                    //Email id found in reply to respodnent string
                                    for(int j=0; j<mails.size(); j++) {
                                        String eachMailText = mails.get(j).toString();
                                        if (Utils.notEmptyString(eachMailText)) {
                                            String format = "<a\n" +
                                                    "href=\"mailto:" + eachMailText + "\"\n" +
                                                    "style=\"color: " + hexCode + "; text-decoration: none;\">" + eachMailText + "</a>";
                                            replyRespondent = replyRespondent.replace(eachMailText, format);
                                            //to avoid email in url validations
                                            //DTV-5682
                                            replyToRespondent = replyToRespondent.replace(eachMailText, "");
                                        }
                                    }

                                    //DTV-5682
                                    //regex pattern to find URLs from string
                                    //String urlRegex = "\\b((?:https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:, .;]*[-a-zA-Z0-9+&@#/%=~_|])";
                                    String urlRegex = "((http?|https|ftp|file)://)?((W|w){3}.)?[a-zA-Z0-9]+\\.[a-zA-Z]+";
                                    Matcher urlMatcher = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE).matcher(replyToRespondent);
                                    while(urlMatcher.find()){
                                        String urlText = urlMatcher.group().trim();
                                        urls.add(urlText);
                                    }


                                    //DTV-5682
                                    //urls  found in reply to respodnent string
                                    for(int k=0; k<urls.size(); k++) {
                                        String eachUrlText = urls.get(k).toString();
                                        if (Utils.notEmptyString(eachUrlText)) {
                                            String format = "<a\n" +
                                                    "href=\"" + eachUrlText + "\"\n" +
                                                    "style=\"color: " + hexCode + "; text-decoration: none;\">" + eachUrlText + "</a>";
                                            replyRespondent = replyRespondent.replace(eachUrlText, format);
                                        }
                                    }
                                }
                                String htmlContent = getFileContent(template);
                                String message = "";
                                HashMap templateParam = new HashMap();
                                templateParam.put("feedback", feedbackStr);
                                templateParam.put("message", messageStr);
                                templateParam.put("triggerName", triggerName);
                                templateParam.put("surveyName", surveyName);
                                templateParam.put("logo", logo);
                                templateParam.put("replyRespondent", replyRespondent);
                                StrSubstitutor sub = new StrSubstitutor(templateParam);
                                message = sub.replace(htmlContent);

                                emailUsageCount = emailUsageCount + toEmailList.size();
                                if(!onprem) {
                                    Map emailMap = new HashMap();
                                    emailMap.put("from", fromEmailMap);
                                    emailMap.put("tos", toMailList);
                                    emailMap.put("subject", subject);
                                    emailMap.put("body", message);
                                    notifyEmailList.add(emailMap);
                                }
                                else{
                                    SendEmailNotificationOnprem(toMailList, subject, message, fromEmailCustomized, fromNameCustomized);
                                }
                            }
                        }
                    }
                }else{
                    //aggregate case or frequency case
                    List participantList = triggerDAO.getMatchedParticipants(query, businessUniqueId);
                    String timezone = Utils.isNotNull(triggerNotification.getTimezone()) ? triggerNotification.getTimezone() : Constants.TIMEZONE_AMERICA_LOSANGELES;
                    // check for open questions token
                    List txtAnalyticsToken = getCategorySentimentTokenList(readableQuery, surveyUUID, businessUniqueId, timezone, participantList, triggerCondition, businessId);
                    if(txtAnalyticsToken != null) {
                        if (participantList.size() > 0 && txtAnalyticsToken.size() >= 0) {
                            // maintain common submission token in both list
                            /**
                             *  eg. trigger condition:  rating is poor and open question is positive => rating query returns matching token and open question returns empty which mean not matching any token,
                             *  since we are using and operator both condition should get satisfied.                         *
                             *  case1: dtquery matched & returns token and cateogory / sentiment for open question not matches, then empty the list
                             *  case2: dtquery matched & returns token and category / sentiment for open question matches & return token, get the tokens present in both list
                             *  */
                            participantList.retainAll(txtAnalyticsToken);
                            logger.info("token after eliza call {}", new JSONArray(participantList).toString());
                        } else if (txtAnalyticsToken.size() > 0) {
                            // this condition matches only for open question trigger condition. trigger condition with only open questions.
                            participantList.addAll(txtAnalyticsToken);
                        }
                    }
                    String dtPerfSql = "";
                    String dtAggrSql = "";
                    int respCount = participantList.size();
                    try{
                        Map queryMap = Utils.notEmptyString(readableQuery) ? mapper.readValue(readableQuery, HashMap.class) : new HashMap();
                        dtPerfSql = queryMap.containsKey(Constants.DT_PERFORMANCE) ? queryMap.get(Constants.DT_PERFORMANCE).toString() : "";
                        dtAggrSql = queryMap.containsKey(Constants.DT_AGGREGATE) ? queryMap.get(Constants.DT_AGGREGATE).toString() : "";
                        //performance score
                        if(Utils.notEmptyString(dtPerfSql)) {
                            List perfList = triggerDAO.checkPerformanceTriggerCondition(participantList, businessUniqueId, surveyUUID, scheduleId, surveyId, dtPerfSql, "", surveyStartDate, surveyEndDate);
                            if(perfList.size() > 0){
                                Map respMap = (Map) perfList.get(0);
                                respCount = respMap.containsKey("responseCount") ? Integer.parseInt(respMap.get("responseCount").toString()) : 0;
                            }
                        }
                        //aggregate score
                        if(Utils.notEmptyString(dtAggrSql)) {
                            Map aggrMap = this.checkAggregateTriggerCondition(participantList, businessUniqueId, surveyUUID, scheduleId, surveyId, dtAggrSql, false);
                            respCount = aggrMap.containsKey("responseCount") && Utils.notEmptyString(aggrMap.get("responseCount").toString()) ?
                                    Integer.parseInt(aggrMap.get("responseCount").toString()) : 0;
                        }
                    }catch (Exception e){
                        logger.error("exception at program/aggregate score readable query {}", e.getMessage());
                    }
                    Map npsMetricMap = getNpsBrandMetricNames(surveyMap, businessUniqueId);
                    String triggerReadableText = this.getTriggerReadableText(triggerCondition, businessUniqueId, npsMetricMap);
                    String template = "aggregateNotification.html";
                    String htmlContent = getFileContent(template);
                    String link = "";


                    if(!triggerCondition.equalsIgnoreCase("DTANY") && Utils.notEmptyString(surveyFilterUUID)) {
                        link = hostUrl + "/" + Constants.LANGUAGE_EN + "/programreport/" + surveyUUID + "/1/0/"+ surveyFilterUUID;
                    }else {
                        link = hostUrl + "/" + Constants.LANGUAGE_EN + "/programreport/" + surveyUUID + "/1/0";
                    }

                    HashMap templateParam = new HashMap();
                    templateParam.put("message", messageStr);
                    templateParam.put("triggerName", triggerName);
                    templateParam.put("triggerCondition", triggerReadableText);
                    templateParam.put("link", link);
                    templateParam.put("logo", logo);
                    templateParam.put("tokenCount", respCount);
                    StrSubstitutor sub = new StrSubstitutor(templateParam);
                    String message = sub.replace(htmlContent);

                    if (channel.equals(Constants.CHANNEL_EMAIL)) {
                        emailUsageCount = toEmailList.size();
                        if(!onprem) {
                            Map emailMap = new HashMap();
                            emailMap.put("from", fromEmailMap);
                            emailMap.put("tos", toEmailList);
                            emailMap.put("subject", subject);
                            emailMap.put("body", message);
                            notifyEmailList.add(emailMap);
                        }
                        else{
                            SendEmailNotificationOnprem(toEmailList, subject, message, fromEmailCustomized, fromNameCustomized);
                        }

                    } else if (channel.equals(Constants.CHANNEL_PUSH)) {
                        List notificationsList = new ArrayList();

                        //removing html tags and extracing plain text
                        messageStr = Jsoup.parse(messageStr).text();
                        // push notification param request
                        if(messageStr.contains("\"")){
                            messageStr = messageStr.replace("\"", "\\\"");
                        }
                        if(messageStr.contains("\t")){
                            messageStr = messageStr.replace("\t", "\\t");
                        }
                        if(messageStr.contains("\n")){
                            messageStr = messageStr.replace("\n", "\\n");
                        }
                        
                        logger.info("messagee "+ messageStr);
                        Map surveyDataMap = new HashMap();
                        Map dataMap = new HashMap();
                        Map eachUserDataMap = new HashMap();
                        surveyDataMap.put(Constants.PUSHMESSAGE, messageStr);
                        dataMap.put(Constants.DEEP_LINK, deeplinkTrigger);
                        dataMap.put(Constants.DATA, surveyDataMap);
                        JSONArray jsonEmailArray = new JSONArray(emailList);
                        eachUserDataMap.put(Constants.APPNAME, appName);
                        eachUserDataMap.put(Constants.EMAILS, jsonEmailArray);
                        eachUserDataMap.put(Constants.PUSHMESSAGE, messageStr);
                        eachUserDataMap.put(Constants.DATA, dataMap);
                        JSONObject json = new JSONObject(eachUserDataMap);
                        String inputJson = json.toString();
                        logger.info("input json {}", inputJson);
                        logger.info("push notification url {}", pushNotificationUrl);
                        callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_PUSH, pushNotificationUrl);

                        if(participantList.size() > 0) {
                            List tokens = new ArrayList();
                            logger.info("participantList  " + participantList);
                            Iterator iterator = participantList.iterator();
                            while (iterator.hasNext()) {
                                Map eachParticipant = (Map) iterator.next();
                                tokens.add(eachParticipant.get("submission_token").toString());
                            }
                            surveyDataMap.put(Constants.TOKENS, tokens);
                            surveyDataMap.put(Constants.BUSINESS_ID, businessId);
                            surveyDataMap.put(Constants.SURVEY_NAME, surveyName);
                            surveyDataMap.put(Constants.SURVEYUUID, surveyUUID);
                            surveyDataMap.put(Constants.EMAILS, emailList);
                            notificationsList.add(surveyDataMap);
                            this.updateTriggerPushNotifications(notificationsList, businessUniqueId);
                        }
                    }
                    else if(channel.equals(Constants.LIST) && !isAnonymous){
                        // TODO this case is not required - based on the requirement
                        // add respondent details to target list
                        List<String> triggerHeaderList = new ArrayList();
                        List triggermetaDataTypeList = new ArrayList();
                        List<Boolean> triggerPhiDataList = new ArrayList<>();
                        int participantGroupId = 0;
                        // if channel is list, get particpant_id and header data details
                        if(channel.equals(Constants.LIST)){
                            Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
                            participantGroupId = Utils.isNotNull(scheduleMap.get("participant_group_id")) ? (int)scheduleMap.get("participant_group_id") : 0;
                            TriggerListHeaderData triggerHeader = scheduleMap.containsKey("header_data") && Utils.isNotNull(scheduleMap.get("header_data")) ? mapper.readValue(scheduleMap.get("header_data").toString(), TriggerListHeaderData.class) : new TriggerListHeaderData();
                            triggerHeaderList = triggerHeader.getHeader();
                            triggermetaDataTypeList = triggerHeader.getMetaData();
                            triggerPhiDataList = Utils.isNull(triggerHeader.getPhiData()) ? triggerHeader.getPhiData() : new ArrayList<>();
                        }
                        // intialize data with header list size
                        List dataList = (triggerHeaderList.size() > 0 ) ? new ArrayList(Collections.nCopies(triggerHeaderList.size(), "")) : new ArrayList();
                        // get all matched participants for aggregate case
                        List tokensList = triggerDAO.getMatchedParticipants(query, businessUniqueId);
                        Iterator itrList = tokensList.iterator();
                        while(itrList.hasNext()) {
                            //loop through each token and get it participant details
                            Map participantMap = triggerDAO.getParticipantByToken(token, surveyUUID);
                            Map eventMetaDataMap = participantMap.containsKey("metadata") ? mapper.readValue(participantMap.get("metadata").toString(), HashMap.class) : new HashMap();
                            Set keySet = eventMetaDataMap.keySet();
                            Iterator itr = keySet.iterator();
                            // get respondent details
                            while (itr.hasNext()) {
                                String metaDataField = (String) itr.next();
                                String metaDataVal = eventMetaDataMap.get(metaDataField).toString();
                                // set participant details  for list trigger action
                                //if (triggerHeaderList.contains(metaDataField)) {
                                if(containsCaseInsensitive(metaDataField, triggerHeaderList)){
                                    int metaDataTypeIdx = triggerHeaderList.indexOf(metaDataField);
                                    if(metaDataTypeIdx > -1)
                                        dataList.set(metaDataTypeIdx, metaDataVal);
                                }
                            }
                            if(dataList.size() > 0)
                                triggerDAO.createParticipants(triggerHeaderList, dataList, triggermetaDataTypeList, triggerPhiDataList, participantGroupId, businessUniqueId, createdBy);
                        }
                    }
                }

                if (notifyEmailList.size() > 0) {
                    if(!onprem) {
                        String inputJson = org.json.simple.JSONArray.toJSONString(notifyEmailList);
                        logger.debug("email request {}", inputJson);
                        String bulkEmailUrl = emailNotificationUrl + "/bulk";//"https://utility-test.dropthought.com/api/v1/notification/email/bulk";
                        callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_EMAIL, bulkEmailUrl);
                    }
                }

                // update sent status to completed
                //triggerDAO.updateNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED);
                //DTV-6955, update sent status to completed in trigger_notification_{buuid} table
                triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                //DTV-6955, Delete the notification in trigger_notification table by notifyId
                triggerDAO.deleteTriggerNotificationById(notifyId);

                //updating emails usage count
                if(emailUsageCount > 0){
                    updateTriggersEmailUsageCount(businessId, emailUsageCount);
                }
//            }
        }catch(Exception e){
            e.printStackTrace();
        }
        logger.debug("end send trigger notification");
    }

    /**
     * method to send email notification by email micro service for onPrem case
     * @param toEmailList
     * @param subject
     * @param message
     */
    public void SendEmailNotificationOnprem(List toEmailList, String subject, String message, String fromEmailCustomized, String fromNameCustomized){
        try {
            /**SE-752 Onprem - should not send bulk emails. The signature is constant in other email services.
             So bulk emails are not available in other email services.*/
            Iterator emailItr = toEmailList.iterator();
            while (emailItr.hasNext()) {
                Map toEmailMap = (Map)emailItr.next();
                ///call email utility api
                Map emailMap = new HashMap();
                emailMap.put("from", fromEmailCustomized);
                emailMap.put("to", toEmailMap.get("email"));
                emailMap.put("subject", subject);
                emailMap.put("body", message);
                emailMap.put("name", fromNameCustomized);
                String inputJson = new JSONObject(emailMap).toString();
                callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_EMAIL, emailNotificationUrl);
            }
        }catch (Exception e){
            logger.error("exception at SendEmailNotificationOnprem {}", e.getMessage());
        }
    }

    /**
     * method to insert into notification table based on trigger condition
     * @param triggerSchedule
     */
    public void insertNotificationForIdentifiedTriggers(TriggerSchedule triggerSchedule) {
        logger.debug("begin insert trigger notification");
        int scheduleId = Utils.isNotNull(triggerSchedule.getScheduleId()) && Utils.notEmptyString(triggerSchedule.getScheduleId()) ? Integer.parseInt(triggerSchedule.getScheduleId()) : 0;
        try {
            int surveyId = Integer.parseInt(triggerSchedule.getSurveyId());
            int businessId = Integer.parseInt(triggerSchedule.getBusinessId());
            int triggerId = Integer.parseInt(triggerSchedule.getTriggerId());
            //DTV-8038
            String channel = triggerSchedule.getChannel();
//            String lastProcessDate = triggerSchedule.getLastProcessedDate();
            String timezone = triggerSchedule.getTimezone();
            String triggerConfig = (String) triggerSchedule.getTriggerConfig();
            String triggerStartDate = triggerSchedule.getStartDate()+" "+ triggerSchedule.getStartTime();
            String frequency = triggerSchedule.getFrequency();
            String createdTime = (Utils.isNotNull(triggerSchedule.getCreatedTime()) && Utils.notEmptyString(triggerSchedule.getCreatedTime())) ? triggerSchedule.getCreatedTime() : "" ;
            //DTV-5582
            String replyToRespondent = (Utils.isNotNull(triggerSchedule.getReplyToRespondent()) && Utils.notEmptyString(triggerSchedule.getReplyToRespondent())) ? triggerSchedule.getReplyToRespondent() : "" ;
            //DTV-4129
            /*if (frequency.equals(Constants.FREQUENCY_SINGLE)) {
                triggerStartDate = Utils.addMinutesToDate(triggerStartDate, -5);
            }*/

            //after getting entry into worker, changing status from tobeprocess to processing.
            this.updateScheduleSentStatusByScheduleId(scheduleId, Constants.STATUS_PROCESSING);

            // get businessUniqueId by businessId
            String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);
            // get surveyUniqueId
            String surveyUinqueId = triggerDAO.getSurveyUniqueIdById(surveyId, businessUniqueId);
            if (Utils.notEmptyString(businessUniqueId) && Utils.notEmptyString(surveyUinqueId)) {
                // get trigger condition for the triggerId
                Map triggerMap = triggerDAO.getTriggerMapByTriggerId(businessUniqueId, triggerId);
                if (Utils.isNotNull(triggerMap)) {
                    String triggerCondition = triggerMap.containsKey(Constants.TRIGGER_CONDITION) && Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : "";
                    String readableQuery = triggerMap.containsKey(Constants.READABLE_QUERY) && Utils.isNotNull(triggerMap.get(Constants.READABLE_QUERY)) ? triggerMap.get(Constants.READABLE_QUERY).toString() : "";
                    // method to get formatted date based on trigger config and notification type
                    Map dateInterval = this.ComputeDateWithIntervals(triggerConfig, timezone);
                    // get notification type
                    boolean isAggregateType = (boolean) dateInterval.get(Constants.ISAGGREGATE);
                    // start time to notify
                    String triggerNotifyTime = (String) dateInterval.get(Constants.FORMATTED_DATE);
                    // DTV-5315, To include frequency notification type
                    boolean isFrequencyType = dateInterval.containsKey(Constants.ISFREQUENCY) && Utils.isNotNull(dateInterval.get(Constants.ISFREQUENCY)) && (boolean) dateInterval.get(Constants.ISFREQUENCY);
                    int intervalInMinutes = dateInterval.containsKey("intervalInMinutes") && Utils.isNotNull(dateInterval.get("intervalInMinutes")) ? Integer.parseInt(dateInterval.get("intervalInMinutes").toString()) : 0;

                    Map processedConditionMap;
                    if (Utils.emptyString(readableQuery)) {
                        logger.info("readable query is empty");
                        // get matching token for the trigger condition
                        processedConditionMap = this.processTriggerCondition(triggerCondition, surveyUinqueId, surveyId, businessUniqueId, triggerStartDate, isAggregateType, scheduleId, triggerNotifyTime, timezone, isFrequencyType);
                        if (Utils.isNotNull(processedConditionMap) && processedConditionMap.containsKey(Constants.READABLE_QUERY)) {
                            // update the readable query in trigger table
                            triggerDAO.updateTriggerReadableQuery(businessUniqueId, processedConditionMap.get(Constants.READABLE_QUERY).toString(), triggerId);
                            readableQuery = processedConditionMap.get(Constants.READABLE_QUERY).toString();
                        }
                    } else {
                        logger.info("readable query is not empty");
                        // readable query exists, execute and get the result
                        // processedConditionMap = this.executeReableQuery(surveyUinqueId, readableQuery, lastProcessDate, isAggregateType);
                        processedConditionMap = this.executeReableQuery(triggerCondition, surveyUinqueId, readableQuery, isAggregateType, scheduleId, triggerNotifyTime, triggerStartDate, surveyId, businessUniqueId, isFrequencyType);
                    }


                    //check if processedConditionMap results contains tokens list
                    if (Utils.isNotNull(processedConditionMap)) {
//                        String frequency = triggerSchedule.getFrequency();
                        // get submission token list
                        List tokenList = processedConditionMap.containsKey(Constants.TOKENS) ? (List) processedConditionMap.get(Constants.TOKENS) : new ArrayList();
                        List contactList = Utils.isNotNull(triggerSchedule.getContacts()) ? triggerSchedule.getContacts() : new ArrayList();
                        boolean isConditionMet = processedConditionMap.containsKey(Constants.ISCONDITIONMET) && (boolean) processedConditionMap.get(Constants.ISCONDITIONMET);

                        //DTV-8019, 8038
                        Boolean isAggregateConditionMatched = processedConditionMap.containsKey("isAggregateConditionMatched") && Utils.isNotNull(processedConditionMap.get("isAggregateConditionMatched"))
                                ? (Boolean) processedConditionMap.get("isAggregateConditionMatched") : null;
                        //DTV-8019, 8038
                        String aggregateConditionFailedToken = processedConditionMap.containsKey("aggregateConditionFailedToken") && Utils.isNotNull(processedConditionMap.get("aggregateConditionFailedToken"))
                                ? processedConditionMap.get("aggregateConditionFailedToken").toString() : "";


                        //  create notification to send
                        TriggerNotification triggerNotification = new TriggerNotification();
                        triggerNotification.setContacts(contactList);
                        triggerNotification.setSubject(triggerSchedule.getSubject());
                        triggerNotification.setMessage(triggerSchedule.getMessage());
                        triggerNotification.setAddFeedback(triggerSchedule.isAddFeedback());
                        triggerNotification.setChannel(triggerSchedule.getChannel());
                        triggerNotification.setStartTime(dateInterval.get(Constants.FORMATTED_DATE).toString());
                        triggerNotification.setScheduleId(triggerSchedule.getScheduleId());
                        triggerNotification.setConditionQuery(processedConditionMap.containsKey(Constants.CONDITIONQUERY) ? processedConditionMap.get(Constants.CONDITIONQUERY).toString() : "");
                        //DTV-5331 --> Adding triggerStatus flag to check trigger is in active or inactive state
                        //triggerRunner will pick only active triggers, so trigger status = 1
                        triggerNotification.setTriggerStatus(Constants.STATUS_ACTIVE_1);
                        // aggregate type : The aggregate alerts will not have meta data and feedbacks attached. Instead it will provide a link to the respondents page.
                        // compare current date with last processed date to avoid duplicate entry for same day
                        /**
                         * Example : trigger condition time 03/25/2021 at 12:00 AM until 08/31/2021 at 11:59 PM
                         *  created action with time at 4 am daily, timezone = Asia/Calcutta
                         * if action is created at 4 pm  03/25/2021 with condition at 4 am daily.
                         * we need to validate created time (4 pm) exceeds or not with action time (4 am) for starting date (03/25/2021).
                         *
                         * triggerNotifytime must be greater than createdDateTime
                         */
                        logger.info("triggerNotifyTime  "+triggerNotifyTime);
                        logger.info("createdTime  "+createdTime);
                        if (isAggregateType && !isFrequencyType && !(this.checkNotificationExistsForTheSchedule(scheduleId, triggerNotifyTime, businessUniqueId)) && Utils.compareDates(triggerNotifyTime, createdTime)) {
                            logger.info("aggregate case");
                            triggerNotification.setSubmissionToken(null);
                            //DTV-6955
                            String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                            //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                            triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUniqueId);
                        }
                        /**
                         * DTV-8038, 8019 --> checking aggregateCondition for recommended focusArea trigger
                         * Recommended focusArea schedules will be immediate type
                         */
                        else if(!isAggregateType && !isFrequencyType && channel.equalsIgnoreCase("focusArea") && isAggregateConditionMatched != null){
                            this.processFocusAreaSchedule(surveyId, businessUniqueId, isAggregateConditionMatched, aggregateConditionFailedToken, tokenList, triggerSchedule, triggerNotification);
                        }
                        //DTV-8038, 8019 --> added 'focusArea' channel condition
                        else if (!isAggregateType && !isFrequencyType && !channel.equalsIgnoreCase("focusArea")) { //DTV-5315
                            logger.info("delayed or immediate");
                            logger.info("token {}", new JSONArray(tokenList).toString());
                            // get tokens from text analytics if condition added in trigger
                            List txtAnalyticsTokenList = getCategorySentimentTokenList(readableQuery, surveyUinqueId, businessUniqueId, timezone, tokenList, triggerCondition, businessId);
                            if (txtAnalyticsTokenList != null) { // txtAnalyticsTokenList is null if open ended question not available
                                if (tokenList.size() > 0 && txtAnalyticsTokenList.size() >= 0) {
                                    // maintain common submission token in both list
                                    tokenList.retainAll(txtAnalyticsTokenList);
                                    logger.info("token after eliza call {}", new JSONArray(tokenList).toString());
                                } else if (txtAnalyticsTokenList.size() > 0) {
                                    // this condition matches only for open question trigger condition
                                    tokenList.addAll(txtAnalyticsTokenList);
                                }
                            }
                            //check if submission tokens not exists in notification table. Adding this method to avoid duplicate entry in notification, since tokens for open questions will be fetched after eliza call
                            if (tokenList.size() > 0)
                                tokenList = triggerDAO.checkNotificationTokensExistsForTheSchedule(scheduleId, tokenList, businessUniqueId);

                            if (triggerSchedule.getChannel().equals("reply")) {
                                Map contacts = this.getContactEmailsByTokens(businessUniqueId, tokenList);
                                logger.info("contacts  " + contacts);
                                for (int i = 0; i < tokenList.size(); i++) {
                                    String eachToken = tokenList.get(i).toString();
                                    if (contacts.containsKey(eachToken)) {
                                        List eachContact = new ArrayList();
                                        eachContact.add(contacts.get(eachToken));
                                        triggerNotification.setContacts(eachContact);
                                        triggerNotification.setSubmissionToken(eachToken);
                                        triggerNotification.setReplyToRespondent(replyToRespondent);
                                        //DTV-6955
                                        String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                                        //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                                        triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUniqueId);
                                        // update schedule sent_status
                                    }
                                }
                            } else {
                                // Delayed or Immediate type: one email for each back will be sent. Apply meta data replacement. Attach feedback if necessary.
                                Map queryMap = Utils.notEmptyString(readableQuery) ? mapper.readValue(readableQuery, HashMap.class) : new HashMap();
                                Iterator itr = tokenList.iterator();
                                while (itr.hasNext()) {
                                    triggerNotification.setSubmissionToken((String) itr.next());
                                    //DTV-6955
                                    String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                                    //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                                    triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUniqueId);
                                    // update schedule sent_status
                                }
                            }
                        }
                        /**
                         * DTV-5315, else block to handle frequncy type actions.
                         * Notification type should be either aggregate or frequency.
                         * Inert notification as soon as condition satisfied
                         * For second reminder check previous created notification created time by schedule id
                         * Check new notify time exceeds previous notification start time then insert new notification
                         */
                        else if(isFrequencyType && !isAggregateType && isConditionMet
                                && !this.checkFrequencyNotificationExistsForTheSchedule(scheduleId, isConditionMet, triggerNotification, intervalInMinutes, businessUniqueId)){
                            logger.info("frequency case");
                            triggerNotification.setSubmissionToken(null);
                            //DTV-6955
                            String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                            //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                            triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUniqueId);
                        }
                        if (frequency.equals(Constants.FREQUENCY_SINGLE)) {
                            triggerDAO.updateScheduleSentStatusByScheduleId(scheduleId, Constants.STATUS_COMPLETED);
                        } else {
                            // changing the sent status to null since setting to completed stops new feedbacks to validate against the trigger condition
                            triggerDAO.updateScheduleSentStatusByScheduleId(scheduleId, null);
                        }
                    } else {
                        /**DTV-3735, If schedule status is processing and not inserted in trigger_notification table
                         * changing processing state to null
                         */
                        if(scheduleId > 0) {
                            triggerDAO.updateScheduleSentStatusByScheduleId(scheduleId, null);
                        }
                    }
                }
            }else if (Utils.emptyString(surveyUinqueId)){
                this.updateScheduleSentStatusByScheduleId(scheduleId, "SURVEYID_" + Constants.INVALID);
            }
        }catch (Exception e){
            /** If schedule status is tobeprocess and not inserted in trigger_notification table
             * changing processing state to null
             */
            if (scheduleId > 0) {
                triggerDAO.updateScheduleSentStatusByScheduleId(scheduleId, "failed");
            }
            e.printStackTrace();
        }
        logger.debug("end insert trigger notification");
    }

    /**
     * method to get category/sentiment submission token list from eliza api
     * @param readableQuery
     * @param surveyUniqueId
     * @param businessUniqueId
     * @return
     */
    public List getCategorySentimentTokenList(String readableQuery, String surveyUniqueId, String businessUniqueId, String timezone,
                                              List token, String triggerCondition, int businessId){
        List txtAnalyticsTokenList = new ArrayList();
        try {
            String TA_TYPE = "advanced";
            Map queryMap = mapper.readValue(readableQuery, HashMap.class);
            Map txtAnalyticsCondition = queryMap.containsKey(Constants.TXT_ANALYTICS) ? (Map) queryMap.get(Constants.TXT_ANALYTICS) : new HashMap();
            String dtQueryCondition = queryMap.containsKey(Constants.DT_QUERY) ? queryMap.get(Constants.DT_QUERY).toString() : "";
            // if dtquery is available in readable_query and no token matches for that query, then no need to call eliza since output token will be common token from both dtquey and txtanalytics
            if( token.size() == 0){ //Utils.notEmptyString(dtQueryCondition) && //commented out this since dtQueryCondition empty case also fetches token based on triggerStart date and token not available in trigger_notification which is fine. (if token is empty the TA call returns all the feedbacks for which emails are sent already. To stop this I'm removing this check)
               return new ArrayList();
            }

            String taType = triggerDAO.getTAType(businessId);
            if(txtAnalyticsCondition.size() > 0 && triggerCondition.length() > 0 && taType != null) {

                Map questionIdsMap = this.formTARequstFromTriggerCondition(triggerCondition);

                Map inputMap = new HashMap();
                inputMap.put("questionIds", questionIdsMap);
                inputMap.put("tokens", token);
                logger.info("input map = "+inputMap);

                String uri = elizaUrl;
                String urlToCall = uri + businessUniqueId + "/" + surveyUniqueId.replace("-", "_") + "?type="+taType;
                logger.info("TA url new check {}", urlToCall);

                JSONObject json = new JSONObject(inputMap);
                String inputJson = json.toString();
                logger.info(inputJson);

                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_JSON);
                HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
                ResponseEntity<Object> responseEntity = restTemplate.postForEntity(urlToCall, entity, Object.class);

                if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                    if (responseEntity.getBody() != null) {
                        logger.info("eliza request sent successfully");
                        logger.info(responseEntity.getBody().toString());
                        Map resultMap = (Map) responseEntity.getBody();
                        if (Utils.isNotNull(resultMap) && resultMap.containsKey("feedbackDataList")) {
                            List resultList = (List) resultMap.get("feedbackDataList");
                            Iterator feedbackItr = resultList.iterator();
                            while (feedbackItr.hasNext()) {
                                Map dataMap = (Map) feedbackItr.next();
                                String submissionToken = dataMap.get("submissionToken").toString();
                                txtAnalyticsTokenList.add(submissionToken);
                            }
                        }
                    }
                }
                logger.info("txtAnalyticsTokenList "+txtAnalyticsTokenList);

            }else{
                // open question not avilable
                return  null;
            }
        }catch (Exception e){
            logger.error(e.getMessage());
        }
        return  txtAnalyticsTokenList;
    }

    /** function to get filter subQueries
      * @param query
     * @return
     */

    public Map filterSubQueries(String query){

        Map result = new HashMap();
        List queries = new ArrayList();
        List headerList = new ArrayList();
        List dataList = new ArrayList();
        List metadataList = new ArrayList();
        List operators = new ArrayList();

        //stores participantGroupUUID, questionUUID
        List headersUUIDList = new ArrayList();

        try {
            String[] andQueries = query.split("&&");
            for (int i = 0; i < andQueries.length; i++) {
                List eachMetadataValuesList = new ArrayList();
                List eachHeadersList = new ArrayList();
                String metadataType = "";
                String operator = "";
                String metaData = "";
                String headerUUID = "";
                String[] orQueries = andQueries[i].split("\\|\\|");
                queries.add(andQueries[i]);

                for(int k=0; k < orQueries.length; k++) {
                    //headerUUId --> participantGroupUUID or questionUUID
                    String eachValue = "";

                    if(orQueries[k].contains("dtmetadata") || orQueries[k].contains("question")||orQueries[k].contains("dtlink")||orQueries[k].contains("dtqr")||orQueries[k].contains("dtkiosk")) {
                        headerUUID = orQueries[k].split("\\.")[2];
                        metaData = orQueries[k].split("\\.")[3].split("=|!=|>=|<=|<|>|~")[0];
                        eachValue = orQueries[k].split("\\.")[3].split("=|!=|>=|<=|<|>|~")[1];
                    }else{
                        headerUUID = orQueries[k].split("\\.")[1];
                        metaData = orQueries[k].split("\\.")[2].split("=|!=|>=|<=|<|>|~")[0];
                        eachValue = orQueries[k].split("\\.")[2].split("=|!=|>=|<=|<|>|~")[1];
                    }

                    String operatorSign = this.getOperatorFromQuery(orQueries[k]);

                    // check for between case in triggerCondition
                    if(orQueries[k].contains("~")){
                        //between case of DATE or NUMBER metadata type for trigger condition
                        String[] dates = eachValue.replaceAll("\\[","").replaceAll("]","").split(",");

                        if(Utils.isStringNumber(dates[0]) && Utils.isStringNumber(dates[1])) { // check for numeric metadata type
                            int num1 = Integer.parseInt(dates[0]);
                            int num2 = Integer.parseInt(dates[1]);
                            eachValue = num1 + "~" + num2;
                            metadataType = "NUMBER";
                        }else {
                            //date metadata type
                            String fromDate = dates[0] + " 00:00:00"; // start of the from date
                            String toDate = dates[1] + " 23:59:59"; // start of the from date
                            eachValue = fromDate + "~" + toDate;
                            metadataType = "DATE";
                        }
                        operatorSign = "~";
                    }

                    //check for value is date
                    String date = Utils.getValidDateTime(eachValue, true, "-");
                    if(date.length() > 0){
                        if(eachHeadersList.contains(metaData)){
                            //this one is for date between case for filter condition.
                            String value = eachMetadataValuesList.get(k-1).toString();
                            eachValue = value + "~" + date;
                            eachMetadataValuesList.remove(value);
                            operatorSign = "~";
                        }else {
                            eachValue = date;
                        }
                        metadataType = "DATE";
                    }
                    //check for value is Numeric type
                    metadataType = Utils.isStringNumber(eachValue) && Utils.emptyString(metadataType) ? "NUMBER" : metadataType;
                    if(!eachHeadersList.contains(metaData)){
                        eachHeadersList.add(metaData);
                    }
                    eachMetadataValuesList.add(eachValue);
                    operator = operatorSign;
                }

                if(!(metadataType.length() > 0)){
                    metadataList.add("STRING");
                }else {
                    if(metadataType.equals("NUMBER") && !andQueries[i].contains("~")){
                        metadataType = eachMetadataValuesList.size() > 1 ? "STRING" : metadataType;
                    }
                    metadataList.add(metadataType);
                }

                headersUUIDList.add(headerUUID);
                headerList.add(metaData);
                operators.add(operator);
                dataList.add(eachMetadataValuesList);
            }
            result.put("header", headerList);
            result.put("metadata", metadataList);
            result.put("data", dataList);
            result.put("operators", operators);
            result.put("queries", queries);
            result.put("headerUUID", headersUUIDList);
        }catch (Exception e){
            logger.error("Exception at filterSubQueries. Msg {}", e.getMessage());
        }
        return result;
    }

    public boolean compareFilterQueryTriggerQuery(String filterQuery, String triggerQuery){
        boolean result = false;
        try
        {
            if(filterQuery.length() > 1 && triggerQuery.length() > 1){

                Map filterQueryMap = this.filterSubQueries(filterQuery);
                Map triggerQueryMap = this.filterSubQueries(triggerQuery);

                List filterQueries = Utils.isNotNull(filterQueryMap.containsKey("queries")) ? (List) filterQueryMap.get("queries") : new ArrayList();
                List triggerQueries = Utils.isNotNull(triggerQueryMap.containsKey("queries")) ? (List) triggerQueryMap.get("queries") : new ArrayList();
                List filterMetadataType = Utils.isNotNull(filterQueryMap.containsKey("metadata")) ? (List) filterQueryMap.get("metadata") : new ArrayList();
                List triggerMetadataType = Utils.isNotNull(triggerQueryMap.containsKey("metadata")) ? (List) triggerQueryMap.get("metadata") : new ArrayList();
                List filterMetadataValues = Utils.isNotNull(filterQueryMap.containsKey("data")) ? (List) filterQueryMap.get("data") : new ArrayList();
                List triggerMetadataValues = Utils.isNotNull(triggerQueryMap.containsKey("data")) ? (List) triggerQueryMap.get("data") : new ArrayList();
                List filterOpearatorsList = Utils.isNotNull(filterQueryMap.containsKey("operators")) ? (List) filterQueryMap.get("operators") : new ArrayList();
                List triggerOperatorsList = Utils.isNotNull(triggerQueryMap.containsKey("operators")) ? (List) triggerQueryMap.get("operators") : new ArrayList();
                List filterHeadersUUID = Utils.isNotNull(filterQueryMap.containsKey("headerUUID")) ? (List) filterQueryMap.get("headerUUID") : new ArrayList();
                List triggerHeadersUUID = Utils.isNotNull(triggerQueryMap.containsKey("headerUUID")) ? (List) triggerQueryMap.get("headerUUID") : new ArrayList();
                List filterHeaders = Utils.isNotNull(filterQueryMap.containsKey("header")) ? (List) filterQueryMap.get("header") : new ArrayList();
                List triggerHeaders = Utils.isNotNull(triggerQueryMap.containsKey("header")) ? (List) triggerQueryMap.get("header") : new ArrayList();

                logger.info("filterQueryMap  "+filterQueryMap);
                logger.info("triggerQueryMap  "+triggerQueryMap);

                if(filterMetadataType.size() > triggerMetadataType.size()){
                    return false;
                }
                else {
                    for(int i=0; i<filterMetadataType.size(); i++) {
                        int index = 0;
                        String type = "";
                        boolean valid = false;
                        String filterHeaderUUID = filterHeadersUUID.get(i).toString();
                        String filterHeader = filterHeaders.get(i).toString();

                        if (filterHeader.equals("response")) {
                            valid = triggerHeadersUUID.contains(filterHeaderUUID);
                            index = triggerHeadersUUID.indexOf(filterHeaderUUID);
                        } else {
                            valid = triggerHeaders.contains(filterHeader);
                            index = triggerHeaders.indexOf(filterHeader);
                        }

                        if (valid) {
                            if (filterMetadataType.get(i).equals(triggerMetadataType.get(index))) {
                                type = triggerMetadataType.get(index).toString();
                                List eachFilterData = (List) filterMetadataValues.get(i); //
                                List eachTriggerData = (List) triggerMetadataValues.get(index);
                                String eachFilterValue = eachFilterData.get(0).toString();
                                String eachTriggerValue = eachTriggerData.get(0).toString();
                                String eachFilterOperator = filterOpearatorsList.get(i).toString();
                                String eachTriggerOperator = triggerOperatorsList.get(index).toString();

                                switch (type) {
                                    case "STRING":

                                        if (eachFilterData.get(0).equals("All")) {
                                            result = true;
                                        } else if (eachTriggerData.get(0).equals("All")) {
                                            //if trigger metadata values has All, check for filter metadata values.
                                            result = eachFilterData.get(0).equals("All") ? true : false;
                                        } else {
                                            //check for trigger metadata values in filter metadata
                                            result = eachFilterData.containsAll(eachTriggerData) ? true : false;
                                        }
                                        logger.info("result " + result);
                                        if (!result) {
                                            return false;
                                        }
                                        break;

                                    case "DATE": case "NUMBER":
                                        //between case
                                        if (eachFilterOperator.equals("~")) {
                                            if (eachTriggerOperator.equals("~")) {
                                                String[] filterBetweenValues = eachFilterValue.split("~");
                                                String[] triggerBetweenValues = eachTriggerValue.split("~");
                                                //comparing dates
                                                //filter metadata start date < trigger metadata start date && filter metadata end date > trigger  metadata end date
                                                if (type.equalsIgnoreCase("DATE")) {
                                                    result = (Utils.compareDates(triggerBetweenValues[0], filterBetweenValues[0]) && Utils.compareDates(filterBetweenValues[1], triggerBetweenValues[1]));
                                                }
                                                if (type.equalsIgnoreCase("NUMBER")) {
                                                    result = (Integer.parseInt(triggerBetweenValues[0]) >= Integer.parseInt(filterBetweenValues[0])) && (Integer.parseInt(filterBetweenValues[1]) >= Integer.parseInt(triggerBetweenValues[1]));
                                                }
                                            } else {
                                                logger.info("result " + false);
                                                return false;
                                            }
                                        } else {
                                            //check for if trigger has between operator
                                            if (eachTriggerOperator.equals("~")) {
                                                String[] triggerBetweenValues = eachTriggerValue.split("~");
                                                if (type.equalsIgnoreCase("DATE")) {
                                                    result = (this.compareDatesByOperator(triggerBetweenValues[0], eachFilterValue, eachFilterOperator) && this.compareDatesByOperator(triggerBetweenValues[1], eachFilterValue, eachFilterOperator));
                                                }
                                                if (type.equalsIgnoreCase("NUMBER")) {
                                                    result = (this.compareNumbersByOperator(triggerBetweenValues[0], eachFilterValue, eachFilterOperator) && this.compareNumbersByOperator(triggerBetweenValues[1], eachFilterValue, eachFilterOperator));
                                                }
                                            } else {
                                                //check for mismatch in operators for >, < cases
                                                if (eachTriggerOperator.equals(eachFilterOperator)) {
                                                    if (type.equalsIgnoreCase("DATE")) {
                                                        result = this.compareDatesByOperator(eachTriggerValue, eachFilterValue, eachFilterOperator);
                                                    }
                                                }
                                                if(compareOperators(eachTriggerOperator, eachFilterOperator) || eachTriggerOperator.equals("=")){
                                                    if (type.equalsIgnoreCase("NUMBER")) {
                                                        result = this.compareNumbersByOperator(eachTriggerValue, eachFilterValue, eachFilterOperator);
                                                    }
                                                } else {
                                                    logger.info("result " + false);
                                                    return false;
                                                }
                                            }
                                        }
                                        logger.info("result " + result);
                                        if (!result) {
                                            return false;
                                        }
                                        break;

                                }
                            } else {
                                // mismatch in metadata type in filter query & trigger condition
                                logger.info("result " + false);
                                return false;
                            }
                        }else {
                            // metadata or question of filter not present in trigger
                            logger.info("result " + false);
                            return  false;
                        }
                    }
                }
            }

        }catch (Exception e){
            logger.error("Exception at compareFilterQueryTriggerQuery. Msg {}", e.getMessage());
        }
        return result;
    }

    /**
     * New Function to get prefix and suffix for filter query
     * @param query
     * @return
     */
    public String  getOperatorFromQuery(String query){
        String operator = "";
        logger.info("The value innnn query = "+query);
        if (query.contains(">=")) {
            logger.info("greater than or equal to");
            return ">=";
        }
        if (query.contains("<=")) {
            logger.info("less than or equal to");
            return "<=";
        }
        if (query.contains(">")) {
            logger.info("greater than");
            return ">";
        }
        if (query.contains("<")) {
            logger.info("less than");
            return "<";
        }
        if (query.contains("!=")) {
            logger.info("not equal to");
            return "!=";
        }
        if (query.contains("=")) {
            logger.info("equal to");
            return "=";
        }
        return operator;
    }

    /**
     * Compare two dates in the format yyyy-MM-dd HH:mm:ss based on operator (>, <)
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public Boolean compareDatesByOperator(String dateString1, String dateString2, String operator) {
        Boolean returnValue = false;
        try {
            Boolean result = false;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) > 0) {
                result = true;
            } else if (date1.compareTo(date2) == 0) {
                result = true;
            }
            if (operator.equals(">")) {
                returnValue = result ? true : false;
            }
            if (operator.equals("<")) {
                returnValue = result ? false : true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    /**
     * Compare two numbers based on operator (>, <)
     *
     * @param num1
     * @param num2
     * @return
     */
    public Boolean compareNumbersByOperator(String num1, String num2, String operator) {
        Boolean returnValue = false;
        try {
            Boolean result = false;
            if (Integer.parseInt(num1) >= Integer.parseInt(num2)) {
                result = true;
            } else {
                result = false;
            }
            if (operator.equals(">") || operator.equals(">=") || operator.equals("=")) {
                returnValue = result ? true : false;
            }
            if (operator.equals("<") || operator.equals("<=") || operator.equals("!=")) {
                returnValue = result ? false : true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    /**
     * Listing all read only & read write users based on comparing filter query with trigger conditions
     *
     * @param businessUniqueId
     * @param surveyId
     * @param triggerCondition
     * @return
     */
    public List getValidUsersByTriggerCondition(String businessUniqueId, int surveyId, String triggerCondition) {
        int KINGUSER = 1;
        int DEFAULTUSER = 0;
        int CSUSER = 2;
        int ADMIN = 1;
        int READONLY = 2;
        int READWRITE = 3;
        int SUPERADMIN = 4;

        List users = new ArrayList();
        try{
            List usersList = triggerDAO.getUsersByBusinessUUID(businessUniqueId);
            logger.info("usersList" + usersList);
            Iterator eachUserList = usersList.iterator();
            while (eachUserList.hasNext()) {
                Map eachUser = (Map) eachUserList.next();
                int accountRole = Utils.isNotNull(eachUser.get("account_role_id")) ? Integer.parseInt(eachUser.get("account_role_id").toString()) : 0;
                int userRole = Utils.isNotNull(eachUser.get("user_role")) ? Integer.parseInt(eachUser.get("user_role").toString()) : -1;
                int userId = Utils.isNotNull(eachUser.get("user_id")) ? Integer.parseInt(eachUser.get("user_id").toString()) : 0;
                logger.info("triggerCondition " + triggerCondition);

                /** check for admin
                 excluding cs user
                 **/
                if ((accountRole == ADMIN || accountRole == SUPERADMIN) && (userRole == DEFAULTUSER || userRole == KINGUSER||userRole==CSUSER)) {
                    users.add(eachUser);
                }

                //read only user or read write user
                if (accountRole == READONLY || accountRole == READWRITE) {

                    //check for user has survey access or not.
                    //check for if program is created by read write user
                    Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
                    int createdBy = surveyMap.containsKey("created_by") ? Integer.parseInt(surveyMap.get("created_by").toString()) : 0;

                    Map mappedSurveys = triggerDAO.getMappedSurveysByUserId(userId, businessUniqueId);
                    if (mappedSurveys.size() > 0 || (createdBy == userId)) {

                        List surveysList = mapper.readValue(mappedSurveys.get("survey_id").toString(), ArrayList.class);
                        //check for survey is mapped to read only or read write user
                        if (surveysList.contains(surveyId) || (createdBy == userId)) {
                            // to check for filter query with trigger condition if filter is applied to given program
                            logger.info("read only user {} " + userId);
                            Map userSurveyFilterMap = triggerDAO.getMappedSurveyFiltersByUserId(userId, surveyId, businessUniqueId);
                            logger.info("userSurveyFilterMap" + userSurveyFilterMap);

                            if (userSurveyFilterMap.size() > 0 && Utils.isNotNull(userSurveyFilterMap.get("filter_query"))) {
                                /**
                                 * Filter is applied for the given program, Check for the user has survey access
                                 */
                                String filterQuery = userSurveyFilterMap.get("filter_query").toString();
                                if(filterQuery.contains("DT_All")){
                                    filterQuery = filterQuery.replaceAll("DT_All", "All");
                                }
                                logger.info("filterQuery  " + filterQuery);
                                logger.info("triggerCondition  " + triggerCondition);
                                if(!triggerCondition.equalsIgnoreCase("DTANY")){
                                    //check for user has appropriate access by comparing filter condition with trigger condition
                                    if (this.compareFilterQueryTriggerQuery(filterQuery, triggerCondition)) {
                                        users.add(eachUser);
                                    }
                                }
                            }else {
                                /**
                                 * User has survey access but filter is not applied to the program
                                 */
                                users.add(eachUser);
                            }
                        }
                    }
                }
            }

        }catch (Exception e){
            logger.error("Exception at getValidUsersByTriggerCondition. Msg {}", e.getMessage());
        }
        return users;
    }


    /**
     * validating the given user for all the triggers of given surveyId
     * @param userId
     * @param surveyIds
     * @param businessUniqueId
     * @return
     */
    public int validateContactsByUserId(int userId, List surveyIds, String businessUniqueId) {

        String filterQuery = "";
        boolean mappedUser = false;
        boolean filterPresent = false;

        List triggersList = new ArrayList();
        try {
            logger.info("surveyIds  "+ surveyIds);
            Iterator surveyIdIterator = surveyIds.iterator();
            while(surveyIdIterator.hasNext()) {

                int surveyId = (int) surveyIdIterator.next();
                //check for if program is created by read write user
                Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
                int createdBy = surveyMap.containsKey("created_by") ? Integer.parseInt(surveyMap.get("created_by").toString()) : 0;

                Map mappedSurveys = triggerDAO.getMappedSurveysByUserId(userId, businessUniqueId);
                if (mappedSurveys.size() > 0 || (createdBy == userId)) {
                    List surveysList = mapper.readValue(mappedSurveys.get("survey_id").toString(), ArrayList.class);
                    //check for survey is mapped to read only or read write user
                    mappedUser = surveysList.contains(surveyId) || (createdBy == userId) ? true : false;

                    if(mappedUser){
                        //check for survey has filter applied or not.
                        Map userSurveyFilterMap = triggerDAO.getMappedSurveyFiltersByUserId(userId, surveyId, businessUniqueId);
                        logger.info("userSurveyFilterMap" + userSurveyFilterMap);

                        if (userSurveyFilterMap.size() > 0 && Utils.isNotNull(userSurveyFilterMap.get("filter_query"))) {
                            filterQuery = userSurveyFilterMap.get("filter_query").toString();
                            filterQuery = filterQuery.contains("DT_All") ? filterQuery.replaceAll("DT_All", "All") : filterQuery;
                            filterPresent = true;
                            logger.info("filterQuery  " + filterQuery);
                        }
                    }
                }

                /**
                 * Listing all the triggers for the given surveyId
                 */
                triggersList = triggerDAO.getAllTriggersBySurveyId(businessUniqueId, surveyId);
                if (triggersList.size() > 0) {
                    Iterator iterator = triggersList.iterator();
                    while (iterator.hasNext()) {
                        Map eachTriggerMap = (Map) iterator.next();
                        int triggerId = Integer.parseInt(eachTriggerMap.get("id").toString());
                        String triggerCondition = Utils.notEmptyString(eachTriggerMap.get("trigger_condition").toString()) ? eachTriggerMap.get("trigger_condition").toString() : "";
                        String triggerUUID = Utils.notEmptyString(eachTriggerMap.get("trigger_uuid").toString()) ? eachTriggerMap.get("trigger_uuid").toString() : "";

                        /**
                         * If user survey access is removed in user management, need to remove the user from contact list.
                         * If use has survey access and filter applied is changed, compare filter condition with trigger condition.
                         */
                        if (!mappedUser || filterPresent) {

                            boolean validUser = (filterQuery.length() > 0 && triggerCondition.length() > 0 && !triggerCondition.equals("DTANY")) ? this.compareFilterQueryTriggerQuery(filterQuery, triggerCondition) : false;

                            //given user is invalid, removing in trigger schedule & trigger notifications if present
                            if (!validUser) {
                                List validContacts = new ArrayList();
                                List users = triggerDAO.getUsersByBusinessUUID(businessUniqueId);
                                Iterator usersIterator = users.iterator();

                                while (usersIterator.hasNext()) {
                                    Map eachUser = (Map) usersIterator.next();
                                    String userEmail = Utils.isNotNull(eachUser.get("user_email")) ? eachUser.get("user_email").toString() : "";
                                    int usrId = Utils.isNotNull(eachUser.get("user_id")) ? Integer.parseInt(eachUser.get("user_id").toString()) : 0;
                                    if (userId != usrId) {
                                        validContacts.add(userEmail);
                                    }
                                }
                                logger.info("invalid user " + userId);
                                List eachTriggerSchedule = triggerDAO.getTriggerScheduleByTriggerID(triggerUUID, businessUniqueId);
                                int updateResult = triggerDAO.updateContactsInTriggerNotificationsAndTriggerSchedule(eachTriggerSchedule, validContacts, businessUniqueId);
                            }else {
                                //if user is valid --> updated with new filter which matches the trigger condition.
                                //DTV-3633, Remove user from access after triggers set, and then re-add the access filter as well as them to the trigger
                                Map userMap = triggerDAO.getUsersByBusinessUserId(businessUniqueId, userId);
                                String userEmail = Utils.isNotNull(userMap.get("user_email")) ? userMap.get("user_email").toString() : "";
                                if(userEmail.length() > 0) {
                                    List eachTriggerScheduleByDeactivatedUserEmail = triggerDAO.getEachTriggerScheduleByDeactivatedUserEmail(triggerUUID, businessUniqueId, userEmail);
                                    int updateResult = triggerDAO.updateDeactivateContactsInTriggerSchedule(eachTriggerScheduleByDeactivatedUserEmail, userEmail);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception at validateContactsByUserId. Msg {}", e.getMessage());
        }
        return 1;
    }

    /**
     *
     * @param uri
     *
     */
    private  Map callElizaRestApi(String uri){
        Map respMap = new HashMap();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> entity = new HttpEntity<String>(headers);
            ResponseEntity<Object> responseEntity = restTemplate.getForEntity(uri,  Object.class);
            //ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, Object.class);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                }
            }
        }
        catch (ResourceAccessException ex){
            logger.error("Eliza service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  respMap;
    }


    /**
     * Compare operators
     *
     * @param operator1
     * @param operator2
     * @return
     */
    public Boolean compareOperators(String operator1, String operator2) {
        Boolean returnValue = false;
        try {

            if (operator1.equals(">") || operator1.equals(">=")) {
                returnValue = operator2.equals(">") || operator2.equals(">=");
            }
            if (operator1.equals("<") || operator1.equals("<=")) {
                returnValue = operator2.equals("<") || operator2.equals("<=");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }


    public Map getContactEmailsByTokens(String businessUUID, List tokens){
        Map resultMap = new HashMap();
        try{
            List contacts = triggerDAO.getContactsByTokens(businessUUID, tokens);
            if(contacts.size() > 0){
                for(int i=0; i<contacts.size(); i++){
                    Map eachTokenMap = (Map) contacts.get(i);
                    String eachEmail = Utils.isNotNull(eachTokenMap.get("email")) && Utils.notEmptyString(eachTokenMap.get("email").toString()) ? eachTokenMap.get("email").toString() : "";
                    String token = Utils.isNotNull(eachTokenMap.get("dynamic_token_id")) && Utils.notEmptyString(eachTokenMap.get("dynamic_token_id").toString()) ? eachTokenMap.get("dynamic_token_id").toString() : "";
                    if(eachEmail.length() > 0){
                        resultMap.put(token, eachEmail);
                    }else {
                        //check for EMAIL metadata as open question type if email is not present in published list.
                        int eachParticipantId = (int) eachTokenMap.get("participant_id");
                        int eachParticipantGroupId = (int) eachTokenMap.get("participant_group_id");
                        int eachTagId = (int) eachTokenMap.get("tag_id");
                        String participantGroupUUID = triggerDAO.getParticipantGroupUUIdByParticipantGrpID(eachTagId, eachParticipantGroupId, businessUUID);
                        List participantDataList = triggerDAO.getParticipantDataById(eachParticipantId, participantGroupUUID);
                        if(participantDataList.size() > 0){
                            Map participantData = (Map) participantDataList.get(0);
                            List dataList = Utils.isNotNull(participantData.get("data")) ? mapper.readValue(participantData.get("data").toString(), ArrayList.class) : new ArrayList();
                            List metaDataList = Utils.isNotNull(participantData.get("meta_data")) ? mapper.readValue(participantData.get("meta_data").toString(), ArrayList.class) : new ArrayList();
                            if(metaDataList.contains("EMAIL")){
                                int index = metaDataList.lastIndexOf("EMAIL");
                                eachEmail = dataList.get(index).toString();
                                resultMap.put(token, eachEmail);
                            }
                        }
                    }
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  resultMap;
    }

    /**
     * Method to send trigger notification via webhook url
     *
     * @param triggerNotification
     */
    public void sendTriggerWebhook(TriggerNotification triggerNotification) {
        try {
            logger.info("trigger webhook");
            Map resultMap = new HashMap();

            int businessId = triggerNotification.getBusinessId();
            int surveyId = triggerNotification.getSurveyId();
            int notifyId = triggerNotification.getNotifyId();
            String token = triggerNotification.getSubmissionToken();
            String timeZone = triggerNotification.getTimezone();
            String channel = triggerNotification.getChannel();
            int scheduleId = Integer.parseInt(triggerNotification.getScheduleId());
            String notifyTime = Utils.isNotNull(triggerNotification.getStartTime()) ? triggerNotification.getStartTime() : "";

            Map businessMap = triggerDAO.getBusinessDetailsById(businessId);
            String businessUniqueId = businessMap.containsKey("business_uuid") ? (String) businessMap.get("business_uuid") : "";
            String businessName = businessMap.containsKey("business_name") ? (String) businessMap.get("business_name") : "";

            //surveyUUID
            Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
            String surveyUUID = surveyMap.containsKey("survey_uuid") ? surveyMap.get("survey_uuid").toString() : "";
            List<String> surveyNames = surveyMap.containsKey(Constants.SURVEY_NAMES) ? mapper.readValue(surveyMap.get(Constants.SURVEY_NAMES).toString(), ArrayList.class) : new ArrayList();
            String surveyName = surveyNames.size() >0 ?surveyNames.get(0) : "";
            //get schedule webhook_config
            Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
            TriggerWebhookConfigurationData triggerWebhookConfig = scheduleMap.containsKey("webhook_config") && Utils.isNotNull(scheduleMap.get("webhook_config")) ? mapper.readValue(scheduleMap.get("webhook_config").toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData();
            Map integrationsConfigMap = scheduleMap.containsKey("integrations_config") && Utils.isNotNull(scheduleMap.get("integrations_config")) ? mapper.readValue(scheduleMap.get("integrations_config").toString(), HashMap.class) : new HashMap();
            //default protocol method
            HttpMethod protocolMethod = getWebhookProtocol(triggerWebhookConfig.getProtocol());

            //webhook headers
            HttpHeaders headers = getWebhookHeaders(triggerWebhookConfig.getHeaders());

            //Webhook payload informations append with response
            Map webhookDataMap = new HashMap();
            webhookDataMap.put(Constants.BUSINESSID, businessUniqueId);
            webhookDataMap.put(Constants.BUSINESSNAME, businessName);
            webhookDataMap.put(Constants.SURVEYNAME, surveyName);
            webhookDataMap.put(Constants.SURVEYID, surveyUUID);

            //Webhook payload events information
            Map eventResponseMap = this.retrieveRespondentByTokenId(surveyUUID, token, businessUniqueId, timeZone, "en", webhookDataMap, businessId);
            String message = eventResponseMap.containsKey("success") ? eventResponseMap.get("success").toString() : "";
            Map eventResponse = new HashMap();
            if (Utils.notEmptyString(message)) {
                List responseList = (List) eventResponseMap.get("result");
                eventResponse = (Map) responseList.get(0);
            }
            //DTV-6809, Forming metadata object with microsoft dynamics Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_MICROSOFT_DYNAMICS) && integrationsConfigMap.containsKey("metaData")){
//                Map metadata = this.updateMetadataForMicrosoftDynamics(integrationsConfigMap, eventResponse);
                Map metadata = this.getIntegrationMetadata(eventResponse, integrationsConfigMap);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-7087, Forming metadata object with salesforce Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_SALESFORCE) && integrationsConfigMap.containsKey("metaData")){
//                Map metadata = this.updateMetadataForSalesforce(integrationsConfigMap, eventResponse, notifyTime, timeZone);
                Map metadata = this.updateMetadataForSalesforceV2(integrationsConfigMap, eventResponse, notifyTime, timeZone);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-7802, Forming metadata object with bambooHR Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_BAMBOOHR) && integrationsConfigMap.containsKey("metaData")){
                Map metadata = this.updateMetadataForBambooHR(integrationsConfigMap, eventResponse);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-10972, Forming metadata object with slack Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_SLACK) && integrationsConfigMap.containsKey("metaData")){
                Map metadata = this.updateMetadataForSlack(integrationsConfigMap, eventResponse);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-11478, Forming metadata object with freshdesk Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_FRESHDESK) && integrationsConfigMap.containsKey("metaData")){
                Map metadata = this.updateMetadataForFreshdesk(integrationsConfigMap, eventResponse);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-11928, Forming metadata object with zendesk Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_ZENDESK) && integrationsConfigMap.containsKey("metaData")){
                Map metadata = this.updateMetadataForZendesk(integrationsConfigMap, eventResponse, notifyTime, timeZone);
                eventResponse.replace("metaData", metadata);
            }
            //DTV-12605, Forming metadata object with jira Metadata
            if(channel.equalsIgnoreCase(Constants.CHANNEL_JIRA) && integrationsConfigMap.containsKey("metaData")){
                Map metadata = this.updateMetadataForJira(integrationsConfigMap, eventResponse);
                eventResponse.replace("metaData", metadata);
            }
            logger.debug("eventResponses after{}",eventResponse);

            JSONObject json = new JSONObject(eventResponse);
            logger.debug("input json {}", json);
            logger.info("webhook url {}", triggerWebhookConfig.getEndPoint());
            //Trigger notification via webhook url
            resultMap = triggerWebhookNotify(json.toString(), protocolMethod, triggerWebhookConfig.getEndPoint(), headers);
            logger.debug("resultMap {}",resultMap);

            //update status to sent once email sent successfully
            if ((resultMap.containsKey(Constants.STATUS) && Constants.SUCCESS.equals(resultMap.get(Constants.STATUS)))
            || (resultMap.containsKey(Constants.STATUS) && Constants.OK.equals(resultMap.get(Constants.STATUS)))){
                logger.info("update notification");
                //DTV-6955, update sent status to completed in trigger_notification_{buuid} table
                triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                //DTV-6955, Delete the notification in trigger_notification table by notifyId
                triggerDAO.deleteTriggerNotificationById(notifyId);
            }
            else {
                //if fails to send, needs to remove the record from trigger_notification. So that it will trigger again.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to update metadata object for zendesk
     *
     * @param integrationsConfigMap
     * @param eventResponse
     * @return
     */
    private Map updateMetadataForZendesk(Map<String, Map> integrationsConfigMap, Map eventResponse, String notifyTime, String timezone) {
        Map metaData = new HashMap();
        try {
            metaData = this.getIntegrationMetadata(eventResponse, integrationsConfigMap);
            String recipeType = getRecipeName(integrationsConfigMap, Constants.CHANNEL_ZENDESK);
            if (recipeType.equalsIgnoreCase("createTicket")) {
                //form due date for zendesk ticket
                setDateInMetaDataV2(integrationsConfigMap, metaData, notifyTime, timezone, "dueAt", "dueAt");
                //appending key value pairs in metaData object using streams.
                for (Map.Entry<String, Map> entry : integrationsConfigMap.entrySet()) {
                    Map<String, String> eachValueMap = entry.getValue();
                    logger.info("eachValueMap {}", eachValueMap);
                    this.replaceHashTag(eachValueMap, eventResponse, "textContext");
                    this.replaceHashTag(eachValueMap, eventResponse, "ticketSubject");
                }
            }

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            if (integrationMap.containsKey("zendesk"))
                integrationMap.remove("zendesk");
            if (integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");

            metaData = appendKeyValuesToMetadata(integrationMap, metaData);
            logger.info("metaData after {}", metaData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    /**
     * Method to update metadata object for Freshdesk
     * @param integrationsConfigMap
     * @param eventResponse
     * @return
     */
    private Map updateMetadataForFreshdesk(Map<String, Map> integrationsConfigMap, Map eventResponse) {
        Map metaData = new HashMap();
        try {
            metaData = this.getIntegrationMetadata(eventResponse, integrationsConfigMap);
            String recipeType = getRecipeName(integrationsConfigMap, Constants.CHANNEL_FRESHDESK);
            if (recipeType.equalsIgnoreCase("createTicket")) {
                //appending key value pairs in metaData object using streams.
                for (Map.Entry<String, Map> entry : integrationsConfigMap.entrySet()) {
                    Map<String, String> eachValueMap = entry.getValue();
                    logger.info("eachValueMap {}", eachValueMap);
                    this.replaceHashTag(eachValueMap, eventResponse, "message");
                }
            }

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            if (integrationMap.containsKey("freshdesk"))
                integrationMap.remove("freshdesk");
            if (integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");

            metaData = appendKeyValuesToMetadata(integrationMap, metaData);
            logger.info("metaData after {}", metaData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    /**
     * method to get protocol method for webhook
     * @param webhookProtocol
     * @return
     */
    private HttpMethod getWebhookProtocol(String webhookProtocol){
        String protocol = Utils.notEmptyString(webhookProtocol) ? webhookProtocol.toUpperCase() : "";
        HttpMethod protocolMethod = HttpMethod.POST;
        if (protocol.equals("PUT")) {
            protocolMethod = HttpMethod.PUT;
        } else if (protocol.equals("GET")) {
            protocolMethod = HttpMethod.GET;
        }
        return protocolMethod;
    }

    /**
     * get http headers for webhook
     * @param webHookHeaders
     * @return
     */
    private HttpHeaders getWebhookHeaders( Object webHookHeaders){
        HttpHeaders headers = new HttpHeaders();
        if (Utils.isNotNull(webHookHeaders)) {
            Map<String, String> headerMap = (Map<String, String>) webHookHeaders;
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                headers.add(entry.getKey(), entry.getValue());
            }
        } else {
            headers.setContentType(MediaType.APPLICATION_JSON);
        }
        return headers;
    }

    /**
     * Execute notification via webhook url
     *
     * @param inputJson
     * @param method
     * @param uri
     * @param headers
     * @return
     */
    private Map triggerWebhookNotify(String inputJson, HttpMethod method, String uri, HttpHeaders headers) {
        Map responseMap = new HashMap();
        logger.info(inputJson);
        logger.info("headers {}",headers);
        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {
            restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("Webhook Notification request sent successfully");
                    logger.info(responseEntity.getBody().toString());
                    responseMap = (Map) responseEntity.getBody();

                    // send email to cs and engg team in case of error
                    if(Utils.isNull(responseMap) || responseMap.containsKey(Constants.REST_CALL_STATUS) && Constants.FAILURE.equals(responseMap.get(Constants.REST_CALL_STATUS))){
                        sendWebHookErrorMail(uri, new JSONObject(headers).toString(), inputJson, responseEntity.getBody().toString() );
                    }
                }
            } else {
                // send email to cs and engg team in case of error
                if(Utils.isNull(responseMap) || responseMap.containsKey(Constants.REST_CALL_STATUS) && Constants.FAILURE.equals(responseMap.get(Constants.REST_CALL_STATUS))){
                    sendWebHookErrorMail(uri, new JSONObject(headers).toString(), inputJson, responseEntity.getBody().toString() );
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseMap;
    }

    /**
     * Method to retrieve the survey submission token responses in respondentBean format
     *
     * @param surveyUUID
     * @param respondentId
     * @param businessUUID
     * @param timeZone
     * @param requestLanguage
     * @param webhookDataMap
     * @return
     */
    public Map retrieveRespondentByTokenId(String surveyUUID, String respondentId, String businessUUID, String timeZone, String requestLanguage,
                                           Map webhookDataMap, int businessId) {
        Map respondentMap = new HashMap();
        List respondents = new ArrayList();
        Map npsTextMap = new HashMap();
        npsTextMap.put("0", "Detractor");
        npsTextMap.put("1", "Detractor");
        npsTextMap.put("2", "Detractor");
        npsTextMap.put("3", "Detractor");
        npsTextMap.put("4", "Detractor");
        npsTextMap.put("5", "Detractor");
        npsTextMap.put("6", "Detractor");
        npsTextMap.put("7", "Passive");
        npsTextMap.put("8", "Passive");
        npsTextMap.put("9", "Promoter");
        npsTextMap.put("10", "Promoter");

        try {

            Map programMap = triggerDAO.returnProgramByUUID(surveyUUID, businessUUID);
            String questionIdString = programMap.get("question_ids").toString();
            boolean anonymous = (Integer.parseInt(programMap.get("anonymous").toString()) == 0) ? false : true;
            List questionIds = mapper.readValue(new JSONArray(questionIdString).toString(), ArrayList.class);
            List languages = mapper.readValue(programMap.get("languages").toString(), ArrayList.class);
            int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
            List multiQuestions = mapper.readValue(programMap.get("multi_questions").toString(), ArrayList.class);
            List questionDatas = (List) multiQuestions.get(languageIndex);
            List tokenResponses = triggerDAO.getTokensOfSurveyId(surveyUUID);
            List tokens = new ArrayList();
            Iterator iterator = tokenResponses.iterator();
            while (iterator.hasNext()) {
                Map eachRow = (Map) iterator.next();
                tokens.add(eachRow.get("submission_token").toString());
            }

            boolean isHippaEnabled = triggerDAO.isHippaEnableForBusiness(businessId);
            Map responseMap = getResponsesByTokenId(surveyUUID,respondentId, businessUUID, tokens, requestLanguage);

            List responses = (List)responseMap.get("responses");
            List responseTokens = (List)responseMap.get("tokens");
            List responseCreatedTime = (List)responseMap.get("createdTime");
            List responseMetrics = (List) responseMap.get("responseMetrics");
            logger.info("responseMetrics {}", responseMetrics);
            logger.info("responses {}", responses);
            int tokensSize = responses.size();
            for (int i = 0; i < tokensSize; i++) {

                try {
                    String eachRespondentNpsText = "";
                    RespondentBean respondentBean = new RespondentBean();
                    Map metaDataMap = new LinkedHashMap();
                    String eachToken = responseTokens.get(i).toString();
                    String createdDate = responseCreatedTime.get(i).toString();
                    createdDate = Utils.getDateAsStringByTimezone(createdDate, timeZone);
                    List eachTokenEvents = (List) responses.get(i);
                    respondentBean.setId(eachToken);
                    respondentBean.setResponseDate(Utils.getFormattedDate3(createdDate));
                    List dynamicTokens = triggerDAO.getSurveyDataByUserToken(eachToken, businessUUID);

                    if (dynamicTokens.size() > 0) {
                        respondentBean.setTokenType("dynamic");
                        Map eachDynamicToken = (Map) dynamicTokens.get(0);
                        int eachParticipantId = (int) eachDynamicToken.get("participant_id");
                        int eachParticipantGroupId = (int) eachDynamicToken.get("participant_group_id");
                        int eachTagId = (int) eachDynamicToken.get("tag_id");
                        String participantGroupUUID = triggerDAO.getParticipantGroupUUIdByParticipantGrpID(eachTagId, eachParticipantGroupId, businessUUID);

                        if (participantGroupUUID.length() > 0) {
                            List participantDataList = triggerDAO.getParticipantDataById(eachParticipantId, participantGroupUUID);
                            for (int j = 0; j < participantDataList.size(); j++) {
                                String eachHeader = "";
                                String eachValue = "";
                                String eachMetaData = "";
                                String participantName = "";
                                Map bean = (Map) participantDataList.get(j);
                                JSONArray dataArr = new JSONArray(bean.get("data").toString());
                                JSONArray headerArr = new JSONArray(bean.get("header").toString());
                                JSONArray metaDataArr = new JSONArray(bean.get("meta_data").toString());
                                int dataLength = dataArr.length();
                                for (int k = 0; k < dataLength; k++) {
                                    boolean insertMetaData = true;
                                    eachValue = (dataArr.get(k).toString());
                                    eachHeader = (headerArr.get(k).toString());
                                    eachMetaData = (metaDataArr.get(k).toString());
                                    if ((anonymous && eachMetaData.equalsIgnoreCase("NAME")) || (anonymous && eachMetaData.equalsIgnoreCase("EMAIL"))
                                            || (anonymous && eachMetaData.equalsIgnoreCase("PHONE"))) {
                                        insertMetaData = false;
                                    }
                                    if (Utils.notEmptyString(eachHeader) && Utils.notEmptyString(eachValue) && insertMetaData) {
                                        metaDataMap.put(eachHeader.trim(), eachValue.trim());

                                        //Sending a fixed metadata to be used for each participant when survey is not anonymous
                                        //if metadata type NAME contains more than one, taking first non empty occurrence as participantName
                                        if(eachMetaData.equalsIgnoreCase("NAME") && Utils.emptyString(participantName)){
                                            participantName = eachValue.trim();
                                        }
                                    }

                                    if(k==dataLength-1 && insertMetaData){
                                        metaDataMap.put("PRTNAME",participantName);
                                    }
                                }
                            }
                        }
                    }

                    respondentBean.setMetaData(metaDataMap);
                    List eventsList = new ArrayList();
                    int eventsSize = eachTokenEvents.size();

                    Map mappingData = this.getMappingDataForEvents(businessUUID, requestLanguage, surveyUUID, isHippaEnabled);
                    logger.info("mappingData {}",mappingData);
                    Map questionRatingLabelMap = (Map) mappingData.get("questionRatingLabelMap");
                    List eachRatingLableList = new ArrayList();

                    for (int eventIndex = 0; eventIndex < eventsSize; eventIndex++) {

                        try {
                            String metaDataType = (((Map) questionDatas.get(eventIndex)).get("metaDataType") != null && Utils.notEmptyString(((Map) questionDatas.get(eventIndex)).get("metaDataType").toString()))? ((Map) questionDatas.get(eventIndex)).get("metaDataType").toString(): "";
                            if((anonymous) && Utils.notEmptyString(metaDataType) ){continue;}
                            String npsText = "";
                            Integer npsRatingValue = null;
                            RespondentEventBean respondentEventBean = new RespondentEventBean();
                            int eachQuestionId = (int) questionIds.get(eventIndex);
                            logger.info("eachQuestionId {}",eachQuestionId);
                            logger.info("questionRatingLabelMap {}",questionRatingLabelMap);
                            eachRatingLableList = (questionRatingLabelMap.containsKey(eachQuestionId) && Utils.isNotNull(questionRatingLabelMap.get(eachQuestionId))) ? (List) questionRatingLabelMap.get(eachQuestionId) : new ArrayList();
                            respondentEventBean.setQuestionId(((Map) questionDatas.get(eventIndex)).get("questionId").toString());
                            respondentEventBean.setQuestionText(Utils.removeAllHtmlEntities(((Map) questionDatas.get(eventIndex)).get("questionTitle").toString()));
                            String eachQuestionType = ((Map) questionDatas.get(eventIndex)).get("type").toString();
                            Object subTypeObject = (((Map)questionDatas.get(eventIndex)).containsKey("subType")) ? ((Map)questionDatas.get(eventIndex)).get("subType") : "slider";
                            String subType = (Utils.isNotNull(subTypeObject))? subTypeObject.toString() : "slider";
                            respondentEventBean.setCategory(new ArrayList());
                            String eachResponse = "";
                            String eachResponseNps = eachTokenEvents.get(eventIndex).toString();
                            if (eachQuestionType.equals("nps") && Utils.notEmptyString(eachResponseNps)) {

                                npsRatingValue = Integer.parseInt(eachTokenEvents.get(eventIndex).toString());
                                String npsResponse = (npsRatingValue > -1) ? npsRatingValue.toString() : "";
                                Map npsLable = new HashMap();
                                if(eachRatingLableList.size()>0){
                                    npsLable.put(0, eachRatingLableList.get(0));
                                    npsLable.put(10, eachRatingLableList.get(1));
                                }
                                if(npsRatingValue == 10 || npsRatingValue == 0){
                                    if(npsLable.size()>0){
                                        eachResponse = npsLable.get(npsRatingValue).toString();
                                    }else{
                                        eachResponse= npsResponse;
                                    }
                                } else {
                                    eachResponse = npsResponse;
                                }
                                npsText = (npsTextMap.containsKey(eachResponseNps)) ? npsTextMap.get(eachResponseNps).toString() : "";
                                eachRespondentNpsText = (Utils.emptyString(eachRespondentNpsText)) ? npsText : "";
                            } else {
                                eachResponse = eachTokenEvents.get(eventIndex).toString();
                            }
                            if(Utils.isNotNull(npsRatingValue)) { respondentEventBean.setNpsValue(npsRatingValue.toString());} else { respondentEventBean.setNpsValue("N/A"); }
                            respondentEventBean.setAnswers(eachResponse);
                            respondentEventBean.setNpsText(npsText);
                            respondentEventBean.setQuestionType(eachQuestionType);
                            respondentEventBean.setSubType(subType);
                            if((Utils.isNotNull(((Map)responseMetrics.get(eventIndex)).get("metricId"))))
                            {
                                respondentEventBean.setMetricId(((Map) responseMetrics.get(eventIndex)).get("metricId").toString());
                                respondentEventBean.setMetricName(((Map) responseMetrics.get(eventIndex)).get("metricName").toString());
                                respondentEventBean.setMetricScale(((Map) responseMetrics.get(eventIndex)).get("metricScale").toString());
                                respondentEventBean.setMetricScore(((Map) responseMetrics.get(eventIndex)).get("metricRatingValue").toString());
                            }

                            else{
                                respondentEventBean.setMetricId("0");
                                respondentEventBean.setMetricName("");
                                respondentEventBean.setMetricScale("0");
                                respondentEventBean.setMetricScore("0");
                            }

                            eventsList.add(respondentEventBean);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    respondentBean.setNpsText(eachRespondentNpsText);
                    respondentBean.setEvents(eventsList);
                    Map eachRespondentMap = mapper.convertValue(respondentBean, HashMap.class);

                    if (webhookDataMap.size() > 0) {
                        eachRespondentMap.putAll(webhookDataMap);
                    }
                    respondents.add(eachRespondentMap);
                    logger.info("respondents {}",respondents);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            logger.info("respondents {}",respondents);
            respondentMap.put("success", true);
            respondentMap.put("result", respondents);


        } catch (Exception e) {
            e.printStackTrace();
            respondentMap.put("success", false);
            respondentMap.put("message", "internal_server_error");
        }
        return respondentMap;
    }

    /**
     * Method to get response of Respondent by respondent id or token Id
     *
     * @param surveyUUID
     * @param respondentId
     * @param businessUUID
     * @param tokens
     * @param requestLanguage
     * @return
     */
    public Map getResponsesByTokenId(String surveyUUID,String respondentId, String businessUUID, List tokens, String requestLanguage) {
        List responses = new ArrayList();
        List responseTokens = new ArrayList();
        List createdTimes = new ArrayList();
        List responseMetrics = new ArrayList();
        Map responseMap = new HashMap();
        logger.info("begin retrieving responses by token id");
        try {

            Map mappingData = this.getMappingDataWithMetricForEvents(surveyUUID, businessUUID, requestLanguage);
            Map questionIdQuestionTypeMap = (Map) mappingData.get("questionIdQuestionTypeMap");
            Map questionRatingLabelMap = (Map) mappingData.get("questionRatingLabelMap");
            Map questionScaleMap = (Map) mappingData.get("questionScaleMap");
            List metricIds = (List) mappingData.get("metricIds");
            List metricNames = (List) mappingData.get("metricNames");
            List metricScale = (List) mappingData.get("metricScale");
            Map<Integer, Map<String, Object>> matrixQuestionsMap = (Map) mappingData.get("matrixQuestionsMap");
            Map<Integer, Map<String, Object>> multiTextQuestionsMap = (Map) mappingData.get("multiTextQuestionsMap");
            Map optionsMap = (Map) mappingData.get("options");
            Map optionIdsMap = (Map) mappingData.get("optionIds");

            if (tokens.size() > 0) {
                logger.info("Retrieving all the survey reports");
                Map eachFeedback = triggerDAO.getEventBySubmissionToken(surveyUUID, respondentId);

                List eachResponse = new ArrayList();
                String eachToken = eachFeedback.get("submission_token").toString();
                logger.info("eachToken{}", eachToken);
                String data = eachFeedback.get("data").toString();
                String eventData = eachFeedback.get("event_data").toString();
                String eachFdBkcreatedTime = eachFeedback.get("created_time").toString();
                responseTokens.add(eachToken);
                createdTimes.add(eachFdBkcreatedTime);
                JSONArray dataArray = new JSONArray(data);
                JSONArray eventDataArray = new JSONArray(eventData);

                int size = dataArray.length();
                for (int i = 0; i < size; i++) {
                    Map eachResponseMetric = new HashMap();
                    int eachQuestionId = (int) eventDataArray.get(i);
                    int eachQuestionType = (questionIdQuestionTypeMap.containsKey(eachQuestionId) && Utils.isNotNull(questionIdQuestionTypeMap.get(eachQuestionId))) ? (int) questionIdQuestionTypeMap.get(eachQuestionId) : 2;
                    List eachRatingLableList = (questionRatingLabelMap.containsKey(eachQuestionId) && Utils.isNotNull(questionRatingLabelMap.get(eachQuestionId))) ? (List) questionRatingLabelMap.get(eachQuestionId) : new ArrayList();
                    int questionScale = (questionScaleMap.containsKey(eachQuestionId) && Utils.isNotNull(questionScaleMap.get(eachQuestionId))) ? (int) questionScaleMap.get(eachQuestionId) : 0;

                    switch (eachQuestionType) {
                        case 1:
                            eachRatingLableList = reverseList(eachRatingLableList);
                            int ratingValue = (int) dataArray.get(i);
                            if (Utils.isNotNull(metricIds)) {
                                eachResponseMetric.put("metricId", metricIds.get(i));
                                eachResponseMetric.put("metricName", metricNames.get(i));
                                eachResponseMetric.put("metricScale", metricScale.get(i));
                                eachResponseMetric.put("metricRatingValue", ratingValue);
                            }
                            String ratingResponse = (ratingValue > -1) ? new Integer(ratingValue + 1).toString() : "";
                            Map ratingLable = new HashMap();
                            //DTV-10051, for ratingslider questions options labels will be empty.
                            String ratingLableStr = "";
                            if(!eachRatingLableList.isEmpty()) {
                                ratingLable.put(0, eachRatingLableList.get(0));
                                ratingLable.put(questionScale - 1, eachRatingLableList.get(1));
                                if (ratingValue > -1) {
                                    ratingLableStr = questionScale < 6 ? eachRatingLableList.get(ratingValue).toString()
                                            : (questionScale > 5 && (ratingValue == 0 || ratingValue == questionScale - 1)) ? ratingLable.get(ratingValue).toString() : ratingResponse;
                                }
                            }else
                                ratingLableStr = ratingResponse;
                            eachResponse.add(ratingLableStr);
                            break;

                        case 2:
                        case 14: //file
                            eachResponse.add(dataArray.get(i).toString());
                            break;

                        case 3: // single choice
                        case 4: // multi choice
                        case 8: // drop down
                        case 13: // picture choice
                        case 15:// poll
                            String multiChoiceString = dataArray.get(i).toString();
                            String multiResponse = getMultiChoiceAnswers(businessUUID, eachQuestionId, surveyUUID, multiChoiceString, requestLanguage, eachQuestionType);
                            eachResponse.add(multiResponse);
                            break;

                        case 5:
                            Integer npsRatingValue = (int) dataArray.get(i);
                            String npsResponse = (npsRatingValue > -1) ? npsRatingValue.toString() : "";
                            eachResponse.add(npsResponse);
                            break;

                        case 7:
                            String rankingOptionString = dataArray.get(i).toString();
                            String rankingResponse = getRankingAnswers(businessUUID, eachQuestionId, surveyUUID, rankingOptionString);
                            eachResponse.add(rankingResponse);
                            break;

                        case 9:
                        case 10:
                            //logger.info("matrix rating / choice");
                            String matrixChoiceString = dataArray.get(i).toString();
                            Map<String, Object> matrixQn = matrixQuestionsMap.get(eachQuestionId) != null ?
                                    (Map) matrixQuestionsMap.get(eachQuestionId) : new HashMap<>();

                            optionsMap.put(eachQuestionId, !matrixQn.isEmpty() ? matrixQn.get("subQuestionTitles") : new ArrayList<>());
                            optionIdsMap.put(eachQuestionId, !matrixQn.isEmpty() ? matrixQn.get("optionsForMatrix") : new ArrayList<>());
                            logger.info("Matrix - optionsMap{}", optionsMap);
                            logger.info("Matrix - optionIdsMap{}", optionIdsMap);
                            String matrixAnswer = getMatrixChoiceAnswers(matrixChoiceString, optionsMap, optionIdsMap, eachQuestionId);
                            eachResponse.add(matrixAnswer);
                            break;

                        case 11://multiple open ended
                            String multiTextAnswers = dataArray.get(i).toString();
                            Map<String, Object> multiTextQn = multiTextQuestionsMap.get(eachQuestionId) != null ?
                                    (Map) multiTextQuestionsMap.get(eachQuestionId) : new HashMap<>();
                            optionsMap.put(eachQuestionId, !multiTextQn.isEmpty() ? multiTextQn.get("subQuestionTitles") : new ArrayList<>());
                            logger.info("MOE - optionsMap{}", optionsMap);
                            String finalAnswer = getMultipleTextAnswers(multiTextAnswers, optionsMap, eachQuestionId);
                            eachResponse.add(finalAnswer);
                            break;

                    }
                    responseMetrics.add(eachResponseMetric);
                }
                responses.add(eachResponse);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end receiving responses by token id");
        responseMap.put("responses", responses);
        responseMap.put("tokens", responseTokens);
        responseMap.put("createdTime", createdTimes);
        responseMap.put("responseMetrics", responseMetrics);
        return responseMap;
    }

    /**
     * method to get ranking label for option Ids
     * @param businessUUID
     * @param eachQuestionId
     * @param surveyUUID
     * @param rankingOptionString
     * @return
     */
    public String getRankingAnswers(String businessUUID, int eachQuestionId, String surveyUUID, String rankingOptionString){
        String rankingResponse = "";
        try {
            List rankingOptionsList = mapper.readValue(rankingOptionString, ArrayList.class);
            Map rankingData = this.getMultiRankingOptionsAndItsLabelsByQuestionId(businessUUID, eachQuestionId, surveyUUID);
            List rankingOptions = (List) rankingData.get("options");
            List rankingLabels = (List) rankingData.get("labels");

            if (rankingOptionsList.size() > 0) {
                Iterator itr = rankingOptionsList.iterator();
                while (itr.hasNext()) {
                    int eachChoiceId = (int) itr.next();
                    if (eachChoiceId > 0) {
                        int eachIdx = rankingOptions.indexOf(eachChoiceId);
                        String eachRankResponse = rankingLabels.get(eachIdx).toString();
                        rankingResponse += eachRankResponse + ", ";
                    }
                }
                rankingResponse = (Utils.isNotNull(rankingResponse) && (rankingResponse.length() >= 2)) ? rankingResponse.substring(0, rankingResponse.length() - 2) : "";
            }
        }catch(Exception e){
            logger.info("exception at getRankingQuestionLabels");
        }
        return rankingResponse;
    }

    /**
     * method to get multi choice response answers
     * @param businessUUID
     * @param eachQuestionId
     * @param surveyUUID
     * @param multiChoiceString
     * @param requestLanguage
     * @return
     */
    public String getMultiChoiceAnswers(String businessUUID, int eachQuestionId, String surveyUUID, String multiChoiceString,
                                        String requestLanguage, int eachQuestionType) {
        String multiResponse = "";
        try {

            Map multiChoiceData = (eachQuestionType == 9) ? this.getDropDownLabelsByQuestionId(businessUUID, surveyUUID, eachQuestionId, requestLanguage)
             : this.getMultiChoiceIdsAndLabelsByQuestionId(businessUUID, surveyUUID, eachQuestionId, requestLanguage);

            List multiOptions = (List) multiChoiceData.get("options");
            List multiLabels = (List) multiChoiceData.get("labels");
            List OtherLabels = (List) multiChoiceData.get("otherLabels");
            if (OtherLabels.size() > 0) {
                multiLabels.addAll(OtherLabels);
            }

            List multiChoiceList = mapper.readValue(multiChoiceString, ArrayList.class);
            if (multiChoiceList.size() > 0) {
                Iterator multiResponseIterator = multiChoiceList.iterator();
                while (multiResponseIterator.hasNext()) {
                    int eachChoiceId = (int) multiResponseIterator.next();
                    if (eachChoiceId > 0) {
                        int multiChoiceIndex = multiOptions.indexOf(eachChoiceId);
                        String eachMultiResponse = multiLabels.get(multiChoiceIndex).toString();
                        if (OtherLabels.contains(eachMultiResponse)) {
                            multiResponse = multiResponse + "Other: " + eachMultiResponse + ", ";
                        } else {
                            multiResponse = multiResponse + eachMultiResponse + ", ";
                        }
                    }
                }
                multiResponse = (Utils.isNotNull(multiResponse) && (multiResponse.length() >= 2)) ?
                        multiResponse.substring(0, multiResponse.length() - 2) : "";
            }
        } catch (Exception e) {
            logger.info("exception at getMultiChoiceAnswers {}", e.getMessage());
        }
        return multiResponse;
    }

    /**
     * method to get matrix choice response answers
     * @param matrixChoiceString
     * @param optionsMap
     * @param optionIdsMap
     * @param eachQuestionId
     * @return
     */
    public String getMatrixChoiceAnswers(String matrixChoiceString, Map optionsMap, Map optionIdsMap, int eachQuestionId) {
        String multiResponse = "";
        try {

            List matrixChoiceList = mapper.readValue(matrixChoiceString, ArrayList.class);
            List options = (List) optionsMap.get(eachQuestionId);
            List optionIds = (List) optionIdsMap.get(eachQuestionId);
            StringBuilder strBuilder = new StringBuilder();

            /*for(int idx = 0; idx< options.size(); idx++){
                String eachRowValue = (String) options.get(idx);
                if(matrixChoiceList.get(idx) instanceof Integer) {
                    int matrixIdx = (int) matrixChoiceList.get(idx);
                    strBuilder.append(eachRowValue).append(": ").append(matrixIdx > -1 ? optionIds.get(matrixIdx) : "Not Answered");
                }else{
                    List matrixIdx = (List) matrixChoiceList.get(idx);
                }
                strBuilder.append("</n>");
            }*/
            for(int idx = 0; idx< options.size(); idx++) {
                String eachRowValue = (String) options.get(idx);
                if(matrixChoiceList.get(idx) instanceof Integer) {
                    int matrixIdx = Integer.parseInt(matrixChoiceList.get(idx).toString());
                    strBuilder.append(eachRowValue).append(": ").append(matrixIdx > -1 ? optionIds.get(matrixIdx) : "Not Answered");
                }else {
                    List answerList = (List) matrixChoiceList.get(idx);
                    Iterator answerIterator = answerList.iterator();
                    while (answerIterator.hasNext()) {
                        String eachAnswer = answerIterator.next().toString();
                        if (Utils.notEmptyString(eachAnswer) && Integer.parseInt(eachAnswer) > 0) {
                            eachAnswer = options.get(Integer.parseInt(eachAnswer))
                                    .toString();
                        } else {
                            eachAnswer = "Not answered";
                        }
                        strBuilder.append(eachRowValue).append(": ").append(eachAnswer);
                        if (!answerIterator.hasNext())
                            strBuilder.append(",");
                    }
                }
            }
            multiResponse = strBuilder.toString();
        } catch (Exception e) {
            logger.info("exception at multiResponse.Msg {}", e.getMessage());
        }
        return multiResponse;
    }

    /**
     * method to get multiple text box response answers
     * @param multipleTextString
     * @param optionsMap
     * @param eachQuestionId
     * @return
     */
    public String getMultipleTextAnswers(String multipleTextString, Map optionsMap, int eachQuestionId) {
        StringBuilder strBuilder = new StringBuilder();
        try {
            List matrixChoiceList = mapper.readValue(multipleTextString, ArrayList.class);
            List options = (List) optionsMap.get(eachQuestionId);

            for(int idx = 0; idx< options.size(); idx++){
                String eachRowValue = (String) options.get(idx);
                String eachMultiTextAnswer = (String) matrixChoiceList.get(idx);
                strBuilder.append(eachRowValue).append(": ").append( Utils.notEmptyString(eachMultiTextAnswer)  ? eachMultiTextAnswer : "Not Answered");
                strBuilder.append("</n>");
            }
        } catch (Exception e) {
            logger.info("exception at getMultipleTextAnswers. Msg {}",e.getMessage());
        }
        return strBuilder.toString();
    }

    /**
     * Method to get question and metric details for event
     *
     * @param surveyUUID
     * @param businessUUID
     * @param requestLanuage
     * @return
     */
    public Map getMappingDataWithMetricForEvents(String surveyUUID, String businessUUID, String requestLanuage) {
        Map surveyQuestionMap = new HashMap();
        try {

            List questions = new ArrayList();
            Map questionIdQuestionTypeMap = new HashMap();
            Map questionRatingLabelMap = new HashMap();
            Map questionScaleMap = new HashMap();
            Map optionsMap = new HashMap();
            Map optionIdsMap = new HashMap();
            Map<Integer, Map<String, Object>> matrixQuestionsMap = new HashMap();
            Map<Integer, Map<String, Object>> multiTextQuestionsMap = new HashMap();

            List metricIds = new ArrayList();
            List metricNames = new ArrayList();
            List metricScale = new ArrayList();

            List surveyQuestions = this.returnSurveyQuestions(surveyUUID, businessUUID, requestLanuage);
            Iterator<Map> questionsIterator = surveyQuestions.iterator();

            while (questionsIterator.hasNext()){
                Map eachQuestion = questionsIterator.next();

                int eachQuestionId = (int)eachQuestion.get("questionId");
                questions.add(eachQuestionId);

                int eachQuestionType = (int)eachQuestion.get("type");
                List labels = (List)eachQuestion.get("labels");
                questionRatingLabelMap.put(eachQuestionId,labels);

                int eachQuestionScale = (int)eachQuestion.get("scale");
                questionScaleMap.put(eachQuestionId, eachQuestionScale);

                int eachQuestionMetricId = (int)eachQuestion.get("metric");
                metricIds.add(eachQuestionMetricId);

                String eachQuestionMetricName = (String) eachQuestion.get("metricText");
                metricNames.add(eachQuestionMetricName);

                int eachQuestionMetricScale = (int)eachQuestion.get("scale");
                metricScale.add(eachQuestionMetricScale);

                optionsMap.put(eachQuestionId, (List)eachQuestion.get("options"));
                optionIdsMap.put(eachQuestionId, (List)eachQuestion.get("optionIds"));

                questionIdQuestionTypeMap.put(eachQuestionId,eachQuestionType);

                if (eachQuestionType == 9 || eachQuestionType == 10) {
                    try {
                        Map matrixMap = new HashMap();
                        List matrixQuestionIds = (List) eachQuestion.get("questionIds");
                        List categories = (List) eachQuestion.get("categories");
                        List subQuestionTitles = (List) eachQuestion.get("questionTitles");
                        List optionsForMatrix = (List) eachQuestion.get("labels");
                        String scale = String.valueOf(eachQuestion.get("scale"));
                        matrixMap.put("matrixQuestionIds", matrixQuestionIds);
                        matrixMap.put("categories", categories);
                        matrixMap.put("optionsForMatrix", optionsForMatrix);
                        matrixMap.put("subQuestionTitles", subQuestionTitles);
                        matrixMap.put("scale", scale);
                        matrixMap.put("questionId", eachQuestionId);
                        matrixQuestionsMap.put(eachQuestionId, matrixMap);
                        logger.info("matrixQuestionsMap " + matrixQuestionsMap);
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                }

                if (eachQuestionType == 11) { // Multiple open ended
                    try {
                        Map<String, Object> multipleTextMap = new HashMap();
                        List subQuestionIds = (List) eachQuestion.get("questionIds");
                        List subQuestionTitles = (List) eachQuestion.get("questionTitles");
                        multipleTextMap.put("subQuestionIds", subQuestionIds);
                        multipleTextMap.put("subQuestionTitles", subQuestionTitles);
                        multipleTextMap.put("questionId", eachQuestionId);
                        multipleTextMap.put("questionType", eachQuestion.get("questionTitle"));
                        multipleTextMap.put("scale", eachQuestion.get("scale"));

                        multiTextQuestionsMap.put(eachQuestionId, multipleTextMap);
                        logger.info("multiTextQuestionsMap " + multiTextQuestionsMap);
                    } catch (Exception e) {
                        logger.error("Error while format event response for multiple open ended : " + e.getMessage());
                    }
                }

            }
            surveyQuestionMap.put("questions",questions);
            surveyQuestionMap.put("questionIdQuestionTypeMap",questionIdQuestionTypeMap);
            surveyQuestionMap.put("questionRatingLabelMap",questionRatingLabelMap);
            surveyQuestionMap.put("questionScaleMap",questionScaleMap);
            surveyQuestionMap.put("metricIds",metricIds);
            surveyQuestionMap.put("metricNames",metricNames);
            surveyQuestionMap.put("metricScale",metricScale);
            surveyQuestionMap.put("options", optionsMap);
            surveyQuestionMap.put("optionIds", optionIdsMap);
            surveyQuestionMap.put("matrixQuestionsMap",matrixQuestionsMap);
            surveyQuestionMap.put("multiTextQuestionsMap", multiTextQuestionsMap);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return surveyQuestionMap;
    }

    /**
     * Method to retrieves the questions for json surveys
     *
     * @param surveyUUID
     * @param businessUUID
     * @return
     */
    public List returnSurveyQuestions(String surveyUUID, String businessUUID, String requestLanguage) {
        List questionsList = new ArrayList();
        try {

            String companyCode = "DT";
            logger.info("begin retrieving the survey questions");

            Map programMap = triggerDAO.returnProgramByUUID(surveyUUID, businessUUID);

            String businessUUIDHyphenReplaced = businessUUID.replace("-","_");

            String languagesString = programMap.get("languages").toString();
            List languages = mapper.readValue(languagesString, ArrayList.class);
            int languageIndex = (languages.indexOf(requestLanguage)>-1) ? languages.indexOf(requestLanguage) : 0;

            String multiQuestionString = programMap.get("multi_questions").toString();
            List multiQuestions = mapper.readValue(multiQuestionString, ArrayList.class);
            List questionData = (List)multiQuestions.get(languageIndex);

            String metricsMapString = programMap.get("metrics_map").toString();
            Map metricsMap = mapper.readValue(metricsMapString, HashMap.class);
            Map questionMetricsMap = this.createQuestionIdMetricMap(metricsMap);

            Iterator questionDataIterator = questionData.iterator();
            int j = 0;
            while(questionDataIterator.hasNext()){
                j = j + 1;
                Map eachQuestionData = (Map)questionDataIterator.next();
                String eachQuestionUUID = eachQuestionData.get("questionId").toString();
                Map eachRetrievedQuestionMap = triggerDAO.getQuestionDataByQuestionUUIDByBusinessId(eachQuestionUUID,businessUUID);
                int eachQuestionId = (int)eachRetrievedQuestionMap.get("question_id");
                String questionCreatedTime = eachRetrievedQuestionMap.get("created_time").toString();
                String questionModifiedTime = (Utils.isNotNull(eachRetrievedQuestionMap.get("modified_time"))) ? eachRetrievedQuestionMap.get("modified_time").toString() : "";
                int questionCreatedBy = (int)eachRetrievedQuestionMap.get("created_by");
                int questionModifiedBy = (Utils.isNotNull(eachRetrievedQuestionMap.get("modified_by"))) ?(int)eachRetrievedQuestionMap.get("modified_by") : 0;
                /**
                 * slider - 1
                 * smiley  - 2
                 */
                int questionRatingType = 0;
                int eachQuestionMetricId = (questionMetricsMap.containsKey(eachQuestionId) && Utils.isNotNull(questionMetricsMap.get(eachQuestionId))) ? (int)questionMetricsMap.get(eachQuestionId) : 0;
                String eachQuestionMetricName = "";
                if(eachQuestionMetricId>0){
                    Map keyMetricMap = triggerDAO.retrieveJsonKeyMetricById(eachQuestionMetricId, businessUUIDHyphenReplaced);
                    eachQuestionMetricName = (keyMetricMap.containsKey("key_metric_text") && Utils.isNotNull(keyMetricMap.get("key_metric_text"))) ? keyMetricMap.get("key_metric_text").toString() : "";
                }

                List labels = new ArrayList();
                List options = new ArrayList();
                List optionIds = new ArrayList();
                List categories = new ArrayList();
                List questionTitles = new ArrayList();
                List questionIds = new ArrayList();

                String questionType = eachQuestionData.get("type").toString();
                int type = 0;
                int scale = 0;
                int mandatoryValue = ((Utils.isNotNull(eachQuestionData.get("mandatory"))) && (boolean)eachQuestionData.get("mandatory")) ? 1 :0 ;


                switch(questionType){

                    case "open":
                        type = 2;
                        break;

                    case "ratingSlider":
                    case "rating":
                        type = 1;
                        labels = (List)eachQuestionData.get("options");
                        scale = (eachQuestionData.containsKey("scale") && Utils.isNotNull(eachQuestionData.get("scale"))) ? Integer.parseInt(eachQuestionData.get("scale").toString()) : labels.size();
                        String ratingType = eachQuestionData.get("subType").toString();
                        questionRatingType = (ratingType.equals("slider")) ? 1 : 2;
                        break;

                    case "singleChoice":
                    case Constants.POLL:
                        type = Constants.questionTypes.get(questionType);
                        List singleChoiceList = triggerDAO.getMultiChoiceDataForQuestionId(eachQuestionId, businessUUID);
                        if(singleChoiceList.size()>0){
                            Map eachSingleChoiceMap = (Map)singleChoiceList.get(0);

                            String optionIdsString = eachSingleChoiceMap.get("option_ids").toString();
                            String optionDataString = eachSingleChoiceMap.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }

                        break;

                    case "multiChoice":
                        type = 4;
                        List multiChoiceList = triggerDAO.getMultiChoiceDataForQuestionId(eachQuestionId, businessUUID);
                        if(multiChoiceList.size()>0){
                            Map eachSingleChoiceMap = (Map)multiChoiceList.get(0);

                            String optionIdsString = eachSingleChoiceMap.get("option_ids").toString();
                            String optionDataString = eachSingleChoiceMap.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;

                    case Constants.RANKING:
                        type = 7;
                        Map rankingData = triggerDAO.getRankingOptionsByQuestionId(businessUUID, eachQuestionId);
                        if (rankingData.containsKey("option_ids")) {
                            String optionIdsString = rankingData.get("option_ids").toString();
                            String optionDataString = rankingData.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;

                    case "nps":
                        type = 5;
                        labels = (List)eachQuestionData.get("options");
                        scale = 11;
                        questionRatingType = 1;
                        break;

                    case "statement":
                        type = 6;
                        break;

                    case Constants.DROP_DOWN:
                        type = 8;
                        Map eachDropdownMap  = triggerDAO.getDropDownOptionsByQuestionId(businessUUID, eachQuestionId);
                        if(eachDropdownMap .size() > 0){
                            String optionIdsString = eachDropdownMap.get("option_ids").toString();
                            String optionDataString = eachDropdownMap.get("option_data").toString();
                            optionIds = mapper.readValue(optionIdsString, List.class);
                            options = mapper.readValue(optionDataString, List.class);
                        }
                        break;

                    case Constants.MATRIX_RATING:
                        type = 9;
                        List<List<String>> optionsForMatrix = (List) eachQuestionData.get("optionsForMatrix");
                        categories = (List) eachQuestionData.get("categories");
                        questionTitles = (List) eachQuestionData.get("questionTitles");
                        questionIds = (List) eachQuestionData.get("questionIds");
                        labels = (List) optionsForMatrix.get(0);
                        scale = (eachQuestionData.containsKey("scale") && Utils.isNotNull(eachQuestionData.get("scale"))) ? Integer.parseInt(eachQuestionData.get("scale").toString()) : labels.size();
                        String matrixRatingType = Utils.isNotNull(eachQuestionData.get("subType")) ? eachQuestionData.get("subType").toString() : "rating";
                        questionRatingType = getRatingSubTypeInteger(matrixRatingType); //(ratingType.equals("slider")) ? 1 : 2;
                        break;

                    case Constants.MATRIX_CHOICE:
                        type = 10;
                        List<List<String>> optionsForMatrixChoice = (List) eachQuestionData.get("optionsForMatrix");
                        categories = (List) eachQuestionData.get("categories");
                        questionTitles = (List) eachQuestionData.get("questionTitles");
                        questionIds = (List) eachQuestionData.get("questionIds");
                        labels = (List) optionsForMatrixChoice.get(0);
                        scale = (eachQuestionData.containsKey("scale") && Utils.isNotNull(eachQuestionData.get("scale"))) ? Integer.parseInt(eachQuestionData.get("scale").toString()) : labels.size();
                        break;

                    case Constants.MULTI_OPENENDED:
                        type = 11;
                        questionTitles = (List) eachQuestionData.get("questionTitles");
                        questionIds = (List) eachQuestionData.get("questionIds");
                        scale = (eachQuestionData.containsKey("scale") && Utils.isNotNull(eachQuestionData.get("scale"))) ?
                                Integer.parseInt(eachQuestionData.get("scale").toString()) : 64;
                        break;

                }

                Map eachQuestionMap = new TreeMap();
                eachQuestionMap.put("questionId",eachQuestionId);
                eachQuestionMap.put("questionUUID",eachQuestionUUID);

                eachQuestionMap.put("name",eachQuestionData.get("questionTitle"));
                eachQuestionMap.put("createdTime",questionCreatedTime);
                eachQuestionMap.put("modifiedTime",questionModifiedTime);
                eachQuestionMap.put("createdBy",questionCreatedBy);
                eachQuestionMap.put("modifiedBy",questionModifiedBy);
                eachQuestionMap.put("description","");
                eachQuestionMap.put("driver","");
                eachQuestionMap.put("driverText","");
                eachQuestionMap.put("metric",eachQuestionMetricId);
                eachQuestionMap.put("metricText",eachQuestionMetricName);
                eachQuestionMap.put("labelId","");
                eachQuestionMap.put("followUpOpen",0);
                eachQuestionMap.put("optionIds",optionIds);
                eachQuestionMap.put("options",options);
                eachQuestionMap.put("labels",labels);
                eachQuestionMap.put("scale",scale);
                eachQuestionMap.put("type",type);
                eachQuestionMap.put("mandatory",mandatoryValue);
                eachQuestionMap.put("cc",companyCode);
                eachQuestionMap.put("order",j);
                eachQuestionMap.put("ratingType",questionRatingType);
                eachQuestionMap.put("categories",categories);
                eachQuestionMap.put("questionTitles",questionTitles);
                eachQuestionMap.put("questionIds",questionIds);
                questionsList.add(eachQuestionMap);

            }
            logger.info("end retrieving the survey questions");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionsList;
    }


    /**
     * method to get rating sub type in numeric form for given string
     * @param subType
     * @return
     */
    private int getRatingSubTypeInteger(String subType){
        switch (subType.toLowerCase()) {
            case "slider":
                return 1;
            case "smiley":
                return 2;
            case "star":
                return 3;
            case "heart":
                return 4;
            case "thumb":
                return 5;
            default:
                return 1;
        }
    }

    /**
     * flatten the map to get question id and its metric mapping
     * @param metricsMap
     * @return
     */
    public Map createQuestionIdMetricMap(Map metricsMap){
        Map questionMetricsMap = new HashMap();
        try{
            Set keySet = metricsMap.keySet();
            Iterator keysIterator = keySet.iterator();
            while(keysIterator.hasNext()){
                String eachMetric = keysIterator.next().toString();
                List eachMetricQuestions = (List)metricsMap.get(eachMetric);
                Iterator questionIterator = eachMetricQuestions.iterator();
                while (questionIterator.hasNext()){
                    int eachQuestionId = (int)questionIterator.next();
                    questionMetricsMap.put(eachQuestionId, Integer.parseInt(eachMetric));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return questionMetricsMap;
    }

    /**
     * method to send error email
     *
     * @param uri
     * @param headers
     * @param respBody
     */
    private void sendWebHookErrorMail(String uri, String headers, String reqeust, String respBody) {

        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<html><ul>");
        stringBuffer.append("<li>Client notification Url:");
        stringBuffer.append(uri);
        stringBuffer.append("</li>");
        stringBuffer.append("<li>Client Request headers:");
        stringBuffer.append(headers);
        stringBuffer.append("</li>");
        stringBuffer.append("<li>Request:");
        stringBuffer.append(reqeust);
        stringBuffer.append("</li>");
        stringBuffer.append("<li>Error Response:");
        stringBuffer.append(respBody);
        stringBuffer.append("</li>>");
        stringBuffer.append("</ul</html>");

        List ccEmailList = new ArrayList();
        ccEmailList.add("venkateswari.nr@bahwancybertek.com");
        ccEmailList.add("arone@dropthought.com");

        //call email utility api
        Map emailMap = new HashMap();
        emailMap.put("from", fromEmail);
        emailMap.put("to", "cs@dropthought.com");
        emailMap.put("subject", "Email Notification Alert: "+ uri);
        emailMap.put("body", stringBuffer.toString());
        emailMap.put("cc", ccEmailList);
        JSONObject json = new JSONObject(emailMap);
        String inputJson = json.toString();
//        emailNotificationUrl = "https://utility-test.dropthought.com/api/v1/notification/email";
        Map resp = callRestApi(inputJson, HttpMethod.POST, Constants.CHANNEL_WEBHOOK, emailNotificationUrl);
    }

    /** method to perfrom contains string in list
     * @param strToCompare
     * @param list
     * @return
     **/
    public boolean containsCaseInsensitive(String strToCompare, List<String>list) {
        for (String str : list) {
            if (str.equalsIgnoreCase(strToCompare)) {
                return (true);
            }
        }
        return (false);
    }



    /**
     * New Function to get filter sub query
     * @param query
     * @return
     */
    public String getFilterSubQueryNewAdvFilters(String query) {
        String prefix = "";
        String suffix = "";
        String operator = "";
        String participantGrpUUID = "";
        String subQuery = "";
        String metaDataField = "";
        double suffixIntType;
        try {

            List<String> strings = this.getFilterQueryPrefixSuffixAdvFilters(query);
            if(strings.size() > 0) {
                prefix = strings.get(0);
                suffix = strings.get(1);
                operator = strings.get(2);
                String[] prefixArr = prefix.split("\\.");
                if(prefixArr.length>2){
                    String participantGroupUUID = prefixArr[2];
                    if(!query.contains("dtlink")||!query.contains("dtkiosk")||!query.contains("dtqr")) {
                        metaDataField = prefix.substring(74, prefix.length());
                    }
                    else {
                        metaDataField = prefix.split("\\.")[3];
                    }
                    if(suffix.equalsIgnoreCase("DT_ALL"))
                    {
                        if(operator.equals("="))
                        {
                            operator = "!=";
                        }
                        else
                        {
                            operator = "=";
                        }
                        suffix = "undefined";
                    }
                    suffix = Utils.validateSurveyFilterDates(suffix);
                    logger.info("The value in prefix = "+prefix);
                    logger.info("The value in suffic = "+suffix);
                    logger.info("The value in operator = "+operator);
                    logger.info("the value in metadatafield = "+metaDataField);
                    boolean isDate = Utils.checkValidDate(suffix,suffix);
                    //The below lines of code added as even if the value is a number it is treated as a string when assigned to suffix.
                    //So to match integer the below pattern is used and the json_unquote is added for integer type values in suffix.
                    //number pattern updated to support negative and positive decimal numbers DTV-3491
                    String numberPattern = "^-?[0-9]\\d*(\\.\\d+)?$";
                    Pattern pattern1 = Pattern.compile(numberPattern);
                    Matcher matcher1 = pattern1.matcher(suffix);
                    String metaDataType = triggerDAO.getMetaDataTypeForHeader(participantGroupUUID, metaDataField);
                    //DTV-4697 adding not equal to empty string query check to avoid empty values being consider for number and date type filters
                    String notEmptyStringQuery = " and json_unquote(json_extract(data, json_unquote(json_search(upper(header), 'one', upper('" + metaDataField + "'))))) != \"\"";

                    if(metaDataType.toLowerCase().contains("json")) {
                        if (metaDataType.equalsIgnoreCase("json_number")) {
                            //json_contains(json_extract(data,json_unquote(JSON_SEARCH(header, 'one', "Technicians"))),  '"Technician A"') = 1;
                            subQuery = (" JSON_CONTAINS(JSON_EXTRACT(data,json_unquote(json_search(upper(header), 'one', upper('\" + metaDataField + \"'))), '\"" + suffix + "\"') =1 ");
                        } else {
                            //json_contains(json_extract(data,json_unquote(JSON_SEARCH(header, 'one', "Technicians ID"))),  '100') = 1;
                            subQuery = (" JSON_CONTAINS(JSON_EXTRACT(data,json_unquote(json_search(upper(header), 'one', upper('\" + metaDataField + \"'))), '" + suffix + "')=1");
                        }
                    }
                    else if(matcher1.matches())
                    {
                        suffixIntType = Double.parseDouble(suffix);
                        logger.info("The value in suffixIntType = "+suffixIntType);
                        subQuery = "json_unquote(json_extract(data, json_unquote(json_search(upper(header), 'one', upper('" + metaDataField + "'))))) " + operator + "" + suffixIntType + notEmptyStringQuery;
                    }
                    else if(isDate)
                    {
                        subQuery = "json_extract(data, json_unquote(json_search(upper(header), 'one', upper('" + metaDataField + "')))) " + operator + "\"" + suffix + "\"" + notEmptyStringQuery;
                    }
                    else
                    {
                        subQuery = "json_extract(data, json_unquote(json_search(upper(header), 'one', upper(\"" + metaDataField + "\")))) " + operator + "'" + suffix + "'";
                    }
                }
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }
        return subQuery;
    }

    public Map getOpenSubQuery(String query) {
        Map subQueryMap = new HashMap();
        try {
            List<String> strings = this.getQueryPrefixSuffix(query);
            String criteriaField;
            if (strings.size() > 0) {
                String prefix = strings.get(0); // condition Type
                String conditionVal = "";
                if(strings.size()!=2)
                {
                    conditionVal = strings.get(1); // condition value
                }
                else {
                    conditionVal = "all";//when case is any no value is sent from ui so string.get(1) will be operator as suffix is not there.
                }
                String[] prefixArr = prefix.split("\\.");
                if (prefixArr.length > 2) {
                    if (query.contains("category")) {
                        subQueryMap.put("categoryName", conditionVal);
                    }
                    else
                    {
                        subQueryMap.put("textSentiment", conditionVal);
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return subQueryMap;
    }

    /**
     * New Function to get prefix and suffix for filter query
     * @param query
     * @return
     */
    public List getFilterQueryPrefixSuffixAdvFilters(String query){
        List optionList = new ArrayList();
        logger.info("The value innnn query = "+query);
        List tempList = new ArrayList();
        List tempOprList = new ArrayList();
        String operator = "";
        String[] stringLst = new String[2];
        boolean bool = false;
        if (query.contains(">=")) {
            logger.info("greater than or equal to");
            tempList.add(query.indexOf(">=",0));
            tempOprList.add(">=");
        } if (query.contains("<=")) {
            logger.info("less than or equal to");
            tempList.add(query.indexOf("<=",0));
            tempOprList.add("<=");
            bool = true;
        } if (query.contains(">")) {
            logger.info("greater than");
            tempList.add(query.indexOf(">",0));
            tempOprList.add(">");
        } if (query.contains("<")) {
            logger.info("less than");
            tempList.add(query.indexOf("<",0));
            tempOprList.add("<");
        } if (query.contains("!=")) {
            logger.info("not equal to");
            tempList.add(query.indexOf("!=",0));
            tempOprList.add("<>");
            bool = true;
        } if (query.contains("=")) {
            logger.info("equal to");
            tempList.add(query.indexOf("=",0));
            tempOprList.add("=");
        }
        logger.info("The value in tempList = "+tempList);
        logger.info("The value in tempoprList = "+tempOprList);
        sort2Lists(tempList,tempOprList);
        logger.info("The value in tempList after sort = "+tempList);
        logger.info("The value in tempOprList after sort = "+tempOprList);
        int oprLength = tempOprList.get(0).toString().length();
        int temp = Integer.parseInt(tempList.get(0).toString());
        stringLst[0] = query.substring(0,temp);
        stringLst[1] = query.substring((temp+oprLength),query.length());
        operator = tempOprList.get(0).toString();

        //When empty values are sent to the filter query
        //Ex. filteruuid.participantgroupuuid.fieldName.operator instead of filteruuid.participantgroupuuid.fieldName.operator.value
        if(stringLst.length<2){
            String[] newStringArray = new String[2];
            newStringArray[0] = stringLst[0];
            newStringArray[1] = "";
            stringLst = newStringArray;
        }

        Collections.addAll(optionList,stringLst);
        optionList.add(operator);
        logger.info("prefix suffix {}", optionList.toString());
        return optionList;
    }


    /**
     * function to send usage limit mail
     *
     * @param businessId
     * @return
     */
    public void sendUsageLimitMail(int businessId, String source, int usageCount) {
        Map usageMailFlagMap = new HashMap();
        logger.info("Start  get all active triggers  by business id");
        try {
            Map result = new HashMap();
            try {

                // uri = https://${domain.url}/api/v1/portal/usage/mail/client/{clientId}?source={source}&usageCount={usageCount}
                final String uri =  portalUsageUrl+"mail/client/"+businessId+"?source="+source+"&usageCount="+usageCount;
                logger.info("uri {}" ,uri);
                Map resultMap = callRestApi(uri);

            } catch (Exception e) {
                logger.error(e.getMessage());
                //e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //logger.info("End get all active triggers  by business id");
    }


    /**
     * call rest api
     * @param urlStr
     * @return
     */
    public Map callRestApi(String urlStr){
        Map respMap = new HashMap();
        // rest api header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        // rest template instance

        try {
            // rest api call for insights url
            ResponseEntity<Object> responseEntity = restTemplate.exchange(urlStr, HttpMethod.GET, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info("portal  response received successfully");
                    respMap = (Map) responseEntity.getBody();
                }
            }
        }   catch (ResourceAccessException | HttpClientErrorException ex){
            logger.error("portal service refused to connect");
            respMap.put("success",false);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            logger.error("Exception at callRestApi. Msg {} ", e.getMessage());
            respMap.put("success",false);
            respMap.put("message", "Exception occured");
        }
        return respMap;
    }

    /**
     * Function to check the triggers usage limit
     *
     * @param businessUUID
     * @retun
     */
    public Map checkTriggersUsageLimit(String businessUUID, int userId) {
        Map result = new HashMap();
        try {
            boolean isLimitReached = false;

            int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);
            Map usageMap  = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsList = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);
            if(configurationsList.size() > 0) {
                Map limitsMap = (Map) configurationsList.get(0);
                int limit = (Utils.isNotNull(limitsMap.get("noOfTriggers")) && Utils.notEmptyString(limitsMap.get("noOfTriggers").toString())) ? Integer.parseInt(limitsMap.get("noOfTriggers").toString()) : 0;
                int usage = (Utils.isNotNull(usageMap.get("triggersUsageCount")) && Utils.notEmptyString(usageMap.get("triggersUsageCount").toString())) ? Integer.parseInt(usageMap.get("triggersUsageCount").toString()) : 0;

                //checking limit after including current new trigger count (usage + 1)
                isLimitReached = (limit >= 0 && (usage + 1 > limit));

                if (isLimitReached) {
                    Map userMap = triggerDAO.getUsersByBusinessUserId(businessUUID, userId);
                    int userRole = (Utils.isNotNull(userMap.get("user_role")) && Utils.notEmptyString(userMap.get("user_role").toString())) ? Integer.parseInt(userMap.get("user_role").toString()) : 0;
                    int roleId = (Utils.isNotNull(userMap.get("account_role_id")) && Utils.notEmptyString(userMap.get("account_role_id").toString())) ? Integer.parseInt(userMap.get("account_role_id").toString()) : 0;

                    Map kingUserMap = triggerDAO.getKingUserByBusinessId(businessId);
                    String kingUserEmail = Utils.isNotNull(kingUserMap.get("user_email")) && Utils.notEmptyString(kingUserMap.get("user_email").toString()) ? kingUserMap.get("user_email").toString() : "";
                    logger.info("kingUserEmail   " + kingUserEmail);

                    String error = "";
                    //If new trigger is created by king or admin user
                    if (userRole == 1 || roleId ==1 || roleId == 4) {
                        error = String.format(Constants.TRIGGER_USAGE_KINGUSER_ERROR, limit);
                    } else {
                        error = String.format(Constants.TRIGGER_USAGE_REGULARUSER_ERROR, limit, kingUserEmail);
                    }
                    result.put("success", false);
                    result.put("message", error);
                    result.put("usage", usage);
                    result.put("limit", limit);
                    result.put("kingUserEmail", kingUserEmail);
                    result.put("limitReached", true);
                    logger.info("result    " + result);

                } else {
                    result.put("success", true);
                    result.put("limitReached", false);
                }
            }

        } catch (Exception e) {
            logger.error("Exception at checkTriggersUsageLimit. Msg {}", e.getMessage());
            //if any exception occured, it should not prevent trigger creation

            result.put("success", true);
            result.put("limitReached", false);
        }
        return result;
    }


    /**
     * Method to manage feedback notifications from AlertsService and sending to FCM
     *
     */
    public void updateTriggerPushNotifications(List notificationsList, String businessUUID) {
        logger.info("notificationsParameters  "+notificationsList);
        logger.info("begin adding notifications to users");
        String companyCode = "DT";
        List messageList = new ArrayList();
        List userIds = new ArrayList();
        int surveyId = 0;
        int businessId = 0;
        String respondentName = "";
        String surveyName = "";
        String respondentId = "";
        String message = "";
        String surveyUUID = "";
        String triggerName;
        String responseDate;
        if(notificationsList.size()>0) {
            try {
                Iterator iterator = notificationsList.iterator();
                while(iterator.hasNext()) {
                    Map eachNotificationParameterMap = (Map) iterator.next();
                    respondentId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.RESPONDENT_ID)) ? eachNotificationParameterMap.get(Constants.RESPONDENT_ID).toString() : "" ;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    String createdTime = simpleDateFormat.format(timestamp);

                    //immediate or delay case
                    if(Utils.notEmptyString(respondentId)) {

                        surveyId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEYUUID)) ? triggerDAO.getSurveyIdByUUID(eachNotificationParameterMap.get(Constants.SURVEYUUID).toString(), businessUUID) : 0;
                        respondentName = Utils.isNotNull(eachNotificationParameterMap.get(Constants.RESPONDENT_NAME)) ? eachNotificationParameterMap.get(Constants.RESPONDENT_NAME).toString() : "";
                        surveyName = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEY_NAME)) ? eachNotificationParameterMap.get(Constants.SURVEY_NAME).toString() : "";
                        respondentId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.RESPONDENT_ID)) ? eachNotificationParameterMap.get(Constants.RESPONDENT_ID).toString() : "";
                        surveyUUID = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEYUUID)) ? eachNotificationParameterMap.get(Constants.SURVEYUUID).toString() : "";
                        businessId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.BUSINESS_ID)) ? Integer.parseInt(eachNotificationParameterMap.get(Constants.BUSINESS_ID).toString()) : 0;
                        message = Utils.isNotNull(eachNotificationParameterMap.get(Constants.PUSHMESSAGE)) ? eachNotificationParameterMap.get(Constants.PUSHMESSAGE).toString() : "";
                        triggerName = Utils.isNotNull(eachNotificationParameterMap.get(Constants.TRIGGER_NAME)) ? eachNotificationParameterMap.get(Constants.TRIGGER_NAME).toString() : "";
                        responseDate = Utils.isNotNull(eachNotificationParameterMap.get(Constants.CREATED_TIME)) ? eachNotificationParameterMap.get(Constants.CREATED_TIME).toString() : "";
                        List emailsList = Utils.isNotNull(eachNotificationParameterMap.get(Constants.EMAILS)) ? (List) (eachNotificationParameterMap.get(Constants.EMAILS)) : new ArrayList();

                        userIds = triggerDAO.getUserIdsByEmailIds(emailsList, businessId);
                        Map eventMap = this.getResponsesByTokenId(surveyUUID, respondentId, businessUUID, "en", false);
                        List questions = Utils.isNotNull(eventMap.get("pushNotificationQuestions")) ? (List) (eventMap.get("pushNotificationQuestions")) : new ArrayList();
                        logger.info("userIds  "+userIds);
                        logger.info("questions  "+questions);
                        for(int i=0; i < userIds.size(); i++){
                            Map eachMessageMap = new HashMap();
                            eachMessageMap.put("message", message);
                            eachMessageMap.put("questions", questions);
                            messageList.add(eachMessageMap);
                        }
                        int result = triggerDAO.createNotificationData(businessUUID, new JSONArray(messageList).toString(), surveyId, new JSONArray(userIds).toString(), respondentName, surveyName, createdTime, respondentId);
                        int resultTask = triggerDAO.createTask(businessUUID, respondentId, triggerName, responseDate);
                    }else {
                        //For aggregate case
                        List respondentNames = new ArrayList();
                        surveyId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEYUUID)) ? triggerDAO.getSurveyIdByUUID(eachNotificationParameterMap.get(Constants.SURVEYUUID).toString(), businessUUID) : 0;
                        surveyName = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEY_NAME)) ? eachNotificationParameterMap.get(Constants.SURVEY_NAME).toString() : "";
                        surveyUUID = Utils.isNotNull(eachNotificationParameterMap.get(Constants.SURVEYUUID)) ? eachNotificationParameterMap.get(Constants.SURVEYUUID).toString() : "";
                        businessId = Utils.isNotNull(eachNotificationParameterMap.get(Constants.BUSINESS_ID)) ? Integer.parseInt(eachNotificationParameterMap.get(Constants.BUSINESS_ID).toString()) : 0;
                        triggerName = Utils.isNotNull(eachNotificationParameterMap.get(Constants.TRIGGER_NAME)) ? eachNotificationParameterMap.get(Constants.TRIGGER_NAME).toString() : "";
                        message = Utils.isNotNull(eachNotificationParameterMap.get(Constants.PUSHMESSAGE)) ? eachNotificationParameterMap.get(Constants.PUSHMESSAGE).toString() : "";
                        responseDate = Utils.isNotNull(eachNotificationParameterMap.get(Constants.CREATED_TIME)) ? eachNotificationParameterMap.get(Constants.CREATED_TIME).toString() : "";
                        List emailsList = Utils.isNotNull(eachNotificationParameterMap.get(Constants.EMAILS)) ? (List) (eachNotificationParameterMap.get(Constants.EMAILS)) : new ArrayList();
                        List tokensList = Utils.isNotNull(eachNotificationParameterMap.get(Constants.TOKENS)) ? (List) (eachNotificationParameterMap.get(Constants.TOKENS)) : new ArrayList();


                        List respondentIds = triggerDAO.checkNotificationTokensExistsForThePushUpdate(businessUUID, surveyId, tokensList);
                        logger.info("respondentIds  "+respondentIds);
                        if(respondentIds.size() > 0) {
                            List messages = new ArrayList();
                            userIds = triggerDAO.getUserIdsByEmailIds(emailsList, businessId);

                            //Fetching responses for each respondentIds.
                            Iterator iterator1 = respondentIds.iterator();
                            while (iterator1.hasNext()) {
                                String eachRespondentId = iterator1.next().toString();
                                Map eventMap = this.getResponsesByTokenId(surveyUUID, eachRespondentId, businessUUID, "en", false);
                                List questions = Utils.isNotNull(eventMap.get("pushNotificationQuestions")) ? (List) (eventMap.get("pushNotificationQuestions")) : new ArrayList();
                                logger.info("userIds  " + userIds);
                                logger.info("questions  " + questions);
                                for (int i = 0; i < userIds.size(); i++) {
                                    Map eachMessageMap = new HashMap();
                                    eachMessageMap.put("message", message);
                                    eachMessageMap.put("questions", questions);
                                    messageList.add(eachMessageMap);
                                    messages.add(new JSONArray(messageList).toString());
                                }
                            }

                            //to find respondentNames for each respondentId
                            List contacts = triggerDAO.getContactsByTokens(businessUUID, respondentIds);
                            for (int i = 0; i < respondentIds.size(); i++) {
                                if(i < contacts.size()) {
                                    Map eachTokenMap = (Map) contacts.get(i);
                                    String eachName = Utils.isNotNull(eachTokenMap.get("name")) && Utils.notEmptyString(eachTokenMap.get("name").toString()) ? eachTokenMap.get("name").toString() : "";
                                    respondentNames.add(eachName);
                                }else {
                                    respondentNames.add("");
                                }
                            }
                            logger.info("respondentNames  "+respondentNames);

                            int result = triggerDAO.createNotificationDataForAggregateCase(businessUUID, messages, surveyId, new JSONArray(userIds).toString(), respondentNames, surveyName, createdTime, respondentIds);
                            int resultTask = triggerDAO.createTask(businessUUID, respondentId, triggerName, responseDate);
                        }
                    }
                }

            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        logger.info("end adding notifications to users");
    }

    /**
     * Return result for email usage(send to) limit when trigger schedule
     *
     * @param triggerBean
     * @return
     */
    public Map checkEmailUsageLimit(TriggerSchedule triggerBean) {
        Map result = new HashMap();
        try {
            boolean isLimitReached = false;

            String businessUUID = triggerBean.getBusinessId();
            int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);
            Map usageMap  = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsList = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);

            Map triggerMap = triggerDAO.getTriggerMapByUUId(businessUUID, triggerBean.getTriggerId());

            if(configurationsList.size() > 0) {
                Map limitsMap = (Map) configurationsList.get(0);
                int limit = (Utils.isNotNull(limitsMap.get("noOfEmailsSent")) && Utils.notEmptyString(limitsMap.get("noOfEmailsSent").toString())) ? Integer.parseInt(limitsMap.get("noOfEmailsSent").toString()) : 0;
                int usage = (Utils.isNotNull(usageMap.get("emailsUsageCount")) && Utils.notEmptyString(usageMap.get("emailsUsageCount").toString())) ? Integer.parseInt(usageMap.get("emailsUsageCount").toString()) : 0;
                Integer sendToContactsSize = triggerBean.getContacts().size();
                //checking limit after including current send to email count (usage + sendToContactsSize)
                isLimitReached = (limit >= 0 && (usage + sendToContactsSize > limit));

                if (isLimitReached) {
                    int userId = triggerMap.containsKey(Constants.CREATED_BY) ? Integer.parseInt(triggerMap.get(Constants.CREATED_BY).toString()) : 0;
                    Map userMap = triggerDAO.getUsersByBusinessUserId(businessUUID, userId);
                    int userRole = (Utils.isNotNull(userMap.get("user_role")) && Utils.notEmptyString(userMap.get("user_role").toString())) ? Integer.parseInt(userMap.get("user_role").toString()) : 0;
                    int roleId = (Utils.isNotNull(userMap.get("account_role_id")) && Utils.notEmptyString(userMap.get("account_role_id").toString())) ? Integer.parseInt(userMap.get("account_role_id").toString()) : 0;

                    Map kingUserMap = triggerDAO.getKingUserByBusinessId(businessId);
                    String kingUserEmail = Utils.isNotNull(kingUserMap.get("user_email")) && Utils.notEmptyString(kingUserMap.get("user_email").toString()) ? kingUserMap.get("user_email").toString() : "";
                    logger.info("kingUserEmail   " + kingUserEmail);

                    String error = "";
                    //If new schedule is created/updated by king or admin user
                    if (userRole == 1 || roleId ==1) {
                        error = String.format(Constants.EMAIL_USAGE_KINGUSER_ERROR, limit);
                    } else {
                        error = String.format(Constants.EMAIL_USAGE_REGULARUSER_ERROR, limit, kingUserEmail);
                    }
                    result.put("success", false);
                    result.put("message", error);
                    result.put("usage", usage);
                    result.put("limit", limit);
                    result.put("kingUserEmail", kingUserEmail);
                    result.put("limitReached", true);
                    logger.info("result    " + result);

                } else {
                    result.put("success", true);
                    result.put("limitReached", false);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at checkTrigger Schedule email UsageLimit. Msg {}", e.getMessage());
            //if any exception occured, it should not prevent trigger schedule

            result.put("success", true);
            result.put("limitReached", false);
        }
        return result;
    }

    /**
     * Return emaillist by checking email usage limit to prevent sending mails
     *
     * @param businessId
     * @param notifyEmailList
     * @return
     */
    public List validateNotifyEmailList(int businessId, List notifyEmailList) {
        List validatedNotifyList = new ArrayList();
        try {
            Map usageMap  = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsList = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);
            if(configurationsList.size() > 0) {
                Map limitsMap = (Map) configurationsList.get(0);
                int limit = (Utils.isNotNull(limitsMap.get("noOfEmailsSent")) && Utils.notEmptyString(limitsMap.get("noOfEmailsSent").toString())) ? Integer.parseInt(limitsMap.get("noOfEmailsSent").toString()) : 0;
                int usage = (Utils.isNotNull(usageMap.get("emailsUsageCount")) && Utils.notEmptyString(usageMap.get("emailsUsageCount").toString())) ? Integer.parseInt(usageMap.get("emailsUsageCount").toString()) : 0;
                if (limit == -1) {
                    //limit set -1 means infinite emails can be send
                    validatedNotifyList = notifyEmailList;
                    logger.info("minus limit {}",validatedNotifyList);

                } else if (limit > 0) {
                    int allowedEmailSize = limit - usage;
                    logger.info("allowedEmailSize ++{}", allowedEmailSize);
                    int notifyEmailListSize = notifyEmailList.size();
                    logger.info("notifyEmailListSize ++{}", notifyEmailListSize);
                    if (allowedEmailSize < notifyEmailListSize) {
                        if (allowedEmailSize > 0) {
                            validatedNotifyList = notifyEmailList.subList(0, allowedEmailSize);
                        }
                    }
                    else {
                        validatedNotifyList = notifyEmailList;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return validatedNotifyList;
    }

    /**
     * Return result for lists usage(send to) limit when trigger schedule
     *
     * @param triggerBean
     * @return
     */
    public Map checkListsUsageLimit(TriggerSchedule triggerBean) {
        Map result = new HashMap();
        try {
            boolean isLimitReached = false;

            String businessUUID = triggerBean.getBusinessId();
            int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);
            Map usageMap = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsList = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);

            Map triggerMap = triggerDAO.getTriggerMapByUUId(businessUUID, triggerBean.getTriggerId());

            if (configurationsList.size() > 0) {
                Map limitsMap = (Map) configurationsList.get(0);
                int limit = (Utils.isNotNull(limitsMap.get("noOfLists")) && Utils.notEmptyString(limitsMap.get("noOfLists").toString())) ? Integer.parseInt(limitsMap.get("noOfLists").toString()) : 0;
                int usage = (Utils.isNotNull(usageMap.get("listsUsageCount")) && Utils.notEmptyString(usageMap.get("listsUsageCount").toString())) ? Integer.parseInt(usageMap.get("listsUsageCount").toString()) : 0;

                //checking limit after including current creating list.
                isLimitReached = (limit >= 0 && (usage + 1 > limit));

                if (isLimitReached) {
                    int userId = triggerMap.containsKey(Constants.CREATED_BY) ? Integer.parseInt(triggerMap.get(Constants.CREATED_BY).toString()) : 0;
                    Map userMap = triggerDAO.getUsersByBusinessUserId(businessUUID, userId);
                    int userRole = (Utils.isNotNull(userMap.get("user_role")) && Utils.notEmptyString(userMap.get("user_role").toString())) ? Integer.parseInt(userMap.get("user_role").toString()) : 0;
                    int roleId = (Utils.isNotNull(userMap.get("account_role_id")) && Utils.notEmptyString(userMap.get("account_role_id").toString())) ? Integer.parseInt(userMap.get("account_role_id").toString()) : 0;

                    Map kingUserMap = triggerDAO.getKingUserByBusinessId(businessId);
                    String kingUserEmail = Utils.isNotNull(kingUserMap.get("user_email")) && Utils.notEmptyString(kingUserMap.get("user_email").toString()) ? kingUserMap.get("user_email").toString() : "";
                    logger.info("kingUserEmail   " + kingUserEmail);

                    String error = "";
                    //If new schedule is created/updated by king or admin user
                    if (userRole == 1 || roleId == 1) {
                        error = String.format(Constants.LIST_USAGE_KINGUSER_ERROR, limit);
                    } else {
                        error = String.format(Constants.LIST_USAGE_REGULARUSER_ERROR, limit, kingUserEmail);
                    }
                    result.put("success", false);
                    result.put("message", error);
                    result.put("usage", usage);
                    result.put("limit", limit);
                    result.put("kingUserEmail", kingUserEmail);
                    result.put("limitReached", true);
                    logger.info("result    " + result);

                } else {
                    result.put("success", true);
                    result.put("limitReached", false);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at checkListsUsageLimit. Msg {}", e.getMessage());
            //if any exception occured, it should not prevent trigger schedule

            result.put("success", true);
            result.put("limitReached", false);
        }
        return result;
    }

    /**
     * method to get sent status is processing for lastporcessingdate is greater than 15 mins and update them to null
     */
    public void updateTriggerStatusToNull() {
        try{
            Map map = triggerDAO.getTriggerWithProcessingStatus();
            if(map.containsKey("id") && Utils.isNotNull(map.get("id"))){
                List ids = mapper.readValue(map.get("id").toString(), ArrayList.class);
                if(ids.size() > 0){
                    triggerDAO.updateScheduleSentStatusToNull(ids);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("exception at updateTriggerStatus. Msg {}", e.getMessage());
        }
    }


    /**
     * method to process triggerCondition and get the questionids with category sentiment key value pair structure
     *
     * @param triggerCondition
     * @return
     */
    public Map formTARequstFromTriggerCondition(String triggerCondition) {
        Map resultMap = new HashMap();
        List queries = new ArrayList();
        String QUESTION = "question";
        String CATEGORY = "category";
        String SENTIMENT = "sentiment";

        if (triggerCondition.contains(QUESTION)) {
            // split the filter condition using and / or operator
            String[] subQueries = triggerCondition.split("&&|\\|\\|");
            // loop through sub queries of filter condition strings
            for (int i = 0; i < subQueries.length; i++) {
                // add each query into queries list
                queries.add(subQueries[i]);
                // replace empty value to each index query String in trigger condition, this will replace all the string values and only operator will be there.
                triggerCondition = triggerCondition.replaceFirst(Pattern.quote(subQueries[i]), "");//Since replaceFirst's 1st parameter is assumed to be a regex, you need to escape special characters. you can use Pattern.quote(string) to escape it.
            }
            Iterator<String> queryIterator = queries.iterator();
            // loop through query iterator
            while (queryIterator.hasNext()) {
                try {
                    // assign each query
                    String eachQuery = queryIterator.next(); // eg: question.78005afc-9907-462b-8160-2d8d348eb06a.049b471b-f7a5-46ca-bea6-451d7e05d34b.sentiment=Negative
                    if (eachQuery.contains(CATEGORY) || eachQuery.contains(SENTIMENT)) {
                        String eachQuestionId = eachQuery.substring(46, 82); //eg: 049b471b-f7a5-46ca-bea6-451d7e05d34b

                        String analyticsType = eachQuery.substring(83);
                        String[] typeArr = analyticsType.split("=");
                        String type = typeArr[0]; // category or sentiment

                        if(type.contains("+")){//for multiple open ended
                            eachQuestionId = type.substring(0, 36);
                            type = type.substring(37, type.length());//to handle for multiple open question category/sentiment type
                        }

                        String typeVal = typeArr.length > 1 ? typeArr[1] : "all"; // category values or sentiment values
                        if(typeVal.equalsIgnoreCase("Any sentiment")){
                            typeVal = "all";
                        }

                        if(typeVal.equalsIgnoreCase("Any category")){
                            typeVal = "all";
                        }

                        if (resultMap.containsKey(eachQuestionId)) {
                            Map existingMap = (Map) resultMap.get(eachQuestionId);
                            if (existingMap.containsKey(type)) {
                                List list = (List) existingMap.get(type);
                                list.add(typeVal);
                            } else {
                                List lst = new ArrayList();
                                lst.add(typeVal);
                                existingMap.put(type, lst);
                            }
                        } else {
                            List lst = new ArrayList();
                            lst.add(typeVal);
                            Map categoryMap = new HashMap();
                            categoryMap.put(type, lst);
                            resultMap.put(eachQuestionId, categoryMap);
                        }
                    }

                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
        return resultMap;
    }


    /**
     * DTV-4013, DTV-3870 creating selected pre defined triggers from template
     * Method to update template used count
     * @param templateUUID
     * @param programUUID
     * @param bean
     */
    public int createTriggersByTemplateProgramId(String templateUUID, String programUUID, TemplateTriggerBean bean){
        try {
            List triggerTemplateList = Utils.isNotNull(bean.getTemplateTrigger()) ? (List) bean.getTemplateTrigger() : new ArrayList();
            String businessUUID = Utils.isNotNull(bean.getBusinessUUID()) ?  bean.getBusinessUUID(): "";
            String userUUID  = Utils.isNotNull(bean.getUserUUID()) ?  bean.getUserUUID(): "";
            String timezone = Utils.isNotNull(bean.getTimezone()) ?  bean.getTimezone(): Constants.TIMEZONE_AMERICA_LOSANGELES;;
            String surveyName = "";

            if(triggerTemplateList.size() > 0 && businessUUID.length() > 0 && userUUID.length() > 0) {
                Map templateMap = triggerTemplateDAO.returnProgramTemplateByUUID(templateUUID, businessUUID, "dropthought");
                if (templateMap.size() > 0) {
                    int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);
                    Map templateSurveyQuestionUUIDMap = new HashMap();
                    Map programMap = triggerDAO.returnProgramByUUID(programUUID, businessUUID);

                    int programId = programMap.containsKey("id") && Utils.isNotNull(programMap.get("id")) ? Integer.parseInt(programMap.get("id").toString()) : 0;
                    String fromDate = Utils.isNotNull(programMap.get("from_date")) ? programMap.get("from_date").toString() : "";
                    String toDate = Utils.isNotNull(programMap.get("to_date")) ? programMap.get("to_date").toString() : "";

                    String surveyMetaDataString = programMap.get("survey_meta_data").toString();
                    JSONObject surveyMetaDataJson = new JSONObject(surveyMetaDataString);
                    surveyName = surveyMetaDataJson.get("surveyName").toString();

                    int templateId = templateMap.containsKey("id") && Utils.isNotNull(templateMap.get("id")) ? Integer.parseInt(templateMap.get("id").toString()) : 0;
                    List templateQuestionData = mapper.readValue(templateMap.get("question_data").toString(), ArrayList.class);
                    List surveyQuestionData = mapper.readValue(programMap.get("question_data").toString(), ArrayList.class);

                    Map userMap = triggerDAO.getUsersByBusinessUserId(businessUUID, triggerDAO.getUserIdByUUID(userUUID));
                    String userEmail = userMap.containsKey("user_email") && Utils.isNotNull(userMap.get("user_email")) ? userMap.get("user_email").toString() : "";

                    /**Mapping template question ids with program question ids
                     key --> templateQuestionUUID, value --> programQuestionUUID
                     **/
                    for(int i=0; i<templateQuestionData.size(); i++){
                        Map eachTemplateQuestionData = (Map) templateQuestionData.get(i);
                        Map eachProgramQuestionData = i < surveyQuestionData.size() ? (Map) surveyQuestionData.get(i) : new HashMap();
                        String eachTemplateQuestionUUID = eachTemplateQuestionData.containsKey("questionId") ? eachTemplateQuestionData.get("questionId").toString() : "";
                        String eachProgramQuestionUUID = eachProgramQuestionData.containsKey("questionId") ? eachProgramQuestionData.get("questionId").toString() : "";
                        if(Utils.notEmptyString(eachProgramQuestionUUID) && Utils.notEmptyString(eachTemplateQuestionUUID)){
                            templateSurveyQuestionUUIDMap.put(eachTemplateQuestionUUID, eachProgramQuestionUUID);
                        }
                    }
                    logger.info("templateSurveyQuestionUUIDMap " + templateSurveyQuestionUUIDMap);

                    Iterator triggerTemplateIterator = triggerTemplateList.iterator();
                    //iterating through each template trigger selected during program creation from template
                    while(triggerTemplateIterator.hasNext()) {
                        Map eachTemplateTriggerMap = (Map) triggerTemplateIterator.next();
                        String eachTemplateTriggerId = eachTemplateTriggerMap.containsKey("triggerId") && Utils.isNotNull(eachTemplateTriggerMap.get("triggerId")) ? eachTemplateTriggerMap.get("triggerId").toString() : "";
                        logger.info("eachTemplateTriggerId  " + eachTemplateTriggerId);
                        List eachTemplateTriggerScheduleIds = eachTemplateTriggerMap.containsKey("scheduleId") ? (List) eachTemplateTriggerMap.get("scheduleId") : new ArrayList();
                        String webhookUrl = eachTemplateTriggerMap.containsKey("webhookUrl") && Utils.isNotNull(eachTemplateTriggerMap.get("webhookUrl")) ? eachTemplateTriggerMap.get("webhookUrl").toString() : "";

                        Map templateTriggerMap = triggerTemplateDAO.getTriggerTemplateMapByUUId(eachTemplateTriggerId);
                        int templateTriggerId = templateTriggerMap.containsKey("id") && Utils.isNotNull(templateTriggerMap.get("id")) ? Integer.parseInt(templateTriggerMap.get("id").toString()) : 0;
                        String templateTriggerName = templateTriggerMap.containsKey("trigger_name") && Utils.isNotNull(templateTriggerMap.get("trigger_name")) ? templateTriggerMap.get("trigger_name").toString() : "";

                        String templateTriggerCondition = templateTriggerMap.containsKey("trigger_condition") && Utils.isNotNull(templateTriggerMap.get("trigger_condition")) ? templateTriggerMap.get("trigger_condition").toString() : "";
                        String modifiedTriggerCondition = this.convertTemplateToTriggerCondition(templateUUID, programUUID, templateTriggerCondition, templateSurveyQuestionUUIDMap);
                        /**
                         * creating trigger in trigger_business with modified Triggercondition from templateTriggerCondition
                         */

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        String currentDateString = simpleDateFormat.format(timestamp);

                        String templateTriggerNameTimeStampAdded = templateTriggerName+"_"+currentDateString;
                        String createdTriggerUUID = "";
                        int createdTriggerId = 0;
                        Trigger trigger = new Trigger();
                        trigger.setSurveyName(surveyName);
                        trigger.setSurveyId(programUUID);
                        trigger.setTriggerCondition(Utils.emptyString(modifiedTriggerCondition) ? templateTriggerCondition : modifiedTriggerCondition);
                        trigger.setTriggerName(templateTriggerNameTimeStampAdded);
                        trigger.setBusinessUniqueId(businessUUID);
                        trigger.setUserId(userUUID);
                        trigger.setTimezone(timezone);
                        trigger.setSurveyEndDate(toDate);
                        trigger.setSurveyStartDate(fromDate);
                        trigger.setTriggerStartDate(fromDate.substring(0, 10));
                        trigger.setTriggerEndDate(toDate.substring(0, 10));
                        trigger.setTriggerStartTime(fromDate.substring(11, 19));
                        trigger.setTriggerEndTime(toDate.substring(11, 19));

                        logger.info("creating trigger for template trigger Id  "+ templateTriggerId);
                        Map triggerCreationMap = this.createTrigger(trigger);
                        if (triggerCreationMap.size() > 0 && (Boolean) triggerCreationMap.get(Constants.SUCCESS)) {
                            createdTriggerUUID = triggerCreationMap.get(Constants.RESULT).toString();
                            Map createdTriggerMap = triggerDAO.getTriggerById(businessUUID, createdTriggerUUID);
                            if (createdTriggerMap.containsKey("trigger_uuid")) {
                                createdTriggerId = Integer.parseInt(createdTriggerMap.get("id").toString());
                            }
                        }
                        logger.info("created triggerId for template trigger Id  "+ createdTriggerId);


                        /**
                         * creating schedules for newly created trigger in trigger_schedule after creating trigger associated with templateTrigger
                         * eachTemplateTriggerScheduleIds --> contains scheduleUUIDs for each templateTrigger
                         * "triggerId": "07273869-7eaf-4e45-803d-38ea4742463f",
                         * "scheduleId": ["d5f1871e-d457-4025-8462-41aae64ecd3f", "437f9a7e-6c1b-42af-bd85-dd5e51d7a669"]
                         *
                         */

                        Map scheduleTriggerProgramMap = new HashMap();
                        List templateScheduleIds = new ArrayList();
                        List triggerScheduleIds = new ArrayList();
                        List channels = new ArrayList();
                        List triggerMetadata = new ArrayList();

                        try {
                            if(eachTemplateTriggerScheduleIds.size() > 0){
                                Iterator eachTemplateTriggerScheduleIdIterator = eachTemplateTriggerScheduleIds.iterator();
                                //iterating through each template trigger selected during program creation from template
                                while (eachTemplateTriggerScheduleIdIterator.hasNext()) {
                                    TriggerSchedule triggerSchedule = new TriggerSchedule();
                                    String eachTemplateTriggerScheduleId = eachTemplateTriggerScheduleIdIterator.next().toString();
                                    String createdScheduleUUID = "";
                                    int createdScheduleId = 0;

                                    logger.info("eachTemplateTriggerScheduleId  " + eachTemplateTriggerScheduleId);
                                    //TriggerTemplateSchedule triggerTemplateSchedule = triggerTemplateService.getTriggerTemplateScheduleById(eachTemplateTriggerScheduleId, "");

                                    Map templateScheduleMap = triggerTemplateDAO.getTriggerTemplateScheduleByUniqueID(eachTemplateTriggerScheduleId);
                                    int templateScheduleId = templateScheduleMap.containsKey("id") && Utils.isNotNull(templateScheduleMap.get("id")) ? Integer.parseInt(templateScheduleMap.get("id").toString()) : 0;

                                    String channel = templateScheduleMap.containsKey(Constants.CHANNEL) && Utils.isNotNull(templateScheduleMap.get(Constants.CHANNEL)) ? templateScheduleMap.get(Constants.CHANNEL).toString() : "";
                                    TriggerConfig triggerConfig = Utils.isNotNull(templateScheduleMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(templateScheduleMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig();
                                    String subject = templateScheduleMap.containsKey(Constants.SUBJECT) && Utils.isNotNull(templateScheduleMap.get(Constants.SUBJECT)) ? templateScheduleMap.get(Constants.SUBJECT).toString() : "";
                                    String message = templateScheduleMap.containsKey(Constants.MESSAGE) && Utils.isNotNull(templateScheduleMap.get(Constants.MESSAGE)) ? templateScheduleMap.get(Constants.MESSAGE).toString() : "";
                                    String replyRespondent = templateScheduleMap.containsKey(Constants.REPLYRESPONDENT) && Utils.isNotNull(templateScheduleMap.get(Constants.REPLYRESPONDENT)) ? templateScheduleMap.get(Constants.REPLYRESPONDENT).toString() : "";
                                    String frequency = templateScheduleMap.containsKey(Constants.FREQUENCY) && Utils.isNotNull(templateScheduleMap.get(Constants.FREQUENCY)) ? templateScheduleMap.get(Constants.FREQUENCY).toString() : "";
                                    Map integrationsConfig = templateScheduleMap.containsKey(Constants.INTEGRATIONS_CONFIG) && Utils.isNotNull(templateScheduleMap.get(Constants.INTEGRATIONS_CONFIG)) ? mapper.readValue(templateScheduleMap.get(Constants.INTEGRATIONS_CONFIG).toString(), HashMap.class) : new HashMap();
                                    TriggerWebhookConfigurationData webhookConfig = templateScheduleMap.containsKey(Constants.WEBHOOK_CONFIG) && Utils.isNotNull(templateScheduleMap.get(Constants.WEBHOOK_CONFIG)) ? mapper.readValue(templateScheduleMap.get(Constants.WEBHOOK_CONFIG).toString(), TriggerWebhookConfigurationData.class) : new TriggerWebhookConfigurationData();
                                    if(Utils.notEmptyString(webhookUrl)) {
                                        Map<String, String> headers = new HashMap();
                                        headers.put("Content-Type","application/json");
                                        webhookConfig.setEndPoint(webhookUrl);
                                        webhookConfig.setProtocol("POST");
                                        webhookConfig.setHeaders(headers);
                                    }
                                    TriggerListHeaderData triggerHeader = templateScheduleMap.containsKey("header_data") && Utils.isNotNull(templateScheduleMap.get("header_data")) ? mapper.readValue(templateScheduleMap.get("header_data").toString(), TriggerListHeaderData.class) : new TriggerListHeaderData();
                                    if(triggerHeader.getName() != null && triggerDAO.checkIfTagNameExists(triggerHeader.getName(), businessUUID)) {//DTV-4981
                                        Timestamp timestampName = new Timestamp(System.currentTimeMillis());
                                        triggerHeader.setName(triggerHeader.getName() + "_" + timestampName );
                                    }

                                    //DTV-4885
                                    if(channel.equalsIgnoreCase("email") || channel.equalsIgnoreCase("push") || channel.equalsIgnoreCase("reply")){
                                        triggerSchedule.setContacts(new ArrayList(Collections.singleton(userEmail)));
                                    }else {
                                        triggerSchedule.setContacts(new ArrayList());
                                    }

                                    triggerSchedule.setChannel(channel);
                                    triggerSchedule.setTriggerConfig(mapper.convertValue(triggerConfig, HashMap.class));
                                    triggerSchedule.setSubject(subject);
                                    triggerSchedule.setMessage(message);
                                    triggerSchedule.setReplyToRespondent(replyRespondent);
                                    triggerSchedule.setBusinessId(businessUUID);
                                    triggerSchedule.setTriggerId(createdTriggerUUID);
                                    triggerSchedule.setFrequency(frequency);
                                    //no start date, start time defined for inbuilt triggers in templates
                                    //adjusting with program start date, end date as trigger start & end date
                                    triggerSchedule.setStartDate(fromDate.substring(0, 10));
                                    triggerSchedule.setStartTime(fromDate.substring(11, 19));
                                    triggerSchedule.setEndDate(toDate.substring(0, 10));
                                    triggerSchedule.setEndTime(toDate.substring(11, 19));
                                    triggerSchedule.setTimezone(timezone);
                                    triggerSchedule.setHeaderData(triggerHeader);
                                    triggerSchedule.setIntegrationsConfig(integrationsConfig);
                                    triggerSchedule.setWebhookConfig(webhookConfig);

                                    logger.info("creating schedule for template Schedule Id  "+ templateScheduleId);


                                    Map triggerScheduleCreationMap = this.createTriggerSchedule(triggerSchedule);
                                    if (triggerScheduleCreationMap.size() > 0 && (Boolean) triggerScheduleCreationMap.get(Constants.SUCCESS)) {
                                        createdScheduleUUID = triggerScheduleCreationMap.get(Constants.RESULT).toString();
                                        //createdScheduleId = triggerDAO.getScheduledIdByUniqueId(createdScheduleUUID);
                                        Map scheduleMap = triggerDAO.getScheduledIdByUniqueId(createdScheduleUUID);
                                        int scheduleId = scheduleMap.containsKey("id") && Utils.isNotNull(scheduleMap.get("id")) ? Integer.parseInt(scheduleMap.get("id").toString()) : 0;
                                        businessId = scheduleMap.containsKey("business_id") && Utils.isNotNull(scheduleMap.get("business_id")) ? Integer.parseInt(scheduleMap.get("business_id").toString()) : 0;
                                    }

                                    templateScheduleIds.add(templateScheduleId);
                                    triggerScheduleIds.add(createdScheduleId);
                                    channels.add(channel);
                                    triggerMetadata.add(mapper.convertValue(triggerHeader, HashMap.class));
                                }

                                //Mapping all the created schedules associated with the trigger & template trigger
                                if(templateScheduleIds.size() > 0){
                                    scheduleTriggerProgramMap.put("templateScheduleIds", templateScheduleIds);
                                    scheduleTriggerProgramMap.put("triggerScheduleIds", triggerScheduleIds);
                                    scheduleTriggerProgramMap.put("channels", channels);
                                    scheduleTriggerProgramMap.put("triggerMetadata", triggerMetadata);
                                    logger.info("scheduleTriggerProgramMap   "+ scheduleTriggerProgramMap);
                                   int result =  triggerDAO.updateTriggerSchedulesInTemplateProgramMap(businessId, templateId, programId, templateTriggerId, createdTriggerId, scheduleTriggerProgramMap);
                                }
                            } else {
                                /**
                                 * No schedules found for given trigger, mapping created trigger with template trigger in template trigger map
                                 */
                                int result =  triggerDAO.updateTriggerInTemplateTriggerMap(businessId, templateId, programId, templateTriggerId, createdTriggerId);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }


                } else {
                    logger.info("no template found to create triggers");
                }
            }else {
                //DTV-4013, DTV-3870 creating selected pre defined triggers from template
                //no triggers selected while creating program from template
                logger.info("no selected triggers found while creating program from template");
            }
        }catch (Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
        }

        return 1;
    }


    /**
     * method to convert template trigger condition to trigger condition
     */
    public String convertTemplateToTriggerCondition(String templateUUID, String surveyUUID, String templateTriggerCondition, Map templateSurveyQuestionUUIDMap) {
        String triggerCondition = "";
        try {
            logger.info("triggerCondition  " + templateTriggerCondition);
            if (Utils.notEmptyString(templateTriggerCondition) && !templateTriggerCondition.equals("DTANY")) {

                templateTriggerCondition = templateTriggerCondition.replaceAll(templateUUID, surveyUUID);
                //spliting the triggerCondition with multiple questions into each question using '&&' split
                String[] andQueries = templateTriggerCondition.split("&&");
                StringBuilder andQuery = new StringBuilder();

                int andIndex = 0;
                for (int i = 0; i < andQueries.length; i++) {
                    String eachAndQuery = andQueries[i];
                    StringBuilder orQuery = new StringBuilder();

                    String[] orQueries = eachAndQuery.split("\\|\\|");
                    /**
                     * Sample :
                     * templateCondition : question.templateUUID.templateQuestionUUID.response=0||question.templateUUID.templateQuestionUUID.response=1
                     * triggerCondition : question.surveyUUID.surveyQuestionUUID.response=0||question.surveyUUID.surveyQuestionUUID.response=1
                     */
                    int orIndex = 0;
                    for (int k = 0; k < orQueries.length; k++) {
                        String eachOrQuery = orQueries[k];
                        String templateQuestionUUID = eachOrQuery.split("\\.")[2];
                        String surveyQuestionUUID = templateSurveyQuestionUUIDMap.containsKey(templateQuestionUUID) ? templateSurveyQuestionUUIDMap.get(templateQuestionUUID).toString() : "";
                        eachOrQuery = eachOrQuery.replace(templateQuestionUUID, surveyQuestionUUID);

                        if (orIndex < orQueries.length - 1) {
                            orQuery.append(eachOrQuery + "||");
                            orIndex++;
                        } else {
                            orQuery.append(eachOrQuery);
                        }
                    }

                    if (andIndex < andQueries.length - 1) {
                        andQuery.append(orQuery + "&&");
                        andIndex++;
                    } else {
                        andQuery.append(orQuery);
                    }

                }
                triggerCondition = andQuery.toString();
                logger.info("triggerCondition  " + triggerCondition);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at convertTemplateToTriggerCondition. Msg {}", e.getMessage());
        }
        return triggerCondition;
    }
    /**
     * method to get list of triggers used in a survey by SurveyId
     * @param programUniqueId
     * @param businessUniqueId
     * @return
     */
    public List getTriggerListByProgramId(String programUniqueId, String businessUniqueId, String timezone){
        List programTriggerList = new ArrayList();
        try {
            int surveyId = triggerDAO.getSurveyIdByUUID(programUniqueId, businessUniqueId);
            List programlist = triggerDAO.getAllTriggersBySurveyId(businessUniqueId, surveyId);
            Iterator itr = programlist.iterator();
            while (itr.hasNext()) {
                try {
                    Map triggerMap = (Map) itr.next();
                    Trigger trigger = new Trigger();
                    String triggerUniqueId = triggerMap.containsKey("trigger_uuid") ? (String) triggerMap.get("trigger_uuid") : "";
                    trigger.setSurveyName(triggerMap.get(Constants.SURVEY_NAME).toString());
                    //DTV-8132 : If survey is deactivated, then trigger should be deactivated (status = 2)
                    trigger.setStatus((((int) triggerMap.get(Constants.STATUS)) == 0 || ((int) triggerMap.get(Constants.STATUS)) == 2) ? Constants.STATUS_INACTIVE : Constants.STATUS_ACTIVE);
                    trigger.setSurveyId(triggerDAO.getSurveyUniqueIdById(Integer.parseInt(triggerMap.get(Constants.SURVEY_ID).toString()), businessUniqueId));
                    trigger.setTriggerName(triggerMap.get(Constants.TRIGGER_NAME).toString());
                    trigger.setTriggerCondition(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : Constants.EMPTY);
                    trigger.setDeactivatedConditions((triggerMap.containsKey(Constants.DEACTIVATED_CONDITIONS) && Utils.isNotNull(triggerMap.get(Constants.DEACTIVATED_CONDITIONS))) ? mapper.readValue(triggerMap.get(Constants.DEACTIVATED_CONDITIONS).toString(), ArrayList.class) : new ArrayList<>());
                    trigger.setLastModified(Utils.isNotNull(triggerMap.get(Constants.MODIFIED_TIME)) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.MODIFIED_TIME).toString(), timezone) : Constants.EMPTY);
                    trigger.setTriggerId(triggerMap.get(Constants.TRIGGER_UUID).toString());
                    trigger.setBusinessUniqueId(businessUniqueId);
                    trigger.setSurveyStartDate(triggerMap.containsKey(Constants.FROM_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.FROM_DATE).toString(), timezone) : Constants.EMPTY);
                    trigger.setSurveyEndDate(triggerMap.containsKey(Constants.TO_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.TO_DATE).toString(), timezone) : Constants.EMPTY);
                    trigger.setTriggerFilterUUID(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_FILTER_UUID)) ? triggerMap.get(Constants.TRIGGER_FILTER_UUID).toString() : Constants.EMPTY);
                    int anonymousValue = (Utils.isNotNull(triggerMap.get(Constants.ANONYMOUS)) && Utils.isStringNumber(triggerMap.get(Constants.ANONYMOUS).toString())) ? Integer.parseInt(triggerMap.get(Constants.ANONYMOUS).toString()) : 0;
                    trigger.setAnonymous((anonymousValue == 1) ? true : false);

                    String startDate = "";
                    String startTime = "";
                    String endDate = "";
                    String endTime = "";
                    if ((triggerMap.containsKey(Constants.START_DATE) && Utils.isNotNull(triggerMap.get(Constants.START_DATE))) && (triggerMap.containsKey(Constants.START_TIME) && (Utils.isNotNull(triggerMap.get(Constants.START_TIME))))) {

                        String convertedStartDate = Utils.getLocalDateStringFromUTCDateAndTime(triggerMap.get(Constants.START_DATE).toString(), triggerMap.get(Constants.START_TIME).toString(), timezone);
                        startDate = convertedStartDate.substring(0, 10);
                        startTime = convertedStartDate.substring(11, 19);
                        trigger.setTriggerStartDate(startDate);
                        trigger.setTriggerStartTime(startTime);
                    } else {
                        trigger.setTriggerStartDate(Constants.EMPTY);
                        trigger.setTriggerStartTime(Constants.EMPTY);
                    }

                    if ((triggerMap.containsKey(Constants.END_DATE) && Utils.isNotNull(triggerMap.get(Constants.END_DATE))) && (triggerMap.containsKey(Constants.END_TIME) && (Utils.isNotNull(triggerMap.get(Constants.END_TIME))))) {
                        String convertedEndDate = Utils.getLocalDateStringFromUTCDateAndTime(triggerMap.get(Constants.END_DATE).toString(), triggerMap.get(Constants.END_TIME).toString(), timezone);
                        endDate = convertedEndDate.substring(0, 10);
                        endTime = convertedEndDate.substring(11, 19);
                        trigger.setTriggerEndDate(endDate);
                        trigger.setTriggerEndTime(endTime);
                    } else {
                        trigger.setTriggerEndDate(Constants.EMPTY);
                        trigger.setTriggerEndTime(Constants.EMPTY);
                    }
                    trigger.setFrequency((triggerMap.containsKey(Constants.FREQUENCY) && Utils.isNotNull(triggerMap.get(Constants.FREQUENCY))) ? triggerMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
                    if (triggerUniqueId.length() > 0) {
                        Map triggerResultMap = mapper.convertValue(trigger, HashMap.class);
                        List<TriggerSchedule> scheduleList = this.getTriggerScheduleByTriggerId(triggerUniqueId, timezone, businessUniqueId);
                        triggerResultMap.put("schedule", scheduleList);
                        programTriggerList.add(triggerResultMap);
                    }
                }catch (Exception e) {
                    logger.error("error {}",e.getMessage());
                }

            }

        }catch (Exception e){
            logger.error("error at getTriggerTemplatesNameListById {}", e.getMessage());
        }
        return programTriggerList;
    }


    /**
     * DTV-4010, DTV-4013
     * If anything related to the question has been changed then the condition tied to the question will be removed from trigger condition.
     * If the question has been deleted by the user during the program creation then the question will be removed from trigger condition.
     *
     */
    public Map updateNewTriggerConditionForTriggers(int surveyId, validateTriggerConditionBean bean){
        Map result = new HashMap();
        try{

            String businessUUID = Utils.isNotNull(bean.getBusinessUUID()) ? bean.getBusinessUUID() : "" ;
            List updatedOrDeletedQuestions = Utils.isNotNull(bean.getQuestionIds()) ? bean.getQuestionIds() : new ArrayList();
            List triggerIds = Utils.isNotNull(bean.getTriggerIds()) ? bean.getTriggerIds() : new ArrayList();
            int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);

            logger.info("updatedOrDeletedQuestions  "+updatedOrDeletedQuestions);
            if(updatedOrDeletedQuestions.size() > 0){

                //templateProgramList contains list all triggers created with  program from template
                List templateProgramTriggerIds = new ArrayList();
                List templateProgramList = triggerDAO.returnTemplateProgramMapWithTriggersByProgramId(businessId, surveyId);
                //check program is created with template & whether program contains pre built triggers or not
                //adding all the triggerIds in triggerIds list associated with program from template program map
                for (int i = 0; i < templateProgramList.size(); i++) {
                    Map eachTemplateProgramMap = (Map) templateProgramList.get(i);
                    int triggerId = Utils.isNotNull(eachTemplateProgramMap.get("trigger_id")) ? Integer.parseInt(eachTemplateProgramMap.get("trigger_id").toString()) : 0;
                    if (!templateProgramTriggerIds.contains(triggerId) && triggerId > 0) {
                        templateProgramTriggerIds.add(triggerId);
                    }
                }


                /** triggerIds contains incoming triggerIds
                 *  templateProgramTriggerIds contians triggerIds from template_program_map
                 *  Trigger condition updation happens only for those pre built triggers created from template during program creation.
                 *
                 * Need to validate incoming triggerIds with templateProgramTriggerIds to remove triggerIds not created from template
                 * If incoming triggerIds is empty or null then triggerIds = templateProgramTriggerIds
                 */

                logger.info("triggerIds  " + triggerIds);
                logger.info("templateProgramTriggerIds  " + templateProgramTriggerIds);
                if(triggerIds.size() > 0) {
                    triggerIds.retainAll(templateProgramTriggerIds);
                }else {
                    triggerIds = templateProgramTriggerIds;
                }

                logger.info("triggerIds  " + triggerIds);
                if(triggerIds.size() > 0) {
                    result = triggerDAO.updateTriggerConditionsByQuestionIds(businessUUID, updatedOrDeletedQuestions, triggerIds);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  result;
    }

    /**
     * method to get participant information
     * @param eachToken
     * @param businessUUID
     * @return
     */
    public Map getParticipantInfo(String eachToken, String businessUUID) {
        LinkedHashMap responseMap = new LinkedHashMap();
        List dynamicTokens = triggerDAO.getSurveyStatusByUserToken(eachToken, businessUUID);
        if (dynamicTokens.size() > 0) {
            Map eachDynamicToken = (Map) dynamicTokens.get(0);
            int eachParticipantId = (int) eachDynamicToken.get("participant_id");
            int eachParticipantGroupId = (int) eachDynamicToken.get("participant_group_id");
            int eachTagId = (int) eachDynamicToken.get("tag_id");
            String createdTime = String.format(eachDynamicToken.get("last_action_time").toString());

            String participantGroupUUID = triggerDAO.getParticipantGroupUUIdByParticipantGrpID(eachTagId, eachParticipantGroupId, businessUUID);

            if (participantGroupUUID.length() > 0) {
                //CSUP-903 respondentName empty case
                String respondentName = "";
                List participantDataList = triggerDAO.getParticipantDataById(eachParticipantId, participantGroupUUID);
                for (int j = 0; j < participantDataList.size(); j++) {
                    String eachHeader = "";
                    String eachValue = "";
                    String  eachMetaData = "";
                    Map bean = (Map) participantDataList.get(j);
                    JSONArray dataArr = new JSONArray(bean.get("data").toString());
                    JSONArray metaDataArr = new JSONArray(bean.get("meta_data").toString());
                    JSONArray headerArr = new JSONArray(bean.get("header").toString());
                    int dataLength = dataArr.length();
                    for (int k = 0; k < dataLength; k++) {
                        eachValue = (dataArr.get(k).toString());
//                        eachHeader = StringUtils.capitalize(headerArr.get(k).toString());
                        eachHeader = (headerArr.get(k).toString());
                        eachMetaData = (metaDataArr.get(k).toString().toUpperCase());
                        responseMap.put(eachHeader + Constants.DELIMITER + eachMetaData, eachValue);
                        //CSUP-903
                        if((eachMetaData.equalsIgnoreCase("NAME") || eachMetaData.equalsIgnoreCase("LINKNAME"))&& Utils.emptyString(respondentName))
                            respondentName = eachValue;
                    }
                }
                responseMap.put("created_time", createdTime);
                //CSUP-904
                responseMap.put("respondentName", respondentName);
                responseMap.put("participantGroupId", participantGroupUUID); //DTV-10605
                responseMap.put("participantId", eachParticipantId);
            }
        }
        return responseMap;
    }

    /**
     * method to add demliter from list of string. From Age and Number to Age~~NUMBER
     * @param headerList
     * @return
     */
    private LinkedList appendHeaderwithMetaDataType(List headerList, List metaDataList){
        LinkedList headerMetaDataTypeList = new LinkedList();

        try {
//            Iterator itr = headerList.iterator();
          for(int i = 0; i < headerList.size(); i++) {
              String eachHeader = headerList.get(i).toString(); //eg: Age
              String eachMetaData = metaDataList.get(i).toString(); //eg: Number
//              headerMetaDataTypeList.add(StringUtils.capitalize(eachHeader) + Constants.DELIMITER + eachMetaData.toUpperCase()); // eg: Age~~NUMBER
              headerMetaDataTypeList.add((eachHeader) + Constants.DELIMITER + eachMetaData.toUpperCase()); // eg: Age~~NUMBER

          }
        }catch (Exception e){
            logger.error("Exception at appendHeaderwithMetaDataType. Msg {}", e.getMessage());
        }
        return  headerMetaDataTypeList;
    }

    /**
     * DTV-5352, DTV-5353
     * update execution start and end times based on hourly basis
     */
    public void updateExecutionStartAndEndTimeV2(){
        logger.info("begin update exec time");
        try{
            List schedules = triggerDAO.getActiveSchedules();
            Iterator iterator = schedules.iterator();
            while (iterator.hasNext()){
                try{
                    Map eachScheduleMap = (Map)iterator.next();
                    String executionStartTime = Utils.isNotNull(eachScheduleMap.get(Constants.EXEC_START_TIME)) ? eachScheduleMap.get(Constants.EXEC_START_TIME).toString() : "";
                    String executionEndTime = Utils.isNotNull(eachScheduleMap.get(Constants.EXEC_END_TIME)) ? eachScheduleMap.get(Constants.EXEC_END_TIME).toString() : "";

                    int id = (int)eachScheduleMap.get(Constants.SCHEDULE_ID);

                    //DTV-5352, DTV-5353 Multiple triggers & delay in triggers aggregate case fix
                    if(Utils.notEmptyString(executionStartTime) && Utils.notEmptyString(executionEndTime)){
                        String currentUTCDate = Utils.getCurrentUTCDateAsString();
                        //check current dat in timezone is greater than exec end time
                        //If current date in timezone > exec end date --> update exec end date and start date with 24 hrs
                        if(Utils.compareDates(currentUTCDate, executionEndTime)){
                            executionStartTime = Utils.getFormattedDateTimeAddHours(executionStartTime, 24);
                            executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
                        }
                    }else {
                        if (Utils.emptyString(executionStartTime)) {
                            executionStartTime = eachScheduleMap.get(Constants.START_DATE).toString() + " " + eachScheduleMap.get(Constants.START_TIME).toString();
                        } else {
                            executionStartTime = Utils.getFormattedDateTimeAddHours(executionStartTime, 24);
                        }

                        if (Utils.emptyString(executionEndTime)) {
                            executionEndTime = eachScheduleMap.get(Constants.START_DATE).toString() + " " + eachScheduleMap.get(Constants.END_TIME).toString();
                            if (Utils.compareTimes(eachScheduleMap.get(Constants.START_TIME).toString(), eachScheduleMap.get(Constants.END_TIME).toString())) {
                                executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
                            }
                        } else {
                            executionEndTime = Utils.getFormattedDateTimeAddHours(executionEndTime, 24);
                        }
                    }

                    updateExecTime(executionStartTime, executionEndTime, id);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.debug("end update exec time");

    }

    /**
     * Method to get metric score for a survey response
     *
     * @param surveyMap
     * @param businessUniqueId
     * @param surveyId
     * @param fromDate
     * @param toDate
     * @param tokenList
     * @param keyMetricId
     * @return
     */

    public String getMetricScoreDetails(Map surveyMap, String businessUniqueId, String surveyId, String fromDate, String toDate, List tokenList, int keyMetricId) {
        try {
            String questionIdsStr = surveyMap.containsKey("question_ids") && Utils.isNotNull(surveyMap.get("question_ids")) ? surveyMap.get("question_ids").toString() : "";
            List questionIds = mapper.readValue(questionIdsStr, ArrayList.class);
            logger.debug("questionIds list {}", questionIds.toString());

            String metricsString = surveyMap.containsKey("metrics_map") && Utils.isNotNull(surveyMap.get("metrics_map")) ? surveyMap.get("metrics_map").toString() : "";
            Map metricMap = Utils.notEmptyString(metricsString) ? mapper.readValue(metricsString, HashMap.class) : new HashMap();

            List keyMetricIdList = triggerDAO.getKeyMetricDetailsById(businessUniqueId, keyMetricId);
            String metricTextStr = "";
            String scale = "";
            if (keyMetricIdList.size() > 0) {
                Map keyMetricMap = (Map) keyMetricIdList.get(0);
                metricTextStr = keyMetricMap.containsKey("key_metric_text") ? keyMetricMap.get("key_metric_text").toString() : "";
                scale = keyMetricMap.containsKey("scale") ? keyMetricMap.get("scale").toString() : "0";
            }

            Map metriScoreMap = new LinkedHashMap();
            List ratingQuestionIdList = (ArrayList) metricMap.get(String.valueOf(keyMetricId));
            Iterator rationQuestionIdItr = ratingQuestionIdList.iterator();

            while (rationQuestionIdItr.hasNext()) {
                int eachId = (int) rationQuestionIdItr.next();
                int eachQuestionIndex = questionIds.indexOf(eachId);
                List ratingQuestionScoreCount = triggerDAO.computeQuestionRatingsCounts(surveyId, fromDate, toDate, eachQuestionIndex, scale, tokenList);
                if (metriScoreMap.containsKey(metricTextStr)) {
                    List eachExistingScoreList = (List) metriScoreMap.get(metricTextStr);

                    for (int idx = 0; idx < eachExistingScoreList.size(); idx++) {
                        List<Integer> eachScoreCount = (List) ratingQuestionScoreCount.get(idx);
                        List<Integer> eachExistScoreCount = (List) eachExistingScoreList.get(idx);

                        List totalScoreList = new ArrayList();
                        for (int j = 0; j < eachExistScoreCount.size(); j++) {
                            totalScoreList.add(eachExistScoreCount.get(j) + eachScoreCount.get(j));
                        }
                        eachExistingScoreList.set(idx, totalScoreList);
                    }
                    metriScoreMap.put(metricTextStr, eachExistingScoreList);

                } else {
                    metriScoreMap.put(metricTextStr, ratingQuestionScoreCount);
                }
            }
            return computeMetricScores(metriScoreMap, scale, metricTextStr);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return "0";

    }

    /**
     * Method to compute metric scores
     *
     * @param questionCountMap
     * @param scale
     * @return
     */
    public String computeMetricScores(Map questionCountMap, String scale, String metricName) {
        Map metricResponse = new HashMap();
        List scoreList = new ArrayList();

        int scaleInt = Utils.notEmptyString(scale) ? Integer.parseInt(scale) : 0;
        float score = 0;
        float totalWeightedSum = 0;
        int totalSum = 0;

        List eachquestioncount = (List) questionCountMap.get(metricName);
        logger.info("eachquestioncount list {}", eachquestioncount);
        Iterator itr = eachquestioncount.iterator();

        while (itr.hasNext()) {
            List eachQuestCountList = (List) itr.next();
            float weightedSum = 0;
            int sum = 0;

            List<Integer> eachCntLst = new ArrayList<Integer>(Collections.nCopies(scaleInt, 0));
            for (int scaleIndex = 0; scaleIndex < scaleInt; scaleIndex++) {
                int eachScaleCount = eachQuestCountList.size() > 0 ? (int) eachQuestCountList.get(scaleIndex) : 0;
                eachCntLst.set(scaleIndex, eachScaleCount);
            }

            for (int scaleIndex = 0; scaleIndex < scaleInt; scaleIndex++) {
                int countAtIndex = (int) eachCntLst.get(scaleIndex);
                weightedSum = weightedSum + ((scaleIndex + 1) * countAtIndex);
                sum = sum + countAtIndex;
            }
            totalWeightedSum = totalWeightedSum + weightedSum;
            totalSum = totalSum + sum;
            score = weightedSum / sum;
            logger.info("score " + score);
            scoreList.add(!Float.isNaN(score) ? String.format("%.1f", score) : 0);
        }
        float totalScore = totalWeightedSum / totalSum;
        String total = !Float.isNaN(totalScore) ? String.format("%.1f", totalScore) : "0";
       /* metricResponse.put("metricScoreList", scoreList);
        metricResponse.put("totalMetricScore", total);*/
        logger.info("metric response info {}", new JSONObject(metricResponse).toString());
        return total;
    }

    /**
     *
     * Method to check frequency notifcation exist for the schedule during the reminder time
     * Example : If frequency is set as 20 mins.
     * First email will trigger as soon as the condition is met, lets say suppose at 2:00 PM
     * Next reminder email will be at 2:20 PM (at >=2.20 PM condition must satisfy)
     * Check new notify time exceeds previous notification start time then insert new notification
     *
     * @param id
     * @return
     */
    public boolean checkFrequencyNotificationExistsForTheSchedule(int id, boolean isConditionMet,TriggerNotification triggerNotification, int intervalInMinutes, String businessUUID){
        boolean isPresent = false;
        try{
            List notifications = triggerDAO.retrieveLatestFrequencyNotificationsForASchedule(id, businessUUID);
            String currentTimeUTC = Utils.getCurrentUTCDateAsString();
            /**
             * For second or next reminders check previous created notification created time by schedule id
             * Check new notify time exceeds previous notification start time then insert new notification
             */
            if(notifications.size() > 0){
                //notification already existed for scheduleId check start_time with triggerNotifyTime
                Map notificationMap = (Map) notifications.get(0);
                String startTime = notificationMap.containsKey("start_time") && Utils.isNotNull(notificationMap.get("start_time")) ? notificationMap.get("start_time").toString() : "";
                //isPresent = true, if triggerNotifytime >= startTime (previous notification)
                if(Utils.notEmptyString(startTime)) {
                    //adding reminder mintues (intervalInMinutes) to latest trigger notification
                    //Check new notify time exceeds previous notification start time then insert new notification
                    String reminderTime = intervalInMinutes > 0 ? Utils.addMinutesToDate(startTime, intervalInMinutes) : startTime;
                    logger.info("currentTimeUTC   "+currentTimeUTC);
                    logger.info("reminderTime   "+reminderTime);
                    logger.info("intervalInMinutes   "+intervalInMinutes);
                    isPresent =  Utils.compareDates(reminderTime, currentTimeUTC);
                    logger.info("isPresent   "+isPresent);
                    triggerNotification.setStartTime(currentTimeUTC);
                }
            }else if(isConditionMet) {
                /**
                 * For example, if the user chooses “20” and “mins”,
                 * send the first reminder as soon as the condition is met
                 * and send reminders every 20 minutes as long as the condition remains true.
                 */
                triggerNotification.setStartTime(currentTimeUTC);
            }
        }catch (Exception e){
            e.printStackTrace();
            return true; //to avoid duplicate trigger emails
        }
        return isPresent;
    }

    public int getBusinessIdbyUUID(String businessId){
        return triggerDAO.getBusinessIdByUUID(businessId);
    }

    /**
     * function to check if the provided arguments are dates or not.
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static boolean checkValidDate(String dateString1,String dateString2) {
        boolean validDate = false;
        try {
            String ymdFormat = "\\d{4}-\\d{1}-\\d{1}"; //2020-3-9
            String ymdFormat1 = "\\d{4}-\\d{1}-\\d{2}"; //2020-3-19
            String ymdFormat2 = "\\d{4}-\\d{2}-\\d{1}"; //2020-11-9
            String ymdFormat3 = "\\d{4}-\\d{2}-\\d{2}"; //2020-11-9
            String ymdHisFormat = "\\d{4}-\\d{1}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-3-9 22:44:00
            String ymdHisFormat1 = "\\d{4}-\\d{1}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-3-19 22:44:00
            String ymdHisFormat2 = "\\d{4}-\\d{2}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00
            String ymdHisFormat3 = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00

            Pattern pattern1 = Pattern.compile(ymdFormat);
            Pattern pattern2 = Pattern.compile(ymdHisFormat);
            Pattern pattern3 = Pattern.compile(ymdFormat1);
            Pattern pattern4 = Pattern.compile(ymdHisFormat1);
            Pattern pattern5 = Pattern.compile(ymdFormat2);
            Pattern pattern6 = Pattern.compile(ymdHisFormat2);
            Pattern pattern7 = Pattern.compile(ymdFormat3);
            Pattern pattern8 = Pattern.compile(ymdHisFormat3);

            Matcher matcher1 = pattern1.matcher(dateString1);
            Matcher matcher2 = pattern2.matcher(dateString1);
            Matcher matcher3 = pattern3.matcher(dateString1);
            Matcher matcher4 = pattern4.matcher(dateString1);
            Matcher matcher5 = pattern5.matcher(dateString1);
            Matcher matcher6 = pattern6.matcher(dateString1);
            Matcher matcher7 = pattern7.matcher(dateString1);
            Matcher matcher8 = pattern8.matcher(dateString1);

            Matcher matcher9 = pattern1.matcher(dateString2);
            Matcher matcher10 = pattern2.matcher(dateString2);
            Matcher matcher11 = pattern3.matcher(dateString2);
            Matcher matcher12 = pattern4.matcher(dateString2);
            Matcher matcher13 = pattern5.matcher(dateString2);
            Matcher matcher14 = pattern6.matcher(dateString2);
            Matcher matcher15 = pattern7.matcher(dateString2);
            Matcher matcher16 = pattern8.matcher(dateString2);

            if (matcher1.matches() || matcher2.matches() || matcher3.matches() || matcher4.matches() || matcher5.matches() || matcher6.matches() || matcher7.matches() || matcher8.matches()) {
                if(matcher9.matches() || matcher10.matches() || matcher11.matches() || matcher12.matches() || matcher13.matches() || matcher14.matches() || matcher15.matches() || matcher16.matches()) {
                    validDate = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return validDate;
    }

    /**
     * method to form microsoftDynamics metadata object with Dt metadata for microsoftDynamics trigger
     */
    public Map updateMetadataForMicrosoftDynamics(Map integrationsConfigMap, Map eventMap) {
        Map metaData = new HashMap();
        try{
            Map dtMetadataMap = eventMap.containsKey("metaData") && Utils.isNotNull(eventMap.containsKey("metaData")) ? (Map) eventMap.get("metaData") : new HashMap();
            Map<String, String> microsoftDynamicsMetadataMap = integrationsConfigMap.containsKey("metaData") && Utils.isNotNull(integrationsConfigMap.containsKey("metaData")) ? (Map<String, String>) integrationsConfigMap.get("metaData") : new HashMap();
            if(microsoftDynamicsMetadataMap.size() > 0){
                for (Map.Entry<String, String> entry : microsoftDynamicsMetadataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = "";
                    String eachDtHeader = Utils.isNotNull(entry.getValue()) && Utils.notEmptyString(entry.getValue()) ? entry.getValue(): "";
                    if(Utils.notEmptyString(eachDtHeader)){
                        value = dtMetadataMap.containsKey(eachDtHeader) && Utils.isNotNull(dtMetadataMap.get(eachDtHeader)) && Utils.notEmptyString(dtMetadataMap.get(eachDtHeader).toString())
                                ? dtMetadataMap.get(eachDtHeader).toString() : "";
                    }
                    metaData.put(key, value);
                }
                if(dtMetadataMap.containsKey("PRTNAME")){
                    metaData.put("PRTNAME", Utils.isNotNull(dtMetadataMap.get("PRTNAME")) && Utils.notEmptyString(dtMetadataMap.get("PRTNAME").toString()) ?
                            dtMetadataMap.get("PRTNAME").toString() : "");
                }
            }else{
                metaData = dtMetadataMap;
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("exception at updateMetadataForMicrosoftDynamics. Msg {}", e.getMessage());
        }
        return metaData;
    }

    /**
     * method to form salesforce metadata object with Dt metadata for salesforce trigger
     */
    public Map updateMetadataForSalesforce(Map integrationsConfigMap, Map eventMap, String notifyTime, String timezone) {

        Map<String, String> metaData = new HashMap();
        boolean isNoteCreation = false;
        boolean isTaskCreation = false;
        try{
            Map dtMetadataMap = eventMap.containsKey("metaData") && Utils.isNotNull(eventMap.containsKey("metaData")) ? (Map) eventMap.get("metaData") : new HashMap();
            Map<String, String> salesforceMetadataMap = integrationsConfigMap.containsKey("metaData") && Utils.isNotNull(integrationsConfigMap.containsKey("metaData")) ? (Map<String, String>) integrationsConfigMap.get("metaData") : new HashMap();
            if(salesforceMetadataMap.size() > 0){
                for (Map.Entry<String, String> entry : salesforceMetadataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = "";
                    String eachDtHeader = Utils.isNotNull(entry.getValue()) && Utils.notEmptyString(entry.getValue()) ? entry.getValue(): "";
                    if(Utils.notEmptyString(eachDtHeader)){
                        value = dtMetadataMap.containsKey(eachDtHeader) && Utils.isNotNull(dtMetadataMap.get(eachDtHeader)) && Utils.notEmptyString(dtMetadataMap.get(eachDtHeader).toString())
                                ? dtMetadataMap.get(eachDtHeader).toString() : "";
                    }
                    metaData.put(key, value);
                }
                if(dtMetadataMap.containsKey("PRTNAME")){
                    metaData.put("PRTNAME", Utils.isNotNull(dtMetadataMap.get("PRTNAME")) && Utils.notEmptyString(dtMetadataMap.get("PRTNAME").toString()) ?
                            dtMetadataMap.get("PRTNAME").toString() : "");
                }
            }else {
                metaData = dtMetadataMap;
            }

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            //DTV-8822
            String recipeType = "";
            //DTV-8828
            //close date format mm/dd/yyyy
            String closeDate = "";
            String dueDate = "";
            String remindDate = "";

            if(integrationMap.containsKey("salesforce")) {
               Object recipeMap = integrationMap.remove("salesforce");
                if(recipeMap instanceof Map){
                     Map recipeMap1 = (Map) recipeMap;
                     if(recipeMap1.containsKey("recipeType")){
                          recipeType = recipeMap1.get("recipeType").toString();
                     }
                }
            }

            if(integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");


            //DTV-8822
            //Create/update Opportunity in Salesforce close date is mandatory for create oppurtunity in salesforce
            if(recipeType.equalsIgnoreCase("createOpportunity")){
                if(integrationMap.containsKey("closeDate")) {
                    Object closeDateMap = integrationMap.remove("closeDate");
                    if (closeDateMap instanceof Map) {
                        closeDate = this.getDateByCloseDateConfig((Map) closeDateMap, notifyTime, timezone, true);
                        metaData.put("Close Date", closeDate);
                    }
                }else{
                    closeDate = this.getDateByCloseDateConfig(new HashMap(), notifyTime, timezone, true);
                    metaData.put("Close Date", closeDate);
                }
            }

            //DTV-9396
            //Create/Update Task in salesforce
            if(recipeType.equalsIgnoreCase("createTask")) {

                isTaskCreation = true;
                if (integrationMap.containsKey("dueDate")) {
                    Object dueDateMap = integrationMap.remove("dueDate");
                    if (dueDateMap instanceof Map) {
                        dueDate = this.getDateByCloseDateConfig((Map) dueDateMap, notifyTime, timezone, true);
                        metaData.put("Due date", dueDate);
                    }
                } else {
                    dueDate = this.getDateByCloseDateConfig(new HashMap(), notifyTime, timezone, true);
                    metaData.put("Due date", dueDate);
                }

                boolean remindSet = true;
                if (integrationMap.containsKey("remind")) {

                    Object remindMap = integrationMap.remove("remind");
                    Map<String, Object> remindMap1 = (Map<String, Object>) remindMap;
                    if (remindMap1.containsKey("enabled") && remindMap1.get("enabled").toString().equalsIgnoreCase("true")) {

                        if (remindMap instanceof Map) {
                            remindDate = this.getDateByCloseDateConfig((Map) remindMap, notifyTime, timezone, true);
                            metaData.put("Remind", remindDate);
                        }

                    } else {
                        remindSet = false;
                    }

                } else {
                    remindSet = false;
                }

                if (!remindSet) {
                    metaData.put("Remind", "");
                }
            }

            if (recipeType.equalsIgnoreCase("createNotes")) {
                isNoteCreation = true;
            }
            logger.info("integrationMap {}", integrationMap);
            logger.info("dtMetadataMap {}", dtMetadataMap);

            //appending key value pairs in metaData object using streams.
            for (Map.Entry<String, Map> entry : integrationMap.entrySet()) {
                Map<String, String> eachValueMap = entry.getValue();

                logger.info("eachValueMap {}", eachValueMap);

                // replace tag value with corresponding respondent meta data info
                if (isNoteCreation) {
                    if(eachValueMap.containsKey("Title") && eachValueMap.get("Title").contains("${")) {

                        String title = eachValueMap.get("Title");
                        List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(title)));
                        if (tagsToReplace.size() > 0) {
                            Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                            eachValueMap.replace("Title", title, Utils.replaceTokens(title, substituteTokens));
                        }
                        logger.info("Note Creation - replaced hashTag {}", eachValueMap);
                    }
                    if(eachValueMap.containsKey("Body") && eachValueMap.get("Body").contains("${")) {

                        String bodyMsg = eachValueMap.get("Body");
                        List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(bodyMsg)));
                        if (tagsToReplace.size() > 0) {
                            Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                            eachValueMap.replace("Body", bodyMsg, Utils.replaceTokens(bodyMsg, substituteTokens));
                        }
                        logger.info("Note Creation - replaced hashTag {}", eachValueMap);
                    }

                }
                if (isTaskCreation) {
                    if(eachValueMap.containsKey("Subject") && eachValueMap.get("Subject").contains("${")) {

                        String subject = eachValueMap.get("Subject");
                        List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(subject)));
                        logger.info("tagsToReplace {}", tagsToReplace);
                        if (tagsToReplace.size() > 0) {
                            Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                            eachValueMap.remove("Subject");
                            eachValueMap.put("Subject", Utils.replaceTokens(subject, substituteTokens));
                            logger.info("Subject {} ", Utils.replaceTokens(subject, substituteTokens));
                        }
                        logger.info("Task Creation - replaced hashTag {}", eachValueMap);
                    }
                    if(eachValueMap.containsKey("Comments") && eachValueMap.get("Comments").contains("${")) {

                        String comments = eachValueMap.get("Comments");
                        List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(comments)));
                        if (tagsToReplace.size() > 0) {
                            Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                            eachValueMap.remove("Comments");
                            eachValueMap.put("Comments", Utils.replaceTokens(comments, substituteTokens));
                            logger.info("Comments {} ", Utils.replaceTokens(comments, substituteTokens));
                        }
                        logger.info("Task Creation - replaced hashTag {}", eachValueMap);
                    }
                }
                Map<String, String> finalMetaData = metaData;
                eachValueMap.forEach((k, v) -> finalMetaData.merge(k, v, (oldval, newval) -> oldval));
                metaData = finalMetaData;
            }

            logger.info("metaData after {}", metaData);

        }catch (Exception e){
            e.printStackTrace();
            logger.error("exception at updateMetadataForMicrosoftDynamics. Msg {}", e.getMessage());
        }
        return metaData;
    }

    /**
     * method to form salesforce metadata object with Dt metadata for salesforce trigger
     */
    public Map updateMetadataForSalesforceV2(Map integrationsConfigMap, Map eventMap, String notifyTime, String timezone) {

        Map<String, String> metaData = new HashMap();
        boolean isNoteCreation = false;
        boolean isTaskCreation = false;
        try{

            metaData = this.getIntegrationMetadata(eventMap, integrationsConfigMap);
            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            String recipeType = getRecipeName(integrationMap, Constants.CHANNEL_SALESFORCE);

            //DTV-8822 Create/update Opportunity in Salesforce close date is mandatory for create oppurtunity in salesforce
            if(recipeType.equalsIgnoreCase("createOpportunity")){
                setDateInMetaData(integrationMap, metaData, notifyTime, timezone, "closeDate", "Close Date", true);
            }
            //DTV-9396 Create/Update Task in salesforce
            else if(recipeType.equalsIgnoreCase("createTask")) {
                isTaskCreation = true;
                setDateInMetaData(integrationMap, metaData, notifyTime, timezone, "dueDate", "Due date", true);
                setReminder(integrationMap, notifyTime, timezone, metaData);
            }
            else if (recipeType.equalsIgnoreCase("createNotes")) {
                isNoteCreation = true;
            }
            logger.info("integrationMap {}", integrationMap);

            //appending key value pairs in metaData object using streams.
            for (Map.Entry<String, Map> entry : integrationMap.entrySet()) {
                Map<String, String> eachValueMap = entry.getValue();
                logger.info("eachValueMap {}", eachValueMap);

                // replace tag value with corresponding respondent meta data info
                if (isNoteCreation) {
                    logger.info("Note Creation");
                    this.replaceHashTag(eachValueMap, eventMap, "Title");
                    this.replaceHashTag(eachValueMap, eventMap, "Body");
                }
                if (isTaskCreation) {
                    logger.info("Task creation");
                    this.replaceHashTag(eachValueMap, eventMap, "Subject");
                    this.replaceHashTag(eachValueMap, eventMap, "Comments");
                }
                Map<String, String> finalMetaData = metaData;
                eachValueMap.forEach((k, v) -> finalMetaData.merge(k, v, (oldval, newval) -> oldval));
                metaData = finalMetaData;
            }

            logger.info("metaData after {}", metaData);

        }catch (Exception e){
            e.printStackTrace();
            logger.error("exception at updateMetadataForMicrosoftDynamics. Msg {}", e.getMessage());
        }
        return metaData;
    }

    /**
     * method to get recipe name
     * @param integrationMap
     * @return
     */
    private String getRecipeName(Map integrationMap, String integrationName){
        String recipeType = "";
        if(integrationMap.containsKey(integrationName)) {
            Object recipeMap = integrationMap.remove(integrationName);
            if(recipeMap instanceof Map){
                Map recipeMap1 = (Map) recipeMap;
                if(recipeMap1.containsKey("recipeType")){
                    recipeType = recipeMap1.get("recipeType").toString();
                }
            }
        }
        if(integrationMap.containsKey("metaData"))
            integrationMap.remove("metaData");
        return recipeType;
    }

    /**
     * method to set reminder values if enabled
     * @param integrationMap
     * @param notifyTime
     * @param timezone
     * @param metaData
     * @return
     */
    private Map setReminder(Map integrationMap, String notifyTime, String timezone, Map metaData){
        metaData.put("Remind", "");
        if (integrationMap.containsKey("remind")) {
            Object remindMap = integrationMap.remove("remind");
            Map<String, Object> remindMap1 = (Map<String, Object>) remindMap;
            if (remindMap1.containsKey("enabled") && remindMap1.get("enabled").toString().equalsIgnoreCase("true")) {
                if (remindMap instanceof Map) {
                    String remindDate = this.getDateByCloseDateConfig((Map) remindMap, notifyTime, timezone, true);
                    metaData.put("Remind", remindDate);
                }
            }
        }
        return metaData;
    }

    /**
     * method to set date values in metadata
     * @param integrationMap
     * @param metaData
     * @param notifyTime
     * @param timezone
     * @param dateField
     * @param dateLabel
     * @param returnDateOnly - true if only date is required else false for date and time
     * @return
     */
    public Map setDateInMetaData(Map integrationMap, Map metaData, String notifyTime, String timezone, String dateField,
                                 String dateLabel, boolean returnDateOnly){
        String dateStr = "";
        if(integrationMap.containsKey(dateField)){
            Object closeDateMap = integrationMap.remove(dateField);
            if (closeDateMap instanceof Map) {
                dateStr = this.getDateByCloseDateConfig((Map) closeDateMap, notifyTime, timezone, returnDateOnly);
                metaData.put(dateLabel, dateStr);
            }
        } else {
            dateStr = this.getDateByCloseDateConfig(new HashMap(), notifyTime, timezone, returnDateOnly);
            metaData.put(dateLabel, dateStr);
        }
        return metaData;
    }

    /**
     * method to replace hastag values for given tag
     * @param eacIntegrationMap
     * @param eventMap
     * @param tagName
     * @return
     */
    private Map replaceHashTag(Map<String, String> eacIntegrationMap, Map eventMap, String tagName){
        if(eacIntegrationMap.containsKey(tagName) && eacIntegrationMap.get(tagName).contains("${")) {
            String tagValue = eacIntegrationMap.get(tagName);
            List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(tagValue)));
            if (tagsToReplace.size() > 0) {
                Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                eacIntegrationMap.remove(tagName);
                eacIntegrationMap.put(tagName, Utils.replaceTokens(tagValue, substituteTokens));
                logger.info(tagName +" {} ", Utils.replaceTokens(tagValue, substituteTokens));
            }
            logger.info("replaced hashTag {}", eacIntegrationMap);
        }
        return eacIntegrationMap;
    }

    /**
     * method to form  metadata object with Dt metadata for integrations trigger
     *
     * @param eventMap
     * @param integrationsConfigMap
     * @return
     */
    private Map getIntegrationMetadata(Map eventMap, Map integrationsConfigMap){
        Map<String, Object> metaData = new HashMap<>();
        try {
            Map dtMetadataMap = eventMap.containsKey("metaData") && Utils.isNotNull(eventMap.containsKey("metaData")) ? (Map) eventMap.get("metaData") : new HashMap();
            Map<String, String> salesforceMetadataMap = integrationsConfigMap.containsKey("metaData") && Utils.isNotNull(integrationsConfigMap.containsKey("metaData")) ? (Map<String, String>) integrationsConfigMap.get("metaData") : new HashMap();
            if (salesforceMetadataMap.size() > 0) {
                for (Map.Entry<String, String> entry : salesforceMetadataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = "";
                    String eachDtHeader = Utils.isNotNull(entry.getValue()) && Utils.notEmptyString(entry.getValue()) ? entry.getValue() : "";
                    if (Utils.notEmptyString(eachDtHeader)) {
                        value = dtMetadataMap.containsKey(eachDtHeader) && Utils.isNotNull(dtMetadataMap.get(eachDtHeader)) && Utils.notEmptyString(dtMetadataMap.get(eachDtHeader).toString())
                                ? dtMetadataMap.get(eachDtHeader).toString() : "";
                    }
                    metaData.put(key, value);
                }
                if (dtMetadataMap.containsKey("PRTNAME")) {
                    metaData.put("PRTNAME", Utils.isNotNull(dtMetadataMap.get("PRTNAME")) && Utils.notEmptyString(dtMetadataMap.get("PRTNAME").toString()) ?
                            dtMetadataMap.get("PRTNAME").toString() : "");
                }
            } else {
                metaData = dtMetadataMap;
            }
        }catch (Exception e){
            logger.error("exception at getIntegrationMetadata. Message {}", e.getMessage());
        }
        return metaData;
    }

    /**
     * update trigger status to inactive when the survey expires
     * SE-1091, DTV-7349 v2 version
     */
    public void updateExpiredSurveyV2(){
        logger.info("begin update expired surveys v2' trigger status");
        try {
            List businessList = triggerDAO.getAllBusiness();
            Iterator<Map> iterator = businessList.iterator();
            while (iterator.hasNext()) {
                try {
                    Map eachBusiness = iterator.next();
                    String businessUUID = eachBusiness.get(Constants.BUSINESS_UUID).toString();
                    triggerDAO.updateTriggerStatusForExpiredBusiness(businessUUID);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End update expired surveys v2' trigger status");

    }

    /**
     * method to form bambooHR metadata object with Dt metadata for bambooHR trigger
     */
    public Map updateMetadataForBambooHR(Map integrationsConfigMap, Map eventMap) {
        Map<String, String> metaData = new HashMap();
        try{
            /*Map dtMetadataMap = eventMap.containsKey("metaData") && Utils.isNotNull(eventMap.containsKey("metaData")) ? (Map) eventMap.get("metaData") : new HashMap();
            Map<String, String> bambooHRMetadataMap = integrationsConfigMap.containsKey("metaData") && Utils.isNotNull(integrationsConfigMap.containsKey("metaData")) ? (Map<String, String>) integrationsConfigMap.get("metaData") : new HashMap();
            if(bambooHRMetadataMap.size() > 0){
                for (Map.Entry<String, String> entry : bambooHRMetadataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = "";
                    String eachDtHeader = Utils.isNotNull(entry.getValue()) && Utils.notEmptyString(entry.getValue()) ? entry.getValue(): "";
                    if(Utils.notEmptyString(eachDtHeader)){
                        value = dtMetadataMap.containsKey(eachDtHeader) && Utils.isNotNull(dtMetadataMap.get(eachDtHeader)) && Utils.notEmptyString(dtMetadataMap.get(eachDtHeader).toString())
                                ? dtMetadataMap.get(eachDtHeader).toString() : "";
                    }
                    metaData.put(key, value);
                }
                if(dtMetadataMap.containsKey("PRTNAME")){
                    metaData.put("PRTNAME", Utils.isNotNull(dtMetadataMap.get("PRTNAME")) && Utils.notEmptyString(dtMetadataMap.get("PRTNAME").toString()) ?
                            dtMetadataMap.get("PRTNAME").toString() : "");
                }
            }else {
                metaData = dtMetadataMap;
            }*/
            metaData = this.getIntegrationMetadata(eventMap, integrationsConfigMap);

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            if(integrationMap.containsKey("bambooHR"))
                integrationMap.remove("bambooHR");
            if(integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");

            //appending key value pairs in metaData object using streams.
            /*for (Map.Entry<String, Map> entry : integrationMap.entrySet()) {
                Map<String, String> eachValueMap = entry.getValue();
                Map<String, String> finalMetaData = metaData;
                eachValueMap.forEach((k, v) -> finalMetaData.merge(k, v, (oldval, newval) -> oldval));
                metaData = finalMetaData;
            }*/
            metaData = appendKeyValuesToMetadata(integrationMap, metaData);
            logger.info("metaData after {}", metaData);

        }catch (Exception e){
            e.printStackTrace();
            logger.error("exception at updateMetadataForMicrosoftDynamics. Msg {}", e.getMessage());
        }
        return metaData;
    }

    /**
     * method to form slack metadata object with DT metadata for slack trigger
     *
     * @param integrationsConfigMap
     * @param eventMap
     * @return
     */
    public Map updateMetadataForSlack(Map<String, Map> integrationsConfigMap, Map eventMap) {
        Map<String, Object> metaData = new HashMap();
        try {
            /*Map dtMetadataMap = eventMap.containsKey("metaData") &&
                    Utils.isNotNull(eventMap.containsKey("metaData")) ? (Map) eventMap.get("metaData") : new HashMap();
            Map<String, String> slackMetadataMap = integrationsConfigMap.containsKey("metaData") &&
                    Utils.isNotNull(integrationsConfigMap.containsKey("metaData")) ? (Map<String, String>) integrationsConfigMap.get(
                    "metaData") : new HashMap();
            if (slackMetadataMap.size() > 0) {
                for (Map.Entry<String, String> entry : slackMetadataMap.entrySet()) {
                    String key = entry.getKey();
                    String value = "";
                    String eachDtHeader = Utils.isNotNull(entry.getValue()) &&
                            Utils.notEmptyString(entry.getValue()) ? entry.getValue() : "";
                    if (Utils.notEmptyString(eachDtHeader)) {
                        value = dtMetadataMap.containsKey(eachDtHeader) &&
                                Utils.isNotNull(dtMetadataMap.get(eachDtHeader)) &&
                                Utils.notEmptyString(dtMetadataMap.get(eachDtHeader).toString())
                                ? dtMetadataMap.get(eachDtHeader).toString() : "";
                    }
                    metaData.put(key, value);
                }
                if (dtMetadataMap.containsKey("PRTNAME")) {
                    metaData.put("PRTNAME",
                            Utils.isNotNull(dtMetadataMap.get("PRTNAME")) &&
                                    Utils.notEmptyString(dtMetadataMap.get("PRTNAME").toString()) ?
                                    dtMetadataMap.get("PRTNAME").toString() : "");
                }
            } else {
                metaData = dtMetadataMap;
            }*/
            metaData = this.getIntegrationMetadata(eventMap, integrationsConfigMap);
            String recipeType = getRecipeName(integrationsConfigMap, Constants.CHANNEL_SLACK);
            /*if (integrationsConfigMap.containsKey("slack")) {
                Object recipeMap = integrationsConfigMap.remove("slack");
                if (recipeMap instanceof Map) {
                    Map recipeMap1 = (Map) recipeMap;
                    if (recipeMap1.containsKey("recipeType")) {
                        recipeType = recipeMap1.get("recipeType").toString();
                    }
                }
            }*/

            if (recipeType.equalsIgnoreCase("notify")) {
                //appending key value pairs in metaData object using streams.
                for (Map.Entry<String, Map> entry : integrationsConfigMap.entrySet()) {
                    Map<String, String> eachValueMap = entry.getValue();
                    logger.info("eachValueMap {}", eachValueMap);
                    // replace tag value with corresponding respondent metadata info
                   /* if (eachValueMap.containsKey("message") && eachValueMap.get("message").contains("${")) {
                        String message = eachValueMap.get("message");
                        List tagsToReplace = new ArrayList(new HashSet<>(Utils.getTokens(message)));
                        if (tagsToReplace.size() > 0) {
                            Map substituteTokens = this.computeParticipantSubstituteTokens(eventMap, tagsToReplace);
                            eachValueMap.replace("message", message, Utils.replaceTokens(message, substituteTokens));
                        }
                    }*/
                    this.replaceHashTag(eachValueMap, eventMap, "message");
                }
            }

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            if (integrationMap.containsKey("slack"))
                integrationMap.remove("slack");
            if (integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");

            //appending key value pairs in metaData object using streams.
           /* for (Map.Entry<String, Map> entry : integrationMap.entrySet()) {
                Map<String, Object> finalEachValueMap = entry.getValue();
                Map<String, Object> finalMetaData = metaData;
                finalEachValueMap.forEach((k, v) -> finalMetaData.merge(k, v, (oldval, newval) -> oldval));
                metaData = finalMetaData;
            }*/
            metaData = appendKeyValuesToMetadata(integrationMap, metaData);
            logger.info("metaData after {}", metaData);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at updateMetadataForSlack. Msg {}", e.getMessage());
        }
        return metaData;
    }

    public Map appendKeyValuesToMetadata(Map<String, Map> integrationMap, Map metaData){
        //appending key value pairs in metaData object using streams.
        for (Map.Entry<String, Map> entry : integrationMap.entrySet()) {
            Map<String, Object> finalEachValueMap = entry.getValue();
            Map<String, Object> finalMetaData = metaData;
            finalEachValueMap.forEach((k, v) -> finalMetaData.merge(k, v, (oldval, newval) -> oldval));
            metaData = finalMetaData;
        }
        return metaData;
    }


    /**
     * DTV-8038
     * Method to add metric/text analytics categories to the BCAE
     *
     * @param triggerNotification
     */
    public void sendTriggerFocusArea(TriggerNotification triggerNotification) {
        try {
            logger.info("send trigger focus area - starts");
            int updateResult = 0;

            /**
             * Focus metrics, categories are at survey level not for each schedule
             * Example : For Trigger 1, if there are 2 schedules
             * For schedule 1, If condition satisfies then adding Metric/Category for that survey in focus_metric_{buuid} table
             * For schedule 2, If condition not satisfies then removing Metric/Category for that survey in focus_metric_{buuid} table
             */
            int businessId = triggerNotification.getBusinessId();
            int surveyId = triggerNotification.getSurveyId();
            int notifyId = triggerNotification.getNotifyId();
            int scheduleId = Integer.parseInt(triggerNotification.getScheduleId());

            //If conditionMet actionType = "insert" else actionType = "delete"
            String actionType = Utils.isNotNull(triggerNotification.getActionType()) ? triggerNotification.getActionType() : "";

            String businessUniqueId = triggerDAO.getBusinessUniqueIdById(businessId);

            //surveyUUID
            Map surveyMap = triggerDAO.getSurveyDetailById(surveyId, businessUniqueId);
            int userId = surveyMap.containsKey("created_by") && Utils.isNotNull(surveyMap.get("created_by")) ? Integer.parseInt(surveyMap.get("created_by").toString()) : 0;

            //get schedule integrations_config
            Map scheduleMap = triggerDAO.getTriggerScheduleById(scheduleId);
            //DTV-8038
            Map integrationsConfigMap = scheduleMap.containsKey("integrations_config") && Utils.isNotNull(scheduleMap.get("integrations_config")) ? mapper.readValue(scheduleMap.get("integrations_config").toString(), HashMap.class) : new HashMap();

            //check for metric
            List<String> metrics = integrationsConfigMap.containsKey("metric") && Utils.isNotNull(integrationsConfigMap.get("metric")) ? (List) integrationsConfigMap.get("metric"): new ArrayList<>();
            List<String> categories = integrationsConfigMap.containsKey("categories") && Utils.isNotNull(integrationsConfigMap.get("categories")) ? (List) integrationsConfigMap.get("categories"): new ArrayList<>();

            //get triggered recommended focus metric by survey_id
            Map focusMetricMap = triggerDAO.getTriggerFocusMetricBySurveyId(surveyId, businessUniqueId);

            List<String> focusMetric;
            List<String> focusMetricType;

            //DTV-8038, FocusMetric already exists for the surveyId
            if(focusMetricMap.size() > 0 && Utils.notEmptyString(actionType)){
                focusMetric = focusMetricMap.containsKey("focus_metric") && Utils.isNotNull(focusMetricMap.get("focus_metric")) ? mapper.readValue(focusMetricMap.get("focus_metric").toString(), ArrayList.class) : new ArrayList<>();
                focusMetricType = focusMetricMap.containsKey("focus_metric_type") && Utils.isNotNull(focusMetricMap.get("focus_metric_type")) ? mapper.readValue(focusMetricMap.get("focus_metric_type").toString(), ArrayList.class) : new ArrayList<>();
                int focusMetricId = focusMetricMap.containsKey("focus_metric_id") && Utils.isNotNull(focusMetricMap.get("focus_metric_id")) ? Integer.parseInt(focusMetricMap.get("focus_metric_id").toString()) : 0;

                /**
                 * If actionType is insert, then appending new metrics/categories to the existing focusMetric
                 */
                if(actionType.equals(Constants.ACTION_TYPE_INSERT)){
                    metrics.stream().forEach(eachMetric -> {
                        if(!focusMetric.contains(eachMetric)){
                            focusMetric.add(eachMetric);
                            focusMetricType.add("Metric");
                        }
                    });
                    categories.stream().forEach(eachCategory -> {
                        if(!focusMetric.contains(eachCategory)){
                            focusMetric.add(eachCategory);
                            focusMetricType.add("Category");
                        }
                    });
                }

                /**
                 * If actionType is delete, then removing trigger schedule metrics/categories present in the existing focusMetric
                 */
                if(actionType.equals(Constants.ACTION_TYPE_DELETE)){
                    for(String eachMetric : metrics){
                        if(focusMetric.contains(eachMetric)){
                            int index = focusMetric.indexOf(eachMetric);
                            focusMetric.remove(eachMetric);
                            if(index > -1)
                                focusMetricType.remove(index);
                        }
                    }
                    for(String eachCategory : categories){
                        if(focusMetric.contains(eachCategory)){
                            int index = focusMetric.indexOf(eachCategory);
                            focusMetric.remove(eachCategory);
                            if(index > -1)
                                focusMetricType.remove(index);
                        }
                    }
                }
                final String finalFocusMetric = focusMetric.size() == 0 || focusMetric == null ? new JSONArray().toString() : new JSONArray(focusMetric).toString();
                final String finalfocusMetricType = focusMetricType.size() == 0 || focusMetricType == null ? new JSONArray().toString() : new JSONArray(focusMetricType).toString();
                //updating the focusMetric by surveyId in focus_metric_{buuid} table
                updateResult = triggerDAO.updateTriggerFocusMetricBySurveyId(surveyId, businessUniqueId, finalFocusMetric, finalfocusMetricType, userId, focusMetricId);
            }
            /**
             * If focusArea doesn't exist for the surveyId
             * then check if the actionType is insert only then create a new entry in focus_metric_{buuid} table (extra check to avoid duplicate entries)
             */
            else if(focusMetricMap.size() == 0 && actionType.equals(Constants.ACTION_TYPE_INSERT)){
                focusMetricType = new ArrayList<>();
                focusMetric = new ArrayList<>();
                if(metrics.size() > 0){
                    focusMetric.addAll(metrics);
                    focusMetricType.addAll(Collections.nCopies(metrics.size(), "Metric"));
                }
                if(categories.size() > 0){
                    focusMetric.addAll(categories);
                    focusMetricType.addAll(Collections.nCopies(categories.size(), "Category"));
                }
                //inserting the focusMetric by surveyId in focus_metric_{buuid} table
                updateResult = triggerDAO.insertTriggerFocusMetricBySurveyId(surveyId, businessUniqueId, focusMetric, focusMetricType, userId);
            }

            //update status to sent once email sent successfully
            logger.info("updateResult  {}", updateResult);
            if (updateResult > 0){
                logger.info("update notification");
                // triggerDAO.updateNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED);
                //DTV-6955, update sent status to completed in trigger_notification_{buuid} table
                triggerDAO.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_COMPLETED, businessUniqueId);
                //DTV-6955, Delete the notification in trigger_notification table by notifyId
                triggerDAO.deleteTriggerNotificationById(notifyId);
            }
            else {
                //if fails to send, needs to remove the record from trigger_notification. So that it will trigger again.
            }
            logger.info("send trigger focus area - ends");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * DTV-8038, Check if focusArea exists for the surveyId
     * @param surveyId
     * @param businessUUID
     * @param triggerSchedule
     * @return
     */
    public void processFocusAreaSchedule(int surveyId, String businessUUID, Boolean isAggregateConditionMatched, String aggregateConditionFailedToken, List<String> tokensList, TriggerSchedule triggerSchedule, TriggerNotification triggerNotification){
        try{
            Map focusMetricMap = triggerDAO.getTriggerFocusMetricBySurveyId(surveyId, businessUUID);
            logger.info("focusArea case");

            //DTV-8038
            Map integrationsConfigMap = Utils.isNotNull(triggerSchedule.getIntegrationsConfig()) ? triggerSchedule.getIntegrationsConfig() : new HashMap();

            String aggregateConditionMatchedToken = tokensList.size() > 0 ? tokensList.get(tokensList.size()-1) : "";
            String submissionToken = isAggregateConditionMatched ? aggregateConditionMatchedToken : aggregateConditionFailedToken;
            logger.info("submissionToken  {}", submissionToken);

            //check for metric
            List<String> metrics = integrationsConfigMap.containsKey(Constants.METRIC) && Utils.isNotNull(integrationsConfigMap.get(Constants.METRIC)) ? (List) integrationsConfigMap.get(Constants.METRIC): new ArrayList<>();
            List<String> categories = integrationsConfigMap.containsKey(Constants.CATEGORIES) && Utils.isNotNull(integrationsConfigMap.get(Constants.CATEGORIES)) ? (List) integrationsConfigMap.get(Constants.CATEGORIES): new ArrayList<>();

            /**
             * If focusArea exist for the surveyId
             * Case 1 : If aggregate condition is matched
             *          If schedule metrics/categories NOT present in the focusArea for the survey --> it will create new notification with actionType --> insert
             *          If schedule metrics/categories present in the focusArea for the survey --> it will not create new notification
             *
             * Case 2 : If aggregate condition is NOT matched
             *          If schedule metrics/categories present in the focusArea for the survey --> it will create new notification with actionType --> delete
             *          If schedule metrics/categories NOT present in the focusArea for the survey --> it will not create new notification
             */
            if(focusMetricMap.size() > 0) {
                logger.info("focusArea exist for the survey");
                List<String> focusMetric = focusMetricMap.containsKey("focus_metric") && Utils.isNotNull(focusMetricMap.get("focus_metric")) ? mapper.readValue(focusMetricMap.get("focus_metric").toString(), ArrayList.class) : new ArrayList<>();
                Boolean createNotifcation = false;

                if(isAggregateConditionMatched){
                    for(String eachMetric : metrics){
                        if(!focusMetric.contains(eachMetric)){
                            createNotifcation = true;
                            break;
                        }
                    }
                    for(String eachCategory : categories){
                        if(!focusMetric.contains(eachCategory)){
                            createNotifcation = true;
                            break;
                        }
                    }
                }

                if(!isAggregateConditionMatched){
                    for(String eachMetric : metrics){
                        if(focusMetric.contains(eachMetric)){
                            createNotifcation = true;
                            break;
                        }
                    }
                    for(String eachCategory : categories){
                        if(focusMetric.contains(eachCategory)){
                            createNotifcation = true;
                            break;
                        }
                    }
                }

                if(createNotifcation && Utils.notEmptyString(submissionToken)) {
                    String actionType = isAggregateConditionMatched ? Constants.ACTION_TYPE_INSERT : Constants.ACTION_TYPE_DELETE;
                    triggerNotification.setActionType(actionType);
                    //adding latest submission token to the triggerNotification for tracking purpose
                    triggerNotification.setSubmissionToken(submissionToken);
                    String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                    //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                    triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUUID);
                }
            }
            /**
             * If focusArea doesn't exist for the surveyId
             * then check if the aggregate condition is matched only then create a new entry in focus_metric_{buuid} table
             */
            else if (focusMetricMap.size() == 0 && isAggregateConditionMatched && Utils.notEmptyString(submissionToken)) {
                logger.info("focusArea doest not exist for the survey");
                //adding latest submission token to the triggerNotification for tracking purpose
                triggerNotification.setSubmissionToken(submissionToken);
                triggerNotification.setActionType(Constants.ACTION_TYPE_INSERT);
                String triggerNotificationUUID = triggerDAO.createTriggerNotification(triggerNotification);
                //DTV-6955, create a new entry in trigger_notification_buuid by triggerNotificationUUID
                triggerDAO.createTriggerNotificationInBusinessUUIDTable(triggerNotificationUUID, businessUUID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method to compute datetime with interval values in days based on the intervalType for salesforce create oppurtinity
     *
     * close date format mm/dd/yyyy
     * @param closeDateConfig
     * @param startTime
     * @param timezone
     * @param retuenDateOnly - true if return date only else return date with time
     * @return
     */
    public String getDateByCloseDateConfig(Map closeDateConfig, String startTime, String timezone, boolean retuenDateOnly) {
        logger.info("computegetDateByCloseDateConfig");
        String formattedDate = "";
        try {
            /**
             * "closeDate": {
             * 		"interval": "01",
             * 		"intervalType": "hours", //minutes, hours, days, weeks, months, years
             * 		"immediate": false
             *  }
             *  "closeDate": {
             * 		"interval": ""
             * 		"intervalType": ""
             * 		"immediate": true
             *        },
             */
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD_HH_MM_SS);

            String notifyTime = Utils.getDateAsStringByTimezone(startTime, timezone);

            Date newDateString = sdf.parse(notifyTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDateString);

            int interval = closeDateConfig.containsKey("interval") && Utils.isNotNull(closeDateConfig.get("interval")) ? Integer.parseInt(closeDateConfig.get("interval").toString()) : 0;
            String intervalType = closeDateConfig.containsKey("intervalType") && Utils.isNotNull(closeDateConfig.get("intervalType")) ? closeDateConfig.get("intervalType").toString() : "";

            if(interval > 0 && Utils.notEmptyString(intervalType)) {
                logger.info("interval {} intervalType {}", interval, intervalType);
                // Delayed notifications
                switch (intervalType) {
                    case "minutes":
                        calendar.add(Calendar.MINUTE, interval);
                        break;
                    case "hours":
                        calendar.add(Calendar.HOUR, interval);
                        break;
                    case "days":
                        calendar.add(Calendar.DATE, interval);
                        break;
                    case "weeks":
                        calendar.add(Calendar.WEEK_OF_MONTH, interval);
                        break;
                    case "months":
                        calendar.add(Calendar.MONTH, interval);
                        break;
                    default:
                        break;
                }
            }

            String resultDate = sdf.format(calendar.getTime());
            formattedDate = retuenDateOnly ? resultDate.length() > 10 ? resultDate.substring(0, 10) : resultDate : resultDate;
            //formattedDate = resultDate.length() > 10 ? resultDate.substring(0, 10) : resultDate;

        } catch (Exception e) {
            logger.error("exception at getDateByCloseDateConfig. Msg {}", e.getMessage());
        }
        return  formattedDate;

    }

    /**
     * Method to get the focus area action message based on integrationsConfig & focusAreaConfig
     *
     * @param businessId
     * @param integrationsConfig
     * @return
     */
    public String getFocusAreaActionMessage(int businessId, Map integrationsConfig) {
        logger.info("starts getFocusAreaActionMessage");
        String actionMessage = "";
        try {
            List businessConfigList = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);
            Map configurationsMap = (Map) businessConfigList.get(0);
            Map focusMetricMap = configurationsMap.containsKey("focusMetric") && Utils.isNotNull(configurationsMap.get("focusMetric")) ? mapper.readValue(configurationsMap.get("focusMetric").toString(), Map.class) : new HashMap();

            int focusMetric = focusMetricMap.containsKey("focusMetric") && Utils.isNotNull(focusMetricMap.get("focusMetric")) ? Integer.parseInt(focusMetricMap.get("focusMetric").toString()) : 0;
            int userDefinedRecommendation = focusMetricMap.containsKey("userDefinedRecommendation") && Utils.isNotNull(focusMetricMap.get("userDefinedRecommendation")) ? Integer.parseInt(focusMetricMap.get("userDefinedRecommendation").toString()) : 0;

            List metrics = integrationsConfig.containsKey("metric") && Utils.isNotNull(integrationsConfig.get("metric")) ? (List) integrationsConfig.get("metric") : new ArrayList();
            List categories = integrationsConfig.containsKey("categories") && Utils.isNotNull(integrationsConfig.get("categories")) ? (List) integrationsConfig.get("categories") : new ArrayList();


            /**
             * userDefinedRecommendation -> 0 (User defined recommendation is turned off)
             * userDefinedRecommendation -> 1 (User defined recommended focus areas)
             * userDefinedRecommendation -> 2 (User defined recommended focus areas + Impact calculation)
             */

            StringBuilder sb = new StringBuilder();
            if(focusMetric == 1 ){
                if(metrics.size() > 0){
                    sb.append("You will find \"")
                            .append(metrics.size() == 1 ? (String) metrics.stream().collect(Collectors.joining("", "", "\" metric")) : (String) metrics.stream().collect(Collectors.joining(", ", "", "\" metrics")))
                            .append(categories.size() > 0 ? " and the selected categories" : "")
                            .append(" as recommended focus area under Report's \"Action planning\" tab. ")
                            .append(userDefinedRecommendation == 1 ? "Immediately when the trigger condition is met." :
                                    userDefinedRecommendation == 2 ? "Immediately when the trigger condition and impact calculation score for the correlation of \""
                                            + (metrics.size() == 1 ? (String) metrics.stream().collect(Collectors.joining("", "", "\""))
                                            : (String) metrics.stream().collect(Collectors.joining(", ", "", "\""))) + " & preferred metric are met." : "");
                }
                else if(metrics.size() == 0 && categories.size() > 0){
                    sb.append(userDefinedRecommendation == 1 ? "You will find the selected categories as recommended focus area under Report's \"Action planning\" tab. Immediately when the trigger condition is met." :
                            userDefinedRecommendation == 2 ? "You will find the selected categories as recommended focus area under Report's \"Action planning\" tab. Immediately when the trigger condition " +
                            "and impact calculation score for the correlation of the selected categories & preferred metric are met." : "");
                }
            }
            actionMessage = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End getFocusAreaActionMessage");
        return  actionMessage;

    }

    /**
     * method to construct query for file upload question conditions
     * @param conditionVal
     * @param questionIndex
     * @return
     */
    public String getFileTypeSubQuery(String conditionVal, String questionIndex) {
        StringBuilder subQuery = new StringBuilder();
        /***
         * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.status=upload
         * output : json_extract(data,'$[1]') != ''
         * input : question.996b9f70-bcc8-47c0-9517-0bf43b4a95c5.cf546602-1f70-4e1c-9b91-58d0f10ff9c6.status=withoutupload
         * output : json_extract(data,'$[1][0]') = ''
         *
         */
        subQuery.append(" json_extract(data,'$[" + questionIndex + "][0]')").
                append( conditionVal.equals(Constants.UPLOAD) ? " !=''" : " =''" );
        return subQuery.toString();
    }

    /**
     * Method to update trigger schedule exec_start_time & exec_end_time by triggerId after turning on trigger
     * This method is useful to update exec_start & end dates immediately after turning on trigger otherwise it will wait till scheduler to pick and update.
     * @param businessUniqueId
     * @param triggerUUID
     * @return
     */
    public int updateTriggerScheduleExecStartEndTimeByTriggerId(String businessUniqueId, String triggerUUID) {
        logger.info("begin updating the trigger schedule exec start & end date after turning the trigger status on");
        int result = 0;
        try {

            List triggerSchedules = triggerDAO.getTriggerScheduleByTriggerID_V2(triggerUUID, businessUniqueId);
            Iterator iterator = triggerSchedules.iterator();
            while(iterator.hasNext()) {
                Map eachScheduleMap = (Map) iterator.next();

                String startDate = eachScheduleMap.getOrDefault(Constants.START_DATE, Constants.EMPTY).toString();
                String startTime = eachScheduleMap.getOrDefault(Constants.START_TIME, Constants.EMPTY).toString();
                String endDate = eachScheduleMap.getOrDefault(Constants.END_DATE, Constants.EMPTY).toString();
                String endTime = eachScheduleMap.getOrDefault(Constants.END_TIME, Constants.EMPTY).toString();
                String scheduleUUID = eachScheduleMap.getOrDefault(Constants.SCHEDULE_UUID, Constants.EMPTY).toString();
                String timezone = eachScheduleMap.getOrDefault(Constants.TIMEZONE, Constants.EMPTY).toString();
                String surveyEndDate = eachScheduleMap.containsKey(Constants.TO_DATE) ? (eachScheduleMap.get(Constants.TO_DATE).toString()) : Constants.EMPTY;

                /**
                 * CSUP-1657
                 * Converting the startDateTime, endDateTime to localTimeZone to consider DST also
                 * SurveyEndDate is already present in UTC
                 */
                String localSurveyEndDate = Utils.getLocalDateTimeByTimezone(surveyEndDate, timezone);
                String localStartDate = Constants.EMPTY;
                String localStartTime = Constants.EMPTY;
                String localEndDate = Constants.EMPTY;
                String localEndTime = Constants.EMPTY;

                if(Utils.notEmptyString(startDate) && Utils.notEmptyString(startTime)) {
                    String startDateTime = Utils.getLocalDateTimeByTimezone(startDate + " " + startTime, timezone);
                    localStartDate = startDateTime.substring(0, 10);
                    localStartTime = startDateTime.substring(11, 19);
                }

                if(Utils.notEmptyString(endDate) && Utils.notEmptyString(endTime)) {
                    String endDateTime = Utils.getLocalDateTimeByTimezone(endDate + " " + endTime, timezone);
                    localEndDate = endDateTime.substring(0, 10);
                    localEndTime = endDateTime.substring(11, 19);
                }

                //get execution start & end dates to update all the schedules for the given schedule
                Map datesMap = getExecutionStartEndTime(localStartDate, localEndDate, localStartTime, localEndTime, localSurveyEndDate,  timezone);
                String executionStartTime = datesMap.getOrDefault(Constants.EXEC_START_TIME,Constants.EMPTY).toString();
                String executionEndTime = datesMap.getOrDefault(Constants.EXEC_END_TIME,Constants.EMPTY).toString();

                /**
                 * update the execution time based upon the schedule uuid
                 */
                if(!executionStartTime.isEmpty() && !executionEndTime.isEmpty())
                    updateExecTimeByUUID(executionStartTime,executionEndTime, scheduleUUID);
            }
            logger.info("end retrieving the trigger details by triggerID");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Method to get open ended keywords responses subquery
     * @param questionData
     * @param conditionValue
     * @param questionIdx
     * @param categoryType
     * @return
     */
    public String getOpenEndedKeywordResponsesSubQuery(Map questionData, String conditionValue, String questionIdx, String categoryType) {

        StringBuilder str = new StringBuilder();
        try {
            /**
             * data value from event_table: ["naveen kumar check for keyword trigge", "open shorttt question check", ["moe first question chek", "moe2 check", "moe3 first question check"], 8]
             * event_data from event_table: [3702, 3703, 3704, 3720]
             * questionsGroup from event_table: ["", "", [3705, 3706, 3707], ""]  // subQuestionIds
             **/

            /***
             * input : question.4d99a3c4-7b10-4fc7-af6f-29727a82f8bd.75bb54d7-776a-48dd-9d66-26cd8ffcdf06.responses=naveen,kumar,keywords,check,trigger
             * output : (
             *     data->'$[0]' LIKE '%naveen%'
             *     OR data->'$[0]' LIKE '%kumar1%'
             * )
             *
             * input2 : question.4d99a3c4-7b10-4fc7-af6f-29727a82f8bd.75bb54d7-776a-48dd-9d66-26cd8ffcdf06.subquestionUUID+responses=naveen,kumar
             * output : (
             *      data -> '$[questionIdx][subquestionIdx]' like "%naveen%"
             *      or data -> '$[questionIdx][subquestionIdx]' like "%kumar%"
             *      )
             */

            //conditionValue = naveen,kumar,keywords,check,trigger, removing unnecessary empty string , sepcial chars, remove duplicates also
            String[] keywords = Arrays.asList(conditionValue.split(",")).stream().filter(Utils::notEmptyString).map(String::trim).distinct().toArray(String[]::new);

            String type = questionData.containsKey("type") ? questionData.get("type").toString() : "";
            if(type.equalsIgnoreCase(Constants.OPEN_ENDED)){
                str.append(" ( ");
                for(int i=0; i<keywords.length; i++){
                    //DTV-11296 -  Escape special characters by doubling them
                    String escapedKeyword = keywords[i]
                            .replace("'", "''")
                            .replace("\"", "\"\"")
                            .replace("%", "%%")
                            .replace("_", "__")
                            .replace("\\", "\\\\")
                            .replace("\r", "\\r")
                            .replace("\n", "\\n");
                    if(i == keywords.length-1){
                        str.append(" data->'$[" + questionIdx + "]' LIKE '%" + escapedKeyword + "%' ");
                    }else{
                        str.append(" data->'$[" + questionIdx + "]' LIKE '%" + escapedKeyword + "%' OR ");
                    }
                }
                str.append(" ) ");
            }else if(type.equalsIgnoreCase(Constants.MULTI_OPENENDED)){
                /**categoryType value will be:- subquestion_uuid+sentiment / subquestion_uuid+contains /
                 * subquestion_uuid+donotcontain / subquestion_uuid+category /
                 * subquestion_uuid+status / subquestion_uuid+status
                 * subquestion_uuid+responses
                 */
                String[] multiOpenTypeArr = categoryType.split("\\+");
                String subQuestionId = multiOpenTypeArr[0];

                //get the list of subquestion uniqueIds
                List subQuestionIds =  questionData.containsKey("subQuestionUniqueIds") ?
                        (List) questionData.get("subQuestionUniqueIds") : new ArrayList();
                //get the index of subQuestionId from the list
                int subQuestionIdx = subQuestionIds.indexOf(subQuestionId);
                if(subQuestionIdx > -1) {
                    // start building the query with subQuestionIdx
                    str.append(" ( ");
                    for (int i = 0; i < keywords.length; i++) {
                        //DTV-11296 -  Escape special characters by doubling them
                        String escapedKeyword = keywords[i]
                                .replace("'", "''")
                                .replace("\"", "\"\"")
                                .replace("%", "%%")
                                .replace("_", "__")
                                .replace("\\", "\\\\")
                                .replace("\r", "\\r")
                                .replace("\n", "\\n");
                        if (i == keywords.length - 1) {
                            str.append(" data->'$[" + questionIdx + "][" + subQuestionIdx + "]' LIKE '%" + escapedKeyword + "%' ");
                        } else {
                            str.append(" data->'$[" + questionIdx + "][" + subQuestionIdx + "]' LIKE '%" + escapedKeyword + "%' OR ");
                        }
                    }
                    str.append(" ) ");
                }
            }
            logger.info("getMatrixRatingQuestionSubQuery --- {}", str.toString());
        }catch (Exception e){
            e.printStackTrace();
            logger.error("getMatrixRatingQuestionSubQuery --- {}", e.getMessage());
        }
        return str.toString();
    }


    /**
     * rest api call to dtworks
     * @param uri
     * @param inputMap
     * @param method
     * @return
     */
    public Map callInishtsRestApi(String uri, Map<String, Object> inputMap, HttpMethod method){

        logger.info("begin insights rest api");
        Map respMap = new HashMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject jsonObject = new JSONObject(inputMap);
        String inputJson = jsonObject.toString();
        logger.info("input json {}", inputJson);
        logger.info("uri {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        try {

            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    logger.info("success response");
                }
            }else{
                respMap.put("success", false);
            }
            logger.info("end insights rest api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 500);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 500);
            e.printStackTrace();
        }
        return  respMap;
    }


    public Trigger getTriggerById(String triggerId, String businessUniqueId, String timezone, String userId) {
        logger.info("begin retrieving the trigger details by triggerID");
        Trigger trigger = null;
        try {
            trigger = triggerDAO.getTriggerById(triggerId, businessUniqueId, timezone, userId);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("end retrieving the trigger details by triggerID");
        return trigger;
    }

    /**
     * Function to form readable query from filterquery
     *
     * @param filterQuery
     * @return
     */
    private Map changedQueries(String filterQuery, String businessUUID) {
        String sqlWhereMetaData = "";
        String sqlWhereQuestion = "";
        String sqlWhereQuestionTemp = "(";
        String sqlWhereMetaDataFinal = "";
        String sqlWhereQuestionFinal = "";
        String sqlWhereOpen = "";
        Map sqlWhereOpenMap = new HashMap();
        try {
            if (filterQuery.length() > 1) {
                String[] subQuerries = filterQuery.split("&&|\\|\\|");
                List conditionList = new ArrayList();
                List metaDataList = new ArrayList();
                List queries = new ArrayList();
                List operators = new ArrayList();
                List operatorList = new ArrayList<>();
                List valuesList = new ArrayList();
                Map operatorMap = new HashMap();
                List metadataQueries = new ArrayList();
                operatorMap.put("&&", "and");
                operatorMap.put("||", "or");
                List questionList = new ArrayList();
                String concatMetadataValueString = "";
                for (int i = 0; i < subQuerries.length; i++) {
                    logger.info(subQuerries[i]);
                    queries.add(subQuerries[i]);
                    if (!subQuerries[i].contains("question")) {
                        metadataQueries.add(subQuerries[i]);
                        String[] splitQuery = subQuerries[i].split("\\.");
                        logger.info("splitQuery = " + splitQuery);
                        logger.info("split query length = " + splitQuery.length);
                        if (subQuerries[i].contains("dtlink") || subQuerries[i].contains("dtkiosk") || subQuerries[i].contains("dtqr")) {
                            metaDataList.add(subQuerries[i].split("\\.")[3].split("=|!=|>|<|<=|>=")[0]);
                            valuesList.add(subQuerries[i].split("\\.")[3].split("=|!=|>|<|<=|>=")[1]);
                        } else {
                            if (splitQuery.length > 3) {
                                for (int j = 2; j < splitQuery.length; j++) {
                                    concatMetadataValueString = concatMetadataValueString + splitQuery[j];
                                }
                                logger.info("concatMetadataValue = " + concatMetadataValueString);
                                metaDataList.add(concatMetadataValueString.split("=|!=|>|<|<=|>=")[0] + ".");
                                valuesList.add(concatMetadataValueString.split("=|!=|>|<|<=|>=")[1]);
                            } else {
                                metaDataList.add(subQuerries[i].split("\\.")[2].split("=|!=|>|<|<=|>=")[0]);
                                valuesList.add(subQuerries[i].split("\\.")[2].split("=|!=|>|<|<=|>=")[1]);
                            }
                        }
                    } else {
                        questionList.add(subQuerries[i]);
                    }
                    conditionList.add(subQuerries[i].split("\\.")[2]);
                    filterQuery = filterQuery.replaceFirst(Pattern.quote(subQuerries[i]), "");//Since replaceFirst's 1st parameter is assumed to be a regex, we need to escape special characters. Using Pattern.quote(string) to escape it.
                    concatMetadataValueString = "";
                }

                for (int k = 0; k < filterQuery.length(); k = k + 2) {
                    if (k + 2 <= filterQuery.length()) {
                        String eachOperator = filterQuery.substring(k, k + 2);
                        operators.add(eachOperator);
                    }
                }
                logger.info("The value in metaDataList = " + metaDataList);
                logger.info("The value in operators = " + operators);
                logger.info("The value in queries = " + queries);
                logger.info("The value in conditionList = " + conditionList);
                operatorList.addAll(operators);
                Iterator<String> queryIterator = queries.iterator();
                Iterator<String> operatorIterator = operatorList.iterator();
                Iterator<String> metaDataIterator = metaDataList.iterator();
                logger.info("The value in operatorList = " + operatorList);
                int i = 0;
                int j = 1;
                boolean in = true;// this variable used to add opening and closing brackets
                boolean isDate = false;
                while (queryIterator.hasNext()) {
                    int k = i;
                    String eachQuery = queryIterator.next();
                    String eachOperator = "";
                    if (operatorIterator.hasNext()) {
                        eachOperator = operatorIterator.next();
                    }
                    if (!eachQuery.contains("question")) {
                        logger.info("The value in eachQuery = " + eachQuery);
                        if (in && j < metaDataList.size() && (metaDataList.get(j).equals(metaDataList.get(j - 1)))) {//Check whether metadata and its next metadata are same to appemd opening brackets
                            sqlWhereMetaData = sqlWhereMetaData + "( ";
                            in = false;
                        }
                        String eachMysqlQueryMetaData = getFilterSubQueryNewAdvFilters(eachQuery);
                        logger.info("The value in eachMysqlQueryMetaData = " + eachMysqlQueryMetaData);
                        if (Utils.isNotNull(eachMysqlQueryMetaData) && eachMysqlQueryMetaData.length() > 0) {
                            sqlWhereMetaData = sqlWhereMetaData + eachMysqlQueryMetaData;
                            if (!in && i < metaDataList.size() - 1 && !(metaDataList.get(i).equals(metaDataList.get(i + 1)))) {//if two metadatas are different then append closing brackets
                                sqlWhereMetaData = sqlWhereMetaData + " )";
                                in = true;
                            }
                            if (Utils.notEmptyString(eachOperator)) {
                                logger.info("The value in eachOperator = " + eachOperator);
                                if (k + 1 < metaDataList.size() && metaDataList.get(k).equals(metaDataList.get(k + 1))) {
                                    logger.info("valuesList.get(k).toString(),valuesList.get(k+1).toString()" + valuesList.get(k).toString() + "        " + valuesList.get(k + 1).toString());
                                    isDate = Utils.checkValidDate(valuesList.get(k).toString(), valuesList.get(k + 1).toString());
                                }
                                logger.info("isDate = " + isDate);
                                if (!isDate) {
                                    sqlWhereMetaData = sqlWhereMetaData + " " + operatorMap.get(eachOperator) + " ";
                                } else {
                                    sqlWhereMetaData = sqlWhereMetaData + " " + "and" + " ";
                                }
                            }
                        }
                        i++;
                        j++;
                    } else {
                        String surveyUUID = eachQuery.split("\\.")[1];
                        String questionUUID = eachQuery.split("\\.")[2];
                        Integer surveyId = triggerDAO.getSurveyIdByUUID(surveyUUID, businessUUID);
                        if (eachQuery.contains("response")) {
                            String eachMysqlQueryQuestion = getQuestionSubQuery(eachQuery, businessUUID, surveyId);
                            logger.info("The value in eachMysqlQueryQuestion = " + eachMysqlQueryQuestion);
                            if (Utils.isNotNull(eachMysqlQueryQuestion) && eachMysqlQueryQuestion.length() > 0) {
//                            sqlWhereQuestionTemp = sqlWhereQuestionTemp + eachMysqlQueryQuestion;
                                if (Utils.notEmptyString(eachOperator)) {
                                    logger.info("The value in eachOperator = " + eachOperator);
                                    if (eachOperator.equals("&&")) {
                                        sqlWhereQuestionTemp = sqlWhereQuestionTemp + eachMysqlQueryQuestion + ") " + operatorMap.get(eachOperator) + " (";
                                    } else {
                                        sqlWhereQuestionTemp = sqlWhereQuestionTemp + eachMysqlQueryQuestion + " " + operatorMap.get(eachOperator) + " ";
                                    }
                                } else {
                                    sqlWhereQuestionTemp = sqlWhereQuestionTemp + eachMysqlQueryQuestion + " ";
                                }
                            }
                            i++;
                            j++;
                        } else {
                            logger.info("Open Question");
                            Map openMap = getOpenSubQuery(eachQuery);
                            if (openMap.containsKey("categoryName")) {
                                if (sqlWhereOpenMap.containsKey("categoryName")) {
                                    sqlWhereOpenMap.put("categoryName", sqlWhereOpenMap.get("categoryName") + "," + openMap.get("categoryName"));
                                } else {
                                    sqlWhereOpenMap.put("categoryName", openMap.get("categoryName"));
                                }
                            }
                            if (openMap.containsKey("textSentiment")) {
                                if (sqlWhereOpenMap.containsKey("textSentiment")) {
                                    sqlWhereOpenMap.put("textSentiment", sqlWhereOpenMap.get("textSentiment") + "," + openMap.get("textSentiment"));
                                } else {
                                    sqlWhereOpenMap.put("textSentiment", openMap.get("textSentiment"));
                                }
                            }
                        }
                    }
                }
                if (sqlWhereMetaData.endsWith("or ")) {
                    sqlWhereMetaDataFinal = sqlWhereMetaData.substring(0, sqlWhereMetaData.length() - 3);
                } else if (sqlWhereMetaData.endsWith("and ")) {
                    sqlWhereMetaDataFinal = sqlWhereMetaData.substring(0, sqlWhereMetaData.length() - 4);
                } else {
                    sqlWhereMetaDataFinal = sqlWhereMetaData;
                }
                if (!in) {//for final closing bracket
                    sqlWhereMetaDataFinal = sqlWhereMetaDataFinal + " )";
                }
                if (sqlWhereQuestionTemp.endsWith("or ")) {
                    sqlWhereQuestion = sqlWhereQuestionTemp.substring(0, sqlWhereQuestionTemp.length() - 3) + ")";
                } else if (sqlWhereQuestionTemp.endsWith("and (")) {
                    sqlWhereQuestion = sqlWhereQuestionTemp.substring(0, sqlWhereQuestionTemp.length() - 5);
                } else {
                    sqlWhereQuestion = sqlWhereQuestionTemp + " )";
                }
                sqlWhereQuestionFinal = "( " + sqlWhereQuestion + " )";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("The value in sqlWhereMetaData = " + sqlWhereMetaData);
        /**
         * possible changes for DTV-3096
         */
        sqlWhereQuestion = Utils.emptyString(sqlWhereQuestion) ? "( )" : sqlWhereQuestion;
        logger.info("The value in sqlWhereQuestion = " + sqlWhereQuestion);
        Map resultMap = new HashMap();
        resultMap.put("metadata", sqlWhereMetaDataFinal);
        resultMap.put("question", sqlWhereQuestion);
        resultMap.put("textAnalytics", sqlWhereOpenMap);
        return resultMap;
    }


    /**
     *
     * DTV-11303 --> Removing circular dependencies as part of spring boot upgrade
     * Method to update trigger notification details using business uuid by triggerId
     *
     * @param trigger
     * @param triggerId
     * @return
     */
    public int updateTriggerById(final Trigger trigger, String triggerId) {
        logger.debug("begin update the trigger details");
        logger.info("begin update the trigger details");
        int result = 0;
        int ADMIN = 1;
        int SUPER_ADMIN = 4;
        try {


            String timezone = Utils.isNotNull(trigger.getTimezone()) ? trigger.getTimezone() : Constants.TIMEZONE_AMERICA_LOSANGELES;

            String startDate = "";
            String endDate = "";
            String startTime = "";
            String endTime = "";

            /** DTV-3636, if trigger startdate, starttime, enddate, endtime is empty from UI
             * start date will be current date
             * end date will be adjusted to survey end date
             */
            Map triggerMap = triggerDAO.getTriggerMapByUUId(trigger.getBusinessUniqueId(), triggerId);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String currentDateString = simpleDateFormat.format(timestamp);

            if (Utils.emptyString(trigger.getTriggerStartDate()) && Utils.emptyString(trigger.getTriggerStartTime()) && Utils.emptyString(trigger.getTriggerEndDate()) && Utils.emptyString(trigger.getTriggerEndTime())) {
                logger.info("inside if update");
                String surveyEndDate = triggerMap.containsKey(Constants.TO_DATE) ? (triggerMap.get(Constants.TO_DATE).toString()) : "";


                String utcStartTime = Utils.getUTCDateStringFromDateTime(currentDateString.substring(0, 10), "00:00:00", timezone);
                startDate = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(0, 10) : "";
                startTime = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(11, 19) : "";

                String utcEndTime = Utils.getUTCDateStringFromDateTime(surveyEndDate.substring(0, 10), "23:59:59", timezone);
                endDate = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(0, 10) : "";
                endTime = Utils.notEmptyString(utcEndTime) ? utcEndTime.substring(11, 19) : "";

            } else {
                logger.info("inside else update");
                if (Utils.isNotNull(trigger.getTriggerStartDate()) && Utils.isNotNull(trigger.getTriggerStartTime())) {

                    String convertedStartDate = Utils.getUTCDateStringFromDateTime(trigger.getTriggerStartDate(), trigger.getTriggerStartTime(), timezone);
                    //String convertedStartDate = Utils.getUTCDateStringFromDateTime(trigger.getTriggerStartDate(), trigger.getTriggerStartTime(), "UTC");

                    startDate = convertedStartDate.substring(0, 10);
                    startTime = convertedStartDate.substring(11, 19);
                }

                if (Utils.isNotNull(trigger.getTriggerEndDate()) && Utils.isNotNull(trigger.getTriggerEndTime())) {

                    String convertedEndDate = Utils.getUTCDateStringFromDateTime(trigger.getTriggerEndDate(), trigger.getTriggerEndTime(), timezone);
                    //String convertedEndDate = Utils.getUTCDateStringFromDateTime(trigger.getTriggerEndDate(), trigger.getTriggerEndTime(), "UTC");

                    endDate = convertedEndDate.substring(0, 10);
                    endTime = convertedEndDate.substring(11, 19);
                }
            }
            trigger.setTriggerStartDate(startDate);
            trigger.setTriggerStartTime(startTime);
            trigger.setTriggerEndDate(endDate);
            trigger.setTriggerEndTime(endTime);

            /*String startDate = Utils.isNotNull(trigger.getTriggerStartDate()) ? Utils.getUTCDateString(trigger.getTriggerStartDate(), timezone) : null;
            String endDate = Utils.isNotNull(trigger.getTriggerEndDate()) ? Utils.getUTCDateString(trigger.getTriggerEndDate(), timezone) : null;
            String startTime = Utils.isNotNull(trigger.getTriggerStartTime()) ? Utils.getUTCTimeAsString(trigger.getTriggerStartTime(), timezone) : null;
            String endTime = Utils.isNotNull(trigger.getTriggerEndTime()) ? Utils.getUTCTimeAsString(trigger.getTriggerEndTime(), timezone) : null;*/


            String frequency = Utils.isNotNull(trigger.getFrequency()) ? trigger.getFrequency() : Constants.EMPTY;
            int userId = triggerDAO.getUserIdByUUID(trigger.getUserId());
            int roleId = triggerDAO.getRoleIdByUserId(userId);

            int status = Utils.isNotNull(trigger.getStatus()) && trigger.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;

            /**
             * remove DT_ALL for all non admin users
             * This needs to be done under DT_ALL is not send from UI for non admin users
             * DTV-2863
             */
            String modifiedTriggerQuery = (roleId != 1 && roleId != 4 && Utils.notEmptyString(trigger.getTriggerCondition())) ? removeQueriesWithAll(trigger.getTriggerCondition()) : trigger.getTriggerCondition();

            /**
             * DTV-2928 while editing the trigger condition to validate the contacts in the action already saved
             * update deactivate contacts in trigger_notification if new trigger condition should not match with old trigger condition
             * Only for admin to check conditions for read only & read write users for applied survey filter conditions
             */

            String triggerConditionBeforeUpdate = "";
            Trigger triggerBeforeUpdate = getTriggerById(triggerId, trigger.getBusinessUniqueId(), timezone, trigger.getUserId());
            String frequencyBeforeUpdate = triggerBeforeUpdate.getFrequency();

            if (roleId == ADMIN || roleId == SUPER_ADMIN) {
                triggerConditionBeforeUpdate = triggerBeforeUpdate.getTriggerCondition();

                if (!modifiedTriggerQuery.equalsIgnoreCase(triggerConditionBeforeUpdate)) {
                    List validContactEmails = new ArrayList();
                    List triggerSchedule = triggerDAO.getTriggerScheduleByTriggerID(triggerId, trigger.getBusinessUniqueId());

                    if (triggerSchedule.size() > 0) {
                        List validContacts = getValidUsersByTriggerCondition(trigger.getBusinessUniqueId(), Integer.parseInt(trigger.getSurveyId()), modifiedTriggerQuery);
                        for (int i = 0; i < validContacts.size(); i++) {
                            Map eachUserMap = (Map) validContacts.get(i);
                            validContactEmails.add(eachUserMap.get("user_email").toString());
                        }
                        int res = triggerDAO.updateContactsInTriggerNotificationsAndTriggerSchedule(triggerSchedule, validContactEmails, trigger.getBusinessUniqueId());
                    }
                }
            }

            if (!modifiedTriggerQuery.equalsIgnoreCase(triggerConditionBeforeUpdate)) {
                if (triggerId.length() > 0 && modifiedTriggerQuery.length() > 0) {
                    trigger.setTriggerId(triggerId);
                    trigger.setTriggerCondition(modifiedTriggerQuery);
                    trigger.setTriggerFilterUUID(triggerBeforeUpdate.getTriggerFilterUUID());
                    createDynamicFilterFromTriggerCondition(trigger, userId, roleId, Integer.parseInt(trigger.getSurveyId()));
                }
            }

            /**
             * SE-647 if trigger freqeuncy is modified from single to multiple or from multiple to single
             * update the sent status & execution time
             */
            if (!frequency.equalsIgnoreCase(frequencyBeforeUpdate)) {
                int triggerIdInt = triggerMap.containsKey(Constants.ID) ? (int) (triggerMap.get(Constants.ID)) : 0;
                int surveyId = triggerMap.containsKey(Constants.SURVEY_ID) ? Integer.parseInt((triggerMap.get(Constants.SURVEY_ID).toString())) : 0;
                triggerDAO.updateScheduleSentStatusByFrequency(triggerIdInt, trigger.getBusinessUniqueId(), surveyId);
            } else {
                /**if same frequency but new start date is future date.
                 * for multiple frequency sent_status will be always null.
                 * for single frequency it new start date is in future from currentdate chaning sent_status to null
                 */
                if (Utils.notEmptyString(startDate) && Utils.notEmptyString(startTime) && frequency.equals(Constants.FREQUENCY_SINGLE)) {
                    //if trigger start date is in future
                    if (Utils.compareDates(startDate + " " + startTime, currentDateString)) {
                        int triggerIdInt = triggerMap.containsKey(Constants.ID) ? (int) (triggerMap.get(Constants.ID)) : 0;
                        int surveyId = triggerMap.containsKey(Constants.SURVEY_ID) ? Integer.parseInt((triggerMap.get(Constants.SURVEY_ID).toString())) : 0;
                        triggerDAO.updateScheduleSentStatusByFrequency(triggerIdInt, trigger.getBusinessUniqueId(), surveyId);
                    }
                }

            }

            result = triggerDAO.updateTriggerV1(trigger.getTriggerName(), modifiedTriggerQuery, trigger.getSurveyName(), trigger.getSurveyId(), userId, status, startDate, endDate, startTime, endTime, frequency, triggerId, trigger.getBusinessUniqueId());
            logger.debug("end update the trigger details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            logger.error("Exception at updateTrigger. Msg {}", e.getMessage());
            result = -1;
        }
        return result;
    }


    /**
     * @param filterQuery
     * @return
     */
    public String removeQueriesWithAll(String filterQuery) {

        StringBuilder stringBuilder = new StringBuilder();
        List queries = new ArrayList();
        List operators = new ArrayList();
        String[] subQuerries = filterQuery.split("&&|\\|\\|");
        for (int i = 0; i < subQuerries.length; i++) {

            queries.add(subQuerries[i]);
            filterQuery = filterQuery.replace(subQuerries[i], "");
        }

        for (int k = 0; k < filterQuery.length(); k = k + 2) {
            String eachOperator = filterQuery.substring(k, k + 2);
            operators.add(eachOperator);
        }

        int querySize = subQuerries.length;
        for (int i = 0; i < querySize; i++) {

            String tempString = subQuerries[i];
            logger.info("each subquery {} ", tempString);
            String condition = "";
            String[] conditionArr = subQuerries[i].split("\\.");
            condition = conditionArr.length > 2 ? conditionArr[3].toUpperCase() : "";
            boolean addQuery = (condition.contains("DT_ALL") || (condition.contains("ALL"))) ? false : true;
            logger.info("add query {}", addQuery);


            String eachOperator = "";
            if (i < operators.size()) {
                eachOperator = operators.get(i).toString();
            }

            if (addQuery) {
                logger.info("adding each query");
                stringBuilder.append(tempString).append(eachOperator);
            } else {
                logger.info("not adding each query");
            }

        }

        String modifiedQuery = stringBuilder.toString();
        logger.info("modified query {}", modifiedQuery);
        return modifiedQuery;
    }

    /**
     * method to create dynamic filter from trigger condition and updating in trigger_businessUUID table.
     *
     * @param trigger
     * @return
     */
    public void createDynamicFilterFromTriggerCondition(Trigger trigger, int userId, int roleId, int surveyId) {
        logger.info("Start creating dynamic filter from triggerCondition");
        try {

            String businessUUID = trigger.getBusinessUniqueId();
            String userUUID = Utils.isNotNull(trigger.getUserId()) ? trigger.getUserId() : "";
            String triggerUUID = Utils.isNotNull(trigger.getTriggerId()) ? trigger.getTriggerId() : "";
            String triggerCondition = Utils.isNotNull(trigger.getTriggerCondition()) ? trigger.getTriggerCondition() : "";
            String triggerFilterUUID = Utils.isNotNull(trigger.getTriggerFilterUUID()) ? trigger.getTriggerFilterUUID() : "";
            String surveyUUID = Utils.isNotNull(trigger.getSurveyId()) ? trigger.getSurveyId() : "";


            String modifiedQuery = adjustTriggerCondition(triggerCondition);

            logger.info("modifiedQuery  " + modifiedQuery);

            SurveyFilterBean surveyFilterBean = new SurveyFilterBean();
            surveyFilterBean.setFilterName(triggerUUID);
            surveyFilterBean.setFilterQuery(modifiedQuery);
            surveyFilterBean.setReadableQuery(modifiedQuery);
            surveyFilterBean.setSurveyId(surveyUUID);
            surveyFilterBean.setTemporaryFilter("1");
            surveyFilterBean.setCreatedBy(userUUID);


            if (triggerFilterUUID.length() > 0) {

                //Dynamic filter already exists, update the existing filter
                surveyFilterBean.setModifiedBy(userUUID);
                String filterQuery = surveyFilterBean.getFilterQuery();
                Map readableQuery = changedQueries(filterQuery, businessUUID);
                int result = triggerDAO.updateSurveyFilter(triggerFilterUUID, surveyFilterBean, businessUUID, userId, readableQuery);

            } else {
                String filterQuery = surveyFilterBean.getFilterQuery();
                if (roleId == 2) {
                    logger.info("filterQuery = " + filterQuery);
                    filterQuery = removeDtAllFromFilterQuery(filterQuery);
                }
                Map readableQuery = this.changedQueries(filterQuery, businessUUID);

                String filterUUID = triggerDAO.createSurveyFilter(surveyFilterBean, businessUUID, userId, surveyId, roleId, readableQuery);
                if (filterUUID.length() > 0) {
                    //Dynamic filter is created, updating the filterUUID in trigger_businessUUID table.
                    triggerDAO.updateSurveyFilterUUIDByTriggerID(triggerUUID, filterUUID, businessUUID);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at createDynamicFilterFromTriggerCondition. Msg {}", e.getMessage());
            e.printStackTrace();
        }
        logger.info("End creating dynamic filter from triggerCondition");
    }

    /**
     * method to get filter query from trigger condition.
     *
     * @param triggerCondition
     * @return
     */

    public String adjustTriggerCondition(String triggerCondition) {
        String modifiedQuery = "";
        try {
            if (Utils.notEmptyString(triggerCondition) && (!triggerCondition.equalsIgnoreCase("DTANY"))) {

                StringBuilder stringBuilder = new StringBuilder();
                List queries = new ArrayList();
                List operators = new ArrayList();
                String[] subQuerries = triggerCondition.split("&&|\\|\\|");
                for (int i = 0; i < subQuerries.length; i++) {

                    queries.add(subQuerries[i]);
                    triggerCondition = triggerCondition.replace(subQuerries[i], "");
                }

                for (int k = 0; k < triggerCondition.length(); k = k + 2) {
                    if (k + 2 <= triggerCondition.length()) {
                        String eachOperator = triggerCondition.substring(k, k + 2);
                        operators.add(eachOperator);
                    }
                }

                int querySize = subQuerries.length;
                for (int i = 0; i < querySize; i++) {

                    String tempString = subQuerries[i];
                    logger.info("each subquery {}", tempString);

                    String eachOperator = "";
                    String metaData = "";
                    String eachValue = "";
                    if (i < operators.size()) {
                        eachOperator = operators.get(i).toString();
                    }


                    if (tempString.substring(0, 10).equals("dtmetadata") || tempString.substring(0, 7).equals("dtkiosk") || tempString.substring(0, 4).equals("dtqr") || tempString.substring(0, 6).equals("dtlink")) {

                        //meta data case
                        tempString = tempString.replaceAll("dtmetadata.", "");
                        tempString = tempString.replaceAll("dtlink", "");
                        tempString = tempString.replaceAll("dtqr", "");
                        tempString = tempString.replaceAll("dtkiosk", "");
                        logger.info(" tempString  " + tempString);
                        String condition = (subQuerries[i].split("\\.")[3]);
                        metaData = subQuerries[i].split("\\.")[3].split("=|!=|>=|<=|<|>|~")[0];
                        eachValue = subQuerries[i].split("\\.")[3].split("=|!=|>=|<=|<|>|~")[1];
                        logger.info(" condition  " + condition);
                        logger.info(" metaData  " + metaData);
                        logger.info(" eachValue  " + eachValue);

                        if (tempString.contains("ALL")) {
                            tempString.replaceAll("ALL", "DT_ALL");
                        }

                        /**between case in metadata type in triggers, between values can be DATE or Numbers
                         * Example : dtmetadata.a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.Customer Value ($$)~[23,55]
                         * modified query : a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.Customer Value ($$)>23||a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.Customer Value ($$)<55
                         *
                         * Sample : dtmetadat.a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.First Date of Purchase~[2020-10-01,2020-10-22]
                         * modified :
                         * a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.First Date of Purchase>2020-10-01 00:00:00||a0bfc938-5129-4fa3-a999-d26cb9a520a9.6bf7b876-bd09-49b1-aa81-ae81b5c50987.First Date of Purchase<2020-10-22 23:59:59
                         */
                        if (condition.contains("~")) {

                            String[] splittedTempString = tempString.split("~");
                            String[] betweenValues = eachValue.replaceAll("\\[", "").replaceAll("]", "").split(",");

                            if (Utils.isStringNumber(betweenValues[0]) && Utils.isStringNumber(betweenValues[1])) { // check for numeric metadata type
                                tempString = splittedTempString[0] + ">" + betweenValues[0] + "||" + splittedTempString[0] + "<" + betweenValues[1];
                            } else {
                                //date metadata type
                                String fromDate = betweenValues[0] + " 00:00:00"; // start of the from date
                                String toDate = betweenValues[1] + " 23:59:59"; // start of the from date
                                tempString = splittedTempString[0] + ">" + fromDate + "||" + splittedTempString[0] + "<" + toDate;
                            }
                        }
                    }
                    stringBuilder.append(tempString).append(eachOperator);
                }
                modifiedQuery = stringBuilder.toString();
            }
        } catch (Exception e) {
            logger.error("Exception at adjustTriggerCondition. Msg {}", e.getMessage());
            e.printStackTrace();
        }
        return modifiedQuery;
    }

    /**
     * Method to remove DT_All from filter query in case of read only user.
     *
     * @param filterQuery
     * @return
     */
    public String removeDtAllFromFilterQuery(String filterQuery) {
        String filterQueryToReturn = "";
        if (filterQuery.length() > 1) {
            String[] subQuerries = filterQuery.split("&&|\\|\\|");
            try {
                StringBuilder sb = new StringBuilder();
                List queryList = new ArrayList();
                String condition = "";
                boolean in = false;
                for (int i = 0; i < subQuerries.length; i++) {
                    logger.info("subqueries = " + subQuerries[i]);
                    condition = (subQuerries[i].split("\\.")[2]);
                    if (condition.contains("DT_All")) {
                        logger.info("contains DTALL");
                    } else {
                        logger.info("Does not contain DTALL");
                        /*String tempQueryString = (filterQuery.indexOf(subQuerries[i]) + subQuerries[i].length() +2 > filterQuery.length()) ?  : filterQuery.substring(filterQuery.indexOf(subQuerries[i]),filterQuery.indexOf(subQuerries[i])+subQuerries[i].length()+2);*/

                        String tempQueryString = (i == subQuerries.length - 1) ? filterQuery.substring(filterQuery.indexOf(subQuerries[i]), filterQuery.indexOf(subQuerries[i]) + subQuerries[i].length()) : filterQuery.substring(filterQuery.indexOf(subQuerries[i]), filterQuery.indexOf(subQuerries[i]) + subQuerries[i].length() + 2);

                        queryList.add(tempQueryString);
                    }
                }
                for (int j = 0; j < queryList.size(); j++) {
                    sb.append(queryList.get(j));
                }
                filterQueryToReturn = sb.toString();
                if (filterQueryToReturn.endsWith("&&") || filterQueryToReturn.endsWith("||")) {
                    filterQueryToReturn = filterQueryToReturn.substring(0, filterQueryToReturn.length() - 2);
                }
                logger.info("filterQueryToReturn Finally = " + filterQueryToReturn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return filterQueryToReturn;
    }

    /**
     * updating emails usage count for triggers
     *
     * @param businessId
     * @param addCount
     */
    public void updateTriggersEmailUsageCount(int businessId, int addCount) {
        logger.info("start update emails usage count");

        try {
            int maxLimit = -1;
            Map usageMailMap = triggerDAO.getUsageMailFlagByBusinessId(businessId);
            boolean emailMailFlag = Utils.isNotNull(usageMailMap.get("emails")) ? (Integer.parseInt(usageMailMap.get("emails").toString()) > 0) : false;

            Map usageMap = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsLimit = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);

            Map configurationsMap = (Map) configurationsLimit.get(0);
            int triggersEmailCount = Utils.isNotNull(usageMap.get("emailsUsageCount")) ? Integer.parseInt(usageMap.get("emailsUsageCount").toString()) : 0;
            int limit = Utils.isNotNull(configurationsMap.get("noOfEmailsSent")) ? Integer.parseInt(configurationsMap.get("noOfEmailsSent").toString()) : 0;

            /**
             * mail will trigger to king user, when mails usage reaches limit, emailMailFlag is 0, addCount > 0 (adding new email invitee), limit != maxlimit
             */
            if ((triggersEmailCount + addCount >= (limit * 0.8)) && !emailMailFlag && addCount > 0 && (limit != maxLimit)) {
                sendUsageLimitMail(businessId, "emails", triggersEmailCount + addCount);
                usageMailMap.replace("emails", 1);
                triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
            }

            //if usage belows limit and mail flag is 1, changing flag to 0
            if ((triggersEmailCount + addCount < (limit * 0.8)) && emailMailFlag) {
                usageMailMap.replace("emails", 0);
                triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
            }

            triggerDAO.updateTriggersEmailUsageCount(businessId, triggersEmailCount+addCount);

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End update emails usage count");
    }

    /**
     * updating lists usage count and sending email after reaching limit
     * addCount will be +1 while creating list, addCount = -1 when deleting each list
     *
     * @param businessUUID
     * @param addCount
     */
    public void updateTriggerListUsageCount(String businessUUID, int addCount) {
        logger.info("start update active triggers usage count");

        try {
            int maxLimit = -1;
            int businessId = triggerDAO.getBusinessIdByUUID(businessUUID);

            if (businessId > 0) {
                Map usageMailMap = triggerDAO.getUsageMailFlagByBusinessId(businessId);
                boolean listsMailFlag = Utils.isNotNull(usageMailMap.get("lists")) ? (Integer.parseInt(usageMailMap.get("lists").toString()) > 0) : false;

                Map usageMap = triggerDAO.getBusinessUsageByBusinessId(businessId);
                List configurationsLimit = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);

                Map configurationsMap = (Map) configurationsLimit.get(0);
                int listsCount = Utils.isNotNull(usageMap.get("listsUsageCount")) ? Integer.parseInt(usageMap.get("listsUsageCount").toString()) : 0;
                int limit = Utils.isNotNull(configurationsMap.get("noOfLists")) ? Integer.parseInt(configurationsMap.get("noOfLists").toString()) : 0;

                /**
                 * mail will trigger to king user, when usage reaches limit, listsMailFlag is 0, addCount > 0 (adding new lists), limit != maxlimit
                 */
                if ((listsCount + addCount >= (limit * 0.8)) && !listsMailFlag && addCount > 0 && (limit != maxLimit)) {
                    sendUsageLimitMail(businessId, "lists", listsCount + addCount);
                    usageMailMap.replace("lists", 1);
                    triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
                }

                //if usage belows limit and mail flag is 1, changing flag to 0
                if ((listsCount + addCount < (limit * 0.8)) && listsMailFlag) {
                    usageMailMap.replace("lists", 0);
                    triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
                }

                triggerDAO.updateTriggerListUsageCount(businessId, listsCount + addCount);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end update active triggers usage count");
    }

    /**
     * updating active survey triggers usage count and sending email after reaching limit
     * addCount will be +1 while trigger status is active, addCount = -1 when deleting trigger & status is inactive
     *
     * @param businessId
     * @param addCount
     */
    public void updateTriggersUsageCount(int businessId, int addCount) {
        logger.info("start update active triggers usage count");

        try {
            int maxLimit = -1;
            Map usageMailMap = triggerDAO.getUsageMailFlagByBusinessId(businessId);
            boolean triggerMailFlag = Utils.isNotNull(usageMailMap.get("triggers")) ? (Integer.parseInt(usageMailMap.get("triggers").toString()) > 0) : false;

            Map usageMap = triggerDAO.getBusinessUsageByBusinessId(businessId);
            List configurationsLimit = triggerDAO.getBusinessConfigurationsByBusinessId(businessId);

            Map configurationsMap = (Map) configurationsLimit.get(0);
            int triggersCount = Utils.isNotNull(usageMap.get("triggersUsageCount")) ? Integer.parseInt(usageMap.get("triggersUsageCount").toString()) : 0;
            int limit = Utils.isNotNull(configurationsMap.get("noOfTriggers")) ? Integer.parseInt(configurationsMap.get("noOfTriggers").toString()) : 0;

            /**
             * mail will trigger to king user, when triggers usage reaches limit, triggerMailFlag is 0, addCount > 0 (adding new acive trigger), limit != maxlimit
             */
            if ((triggersCount + addCount >= (limit * 0.8)) && !triggerMailFlag && addCount > 0 && (limit != maxLimit)) {
                sendUsageLimitMail(businessId, "triggers", triggersCount + addCount);
                usageMailMap.replace("triggers", 1);
                triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
            }

            //if usage belows limit and mail flag is 1, changing flag to 0
            if ((triggersCount + addCount < (limit * 0.8)) && triggerMailFlag) {
                usageMailMap.replace("triggers", 0);
                triggerDAO.updateUsageMailFlagByBusinessId(businessId, usageMailMap);
            }
            triggerDAO.updateTriggersUsageCount(businessId, triggersCount + addCount);

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end update active triggers usage count");
    }

    /**
     * method to set date values in metadata for ZENDESK
     *
     * @param integrationMap
     * @param metaData
     * @param notifyTime
     * @param timezone
     * @param dateField
     * @param dateLabel
     * @return
     */
    private Map setDateInMetaDataV2(Map integrationMap, Map metaData, String notifyTime, String timezone, String dateField, String dateLabel) {
        String dateStr = "";
        Map zendeskMap = integrationMap.containsKey("ticketInformation") ? (Map) integrationMap.get("ticketInformation") : new HashMap();
        if (zendeskMap.containsKey(dateField)) {
            Object dueDate = zendeskMap.remove(dateField);
            if (dueDate instanceof Map) {
                dateStr = this.getDateByCloseDateConfigV2((Map) dueDate, notifyTime, timezone);
            } else {
                dateStr = dueDate.toString();
            }
            metaData.put(dateLabel, dateStr);
        }
        return metaData;
    }

    /**
     * Method to compute datetime with interval values in days based on the intervalType for ZENDESK
     * <p>
     * close date format yyyy-MM-dd
     *
     * @param closeDateConfig
     * @param startTime
     * @param timezone
     * @return
     */
    public String getDateByCloseDateConfigV2(Map closeDateConfig, String startTime, String timezone) {
        String formattedDate = "";
        try {
            /**
             * "closeDate": {
             * 		"interval": "01",
             * 		"intervalType": "hours", //minutes, hours, days, weeks, months, years
             * 		"onDate": ""
             *  }
             *  "closeDate": {
             * 		"interval": ""
             * 		"intervalType": ""
             * 		"onDate": "2024-08-31"
             *        },
             */
            SimpleDateFormat sdf = new SimpleDateFormat(Constants.YYYY_MM_DD);

            String notifyTime = Utils.getDateAsStringByTimezone(startTime, timezone);

            Date newDateString = sdf.parse(notifyTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDateString);

            int interval = closeDateConfig.containsKey("interval") && Utils.isNotNull(closeDateConfig.get("interval")) ? Integer.parseInt(closeDateConfig.get("interval").toString()) : 0;
            String intervalType = closeDateConfig.containsKey("intervalType") && Utils.isNotNull(closeDateConfig.get("intervalType")) ? closeDateConfig.get("intervalType").toString() : "";

            if (interval > 0 && Utils.notEmptyString(intervalType)) {
                logger.info("interval {} intervalType {}", interval, intervalType);
                // Delayed notifications
                switch (intervalType) {
                    case "minutes":
                        calendar.add(Calendar.MINUTE, interval);
                        break;
                    case "hours":
                        calendar.add(Calendar.HOUR, interval);
                        break;
                    case "days":
                        calendar.add(Calendar.DATE, interval);
                        break;
                    case "weeks":
                        calendar.add(Calendar.WEEK_OF_MONTH, interval);
                        break;
                    case "months":
                        calendar.add(Calendar.MONTH, interval);
                        break;
                    default:
                        break;
                }
            } else {
                formattedDate = closeDateConfig.containsKey("onDate") ? closeDateConfig.get("onDate").toString() : "";
            }

            String resultDate = sdf.format(calendar.getTime());
            formattedDate = Utils.emptyString(formattedDate) ? resultDate.length() > 10 ? resultDate.substring(0, 10) : resultDate : formattedDate;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * Method to update metadata object for Jira
     *
     * @param integrationsConfigMap
     * @param eventResponse
     * @return
     */
    private Map updateMetadataForJira(Map<String, Map> integrationsConfigMap, Map eventResponse) {
        Map metaData = new HashMap();
        try {
            metaData = this.getIntegrationMetadata(eventResponse, integrationsConfigMap);
            String recipeType = getRecipeName(integrationsConfigMap, Constants.CHANNEL_JIRA);
            if (recipeType.equalsIgnoreCase("createIssue")) {
                //appending key value pairs in metaData object using streams.
                for (Map.Entry<String, Map> entry : integrationsConfigMap.entrySet()) {
                    Map<String, String> eachValueMap = entry.getValue();
                    logger.info("eachValueMap {}", eachValueMap);
                    this.replaceHashTag(eachValueMap, eventResponse, "summary");
                    this.replaceHashTag(eachValueMap, eventResponse, "description");
                }
            }

            /**
             * Appending additional headers (if any) accountInformation, leadInformation, opportunityInformation, taskInformation to metaData object
             */
            Map<String, Map> integrationMap = new HashMap(integrationsConfigMap);
            if (integrationMap.containsKey("jira"))
                integrationMap.remove("jira");
            if (integrationMap.containsKey("metaData"))
                integrationMap.remove("metaData");

            metaData = appendKeyValuesToMetadata(integrationMap, metaData);
            logger.info("metaData after {}", metaData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    /**
     * Method to get start dateTime & end dateTime after adjusting to DST
     * @param startDate
     * @param startTime
     * @param endDate
     * @param endTime
     * @param surveyEndDate
     * @param timezone
     * @return
     */
    private Map<String, String> getTriggerStartEndDSTAdjustedDates(String startDate, String startTime, String endDate, String endTime,
                                                                   String surveyEndDate, String timezone) {

        String calculatedStartDate = Constants.EMPTY;
        String calculatedEndDate = Constants.EMPTY;
        String calculatedStartTime = Constants.EMPTY;
        String calculatedEndTime = Constants.EMPTY;
        boolean isStartTimeInDST = false;
        boolean isEndTimeInDST = false;
        Map<String, String> resultMap = new HashMap();

        try {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String currentDateString = simpleDateFormat.format(timestamp);

            if (Utils.emptyString(startDate) && Utils.emptyString(startTime) && Utils.emptyString(endDate) && Utils.emptyString(endTime)) {
                logger.info("inside if update");

                String utcStartTime = Utils.getUTCDateStringFromDateTime(currentDateString.substring(0, 10), "00:00:00", timezone);
                isStartTimeInDST = Utils.isDateInDST(currentDateString, timezone);

                calculatedStartDate = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(0, 10) : Constants.EMPTY;
                calculatedStartTime = Utils.notEmptyString(utcStartTime) ? utcStartTime.substring(11, 19) : Constants.EMPTY;

                /**
                 * CSUP-1675, if survey is created in another timezone and if trigger is created in another timezone
                 * There will be difference in exection end time (adding extra hours or less hours)
                 * solution : convert the survey end date to trigger timezone and
                 * get the end date and adding 23:59:59 for final time in the corresponding request timezone
                 */
                String localSurveyEndDate = Utils.getLocalDateTimeByTimezone(surveyEndDate, timezone);
                isEndTimeInDST = Utils.isDateInDST(localSurveyEndDate, timezone);

                String utcEndDate = Utils.getUTCDateStringFromDateTime(localSurveyEndDate.substring(0, 10), "23:59:59", timezone);
                calculatedEndDate = Utils.notEmptyString(utcEndDate) ? utcEndDate.substring(0, 10) : Constants.EMPTY;
                calculatedEndTime = Utils.notEmptyString(utcEndDate) ? utcEndDate.substring(11, 19) : Constants.EMPTY;

            } else {
                logger.info("inside else update");
                if (Utils.notEmptyString(startDate) && Utils.notEmptyString(startTime)) {

                    isStartTimeInDST = Utils.isDateInDST(startDate + " " + startTime, timezone);
                    String convertedStartDate = Utils.getUTCDateStringFromDateTime(startDate, startTime, timezone);

                    calculatedStartDate = convertedStartDate.substring(0, 10);
                    calculatedStartTime = convertedStartDate.substring(11, 19);
                }

                if (Utils.notEmptyString(endDate) && Utils.notEmptyString(endTime)) {

                    isEndTimeInDST = Utils.isDateInDST(endDate + " " + endTime, timezone);
                    String convertedEndDate = Utils.getUTCDateStringFromDateTime(endDate, endTime, timezone);
                    calculatedEndDate = convertedEndDate.substring(0, 10);
                    calculatedEndTime = convertedEndDate.substring(11, 19);
                }
            }

            //adjusting as per DST
            /**
             * If start time is in DST and end time is not in DST, then reducing 1 hour to end time
             * Example : Start time : 2024-07-14 00:00:00, End time : 2024-12-31 23:59:59 (New York Timezone)
             * Example : Start time : 2024-07-14 04:00:00, End time : 2025-01-01 04:59:59 (UTC)
             * Assume current date : 2024-10-01
             * DST Adjusted : So execution start & end dates should be : 2024-07-14 04:00:00, 2024-07-15 03:59:59 (covers next 24 hours UTC)
             * Without DST adjust : So execution start & end dates will be : 2024-07-15 04:00:00, 2024-07-15 04:59:59 (covers next 1 hour UTC only
             * trigger schedule will work for only 1 hour).
             */
            if(isStartTimeInDST && !isEndTimeInDST) {
                //have to reduce 1 hour from end date to match with start date
                String endDateTime = Utils.getFormattedDateTimeAddHours(calculatedEndDate +" "+ calculatedEndTime, -1);
                calculatedEndDate = endDateTime.substring(0, 10);
                calculatedEndTime = endDateTime.substring(11, 19);
            }

            /**
             * If start time is not in DST and end time is in DST, then reducing 1 hour to end time
             * Example : Start time : 2024-12-31 00:00:00, End time : 2025-04-12 23:59:59 (New York Timezone)
             * Example : Start time : 2025-01-01 05:00:00, End time : 2025-04-13 03:59:59 (UTC)
             * Assume current date : 2025-02-01
             * DST Adjusted : So execution start & end dates should be : 2025-02-01 05:00:00, 2025-02-01 04:59:59 (covers next 24 hours UTC)
             * Without DST adjustment : The execution start & end dates will be : 2025-02-01 05:00:00, 2025-02-01 03:59:59 (covers next 23 hours UTC)
             *
             */
            if(!isStartTimeInDST && isEndTimeInDST) {
                String endDateTime = Utils.getFormattedDateTimeAddHours(calculatedEndDate +" "+ calculatedEndTime, 1);
                calculatedEndDate = endDateTime.substring(0, 10);
                calculatedEndTime = endDateTime.substring(11, 19);
            }

        }catch (Exception e) {
            logger.error("Exception at getTriggerStartEndDSTAdjustedDates. Msg {}", e.getMessage());
        }
        resultMap.put(Constants.START_DATE, calculatedStartDate);
        resultMap.put(Constants.END_DATE, calculatedEndDate);
        resultMap.put(Constants.START_TIME, calculatedStartTime);
        resultMap.put(Constants.END_TIME, calculatedEndTime);

        return resultMap;
    }

    /**
     *
     *
     * @param requestBody
     * @return
     */
    public Map createTriggersFromParticipant(String requestBody){
        Map responseMap = new HashMap();
        try{
            JSONObject requestJson = new JSONObject(requestBody);
            Map requestMap = mapper.readValue(requestJson.toString(), HashMap.class);
            String participantGroupUUID = requestMap.get("participantGroupId").toString();
            String columnName = requestMap.get("columnName").toString();
            List dataValues = triggerDAO.getParticipantDataFromHeader(participantGroupUUID, columnName);


            Iterator<String> iterator = dataValues.iterator();
            while(iterator.hasNext()){
                try{
                    String dataValue = iterator.next();
                    String triggerState = "active";
                    String businessId = requestMap.get("businessId").toString();
                    String triggerName = requestMap.get("triggerPrefixName").toString();
                    String triggerCondition = requestMap.get("triggerCondition").toString();
                    triggerCondition += "." + columnName + "=" + dataValue;
                    logger.info("triggerCondition = {}", triggerCondition);
                    triggerName += dataValue;
                    logger.info("trigger name = {}", triggerName);

                    //inserting trigger
                    Trigger trigger = new Trigger();
                    trigger.setBusinessUniqueId(businessId);
                    trigger.setSurveyId(requestMap.get("surveyId").toString());
                    trigger.setSurveyName(requestMap.get("surveyName").toString());
                    trigger.setTriggerName(triggerName);
                    trigger.setUserId(requestMap.get("userId").toString());
                    trigger.setTriggerCondition(triggerCondition);

                    String triggerId = "";
                    Map triggerCreationMap = this.createTrigger(trigger);
                    if (triggerCreationMap.size() > 0 && (Boolean) triggerCreationMap.get(Constants.SUCCESS)) {
                        triggerId = triggerCreationMap.get(Constants.RESULT).toString();
                    }
                    logger.info("triggerId = {}", triggerId);

                    //inserting trigger schedule
                    TriggerSchedule triggerSchedule = new TriggerSchedule();
                    triggerSchedule.setChannel(requestMap.get("channel").toString());
                    List contacts = (List)requestMap.get("contacts");
                    triggerSchedule.setContacts(contacts);
                    triggerSchedule.setTriggerConfig(requestMap.get("triggerConfig"));
                    triggerSchedule.setSubject(requestMap.get("subject").toString());
                    triggerSchedule.setMessage(requestMap.get("message").toString());
                    triggerSchedule.setAddFeedback((boolean)requestMap.get("addFeedback"));
                    triggerSchedule.setBusinessId(requestMap.get("businessId").toString());
                    triggerSchedule.setTriggerId(triggerId);
                    triggerSchedule.setStartDate(requestMap.get("startDate").toString());
                    triggerSchedule.setEndDate(requestMap.get("endDate").toString());
                    triggerSchedule.setStartTime(requestMap.get("startTime").toString());
                    triggerSchedule.setEndTime(requestMap.get("endTime").toString());
                    triggerSchedule.setFrequency(requestMap.get("frequency").toString());
                    triggerSchedule.setTimezone(requestMap.get("timezone").toString());

                    Map triggerScheduleMap = createTriggerSchedule(triggerSchedule);

                    //activate the trigger
                    updateTriggerStatus(businessId, triggerId, triggerState);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("end createTriggersFromParticipant");
        return responseMap;
    }

}

