package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.dao.TriggerTemplateDAO;
import com.dropthought.trigger.model.WorkatoAdminBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dropthought.trigger.common.Utils.emptyString;

@Component
public class TriggerWorkatoService {

    @Autowired
    protected RestTemplate restTemplate;

    @Value("${dt.workato.url}")
    private String workatoUrl;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TriggerTemplateDAO triggerTemplateDAO;

    @Autowired
    TriggerDAO triggerDAO;

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    /**
     * method to query list of recipes for given client token
     *
     * @param platForm
     * @return
     */
    public List getListOfRecipesWithId(String platForm, int integrationId, String businessId, List requiredRecipeList) {
        logger.info("getListOfRecipies - start");
        List recipesWithId = new ArrayList();
        Map resultMap;
        try {
            //fetch business id by its uniqueID
            int bid = getBusinessIdByUUID(businessId);

            //check if platForm name is not present and integration id is available
            if (integrationId > 0 && Utils.emptyString(platForm)) {
                //fetch integrationName
                platForm = getProviderNameById(integrationId);
            }

            //get managed customerId by using business primary id
            int managedCustomerId = getManagedCustomerByBusiness(bid);

            //get workato admin details
            WorkatoAdminBean workatoAdmin = getWorkatoAdmin();
            if (Utils.isNotNull(workatoAdmin) && managedCustomerId > 0) {
                String userEmail = workatoAdmin.getUserEmail();
                String userToken = workatoAdmin.getUserToken();

                if (Utils.notEmptyString(userEmail) && Utils.notEmptyString(userToken)) {
                    //set the API URL
                    String uri = workatoUrl +
                            "/managed_users/" +
                            managedCustomerId +
                            "/recipes?adapter_names_any=" +
                            platForm;

                    //set the headers
                    HttpHeaders headers = new HttpHeaders();
                    headers.add("x-user-email", userEmail);
                    headers.add("x-user-token", userToken);

                    //REST API call to workato API to get recipes
                    resultMap = workatoApiCall(uri, headers, new HashMap(), HttpMethod.GET);

                    if (resultMap.size() > 0) {
                        List recipeList = resultMap.containsKey("result") ? (List) resultMap.get("result") : new ArrayList();
                        if (recipeList.size() > 0) {
                            for (int index = 0; index < requiredRecipeList.size(); index++) {
                                String recipeName = requiredRecipeList.get(index).toString();
                                for (int i = 0; i < recipeList.size(); i++) {
                                    Map eachRecipeMap = (Map) recipeList.get(i);
                                    String name = eachRecipeMap.containsKey("name") ? eachRecipeMap.get("name").toString() : "";
                                    if (name.equals(recipeName)) {
                                        Map eachRequiredRecipeMap = new HashMap();
                                        String recipeId = eachRecipeMap.containsKey("id") ? eachRecipeMap.get("id").toString() : "";
                                        eachRequiredRecipeMap.put("recipeId", recipeId);
                                        String webhookUrl = eachRecipeMap.containsKey("webhook_url") && Utils.isNotNull(eachRecipeMap.get("webhook_url")) ? eachRecipeMap.get("webhook_url").toString() : "";
                                        eachRequiredRecipeMap.put("webhookUrl", webhookUrl);
                                        eachRequiredRecipeMap.put("recipeName", recipeName);
                                        recipesWithId.add(eachRequiredRecipeMap);
                                        //requiredRecipeList.remove(recipeName);
                                        //index = 0;
                                        break;
                                   }
                                }
                            }
                        }
                    }
                }
            } else {
                logger.info("Bad request, admin details and managedCustomerId are mandatory");
            }
        } catch (Exception e) {
            logger.error("Exception at getListOfRecipes() {}", e.getMessage());
            //e.printStackTrace();
        }
        logger.info("getListOfRecipes - end");
        return recipesWithId;
    }

    /**
     * Function to get  businessId by businessUUID
     *
     * @return
     */
    public int getBusinessIdByUUID(String businessUUID) {
        Integer result = 0;
        try {
            result = jdbcTemplate.queryForObject("select business_id from business where business_uuid = ?",
                    new Object[]{businessUUID},
                    Integer.class);
        } catch (Exception e) {
            logger.info("business id not found for the given business uuid");
        }
        return result;
    }

    /**
     * method to fetch provider name by its integration id
     *
     * @return
     */
    public String getProviderNameById(int integrationId) {
        String integrationName = "";
        try {
            String integrationSql = "SELECT provider from workato_integrations WHERE id = ?";
            Map integrationMap = jdbcTemplate.queryForMap(integrationSql, new Object[]{integrationId});
            //set user email of admin
            integrationName = integrationMap.containsKey("provider") && Utils.isNotNull(integrationMap.get("provider")) ?
                    integrationMap.get("provider").toString() : "";
        } catch (Exception e) {
            logger.info("provider not present!");
            //e.printStackTrace();
        }
        return integrationName;
    }

    /**
     * method to fetch managed customer for a given business
     *
     * @return
     */
    public int getManagedCustomerByBusiness(int businessId) {
        int managedCustId = 0;
        try {
            if(businessId > 0) {
                String managedCustSql = "SELECT managed_customer_id from workato_managed_customer_mapping WHERE business_id = ?";
                Map customerMap = jdbcTemplate.queryForMap(managedCustSql, new Object[]{businessId});
                //set user email of admin
                managedCustId = customerMap.containsKey("managed_customer_id") && Utils.isNotNull(customerMap.get("managed_customer_id")) ?
                        (int) customerMap.get("managed_customer_id") : 0;
            }
        } catch (Exception e) {
            logger.info("Managed customer not present!");
            //e.printStackTrace();
        }
        return managedCustId;
    }

    /**
     * method to fetch workato account admin information
     *
     * @return
     */
    public WorkatoAdminBean getWorkatoAdmin() {
        WorkatoAdminBean admin = null;
        try {
            String workatoAdminSql = "SELECT user_email, user_token FROM workato_admin WHERE status = '1' LIMIT 1";
            Map workatoAdminMap = jdbcTemplate.queryForMap(workatoAdminSql);

            //set user email of admin
            String userEmail = workatoAdminMap.containsKey("user_email") && Utils.notEmptyString(workatoAdminMap.get("user_email").toString()) ? workatoAdminMap.get("user_email").toString() : "";

            //set API key of admin
            String userToken = workatoAdminMap.containsKey("user_token") && Utils.notEmptyString(workatoAdminMap.get("user_token").toString()) ? workatoAdminMap.get("user_token").toString() : "";

            admin = new WorkatoAdminBean();
            admin.setUserEmail(userEmail);
            admin.setUserToken(userToken);
        } catch (Exception e) {
            logger.info("Workato Admin details are not present!");
            //e.printStackTrace();
        }
        return admin;
    }

    /**
     * method to call workato APIs
     *
     * @param url
     * @param headers
     * @param inputMap
     * @param method
     * @return
     */
    public Map workatoApiCall(String url, HttpHeaders headers, Map inputMap, HttpMethod method) {
        Map resultMap = new HashMap();
        try {
            HttpEntity entity = new HttpEntity<>(inputMap, headers);
            //logger.info("Entity {}", entity.toString());
            try {
                ResponseEntity<String> responseEntity = restTemplate.exchange(url, method, entity, String.class);
                //logger.info("Response Entity {}", responseEntity.toString());
                if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK || responseEntity.getStatusCode() == HttpStatus.CREATED)) {
                    if (responseEntity.getBody() != null) {
                        //logger.info(responseEntity.getBody().toString());
                        //resultMap = (Map) responseEntity.getBody();
                        resultMap = mapper.readValue(responseEntity.getBody(), HashMap.class);
                        //logger.info("Workato result map {}", resultMap);
                    }
                }
            } catch (Exception e) {
                logger.error("Exception at workatoApiCall() - {} ", e.getMessage());
                //e.printStackTrace();
            }
        } catch (Exception e) {
            logger.error("Exception at workatoApiCall() - {} ", e.getMessage());
        }
        return resultMap;
    }
}
