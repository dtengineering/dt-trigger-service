package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.TwoFortySevenSoftwareDAO;
import com.dropthought.trigger.model.TwoFortySevenRequestBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TwoFortySevenSoftwareService {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    TwoFortySevenSoftwareDAO softwareDAO;

    @Autowired
    RestTemplate restTemplate;

    @Value("${247software.url}")
    private String softwareUrl;

    /**
     * Method to get the config map from config_247software table
     * @param businessId
     * @return
     */
    public Map<String, Object> getConfigMap(int businessId) {

        Map<String, Object> configMap = new HashMap<>();
        try {
            configMap = softwareDAO.getConfigMap(businessId);
        } catch (Exception e) {
            logger.error("Error in 24/7 software login method: ", e);
        }

        return configMap;
    }

    /**
     * Method to construct request bean for 24/7 software to create incident
     *
     * @param token
     * @param metadataMap
     * @param personalInfoMap
     * @return
     */
    public TwoFortySevenRequestBean constructRequestBean(String token, Map<String, String> metadataMap,
                                                         String requestTypeId, String recipeType, String businessUUID, String facility_Id,
                                                         String department_Id, String incidentType_Id, String location_Id,
                                                         String section_Id, Map personalInfoMap, String workOrderId, String userId,
                                                         String event_Id) {

        TwoFortySevenRequestBean requestBean = new TwoFortySevenRequestBean();
        try {

            String facilityId = "";
            String facilityName;
            if (Utils.emptyString(facility_Id)) {

                String metaDataName = metadataMap.containsKey("Facility") ? metadataMap.get("Facility") : "";
                facilityName = personalInfoMap.containsKey(metaDataName) ?
                        personalInfoMap.get(metaDataName).toString() : "";

                if (Utils.notEmptyString(facilityName)) {

                    //get facility id from 24/7 software and set it in request bean
                    Map<String, Object> facilityMap = getFacilities(token, businessUUID);
                    if (!facilityMap.isEmpty() && facilityMap.containsKey("success") && facilityMap.get("success").equals(true)) {

                        List<Map<String, Object>> facilityDataList = facilityMap.containsKey("facilityData") ?
                                (List<Map<String, Object>>) facilityMap.get("facilityData") : new ArrayList<>();
                        facilityId = !facilityDataList.isEmpty() ? facilityDataList.stream()
                                .filter(facility -> facility.containsKey("facility_name") &&
                                        facility.get("facility_name").toString().equals(facilityName))
                                .findFirst().map(facility -> facility.get("facility_id").toString()).orElse("") : "";
                    }
                }
            } else {
                facilityId = facility_Id;
            }
            requestBean.setFacilityId(facilityId);

            //DTV-13825
            String eventId = "";
            String eventName;
            if (Utils.emptyString(event_Id)) {
                String metaDataName = metadataMap.containsKey("Event") ? metadataMap.get("Event") : "";
                eventName = personalInfoMap.containsKey(metaDataName) ?
                        personalInfoMap.get(metaDataName).toString() : "";
                if (Utils.notEmptyString(eventName)) {

                        List<Map<String, Object>> eventList = getEvents(token, businessUUID);
                        eventId = !eventList.isEmpty() ? eventList.stream()
                                .filter(event -> event.containsKey("event_name") &&
                                        event.get("event_name").toString().equals(eventName))
                                .findFirst().map(event -> event.get("_id").toString()).orElse("") : "";
                }
            } else {
                eventId = event_Id;
            }
            requestBean.setEventId(eventId);

            //get department name from metadata map and fetch department id from 24/7 software and set it in request bean
            String departmentId = "";
            String departmentName;
            if (Utils.emptyString(department_Id)) {
                String metaDataName = metadataMap.containsKey("Department") ? metadataMap.get("Department") : "";
                departmentName = personalInfoMap.containsKey(metaDataName) ?
                        personalInfoMap.get(metaDataName).toString() : "";
                if (Utils.notEmptyString(departmentName)) {

                    List<Map<String, Object>> departmentList = getDepartments(token, businessUUID, facilityId);
                    departmentId = !departmentList.isEmpty() ? departmentList.stream()
                            .filter(department -> department.containsKey("department_name") &&
                                    department.get("department_name").toString().equals(departmentName))
                            .findFirst().map(department -> department.get("id").toString()).orElse("") : "";
                }
            } else {
                departmentName = "";
                departmentId = department_Id;
            }
            requestBean.setDepartmentId(departmentId);
            requestBean.setDepartmentName(departmentName);

            if (recipeType.equals("Incident")) {
                //get incident type from metadata map and fetch incident type id from 24/7 software and set it in request bean
                String incidentTypeId = "";
                String incidentType;
                if (Utils.emptyString(incidentType_Id)) {

                    String metaDataName = metadataMap.containsKey("Incident Type") ? metadataMap.get("Incident Type") : "";
                    incidentType = personalInfoMap.containsKey(metaDataName) ?
                            personalInfoMap.get(metaDataName).toString() : "";
                    if (Utils.notEmptyString(incidentType)) {

                        List<Map<String, Object>> incidentTypeList = getIncidentTypes(token, businessUUID, facilityId, departmentId);
                        incidentTypeId = !incidentTypeList.isEmpty() ? incidentTypeList.stream()
                                .filter(incident -> incident.containsKey("incident_type") &&
                                        incident.get("incident_type").toString().equals(incidentType))
                                .findFirst().map(incident -> incident.get("id").toString()).orElse("") : "";
                    }
                } else {
                    incidentTypeId = incidentType_Id;
                    incidentType = "";
                }
                requestBean.setIncidentTypeId(incidentTypeId);
                requestBean.setIncidentTypeName(incidentType);
            }

            if (recipeType.equals("Incident") || recipeType.equals("WorkOrder")) {

                //get location name from metadata map and fetch location id from 24/7 software and set it in request bean
                String locationId = "";
                String locationName;
                if (Utils.emptyString(location_Id)) {

                    String metaDataName = metadataMap.containsKey("Location") ? metadataMap.get("Location") : "";
                    locationName = personalInfoMap.containsKey(metaDataName) ?
                            personalInfoMap.get(metaDataName).toString() : "";
                    if (Utils.notEmptyString(locationName)) {

                        List<Map<String, Object>> locationList = getLocations(token, businessUUID, facilityId);
                        locationId = !locationList.isEmpty() ? locationList.stream()
                                .filter(location -> location.containsKey("location_name") &&
                                        location.get("location_name").toString().equals(locationName))
                                .findFirst().map(location -> location.get("id").toString()).orElse("") : "";
                    }
                } else {
                    locationId = location_Id;
                    locationName = "";
                }
                requestBean.setLocationId(locationId);
                requestBean.setLocationName(locationName);

                //get section name from metadata map and fetch section id from 24/7 software and set it in request bean
                String sectionId = "";
                String sectionName;
                if (Utils.emptyString(section_Id)) {

                    String metaDataName = metadataMap.containsKey("Section") ? metadataMap.get("Section") : "";
                    sectionName = personalInfoMap.containsKey(metaDataName) ?
                            personalInfoMap.get(metaDataName).toString() : "";
                    if (Utils.notEmptyString(sectionName)) {
                        List<Map<String, Object>> sectionList = getSections(token, businessUUID, facilityId);
                        sectionId = !sectionList.isEmpty() ? sectionList.stream()
                                .filter(section -> section.containsKey("section_name") &&
                                        section.get("section_name").toString().equals(sectionName))
                                .findFirst().map(section -> section.get("id").toString()).orElse("") : "";
                    }
                } else {
                    sectionId = section_Id;
                    sectionName = "";
                }
                requestBean.setSectionId(sectionId);
                requestBean.setSectionName(sectionName);

            }
            if (recipeType.equals("WorkOrder")) {
                requestBean.setWorkOrderId(workOrderId);
                requestBean.setUserId(userId);
                requestBean.setRequestTypeId(requestTypeId);
                requestBean.setSubmittedDate(metadataMap.containsKey("submitted") ? metadataMap.get("submitted") : "");
                requestBean.setScheduleDate(metadataMap.containsKey("schedule") ? metadataMap.get("schedule") : "");
                requestBean.setReminderDate(metadataMap.containsKey("reminder") ? metadataMap.get("reminder") : "");
                requestBean.setRequiredByDate(metadataMap.containsKey("requiredBy") ? metadataMap.get("requiredBy") : "");
                String specificLocation = metadataMap.containsKey("Specific Location") ? metadataMap.get("Specific Location") : "";
                requestBean.setSpecificLocation(specificLocation);
            }

            if (recipeType.equals("Request")) {
                requestBean.setRequestTypeId(requestTypeId);
            }

        } catch (Exception e) {
            logger.error("Error in constructing 24/7 software request bean: ", e);
        }

        return requestBean;
    }

    /**
     * Method to get the most recent event from 24/7 software
     * @param token
     * @param businessUUID
     * @return
     *
     */
    private List<Map<String, Object>> getEvents(String token, String businessUUID) {

        //String eventId = "";
        List<Map<String, Object>> eventList = new ArrayList<>();
        try {
            String baseUrl = softwareUrl + "event";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);

            HttpEntity<?> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {
                eventList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();

                // filter the eventList by "status_id" field in the list and get the events which are having either "0" or "1" in the status_id field
                /*eventList = !eventList.isEmpty() ? eventList.stream()
                        .filter(event -> event.containsKey("status_id") &&
                                (event.get("status_id").toString().equals("0") || event.get("status_id").toString().equals("1")))
                        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll) : new ArrayList<>();*/

                // filter the eventList by "event_end_time" field in the list and get the events which are having "event_end_time" field as  ""
                /*eventList = !eventList.isEmpty() ? eventList.stream()
                        .filter(event -> event.containsKey("event_end_time") &&
                                event.get("event_end_time").toString().equals(""))
                        .collect(ArrayList::new, ArrayList::add, ArrayList::addAll) : new ArrayList<>();*/

                // filter the most recent event from the list by using event_start_time field in the list
                /*eventId = !eventList.isEmpty() ? eventList.stream()
                        .filter(event -> event.containsKey("event_start_time"))
                        .sorted((event1, event2) -> event2.get("event_start_time").toString()
                                .compareTo(event1.get("event_start_time").toString()))
                        .findFirst().map(event -> event.get("_id").toString()).orElse("") : "";*/
            } else {
                logger.info("Not able to get events. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getEvents method: ", e);
        }

        return eventList;
    }

    /**
     * Method to get the sections from 24/7 software
     * @param token
     * @param businessUUID
     * @return
     */
    private List<Map<String, Object>> getSections(String token, String businessUUID, String facilityId) {

        List<Map<String, Object>> sectionList = new ArrayList<>();
        try {
            String baseUrl = softwareUrl + "section";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);

           /* String requestParam = "data={\"filter\": [{\"facility_filter\": [{\"facility_id\":" + "\"" + facilityId + "\"}]}]}";
            baseUrl = baseUrl + "?" + requestParam;*/

            HttpEntity<?> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {
                sectionList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();
            } else {
                logger.info("Not able to get sections. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getSections method: ", e);
        }

        return sectionList;
    }

    /**
     * Method to get the locations from 24/7 software
     * @param token
     * @param businessUUID
     * @return
     */
    private List<Map<String, Object>> getLocations(String token, String businessUUID, String facilityId) {

        List<Map<String, Object>> locationList = new ArrayList<>();
        try {
            String baseUrl = softwareUrl + "location";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);

            /*String requestParam = "data={\"filter\": [{\"facility_filter\": [{\"facility_id\":" + "\"" + facilityId + "\"}]}]}";
            baseUrl = baseUrl + "?" + requestParam;*/

            HttpEntity<?> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {

                locationList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();
            } else {
                logger.info("Not able to get locations. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getLocations method: ", e);
        }

        return locationList;
    }

    /**
     * Method to create failure data in failure_247software_{businessuuid} table
     * @param baseUrl
     * @param token
     * @param message
     * @param businessUUID
     * @param method
     * @param payload
     */
    public void createFailureData(String baseUrl, String token, String message, String businessUUID, String method, String payload) {

        // insert the data in to failure_247software_{businessuuid} database for retry / reference
        softwareDAO.createSoftwareFailureTable(businessUUID);
        softwareDAO.insertFailureData(baseUrl, token, method, message, businessUUID, payload);
    }



    /**
     * Method to get the incident types from 24/7 software
     * @param token
     * @return
     */
    private List<Map<String, Object>> getIncidentTypes(String token, String businessUUID, String facilityId, String departmentId) {

        List<Map<String, Object>> incidentTypeList = new ArrayList<>();
        try {
            String baseUrl = softwareUrl + "incidentType";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);

            /*String requestParam = "data={\"filter\": [{\"facility_filter\": [{\"facility_id\":" + "\"" + facilityId + "\"}]}" +
                    ",{\"department_filter\":[{\"department_id\":" + "\"" + departmentId + "\"}]}]}";
            baseUrl = baseUrl + "?" + requestParam;*/


            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {
                incidentTypeList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();
            } else {
                logger.info("Not able to get incident types. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getIncidentTypes method: ", e);
        }

        return incidentTypeList;
    }

    /**
     * Method to get the departments from 24/7 software
     * @param token
     * @return
     */
    private List<Map<String, Object>> getDepartments(String token, String businessUUID, String facilityId) {

        List<Map<String, Object>> departmentList = new ArrayList<>();
        try {
            String baseUrl = softwareUrl + "department";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);
            // Comment the below line temporarily since getting error response from 24/7 software but working fine in postman
            //{success=false, message=Not enough variable values available to expand '"filter"', status=500}
            //https://app.247software.com/department?data={"filter": [{"facility_filter": [{"facility_id":"abc"}]}]}

            /*String requestParam = "data={\"filter\": [{\"facility_filter\": [{\"facility_id\":" + "\"" + facilityId + "\"}]}]}";
            baseUrl = baseUrl + "?" + requestParam;*/

            HttpEntity<?> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {
                departmentList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();
            } else {
                logger.info("Not able to get departments. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getDepartments method: ", e);
        }

        return departmentList;
    }

    /**
     * Method to get the facilities from 24/7 software (Note : Each account has only one facility data. So, getting the first facility data from the list
     * and returning the facility_id)
     * @param token
     * @return
     */
    public Map getFacilities(String token, String businessUUID) {

        Map<String, Object> responseMap = new HashMap();
        try {
            // data found in config_247software table for the given businessId.
            // Proceed with getFacilities
            String baseUrl = softwareUrl + "facility/getFacilityWithModules";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Cookie", "jwt_token=" + token);

            HttpEntity<?> requestEntity = new HttpEntity<>(headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.GET);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                    softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200) &&
                    softwareResponseMap.containsKey("data")) {
                List<Map<String, Object>> facilityDataList = softwareResponseMap.get("data") instanceof List ?
                        (List<Map<String, Object>>) softwareResponseMap.get("data") : new ArrayList<>();
               // Map<String, Object> facilityDataMap = facilityDataList.get(0); // Facilities list has only one facility data
                responseMap.put("success", true);
                responseMap.put("facilityData", facilityDataList);
            } else {
                responseMap.put("success", false);
                responseMap.put("message", "Not able to get facilities. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                        softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "GET", "");
            }

        } catch (Exception e) {
            logger.error("Error in 24/7 software getFacilities method: ", e);
            responseMap.put("success", false);
            responseMap.put("message", "Not able to get facilities. Please try again.");
        }

        return responseMap;
    }

    /**
     * method to call a 24/7 software endpoint using rest template
     * @param baseUrl
     * @param httpEntity
     * @param method
     * @return
     */
    public Map call247SoftwareEndpoint(String baseUrl, HttpEntity httpEntity, HttpMethod method) {

        logger.info("begin 24/7 software rest api");
        Map respMap = new HashMap();

        logger.info("24/7 Request form-data : ", httpEntity);
        logger.info("uri {}", baseUrl);

        try {
            ResponseEntity<Object> responseEntity = restTemplate.exchange(baseUrl, method, httpEntity, Object.class);
            if (responseEntity != null && (responseEntity.getStatusCode() == HttpStatus.OK ||
                    responseEntity.getStatusCode() == HttpStatus.CREATED)) {

                logger.info("Success response from 24/7 software. response : {}", responseEntity);
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    respMap.put("success", true);
                }
            } else if (responseEntity.getHeaders().containsKey("content-type") && responseEntity.getHeaders().getContentType().getType().equals("text") &&
                responseEntity.getHeaders().getContentType().getSubtype().equals("html")) {
                // if the response is in html format, then there may be error in 24/7 software or the given request
                respMap.put("success", false);
                respMap.put("message", "Invalid Request. Please try again.");

            } else{
                logger.info("Failure response from 24/7 software. response : {}", responseEntity);
                respMap.put("success", false);
                Map<String, Object> failureMap = (Map) responseEntity.getBody();
                respMap.put("message", failureMap.containsKey("status_message") ? failureMap.get("status_message") : "Error response from 24/7 software");
            }
            logger.info("end 24/7 software rest api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", baseUrl);
            respMap.put("success",false);
            respMap.put("status", 500);
            respMap.put("message", "Connection refused");
        }
        catch (HttpClientErrorException ex){
            logger.error("client exception {}", ex.getMessage());
            respMap.put("success",false);
            respMap.put("status", ex.getRawStatusCode());
            respMap.put("message", ex.getMessage());
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", e.getMessage());
            respMap.put("status", 500);
            logger.error("Error in 24/7 software call247SoftwareEndpoint method: ", e.getMessage());
        }

        return  respMap;
    }

    /**
     * Method to create incident in 24/7 software
     * @param requestBean
     * @param token
     * @return
     */
    public Map createIncidentInTwoFortySevenSoftware(TwoFortySevenRequestBean requestBean, String token,
                                                     String businessUUID, String feedback, String baseUrl) {

        Map<String, Object> responseMap = new HashMap<>();
        boolean success = false;
       MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "*/*");
            headers.set("Cookie", "jwt_token=" + token);
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            formData.add("facility_id", requestBean.getFacilityId());
            if (Utils.notEmptyString(requestBean.getEventId()) ) {
                logger.info("Event Id is not empty : " + requestBean.getEventId());
                formData.add("event_id", requestBean.getEventId());
            }
            formData.add("department_id", requestBean.getDepartmentId());
            formData.add("incident_type_id", requestBean.getIncidentTypeId());
            //location and section are optional parameters. If not empty, then add it to the form data
            if (Utils.notEmptyString(requestBean.getLocationId())) {
                formData.add("location_ids[]", requestBean.getLocationId());
            }

            if (Utils.notEmptyString(requestBean.getSectionId())) {
                formData.add("section_ids[]", requestBean.getSectionId());
            }
            if (Utils.notEmptyString(feedback)) {
                formData.add("summary", feedback);
            }
            formData.add("is_internal_api", "1");

            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(formData, headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.POST);
           if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                        softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(201)) {
                success = true;
                Map<String, Object> responseData = (Map) softwareResponseMap.get("data");
                responseMap.put("success", true);
                responseMap.put("message", "Incident created successfully in 24/7 software");
                responseMap.put("incidentNo", responseData.get("incident_no").toString());
                responseMap.put("incidentId", responseData.get("incident_id").toString());
                logger.info("Incident created successfully in 24/7 software. incident_no : {} incident_id : {}",
                        responseData.get("incident_no").toString(), responseData.get("incident_id").toString());
            } else {
                responseMap.put("success", false);
                responseMap.put("message", "Not able to create incident in 24/7 software. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                                softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "POST",
                        formData.toString());
            }

        } catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", baseUrl);
            responseMap.put("success",false);
            responseMap.put("status", 500);
            responseMap.put("message", "Connection refused");
        } catch (HttpClientErrorException ex){
            logger.error("client exception {}", ex.getMessage());
            responseMap.put("success",false);
            responseMap.put("status", ex.getRawStatusCode());
            responseMap.put("message", ex.getMessage());
        } catch (Exception e) {
            logger.error("Error in 24/7 software createIncidentInTwoFortySevenSoftware method: ", e);
            responseMap.put("success", false);
            responseMap.put("message", "Not able to create incident in 24/7 software. Please try again.");
        }

        if(!success){
            //insert the data in to failure_247software_{businessuuid} database for retry / reference
            createFailureData(baseUrl, token,
                    responseMap.containsKey("message") ? responseMap.get("message").toString() : "Internal error", businessUUID, "POST",
                    formData.toString());
        }



        return responseMap;
    }

    /**
     * Method to create request in 24/7 software
     * @param requestBean
     * @param token
     * @return
     */
    public Map createRequestInTwoFortySevenSoftware(TwoFortySevenRequestBean requestBean, String token, String businessUUID) {

            Map<String, Object> responseMap = new HashMap<>();
            try {
                String baseUrl = softwareUrl + "RequestWrapper";
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                headers.set("Cookie", "jwt_token=" + token);

                MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
                formData.add("facility_id", requestBean.getFacilityId());
                formData.add("event_id", requestBean.getEventId());
                formData.add("department_id", requestBean.getDepartmentId());
                formData.add("request_type_id", requestBean.getRequestTypeId());

                HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(formData, headers);

                Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                        HttpMethod.POST);

                if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true) &&
                        softwareResponseMap.containsKey("status_code") && softwareResponseMap.get("status_code").equals(200)) {
                    responseMap.put("success", true);
                    responseMap.put("message", "Request created successfully in 24/7 software");
                } else {
                    responseMap.put("success", false);
                    responseMap.put("message", "Not able to create request in 24/7 software. Please try again.");
                    //insert the data in to failure_247software_{businessuuid} database for retry / reference
                    createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                            softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "POST",
                            formData.toString());
                }

            } catch (Exception e) {
                logger.error("Error in 24/7 software createRequestInTwoFortySevenSoftware method: ", e);
                responseMap.put("success", false);
                responseMap.put("message", "Not able to create request in 24/7 software. Please try again.");
            }

            return responseMap;
    }

    /**
     * Method to create work order in 24/7 software
     * @param requestBean
     * @param token
     * @param businessUUID
     * @param feedback
     * @param baseUrl
     * @return
     */
    public Map createWorkOrderInTwoFortySevenSoftware(TwoFortySevenRequestBean requestBean, String token,
                                                      String businessUUID, String feedback, String baseUrl) {

        Map<String, Object> responseMap = new HashMap<>();
        boolean success = false;
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Accept", "*/*");
            headers.set("Cookie", "jwt_token=" + token);
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            formData.add("facility", requestBean.getFacilityId());
            //DTV-13825 currently commented out since work order is not created when we pass event in the payload
            /*if (Utils.notEmptyString(requestBean.getEventId())) {
                formData.add("event_id", requestBean.getEventId());
            }*/
            formData.add("work_order_type", requestBean.getWorkOrderId());
            formData.add("requesting[0][user_id]", requestBean.getUserId());
            formData.add("requesting[0][department_id]", requestBean.getDepartmentId());
            formData.add("requesting[0][type]", "USER");
            formData.add("submitted", requestBean.getSubmittedDate());
            //request type, location, specific location and section are optional parameters. If not empty, then add it to the form data
            if (Utils.notEmptyString(requestBean.getRequestTypeId())) {
                formData.add("request_type", requestBean.getRequestTypeId());
            }
            if (Utils.notEmptyString(requestBean.getLocationId())) {
                formData.add("location", requestBean.getLocationId());
            }
            if (Utils.notEmptyString(requestBean.getSectionId())) {
                formData.add("section", requestBean.getSectionId());
            }
            if (Utils.notEmptyString(feedback)) {
                formData.add("summary", feedback);
            }
            if (Utils.notEmptyString(requestBean.getScheduleDate())) {
                formData.add("scheduled", requestBean.getScheduleDate());
            }
            if (Utils.notEmptyString(requestBean.getReminderDate())) {
                formData.add("reminder", requestBean.getReminderDate());
            }
            if (Utils.notEmptyString(requestBean.getRequiredByDate())) {
                formData.add("required_by", requestBean.getRequiredByDate());
            }
            if (Utils.notEmptyString(requestBean.getSpecificLocation())) {
                formData.add("custom_fields[621e43be0249fb3409269106][33317873]", requestBean.getSpecificLocation());
            }

            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(formData, headers);

            Map<String, Object> softwareResponseMap = call247SoftwareEndpoint(baseUrl, requestEntity,
                    HttpMethod.POST);

            if (softwareResponseMap.containsKey("success") && softwareResponseMap.get("success").equals(true)) {
                success = true;
                responseMap.put("success", true);
                responseMap.put("message", "Work Order created successfully in 24/7 software");
                String recordNo = softwareResponseMap.containsKey("record_no") ? softwareResponseMap.get("record_no").toString() : "";
                String recordId = softwareResponseMap.containsKey("record_id") ? softwareResponseMap.get("record_id").toString() : "";
                responseMap.put("recordNo", recordNo);
                responseMap.put("recordId", recordId);
                logger.info("Work Order created successfully in 24/7 software. recordNo : {} recordId : {}",
                        recordNo, recordId);
            } else {
                responseMap.put("success", false);
                responseMap.put("message", "Not able to create work order in 24/7 software. Please try again.");
                //insert the data in to failure_247software_{businessuuid} database for retry / reference
                createFailureData(baseUrl, token, softwareResponseMap.containsKey("message") ?
                                softwareResponseMap.get("message").toString() : "Internal error", businessUUID, "POST",
                        formData.toString());
            }

        } catch (ResourceAccessException ex) {
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", baseUrl);
            responseMap.put("success", false);
            responseMap.put("status", 500);
            responseMap.put("message", "Connection refused");
        } catch (HttpClientErrorException ex) {
            logger.error("client exception {}", ex.getMessage());
            responseMap.put("success", false);
            responseMap.put("status", ex.getRawStatusCode());
            responseMap.put("message", ex.getMessage());
        } catch (Exception e) {
            logger.error("Error in 24/7 software createWorkOrderInTwoFortySevenSoftware method: ", e);
            responseMap.put("success", false);
            responseMap.put("message", "Not able to create work order in 24/7 software. Please try again.");
        }

        if (!success) {
            //insert the data in to failure_247software_{businessuuid} database for retry / reference
            createFailureData(baseUrl, token,
                    responseMap.containsKey("message") ? responseMap.get("message").toString() : "Internal error", businessUUID, "POST",
                    formData.toString());
        }

        return responseMap;
    }
}
