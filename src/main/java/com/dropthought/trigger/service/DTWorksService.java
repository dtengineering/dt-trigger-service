package com.dropthought.trigger.service;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.dao.DTWorksDAO;
import com.dropthought.trigger.dao.TriggerDAO;
import com.dropthought.trigger.model.DTWorksLoginBean;
import com.dropthought.trigger.model.DTWorksProfileBean;
import com.dropthought.trigger.model.DTWorksRecordResponseBean;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;

@Component
public class DTWorksService{
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    DTWorksDAO dtWorksDAO;

    @Autowired
    TriggerDAO triggerDAO;

    @Autowired
    RestTemplate restTemplate;

    @Value("${dtworks.url}")
    private String dtworksUrl;

    @Value("${dtworks.multitenant}")
    private boolean isMultiTenant;

    @PostConstruct
    public void getDTWorksUrl(){
        if(isMultiTenant){
            logger.info("multi tenant enabled");
            dtworksUrl = dtworksUrl.replace("test" , "dt-test")
                    .replace("stage", "dt-stage").replace("demo", "dt-demo")
                    .replace("sandbox", "dt-sandbox");
            logger.info("dtworks url {}", dtworksUrl);
        }else{
            logger.info("multi tenant disabled");
        }
    }


    public Map retrieveDTWorksConfigs(int businessId){
        Map config = new HashMap();
        try{
            List configs = dtWorksDAO.getDTWorksConfigs(businessId);
            if(configs != null && configs.size() > 0){
                config = (Map) configs.get(0);
            }
        }catch (Exception e){
            logger.error("Error while retrieving the dtworks configs {}", e);
        }
        return  config;
    }


    public Map verifyAndRenewToken(int businessId) {
        logger.info("start checking the access token expiration");
        Map renewalMap = new HashMap();
        try{
            Map config = retrieveDTWorksConfigs(businessId);
            String accessToken = (config.containsKey("access_token")) ? (String) config.get("access_token") : "";
            String refreshToken = (config.containsKey("refresh_token")) ? (String) config.get("refresh_token") : "";
            String dtworksTenantId = config.containsKey("tenant_id") ? (String) config.get("tenant_id") : "";
            String expirationTime = config.containsKey("expiration_time") && Utils.isNotNull(config.get("expiration_time")) ?
                    config.get("expiration_time").toString() : "";

            if(Utils.emptyString(accessToken)){
                //renew the access token
                renewalMap = loginToDtworks(config, dtworksTenantId, renewalMap);
                renewalMap.put("tenantId",dtworksTenantId);
            }else {
                expirationTime = expirationTime.substring(0, 19);
                Map<String, Integer> responseMap = verifyToken(expirationTime);
                logger.info("responseMap {} ", responseMap);
                //TODO epiratetime is valid but access token is not autorized since the same token is used by another user via dashboard
                if (responseMap.containsKey("status") && responseMap.get("status").equals(401)) {
                    //renew the access token
                    renewalMap = renewTheAccessToken(dtworksTenantId, refreshToken, config);
                    renewalMap.put("tenantId", dtworksTenantId);
                } else if (responseMap.containsKey("status") && responseMap.get("status").equals(200)) {
                    //check the expiration time
                    renewalMap.put("accessToken", accessToken);
                    renewalMap.put("refreshToken", refreshToken);
                    renewalMap.put("tenantId", dtworksTenantId);
                }
            }

        }catch (Exception e){
            logger.error("Error while checking the access token expiration", e);
        }
        logger.info("end checking the access token expiration");
        return renewalMap;
    }


    public Map verifyAndRenewUserToken(Map  userAccessMap, int businessId) {
        logger.info("start checking the user access token expiration");
        Map renewalMap = new HashMap();
        try{
            Map config = retrieveDTWorksConfigs(businessId);
            //get the tenant id from master account
            String dtworksTenantId = config.containsKey("tenant_id") ? (String) config.get("tenant_id") : "";
            //user access details from dtworks user config
            String accessToken = (userAccessMap.containsKey("access_token")) ? (String) userAccessMap.get("access_token") : "";
            String refreshToken = (userAccessMap.containsKey("refresh_token")) ? (String) userAccessMap.get("refresh_token") : "";
            String expirationTime = userAccessMap.containsKey("expiration_time") && Utils.isNotNull(userAccessMap.get("expiration_time")) ?
                    userAccessMap.get("expiration_time").toString() : "";
            int userId = (userAccessMap.containsKey("userId")) ? (int) userAccessMap.get("userId") : 0;

            if(Utils.emptyString(accessToken)){
                //login to dtworks and get the access token in case of empty access token - this will be master account login
                renewalMap = loginToDtworks(config, dtworksTenantId, renewalMap);
                renewalMap.put("tenantId",dtworksTenantId);
            }else {
                expirationTime = expirationTime.substring(0, 19);
                Map responseMap = verifyToken(expirationTime);
                logger.info("responseMap {} ", responseMap);
                //TODO epiratetime is valid but access token is not autorized since the same token is used by another user via dashboard
                if (responseMap.containsKey("status") && responseMap.get("status").equals(401)) {
                    //renew the access token
                    renewalMap = renewUserAccessToken(dtworksTenantId, refreshToken, userId);
                }
                renewalMap.put("tenantId", dtworksTenantId);
            }
        }catch (Exception e){
            logger.error("Error while checking the user access token expiration", e);
        }
        logger.info("end checking the user access token expiration");
        return renewalMap;
    }


    /**
     *
     * @param expirationTime
     * @return
     */
    public Map verifyToken(String expirationTime){
        logger.info("start verifying the token");
        logger.info("expirationTime {} ",expirationTime);
        Map responseMap = new HashMap();
        try{

            String currentDate = Utils.getCurrentUTCDateAsString();
            logger.info("currentDate {} ",currentDate);
            boolean isExpired = Utils.compareDates(currentDate,expirationTime);
            //current date is greater than the expiration time which means token expired

            if(isExpired){
                logger.info("token expired");
                responseMap.put("status", 401);
            }else{
                logger.info("token not expired");
                responseMap.put("status", 200);
            }
        }catch (Exception e){
            logger.error("Error while verifying the token", e);
        }
        logger.info("end verifying the token");
        return responseMap;
    }


    public Map renewTheAccessToken(String tenantId, String refreshToken, Map config){
        logger.info("start the access token renewal");
        Map returnMap = new HashMap();
        try{
            String uri = dtworksUrl + "/auth/refresh-token";
            Map inputMap = new HashMap();
            inputMap.put("refreshToken", refreshToken);
            Map responseMap = this.callDtWorksRestApi( uri,  inputMap, HttpMethod.POST, tenantId, "");
            if(responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == HttpStatus.UNAUTHORIZED.value()){
                //TODO handle the case if the refresh token is expired
                returnMap = loginToDtworks(config, tenantId, returnMap);
            }
            else {
                Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
                String mewAccessToken = (dataMap.containsKey("accessToken")) ? (String) dataMap.get("accessToken") : "";
                String newRefreshToken = (dataMap.containsKey("refreshToken")) ? (String) dataMap.get("refreshToken") : "";
                Timestamp expirationTimeStamp = (dataMap.containsKey("expireAt")) ? new Timestamp((Long) dataMap.get("expireAt")) : new Timestamp(System.currentTimeMillis());
                logger.info("expirationTimeStamp {} ", expirationTimeStamp);
                returnMap.put("accessToken", mewAccessToken);
                returnMap.put("refreshToken", newRefreshToken);
                returnMap.put("expirationTime", expirationTimeStamp);
                String dateTime = Utils.getFormattedDateFromTimestamp(expirationTimeStamp.toString());
                dtWorksDAO.updateDTWorksConfigs(mewAccessToken, newRefreshToken, dateTime, tenantId);
            }

        }catch (Exception e){
            logger.error("Error while renewing the access token", e);
        }
        logger.info("end the access token renewal");
        return returnMap;
    }

    public Map getUserAccessByUserId(int createdBy) {
        Map userAccessMap = new HashMap();
        try {
            userAccessMap = dtWorksDAO.getUserAccessByUserId(createdBy);
        } catch (Exception e) {
            logger.error("exception at getUserAccessByUserId. Msg {}", e.getMessage());
        }
        return userAccessMap;
    }


    public Map renewUserAccessToken(String tenantId, String userRefreshToken, int userID){
        logger.info("start the access token renewal");
        Map returnMap = new HashMap();
        try{
            String uri = dtworksUrl + "/auth/refresh-token";
            Map inputMap = new HashMap();
            inputMap.put("refreshToken", userRefreshToken);
            Map responseMap = this.callDtWorksRestApi( uri,  inputMap, HttpMethod.POST, tenantId, "");
            if(responseMap.containsKey("status") && (int) responseMap.get("status") == HttpStatus.OK.value()){
                //TODO handle the case if the refresh token is expired
               /* returnMap = loginToDtworks(config, tenantId, returnMap);
            }
            else {*/
                Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
                String mewAccessToken = (dataMap.containsKey("accessToken")) ? (String) dataMap.get("accessToken") : "";
                String newRefreshToken = (dataMap.containsKey("refreshToken")) ? (String) dataMap.get("refreshToken") : "";
                Timestamp expirationTimeStamp = (dataMap.containsKey("expireAt")) ? new Timestamp((Long) dataMap.get("expireAt")) : new Timestamp(System.currentTimeMillis());
                logger.info("expirationTimeStamp {} ", expirationTimeStamp);
                returnMap.put("accessToken", mewAccessToken);
                returnMap.put("refreshToken", newRefreshToken);
                returnMap.put("expirationTime", expirationTimeStamp);
                String dateTime = Utils.getFormattedDateFromTimestamp(expirationTimeStamp.toString());
                dtWorksDAO.updateDTWorksUserConfigs(mewAccessToken, newRefreshToken, dateTime, userID);
            }

        }catch (Exception e){
            logger.error("Error while renewing the access token", e);
        }
        logger.info("end the access token renewal");
        return returnMap;
    }

    /**
     * method to login to dtworks and get the access tokens. Update the access and refresh tokens in the config table
     * @param config
     * @param dtworksTenantId
     * @return
     */
    private Map loginToDtworks(Map config, String dtworksTenantId, Map<String, Object> renewalMap){
        String userEmail = (config.containsKey("user_email")) ? (String) config.get("user_email") : "";
        String encodedPwd = (config.containsKey("password")) ? (String) config.get("password") : "";
        String password = Utils.decodeString(encodedPwd);

        //login the user and get the access token
        DTWorksLoginBean dtWorksLoginBean = new DTWorksLoginBean();
        dtWorksLoginBean.setEmail(userEmail);
        dtWorksLoginBean.setPassword(password);
        dtWorksLoginBean.setTenantId(dtworksTenantId);
        Map responseMap = authenticateDtworksUser(dtWorksLoginBean);

        if (responseMap.containsKey("status") && (int) responseMap.get("status") == 200) {
            Map configDataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
            if(configDataMap.containsKey("anotherSession")) {
                int dtWorksUserId = configDataMap.containsKey("userId") ? Integer.parseInt(configDataMap.get("userId").toString()) : 0;
                //TODO handle if the user is already logged in to another session case
                logoutDtworksUser(dtWorksUserId, dtworksTenantId);
                //login the user and get the access token
                renewalMap = loginToDtworks(config, dtworksTenantId, renewalMap);
            }else {
                renewalMap = updateDtWorksConfig(configDataMap, dtworksTenantId);
                renewalMap.put("tenantId", dtworksTenantId);
            }

        }else{
            logger.error("Error while authenticating the user");

        }
        return renewalMap;
    }
    public Map logoutDtworksUser(int userId, String tenantId) {
        String uri = dtworksUrl + "/auth/logout/"+userId;
        return this.callDtWorksRestApi(uri, new HashMap<>(),  HttpMethod.DELETE,  tenantId, "");
    }


    private Map updateDtWorksConfig(Map dataMap, String dtworksTenantId) {
        Map returnMap = new HashMap();
        try {
            String accessToken = (dataMap.containsKey("accessToken")) ? (String) dataMap.get("accessToken") : "";
            String refreshToken = (dataMap.containsKey("refreshToken")) ? (String) dataMap.get("refreshToken") : "";
            Timestamp expirationTimeStamp = (dataMap.containsKey("tokenExpiresAt")) ? new Timestamp((Long) dataMap.get("tokenExpiresAt")) : new Timestamp(System.currentTimeMillis());
            logger.info("expirationTimeStamp {} ", expirationTimeStamp);
            String expirationTime = Utils.getFormattedDateFromTimestamp(expirationTimeStamp.toString());

            returnMap.put("accessToken", accessToken);
            returnMap.put("refreshToken", refreshToken);
            returnMap.put("expirationTime", expirationTimeStamp);
            dtWorksDAO.updateDTWorksConfigs(accessToken, refreshToken, expirationTime, dtworksTenantId);
        } catch (Exception e) {
            logger.error("Error while updating the dtworks config", e);
        }
        return returnMap;
    }

    public Map authenticateDtworksUser(DTWorksLoginBean data) {

        String uri = dtworksUrl + "/auth/login";
        Map inputMap = new HashMap();
        inputMap.put("loginId", data.getEmail());
        inputMap.put("password", data.getPassword());
        inputMap.put("channel", "UAM_WEB"); //TODO: need to verify the channel

        return this.callDtWorksRestApi(uri,  inputMap, HttpMethod.POST,  data.getTenantId(), "");

    }

/*
    public Map callDtWorksRenew(String uri, Map<String, Object> inputMap, HttpMethod method, String refreshToken, String tenantId){

        logger.info("begin dt works renew api");
        Map respMap = new HashMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("x-tenant-id", tenantId);

        JSONObject jsonObject = new JSONObject(inputMap);
        String inputJson = jsonObject.toString();
        logger.info("input json {}", inputJson);
        logger.info("uri {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);

        try {

            ResponseEntity<Object> responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    respMap.put("success", true);
                    respMap.put("message", "");
                    respMap.put("statusCode", 200);
                }
            }else{
                logger.info("success response");
                respMap.put("success", true);
                respMap.put("message", "");
                respMap.put("statusCode", 200);

            }
            logger.info("end dt works renew api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 401);
            respMap.put("message", "Connection refused");
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error");
            respMap.put("statusCode", 401);
            e.printStackTrace();
        }
        return  respMap;
    }*/

    /**
     * method
     * @param eventResponse
     * @param authMap
     */
    public Map returnDTWorksCustomerId(Map eventResponse, Map<String, Object> authMap) {
        String email = (eventResponse.containsKey("email")) ? (String) eventResponse.get("email") : "";
        Map customerMap = checkIfCustomerExistsInDTWorks(email, authMap);
        if (customerMap.isEmpty() || !customerMap.containsKey("customerId")) {
             customerMap = createDTWorksCustomer(eventResponse, authMap);
        }
        logger.info("customerId {}", customerMap.toString());
        return customerMap;
    }

    /**
     * method to get the profile id from dtworks
     * @param eventResponse
     * @param authMap
     */
    public Map returnDTWorksProfileId(Map eventResponse, Map<String, Object> authMap, String channel) {

        String phn = validateField(eventResponse, Constants.PHONE);
        String email = validateField(eventResponse, Constants.EMAIL);
        boolean isDummyPhone = phn.equals(Constants.DUMMY);
        boolean isDummyEmail = email.equals(Constants.DUMMY);

        if(isDummyPhone && isDummyEmail){ //both email and phone are not availalbe set BCT phn and email
            email = Constants.DUMMY_EMAIL;
            phn = Constants.DUMMY_PHONE;
        }
        //fetch profile details from DT profile table
        Map dtProfileMap = returnProfileIdIfExists(email, phn, eventResponse);
        if(dtProfileMap != null && dtProfileMap.containsKey("profileId")){
            return dtProfileMap;
        }
        //check if profile exists in dtworks
        Map profileMap = checkIfProfileExistsInDTWorks(email, "", authMap);
        if (profileMap.isEmpty() || !profileMap.containsKey("profileId")) {
            profileMap = createDTWorksProfile(eventResponse, authMap, channel, email, phn);
        } else {
            //if profileId exist for email in dtworks and not available in DT, then add the profile details in DT
            storeProfileInDT(profileMap, eventResponse, email.equals(Constants.DUMMY),  phn.equals(Constants.DUMMY), email, phn);
        }

        logger.info("profileId {}", profileMap.toString());
        return profileMap;
    }

    /**
     * method to store the profile details in DT
     * @param email
     * @param phn
     * @param eventResponse
     * @return
     */
    private Map returnProfileIdIfExists(String email, String phn, Map eventResponse) {
        Map<String, Object> dtProfileMap = null;
        try {
            List<String> fieldList = new ArrayList<>();
            List<String> typeList = new ArrayList<>();

            addToListsIfNotEmpty(fieldList, typeList, email, "email");
            addToListsIfNotEmpty(fieldList, typeList, phn, "phone");
            //fetch profile details from DT profile table for both email and phone
            dtProfileMap = triggerDAO.checkIfProfileExistsInDT(fieldList, typeList, eventResponse.get("businessUniqueId").toString());
            if (dtProfileMap != null) {
                List<String> values = dtProfileMap.containsKey("uniqueValues") ?
                        (List) dtProfileMap.get("uniqueValues") : null;
                if (values != null && (values.contains(email) || values.contains(phn))) {
                    List<Integer> profileIds = dtProfileMap.containsKey("profileIds") ?
                            (List) dtProfileMap.get("profileIds") : null;
                    List<String> profileNos = dtProfileMap.containsKey("profileNos") ?
                            (List) dtProfileMap.get("profileNos") : null;
                    int idx = values.indexOf(email) != -1 ? values.indexOf(email) : values.indexOf(phn);

                    if (profileIds != null && profileNos != null && idx > -1) {
                        int profileId =  profileIds.get(idx);
                        String profileNo = profileNos.get(idx);
                        //for same email with different phone number - use the existing profileId
                        List<String> fieldList1 = new ArrayList<>();
                        List<String> typeList1 = new ArrayList<>();
                        addToListsIfNotExist(fieldList1, typeList1, email, "email", values);
                        addToListsIfNotExist(fieldList1, typeList1, phn, "phone", values);
                        if (fieldList1.size() > 0) {
                            DTWorksProfileBean profileBean = new DTWorksProfileBean();
                            profileBean.setUniqueValue(fieldList1);
                            profileBean.setType(typeList1);
                            profileBean.setProfileId(profileId);
                            profileBean.setProfileNo(profileNo);
                            profileBean.setBusinessUniqueIdById((String) eventResponse.get("businessUniqueId"));
                            profileBean.setToken((String) eventResponse.get("token"));
                            triggerDAO.createProfileInDT(profileBean);
                        }

                        //if profileId exist then return the profileId
                        dtProfileMap.put("profileId", profileId);
                        dtProfileMap.put("profileNo", profileNo);
                        return dtProfileMap;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error while fetching the profile details from DT, msg {}", e.getMessage());
        }
        return dtProfileMap;
    }
    private void addToListsIfNotEmpty(List<String> fieldList, List<String> typeList, String value, String type) {
        if (Utils.notEmptyString(value) && !value.equals(Constants.DUMMY)) {
            fieldList.add(value);
            typeList.add(type);
        }
    }
    private void addToListsIfNotExist(List<String> fieldList, List<String> typeList, String value,
                                      String type, List<String> uniqueVal) {
        if (Utils.notEmptyString(value) && !uniqueVal.contains(value)) {
            fieldList.add(value);
            typeList.add(type);
        }
    }

    public Map formDtworksInteractionRequest(Map profileMap, Map integrationMetadata, String feedback, String channel, Map personalInfoMap,
                                             Map authMap, Map dtworksMetaData) {
        Map<String, Object> interactionRequest = new HashMap();
        // all are mandatory fields
        String intxnCat = Utils.notEmptyString((String) integrationMetadata.get("interactionCategory")) ? (String)integrationMetadata.get("interactionCategory") : "SERVICE_RELATED";
        String svcCat = Utils.notEmptyString((String) integrationMetadata.get("serviceCategory")) ? (String)integrationMetadata.get("serviceCategory") : "PST_ADMINSRV";
        String priority = Utils.notEmptyString((String) integrationMetadata.get("priority")) ? (String)integrationMetadata.get("priority") : "PRTYLOW";
        interactionRequest.put("profileId", profileMap.containsKey("profileId") ? Integer.parseInt(profileMap.get("profileId").toString()) : 0);
        interactionRequest.put("referenceCategory", "PUBLIC_SERVICES");
        interactionRequest.put("referenceValue", profileMap.containsKey("profileNo") ? (String) profileMap.get("profileNo") : "");
        interactionRequest.put("serviceType", Utils.notEmptyString((String) integrationMetadata.get("serviceType")) ? integrationMetadata.get("serviceType") : "ST_APPMAINTENANCE");
        interactionRequest.put("interactionCategory", Utils.notEmptyString((String) integrationMetadata.get("interactionCategory")) ? integrationMetadata.get("interactionCategory") : "SERVICE_RELATED");
        interactionRequest.put("serviceCategory", Utils.notEmptyString((String) integrationMetadata.get("serviceCategory")) ? integrationMetadata.get("serviceCategory") : "PST_ADMINSRV");
        interactionRequest.put("interactionType", Utils.notEmptyString((String) integrationMetadata.get("interactionType")) ? integrationMetadata.get("interactionType") : "GENERAL");
        interactionRequest.put("channel", "DROPTHOUGHT");
        interactionRequest.put("contactPreference", Collections.singletonList(Constants.DTWORSK_CONTACTPREFERENCE.containsKey(channel.toLowerCase()) ? Constants.DTWORSK_CONTACTPREFERENCE.get(channel.toLowerCase()) : "CNT_PREF_EMAIL")); //optional
        interactionRequest.put("priorityCode", priority);
        interactionRequest.put("remarks", Utils.emptyString(feedback) ? "Ticket from DT" : feedback);
        String statement = (String) integrationMetadata.get("title");
        int statementId = Utils.notEmptyString(statement) && Constants.DTWORSK_STATEMENT.containsKey(statement.toLowerCase()) ?
                Constants.DTWORSK_STATEMENT.get(statement.toLowerCase()) : 0 ;
        if(statementId == 0) {
            statementId = createStatementInDTWorks(statement, intxnCat, svcCat, priority, authMap);
        }
        /*int statementId = Utils.notEmptyString(statement) && Constants.DTWORSK_STATEMENT.containsKey(statement.toLowerCase()) ?
                Constants.DTWORSK_STATEMENT.get(statement.toLowerCase()) : 0 ;
        interactionRequest.put("statement", Constants.DTWORSK_STATEMENT.containsKey(statement.toLowerCase()) ? statement : "Sewage waters are coming out need to be cleaned" ); //Needed for analytics
        interactionRequest.put("statementId", statementId > 0 ? statementId : "17");*/
        if(statementId > 0) {
            interactionRequest.put("statement", statement);
            interactionRequest.put("statementId", statementId);
        }
        interactionRequest.put("currUser", Utils.isNotNull(integrationMetadata.get("assignee")) ? integrationMetadata.get("assignee") : "");

        /*Map addressMap = setAddress(personalInfoMap, dtworksMetaData);
        if(addressMap != null){
            interactionRequest.put("appointAddress", addressMap);
        }*/
        if(personalInfoMap.containsKey("postcode"))
            interactionRequest.put("appointAddress", setAddress(personalInfoMap));

        return interactionRequest;
    }



   /* public Map setAddress(Map<String, String> respondentInfo, Map<String, String> dtworksMetaData) {
        Map<String, String> addressMap = new HashMap();
        addressMap.put("address1", dtworksMetaData.containsKey("address1") && respondentInfo.containsKey(dtworksMetaData.get("address1")) ?
                respondentInfo.get(dtworksMetaData.get("address1")) : "");
        addressMap.put("address2", dtworksMetaData.containsKey("address2") && respondentInfo.containsKey(dtworksMetaData.get("address2")) ?
                respondentInfo.get(dtworksMetaData.get("address2")) : "");
        addressMap.put("city", dtworksMetaData.containsKey("city") && respondentInfo.containsKey(dtworksMetaData.get("city")) ?
                respondentInfo.get(dtworksMetaData.get("city")) : ""); //mandatory
        addressMap.put("state", dtworksMetaData.containsKey("state") && respondentInfo.containsKey(dtworksMetaData.get("state")) ?
                respondentInfo.get(dtworksMetaData.get("state")) : "");//mandatory
        addressMap.put("district", dtworksMetaData.containsKey("district") && respondentInfo.containsKey(dtworksMetaData.get("district")) ?
                respondentInfo.get(dtworksMetaData.get("district")) : "");//mandatory
        addressMap.put("addrZone", dtworksMetaData.containsKey("addrZone") && respondentInfo.containsKey(dtworksMetaData.get("addrZone")) ?
                respondentInfo.get(dtworksMetaData.get("addrZone")) : "");
        addressMap.put("postcode", dtworksMetaData.containsKey("postcode") && respondentInfo.containsKey(dtworksMetaData.get("postcode")) ?
                respondentInfo.get(dtworksMetaData.get("postcode")) : "");//mandatory
        addressMap.put("country", dtworksMetaData.containsKey("country") && respondentInfo.containsKey(dtworksMetaData.get("country")) ?
                respondentInfo.get(dtworksMetaData.get("country")) : "");//mandatory
        if (Utils.notEmptyString(addressMap.get("city")) && Utils.notEmptyString(addressMap.get("state")) &&
                Utils.notEmptyString(addressMap.get("district")) && Utils.notEmptyString(addressMap.get("postcode")) &&
                Utils.notEmptyString(addressMap.get("country"))) {
            return addressMap;
        }
        return null;
    }*/
    public Map setAddress(Map respondentInfo) { //set address for dtworks ticket
        Map addressMap = new HashMap();
        addressMap.put("address1", respondentInfo.containsKey("address1") ?
                respondentInfo.get("address1") : "Terminal");//terminal
        addressMap.put("address2", respondentInfo.containsKey("address2") ?
                respondentInfo.get("address2") : "");//level
//        addressMap.put("address3", "");
        addressMap.put("city", respondentInfo.containsKey("city") ?
                respondentInfo.get("city") : ""); //mandatory
        addressMap.put("state", respondentInfo.containsKey("state") ?
                respondentInfo.get("state") : "");//mandatory
        addressMap.put("district", respondentInfo.containsKey("district") ?
                respondentInfo.get("district") : "");
        addressMap.put("addrZone", respondentInfo.containsKey("Area") ?
                Constants.DTWORSK_ADDRZONE.get(respondentInfo.get("Area")) : "NEAR_THE_SOCIAL_TREE");
        addressMap.put("postcode",  respondentInfo.containsKey("postcode") ?
                respondentInfo.get("postcode") : "");//mandatory
        addressMap.put("country",  respondentInfo.containsKey("country") ?
                respondentInfo.get("country") : "");
        return addressMap;
    }

    /**
     * check if the profile exists in dtworks
     * @param email
     * @return
     */
    public Map checkIfProfileExistsInDTWorks(String email, String phone, Map<String, Object> authMap) {
        logger.info("checkIfProfileExistsInDTWorks - start");
        Map customerMap = new HashMap();
        String url = dtworksUrl + "/profile/search";
        try {
            Map profileReq = new HashMap();
            if(Utils.notEmptyString(email))
                profileReq.put("emailId", email);
            if(Utils.notEmptyString(phone))
                profileReq.put("mobileNo", phone);

            String accessToken = (authMap.containsKey("accessToken")) ? (String) authMap.get("accessToken") : "";
            String tenantId = (authMap.containsKey("tenantId")) ? (String) authMap.get("tenantId") : "";

            Map responseMap = this.callDtWorksRestApi(url, profileReq, HttpMethod.POST, tenantId, accessToken);
            if (responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 200) {
                Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap<>();
                if (dataMap.containsKey("rows")) {
                    customerMap = (Map)((List) dataMap.get("rows")).get(0);
                }
            }
            logger.info("checkIfProfileExistsInDTWorks - end");
        }catch (Exception e){
            logger.error("checkIfProfileExistsInDTWorks() - Error while checking if customer exists in dtworks {}", e);
        }
        return customerMap;
    }

    /**
     * method to create the profile in dtworks - profile is nothing but the respondents of a survey
     * @param eventResponse
     * @param authMap
     */
    public Map createDTWorksProfile(Map eventResponse, Map authMap, String channel, String email, String phone) {
        String url = dtworksUrl + "/profile/create";
        Map respMap = new HashMap();
        try {
            /*String phone = validateField(eventResponse, Constants.PHONE);
            String email = validateField(eventResponse, Constants.EMAIL);*/

            boolean isDummyPhone = phone.equals(Constants.DUMMY);
            boolean isDummyEmail = email.equals(Constants.DUMMY);
           /* if(isDummyPhone && isDummyEmail){ //both email and phone are not availalbe set BCT phn and email
               email = Constants.DUMMY_EMAIL;
               phone = Constants.DUMMY_PHONE;
            }else*/
            if(isDummyPhone) { //phone is not available, then set random unique number
                phone = uniqueStringUsingTimestamp();
            }else if(isDummyEmail){ //email is not available, then set random unique emailid
                email = uniqueStringUsingTimestamp()+"@yopmail.com";
            }

            String name = eventResponse.containsKey("name") ? (String) eventResponse.get("name") : "DT";
            Map contactPayload = new HashMap();
            contactPayload.put("isPrimary", true);
            contactPayload.put("mobileNo", phone);//dummy phone number
            contactPayload.put("mobilePrefix", "+91");//dummy phone prefix
            contactPayload.put("emailId", email);//dummy email
            contactPayload.put("firstName", name);
            contactPayload.put("lastName", name);
            contactPayload.put("contactType", "CNTMOB");

            Map customerDetailsMap = new HashMap();
            contactPayload.put("lastName", name);
            customerDetailsMap.put("firstName", name);
            customerDetailsMap.put("contact", contactPayload);

            Map dtWorksCustomerRequest = new HashMap();
            dtWorksCustomerRequest.put("lastName", name);
            dtWorksCustomerRequest.put("firstName", name);
            dtWorksCustomerRequest.put("contact", contactPayload);
            dtWorksCustomerRequest.put("profileCategory", "BUS");//Business
            dtWorksCustomerRequest.put("projectMapping", new ArrayList<>());
            dtWorksCustomerRequest.put("contactPreferences", Collections.singletonList(Constants.DTWORSK_CONTACTPREFERENCE.containsKey(channel.toLowerCase()) ? Constants.DTWORSK_CONTACTPREFERENCE.get(channel.toLowerCase()) : "CNT_PREF_EMAIL")); //optional


            String accessToken = (authMap.containsKey("accessToken")) ? (String) authMap.get("accessToken") : "";
            String tenantId = (authMap.containsKey("tenantId")) ? (String) authMap.get("tenantId") : "";
            Map responseMap = this.callDtWorksRestApi(url, dtWorksCustomerRequest, HttpMethod.POST, tenantId, accessToken);
            if (responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 200) {
                Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap<>();
                if (dataMap.containsKey("rows")) {
                    respMap = (Map) ((List) dataMap.get("rows")).get(0);
                } else {
                    respMap = dataMap;
                }
                //store the profile details in DT
                storeProfileInDT(respMap, eventResponse, isDummyEmail, isDummyPhone, email, phone);

            } else if (responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 422 ||
                    responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 409) {
                respMap = checkIfProfileExistsInDTWorks("", phone, authMap);
                //if profileId exist for phone in dtworks and not available in DT, then add the profile details in DT
                storeProfileInDT(respMap, eventResponse, isDummyEmail, isDummyPhone, email, phone);
            }
        } catch (Exception e) {
            logger.error("createDTWorksCustomer() - Error while creating the customer in dtworks {}", e);

        }
        return respMap;
    }

    /**
     * method to return the unique string using timestamp for random phone number and email id - for profile creation
     * @return
     */
    private String uniqueStringUsingTimestamp(){
        // Get the current timestamp in milliseconds
        long timestamp = System.currentTimeMillis();

        // Format the timestamp into a readable format
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        String formattedTimestamp = sdf.format(new Date(timestamp));
        return formattedTimestamp;
    }

    /**
     * method to store the profile in dtworks
     * @param respMap
     * @param eventResponse
     * @param isDummyEmail
     * @param isDummyPhn
     * @param email
     * @param phone
     */
    public void storeProfileInDT(Map respMap, Map eventResponse, boolean isDummyEmail, boolean isDummyPhn,
                                 String email, String phone) {
        try {
            if(respMap.containsKey("profileId") && respMap.containsKey("profileNo")) {
                String token = (eventResponse.containsKey("token")) ? (String) eventResponse.get("token") : "";
                String businessUniqueId = (eventResponse.containsKey("businessUniqueId")) ? (String) eventResponse.get("businessUniqueId") : "";
                int profileId = Integer.parseInt((String) respMap.get("profileId"));
                String profileNo = (String) respMap.get("profileNo");

                List<String> valList = new ArrayList<>();
                List<String> typeList = new ArrayList<>();
                addToLists(valList, typeList, isDummyEmail, email, "email");
                addToLists(valList, typeList, isDummyPhn, phone, "phone");

                if(valList.size() > 0 && typeList.size() > 0) {
                    DTWorksProfileBean dtWorksProfileBean = new DTWorksProfileBean();
                    dtWorksProfileBean.setProfileId(profileId);
                    dtWorksProfileBean.setProfileNo(profileNo);
                    dtWorksProfileBean.setToken(token);
                    dtWorksProfileBean.setUniqueValue(valList);
                    dtWorksProfileBean.setType(typeList);
                    dtWorksProfileBean.setBusinessUniqueIdById(businessUniqueId);
                    triggerDAO.createProfileInDT(dtWorksProfileBean);
                }
            }
        }catch (Exception e){
            logger.error("storeProfileInDT() - Error while storing the profile in dtworks {}", e);
        }
    }

    /**
     * validate the field and return the value
     * @param eventResponse
     * @param fieldKey
     * @return
     */
    private static String validateField(Map<String, Object> eventResponse, String fieldKey) {
        if (eventResponse.containsKey(fieldKey) && Utils.notEmptyString((String) eventResponse.get(fieldKey))) {
            if(fieldKey.equals(Constants.PHONE))
                return Utils.removeExtraChar((String) eventResponse.get(fieldKey));
            else
                return (String) eventResponse.get(fieldKey);
        } else {
            return Constants.DUMMY;
        }
    }
    private void addToLists(List<String> valList, List<String> typeList, boolean isDummyVal, String value, String type) {
        if (!isDummyVal) {
            valList.add(value);
            typeList.add(type);
        }
    }

    /**
     * check if the customer exists in dtworks
     * @param email
     * @return
     */
    public Map checkIfCustomerExistsInDTWorks(String email, Map<String, Object> authMap) {
        Map customerMap = new HashMap();
        email = Utils.emptyString(email) ? Constants.DUMMY_EMAIL : email;
        String url = dtworksUrl + "/customer/search?q="+email;
        try {
            if(Utils.notEmptyString(email)) {
                String accessToken = (authMap.containsKey("accessToken")) ? (String) authMap.get("accessToken") : "";
                String tenantId = (authMap.containsKey("tenantId")) ? (String) authMap.get("tenantId") : "";

                Map responseMap = this.callDtWorksRestApi(url, new HashMap<>(), HttpMethod.GET, tenantId, accessToken);
                if (responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 200) {
                    List dataList = (responseMap.containsKey("data")) ? (List) responseMap.get("data") : new ArrayList();
                    if (dataList != null && dataList.size() > 0) {
                        customerMap = (Map) dataList.get(0);
                    }
                }
            }
        }catch (Exception e){
            logger.error("checkIfCustomerExistsInDTWorks() - Error while checking if customer exists in dtworks {}", e);
        }
        return customerMap;
    }

    /**
     * method to create the customer in dtworks - customer is nothing but the respondents of a survey
     * @param eventResponse
     * @param authMap
     */
    public Map createDTWorksCustomer(Map eventResponse, Map authMap){
        String url = dtworksUrl + "/customer/create";
        Map respMap = new HashMap();
        try {
            Map contactPayload = new HashMap();
            String phone = eventResponse.containsKey("phone") ? (String) eventResponse.get("phone") : Constants.DUMMY_PHONE;
            contactPayload.put("mobileNo", phone);//dummy phone number
            contactPayload.put("emailId", eventResponse.containsKey("email") ? eventResponse.get("email") : Constants.DUMMY_EMAIL);//dummy email
            Map customerDetailsMap = new HashMap();
            customerDetailsMap.put("source", "CREATE_INTERACTION");
            customerDetailsMap.put("firstName", eventResponse.containsKey("name") ? eventResponse.get("name") : "Changi");
            customerDetailsMap.put("contactPayload", contactPayload);
            Map dtWorksCustomerRequest = new HashMap();
            dtWorksCustomerRequest.put("details", customerDetailsMap);

//            Map authMap = verifyAndRenewToken();
            String accessToken = (authMap.containsKey("accessToken")) ? (String) authMap.get("accessToken") : "";
            String tenantId = (authMap.containsKey("tenantId")) ? (String) authMap.get("tenantId") : "";
            Map responseMap = this.callDtWorksRestApi(url, dtWorksCustomerRequest, HttpMethod.POST, tenantId, accessToken);
            if(responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 200){
                respMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
            }else if(responseMap.containsKey("status") && Integer.parseInt(responseMap.get("status").toString()) == 422){
                respMap = checkIfCustomerExistsInDTWorks(phone, authMap);
            }
        } catch (Exception e) {
            logger.error("createDTWorksCustomer() - Error while creating the customer in dtworks {}", e);

        }
        return respMap;
    }

    /**
     * method to create the interation in dtworks - interaction is nothing ticket raised by the customer
     * @param dtWorksInteractionRequest
     * @param authMap
     */
    public Map createDTWorksInteraction(Map dtWorksInteractionRequest, Map authMap, Map userMap){
        String url = dtworksUrl + "/interaction/dropthought/create";
        Map respMap = new HashMap();
        try {
            String accessToken = (authMap.containsKey("accessToken")) ? (String) authMap.get("accessToken") : "";
            String tenantId = (authMap.containsKey("tenantId")) ? (String) authMap.get("tenantId") : "";

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("x-tenant-id", tenantId);
            headers.add("Authorization", accessToken);
            if(userMap.containsKey("userId") && userMap.containsKey("roleId") && userMap.containsKey("deptId")) {
                headers.add("x-request-from", "DROPTHOUGHT");
                headers.add("x-dt-user-id", userMap.get("userId").toString());
                headers.add("x-dt-role-id", userMap.get("roleId").toString());
                headers.add("x-dt-department-id", userMap.get("deptId").toString());
            }
            respMap = this.callDtWorksRestApi(url, dtWorksInteractionRequest, HttpMethod.POST, headers);
        } catch (Exception e) {
            logger.error("createDTWorksCustomer() - Error while creating the customer in dtworks {}", e);
        }
        return respMap;
    }


    /**
     * rest api call to dtworks
     * @param uri
     * @param inputMap
     * @param method
     * @param tenantId
     * @param accessToken
     * @return
     */
    public Map callDtWorksRestApi(String uri, Map<String, Object> inputMap, HttpMethod method, String tenantId, String accessToken){

        logger.info("begin dt works rest api");
        Map respMap = new HashMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("x-tenant-id", tenantId);
        if(Utils.notEmptyString(accessToken))
            headers.add("Authorization", accessToken);

        JSONObject jsonObject = new JSONObject(inputMap);
        String inputJson = jsonObject.toString();
        logger.info("input json {}", inputJson);
        logger.info("uri {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        ResponseEntity<Object> responseEntity = null;
        try {

             responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    logger.info("success response");
                }
            }else{
                respMap.put("success", false);
            }
            logger.info("end dt works rest api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("status", 503);
            respMap.put("message", "Connection refused");
        }
        catch (HttpClientErrorException ex){
            ex.printStackTrace();
            logger.error("Client error. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message",  responseEntity!=null && responseEntity.getBody() != null ?
                    responseEntity.getBody().toString() : ex.getStatusCode());
            respMap.put("status", ex.getRawStatusCode());
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error");
            respMap.put("status", 500);
            e.printStackTrace();
        }
        return  respMap;
    }

    /**
     * rest api call to dtworks
     * @param uri
     * @param inputMap
     * @param method
     * @return
     */
    public Map callDtWorksRestApi(String uri, Map<String, Object> inputMap, HttpMethod method,
                                  HttpHeaders headers){

        logger.info("begin dt works rest api");
        Map respMap = new HashMap();


        JSONObject jsonObject = new JSONObject(inputMap);
        String inputJson = jsonObject.toString();
        logger.info("input json {}", inputJson);
        logger.info("uri {}", uri);

        HttpEntity<String> entity = new HttpEntity<String>(inputJson, headers);
        ResponseEntity<Object> responseEntity = null;
        try {

            responseEntity = restTemplate.exchange(uri, method, entity, Object.class);
            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                if (responseEntity.getBody() != null) {
                    logger.info(responseEntity.getBody().toString());
                    respMap = (Map) responseEntity.getBody();
                    logger.info("success response");
                }
            }else{
                respMap.put("success", false);
            }
            logger.info("end dt works rest api");
        }
        catch (ResourceAccessException ex){
            ex.printStackTrace();
            logger.error("service refused to connect. uri {}", uri);
            respMap.put("success",false);
            respMap.put("status", 503);
            respMap.put("message", "Connection refused. " + ex.getMessage());
        }
        catch (HttpClientErrorException ex){
            ex.printStackTrace();
            logger.error("Client error. uri {}", uri);
            respMap.put("success",false);
            respMap.put("message",  responseEntity!=null && responseEntity.getBody() != null ?
                    responseEntity.getBody().toString() : ex.getMessage());
            respMap.put("status", ex.getRawStatusCode());
        }
        catch (Exception e){
            respMap.put("success", false);
            respMap.put("message", "internal error - "+ e.getMessage());
            respMap.put("status", 500);
            e.printStackTrace();
        }
        return  respMap;
    }

   /* public List<Map<String, Object>> updateExpiredUserAccessTokens() {
        List<Map<String, Object>> userAccessList = new ArrayList<>();
        try {
            userAccessList = dtWorksDAO.getExpiredUserAccessTokes();
            Map dtworksConfig = retrieveDTWorksConfigs();
            String tenantId = (dtworksConfig.containsKey("tenant_id")) ? (String) dtworksConfig.get("tenant_id") : "";

            Iterator itr = userAccessList.iterator();
            while (itr.hasNext()) {
                Map userAccessMap = (Map) itr.next();
                int userId = (userAccessMap.containsKey("dt_user_id")) ? (int) userAccessMap.get("dt_user_id") : 0;
                Map renewalMap = renewUserAccessToken(tenantId, (String) userAccessMap.get("refreshToken"), userId);
                if (renewalMap.containsKey("accessToken")) {
                    Timestamp expirationTimeStamp = (Timestamp) renewalMap.get("expirationTime");
                    String dateTime = Utils.getFormattedDateFromTimestamp(expirationTimeStamp.toString());
                    dtWorksDAO.updateDTWorksUserConfigs((String) renewalMap.get("accessToken"), (String) renewalMap.get("refreshToken"), dateTime, userId);
                }
            }
        } catch (Exception e) {
            logger.error("exception at getExpiredUserAccessTokes. Msg {}", e.getMessage());
        }
        return userAccessList;
    }*/

    public Map getDTWorksUserDetails(int dtWorksUserId, Map tokenMap){
        String uri = dtworksUrl + "/users/search/"+dtWorksUserId;

//        Map tokenMap = verifyAndRenewToken();
        String accessToken = (tokenMap.containsKey("accessToken")) ? (String) tokenMap.get("accessToken") : "";
        String tenantId = (tokenMap.containsKey("tenantId")) ? (String) tokenMap.get("tenantId") : "";
        Map responseMap = this.callDtWorksRestApi(uri, new HashMap<>(), HttpMethod.GET,  tenantId, accessToken);
        if(responseMap.containsKey("status") && (int) responseMap.get("status") == 200){
            Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
            return dataMap;
        }else if(responseMap.containsKey("status") && (int) responseMap.get("status") == 401){
            logger.error("User not found in dtworks. userId {}", dtWorksUserId);
            return null;
        }
        return new HashMap();
    }

    public Map getDTWorksUserDetailsV1(int dtWorksUserId, Map tokenMap){
        String uri = dtworksUrl + "/users/search/?limit=10&page=0";

        Map input = new HashMap();
        input.put("id", "userId");
        input.put("value",String.valueOf(dtWorksUserId));
        input.put("filter","contains" );

        //{"filters":[{"id":"userId","value":"849","filter":"contains"}]}
        Map req = new HashMap();
        req.put("filters", Collections.singletonList(input));
        logger.info(new JSONObject(req).toString());

        String accessToken = (tokenMap.containsKey("accessToken")) ? (String) tokenMap.get("accessToken") : "";
        String tenantId = (tokenMap.containsKey("tenantId")) ? (String) tokenMap.get("tenantId") : "";
        Map responseMap = this.callDtWorksRestApi(uri, req, HttpMethod.POST,  tenantId, accessToken);
        if(responseMap.containsKey("status") && (int) responseMap.get("status") == 200){
            Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
            List<Map<String, Object>>  userDetails = (List) dataMap.get("rows");
            // Use streams to filter the user with userId
            Optional<Map<String, Object>> userOpt = userDetails.stream()
                    .filter(user -> (Integer) user.get("userId") == dtWorksUserId)
                    .findFirst();

            return userOpt.isPresent() ? userOpt.get() : new HashMap();
        }else if(responseMap.containsKey("status") && (int) responseMap.get("status") == 401){
            logger.error("User not found in dtworks. userId {}", dtWorksUserId);
            return null;
        }
        return new HashMap();
    }

    /**
     * api to create statement in dtworks
     * @param statement
     * @param intxnCategory
     * @param srvCategory
     * @param priority
     * @param tokenMap
     * @return
     */
    public int createStatementInDTWorks(String statement, String intxnCategory, String srvCategory, String priority, Map tokenMap){
        String uri = dtworksUrl + "/knowledge-Base/add-statement";

        Map inputMap = new HashMap();

        inputMap.put("intxnCategory", intxnCategory);
        inputMap.put("intxnType", "ACTIVE");
        inputMap.put("serviceCategory", srvCategory);
        inputMap.put("serviceType", "ACTIVE");
        inputMap.put("requestStatement", statement);
        inputMap.put("priorityCode", priority);

        String accessToken = (tokenMap.containsKey("accessToken")) ? (String) tokenMap.get("accessToken") : "";
        String tenantId = (tokenMap.containsKey("tenantId")) ? (String) tokenMap.get("tenantId") : "";
        Map responseMap = this.callDtWorksRestApi(uri, inputMap, HttpMethod.POST,  tenantId, accessToken);
        if(responseMap.containsKey("status") && (int) responseMap.get("status") == 200){
            Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
            return dataMap.containsKey("requestId") && Utils.isNotNull(dataMap.get("requestId")) ?
            Integer.parseInt(dataMap.get("requestId").toString()) : 0;
        }else if(responseMap.containsKey("status") && (int) responseMap.get("status") == 409){
            //TODO need to handle the case when the statement already exists - currently api return only msg not statement id

        }
        return 0;
    }


    public Map searchUserByEmailInDTworks(String email, Map tokenMap){
        Map userDetailsMap = new HashMap();
        String uri = dtworksUrl + "/users/search?limit=10&page=0";
        try {
            String accessToken = (tokenMap.containsKey("accessToken")) ? (String) tokenMap.get("accessToken") : "";
            String tenantId = (tokenMap.containsKey("tenantId")) ? (String) tokenMap.get("tenantId") : "";

            Map input = new HashMap();
            input.put("id", "email");
            input.put("value",email );
            input.put("filter","contains" );

            Map req = new HashMap();
            req.put("filters", Collections.singletonList(input));
            logger.info(new JSONObject(req).toString());

            Map responseMap = this.callDtWorksRestApi(uri, req, HttpMethod.POST, tenantId, accessToken);
            if (responseMap.containsKey("status") && (int) responseMap.get("status") == 200) {
                Map dataMap = (responseMap.containsKey("data")) ? (Map) responseMap.get("data") : new HashMap();
                Map userDetails = (Map) ((List) dataMap.get("rows")).get(0);
                //return userDetails.containsKey("userId") ? Integer.parseInt(userDetails.get("userId").toString()) : 0;

                Map userMapping = (Map) userDetails.get("mappingPayload");
                Map deptRoleMapping = (Map) ((List) userMapping.get("userDeptRoleMapping")).get(0);
                String deptId = (String) deptRoleMapping.get("unitId");
                String roleId = (String) ((List) deptRoleMapping.get("roleId")).get(0);
                userDetailsMap.put("deptId", deptId);
                userDetailsMap.put("roleId", roleId);
                userDetailsMap.put("userId", userDetails.get("userId"));
            }
        }catch (Exception e){
            logger.error("Error while searching the user in dtworks. Msg {}", e.getMessage());
        }
        return userDetailsMap;
    }

    public void recordResponseInDTworks(DTWorksRecordResponseBean recordResponseBean){
        dtWorksDAO.createDTWorksResonseTable(recordResponseBean.getBusinesssUniqueId(), recordResponseBean.getBusinessId() );
        dtWorksDAO.createDTWorksResponseData(recordResponseBean);
    }

    /**
     * method to get the user details from dtworks
     * @param createdBy
     * @param tokenMap
     * @return
     */
    public Map getDtworksUserDetails(int createdBy, Map tokenMap) {
        Map userDetailsMap = new HashMap();
        try {
            if(createdBy == 0)
                return userDetailsMap;
            int dtworksUserId = dtWorksDAO.getDTWorksUserByDTUserId(createdBy);
            Map dtUserMap = this.getDTWorksUserDetailsV1(dtworksUserId, tokenMap);
            if(dtUserMap == null){
                return null; //unauthorized
            }

            if (dtUserMap.containsKey("userId") && dtUserMap.containsKey("mappingPayload")) {
                Map userMapping = (Map) dtUserMap.get("mappingPayload");
                Map deptRoleMapping = (Map) ((List) userMapping.get("userDeptRoleMapping")).get(0);;
                userDetailsMap.put("deptId", deptRoleMapping.get("unitId"));
                userDetailsMap.put("roleId", ((List) deptRoleMapping.get("roleId")).get(0));
                userDetailsMap.put("userId", dtUserMap.get("userId"));
                logger.info("usermapping {}", userDetailsMap);
            }
        } catch (Exception e) {
            logger.error("exception at getUserAccessByUserId. Msg {}", e.getMessage());
        }
        return userDetailsMap;
    }

    /**
     * method to get the user details from dtworks
     * @param users
     * @return
     */
    public List fetchDTworksMigratedUsers(List users) {
        List migratedUsers = new ArrayList();
        try {
            migratedUsers = dtWorksDAO.getDTWorksMigratedUsers(users);
        } catch (Exception e) {
            logger.error("exception at fetchDTworksMigratedUsers. Msg {}", e.getMessage());
        }
        return migratedUsers;
    }
}
