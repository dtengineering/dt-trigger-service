package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)

public class TriggerListHeaderData implements Serializable {

    @NotBlank(message = "header cannot be null or empty")
    @JsonProperty("header")
    private List header;
    private List metaData;
    private List<Boolean> phiData;
    @NotBlank(message = "list name cannot be null or empty")
    @JsonProperty("name")
    private String name;

    public List getHeader() {
        return header;
    }

    public void setHeader(List header) {
        this.header = header;
    }

    public List getMetaData() {
        return metaData;
    }

    public void setMetaData(List metaData) {
        this.metaData = metaData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Boolean> getPhiData() { return phiData; }

    public void setPhiData(List<Boolean> phiData) { this.phiData = phiData; }
}
