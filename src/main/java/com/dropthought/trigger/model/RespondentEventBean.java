package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RespondentEventBean implements Serializable {

    @JsonProperty("questionId")
    private String questionId;

    @JsonProperty("answers")
    private String answers;

    @JsonProperty("category")
    private List category;

    @JsonProperty("questionText")
    private String questionText;

    @JsonProperty("npsText")
    private String npsText;

    @JsonProperty("npsValue")
    private String npsValue;

    @JsonProperty("questionType")
    private String questionType;

    @JsonProperty("subType")
    private String subType;

    @JsonProperty("metricName")
    private String metricName;

    @JsonProperty("metricId")
    private String metricId;

    @JsonProperty("metricScore")
    private String metricScore;

    @JsonProperty("metricScale")
    private String metricScale;


    public String getMetricName() {  return metricName;  }

    public void setMetricName(String metricName) { this.metricName = metricName;   }

    public String getMetricId() { return metricId;  }

    public void setMetricId(String metricId) { this.metricId = metricId; }

    public String getMetricScore() { return metricScore;  }

    public void setMetricScore(String metricScore) { this.metricScore = metricScore;  }

    public String getMetricScale() { return metricScale;  }

    public void setMetricScale(String metrciScale) { this.metricScale = metrciScale; }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }

    public List getCategory() {
        return category;
    }

    public void setCategory(List category) {
        this.category = category;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getNpsText() {
        return npsText;
    }

    public void setNpsText(String npsText) {
        this.npsText = npsText;
    }

    public String getNpsValue() { return npsValue; }

    public void setNpsValue(String npsValue) { this.npsValue = npsValue; }

    public String getQuestionType() { return questionType; }

    public void setQuestionType(String questionType) { this.questionType = questionType; }

    public String getSubType() { return subType; }

    public void setSubType(String subType) { this.subType = subType; }
}
