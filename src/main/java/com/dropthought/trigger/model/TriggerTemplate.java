package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL) /*Ignore null value field from the response*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerTemplate implements Serializable {

    @NotBlank(message = "templateName cannot be null or empty")
    @JsonProperty("templateName")
    private String templateName;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid templateId")
    @NotBlank(message = "templateId is required")
    @JsonProperty("templateId")
    private String templateId;

    @JsonProperty("triggerCondition")
    private String triggerCondition;

    @NotBlank(message = "Trigger name cannot be null or empty")
    @JsonProperty("triggerName")
    private String triggerName;

    @JsonProperty("status")
    private String status;

    @JsonProperty("businessId")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUniqueId")
    @NotBlank(message = "businessUniqueId is required")
    private String businessUniqueId;

    @JsonProperty("userId")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId")
    @NotBlank(message = "userId is required")
    private String userId;

    private String lastModified;
    private String triggerId;

    @JsonProperty("frequency")
    private String frequency;

    @JsonProperty("timezone")
    private String timezone;

    @JsonProperty("anonymous")
    private Boolean anonymous;

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String surveyName) {
        this.templateName = surveyName;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(String triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBusinessUniqueId() {
        return businessUniqueId;
    }

    public void setBusinessUniqueId(String businessUniqueId) {
        this.businessUniqueId = businessUniqueId;
    }
    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
    public String getUserId() {
        return userId;
    }

    @JsonIgnore
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

}

