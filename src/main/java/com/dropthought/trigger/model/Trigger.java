package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.SerializationUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL) /*Ignore null value field from the response*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class Trigger implements Serializable {

    @NotBlank(message = "surveyName cannot be null or empty")
    @JsonProperty("surveyName")
    private String surveyName;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid SurveyId")
    @NotBlank(message = "surveyId is required")
    @JsonProperty("surveyId")
    private String surveyId;

    @JsonProperty("triggerCondition")
    private String triggerCondition;

    @NotBlank(message = "Trigger name cannot be null or empty")
    @JsonProperty("triggerName")
    private String triggerName;

    @JsonProperty("status")
    private String status;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUniqueId")
    @NotBlank(message = "businessUniqueId is required")
    @JsonProperty("businessId")
    private String businessUniqueId;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId")
    @NotBlank(message = "userId is required")
    @JsonProperty("userId")
    private String userId;

    private String lastModified;
    private String triggerId;
    private String surveyStartDate;
    private String surveyEndDate;
    @JsonProperty("startDate")
    private String triggerStartDate;
    @JsonProperty("endDate")
    private String triggerEndDate;
    @JsonProperty("startTime")
    private String triggerStartTime;
    @JsonProperty("endTime")
    private String triggerEndTime;
    @JsonProperty("frequency")
    private String frequency;
    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("triggerParentCondition")
    private String triggerParentCondition;

    @JsonProperty("anonymous")
    private Boolean anonymous;

    @JsonProperty("surveyFilterId")
    private String surveyFilterId;

    @JsonProperty("triggerFilterUUID")
    private String triggerFilterUUID;

    @JsonProperty("deactivatedConditions")
    private List deactivatedConditions;
    public List getDeactivatedConditions() {
        return deactivatedConditions;
    }

    public void setDeactivatedConditions(List deactivatedConditions) {
        this.deactivatedConditions = deactivatedConditions;
    }


    public String getSurveyFilterId() {
        return surveyFilterId;
    }

    public void setSurveyFilterId(String surveyFilterId) {
        this.surveyFilterId = surveyFilterId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(String triggerCondition) {
        this.triggerCondition = triggerCondition;
    }

    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBusinessUniqueId() {
        return businessUniqueId;
    }

    public void setBusinessUniqueId(String businessUniqueId) {
        this.businessUniqueId = businessUniqueId;
    }
    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }
    public String getUserId() {
        return userId;
    }

    @JsonIgnore
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    public String getSurveyStartDate() {
        return surveyStartDate;
    }

    public void setSurveyStartDate(String surveyStartDate) {
        this.surveyStartDate = surveyStartDate;
    }

    public String getSurveyEndDate() {
        return surveyEndDate;
    }

    public void setSurveyEndDate(String surveyEndDate) {
        this.surveyEndDate = surveyEndDate;
    }

    public String getTriggerStartDate() {
        return triggerStartDate;
    }

    public void setTriggerStartDate(String triggerStartDate) {
        this.triggerStartDate = triggerStartDate;
    }

    public String getTriggerEndDate() {
        return triggerEndDate;
    }

    public void setTriggerEndDate(String triggerEndDate) {
        this.triggerEndDate = triggerEndDate;
    }

    public String getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(String triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public String getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(String triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getTriggerParentCondition() { return triggerParentCondition; }

    public void setTriggerParentCondition(String triggerParentCondition) { this.triggerParentCondition = triggerParentCondition; }

    public Boolean getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Boolean anonymous) {
        this.anonymous = anonymous;
    }

    public String getTriggerFilterUUID() { return triggerFilterUUID; }

    public void setTriggerFilterUUID(String triggerFilterUUID) { this.triggerFilterUUID = triggerFilterUUID; }
}
