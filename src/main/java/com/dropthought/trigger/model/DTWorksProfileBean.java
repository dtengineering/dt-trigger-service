package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

public class DTWorksProfileBean {

        private List<String> uniqueValue;
        private List<String> type;
        private String token;
        private int profileId;
        private String profileNo;
        private String businessUniqueIdById;

        public List<String> getUniqueValue() {
            return uniqueValue;
        }

        public void setUniqueValue(List<String> uniqueValue) {
            this.uniqueValue = uniqueValue;
        }

        public List<String> getType() {
            return type;
        }

        public void setType(List<String> type) {
            this.type = type;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getProfileId() {
            return profileId;
        }

        public void setProfileId(int profileId) {
            this.profileId = profileId;
        }

        public String getProfileNo() {
            return profileNo;
        }

        public void setProfileNo(String profileNo) {
            this.profileNo = profileNo;
        }

        public String getBusinessUniqueIdById() {
            return businessUniqueIdById;
        }

        public void setBusinessUniqueIdById(String businessUniqueIdById) {
            this.businessUniqueIdById = businessUniqueIdById;
        }
    }
