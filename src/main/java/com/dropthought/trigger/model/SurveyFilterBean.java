package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SurveyFilterBean implements Serializable {

    @JsonProperty("filter_id")
    private String filterId;

    @JsonProperty("filter_name")
    private String filterName;

    @JsonProperty("filter_query")
    private String filterQuery;

    @JsonProperty("readable_query")
    private String readableQuery;

    @JsonProperty("survey_id")
    private String surveyId;

    @JsonProperty("created_by")
    private String createdBy;

    @JsonProperty("modified_by")
    private String modifiedBy;

    @JsonProperty("cc")
    private String currentDB;

    @JsonProperty(value = "temporary_filter", defaultValue = "0")
    private String temporaryFilter;

    public String getCurrentDB() {
        return currentDB;
    }

    public void setCurrentDB(String currentDB) {
        this.currentDB = currentDB;
    }

    public String getReadableQuery() {
        return readableQuery;
    }

    public void setReadableQuery(String readableQuery) {
        this.readableQuery = readableQuery;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public String getFilterQuery() {
        return filterQuery;
    }

    public void setFilterQuery(String filterQuery) {
        this.filterQuery = filterQuery;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTemporaryFilter() { return temporaryFilter; }

    public void setTemporaryFilter(String temporaryFilter) { this.temporaryFilter = temporaryFilter; }
}

