package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateTriggerBean implements Serializable {

    @JsonProperty("businessUUID")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUUID")
    @NotBlank(message = "businessUUID is required")
    private String businessUUID;

    @JsonProperty("userUUID")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userUUID")
    @NotBlank(message = "userUUID is required")
    private String userUUID;

    @JsonProperty("timezone")
    private String timezone;

    @JsonProperty("templateTrigger")
    private List templateTrigger;

    public String getUserUUID() { return userUUID; }

    public void setUserUUID(String userUUID) { this.userUUID = userUUID; }

    public List getTemplateTrigger() { return templateTrigger; }

    public void setTemplateTrigger(List templateTrigger) { this.templateTrigger = templateTrigger; }

    public String getBusinessUUID() {
        return businessUUID;
    }

    public void setBusinessUUID(String businessUUID) {
        this.businessUUID = businessUUID;
    }

    public String getTimezone() { return timezone; }

    public void setTimezone(String timezone) { this.timezone = timezone; }
}
