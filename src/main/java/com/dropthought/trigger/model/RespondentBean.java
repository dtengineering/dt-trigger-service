package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@JsonIgnoreProperties(ignoreUnknown = true)
public class RespondentBean implements Serializable {

    @JsonProperty("metaData")
    private Map metaData;

    @JsonProperty("id")
    private String id;

    @JsonProperty("responseDate")
    private String responseDate;

    @JsonProperty("events")
    private List<RespondentEventBean> events;

    @JsonProperty("npsText")
    private String npsText;

    @JsonProperty("tokenType")
    private String tokenType;

    public Map getMetaData() {
        return metaData;
    }

    public void setMetaData(Map metaData) {
        this.metaData = metaData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(String responseDate) {
        this.responseDate = responseDate;
    }

    public List<RespondentEventBean> getEvents() {
        return events;
    }

    public void setEvents(List<RespondentEventBean> events) {
        this.events = events;
    }

    public String getNpsText() {
        return npsText;
    }

    public void setNpsText(String npsText) {
        this.npsText = npsText;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
