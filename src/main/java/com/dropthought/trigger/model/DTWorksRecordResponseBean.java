package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DTWorksRecordResponseBean implements Serializable {
    private String metaDataType;
    private String metaDataValue;
    private String responseData;
    private String token;
    private String businesssUniqueId;
    private int surveyId;
    private String triggerScheduleId;
    private String scheduleId;
    private String triggerCondition;
    private int businessId;

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public String getMetaDataType() {
        return metaDataType;
    }

    public String getTriggerScheduleId() {
        return triggerScheduleId;
    }

    public void setTriggerScheduleId(String triggerScheduleId) {
        this.triggerScheduleId = triggerScheduleId;
    }

    public void setMetaDataType(String metaDataType) {
        this.metaDataType = metaDataType;
    }

    public String getMetaDataValue() {
        return metaDataValue;
    }

    public void setMetaDataValue(String metaDataValue) {
        this.metaDataValue = metaDataValue;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBusinesssUniqueId() {
        return businesssUniqueId;
    }

    public void setBusinesssUniqueId(String businesssUniqueId) {
        this.businesssUniqueId = businesssUniqueId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getTriggerCondition() {
        return triggerCondition;
    }

    public void setTriggerCondition(String triggerCondition) {
        this.triggerCondition = triggerCondition;
    }
}
