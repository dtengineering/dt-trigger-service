package com.dropthought.trigger.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerConfig implements Serializable {

    @JsonProperty("interval")
    private Integer interval;
    @JsonProperty("intervalType")
    private String intervalType;
    @JsonProperty("notifyTime")
    private String notifyTime;
    @JsonProperty("aggregate")
    private boolean aggregate;
    @JsonProperty("frequency")
    private boolean frequency;

    public boolean isAggregate() {
        return aggregate;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public String getIntervalType() {
        return intervalType;
    }

    public void setIntervalType(String intervalType) {
        this.intervalType = intervalType;
    }

    public String getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(String notifyTime) {
        this.notifyTime = notifyTime;
    }

    public boolean getAggregate() {
        return aggregate;
    }

    public void setAggregate(boolean aggregate) {
        this.aggregate = aggregate;
    }

    public boolean getFrequency() { return frequency; }

    public void setFrequency(boolean frequency) { this.frequency = frequency; }
}
