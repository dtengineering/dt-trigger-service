package com.dropthought.trigger.model;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL) /*Ignore null value field from the response*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerTemplateSchedule implements Serializable {

    @NotBlank(message = "frequency cannot be null or empty")
    @JsonProperty("frequency")
    private String frequency;

    private String status;

    @JsonProperty("timezone")
    private String timezone;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId")
    @NotBlank(message = "businessId is required")
    @JsonProperty("businessId")
    private String businessId;

    @JsonProperty("templateId")
    private String templateId;

    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId")
    @NotBlank(message = "triggerId is required")
    @JsonProperty("triggerId")
    private String triggerId;

    @JsonProperty("scheduleId")
    private String scheduleId;

    @NotBlank(message = "channel cannot be null or empty")
    @JsonProperty("channel")
    private String channel;

    @JsonProperty("message")
    private String message;
    @JsonProperty("replyRespondent")
    private String replyToRespondent;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("contacts")
    private List<Object> contacts;

    @JsonProperty("triggerConfig")
    private Object triggerConfig;
    @JsonProperty("addFeedback")
    private boolean addFeedback;
    private String host;

    @JsonProperty("headerData")
    private TriggerListHeaderData headerData;
    @JsonProperty("webhookConfig")
    private TriggerWebhookConfigurationData webhookConfig;

    @JsonProperty("integrationsConfig")
    private Map integrationsConfig;

    private String createdTime;

    private String transactionId;

    public TriggerListHeaderData getHeaderData() {
        return headerData;
    }

    public void setHeaderData(TriggerListHeaderData headerData) {
        this.headerData = headerData;
    }


    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReplyToRespondent() {
        return replyToRespondent;
    }

    public void setReplyToRespondent(String replyToRespondent) { this.replyToRespondent = replyToRespondent; }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Object> getContacts() {
        return contacts;
    }

    public void setContacts(List<Object> contacts) {
        this.contacts = contacts;
    }

    public Object getTriggerConfig() {
        return triggerConfig;
    }

    public void setTriggerConfig(Object triggerConfig) {
        this.triggerConfig = triggerConfig;
    }

    public boolean isAddFeedback() {
        return addFeedback;
    }

    public void setAddFeedback(boolean addFeedback) {
        this.addFeedback = addFeedback;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @AssertTrue(message = "list name and headerData details are mandatory for list channel")
    private boolean isOk() {
        if (channel.equals(Constants.LIST) && (Utils.isNull(headerData) ||  Utils.isNull(headerData.getName()) || Utils.emptyString(headerData.getName())
                || Utils.isNull(headerData.getHeader()) || (headerData.getHeader().size() == 0))) {
            return false;
        }
        return true;
    }

    public String getCreatedTime() { return createdTime; }

    public void setCreatedTime(String createdTime) { this.createdTime = createdTime; }

    public TriggerWebhookConfigurationData getWebhookConfig() {
        return webhookConfig;
    }

    public void setWebhookConfig(TriggerWebhookConfigurationData webhookConfig) {
        this.webhookConfig = webhookConfig;
    }

    public Map getIntegrationsConfig() {
        return integrationsConfig;
    }

    public void setIntegrationsConfig(Map integrationsConfig) {
        this.integrationsConfig = integrationsConfig;
    }
}
