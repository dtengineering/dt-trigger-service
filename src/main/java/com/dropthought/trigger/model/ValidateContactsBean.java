package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidateContactsBean implements Serializable {

    @JsonProperty("surveyIds")
    @NotEmpty(message = "At least 1 surveyId is required")
    @Valid
    private List<Integer> surveyIds;

    @JsonProperty("businessUUID")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUUID")
    @NotBlank(message = "businessUUID is required")
    private String businessUUID;

    public List<Integer> getSurveyIds() {
        return surveyIds;
    }

    public void setSurveyIds(List<Integer> surveyIds) {
        this.surveyIds = surveyIds;
    }

    public String getBusinessUUID() {
        return businessUUID;
    }

    public void setBusinessUUID(String businessUUID) {
        this.businessUUID = businessUUID;
    }
}
