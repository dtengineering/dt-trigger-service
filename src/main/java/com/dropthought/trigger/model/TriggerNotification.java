package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL) /*Ignore null value field from the response*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerNotification implements Serializable {

    @NotBlank(message = "channel cannot be null or empty")
    @JsonProperty("channel")
    private String channel;
//    @NotNull(message = "message cannot be null")
//    @NotEmpty(message = "message cannot be empty")
    @JsonProperty("message")
    private String message;
//    @NotNull(message = "subject cannot be null")
//    @NotEmpty(message = "subject cannot be empty")
    @JsonProperty("subject")
    private String subject;
//    @NotNull(message = "contacts cannot be null")
//    @NotEmpty(message = "contacts cannot be empty")
    @JsonProperty("contacts")
    private List<String> contacts;
//    @NotNull(message = "trigger config cannot be null")
//    @NotEmpty(message = "trigger config cannot be empty")
    @JsonProperty("triggerConfig")
    private TriggerConfig triggerConfig;
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId")
    @NotBlank(message = "scheduleId is required")
    @JsonProperty("scheduleId")
    private String scheduleId;
   /* @NotNull(message = "businessId cannot be null")
    @NotEmpty(message = "businessId cannot be empty")
    @JsonProperty("businessId")
    private String busienssId;*/
    @JsonProperty("status")
    private String status;
    @JsonProperty("addFeedback")
    private boolean addFeedback;
    @JsonProperty("notificationId")
    private String notificationId;
    private String startTime;
    private String conditionQuery;
    private String submissionToken;
    private int businessId;
    private int surveyId;
    private int triggerId;
    private int notifyId;
    private String host;
    private String timezone;
    private String transactionId;
    @JsonProperty("triggerStatus")
    private String triggerStatus;
    @JsonProperty("replyRespondent")
    private String replyToRespondent;

    private String actionType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public void setContacts(List<String> contacts) {
        this.contacts = contacts;
    }

    public TriggerConfig getTriggerConfig() {
        return triggerConfig;
    }

    public void setTriggerConfig(TriggerConfig triggerConfig) {
        this.triggerConfig = triggerConfig;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

/*    public String getBusienssId() {
        return busienssId;
    }

    public void setBusienssId(String busienssId) {
        this.busienssId = busienssId;
    }*/

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAddFeedback() {
        return addFeedback;
    }

    public void setAddFeedback(boolean addFeedback) {
        this.addFeedback = addFeedback;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public String getConditionQuery() {
        return conditionQuery;
    }

    public void setConditionQuery(String conditionQuery) {
        this.conditionQuery = conditionQuery;
    }

    public String getSubmissionToken() {
        return submissionToken;
    }

    public void setSubmissionToken(String submissionToken) {
        this.submissionToken = submissionToken;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public int getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(int triggerId) {
        this.triggerId = triggerId;
    }

    public int getNotifyId() {
        return notifyId;
    }

    public void setNotifyId(int notifyId) {
        this.notifyId = notifyId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTriggerStatus() { return triggerStatus; }

    public void setTriggerStatus(String triggerStatus) { this.triggerStatus = triggerStatus; }

    public String getReplyToRespondent() { return replyToRespondent; }

    public void setReplyToRespondent(String replyToRespondent) { this.replyToRespondent = replyToRespondent; }

    public String getActionType() {return actionType;}

    public void setActionType(String actionType) {this.actionType = actionType;}
}
