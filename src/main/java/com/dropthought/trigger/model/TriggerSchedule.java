package com.dropthought.trigger.model;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.SerializationUtils;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL) /*Ignore null value field from the response*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerSchedule implements Serializable {

    @NotBlank(message = "start date cannot be null or empty")
    @JsonProperty("startDate")
    private String startDate;

    @NotBlank(message = "end date cannot be null or empty")
    @JsonProperty("endDate")
    private String endDate;

    @NotBlank(message = "start time cannot be null or empty")
    @JsonProperty("startTime")
    private String startTime;

    @NotBlank(message = "end time cannot be null or empty")
    @JsonProperty("endTime")
    private String endTime;

    @NotBlank(message = "frequency cannot be null or empty")
    @JsonProperty("frequency")
    private String frequency;
    private String status;
    @JsonProperty("timezone")
    private String timezone;

    @JsonProperty("businessId")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId")
    @NotBlank(message = "businessId is required")
    private String businessId;

    @JsonProperty("surveyId")
   // @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid surveyId")
    private String surveyId;

    @JsonProperty("triggerId")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId")
    @NotBlank(message = "triggerId is required")
    private String triggerId;

    @JsonProperty("scheduleId")
    //@Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId")
    private String scheduleId;

    @NotBlank(message = "channel cannot be null or empty")
    @JsonProperty("channel")
    private String channel;
    @JsonProperty("message")
    private String message;
    @JsonProperty("replyRespondent")
    private String replyToRespondent;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("contacts")
    private List<Object> contacts;
    @JsonProperty("deAcitvatedContacts")
    private List<Object> deAcitvatedContacts;
    @JsonProperty("triggerConfig")
    private Object triggerConfig;
    @JsonProperty("addFeedback")
    private boolean addFeedback;
    private String host;
    private String lastProcessedDate;
    private String lastProcessedTime;
    @JsonProperty("participantGroupId")
    private Integer participantGroupId;
    @JsonProperty("deactivatedConditions")
    private List deactivatedConditions;

    @JsonProperty("headerData")
    private TriggerListHeaderData headerData;

    @JsonProperty("webhookConfig")
    private TriggerWebhookConfigurationData webhookConfig;

    @JsonProperty("integrationsConfig")
    private Map integrationsConfig;

    private String createdTime;

    private String transactionId;

    private String actionMessage;

    // Deep copy using Apache Commons Lang
    public TriggerSchedule deepCopy() {
        return SerializationUtils.clone(this);
    }

    public Integer getParticipantGroupId() {
        return participantGroupId;
    }

    public void setParticipantGroupId(Integer participantGroupId) {
        this.participantGroupId = participantGroupId;
    }

    public TriggerListHeaderData getHeaderData() {
        return headerData;
    }

    public void setHeaderData(TriggerListHeaderData headerData) {
        this.headerData = headerData;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getTriggerId() {
        return triggerId;
    }

    public void setTriggerId(String triggerId) {
        this.triggerId = triggerId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getLastProcessedDate() {
        return lastProcessedDate + " " + lastProcessedTime;
    }

    public void setLastProcessedDate(String lastProcessedDate) {
        this.lastProcessedDate = lastProcessedDate;
    }

    public String getLastProcessedTime() {
        return lastProcessedTime;
    }

    public void setLastProcessedTime(String lastProcessedTime) {
        this.lastProcessedTime = lastProcessedTime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReplyToRespondent() {
        return replyToRespondent;
    }

    public void setReplyToRespondent(String replyToRespondent) { this.replyToRespondent = replyToRespondent; }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Object> getContacts() {
        return contacts;
    }

    public void setContacts(List<Object> contacts) {
        this.contacts = contacts;
    }

    public Object getTriggerConfig() {
        return triggerConfig;
    }

    public void setTriggerConfig(Object triggerConfig) {
        this.triggerConfig = triggerConfig;
    }

    public boolean isAddFeedback() {
        return addFeedback;
    }

    public void setAddFeedback(boolean addFeedback) {
        this.addFeedback = addFeedback;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public List<Object> getDeAcitvatedContacts() {
        return deAcitvatedContacts;
    }

    public void setDeAcitvatedContacts(List<Object> deAcitvatedContacts) {
        this.deAcitvatedContacts = deAcitvatedContacts;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @AssertTrue(message = "list name and headerData details are mandatory for list channel")
    private boolean isOk() {
        if (channel.equals(Constants.LIST) && (Utils.isNull(headerData) ||  Utils.isNull(headerData.getName()) || Utils.emptyString(headerData.getName())
                || Utils.isNull(headerData.getHeader()) || (headerData.getHeader().size() == 0))) {
            return false;
        }
        return true;
    }

    public TriggerWebhookConfigurationData getWebhookConfig() {
        return webhookConfig;
    }

    public void setWebhookConfig(TriggerWebhookConfigurationData webhookConfig) {
        this.webhookConfig = webhookConfig;
    }

    public String getCreatedTime() { return createdTime; }

    public void setCreatedTime(String createdTime) { this.createdTime = createdTime; }

    public void setDeactivatedConditions(List deactivatedConditions) { this.deactivatedConditions = deactivatedConditions; }

    public List getDeactivatedConditions() { return deactivatedConditions; }

    public Map getIntegrationsConfig() {return integrationsConfig;}

    public void setIntegrationsConfig(Map integrationsConfig) {this.integrationsConfig = integrationsConfig;}

    public String getActionMessage() { return actionMessage; }

    public void setActionMessage(String actionMessage) { this.actionMessage = actionMessage;}
}
