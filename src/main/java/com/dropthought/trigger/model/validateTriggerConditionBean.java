package com.dropthought.trigger.model;

import com.dropthought.trigger.constants.DTConstants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class validateTriggerConditionBean implements Serializable {

    @JsonProperty("questionIds")
    @NotEmpty(message = "At least 1 questionId is required")
    @Valid
    private List<String> questionIds;

    @JsonProperty("triggerIds")
    private List<Integer> triggerIds;

    @JsonProperty("businessUUID")
    @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessUUID")
    @NotBlank(message = "businessUUID is required")
    private String businessUUID;

    public List<String> getQuestionIds() { return questionIds; }

    public void setQuestionIds(List<String> questionIds) { this.questionIds = questionIds; }

    public List<Integer> getTriggerIds() { return triggerIds; }

    public void setTriggerIds(List<Integer> triggerIds) { this.triggerIds = triggerIds; }

    public String getBusinessUUID() { return businessUUID; }

    public void setBusinessUUID(String businessUUID) { this.businessUUID = businessUUID; }
}
