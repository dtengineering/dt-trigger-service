package com.dropthought.trigger.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@Profile("notificationWorker")
@Configuration
public class TriggerNotificationWorkerConfiguration implements ApplicationListener<ApplicationReadyEvent> {

    private Logger logger = LoggerFactory.getLogger(TriggerNotificationWorkerConfiguration.class);

    @Value("${notify.trigger.queue}")
    private String notifyQueueName;

    @Value("${notify.trigger.routing.key}")
    private String notifyRoutingKey;

    @Value("${notify.trigger.topic}")
    private String notifyTopicName;

    @Bean
    public TopicExchange notifyReceiverExchange() {
        return new TopicExchange(notifyTopicName);
    }

    @Bean
    public Queue triggerNotifyReceivingQueue() {
        if (notifyQueueName == null) {
            throw new IllegalStateException("No queue to listen to! Please specify the name of the queue to listen to with the property 'subscriber.queue'");
        }
        return new Queue(notifyQueueName);
    }

    @Bean
    public Binding notifyBinding(Queue notifyReceivingQueue, TopicExchange notifyReceiverExchange) {
        if (notifyRoutingKey == null) {
            throw new IllegalStateException("No events to listen to! Please specify the routing key for the events to listen to with the property 'subscriber.notifyRoutingKey' (see NotifyTrigger for available routing keys).");
        }
        return BindingBuilder
                .bind(notifyReceivingQueue)
                .to(notifyReceiverExchange)
                .with(notifyRoutingKey);
    }

    @Bean
    public SimpleMessageListenerContainer notifyContainer(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter triggerListenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(notifyQueueName);
        container.setMessageListener(triggerListenerAdapter);
        return container;
    }

    @Bean
    public MessageListenerAdapter triggerListenerAdapter(TriggerNotificationWorker notificationWorker, SimpleMessageConverter triggerNotificationModelConverters) {
        MessageListenerAdapter listenerAdapter = new MessageListenerAdapter(notificationWorker, "receive");
        listenerAdapter.setMessageConverter(triggerNotificationModelConverters);
        return listenerAdapter;
    }

    @Bean
    public TriggerNotificationWorker triggerNotifyReceiver() {
        return new TriggerNotificationWorker();
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        logger.info("SUBSCRIBING TO TRIGGER NOTIFY MATCHING KEY '{}' FROM QUEUE '{}'!", notifyRoutingKey, notifyQueueName);
    }

    //DTV-11301 - Added converter for trigger model
    //DTV-11301 - Added converter for trigger model
    @Bean
    public SimpleMessageConverter triggerNotificationModelConverters() {
        //adding all model beans for now
        SimpleMessageConverter converter = new SimpleMessageConverter();
        converter.setAllowedListPatterns(Arrays.asList("com.dropthought.trigger.model.*",
                "java.util.*", "java.lang.*", "java.time.*"));
        return converter;
    }
}
