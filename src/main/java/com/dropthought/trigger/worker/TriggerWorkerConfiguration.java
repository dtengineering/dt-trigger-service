package com.dropthought.trigger.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@Profile("scheduleWorker")
@Configuration
public class TriggerWorkerConfiguration implements ApplicationListener<ApplicationReadyEvent> {

    private Logger logger = LoggerFactory.getLogger(TriggerWorkerConfiguration.class);

    @Value("${trigger.queue}")
    private String queueName;

    @Value("${trigger.routing.key}")
    private String routingKey;

    @Value("${trigger.topic}")
    private String topicName;

    @Bean
    public TopicExchange receiverExchange() {
        return new TopicExchange(topicName);
    }

    @Bean
    public Queue triggerReceivingQueue() {
        if (queueName == null) {
            throw new IllegalStateException("No queue to listen to! Please specify the name of the queue to listen to with the property 'subscriber.queue'");
        }
        return new Queue(queueName);
    }

    @Bean
    public Binding binding(Queue receivingQueue, TopicExchange receiverExchange) {
        if (routingKey == null) {
            throw new IllegalStateException("No events to listen to! Please specify the routing key for the events to listen to with the property 'subscriber.routingKey' (see Trigger for available routing keys).");
        }
        return BindingBuilder
                .bind(receivingQueue)
                .to(receiverExchange)
                .with(routingKey);
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                                    MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(TriggerWorker scheduleWorker, SimpleMessageConverter triggerModelConverters){
        MessageListenerAdapter listenerAdapter = new MessageListenerAdapter(scheduleWorker, "receive");
        listenerAdapter.setMessageConverter(triggerModelConverters);
        return listenerAdapter;
    }

    @Bean
    public TriggerWorker triggerReceiver() {
        return new TriggerWorker();
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        logger.info("SUBSCRIBING TO TRIGGER MATCHING KEY '{}' FROM QUEUE '{}'!", routingKey, queueName);
    }

    //DTV-11301 - Added converter for trigger model
    @Bean
    public SimpleMessageConverter triggerModelConverters() {
        SimpleMessageConverter converter = new SimpleMessageConverter();
        converter.setAllowedListPatterns(Arrays.asList("com.dropthought.trigger.model.*",
                "java.util.*", "java.lang.*", "java.time.*"));
        return converter;
    }
}
