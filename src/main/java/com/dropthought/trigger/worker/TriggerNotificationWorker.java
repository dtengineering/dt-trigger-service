package com.dropthought.trigger.worker;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.service.TriggerDTworksService;
import com.dropthought.trigger.service.TriggerService;
import com.dropthought.trigger.service.TriggerTwoFortySevenSoftwareService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import java.util.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class TriggerNotificationWorker {


    @Autowired
    TriggerService triggerService;

    @Autowired
    TriggerDTworksService triggerDTworksService;

    @Autowired
    TriggerTwoFortySevenSoftwareService triggerTwoFortySevenSoftwareService;

    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();

    private Logger logger = LoggerFactory.getLogger(TriggerNotificationWorker.class);

    public void receive(TriggerNotification triggerNotification) throws JsonProcessingException {
        logger.info("Received message for notify trigger schedule id '{}'", triggerNotification.getScheduleId());
        String channel = triggerNotification.getChannel();

        if (channel.equals(Constants.CHANNEL_WEBHOOK)
                || channel.equalsIgnoreCase(Constants.CHANNEL_HUBSPOT)
                || channel.equalsIgnoreCase(Constants.CHANNEL_MICROSOFT_DYNAMICS)
                || channel.equalsIgnoreCase(Constants.CHANNEL_SALESFORCE)
                || channel.equalsIgnoreCase(Constants.CHANNEL_BAMBOOHR)
                || channel.equalsIgnoreCase(Constants.CHANNEL_SLACK)
                || channel.equalsIgnoreCase(Constants.CHANNEL_FRESHDESK)
                || channel.equalsIgnoreCase(Constants.CHANNEL_ZENDESK)
                || channel.equalsIgnoreCase(Constants.CHANNEL_JIRA)
               )
        {
            logger.info("Trigger notification sent to webhook for schedule id '{}'", mapper.writeValueAsString(triggerNotification));
            triggerService.sendTriggerWebhook(triggerNotification);
        } else if (channel.equals(Constants.CHANNEL_EMAIL) || channel.equals(Constants.CHANNEL_REPLY)) {
            List emailList = triggerService.validateNotifyEmailList(triggerNotification.getBusinessId(), triggerNotification.getContacts());
            if (emailList.size()  > 0) {
                triggerNotification.setContacts(emailList);
                triggerService.sendTriggerNotification(triggerNotification);
            }else {
                logger.info("Email limit reached to send notification");
            }
        } else if(channel.equalsIgnoreCase(Constants.CHANNEL_DTWORKS)){
            triggerDTworksService.sendTriggerDTWorksWebhook(triggerNotification, false);
        }
        //DTV-8038
        else if (channel.equals(Constants.CHANNEL_FOCUS_AREA)){
            triggerService.sendTriggerFocusArea(triggerNotification);
        }
        //DTV-11474 - Added for 24/7 Software
        else if(channel.equalsIgnoreCase(Constants.CHANNEL_247SOFTWARE)){
            triggerTwoFortySevenSoftwareService.sendTrigger247Software(triggerNotification);
        }
        else {
            triggerService.sendTriggerNotification(triggerNotification);
        }
    }
}
