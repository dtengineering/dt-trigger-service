package com.dropthought.trigger.worker;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.model.TriggerSchedule;
import com.dropthought.trigger.service.TriggerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TriggerWorker {

    @Autowired
    TriggerService triggerService;
    private Logger logger = LoggerFactory.getLogger(TriggerWorker.class);

    public void receive(TriggerSchedule triggerSchedule) {

        //DTV-8332 --> Schedule worker exception
        try {
            logger.info("Received message for trigger schedule id '{}'", triggerSchedule.getScheduleId());
            triggerService.insertNotificationForIdentifiedTriggers(triggerSchedule);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}



