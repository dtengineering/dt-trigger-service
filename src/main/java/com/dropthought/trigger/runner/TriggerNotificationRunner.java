package com.dropthought.trigger.runner;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.service.TriggerDTworksService;
import com.dropthought.trigger.service.TriggerNotesService;
import com.dropthought.trigger.service.TriggerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TriggerNotificationRunner {
    @Autowired
    TriggerService triggerService;

    @Autowired
    TriggerNotesService triggerNotesService;

    @Autowired
    TriggerDTworksService triggerDTworksService;

    private final RabbitTemplate notifyRabbitTemplate;
    private final TopicExchange notifyTopicExchange;

    protected final ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(TriggerNotificationRunner.class);

    @Value("${notify.trigger.routing.key}")
    private String notifyRoutingKey;

    @Autowired
    public TriggerNotificationRunner(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        this.notifyRabbitTemplate = rabbitTemplate;
        this.notifyTopicExchange = topicExchange;
    }

    @Scheduled(fixedDelayString = "${triggernotify.fixedDelay.send}", initialDelayString = "${triggernotify.intialDelay.send}")
    public void sendTriggerNotificationT() {
        this.sendNotificationsV1();
    }

    @Scheduled(fixedDelayString = "${notesnotify.fixedDelay.send}", initialDelayString = "${notesnotify.intialDelay.send}")
    public void sendNotesNotification() {
        this.sendNotesNotifications();
    }

   /* @Scheduled(fixedDelayString = "${notesnotify.fixedDelay.send}", initialDelayString = "${notesnotify.intialDelay.send}")
    public void updateDTworksAccessToken() {
       // triggerDTworksService.updateAccessTokenForUser();//TODO uncomment once user based access added
    }*/

    /**
     * method to get active trigger notification and notify via email / push
     */
    public void sendNotificationsV1() {
        // get list of active notification details to send
        List notifyTriggerList = triggerService.getActiveTriggerNotificationsV1();
        Iterator iterator = notifyTriggerList.iterator();
        // loop through each notification records
        while (iterator.hasNext()) {
            Map triggerNotifyMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(triggerNotifyMap)) {
                    TriggerNotification triggerNotification = new TriggerNotification();
                    triggerNotification.setChannel(triggerNotifyMap.get(Constants.CHANNEL).toString());
                    if(Utils.isNotNull(triggerNotifyMap.get(Constants.SUBJECT))) {
                        triggerNotification.setSubject(triggerNotifyMap.get(Constants.SUBJECT).toString());
                    }
                    if(Utils.isNotNull(triggerNotifyMap.get(Constants.MESSAGE))) {
                        triggerNotification.setMessage(triggerNotifyMap.get(Constants.MESSAGE).toString());
                    }
                    int notifyId = (int) triggerNotifyMap.get(Constants.ID);
                    triggerNotification.setNotifyId(notifyId);
                    triggerNotification.setContacts(Utils.isNotNull(triggerNotifyMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerNotifyMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    triggerNotification.setAddFeedback((int) triggerNotifyMap.get(Constants.ADD_FEEDBACK) == 0 ? false : true);
                    triggerNotification.setConditionQuery(triggerNotifyMap.containsKey(Constants.CONDITION_QUERY) ? triggerNotifyMap.get(Constants.CONDITION_QUERY).toString() : "");
                    triggerNotification.setBusinessId((int) triggerNotifyMap.get(Constants.BUSINESS_ID));
                    triggerNotification.setSurveyId((int) triggerNotifyMap.get(Constants.SURVEY_ID));
                    triggerNotification.setTriggerId((int) triggerNotifyMap.get(Constants.TRIGGER_ID));
                    triggerNotification.setHost(Utils.isNotNull(triggerNotifyMap.get(Constants.HOST)) ? triggerNotifyMap.get(Constants.HOST).toString() : "http://localhost:4200");
                    triggerNotification.setSubmissionToken(Utils.isNotNull(triggerNotifyMap.get(Constants.SUBMISSION_TOKEN)) ? triggerNotifyMap.get(Constants.SUBMISSION_TOKEN).toString() : "");
                    triggerNotification.setTimezone(Utils.isNotNull(triggerNotifyMap.get(Constants.TIMEZONE)) ? triggerNotifyMap.get(Constants.TIMEZONE).toString() : Constants.TIMEZONE_AMERICA_LOSANGELES);
                    triggerNotification.setScheduleId(triggerNotifyMap.get(Constants.SCHEDULE_ID).toString());
                    if(triggerNotifyMap.containsKey(Constants.REPLYRESPONDENT) && Utils.isNotNull(triggerNotifyMap.get(Constants.REPLYRESPONDENT))) {
                        triggerNotification.setReplyToRespondent(triggerNotifyMap.get(Constants.REPLYRESPONDENT).toString());
                    }
                    //DTV-8038, adding action type to trigger notification
                    if(triggerNotifyMap.containsKey(Constants.ACTION_TYPE) && Utils.isNotNull(triggerNotifyMap.get(Constants.ACTION_TYPE))) {
                        triggerNotification.setActionType(triggerNotifyMap.get(Constants.ACTION_TYPE).toString());
                    }
                    //DTV-8822, adding start time to trigger notification for salesforce create opportunity action
                    if(triggerNotifyMap.containsKey(Constants.START_TIME) && Utils.isNotNull(triggerNotifyMap.get(Constants.START_TIME))) {
                        triggerNotification.setStartTime(triggerNotifyMap.get(Constants.START_TIME).toString().substring(0,19));
                    }

                    triggerNotification.setTransactionId(MDC.get(Constants.TRANSACTION_ID));
                    notifyRabbitTemplate.convertAndSend(notifyTopicExchange.getName(), notifyRoutingKey, triggerNotification);

                    logger.info("sent message to {} routingkey {}", notifyTopicExchange.getName(), notifyRoutingKey);
                    // update trigger notification status to processing
                    triggerService.updateNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_PROCESSING);
                    // DTV-6955 update trigger notification in trigger_notification_{buuid} by notificationUUID.
                    triggerService.updateBusinessNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_PROCESSING, (int) triggerNotifyMap.get(Constants.BUSINESS_ID));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to send notes notifications from notes_notification table (currently only sending push notifications)
     */
    public void sendNotesNotifications() {
        List activeNotesNotificationsList = triggerNotesService.getActiveNotesNotifications();
        Iterator iterator = activeNotesNotificationsList.iterator();
        while (iterator.hasNext()) {
            Map eachNoteNotificationMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(eachNoteNotificationMap)) {
                    int notifyId = Integer.parseInt(eachNoteNotificationMap.getOrDefault("id","0").toString());
                    logger.info("sending notes notification for notes notifyId {}", notifyId);

                    triggerNotesService.updateNotesNotificationSentStatusByNotifyId(notifyId, "processing");
                    String status = triggerNotesService.processNotesNotification(eachNoteNotificationMap);

                    logger.info("sent notes notification for notes notifyId {} with status {}", notifyId, status);
                    triggerNotesService.performNotesNotificationPostProcessing(notifyId, status);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
