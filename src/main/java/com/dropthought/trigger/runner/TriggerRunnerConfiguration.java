package com.dropthought.trigger.runner;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

@Profile("runner")
@Configuration
@EnableScheduling
public class TriggerRunnerConfiguration {

    @Value("${trigger.topic}")
    private String topicName;

    @Bean
    public TopicExchange senderTopicExchange() {
        return new TopicExchange(topicName);
    }

    @Bean
    public TriggerRunner triggerPublisher(RabbitTemplate rabbitTemplate, TopicExchange senderTopicExchange) {
        return new TriggerRunner(rabbitTemplate, senderTopicExchange);
    }
}
