package com.dropthought.trigger.runner;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

@Profile("notificationRunner")
@Configuration
@EnableScheduling
public class TriggerNotificationRunnerConfiguration {


    @Value("${notify.trigger.topic}")
    private String topicName;

    @Bean
    public TopicExchange triggerTopicExchange() {
        return new TopicExchange(topicName);
    }

    @Bean
    public TriggerNotificationRunner triggerNotifyPublisher(RabbitTemplate rabbitTemplate, TopicExchange triggerTopicExchange) {
        return new TriggerNotificationRunner(rabbitTemplate, triggerTopicExchange);
    }
}
