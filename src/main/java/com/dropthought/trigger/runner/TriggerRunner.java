package com.dropthought.trigger.runner;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.model.TriggerSchedule;
import com.dropthought.trigger.service.TriggerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.*;

public class TriggerRunner {

    @Autowired
    TriggerService triggerService;

    private final RabbitTemplate rabbitTemplate;
    private final TopicExchange topicExchange;

    protected final ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(TriggerRunner.class);

    @Value("${trigger.routing.key}")
    private String routingKey;

    @Autowired
    public TriggerRunner(RabbitTemplate rabbitTemplate, TopicExchange topicExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.topicExchange = topicExchange;
    }


    @Scheduled(fixedDelayString = "${trigger.fixedDelay.send}", initialDelayString = "${trigger.intialDelay.send}")
    public void triggerNotification() {
        this.identifyTriggersToPublish();
    }

    /**
     * method to get active scheduled triggers from trigger_schedule table
     *
     * @return
     */
    public void identifyTriggersToPublish() {
        List scheduledList = triggerService.getActiveTriggerScheduled();

        if (Utils.isNotNull(scheduledList)) {
            Iterator iterator = scheduledList.iterator();
            while (iterator.hasNext()) {
                try {
                    Map schedule = (Map) iterator.next();
                    logger.info("schedule id {} ",schedule.get(Constants.SCHEDULE_ID).toString());

                    TriggerSchedule triggerSchedule = new TriggerSchedule();
                    triggerSchedule.setSurveyId(schedule.containsKey(Constants.SURVEY_ID) ? (schedule.get(Constants.SURVEY_ID).toString()) : Constants.EMPTY);
                    triggerSchedule.setBusinessId(schedule.containsKey(Constants.BUSINESS_ID) ? (schedule.get(Constants.BUSINESS_ID).toString()) : Constants.EMPTY);
                    triggerSchedule.setTriggerId(schedule.containsKey(Constants.TRIGGER_ID) ? (schedule.get(Constants.TRIGGER_ID).toString()) : Constants.EMPTY);
                    triggerSchedule.setScheduleId(schedule.containsKey(Constants.SCHEDULE_ID) ? (schedule.get(Constants.SCHEDULE_ID).toString()) : Constants.EMPTY);
                    triggerSchedule.setLastProcessedDate(schedule.get(Constants.LAST_PROCCESSED_DATE).toString());
                    triggerSchedule.setLastProcessedTime(schedule.get(Constants.LAST_PROCCESSED_TIME).toString());
                    triggerSchedule.setTimezone(schedule.containsKey(Constants.TIMEZONE) ? schedule.get(Constants.TIMEZONE).toString() : Constants.EMPTY);
                    triggerSchedule.setFrequency(schedule.get(Constants.FREQUENCY).toString());
                    triggerSchedule.setEndDate(schedule.containsKey(Constants.END_DATE) ? schedule.get(Constants.END_DATE).toString() : Constants.EMPTY);
                    triggerSchedule.setEndTime(schedule.containsKey(Constants.END_TIME) ? schedule.get(Constants.END_TIME).toString() : Constants.EMPTY);
                    triggerSchedule.setChannel(schedule.get(Constants.CHANNEL).toString());
                    if(Utils.isNotNull(schedule.get(Constants.SUBJECT))) {
                        triggerSchedule.setSubject((schedule.get(Constants.SUBJECT).toString()));
                    }
                    if(Utils.isNotNull(schedule.get(Constants.MESSAGE))) {
                        triggerSchedule.setMessage(schedule.get(Constants.MESSAGE).toString());
                    }
                    triggerSchedule.setContacts(Utils.isNotNull(schedule.get(Constants.CONTACTS)) ? mapper.readValue(schedule.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    triggerSchedule.setAddFeedback((int) schedule.get(Constants.ADD_FEEDBACK) == 0 ? false : true);
                    triggerSchedule.setStartDate(schedule.get(Constants.START_DATE).toString());
                    triggerSchedule.setStartTime(schedule.get(Constants.START_TIME).toString());
                    triggerSchedule.setTransactionId(MDC.get(Constants.TRANSACTION_ID));
                    if(Utils.isNotNull(schedule.get(Constants.TRIGGER_CONFIG))) {
                        triggerSchedule.setTriggerConfig((schedule.get(Constants.TRIGGER_CONFIG)));
                    }
                    triggerSchedule.setCreatedTime(schedule.containsKey(Constants.CREATED_TIME) ? schedule.get(Constants.CREATED_TIME).toString() : Constants.EMPTY);
                    if(Utils.isNotNull(schedule.get(Constants.REPLYRESPONDENT))) {
                        triggerSchedule.setReplyToRespondent(schedule.get(Constants.REPLYRESPONDENT).toString());
                    }
                    //DTV-8038
                    if(schedule.containsKey(Constants.INTEGRATIONS_CONFIG) && Utils.isNotNull(schedule.get(Constants.INTEGRATIONS_CONFIG))) {
                        triggerSchedule.setIntegrationsConfig(mapper.readValue(schedule.get(Constants.INTEGRATIONS_CONFIG).toString(), HashMap.class));
                    }
                    // send to queue
                    rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, triggerSchedule);
                    // update sent status to tobeprocess
                    triggerService.updateScheduleSentStatusByScheduleId(Integer.parseInt(triggerSchedule.getScheduleId()), Constants.STATUS_TO_BE_PROCESS);
                    String scheduleEndDateTime = triggerSchedule.getEndDate() +" "+triggerSchedule.getEndTime();
                    //update the status to completed and last processing time, when based on singe/multiple frequency condition
                    triggerService.updateTriggerStatusByFrequency(triggerSchedule.getFrequency(), (int)(schedule.get(Constants.SCHEDULE_ID)), scheduleEndDateTime);

                } catch (Exception e) {
                    e.printStackTrace();
                    logger.error(e.getMessage());
                }
            }
        }
    }

    @Scheduled(fixedDelayString = "${triggernotify.fixedDelay.send}", initialDelayString = "${triggernotify.intialDelay.send}")
    public void updateTriggerState() {
        triggerService.updateExpiredSurveyV2();
    }

/*
    @Scheduled(fixedDelayString = "${triggernotify.fixedDelay.send}", initialDelayString = "${triggernotify.intialDelay.send}")
    public void sendTriggerNotificationT() {
        this.sendNotificationsV1();
    }

    *//**
     * method to get active trigger notification and notify via email / push
     *//*
    public void sendNotificationsV1() {
        // get list of active notification details to send
        List notifyTriggerList = triggerService.getActiveTriggerNotificationsV1();
        Iterator iterator = notifyTriggerList.iterator();
        // loop through each notification records
        while (iterator.hasNext()) {
            Map triggerNotifyMap = (Map) iterator.next();
            try {
                if (Utils.isNotNull(triggerNotifyMap)) {
                    TriggerNotification triggerNotification = new TriggerNotification();
                    triggerNotification.setChannel(triggerNotifyMap.get(Constants.CHANNEL).toString());
                    triggerNotification.setSubject(triggerNotifyMap.get(Constants.SUBJECT).toString());
                    triggerNotification.setMessage(triggerNotifyMap.get(Constants.MESSAGE).toString());
                    int notifyId = (int) triggerNotifyMap.get(Constants.ID);
                    triggerNotification.setNotifyId(notifyId);
                    triggerNotification.setContacts(Utils.isNotNull(triggerNotifyMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerNotifyMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                    triggerNotification.setAddFeedback((int) triggerNotifyMap.get(Constants.ADD_FEEDBACK) == 0 ? false : true);
                    triggerNotification.setConditionQuery(triggerNotifyMap.get(Constants.CONDITION_QUERY).toString());
                    triggerNotification.setBusinessId((int) triggerNotifyMap.get(Constants.BUSINESS_ID));
                    triggerNotification.setSurveyId((int) triggerNotifyMap.get(Constants.SURVEY_ID));
                    triggerNotification.setTriggerId((int) triggerNotifyMap.get(Constants.TRIGGER_ID));
                    triggerNotification.setHost(Utils.isNotNull(triggerNotifyMap.get(Constants.HOST)) ? triggerNotifyMap.get(Constants.HOST).toString() : "http://localhost:4200");
                    triggerNotification.setSubmissionToken(Utils.isNotNull(triggerNotifyMap.get(Constants.SUBMISSION_TOKEN)) ? triggerNotifyMap.get(Constants.SUBMISSION_TOKEN).toString() : "");
                    rabbitTemplate.convertAndSend(topicExchange.getName(), routingKey, triggerNotification);
                    // update trigger notification status to processing
                    triggerService.updateNotificationSentStatusByNotifyId(notifyId, Constants.STATUS_PROCESSING);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/

    /**
     * chron to run every day at 12 AM
     */
    //@Scheduled(cron = "* * * * * *")
    /*@Scheduled(cron = "0 0 0 * * *")
    public void updateExecutionTimes() {
        triggerService.updateExecutionStartAndEndTime();
    }*/

    /**
     * chron job to run 30 mins
     * 30 minutes = 1800 seconds = 1,800,000 milliseconds.
     */
    @Scheduled(fixedDelayString = "1800000", initialDelayString = "1000")
    public void updateExecutionTimes() {
        triggerService.updateExecutionStartAndEndTimeV2();
    }


    /**
      * sometimes(during deployment)sent_status is set processing and actions are not performing its values.
      * In order to solve that, this method to update the processing status to null is used.
      * This will be one time job, every time when the application gets started.
     * 30 minutes = 1800 seconds = 1,800,000 milliseconds.
      */

    @Scheduled(initialDelayString = "1000", fixedDelayString = "900000") // 9,00,000 milliseconds = 15 minutes"
    public void updateTriggerStatus(){
        triggerService.updateTriggerStatusToNull();
    }

}
