package com.dropthought.trigger.aop;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.controller.TriggerController;
import com.dropthought.trigger.model.TriggerNotification;
import com.dropthought.trigger.model.TriggerSchedule;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Aspect
@Configuration
public class TransactionAspect {
    private static final Logger logger = LoggerFactory.getLogger(TriggerController.class);

    /**
     * worker transaction
     * @param joinPoint
     */
    @Before("execution(* com.dropthought.trigger.worker.*.receive(..))")
    public void beforeWorker(JoinPoint joinPoint) {
        String correlationId = "";
        if(joinPoint.getArgs()[0] instanceof  TriggerSchedule){
            correlationId = ((TriggerSchedule)joinPoint.getArgs()[0]).getTransactionId();
        }else if(joinPoint.getArgs()[0] instanceof  TriggerNotification){
            correlationId = ((TriggerNotification)joinPoint.getArgs()[0]).getTransactionId();
        }
        if(Utils.emptyString(correlationId)){
            correlationId = generateUniqueCorrelationId();
        }
        MDC.put(Constants.WORKER_TRANSACTION_ID, correlationId);
//        logger.info("TransactionAspect  Before call :: {} ", joinPoint.getArgs()[0]);
    }

    /**
     * runner transaction
     * @param joinPoint
     */
    @Before("execution(* com.dropthought.trigger.runner.*.*(..))")
    public void beforeRunner(JoinPoint joinPoint) {
        MDC.put(Constants.RUNNER_TRANSACTION_ID, generateUniqueCorrelationId());
//        logger.info("TransactionAspect runner Before call :: {} ", joinPoint);
    }

    @After(value = "execution(* com.dropthought.trigger.worker.*.receive(..))")
    public void afterScheduleReturning(JoinPoint joinPoint) {
        logger.info("TransactionAspect worker after call :: {} ", joinPoint);
        MDC.remove(Constants.WORKER_TRANSACTION_ID);
    }

    @After("execution(* com.dropthought.trigger.runner.*.*(..))")
    public void afterNotificationReturning(JoinPoint joinPoint) {
//        logger.info("TransactionAspect runner after call :: {} ", joinPoint);
        MDC.remove(Constants.RUNNER_TRANSACTION_ID);
    }

    /**
     * to log exception msg in the application
     * @param ex
     */
    @AfterThrowing(value = "execution(* com.dropthought.trigger.*.*.*(..))", throwing = "ex")
    public void logError(Exception ex) {
        logger.error("TransactionAspect error log :: {} , url {} ", ex.getMessage());
    }

    private String generateUniqueCorrelationId() {
        return UUID.randomUUID().toString();
    }
}
