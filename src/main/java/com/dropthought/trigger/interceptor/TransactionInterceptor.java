package com.dropthought.trigger.interceptor;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.controller.TriggerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class TransactionInterceptor implements HandlerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(TriggerController.class);

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        String transactionId = request.getHeader(Constants.TRANSACTION_ID);
        if(Utils.emptyString(transactionId)){
            transactionId = generateUniqueId();
        }
        MDC.put(Constants.TRANSACTION_ID, transactionId);
        logger.info("interceptor Before call :: {} ", request.getRequestURI());
        return true;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex) throws Exception {
        logger.info("interceptor After completion :: {} ", request.getRequestURI());
        MDC.remove(Constants.TRANSACTION_ID);
    }

    private String generateUniqueId() {
        return UUID.randomUUID().toString();
    }


}



