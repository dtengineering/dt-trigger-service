package com.dropthought.trigger.controller;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.constants.DTConstants;
import com.dropthought.trigger.model.*;
import com.dropthought.trigger.service.TriggerDTworksService;
import com.dropthought.trigger.service.TriggerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.*;

@RestController
@RequestMapping("/api/v1/trigger")
@Validated
public class TriggerController {

    private static final Logger logger = LoggerFactory.getLogger(TriggerController.class);

    @Autowired
    TriggerService triggerService;

    @Autowired
    TriggerDTworksService triggerDTworksService;

    @Value("${dtp.domain}")
    private String dtpDomain;

    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("version")
    public Map<String, String> version() {
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    /**
     * create a new trigger notification for given request.
     * @param bean
     * @return
     */
    @PostMapping()
    public Map createTrigger(@Valid @RequestBody Trigger bean) {
        Map result = new HashMap();
        logger.debug("begin creating trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the Trigger bean via validator object defined in the Trigger bean
        Set<ConstraintViolation<Trigger>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<Trigger> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(isValid) {
            result = triggerService.createTrigger(bean);
        }

        logger.debug("end creating trigger notification");
        return result;
    }

    /**
     * update a trigger notification by using its primary id.
     * @param bean
     * @return
     */
    @PutMapping("{triggerId}")
    public Map updateTrigger(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                             @Valid @RequestBody Trigger bean) {
        Map result = new HashMap();
        logger.debug("begin update trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the Trigger bean via validator object defined in the Trigger bean
        Set<ConstraintViolation<Trigger>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<Trigger> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(Utils.notEmptyString(triggerId)  && isValid){
            // update trigger
            int rec = triggerService.updateTrigger(bean, triggerId);
            if(rec > 0) {
                // set the success response
                result.put(Constants.SUCCESS, true);

                // assign the updated result
                result.put(Constants.RESULT, triggerService.getTriggerById(triggerId, bean.getBusinessUniqueId(), bean.getTimezone(), bean.getUserId()));
            }
            else{
                result.put(Constants.SUCCESS, false);
            }

        }else{
            // assigning failure status & error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }

        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * delete a trigger notification by using its primary id.
     * @return
     */
    @DeleteMapping("{triggerId}")
    public Map deleteTrigger(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                             @RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId) {
        Map result = new HashMap();
        logger.debug("begin delete trigger notification");
        if(Utils.notEmptyString(triggerId )){
            // delete trigger
            int rec = triggerService.deleteTrigger(triggerId, businessId);
            // assign success response
            result.put(Constants.SUCCESS, rec > 0 ? true : false);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId is mandatory");
        }
        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * get a trigger notification by using its primary id.
     * @return
     */
    @GetMapping("{triggerId}")
    public Map getTrigger(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                          @RequestParam(defaultValue = "") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId,
                          @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone,
                          @RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId") String userId ) {
        Map result = new HashMap();
        logger.debug("begin get trigger notification");
        if(Utils.notEmptyString(triggerId) && Utils.notEmptyString(businessId)){
            // get trigger details
            Trigger trigger = triggerService.getTriggerById(triggerId, businessId, timezone, userId);
            if(trigger != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, trigger);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }
        logger.debug("end get trigger notification");
        return result;
    }

    /**
     * get all triggers notification by using its businessId.
     *
     * @return
     */
    @GetMapping
    public Map getAllTriggers(@RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId,
                              @RequestParam(defaultValue = "created_time") String sortBy,
                              @RequestParam(defaultValue = "") String viewBy,
                              @RequestParam(defaultValue = "all") String state,
                              @RequestParam(defaultValue = "") String search,
                              @RequestParam(defaultValue = "false") Boolean isMatch,
                              @RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid userId") String userId,
                              @RequestParam(defaultValue = "-1") Integer pageNo,
                              @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone) {
        Map result = new HashMap();
        logger.info("begin get all triggers notification");
        // businessUniqueId and userId are mandatory, check its values.
        if(Utils.notEmptyString(businessId) && Utils.notEmptyString(userId)){
            // get all trigger details by using sort, state and viewing filter
            result = triggerService.getAllTriggersByBusiness(businessId, sortBy, viewBy, state, search, isMatch, userId, pageNo, timezone);
            // assign success response
           /* result.put(Constants.SUCCESS, true);
            result.put(Constants.RESULT, resultList);*/
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "businessUniqueId and userId are mandatory");
        }
        logger.info("end get all triggers notification");
        return result;
    }

    /**
     * update trigger status by using triggerId
     * @return
     */
    @PutMapping("{triggerId}/status")
    public Map updateTriggerStatus(@RequestParam(defaultValue = "") @NotBlank(message = "state is required") String state,
                                   @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") @RequestParam(defaultValue = "") String businessId,
                                    @PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId) {
        Map result = new HashMap();
        logger.debug("begin update trigger status");
        // TriggerId, businessUniqueId and userId are mandatory, check its value
        if(Utils.notEmptyString(triggerId) && Utils.notEmptyString(state) && Utils.notEmptyString(businessId)){
            // get all trigger details by using sort, state and viewing filter
            result = triggerService.updateTriggerStatus(businessId, triggerId, state);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId, businessUniqueId and state are mandatory");
        }
        logger.debug("end update trigger status");
        return result;
    }

    /**************** schedule **********/
    /**
     * create a new trigger schedule for given request.
     * @param bean
     * @return
     */
    @PostMapping("schedule")
    public Map createTriggerSchedule(@RequestBody @Valid TriggerSchedule bean, HttpServletRequest request) {
        Map result = new HashMap();
        logger.debug("begin creating trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the bean via validator object defined in the bean
        Set<ConstraintViolation<TriggerSchedule>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerSchedule> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(isValid) {
            String host = request.getHeader("host");
            host = host.contains("localhost") ? "http://localhost:4200" : "https" + "://" + dtpDomain;
            bean.setHost(host);
            //calculate email limit and throw error
            boolean emailLimitReached = false;
            boolean listsLimitReached = false;
            Map emailUsageMap = new HashMap();
            Map listsUsageMap = new HashMap();

            if (bean.getChannel().equals("email")) {
                emailUsageMap = triggerService.checkEmailUsageLimit(bean);
                emailLimitReached = emailUsageMap.containsKey("limitReached") && Utils.isNotNull(emailUsageMap.get("limitReached")) ? (Boolean) emailUsageMap.get("limitReached") : false;
            }

            if(bean.getChannel().equals("list")){
                listsUsageMap = triggerService.checkListsUsageLimit(bean);
                listsLimitReached = listsUsageMap.containsKey("limitReached") && Utils.isNotNull(listsUsageMap.get("limitReached")) ? (Boolean) listsUsageMap.get("limitReached") : false;
            }

            if (!emailLimitReached && !listsLimitReached) {
                result = triggerService.createTriggerSchedule(bean);
            }
            else {
                if(bean.getChannel().equals("email")) {
                    result = emailUsageMap;
                }
                if(bean.getChannel().equals("list")) {
                    result = listsUsageMap;
                }
            }
        }

        logger.debug("end creating trigger notification");
        return result;
    }

    /**
     * update a trigger schedule by using its primary id.
     * @param bean
     * @return
     */
    @PutMapping("schedule/{scheduleId}")
    public Map updateTriggerSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId,
                                     @Valid @RequestBody TriggerSchedule bean) {
        Map result = new HashMap();
        logger.debug("begin update trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
//        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//        // instantiating a validator object
//        Validator validator = factory.getValidator();
//        // validating the schedule bean via validator object defined in the schedule bean
//        Set<ConstraintViolation<TriggerSchedule>> violations = validator.validate(bean);
//        // iterating all the voilations and send the error message as api response
//        for (ConstraintViolation<TriggerSchedule> violation : violations) {
//            // if validations fails assigning false to isValid variable.
//            isValid = false;
//            // assigning failure status & validation error message to result
//            result.put(Constants.SUCCESS, isValid);
//            result.put(Constants.ERROR, violation.getMessage());
//        }
        if(Utils.notEmptyString(scheduleId)  && isValid){
            if( !bean.getChannel().equalsIgnoreCase(Constants.LIST) || (bean.getChannel().equalsIgnoreCase(Constants.LIST) && ( Utils.isNotNull(bean.getParticipantGroupId())
                    && bean.getParticipantGroupId() > 0 && Utils.isNotNull(bean.getHeaderData())))) {
                //calculate email limit and throw error
                boolean emailLimitReached = false;
                Map emailUsageMap = new HashMap();

                if (bean.getChannel().equals("email")) {
                    emailUsageMap = triggerService.checkEmailUsageLimit(bean);
                    emailLimitReached = emailUsageMap.containsKey("limitReached") && Utils.isNotNull(emailUsageMap.get("limitReached")) ? (Boolean) emailUsageMap.get("limitReached") : false;
                }

                // update trigger
                if (!emailLimitReached) {
                    int rec = triggerService.updateTriggerSchedule(bean, scheduleId);
                    if (rec > 0) {
                        // set the success response
                        result.put(Constants.SUCCESS, true);
                        // assign the updated result
                        result.put(Constants.RESULT, triggerService.getTriggerScheduleById(scheduleId, bean.getTimezone()));
                    } else {
                        result.put(Constants.SUCCESS, false);
                        result.put(Constants.ERROR, "Record not found");
                    }
                } else {
                    result = emailUsageMap;
                }
            }else{
                result.put(Constants.SUCCESS, false);
                result.put(Constants.ERROR, "participant group id and header info are mandatory for trigger List action.");
            }

        }else{
            // assigning failure status & error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }

        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * delete a trigger schedule by using its primary id.
     * @return
     */
    @DeleteMapping("schedule/{scheduleId}")
    public Map deleteTriggerSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId) {
        Map result = new HashMap();
        logger.debug("begin delete trigger schedule");
        if(Utils.notEmptyString(scheduleId )){
            // delete trigger
            int rec = triggerService.deleteTriggerSchedule(scheduleId);
            // assign success response
            result.put(Constants.SUCCESS, rec > 0 ? true : false);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId is mandatory");
        }
        logger.debug("end update trigger schedule");
        return result;
    }

    /**
     * get a trigger Schedule by using its primary id.
     * @return
     */
    @GetMapping("schedule/{scheduleId}")
    public Map getTriggerSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId,
                                  @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone) {
        Map result = new HashMap();
        logger.debug("begin get trigger notification");
        if(Utils.notEmptyString(scheduleId)){
            // get trigger details
            TriggerSchedule schedule = triggerService.getTriggerScheduleById(scheduleId, timezone);
            if(schedule != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, schedule);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "ScheduleId & businessUniqueId are mandatory");
        }
        logger.debug("end get trigger notification");
        return result;
    }

    /**
     * get a trigger Schedule by using trigger unique id.
     * @return
     */
    @GetMapping("{triggerId}/schedule")
    public Map getTriggerScheduleByTriggerId(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                                             @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone,
                                             @RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId) {
        Map result = new HashMap();
        logger.debug("begin get trigger schedule by triggerID");
        if(Utils.notEmptyString(triggerId)){
            // get trigger details
            List<TriggerSchedule> schedule = triggerService.getTriggerScheduleByTriggerId(triggerId, timezone, businessId);
            if(schedule != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, schedule);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }
        logger.debug("end get trigger schedule by triggerID");
        return result;
    }
    /**************** trigger notification **********/
    /**
     * create a new trigger notification for given request.
     * @param bean
     * @return
     */
    @PostMapping("notify")
    public Map createTriggerNotification(@Valid @RequestBody TriggerNotification bean) {
        Map result = new HashMap();
        logger.debug("begin creating trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the bean via validator object defined in the bean
        Set<ConstraintViolation<TriggerNotification>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerNotification> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(isValid) {
            result = triggerService.createTriggerNotification(bean);
        }

        logger.debug("end creating trigger notification");
        return result;
    }

    /**
     * update a trigger notification by using its unique id.
     * @param bean
     * @return
     */
    @PutMapping("notify/{notifyId}")
    public Map updateTriggerNotification(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid notifyId") String notifyId,
                                         @Valid @RequestBody TriggerNotification bean) {
        Map result = new HashMap();
        logger.debug("begin update trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the schedule bean via validator object defined in the schedule bean
        Set<ConstraintViolation<TriggerNotification>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerNotification> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(Utils.notEmptyString(notifyId)  && isValid){
            // update trigger
            int rec = triggerService.updateTriggerNotification(bean, notifyId);
            if(rec > 0) {
                // set the success response
                result.put(Constants.SUCCESS, true);
                // assign the updated result
                result.put(Constants.RESULT, triggerService.getTriggerNotificationById(notifyId));
            }
            else{
                result.put(Constants.SUCCESS, false);
                result.put(Constants.ERROR, "Record not found");
            }

        }else{
            // assigning failure status & error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }

        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * delete a trigger notification by using its unique id.
     * @return
     */
    @DeleteMapping("notify/{notifyId}")
    public Map deleteTriggerNotification(@PathVariable  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid notifyId") String notifyId) {
        Map result = new HashMap();
        logger.debug("begin delete trigger notification");
        if(Utils.notEmptyString(notifyId )){
            // delete trigger
            int rec = triggerService.deleteTriggerNotification(notifyId);
            // assign success response
            result.put(Constants.SUCCESS, rec > 0 ? true : false);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "NotifyId is mandatory");
        }
        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * get a trigger Schedule by using its primary id.
     * @return
     */
    @GetMapping("notify/{notifyId}")
    public Map getTriggerNotification(@PathVariable  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid notifyId") String notifyId) {
        Map result = new HashMap();
        logger.debug("begin get trigger notification");
        if(Utils.notEmptyString(notifyId)){
            // get trigger details
            TriggerNotification schedule = triggerService.getTriggerNotificationById(notifyId);
            if(schedule != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, schedule);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "NotifyId & businessUniqueId are mandatory");
        }
        logger.debug("end get trigger notification");
        return result;
    }


    /**
     * get users based on triggerId.
     *
     * @return
     */
    @GetMapping("users")
    public Map getTriggerById(@RequestParam  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                              @RequestParam  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId) {
        Map result = new HashMap();
        logger.info("begin get all users based on triggerId");
        // businessUniqueId and userId are mandatory, check its values.
        if(Utils.notEmptyString(triggerId) && Utils.notEmptyString(businessId)){
            // get all user details by trigger id
            List users = triggerService.getUsersByTriggerId(triggerId, businessId);
            // assign success response
            result.put(Constants.SUCCESS, true);
            result.put(Constants.RESULT, users);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "businessUniqueId and triggerUUID are mandatory");
        }
        logger.info("end get all users based on triggerId");
        return result;
    }

    @PostMapping("updateExecutionTime")
    public void updateExecutionTime() {
        logger.info("end get all users based on triggerId");
        triggerService.updateExecutionStartAndEndTime();
        logger.info("end get all users based on triggerId");
    }


    /**
     * validate the contact of all triggers for the given userId by comparing survey filter with trigger condition.
     *
     * @return
     */
    @PutMapping("user/{userId}/surveys")
    public Map validateContactsByUserId(@PathVariable @Pattern(regexp = DTConstants.ONLY_NUMBERS, message = "Invalid userId") int userId,
                                        @Valid @RequestBody ValidateContactsBean bean) {
        Map result = new HashMap();
        logger.info("begin validate contacts for the given userId & surveyId");
        // businessUniqueId and userId are mandatory, check its values.
        if(userId > 0 && bean.getSurveyIds().size() > 0 && Utils.notEmptyString(bean.getBusinessUUID())){
            // validate the contacts in trigger schedule & trigger notifications with given userId
            int res = triggerService.validateContactsByUserId(userId, bean.getSurveyIds(), bean.getBusinessUUID());
            // assign success response
            result.put(Constants.SUCCESS, true);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "userId, businessId and surveyId are mandatory");
        }
        logger.info("End validate contacts for the given userId & surveyId");
        return result;
    }

    /**
     * get a trigger notification by using its primary id.
     * @return
     */
    @GetMapping("eliza")
    public Map getTrigger(@RequestBody Map elizaRequest) {
        Map result = new HashMap();
        logger.debug("begin get trigger notification");
        String readableQeury = elizaRequest.get("readableQuery").toString();
        String surveyId = elizaRequest.get("surveyId").toString();
        String businessId = elizaRequest.get("businessId").toString();
        String timezone = elizaRequest.get("timezone").toString();
        String triggerCondition = Utils.isNotNull(elizaRequest.get("triggerCondition")) ? elizaRequest.get("triggerCondition").toString() : "";
        if(Utils.notEmptyString(readableQeury) && Utils.notEmptyString(businessId)){
            // get trigger details
            int bId = triggerService.getBusinessIdbyUUID(businessId);
            List analyticsToken = triggerService.getCategorySentimentTokenList(readableQeury, surveyId, businessId, timezone,
                    new ArrayList(), triggerCondition, bId);
            if(analyticsToken != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, analyticsToken);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId & businessUniqueId are mandatory");
        }
        logger.debug("end get trigger notification");
        return result;
    }

    /**
      * api to update sent_status of trigger schedule from processing to null for
      */
    @GetMapping("updateSentStatus")
    public void updateSentStatus() {
        logger.info("Start of updateSentStatus ");
        triggerService.updateTriggerStatusToNull();
        logger.info("end of updateSentStatus ");
    }

    /**
     * create the pre-built triggers of template in trigger_buuid table when program is created from templates in dtapp.
     *
     * @return
     */
    @PostMapping("template/{templateId}/program/{programId}")
    public Map createTriggersByTemplateProgramId(@PathVariable  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid templateId") String templateId,
                                                 @PathVariable  @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid programId") String programId,
                                                 @Valid @RequestBody TemplateTriggerBean bean) {
        Map result = new HashMap();
        logger.info("begin creating triggers from template triggers by templateId & programId");
        // businessUniqueId and userId are mandatory, check its values.
        if (Utils.notEmptyString(bean.getBusinessUUID()) && Utils.isNotNull(bean.getTemplateTrigger()) && Utils.isNotNull(bean.getUserUUID())) {
            // validate the contacts in trigger schedule & trigger notifications with given userId
            int res = triggerService.createTriggersByTemplateProgramId(templateId, programId, bean);
            // assign success response
            result.put(Constants.SUCCESS, true);
        } else {
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "userId, businessId and template trigger are mandatory");
        }
        logger.info("End creating triggers from template triggers by templateId & programId");
        return  result;
    }
    /**
     * get a trigger names list by using templateId.
     * @return
     */
    @GetMapping("program/{programId}/triggers")
    public Map getTriggerTemplatesListById(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid programId") String programId,
                                           @RequestParam(defaultValue = "") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId,
                                           @RequestParam(defaultValue = "") String timezone) {
        Map result = new HashMap();
        logger.debug("begin get trigger template name list");
        if(Utils.notEmptyString(programId) && Utils.notEmptyString(businessId)){
            // get trigger details
            List trigger = triggerService.getTriggerListByProgramId(programId, businessId, timezone);
            if(trigger != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, trigger);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "ProgramUnqiueID & BusinessUniqueId are mandatory");
        }
        logger.debug("end get trigger template name list");
        return result;
    }

    /**
     * validate the trigger condition of all triggers created from template when program is updated.
     *
     * @return
     */
    @PutMapping("/program/{programId}")
    public Map updateTriggerConditionByProgramid(@PathVariable @Pattern(regexp = DTConstants.ONLY_NUMBERS, message = "Invalid programId")  int programId,
                                                 @Valid @RequestBody validateTriggerConditionBean bean) {
        Map result = new HashMap();
        logger.info("begin update trigger condition for the given programId & updatedOrDeletedQuestions");
        // businessUniqueId and updatedOrdeleteQuestions are mandatory, check its values.
        if(bean.getQuestionIds().size() > 0 && Utils.notEmptyString(bean.getBusinessUUID())){
            // validate the contacts in trigger schedule & trigger notifications with given userId
            // assign success response
            Map res = triggerService.updateNewTriggerConditionForTriggers(programId, bean);
            if(res.containsKey("success") && (boolean) res.get("success")){
                result.put("updatedTriggerIds", res.get("updatedTriggerIds"));
            }
            result.put(Constants.SUCCESS, true);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "userId, businessId and surveyId are mandatory");
        }
        logger.info("End update trigger condition for the given programId & updatedOrDeletedQuestions");
        return result;
    }

    /**
     * validate the trigger condition of all triggers created from template when program is updated.
     *
     * @return
     */
    @PutMapping("/dtworks/resend")
    public Map resendFailedDtworksTriggers(@RequestParam(required = false)  int scheduleId,
                                           @RequestParam String businessUniqueId,
                                           @RequestParam(required = false) int programId) {
        Map result = new HashMap();
        logger.info("begin resendFailedDtworksTriggers");
        // businessUniqueId is mandatory, check its values.
        if(Utils.notEmptyString(businessUniqueId) && (programId > 0 || scheduleId > 0)){
            logger.info("resendFailedDtworksTriggers {} {}", scheduleId, programId);
            triggerDTworksService.resendFailedDtworksTriggers(businessUniqueId, programId, scheduleId);
            result.put(Constants.SUCCESS, true);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "businessUniqueId and programId / scheduleId are mandatory");
        }
        logger.info("End resendFailedDtworksTriggers");
        return result;
    }


    @PostMapping("participant")
    public Map createTriggersFromParticipant(@RequestBody @Valid String requestBody) {
        Map result = new HashMap();
        try{
            logger.info("begin creating trigger from participant");
            result = triggerService.createTriggersFromParticipant(requestBody);
            result.put(Constants.SUCCESS, true);
            logger.info("end creating trigger from participant");
        }catch (Exception e){
            e.printStackTrace();
            result.put(Constants.SUCCESS, false);
        }
        return result;
    }



}
