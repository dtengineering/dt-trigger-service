package com.dropthought.trigger.controller;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.constants.DTConstants;
import com.dropthought.trigger.model.TriggerTemplate;
import com.dropthought.trigger.model.TriggerTemplateSchedule;
import com.dropthought.trigger.service.TriggerTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.*;
import javax.validation.constraints.Pattern;
import java.util.*;

@RestController
@RequestMapping("/api/v1/trigger")
@Validated
public class TriggerTemplateController {

    private static final Logger logger = LoggerFactory.getLogger(TriggerController.class);
    private static final String TEMPLATE_TRIGGER="template";
    private static final String TEMPLATE_TRIGGER_SCHEDULE=TEMPLATE_TRIGGER+"/schedule";

    @Autowired
    TriggerTemplateService triggerTemplateService;

    @Value("${dtp.domain}")
    private String dtpDomain;

    /**
     * returns the version of the service
     *
     * @return Map(version, major.minor.patch)
     */
    @GetMapping("template/version")
    public Map<String, String> version() {
        return Collections.singletonMap("VERSION", "1.0.0");
    }

    /**
     * create a new trigger notification for given request.
     * @param bean
     * @return
     */
    @PostMapping(TEMPLATE_TRIGGER)
    public Map createTriggerTemplate(@Valid @RequestBody TriggerTemplate bean) {
        Map result = new HashMap();
        logger.debug("begin creating trigger template");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the Trigger bean via validator object defined in the Trigger bean
        Set<ConstraintViolation<TriggerTemplate>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerTemplate> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(isValid) {
            result = triggerTemplateService.createTriggerTemplate(bean);
        }

        logger.debug("end creating trigger template");
        return result;
    }

    /**
     * update a trigger notification by using its primary id.
     * @param bean
     * @return
     */
    @PutMapping(TEMPLATE_TRIGGER)
    public Map updateTrigger(@Valid @RequestBody TriggerTemplate bean) {
        Map result = new HashMap();
        logger.debug("begin update trigger template");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the Trigger bean via validator object defined in the Trigger bean
        Set<ConstraintViolation<TriggerTemplate>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerTemplate> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        String triggertemplateId = bean.getTriggerId();
        if(Utils.notEmptyString(triggertemplateId) && isValid){
            // update trigger
            int rec = triggerTemplateService.updateTrigger(bean, triggertemplateId);
            if(rec > 0) {
                // set the success response
                result.put(Constants.SUCCESS, true);
                // assign the updated result
                result.put(Constants.RESULT, triggerTemplateService.getTriggerTemplateById(triggertemplateId, bean.getBusinessUniqueId(), bean.getTimezone()));
            }
            else{
                result.put(Constants.SUCCESS, false);
            }

        }else{
            // assigning failure status & error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, "TriggerId is mandatory");
        }

        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * delete a trigger  by using its primary id.
     * @return
     */
    @DeleteMapping(TEMPLATE_TRIGGER)
    public Map deleteTrigger(@RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId) {
        Map result = new HashMap();
        logger.debug("begin delete trigger template and its schedule");
        if(Utils.notEmptyString(triggerId)){
            // delete trigger
            int rec = triggerTemplateService.deleteTriggerTemplate(triggerId);
            // assign success response
            result.put(Constants.SUCCESS, rec > 0 ? true : false);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerTemplateId is mandatory");
        }
        logger.debug("end delete trigger template and its schedule");
        return result;
    }

    /**
     * get a trigger temlpate by using its unique id.
     * @return
     */
    @GetMapping(TEMPLATE_TRIGGER)
    public Map getTriggerTemplatesDetails(@RequestParam(defaultValue = "") String businessId,
                          @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone,
                          @RequestParam String userId,
                          @RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId) {

        Map result = new HashMap();
        logger.debug("begin get trigger template");
        if(Utils.notEmptyString(triggerId)){
            // get trigger details
            TriggerTemplate trigger = triggerTemplateService.getTriggerTemplateById(triggerId, businessId, timezone);
            if(trigger != null && Utils.notEmptyString(trigger.getTriggerId())) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, trigger);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerTemplateId is mandatory");
        }
        logger.debug("end get trigger template");
        return result;
    }

    /**
     * get a trigger names list by using templateId.
     * @return
     */
    @GetMapping("template/{templateId}")
    public Map getTriggerTemplatesListById(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid templateId") String templateId,
                                           @RequestParam(defaultValue = "") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId) {

        Map result = new HashMap();
        logger.debug("begin get trigger template name list");
        if(Utils.notEmptyString(templateId)){
            // get trigger details
            logger.info("trigger details");
            Map trigger = triggerTemplateService.getTriggerTemplatesNameListById(templateId, businessId);
            logger.info(trigger.toString());
            if(trigger != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, trigger.get("triggers"));
                result.put(Constants.WORKFLOWS, trigger.get("workflows"));
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TemplateId is mandatory");
        }
        logger.debug("end get trigger template name list");
        return result;
    }

    /**************** schedule **********/
    /**
     * create a new trigger template schedule for given request.
     * @param bean
     * @return
     */
    @PostMapping(TEMPLATE_TRIGGER_SCHEDULE)
    public Map createTriggerSchedule(@Valid @RequestBody TriggerTemplateSchedule bean, HttpServletRequest request) {
        Map result = new HashMap();
        logger.debug("begin creating trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the bean via validator object defined in the bean
        Set<ConstraintViolation<TriggerTemplateSchedule>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerTemplateSchedule> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(isValid) {
            String host = request.getHeader("host");
            host = host.contains("localhost") ? "http://localhost:4200" : "https" + "://" + dtpDomain;
            bean.setHost(host);
            result = triggerTemplateService.createTriggerTemplateSchedule(bean);
        }

        logger.debug("end creating trigger template schedule");
        return result;
    }

    /**
     * update a trigger schedule by using its primary id.
     * @param bean
     * @return
     */
    @PutMapping(TEMPLATE_TRIGGER_SCHEDULE+"/{scheduleId}")
    public Map updateTriggerSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId,
                                     @Valid @RequestBody TriggerTemplateSchedule bean) {
        Map result = new HashMap();
        logger.debug("begin update trigger notification");
        // initializing variable to be true
        boolean isValid = true;
        // instantiating a validator factory
        /*ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        // instantiating a validator object
        Validator validator = factory.getValidator();
        // validating the schedule bean via validator object defined in the schedule bean
        Set<ConstraintViolation<TriggerTemplateSchedule>> violations = validator.validate(bean);
        // iterating all the voilations and send the error message as api response
        for (ConstraintViolation<TriggerTemplateSchedule> violation : violations) {
            // if validations fails assigning false to isValid variable.
            isValid = false;
            // assigning failure status & validation error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, violation.getMessage());
        }*/
        if(Utils.notEmptyString(scheduleId)  && isValid){
            int rec = triggerTemplateService.updateTriggerSchedule(bean, scheduleId);
            if (rec > 0) {
                // set the success response
                result.put(Constants.SUCCESS, true);
                // assign the updated result
                result.put(Constants.RESULT, triggerTemplateService.getTriggerTemplateScheduleById(scheduleId, bean.getTimezone()));
            } else {
                result.put(Constants.SUCCESS, false);
                result.put(Constants.ERROR, "Record not found");
            }
        }else{
            // assigning failure status & error message to result
            result.put(Constants.SUCCESS, isValid);
            result.put(Constants.ERROR, "scheduleId is mandatory");
        }

        logger.debug("end update trigger notification");
        return result;
    }

    /**
     * delete a trigger schedule by using its primary id.
     * @return
     */
    @DeleteMapping(TEMPLATE_TRIGGER_SCHEDULE+"/{scheduleId}")
    public Map deleteTriggerSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId) {
        Map result = new HashMap();
        logger.debug("begin delete trigger schedule");
        if(Utils.notEmptyString(scheduleId )){
            // delete trigger
            int rec = triggerTemplateService.deleteTriggerSchedule(scheduleId);
            // assign success response
            result.put(Constants.SUCCESS, rec > 0 ? true : false);
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId is mandatory");
        }
        logger.debug("end update trigger schedule");
        return result;
    }

    /**
     * get a trigger Schedule by using its primary id.
     * @return
     */
    @GetMapping(TEMPLATE_TRIGGER_SCHEDULE+"/{scheduleId}")
    public Map getTriggerTemplateSchedule(@PathVariable @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid scheduleId") String scheduleId,
                                          @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone) {
        Map result = new HashMap();
        logger.debug("begin get trigger notification");
        if(Utils.notEmptyString(scheduleId)){
            // get trigger details
            TriggerTemplateSchedule schedule = triggerTemplateService.getTriggerTemplateScheduleById(scheduleId, timezone);
            if(schedule != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, schedule);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "ScheduleId is mandatory");
        }
        logger.debug("end get trigger notification");
        return result;
    }

    /**
     * get a list of triggers within template Schedule by using trigger id.
     * @return
     */
    @GetMapping(TEMPLATE_TRIGGER+"/schedule")
    public Map getTriggerScheduleByTriggerId(@RequestParam @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid triggerId") String triggerId,
                                             @RequestParam(defaultValue = Constants.TIMEZONE_AMERICA_LOSANGELES) String timezone,
                                             @RequestParam(defaultValue = "") @Pattern(regexp = DTConstants.UUID_REGREX, message = "Invalid businessId") String businessId) {
        Map result = new HashMap();
        logger.debug("begin get trigger schedule by triggerID");
        if(Utils.notEmptyString(triggerId)){
            // get trigger details
            List<TriggerTemplateSchedule> schedule = triggerTemplateService.getTriggerScheduleByTriggerId(triggerId, timezone, businessId);
            if(schedule != null) {
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, schedule);
            }else{
                // assign success response
                result.put(Constants.SUCCESS, true);
                result.put(Constants.RESULT, "Record Not Found");
            }
        }else{
            // assign failure status and corresponding error message
            result.put(Constants.SUCCESS, false);
            result.put(Constants.ERROR, "TriggerId is mandatory");
        }
        logger.debug("end get trigger schedule by triggerID");
        return result;
    }

}
