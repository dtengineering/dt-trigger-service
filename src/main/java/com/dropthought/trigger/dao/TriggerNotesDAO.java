package com.dropthought.trigger.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class TriggerNotesDAO {


    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * namedParameterJdbcTemplate for DB Connectivity.
     */
    private static final String GET_ACTIVE_NOTES_NOTIFICATION = "select id, channel, message, start_time, subject, status, contacts, sent_status, business_id, notes_uuid, notes_notification_uuid, created_by, modified_by, created_time, modified_time from notes_notification where status = 1 and sent_status is null and start_time <= current_timestamp order by start_time limit 1000";
    private static final String DELETE_NOTES_NOTIFICATION_BY_NOTIFY_ID = "delete from notes_notification where id = ?";
    private static final String UPDATE_NOTES_NOTIFICATION_SENT_STATUS_BY_NOTIFY_ID = "update notes_notification set sent_status = ? where id = ?";

    private static final String INSERT_INTO_NOTES_NOTIFICATION_ARCHIVE = "INSERT INTO notes_notification_archive " +
            "(channel, message, start_time, subject, status, " +
            "contacts, sent_status, business_id, notes_uuid, " +
            "notes_notification_uuid, created_by, modified_by, " +
            "created_time, modified_time) " +
            "SELECT channel, message, start_time, subject, status, " +
            "contacts, ?, business_id, notes_uuid, " +
            "notes_notification_uuid, created_by, modified_by, " +
            "created_time, modified_time " +
            "FROM notes_notification " +
            "WHERE id = ?";

    /**
     * Method to get active notes notification scheduled to send push notification
     *
     * @return
     */
    public List getActiveNotesNotifications() {
        logger.debug("begin retrieving the active trigger notification");
        List resultList = new ArrayList();
        try {
            resultList = jdbcTemplate.queryForList(GET_ACTIVE_NOTES_NOTIFICATION);
            logger.debug("end retrieving the active trigger notification");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
        }
        return resultList;
    }

    /**
     * Method to create entry in notes_notification_archive table from notes_notification table before deleting entry from notes_notification table
     * @param notifyId
     * @param status
     */
    public void createEntryInNotesNotificationArchive(int notifyId, String status) {
        try {
            jdbcTemplate.update(INSERT_INTO_NOTES_NOTIFICATION_ARCHIVE, status, notifyId);
        }catch (Exception e){
            throw new RuntimeException("Transaction failed", e);
        }
    }

    /**
     * Method to delete entry from notes_notification table
     * @param notifyId
     */
    public void deleteEntryFromNotesNotification(int notifyId) {
        try {
            jdbcTemplate.update(DELETE_NOTES_NOTIFICATION_BY_NOTIFY_ID, notifyId);
        }catch (Exception e){
            throw new RuntimeException("Transaction failed", e);
        }
    }

    /**
     * Method to update notes notification status by notify id
     * @param notifyId
     * @param status
     */
    public void updateNotesNotificationStatusByNotifyId(int notifyId, String status){
        try {
            jdbcTemplate.update(UPDATE_NOTES_NOTIFICATION_SENT_STATUS_BY_NOTIFY_ID, status, notifyId);
        }catch (Exception e){
            logger.error("Error in updateNotesNotificationStatusByNotifyId", e);
        }
    }


}
