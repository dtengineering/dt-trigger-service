package com.dropthought.trigger.dao;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.TriggerTemplate;
import com.dropthought.trigger.model.TriggerTemplateSchedule;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class TriggerTemplateDAO {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TriggerDAO triggerDAO;

    /**
     * namedParameterJdbcTemplate for DB Connectivity.
     */
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /****************** trigger_templates queries*********************/
    private static final String INSERT_TRIGGER_TEMPLATE = "insert into trigger_templates (trigger_name, trigger_condition, status, template_name, template_id, created_by, trigger_uuid, created_time, frequency, business_id) values (?,?,?,?,?,?,?,current_timestamp,?,?) ";
    private static final String UPDATE_TRIGGER_TEMPLATE = "update trigger_templates set trigger_name = ?, trigger_condition=?, template_name = ?, template_id = ?, modified_by =?, modified_time=?, status=?, frequency = ?, readable_query=null where trigger_uuid = ? ";
    private static final String GET_TRIGGERS_TEMPLATE = "select trigger_uuid, template_id, template_name, trigger_name, trigger_condition, frequency, business_id from trigger_templates where template_id=? and business_id = ?";
    private static final String TRIGGER_TEMPLATE_NAME_CHECK = "select count(id) from trigger_templates where upper(trigger_name) = ? and created_by = ? and business_id=?";
    private static final String GET_TRIGGER_TEMPLATE_BY_UUID = "select id, trigger_uuid, template_id, template_name, trigger_name, trigger_condition, status, modified_time, created_by, business_id from trigger_templates where trigger_uuid=? ";
    private static final String GET_SURVEY_TEMPLATE_ID = "select id from template_%s where template_uuid=?";
    private static final String DELETE_TRIGGER_TEMPLATE = "delete from trigger_templates where trigger_uuid = ?";

    /*** trigger_tempalte_schedule table queries *****/
    private static final String INSERT_TRIGGER_TEMPLATE_SCHEDULE = "insert into trigger_template_schedule (frequency, business_id, template_id, timezone, trigger_id, schedule_uuid, channel, message, subject, contacts, add_feedback, trigger_config, status, host, header_data, webhook_config, integrations_config) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    private static final String UPDATE_TRIGGER_TEMPLATE_SCHEDULE = "update trigger_template_schedule set frequency=?, timezone=?, channel=?, message=?, subject=?, add_feedback=?, contacts=?, trigger_config=?, status=?,  header_data=?, webhook_config=?, integrations_config = ? where schedule_uuid=?";
    private static final String DELETE_TRIGGER_TEMPLATE_SCHEDULE = "delete from trigger_template_schedule where schedule_uuid = ?";
    private static final String DELETE_TRIGGER_SCHEDULES_BY_TRIGGER_ID = "delete from trigger_template_schedule where trigger_id = (select id from trigger_templates where trigger_uuid = ?)";
    private static final String GET_TRIGGER_TEMPLATE_SCHEDULE_BY_UNIQUE_ID = "select id, status, frequency, business_id, template_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, header_data, webhook_config, integrations_config from trigger_template_schedule where schedule_uuid=?";
    //Ignore the business id while listing the schedules of a trigger template
    private static final String GET_TRIGGER_TEMPLATE_SCHEDULE_BY_TRIGGER_ID = "select id,status,  frequency, business_id, template_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, header_data, webhook_config from trigger_template_schedule where trigger_id = ?";
    //private static final String GET_TRIGGER_TEMPLATE_SCHEDULE_BY_TRIGGER_ID = "select id,status,  frequency, business_id, template_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, header_data, webhook_config from trigger_template_schedule where trigger_id = ? and business_id=?";
    private static final String GET_TRIGGER_TEMPLATE_SCHEDULE_BY_ID = "select status, frequency, business_id, template_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, header_data, webhook_config from trigger_template_schedule where id=?";

    /**
     * Method to create trigger condition details using business uuid
     *
     * @param trigger
     * @return
     */
    public String createTriggerTemplate(final TriggerTemplate trigger) {
        logger.debug("begin creating the trigger template details");
        String result = "";
        try {
            int userId = Utils.notEmptyString(trigger.getUserId()) ? triggerDAO.getUserIdByUUID(trigger.getUserId()) : 0;
            int templateId = this.returnTemplateIdByUniqueId(trigger.getTemplateId(), trigger.getBusinessUniqueId(), "dropthought");
            int businessId = triggerDAO.getBusinessIdByUUID(trigger.getBusinessUniqueId());
            final String uuid = Utils.generateUUID();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_TRIGGER_TEMPLATE, Statement.RETURN_GENERATED_KEYS);
                    logger.info("trigger Bean {}", trigger.toString());
                    int status = Utils.isNotNull(trigger.getStatus()) && trigger.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
                    statement.setString(1, (trigger.getTriggerName()));
                    statement.setString(2, trigger.getTriggerCondition());
                    statement.setInt(3, status);
                    statement.setString(4, trigger.getTemplateName());
                    statement.setInt(5, templateId);
                    statement.setInt(6, userId);
                    statement.setString(7, uuid);
                    statement.setString(8, Utils.isNotNull(trigger.getFrequency()) ? trigger.getFrequency() : "");
                    statement.setInt(9, businessId);
                    return statement;
                }
            });
            result = uuid;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.debug("end creating trigger details");
        return result;
    }

    /**
     * check trigger name exists or not by passing businessId and trigger Name
     *
     * @param triggerName
     * @param businessUniqueId
     * @return true: trigger name exists / false: trigger name not exists
     */
    public boolean isTriggerTemplateNameExists(String triggerName, String businessUniqueId, String userUniqueId ) {
        int userId = triggerDAO.getUserIdByUUID(userUniqueId);
        int businessId = triggerDAO.getBusinessIdByUUID(businessUniqueId);
        logger.debug("begin trigger name check");
        try {
            Integer recordCount = jdbcTemplate.queryForObject(TRIGGER_TEMPLATE_NAME_CHECK, new Object[]{triggerName.toUpperCase(), userId, businessId}, Integer.class);
            if (recordCount > 0) {
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end trigger name check");
        return false;
    }

    /**
     * Method to update trigger template details using business id by triggerId
     *
     * @param trigger
     * @param triggerId
     * @return
     */
    public int updateTriggerTemplate(final TriggerTemplate trigger, String triggerId) {
        logger.info("begin update the trigger template details");
        int result = 0;
        try {
            // get survey id for given survey unique id
            int templatePrimaryId = this.returnTemplateIdByUniqueId(trigger.getTemplateId(), trigger.getBusinessUniqueId(), "dropthought");
            trigger.setTemplateId(String.valueOf(templatePrimaryId));
            String frequency = Utils.isNotNull(trigger.getFrequency()) ? trigger.getFrequency() : Constants.EMPTY;
            int userId = Utils.notEmptyString(trigger.getUserId()) ? triggerDAO.getUserIdByUUID(trigger.getUserId()) : 0;
            int status = Utils.isNotNull(trigger.getStatus()) && trigger.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
            result = jdbcTemplate.update(UPDATE_TRIGGER_TEMPLATE, new Object[]{trigger.getTriggerName(), trigger.getTriggerCondition(), trigger.getTemplateName(), trigger.getTemplateId(), userId, (new Timestamp(System.currentTimeMillis())), status, frequency, triggerId});
            logger.debug("end update the trigger template details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            logger.error("Exception at updateTriggerTemplate. Msg {}", e.getMessage());
            result = -1;
        }
        return result;
    }

    /**
     * Method to get triggers template details  by triggerUUId
     *
     * @param triggerId
     * @return
     */
    public Map getTriggerTemplateMapByUUId(String triggerId) {
        logger.debug("begin retrieving the trigger template details by triggerUUID");
        Map triggerMap = null;
        try {
            List triggerList = jdbcTemplate.queryForList(GET_TRIGGER_TEMPLATE_BY_UUID, new Object[]{triggerId});
            if(triggerList.size() > 0){
                triggerMap = (Map) triggerList.get(0);
            }
            logger.debug("end retrieving the trigger  template details by triggerUUID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerMap;
    }

    /**
     * get surveyId by using surveyUniqueID
     *
     * @param surveyUniqueId
     * @param businessUniqueId
     * @return
     */
    public Integer getSurveyTemplateIdByUUID(String surveyUniqueId, String businessUniqueId) {
        logger.debug("begin get survey template id");
        Integer id = 0;
        try {
            String query = String.format(GET_SURVEY_TEMPLATE_ID, businessUniqueId.replaceAll("-", "_"));
            id = jdbcTemplate.queryForObject(query, new Object[]{surveyUniqueId}, Integer.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get survey template id");
        return id;
    }

    /**
     * return a program template by templateUUID
     *
     * @param templateId
     * @param businessUUID
     * @return
     */
    public String returnTemplateUniqueIdById(int templateId, String businessUUID, String type) {
        String uniqueId = "";
        try {
            String tableName = "templates";
            String sql = "";
            if(!type.equals(Constants.DROPTHOUGHT_TYPE)) {
                String businessUUIDParsed = businessUUID.replace("-", "_");
                tableName += "_" + businessUUIDParsed;
            }

            String surveySql = "select count_questions, theme, industry, completion_time, used_count, id, state, active,anonymous,template_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys,multi_questions, template_property, template_uuid, created_time, modified_time, created_by, modified_by, template_names, survey_descriptions from " + tableName + " where id = ?";
            Map programMap = jdbcTemplate.queryForMap(surveySql, new Object[]{templateId});
            if(programMap.containsKey("template_uuid"))
                uniqueId = programMap.get("template_uuid").toString();

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return uniqueId;
    }

    /**
     * Method to delete trigger notification details using business uuid by triggerId
     *
     * @param triggerId
     * @return
     */
    public int deleteTriggerTemplate(String triggerId) {
        logger.debug("begin deleting the trigger template details");
        int result = 0;
        try {

            this.deleteTriggerTemplateScheduleByTriggerTemplateId(triggerId);
            result = jdbcTemplate.update(DELETE_TRIGGER_TEMPLATE, new Object[]{triggerId});

            logger.debug("end deleting the trigger template details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * return a program template by templateUUID
     *
     * @param templateId
     * @param businessUUID
     * @return
     */
    public int returnTemplateIdByUniqueId(String templateId, String businessUUID, String type) {
        int id = 0;
        try {
            String tableName = "templates";
            if(!type.equals(Constants.DROPTHOUGHT_TYPE)) {
                String businessUUIDParsed = businessUUID.replace("-", "_");
                tableName += "_" + businessUUIDParsed;
            }

            String surveySql = "select id from " + tableName + " where template_uuid = ?";
            Map programMap = jdbcTemplate.queryForMap(surveySql, new Object[]{templateId});
            if(programMap.containsKey("id"))
                id = (int)programMap.get("id");

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return id;
    }

    /**
     * return a trigger template details by template and businessID
     *
     * @param templateId
     * @param businessId
     * @return
     */
    public List getTriggerTemplateMapByTemplateId(int templateId, int businessId) {
        logger.info("get trigger template by template id {}",templateId);
        List triggerNames = new ArrayList();
        try {
            /**
             * DTV-4822 List triggers of a template without business id restriction
             */
            //String surveySql = "select id, trigger_name, trigger_uuid, template_id, template_name, trigger_condition,status,frequency from trigger_templates where template_id = ? and business_id = ?";
            String surveySql = "select id, trigger_name, trigger_uuid, template_id, template_name, trigger_condition,status,frequency from trigger_templates where template_id = ?";
            //triggerNames = jdbcTemplate.queryForList(surveySql, new Object[]{templateId, businessId});
            triggerNames = jdbcTemplate.queryForList(surveySql, new Object[]{templateId});
            logger.info("list of triggers");
            logger.info(triggerNames.toString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return triggerNames;
    }

    /**
     * Method to create trigger schedule details
     *
     * @param schedule
     * @return
     */
    public String createTriggerTemplateSchedule(final TriggerTemplateSchedule schedule) {
        logger.debug("begin creating the trigger schedule details");
        String result = "";
        try {
            // generate uniqueId for schedule
            String uuid = Utils.generateUUID();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_TRIGGER_TEMPLATE_SCHEDULE, Statement.RETURN_GENERATED_KEYS);
                    logger.info("schedule Bean {}", schedule.toString());
                    statement.setString(1, schedule.getFrequency());
                    statement.setString(2, schedule.getBusinessId());
                    statement.setString(3, schedule.getTemplateId());
                    statement.setString(4, schedule.getTimezone());
                    statement.setString(5, schedule.getTriggerId());
                    statement.setString(6, uuid);
                    statement.setString(7, schedule.getChannel());
                    statement.setString(8, schedule.getMessage());
                    statement.setString(9, schedule.getSubject());
                    statement.setString(10, Utils.isNotNull(schedule.getContacts()) ? new JSONArray(schedule.getContacts()).toString() : new JSONArray().toString());
                    statement.setInt(11, schedule.isAddFeedback() ? 1 : 0);
                    statement.setString(12, Utils.isNotNull(schedule.getTriggerConfig()) ? new JSONObject((Map) schedule.getTriggerConfig()).toString() : new JSONObject().toString());
                    statement.setString(13, Utils.isNotNull(schedule.getStatus()) ? schedule.getStatus() : Constants.STATUS_INACTIVE);
                    statement.setString(14, schedule.getHost());
                    statement.setString(15, Utils.isNotNull(schedule.getHeaderData()) ? new JSONObject(schedule.getHeaderData()).toString() : new JSONObject().toString());
                    statement.setString(16, Utils.isNotNull(schedule.getWebhookConfig()) ? new JSONObject(schedule.getWebhookConfig()).toString() : new JSONObject().toString());
                    statement.setString(17, Utils.isNotNull(schedule.getIntegrationsConfig()) ? new JSONObject(schedule.getIntegrationsConfig()).toString() : new JSONObject().toString());
                    return statement;
                }
            });
            result = uuid;
            logger.debug("end creating trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to update trigger schedule details by scheduleId
     *
     * @param schedule
     * @param scheduleId
     * @return
     */
    public int updateTriggerTemplateSchedule(final TriggerTemplateSchedule schedule, String scheduleId) {
        logger.debug("begin update the trigger template schedule details");
        int result = -1;
        try {

            String status = Utils.isNotNull(schedule.getStatus()) ? schedule.getStatus() : Constants.STATUS_INACTIVE;
            String contacts = Utils.isNotNull(schedule.getContacts()) ? new JSONArray(schedule.getContacts()).toString() : new JSONArray().toString();
            String message = Utils.isNotNull(schedule.getMessage()) ? schedule.getMessage() : "";
            String subject = Utils.isNotNull(schedule.getSubject()) ? schedule.getSubject() : "";
            boolean isFeedback = Utils.isNotNull(schedule.isAddFeedback()) ? schedule.isAddFeedback() : false;

            String triggerConfig = new JSONObject().toString();
            if(Utils.isNotNull(schedule.getTriggerConfig())){
                Map triggerConfigMap = (Map)schedule.getTriggerConfig();
                triggerConfig = new JSONObject(triggerConfigMap).toString();
            }

            String headerData = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getHeaderData())) {
                headerData = new JSONObject(schedule.getHeaderData()).toString();
            }

            String webhook = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getWebhookConfig())) {
                webhook = new JSONObject(schedule.getWebhookConfig()).toString();
            }

            String integrationConfig = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getIntegrationsConfig())) {
                integrationConfig = new JSONObject(schedule.getIntegrationsConfig()).toString();
            }

            result = jdbcTemplate.update(UPDATE_TRIGGER_TEMPLATE_SCHEDULE,
                    new Object[]{schedule.getFrequency(), schedule.getTimezone(), schedule.getChannel(), message, subject,
                            isFeedback, contacts, triggerConfig, status,
                            headerData,webhook,integrationConfig, scheduleId});
            logger.debug("end update the trigger template schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to delete trigger schecdule details by triggerId
     *
     * @param scheduleId
     * @return
     */
    public int deleteTriggerTemplateSchedule(String scheduleId) {
        logger.debug("begin deleting the trigger template schedule details");
        int result = 0;
        try {
            result = jdbcTemplate.update(DELETE_TRIGGER_TEMPLATE_SCHEDULE, new Object[]{scheduleId});
            logger.debug("end deleting the trigger template schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }


    /**
     * Method to delete trigger schecdules details by triggerId
     *
     * @param triggerUUID
     * @return
     */
    public int deleteTriggerTemplateScheduleByTriggerTemplateId(String triggerUUID) {
        logger.debug("begin deleting the trigger schedule details");
        int result = 0;
        try {
            result = jdbcTemplate.update(DELETE_TRIGGER_SCHEDULES_BY_TRIGGER_ID, new Object[]{triggerUUID});
            logger.debug("end deleting the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to get triggers schedule details by scheduleId
     *
     * @param scheduleId
     * @return
     */
    public Map getTriggerTemplateScheduleByUniqueID(String scheduleId) {
        logger.debug("begin retrieving the trigger template schedule details by unique ID");
        Map scheduleMap = null;
        try {

            scheduleMap = jdbcTemplate.queryForMap(GET_TRIGGER_TEMPLATE_SCHEDULE_BY_UNIQUE_ID, new Object[]{scheduleId});
            logger.debug("end retrieving the trigger template details by unique ID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return scheduleMap;
    }

    /*//**
     * Method to get triggers schedule details by scheduleId
     *
     * @param scheduleId
     * @return
     *//*
    public Map getTriggerTemplateScheduleById(int scheduleId) {
        logger.debug("begin retrieving the trigger template schedule details by ID");
        Map scheduleMap = null;
        try {
            scheduleMap = jdbcTemplate.queryForMap(GET_TRIGGER_TEMPLATE_SCHEDULE_BY_ID, new Object[]{scheduleId});
            logger.debug("end retrieving the trigger template details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return scheduleMap;
    }*/

    /**
     * Method to get triggers schedule details by triggerId
     *
     * @param triggerId
     * @return
     */
    public List getTriggerTemplateScheduleByTriggerID(int triggerId, int businessId) {
        logger.debug("begin retrieving the trigger template schedule details by triggerID");
        List triggerScheduleList = new ArrayList();
        try {
            //triggerScheduleList = jdbcTemplate.queryForList(GET_TRIGGER_TEMPLATE_SCHEDULE_BY_TRIGGER_ID, new Object[]{triggerId, businessId});
            triggerScheduleList = jdbcTemplate.queryForList(GET_TRIGGER_TEMPLATE_SCHEDULE_BY_TRIGGER_ID, new Object[]{triggerId});
            logger.debug("end retrieving the trigger template details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleList;
    }

    /**
     * return a program template by templateUUID
     *
     * @param templateUUID
     * @param businessUUID
     * @return
     */
    public Map returnProgramTemplateByUUID(String templateUUID, String businessUUID, String type) {
        Map programMap = new HashMap();
        try {
            String tableName = "templates";
            String sql = "";
            if(!type.equals("dropthought")) {
                String businessUUIDParsed = businessUUID.replace("-", "_");
                tableName += "_" + businessUUIDParsed;
            }

            if(type.equalsIgnoreCase("all"))
            {
                String table = "templates";
                String parsedTable = table + "_" + businessUUID.replace("-", "_");
                sql = "select count_questions, theme, industry, completion_time, used_count, id, state, active, anonymous, template_meta_data , question_data, question_ids, metrics_map, languages, rules, multi_surveys, multi_questions, template_property, template_uuid, created_time, modified_time, created_by, modified_by, template_names, survey_descriptions, purpose, template_image, template_type, related_template_ids, business_ids from " + table + " where template_uuid = ?" +
                        "UNION" + " select count_questions, theme, industry, completion_time, used_count, id, state, active, anonymous, template_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys, multi_questions, template_property, template_uuid, created_time, modified_time, created_by, modified_by, template_names, survey_descriptions,purpose, template_image, template_type, related_template_ids, null from " + parsedTable + " where template_uuid = ?";
            }

            String surveySql = "select count_questions, theme, industry, completion_time, used_count, id, state, active,anonymous,template_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys,multi_questions, template_property, template_uuid, created_time, modified_time, created_by, modified_by, template_names, survey_descriptions, purpose, template_image, template_type, related_template_ids";

            if(type.equalsIgnoreCase("all"))
            {
                programMap = jdbcTemplate.queryForMap(sql, new Object[]{templateUUID,templateUUID});
            }
            else
            {
                if(type.equals("dropthought")) {
                    surveySql += ", business_ids";
                }
                surveySql += "  from " + tableName + " where template_uuid = ?" ;
                programMap = jdbcTemplate.queryForMap(surveySql, new Object[]{templateUUID});
            }

        } catch (Exception e) {
//            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return programMap;
    }


    /**
     * return a workflows by templateUUID
     *
     * @param templateId
     * @param businessId
     * @return
     */
    public List getTriggerTemplateWorkflowByTemplateId(String templateId, int businessId) {
        List responseList = new ArrayList();
        try {
            String getWorkflowsSql = "SELECT workflows from template_workflows where template_id = ?";
            responseList = jdbcTemplate.queryForList(getWorkflowsSql, new Object[]{templateId});
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ArrayList();
        }
        return responseList;
    }
}
