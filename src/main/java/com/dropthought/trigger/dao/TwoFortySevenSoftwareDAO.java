package com.dropthought.trigger.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Component
public class TwoFortySevenSoftwareDAO {

    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    /**
     * Method to get the config map from config_247software table
     * @param businessId
     * @return
     */
    public Map<String, Object> getConfigMap(int businessId) {
        Map<String, Object> configMap = new HashMap<>();
        try {
            String sql = "SELECT * FROM config_247software WHERE dt_business_id = ? and connection_status = '1'";
            configMap = jdbcTemplate.queryForMap(sql, new Object[]{businessId});
        } catch (EmptyResultDataAccessException e) {
            logger.error("No Data found in config_247software table");
        } catch (Exception e) {
            logger.error("Error while fetching the config map from config_247software table", e);
        }
        return configMap;
    }

    /**
     * Method to create failure table when API fails to connect to 24/7 software or any other failure
     * @param businessUUID
     */
    public void createSoftwareFailureTable(String businessUUID) {

        // create the table failure_247software_{businessuuid} if not exists
        String replacedBusinessUUID = businessUUID.replace("-", "_");
        String tableName = "failure_247software_" + replacedBusinessUUID;
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + "( " +
                "  `id` int(10) NOT NULL AUTO_INCREMENT, " +
                "  `software_endpoint` varchar(200) NOT NULL, " +
                "  `token` varchar(500) NOT NULL, " +
                "  `method` varchar(15) NOT NULL, " +
                "  `failure_message` varchar(500) NOT NULL, " +
                "  `payload` JSON NULL, " +
                "  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, " +
                "  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP, " +
                "  PRIMARY KEY (`id`) " +
                ") ENGINE=InnoDB  DEFAULT CHARSET=utf8;";
        jdbcTemplate.execute(sql);
    }

    /**
     * Method to insert failure data in failure_247software table
     * @param baseUrl
     * @param token
     * @param method
     * @param message
     * @param payload
     * @param businessUUID
     */
    public void insertFailureData(String baseUrl, String token, String method, String message, String businessUUID, String payload) {
        try {
            String replacedBusinessUUID = businessUUID.replace("-", "_");
            String sql = "INSERT INTO failure_247software_" + replacedBusinessUUID +
                    " (software_endpoint, token, method, failure_message, payload, created_time) VALUES (?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, baseUrl, token, method, message, payload, new Timestamp(System.currentTimeMillis()).toString());
        } catch (Exception e) {
            logger.error("Error while inserting failure data in failure_247software table", e);
        }
    }
}
