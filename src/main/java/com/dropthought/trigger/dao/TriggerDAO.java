package com.dropthought.trigger.dao;

import com.dropthought.trigger.common.Constants;
import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.*;
import com.dropthought.trigger.service.TriggerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Component
public class TriggerDAO {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * namedParameterJdbcTemplate for DB Connectivity.
     */
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    /*Page*/
    private Integer upperLimit = 0;
    private Integer length = 10;

    /*************** query statements - starts ************************/
    /*** trigger_{businesuuid} tables query *****/
    private static final String INSERT_TRIGGER = "insert into trigger_%s (trigger_name, trigger_condition, status, survey_name, survey_id, created_by, trigger_uuid, modified_time, start_date, end_date, start_time, end_time, frequency) values (?,?,?,?,?,?,?,current_timestamp, ?,?,?,?,?) ";
    private static final String UPDATE_TRIGGER = "update trigger_%s set trigger_name = ?, trigger_condition=?, survey_name = ?, survey_id = ?, modified_by =?, modified_time=?, status=?, start_date = ?, end_date =?, start_time =?, end_time =?, frequency = ?, readable_query=null where trigger_uuid = ? ";
    private static final String GET_TRIGGERS = "select trigger_uuid, survey_id, survey_name, trigger_name, trigger_condition, start_date, end_date, start_time, end_time, frequency, a.status, a.modified_time, a.created_by, survey_uuid, from_date, to_date, b.anonymous, b.state from trigger_%s a, surveys_%s b where a.survey_id=b.id and a.created_by = ? and (b.state <> '6')";
    //    private static final String GET_TRIGGER_BY_UUID = "select id, trigger_uuid, survey_id, survey_name, trigger_name, trigger_condition, status, modified_time, created_by from trigger_%s where trigger_uuid=? ";
    private static final String GET_TRIGGER_BY_UUID = " select a.id, a.trigger_uuid, a.survey_id, a.survey_name, a.trigger_name, a.trigger_condition, b.from_date, b.to_date, a.status, a.modified_time, a.created_by, a.start_date, a.end_date, a.start_time, a.end_time, a.frequency, b.anonymous, a.trigger_filter_uuid from trigger_%s a, surveys_%s b  where a.survey_id = b.id and a.trigger_uuid= ?";
    private static final String DELETE_TRIGGER = "delete from trigger_%s where trigger_uuid = ?";
    private static final String UPDATE_TRIGGER_STATUS = "update trigger_%s set status = ?, modified_time=? where trigger_uuid = ?";
    private static final String TRIGGER_NAME_CHECK = "select count(id) from trigger_%s where upper(trigger_name) = ? and created_by = ? and survey_id = ?";
    private static final String GET_TRIGGER_BY_ID = "select id, survey_id, trigger_name, survey_name, trigger_condition, status, readable_query, created_by, trigger_filter_uuid from trigger_%s where id=?";
    private static final String GET_USER_BY_TRIGGERID = "select  created_by from trigger_%s where id=?";

    private static final String GET_ALL_TRIGGERS_BY_SURVEYID = " select id, trigger_uuid, trigger_name, trigger_condition, readable_query, status, survey_name, survey_id, created_time, created_by, modified_time, modified_by, start_date, end_date, start_time, end_time, frequency, deactivated_conditions from trigger_%s where survey_id=?";
    private static final String GET_TRIGGERS_COUNT = "select count(id) from trigger_%s where created_by = ? and survey_id  IN (SELECT id FROM surveys_%s where state <> '6')";
    private static final String GET_SURVEY_DATE_BY_TRIGGER_ID = "select distinct concat(a.start_date,\" \",a.start_time) as trigger_fromdate, concat(a.start_date, \" \",a.start_time) as trigger_endeate, b.from_date, b.to_date, b.survey_uuid from trigger_schedule a, surveys_%s b where a.survey_id = b.id and  a.trigger_id= ?;";
    private static final String UPDATE_TRIGGER_READABLE_QUERY = "update trigger_%s set readable_query = ? where id = ?";
    private static final String UPDATE_TRIGGER_STATE = "update trigger_%s set status = ? where survey_id = ?";
    private static final String GET_TRIGGERS_BY_TRIGGER_IDS = "select id, trigger_uuid, trigger_name, trigger_condition, deactivated_conditions, readable_query, status, survey_name, survey_id, created_time, created_by, modified_time, modified_by, frequency from trigger_%s where id in (:triggerIds)";
    private static final String UPDATE_TRIGGER_DEACTIVATED_CONDITION_BY_ID = "update trigger_%s set trigger_condition = ?, deactivated_conditions = ? , readable_query = ? where id = ?";

    private static final String GET_USER_ID = "select user_id from users where user_uuid=?";
    private static final String GET_SURVEY_ID = "select id from surveys_%s where survey_uuid=?";
    private static final String GET_SURVEY_UUID = "select survey_uuid from surveys_%s where id=?";
    private static final String GET_BUSINEES_ID = "select business_id from business where business_uuid=?";
    private static final String GET_BUSINEES_UUID = "select business_uuid from business where business_id=?";
    private static final String GET_BUSINESS_NAME = "select business_name from business where business_uuid=?";
    private static final String GET_QUESTION_ID = "select question_id from questions_%s where question_uuid=?";
    private static final String GET_QUESTION_ID_INDEX = "select json_unquote(json_search(question_data, 'one', ?)) from surveys_%s where id=?";
    //    private static final String GET_SURVEY_DETAILS_BY_ID = "select survey_uuid, anonymous, created_by, modified_by, from_date, to_date, survey_property from surveys_%s where id=?";
    private static final String GET_SURVEY_DETAILS_BY_ID = "select survey_uuid, survey_names, anonymous, created_by, modified_by, from_date, to_date, survey_property, question_ids, metrics_map, multi_questions, languages  from surveys_%s where id=?";
    private static final String GET_USERS_BY_BUSINESSUUID = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_email, u.user_uuid, u.phone, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.push_notification, u.title, u.user_role, u.alias_email, a.account_id, a.account_type, a.account_user_id, a.account_role_id from users as u  left join account as a on u.user_id = a.account_user_id  left join business as b on a.account_business_id = b.business_id where user_status = '1' and business_uuid = ? order by created_time";
    private static final String GET_SURVEY_FILTERS_BY_SURVEY_ID_LEFT_JOIN_USER_SURVEY_FILTER = "select a.user_access_id, a.user_id, a.survey_id, a.role_id, a.created_by, a.user_access_uuid, b.filter_name, b.filter_query, b.survey_filter_uuid, b.filter_name FROM user_survey_filters_%s as a left join survey_filters_%s as b on a.filter_id = b.filter_id  where a.survey_id = ? and a.user_id = ?  order by a.created_time desc";
    private static final String GET_MAPPED_SURVEYS_BY_USER_ID = "select user_survey_id, user_id, survey_id, user_survey_map_uuid, created_by, created_time from user_survey_map_%s where user_id = ? order by created_time desc";
    private static final String GET_ROLE_ID = "select a.account_role_id from users as u  left join account as a on u.user_id = a.account_user_id  where user_id = ?";
    private static final String GET_USER_DETAILS_BY_ID = "  select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_email, u.user_uuid, u.phone, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.push_notification, u.title, u.user_role, u.alias_email, a.account_id, a.account_type, a.account_user_id, a.account_role_id from users as u  left join account as a on u.user_id = a.account_user_id  left join business as b on a.account_business_id = b.business_id where user_status = '1' and business_uuid = ? and user_id = ?";
    /*** trigger_notification table queries *****/
    private static final String INSERT_TRIGGER_NOTIFICATION = "insert into trigger_notification (start_time, channel, message, status, contacts, add_feedback, schedule_id, notification_uuid , subject, condition_query, submission_token, trigger_status, action_type) values (?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    private static final String GET_NOTIFICATION_BY_UUID = "select id,start_time, channel, message, status, contacts, add_feedback, schedule_id from trigger_notification where notification_uuid=?";
    private static final String GET_NOTIFICATION_BY_SCHEDULEID = "select notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id from trigger_notification where schedule_id=?";
    private static final String GET_NOTIFICATION_BY_SCHEDULEID_BYDAY = "select notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id, created_time from trigger_notification where start_time > ? and start_time < ? and schedule_id=?";
    private static final String GET_NOTIFICATION_BY_SCHEDULEID_BYDAY_V1 = "select notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id, created_time from trigger_notification_%s where start_time > ? and start_time < ? and schedule_id=?";
    private static final String UPDATE_NOTIFICATION_BY_UUID = "update trigger_notification set channel=?, message=?, subject=?, status=?, add_feedback=?, contacts=?, modified_time=? where notification_uuid=?";
    private static final String UPDATE_NOTIFICATION_TIME_BY_UUID = "update trigger_notification set start_time=? where notification_uuid=?";
    private static final String GET_ACTIVE_NOTIFICATION = "select id,start_time, channel, message, subject, status, contacts, add_feedback, schedule_id, condition_query from trigger_notification where status = 0 and start_time <= current_timestamp order by start_time desc limit 1000";
    private static final String GET_ACTIVE_NOTIFICATION_V1 = "select a.notification_uuid, a.id, b.id as schedule_id, a.start_time, a.channel, a.message, a.subject, a.status, a.contacts, a.add_feedback, a.schedule_id, a.action_type, condition_query, business_id, survey_id, trigger_id, host, submission_token, reply_respondent, timezone from trigger_notification a, trigger_schedule b where a.schedule_id = b.id and a.status = 0 and a.start_time <= current_timestamp and a.sent_status is null and trigger_status = '1' order by a.start_time desc limit 1000";
    private static final String GET_NOTIFICATION_BY_SCHEDULEID_LATEST = "select notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id, created_time from trigger_notification where schedule_id=? order by start_time desc limit 1";
    private static final String GET_NOTIFICATION_BY_SCHEDULEID_LATEST_V1 = "select notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id, created_time from trigger_notification_%s where schedule_id=? order by start_time desc limit 1";

    private static final String DELETE_TRIGGER_NOTIFICATION = "delete from trigger_notification where notification_uuid = ?";
    private static final String DELETE_TRIGGER_NOTIFICATION_BY_ID = "delete from trigger_notification where id = ?";
    private static final String UPDATE_TRIGGER_NOTIFICATION_STATUS = "update trigger_notification set status = :state, modified_time=current_timestamp where id in (:ids)";
    private static final String GET_NOTIFICATION_TOKENS_BY_SCHEDULEID = "select json_arrayagg(submission_token) token from trigger_notification where schedule_id=? and submission_token is not null";
    private static final String GET_NOTIFICATION_TOKENS_BY_SCHEDULEID_V1 = "select json_arrayagg(submission_token) token from trigger_notification_%s where schedule_id=? and submission_token is not null";
    private static final String GET_NOTIFICATION_COUNT_BY_SCHEDULEID = "select count(id) from trigger_notification where schedule_id=?";
    private static final String GET_NOTIFICATION_COUNT_BY_SCHEDULEID_V1 = "select count(id) from trigger_notification_%s where schedule_id=?";
    private static final String GET_ACTIVE_NOTIFICATION_BY_SCHEDULEID = "select id, notification_uuid, start_time, channel, message, status, contacts, add_feedback, schedule_id from trigger_notification where schedule_id=? and sent_status is null";

    private static final String GET_NOTIFICATION_TOKENS_BY_SCHEDULEID_By_DAY = "select submission_token from trigger_notification where start_time > ? and start_time < ? and schedule_id=?";
    private static final String UPDATE_TRIGGER_NOTIFICATION_SENT_STATUS = "update trigger_notification set status = 1, sent_status = ?, modified_time = ? where id = ?";
    private static final String UPDATE_BUSINESS_TRIGGER_NOTIFICATION_SENT_STATUS = "update trigger_notification_%s set status = 1, sent_status = ?, modified_time = ? where id = ?";
    private static final String UPDATE_TRIGGER_NOTIFICATION_TRIGGER_STATUS = "update trigger_notification set trigger_status = :triggerStatus  where schedule_id in (:scheduleIds) and sent_status is null and start_time >= current_timestamp";

    /*** trigger_schedule table queries *****/
    private static final String INSERT_TRIGGER_SCHEDULE = "insert into trigger_schedule (start_date, end_date, start_time, end_time, frequency, business_id, survey_id, timezone, trigger_id, schedule_uuid, last_processed_date, last_processed_time, channel, message, subject, contacts, add_feedback, trigger_config, status, host, participant_group_id, header_data, webhook_config, integrations_config, reply_respondent) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
    private static final String UPDATE_TRIGGER_SCHEDULE = "update trigger_schedule set start_date=?, end_date=?, start_time=?, end_time=?, frequency=?, timezone=?, channel=?, message=?, subject=?, add_feedback=?, contacts=?, trigger_config=?, status=?, participant_group_id=?, header_data=?, webhook_config=?, integrations_config = ?, reply_respondent=? where schedule_uuid=?";
    private static final String DELETE_TRIGGER_SCHEDULE = "delete from trigger_schedule where schedule_uuid = ?";
    private static final String DELETE_TRIGGER_SCHEDULES_BY_TRIGGER_ID = "delete from trigger_schedule where trigger_id = (select id from trigger_%s where trigger_uuid=?) and business_id=?";

    private static final String GET_TRIGGER_SCHEDULE_BY_UNIQUE_ID = "select status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, deactivated_contacts, participant_group_id, header_data, webhook_config, reply_respondent, integrations_config from trigger_schedule where schedule_uuid=?";

    //private static final String GET_TRIGGER_SCHEDULE_BY_TRIGGER_ID = "select id,status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, deactivated_contacts, participant_group_id, header_data, webhook_config, reply_respondent, integrations_config from trigger_schedule where trigger_id in (select id from trigger_%s where trigger_uuid=?) and business_id=?";

    private static final String GET_TRIGGER_SCHEDULE_JOIN_SURVEY_TABLE_BY_TRIGGER_ID = "SELECT ts.id, s.to_date, ts.status, ts.start_date, ts.end_date, ts.start_time, ts.end_time, ts.frequency, ts.business_id, ts.survey_id, ts.timezone, ts.schedule_uuid, ts.channel, ts.message, ts.subject, ts.contacts, ts.add_feedback, ts.trigger_config, ts.deactivated_contacts, ts.participant_group_id, ts.header_data, ts.webhook_config, ts.reply_respondent, ts.integrations_config FROM trigger_schedule AS ts\n" +
            " JOIN  trigger_%s AS t\n" +
            " ON ts.trigger_id = t.id \n" +
            " JOIN  surveys_%s AS s \n" +
            " ON t.survey_id = s.id  " +
            " WHERE t.trigger_uuid = ? AND ts.business_id = ?;";
    private static final String GET_TRIGGER_SCHEDULE_BY_TRIGGER_ID = "SELECT ts.id, ts.status, ts.start_date, ts.end_date, ts.start_time, ts.end_time, ts.frequency, ts.business_id, ts.survey_id, ts.timezone, ts.schedule_uuid, ts.channel, ts.message, ts.subject, ts.contacts, ts.add_feedback, ts.trigger_config, ts.deactivated_contacts, ts.participant_group_id, ts.header_data, ts.webhook_config, ts.reply_respondent, ts.integrations_config FROM trigger_schedule AS ts\n" +
            " JOIN  trigger_%s AS t\n" +
            " ON ts.trigger_id = t.id \n" +
            " WHERE t.trigger_uuid = ? AND ts.business_id = ?;";
    private static final String GET_TRIGGER_SCHEDULE_IDS_BY_TRIGGER_ID = "select json_arrayagg(id) as schedule_id from trigger_schedule where trigger_id in (select id from trigger_%s where trigger_uuid=?) and business_id=?";
    private static final String GET_TRIGGER_SCHEDULES_BY_TRIGGER_ID_BY_DEACTIVATED_USER_EMAIL = "select id,status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, deactivated_contacts, participant_group_id, header_data, webhook_config, reply_respondent from trigger_schedule where trigger_id in (select id from trigger_%s where trigger_uuid=?) and business_id=? and json_contains(deactivated_contacts, ?)";
    private static final String GET_TRIGGER_SCHEDULE_BY_ID = "select status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, timezone, schedule_uuid , channel, message, subject, contacts, add_feedback,trigger_config, participant_group_id, header_data, webhook_config, integrations_config, reply_respondent from trigger_schedule where id=?";


    private static final String UPDATE_TRIGGER_EXEC_TIME = "update trigger_schedule set exec_start_time = ?, exec_end_time = ?, sent_status=?  where id = ?";
    private static final String UPDATE_TRIGGER_EXEC_TIME_BY_UUID = "update trigger_schedule set exec_start_time = ?, exec_end_time = ?  where schedule_uuid = ?";
    private static final String UPDATE_TRIGGER_SCHEDULE_STATUS = "update trigger_schedule set status = ? where trigger_id = ? and business_id=? and survey_id=?";
    private static final String UPDATE_TRIGGER_VALUES = "update trigger_schedule set status = ?, start_date = ?, start_time = ?, end_date = ?, end_time = ?, frequency = ?, exec_start_time = ?, exec_end_time = ?  where trigger_id = ? and business_id=? and survey_id=?";
    private static final String UPDATE_TRIGGER_SCHEDULE_STATUS_BY_SHEDULEID = "update trigger_schedule set status = ? where id=?";
    private static final String UPDATE_TRIGGER_SCHEDULE_SENT_STATUS_BY_SHEDULEID = "update trigger_schedule set sent_status = ? where id=?";
    private static final String UPDATE_TRIGGER_SCHEDULE_SENT_STATUS = "update trigger_schedule set sent_status = ? where trigger_id = ? and business_id=? and survey_id=?";

    private static final String IS_SCHEDULE_EXISTS = "select count(schedule_uuid) from trigger_schedule where trigger_id=? and survey_id=? and business_id=?";
    private static final String GET_ACTIVE_TRIGGERS = "select id as schedule_id, status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, trigger_id, timezone, schedule_uuid, channel, message, subject, contacts, add_feedback,trigger_config, last_processed_date, last_processed_time, exec_start_time, exec_end_time, participant_group_id, header_data  from trigger_schedule where current_date >= start_date and current_date <= end_date and status='active'  order by start_date desc limit 1000";

    //TEST
    //private static final String GET_ACTIVE_TRIGGER_EXECUTION ="select id as schedule_id, status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, trigger_id, timezone, schedule_uuid, channel, message, subject, contacts, add_feedback,trigger_config, last_processed_date, last_processed_time  from trigger_schedule where id = 278";
    private static final String GET_ACTIVE_TRIGGER_EXECUTION = "select id as schedule_id, status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, trigger_id, timezone, schedule_uuid, channel, message, subject, contacts, add_feedback, trigger_config, last_processed_date, last_processed_time,  participant_group_id, header_data, webhook_config, integrations_config, reply_respondent, created_time from trigger_schedule where current_timestamp >= exec_start_time and current_timestamp <= exec_end_time and status = 'active' and sent_status is null  order by start_date desc limit 1000";

    private static final String GET_ACTIVE_TRIGGER_SCHEDULED = "select id as schedule_id, status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, trigger_id, timezone, schedule_uuid, channel, message, subject, contacts, add_feedback,trigger_config, last_processed_date, last_processed_time  from trigger_schedule where current_date >= start_date and current_date <= end_date and status = 'active' and (last_processed_date <= current_date) and last_processed_time <= STR_TO_DATE(end_time,'%h:%i:%s')  and current_time >= STR_TO_DATE(start_time,'%h:%i:%s') order by start_date desc limit 1000";
    //    private static final String GET_ACTIVE_TRIGGER_SCHEDULED ="select id as schedule_id, status, start_date, end_date, start_time, end_time, frequency, business_id, survey_id, trigger_id, timezone, schedule_uuid, channel, message, subject, contacts, add_feedback,trigger_config, last_processed_date, last_processed_time  from trigger_schedule where current_date >= start_date and current_date <= end_date and status = 'active' and (last_processed_date <= current_date) and  (current_time >= start_time and current_time <= end_time) order by start_date desc limit 1000";
    private static final String UPDATE_LAST_PROCESSED_DATETIME = "update trigger_schedule set last_processed_date = ?, last_processed_time=? where id = ?";
    private static final String GET_TRIGGER_SCHEDULE_ID = "select id, business_id from trigger_schedule where schedule_uuid=?";
    private static final String GET_ALL_TOKENS_BY_SURVEY_ID = "select submission_token, created_time from %s order by created_time desc";
    private static final String GET_SURVEY_BY_ID = "select id, from_date, to_date, state, active,anonymous,survey_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys,multi_questions, survey_property, survey_uuid,questions_group, created_time, modified_time, created_by, modified_by from `%s` where id = ?";
    private static final String GET_SURVEY_BY_UUID = "select id, from_date, to_date, state, active,anonymous,survey_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys,multi_questions, survey_property, survey_uuid, created_time, modified_time, created_by, modified_by from `%s` where survey_uuid = ?";
    private static final String GET_EVENT_BY_TOKEN = "select json_extract(event_metadata, '$.metaData') metaData, data, event_data, submission_token, created_time,id from %s where submission_token=?";
    private static final String GET_MULTI_CHOICE_OPTIONS_BY_QUESTION_ID = "select option_ids,other_option_data,option_data,other_option_ids from %s where question_id = ?";
    private static final String GET_MULTI_CHOICE_OPTIONS_BY_QUESTION_UUID = "select option_ids,option_data,other_option_data from %s where question_id  in (select question_id from %s where question_uuid=?)";
    private static final String GET_QUESTION_ID_BY_UUID = "select question_id from %s where question_uuid = ?";
    private static final String GET_MULTI_RANKING_OPTIONS_BY_QUESTION_ID = "select option_ids,option_data from %s where question_id = ?";
    /*private static final String GET_MULTI_RANKING_OPTIONS_BY_QUESTION_UUID = "select d.option_ids, d.option_data  from %s d where question_id in \n" +
            " (select question_id from %s where question_uuid  = ?)";*/
    private static final String GET_MULTI_RANKING_OPTIONS_BY_QUESTION_UUID = "select d.option_ids, d.option_data  from %s d \n" +
            " join %s q on d.question_id = q.question_id where q.question_uuid  = ?";
    /***************** participants ******************/
    private static final String IS_LIST_NAME_EXIST = "select id from tag_%s where upper(tag_name) = ?";
    private static final String CREATE_TAG_LIST = "insert into tag_%s(tag_name, participant_groups, created_by, created_time, modified_by, modified_time, tag_uuid, active) values (?,?,?,?,?,?,?,?)";
    private static final String UPDATE_TAG_LIST = "update tag_%s set tag_name = ? where json_length(participant_groups)-1 > -1 and json_extract(participant_groups, concat('$[', json_length(participant_groups)-1,']')) = ?";
    private static final String CREATE_PARTICIPANTS = "insert into participant_%s(data, header, meta_data, phi_data, question_metadata, participant_uuid, created_by, created_time) values (?,?,?,?,?,?,?,?)";
    private static final String GET_PARTICIPANT_METADATA = "select distinct(meta_data) as meta_data from %s";
    private static final String GET_PARTICIPANT_DATA = "select data from %s";
    private static final String CREATE_PARTICIPANT_GROUP = "insert into participant_group_%s(file_name, active, participant_group_uuid, created_by, created_time, primary_key_column) values (?,?,?,?,?,?)";
    private static final String GET_SURVEY_BY_UUID_TEMPLATE = "select id, from_date, to_date, state, active,anonymous,survey_meta_data, question_data, question_ids, metrics_map, languages, rules, multi_surveys,multi_questions, survey_property, survey_uuid, created_time, modified_time, created_by, modified_by from `%s` where survey_uuid = ?";
    private static final String GET_SURVEY_DATA_BY_USERTOKEN = "select survey_id, last_action, participant_id, tag_id, participant_group_id from %s where dynamic_token_id = ?";
    private static final String GET_QUESTION_BY_UUID_TEMPLATE = "select * from `%s` where question_uuid = ?";
    private static final String GET_KEY_METRIC_BY_ID_TEMPLATE = "select * from %s where key_metric_id = ?";
    private static final String GET_USER_BY_UUID = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_db, u.user_email, u.password, u.phone, u.trial_flag, u.complete_social_signup, u.confirmation_status, u.created_by, u.created_time, u.modified_time, u.modified_by, u.password_update_count, u.user_uuid, u.eula_status, u.push_notification, u.thoughtful, u.user_role, a.account_id, a.account_type, a.signup_type_id, a.account_user_id, a.account_business_id, a.account_role_id, a.account_country_id, a.account_emp_range from users u left join account a on u. user_id = a.account_user_id where u.user_uuid = ?";
    private static final String GET_KING_USER_BY_BUSINESS_ID = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_db, u.user_email, u.user_uuid, u.password, u.phone, u.trial_flag, u.complete_social_signup, u.confirmation_status, u.password_update_count, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.eula_status, u.push_notification, u.thoughtful, u.title, u.cs_start_date, u.cs_end_date, u.user_role, u.alias_email, u.last_login_time, a.account_id, a.account_type, a.signup_type_id, a.account_user_id, a.account_business_id, a.account_role_id, a.account_country_id, a.account_emp_range from users  as u  left join account as a on u.user_id = a.account_user_id where user_status = '1' and user_role = '1' and account_business_id = ?";
    private static final String GET_BUSINESS_CONFIGRUATIONS_BY_BUSINESSID = "select hippa from business_configurations where business_id = ?";

    private static final String GET_SURVEY_SCHEDULE_BY_SURVEYID = "select type from survey_schedule where business_id = ? and survey_id = ? order by created_time desc limit 1";

    private static final String GET_EVENT_SOURCE_BY_TOKEN = "SELECT json_unquote(JSON_EXTRACT(event_metadata, '$.metaData.source')) as source FROM %s WHERE submission_token = ?";
    /***************** template triggers  ******************/
    private static final String INSERT_TEMPLATE_PROGRAM_MAP = "insert into template_program_map (template_id, business_id, program_id, template_trigger_id, template_schedule_id, channel, trigger_id, schedule_id, trigger_metadata) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    /** dtworks failed triggers**/
    private static final String INSERT_FAILED_TRIGGER_NOTIFICATION = "insert into failed_trigger_notifications (start_time, channel, message, status, contacts, add_feedback, schedule_id, notification_uuid , subject, condition_query, submission_token, trigger_status, action_type, business_id, reason) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?) ";
    private static final String UPDATE_FAILED_TRIGGER_NOTIFICATION_SENT_STATUS = "update failed_trigger_notifications set status = ?, sent_status = ?, modified_time = ? where notification_uuid = ?";
    private static final String GET_FAILED_TRIGGER_NOTIFICATION = "select a.notification_uuid, a.id, b.id as schedule_id, a.start_time, a.channel, a.message, a.subject, a.status, a.contacts, a.add_feedback, a.schedule_id, a.action_type, condition_query, a.business_id, survey_id, trigger_id, host, a.submission_token, reply_respondent, timezone " +
            " from failed_trigger_notifications a, trigger_schedule b where a.schedule_id = b.id and a.status = 0 and a.sent_status is null and a.business_id = :businessId and a.schedule_id in (:scheduleIds)";

    /**
     * @param filterQuery
     * @return
     */
    public String removeQueriesWithAll(String filterQuery) {

        StringBuilder stringBuilder = new StringBuilder();
        List queries = new ArrayList();
        List operators = new ArrayList();
        String[] subQuerries = filterQuery.split("&&|\\|\\|");
        for (int i = 0; i < subQuerries.length; i++) {

            queries.add(subQuerries[i]);
            filterQuery = filterQuery.replace(subQuerries[i], "");
        }

        for (int k = 0; k < filterQuery.length(); k = k + 2) {
            String eachOperator = filterQuery.substring(k, k + 2);
            operators.add(eachOperator);
        }

        int querySize = subQuerries.length;
        for (int i = 0; i < querySize; i++) {

            String tempString = subQuerries[i];
            logger.info("each subquery {} ", tempString);
            String condition = "";
            String[] conditionArr = subQuerries[i].split("\\.");
            condition = conditionArr.length > 2 ? conditionArr[3].toUpperCase() : "";
            boolean addQuery = (condition.contains("DT_ALL") || (condition.contains("ALL"))) ? false : true;
            logger.info("add query {}", addQuery);


            String eachOperator = "";
            if (i < operators.size()) {
                eachOperator = operators.get(i).toString();
            }

            if (addQuery) {
                logger.info("adding each query");
                stringBuilder.append(tempString).append(eachOperator);
            } else {
                logger.info("not adding each query");
            }

        }

        String modifiedQuery = stringBuilder.toString();
        logger.info("modified query {}", modifiedQuery);
        return modifiedQuery;
    }


    public String adjustFilterQuery(String filterQuery) {

        StringBuilder stringBuilder = new StringBuilder();
        List queries = new ArrayList();
        List operators = new ArrayList();
        String[] subQuerries = filterQuery.split("&&|\\|\\|");
        for (int i = 0; i < subQuerries.length; i++) {

            queries.add(subQuerries[i]);
            filterQuery = filterQuery.replace(subQuerries[i], "");
        }

        for (int k = 0; k < filterQuery.length(); k = k + 2) {
            String eachOperator = filterQuery.substring(k, k + 2);
            operators.add(eachOperator);
        }

        int querySize = subQuerries.length;
        for (int i = 0; i < querySize; i++) {

            String tempString = subQuerries[i];
            logger.info("each subquery {}", tempString);

            String eachOperator = "";
            if (i < operators.size()) {
                eachOperator = operators.get(i).toString();
            }

            /**
             * the DT_ALL query is removed to prevent passing DT_ALL from parent filter to the read only/ read write users
             * if individual DT_ALL conditions are used, this will remove them.
             * UI Should prevent sending DT_ALL for the triggers if user role is not admin
             * check DTV-2863. May need further revisit
             */
            //String condition = "";
            //condition = (subQuerries[i].split("\\.")[2]);
            //boolean appendQuery = (condition.contains("DT_All")) ? false : true;


            if (tempString.substring(0, 8).equals("question")) {
                //question case
                String condition = (subQuerries[i].split("\\.")[3]).toUpperCase();
                if ((!condition.contains("DT_ALL")) || (!condition.contains("ALL"))) {
                    logger.info("appending subquery");
                    stringBuilder.append(tempString).append(eachOperator);
                }


            } else {
                //meta data case
                String condition = (subQuerries[i].split("\\.")[2]).toUpperCase();
                if ((!condition.contains("DT_ALL")) || (!condition.contains("ALL"))) {
                    logger.info("appending subquery");
                    if (tempString.substring(0, 10).equals("dtmetadata")) {
                        stringBuilder.append("dtmetadata.").append(tempString).append(eachOperator);
                    } else if (tempString.substring(0, 7).equals("dtkiosk")) {
                        stringBuilder.append("dtkiosk.").append(tempString).append(eachOperator);
                    } else if (tempString.substring(0, 6).equals("dtlink")) {
                        stringBuilder.append("dtlink.").append(tempString).append(eachOperator);
                    } else {
                        stringBuilder.append("dtqr.").append(tempString).append(eachOperator);
                    }
                }


            }
        }

        String modifiedQuery = stringBuilder.toString();
        return modifiedQuery;
    }

    /*************** query statements - ends ************************/

    /**
     * Method to create trigger condition details using business uuid
     *
     * @param trigger
     * @return
     */
    public String createTrigger(final Trigger trigger, int surveyId,int userId, int roleId) {
        logger.debug("begin creating the trigger details");
        String result = "";
        try {
            final String query = String.format(INSERT_TRIGGER, trigger.getBusinessUniqueId().replaceAll("-", "_"));
            String triggerFilterUUID = "";
            //DTV-2863
            if (roleId != 1 && roleId != 4) {
                Map userSurveyFilterMap = this.getMappedSurveyFiltersByUserId(userId, surveyId, trigger.getBusinessUniqueId());
                if (userSurveyFilterMap.size() > 0) {
                    String filterQuery = Utils.isNotNull(userSurveyFilterMap.get("filter_query")) ? userSurveyFilterMap.get("filter_query").toString() : "";

                    /**
                     * survey filter query meta data does not have the prefix but the question has
                     * metadata : df33ef19-56f4-4701-98af-fe83a8b1159d.0a8c1432-ffd8-4d85-9359-07727ec1917f.Name=DT_All
                     * question : question.0ea5293f-9b03-41db-b5e1-01e87fcba564.c6868fa4-bd10-4c01-8335-7326d57cca5c.response>9
                     *
                     */
                    String modifiedQuery = adjustFilterQuery(filterQuery);
                    trigger.setTriggerCondition(modifiedQuery);
                }
            }
            final String uuid = Utils.generateUUID();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    logger.info("trigger Bean {}", trigger.toString());
                    int status = Utils.isNotNull(trigger.getStatus()) && trigger.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
                    statement.setString(1, (trigger.getTriggerName()));
                    statement.setString(2, trigger.getTriggerCondition());
                    statement.setInt(3, status);
                    statement.setString(4, trigger.getSurveyName());
                    statement.setString(5, String.valueOf(surveyId));
                    statement.setInt(6, userId);
                    statement.setString(7, uuid);
                    statement.setString(8, Utils.isNotNull(trigger.getTriggerStartDate()) ? Utils.getUTCDateString(trigger.getTriggerStartDate(), trigger.getTimezone()) : null);
                    statement.setString(9, Utils.isNotNull(trigger.getTriggerEndDate()) ? Utils.getUTCDateString(trigger.getTriggerEndDate(), trigger.getTimezone()) : null);
                    statement.setString(10, Utils.isNotNull(trigger.getTriggerStartTime()) ? Utils.getUTCTimeAsString(trigger.getTriggerStartTime(), trigger.getTimezone()) : null);
                    statement.setString(11, Utils.isNotNull(trigger.getTriggerEndDate()) ? Utils.getUTCTimeAsString(trigger.getTriggerEndTime(), trigger.getTimezone()) : null);
                    statement.setString(12, Utils.isNotNull(trigger.getFrequency()) ? trigger.getFrequency() : "");
                    return statement;
                }
            });
            result = uuid;
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.debug("end creating trigger details");
        return result;
    }

    /**
     * Method to delete trigger notification details using business uuid by triggerId
     *
     * @param businessUUID
     * @param triggerId
     * @return
     */
    public int deletTrigger(String businessUUID, String triggerId, int businessId) {
        logger.debug("begin deleting the trigger details");
        int result = 0;
        try {
            Map triggerMap = getTriggerById(businessUUID, triggerId);
            //DTV-4010, DTV-4013
            //delete all the schedules when trigger is deleted
            this.deleteTriggerSchedulesByTriggerId(businessUUID, triggerId, businessId);

            final String query = String.format(DELETE_TRIGGER, (businessUUID.replaceAll("-", "_")));
            result = jdbcTemplate.update(query, new Object[]{triggerId});

            String triggerFilterUUID = Utils.isNotNull(triggerMap.get(Constants.TRIGGER_FILTER_UUID)) ? triggerMap.get(Constants.TRIGGER_FILTER_UUID).toString() : Constants.EMPTY;
            int triggerStatus = Utils.isNotNull(triggerMap.get(Constants.STATUS)) ? (Integer.parseInt(triggerMap.get(Constants.STATUS).toString())) : 0;
            if (result > 0 && Utils.notEmptyString(triggerFilterUUID)) {
                //trigger deleted successfully, removing the dynamic filter for the trigger
                int res = this.deleteSurveyFilter(triggerFilterUUID, businessUUID);
            }

            //updating trigger usage in business_usage table after deleting active trigger
            if (result > 0 && triggerStatus == 1) {
                /**updating usage count to business usage table
                 * deleting active trigger, reduce the usage count by 1
                 */
                result = 1;
            }
            logger.debug("end deleting the trigger details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to get all triggers notification details using business uuid by sorting, state and userId
     *
     * @param businessUUID
     * @param sortBy            - created_time / modified_time
     * @param viewBy            - all / surveyName
     * @param state             - active / inactive / all
     * @param searchTriggerName - trigger name that needs to be searched
     * @param isExactMatch      - search match should be exact or like
     * @param userId
     * @return
     */
    public List getTriggers(String businessUUID, String sortBy, String viewBy, String state, String searchTriggerName, boolean isExactMatch, int userId, int pageNo) {
        logger.info("begin retrieving the trigger details");
        boolean search = false;
        boolean view = false;
        List resultList = new ArrayList();
        try {
            String searchStr = (isExactMatch) ? searchTriggerName : "%" + searchTriggerName + "%";
            String formattedBusiness = (businessUUID.replaceAll("-", "_"));
            String query = String.format(GET_TRIGGERS, formattedBusiness, formattedBusiness);
            // add state condition
            if (Utils.isNotNull(state) && !state.equals(Constants.ALL)) {
                /*  state => all / active / inactive
                    state all : no need to add state in where condition, it will fetch both active and inactive
                    state active : status = 1, state inactive : status = 0  */
                query += state.equals(Constants.STATUS_ACTIVE) ? " and status = 1 " : " and status = 0";
            }
            // add viewBy condition
            if (Utils.isNotNull(viewBy) && !viewBy.equals(Constants.ALL)) {
                /*  viewBy => all or {surveyName}
                    viewBy all : no need to add survey_name in where condition, it will fetch all the surveys
                    veiwBy surveyName : eg: survey_name = 'survey one'  */
//                query += " and survey_name = '" + viewBy + "'";
                query += " and survey_name = ?";
                view = true;
            }
            // add search condition if available
            if (Utils.isNotNull(searchTriggerName)) {
                 /*  search surveyName with isExactMatch true, returns exact matching result
                    search surveyName with isExactMatch false, returns all match survey name */
//                query += isExactMatch ? " and trigger_name = '" + searchTriggerName + "'" : "and trigger_name like " + "'%" + searchTriggerName + "%'";
                query += isExactMatch ? " and trigger_name = ?" : " and trigger_name like ?";
                search = true;
            }
            // pagination
            if (pageNo > 0) {
                upperLimit = (pageNo - 1) * length;
                query += (Utils.isNotNull(sortBy) && sortBy.equals(Constants.MODIFIED_TIME)) ? " order by a.modified_time desc limit ? , ? " :
                        (Utils.isNotNull(sortBy) && sortBy.equals(Constants.TRIGGER_NAME)) ? " order by a.trigger_name asc limit ? , ? " : " order by a.created_time desc limit ? , ? ";
            } else {
                upperLimit = 0;
                query += (Utils.isNotNull(sortBy) && sortBy.equals(Constants.MODIFIED_TIME)) ? " order by a.modified_time desc " :
                        (Utils.isNotNull(sortBy) && sortBy.equals(Constants.TRIGGER_NAME)) ? " order by a.trigger_name asc " : " order by a.created_time desc";
            }

            // sort by created time or modified time / trigger name based on given request, default is created time
            //query += (Utils.isNotNull(sortBy) && sortBy.equals(Constants.MODIFIED_TIME)) ? " order by a.modified_time desc limit ? , ? " :
            //      (Utils.isNotNull(sortBy) && sortBy.equals(Constants.TRIGGER_NAME)) ? " order by a.trigger_name asc limit ? , ? " : " order by a.created_time desc limit ? , ? ";
            if (view) {
                if (pageNo > 0) {
                    resultList = (search) ? jdbcTemplate.queryForList(query, new Object[]{userId, viewBy, searchStr, upperLimit, length}) : jdbcTemplate.queryForList(query, new Object[]{userId, viewBy, upperLimit, length});
                    ;
                } else {
                    resultList = (search) ? jdbcTemplate.queryForList(query, new Object[]{userId, viewBy, searchStr}) : jdbcTemplate.queryForList(query, new Object[]{userId, viewBy});
                    ;
                }

            } else {
                if (pageNo > 0) {
                    resultList = (search) ? jdbcTemplate.queryForList(query, new Object[]{userId, searchStr, upperLimit, length}) : jdbcTemplate.queryForList(query, new Object[]{userId, upperLimit, length});
                } else {
                    resultList = (search) ? jdbcTemplate.queryForList(query, new Object[]{userId, searchStr}) : jdbcTemplate.queryForList(query, new Object[]{userId});
                }

            }
            //resultList = jdbcTemplate.queryForList(query, new Object[]{userId, upperLimit, length});

            logger.debug("end retrieving the trigger details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * Method to get total count of triggers notification details using business uuid by sorting, state and userId
     *
     * @param businessUUID
     * @param sortBy            - created_time / modified_time
     * @param viewBy            - all / surveyName
     * @param state             - active / inactive / all
     * @param searchTriggerName - trigger name that needs to be searched
     * @param isExactMatch      - search match should be exact or like
     * @param userId
     * @return
     */
    public int getTotalTriggersCountByCondition(String businessUUID, String sortBy, String viewBy, String state, String searchTriggerName, boolean isExactMatch, int userId) {
        logger.debug("begin retrieving the trigger details");
        int recordCount = 0;
        boolean view = false;
        boolean search = false;
        String searchStr = (isExactMatch) ? searchTriggerName : " %" + searchTriggerName + "%";
        try {
            String query = String.format(GET_TRIGGERS_COUNT, businessUUID.replaceAll("-", "_"), businessUUID.replaceAll("-", "_"));
            // add state condition
            if (Utils.isNotNull(state) && !state.equals(Constants.ALL)) {
                /**  state => all / active / inactive
                    state all : no need to add state in where condition, it will fetch both active and inactive
                    state active : status = 1, state inactive : status = 0  */
                query += state.equals(Constants.STATUS_ACTIVE) ? " and status = 1 " : " and status = 0";
            }
            // add viewBy condition
            if (Utils.isNotNull(viewBy) && !viewBy.equals(Constants.ALL)) {
                /**  viewBy => all or {surveyName}
                    viewBy all : no need to add survey_name in where condition, it will fetch all the surveys
                    veiwBy surveyName : eg: survey_name = 'survey one'  */
//                query += " and survey_name = '" + viewBy + "'";
                query += " and survey_name = ?";
                view = true;
            }
            // add search condition if available
            if (Utils.isNotNull(searchTriggerName)) {
                 /**  search surveyName with isExactMatch true, returns exact matching result
                    search surveyName with isExactMatch false, returns all mactch survey name */
//                query += isExactMatch ? " and trigger_name = '" + searchTriggerName + "'" : "and trigger_name like " + "'%" + searchTriggerName + "%'";
                query += isExactMatch ? " and trigger_name = ?" : " and trigger_name like ?";
                search = true;
            }

            // sort by cratedtime or modifiedtime based on given request, default is createdtime
//            query += (Utils.isNotNull(sortBy) && sortBy.equals(Constants.MODIFIED_TIME)) ? " order by modified_time desc " : " order by created_time desc ";

            if (view) {
                recordCount = (search) ? jdbcTemplate.queryForObject(query, new Object[]{userId, viewBy, searchStr}, Integer.class) : jdbcTemplate.queryForObject(query, new Object[]{userId, viewBy}, Integer.class);
            } else {
                recordCount = (search) ? jdbcTemplate.queryForObject(query, new Object[]{userId, searchStr}, Integer.class) : jdbcTemplate.queryForObject(query, new Object[]{userId}, Integer.class);
            }
            recordCount = jdbcTemplate.queryForObject(query, new Object[]{userId}, Integer.class);

            logger.debug("end retrieving the trigger details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return recordCount;
    }

    /**
     * method to get string status values
     * status 0 / 1 / 2 :: inactive / active / completed
     *
     * @param state
     * @return
     */
    private String getStatusString(int state) {
        String stateStr = Constants.STATUS_INACTIVE;
        switch (state) {
            case 0:
                stateStr = Constants.STATUS_ACTIVE;
            case 1:
                stateStr = Constants.STATUS_INACTIVE;
            case 2:
                stateStr = Constants.STATUS_COMPLETED;
        }
        return stateStr;
    }

    /**
     * Method to get all triggers notification details using business uuid by triggerId
     *
     * @param businessUUID
     * @param triggerId
     * @return
     */
    public Map getTriggerById(String businessUUID, String triggerId) {
        logger.info("begin retrieving the trigger details by triggerID");
        Map triggerMap = new HashMap();
        try {
            String businessReplaced = (businessUUID.replaceAll("-", "_"));
            String query = String.format(GET_TRIGGER_BY_UUID, businessReplaced, businessReplaced);
            triggerMap = jdbcTemplate.queryForMap(query, new Object[]{triggerId});
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
//            e.printStackTrace();
        }
        logger.info("end retrieving the trigger details by triggerID");
        return triggerMap;
    }

    /**
     * method to update status of the trigger by using triggerId
     *
     * @param businessUUID
     * @param triggerId
     * @param state        active / inactive
     * @return
     */
    public int updateTriggerStatusByTriggerId(String businessUUID, String triggerId, String state) {
        logger.debug("begin updating the trigger status");
        int result = 0;
        try {
            final String query = String.format(UPDATE_TRIGGER_STATUS, (businessUUID.replaceAll("-", "_")));
            int status = Utils.isNotNull(state) && state.equals(Constants.STATUS_ACTIVE) ? 1 : 0;
            result = jdbcTemplate.update(query, new Object[]{status, (new Timestamp(System.currentTimeMillis())), triggerId});
            logger.debug("end updating the trigger status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to check if table exits or not
     *
     * @param uuid
     * @return
     */
    public boolean isTriggerTableExists(String uuid) {
        List result = new ArrayList();
        try {
            String uuidHyphenReplaced = uuid.replace("-", "_");
            //String query = String.format("delete from %s where survey_id = ?", "token_" + businessUUID.replaceAll("-", "_"));
//            String qury = "SHOW TABLES like 'trigger_" + uuidHyphenReplaced + "'";
            //String qury = String.format("SHOW TABLES like '%s'", "trigger_" + uuidHyphenReplaced);
            String qury = "SELECT * FROM information_schema.tables WHERE table_schema = 'DT' AND table_name = ' trigger_"+ uuidHyphenReplaced + "'";
            logger.debug("check table exists " + qury);
            result = jdbcTemplate.queryForList(qury);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("check table " + result);
        if (result.size() > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method to create trigger table if not exists
     *
     * @param businessUniqueId
     * @return
     */
    public int createTriggerTable(String businessUniqueId) {
        int tableCreated = 1;
        logger.debug("begin creating trigger table ");
        String tableName = String.format("trigger_%s", businessUniqueId.replace("-", "_"));
        try {
            String createSql = "CREATE TABLE IF NOT EXISTS " + tableName + " ( " +
                    "  `id` int(11) NOT NULL AUTO_INCREMENT," +
                    "  `trigger_uuid` varchar(100) NULL DEFAULT NULL," +
                    "  `trigger_name` varchar(300) NULL DEFAULT NULL," +
                    "  `trigger_condition` text," +
                    "  `readable_query` text," +
                    "  `status` int(1) DEFAULT '0'," +
                    "  `survey_name` varchar(300) DEFAULT NULL," +
                    "  `survey_id` varchar(100) DEFAULT NULL," +
                    "  `created_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP," +
                    "  `created_by` int(11) DEFAULT NULL," +
                    "  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP," +
                    "  `modified_by` int(11) DEFAULT NULL," +
                    "  `start_date` varchar(12) DEFAULT NULL," +
                    "  `end_date` varchar(12) DEFAULT NULL," +
                    "  `start_time` varchar(12) DEFAULT NULL," +
                    "  `end_time` varchar(12) DEFAULT NULL," +
                    "  `frequency` varchar(100) DEFAULT NULL," +
                    "  `trigger_filter_uuid` varchar(100) DEFAULT NULL," +
                    "  `deactivated_conditions` JSON NULL DEFAULT NULL," +
                    "  PRIMARY KEY (`id`)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci";
            tableCreated = jdbcTemplate.update(createSql);
            logger.debug("Create table result {}", tableCreated);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end creating trigger table");
        return tableCreated;
    }

    /**
     * check trigger name exists or not by passing businessId and trigger Name
     *
     * @param triggerName
     * @param businessUniqueId
     * @return true: trigger name exists / false: trigger name not exists
     */
    public boolean isTriggerNameExists(String triggerName, String businessUniqueId, String userUniqueId, int surveyId) {
        int userId = this.getUserIdByUUID(userUniqueId);
        logger.debug("begin trigger name check");
        try {
            String query = String.format(TRIGGER_NAME_CHECK, businessUniqueId.replaceAll("-", "_"));
            Integer recordCount = jdbcTemplate.queryForObject(query, new Object[]{triggerName.toUpperCase(), userId, surveyId}, Integer.class);
            if (recordCount > 0) {
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end trigger name check");
        return false;
    }

    /**
     * get user Id by using userUniqueID
     *
     * @param userUniqueId
     * @return
     */
    public Integer getUserIdByUUID(String userUniqueId) {
        logger.debug("begin get user id");
        Integer id = 0;
        try {
            id = jdbcTemplate.queryForObject(GET_USER_ID, new Object[]{userUniqueId}, Integer.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get user id");
        return id;
    }

    /**
     * get surveyId by using surveyUniqueID
     *
     * @param surveyUniqueId
     * @param businessUniqueId
     * @return
     */
    public Integer getSurveyIdByUUID(String surveyUniqueId, String businessUniqueId) {
        logger.debug("begin get survey id");
        Integer id = 0;
        try {
            String query = String.format(GET_SURVEY_ID, businessUniqueId.replaceAll("-", "_"));
            id = jdbcTemplate.queryForObject(query, new Object[]{surveyUniqueId}, Integer.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get survey id");
        return id;
    }

    /**
     * get businessId by using businessUniqueID
     *
     * @param businessUniqueId
     * @return
     */
    public Integer getBusinessIdByUUID(String businessUniqueId) {
        logger.debug("begin get business id");
        Integer id = 0;
        try {
            id = jdbcTemplate.queryForObject(GET_BUSINEES_ID, new Object[]{businessUniqueId}, Integer.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get business id");
        return id;
    }

    /**
     * Method to insert trigger notification details using business uuid
     *
     * @param triggerNotification
     * @return
     */
    public String createTriggerNotification(final TriggerNotification triggerNotification) {
        logger.info("begin creating the trigger details");
        String result = "";
        final String uuid = Utils.generateUUID();
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_TRIGGER_NOTIFICATION, Statement.RETURN_GENERATED_KEYS);
                    logger.info("trigger notification Bean {}", triggerNotification.toString());
                    int status = Utils.isNotNull(triggerNotification.getStatus()) && triggerNotification.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
                    statement.setString(1, triggerNotification.getStartTime());
                    statement.setString(2, triggerNotification.getChannel());
                    statement.setString(3, triggerNotification.getMessage());
                    statement.setInt(4, status);
                    statement.setString(5, new JSONArray(triggerNotification.getContacts()).toString());
                    statement.setInt(6, triggerNotification.isAddFeedback() ? 1 : 0);
                    statement.setInt(7, Utils.isNotNull(triggerNotification.getScheduleId()) ? Integer.parseInt(triggerNotification.getScheduleId()) : 0);
                    statement.setString(8, uuid);
                    statement.setString(9, triggerNotification.getSubject());
                    statement.setString(10, triggerNotification.getConditionQuery());
                    statement.setString(11, triggerNotification.getSubmissionToken());
                    statement.setString(12, Utils.isNotNull(triggerNotification.getTriggerStatus()) ? (triggerNotification.getTriggerStatus()) : Constants.STATUS_ACTIVE_1);
                    //DTV-8038, added action_type column
                    statement.setString(13, Utils.isNotNull(triggerNotification.getActionType()) && Utils.notEmptyString(triggerNotification.getActionType()) ? (triggerNotification.getActionType()) : Constants.EMPTY);
                    return statement;
                }
            });
            result = uuid;
            logger.info("end creating trigger details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to get trigger schedule primary id by using its uniqueId
     *
     * @param scheduleId
     * @return
     */
    public Map getScheduledIdByUniqueId(String scheduleId) {
        logger.debug("start getting schedule id by scheduleuuid");
        Map resultMap = new HashMap();
        try {
            List result = jdbcTemplate.queryForList(GET_TRIGGER_SCHEDULE_ID, new Object[]{scheduleId});
            if(result.size() > 0){
                resultMap = (Map) result.get(0);
            }
        } catch (Exception e) {

        }
        logger.debug("end getting schedule id by scheduleuuid");
        return resultMap;
    }

    /**
     * Method to create trigger schedule details
     *
     * @param schedule
     * @return
     */
    public String createTriggerSchedule(final TriggerSchedule schedule) {
        logger.debug("begin creating the trigger schedule details");
        String result = "";
        try {
            // generate uniqueId for schedule
            String uuid = Utils.generateUUID();
            // get current date time in UTC
            String utcCurrentDateTime = Utils.getCurrentUTCDateAsString();
            // get date part from UTC current datetime
            String lastProcessedDate = utcCurrentDateTime.substring(0, 10);
            // get time part from UTC current datetime
            String lastProcessedTime = utcCurrentDateTime.substring(11);

            //Preventing the conversion to UTC. Dates are retrieved from DB in UTC
            //String startDateUTC = Utils.getUTCDateAsString(schedule.getStartDate()+" "+schedule.getStartTime(), schedule.getTimezone());
            //String endDateUTC = Utils.getUTCDateAsString(schedule.getEndDate()+" "+schedule.getEndTime(), schedule.getTimezone());

            String startDateUTC = schedule.getStartDate() + " " + schedule.getStartTime();
            String endDateUTC = schedule.getEndDate() + " " + schedule.getEndTime();

            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_TRIGGER_SCHEDULE, Statement.RETURN_GENERATED_KEYS);
                    logger.info("schedule Bean {}", schedule.toString());
                    statement.setString(1, startDateUTC.substring(0, 10));
                    statement.setString(2, endDateUTC.substring(0, 10));
                    statement.setString(3, startDateUTC.substring(11, 19));
                    statement.setString(4, endDateUTC.substring(11, 19));
                    statement.setString(5, schedule.getFrequency());
                    statement.setString(6, schedule.getBusinessId());
                    statement.setString(7, schedule.getSurveyId());
                    statement.setString(8, schedule.getTimezone());
                    statement.setString(9, schedule.getTriggerId());
                    statement.setString(10, uuid);
                    statement.setString(11, lastProcessedDate);
                    statement.setString(12, lastProcessedTime);
                    statement.setString(13, schedule.getChannel());
                    statement.setString(14, schedule.getMessage());
                    statement.setString(15, schedule.getSubject());
                    statement.setString(16, Utils.isNotNull(schedule.getContacts()) ? new JSONArray(schedule.getContacts()).toString() : new JSONArray().toString());
                    statement.setInt(17, schedule.isAddFeedback() ? 1 : 0);
                    statement.setString(18, Utils.isNotNull(schedule.getTriggerConfig()) ? new JSONObject((Map) schedule.getTriggerConfig()).toString() : new JSONObject().toString());
                    statement.setString(19, Utils.isNotNull(schedule.getStatus()) ? schedule.getStatus() : Constants.STATUS_INACTIVE);
                    statement.setString(20, schedule.getHost());
                    statement.setInt(21, Utils.isNotNull(schedule.getParticipantGroupId()) ? schedule.getParticipantGroupId() : 0);
                    statement.setString(22, Utils.isNotNull(schedule.getHeaderData()) ? new JSONObject(schedule.getHeaderData()).toString() : new JSONObject().toString());
                    statement.setString(23, Utils.isNotNull(schedule.getWebhookConfig()) ? new JSONObject(schedule.getWebhookConfig()).toString() : new JSONObject().toString());
                    statement.setString(24, Utils.isNotNull(schedule.getIntegrationsConfig()) ? new JSONObject(schedule.getIntegrationsConfig()).toString() : new JSONObject().toString());
                    statement.setString(25, Utils.isNotNull(schedule.getReplyToRespondent()) ? schedule.getReplyToRespondent() : "");
                    return statement;
                }
            });
            result = uuid;
            logger.debug("end creating trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to update trigger schedule details by scheduleId
     *
     * @param schedule
     * @param scheduleId
     * @return
     */
    public int updateTriggerSchedule(final TriggerSchedule schedule, String scheduleId) {
        logger.debug("begin update the trigger schedule details");
        int result = -1;
        try {
            String timezone = Utils.isNotNull(schedule.getTimezone()) ? schedule.getTimezone() : Constants.TIMEZONE_AMERICA_LOSANGELES;

            /**
             * Preventing UTC conversion since dates are obtained directly from DB converted to UTC
             */
            String startDateUTC = schedule.getStartDate() + " " + schedule.getStartTime();
            String endDateUTC = schedule.getEndDate() + " " + schedule.getEndTime();

            //String startDateUTC = Utils.getUTCDateAsString(schedule.getStartDate()+" "+schedule.getStartTime(), timezone);
            //String endDateUTC = Utils.getUTCDateAsString(schedule.getEndDate()+" "+schedule.getEndTime(), timezone);

            String startDate = startDateUTC.substring(0, 10);
            String endDate = endDateUTC.substring(0, 10);
            String startTime = startDateUTC.substring(11, 19);
            String endTime = endDateUTC.substring(11, 19);

            String status = Utils.isNotNull(schedule.getStatus()) ? schedule.getStatus() : Constants.STATUS_INACTIVE;
            String contacts = Utils.isNotNull(schedule.getContacts()) ? new JSONArray(schedule.getContacts()).toString() : new JSONArray().toString();
            String message = Utils.isNotNull(schedule.getMessage()) ? schedule.getMessage() : "";
            String subject = Utils.isNotNull(schedule.getSubject()) ? schedule.getSubject() : "";
            boolean isFeedback = Utils.isNotNull(schedule.isAddFeedback()) ? schedule.isAddFeedback() : false;


            //String triggerConfig = Utils.isNotNull(schedule.getTriggerConfig()) ? new JSONObject(schedule.getTriggerConfig()).toString() : new JSONObject().toString();

            String triggerConfig = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getTriggerConfig())) {
                Map triggerConfigMap = (Map) schedule.getTriggerConfig();
                triggerConfig = new JSONObject(triggerConfigMap).toString();
            }

            String headerData = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getHeaderData())) {
                headerData = new JSONObject(schedule.getHeaderData()).toString();
            }

            String webhookConfig = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getWebhookConfig())) {
                webhookConfig = new JSONObject(schedule.getWebhookConfig()).toString();
            }

            //DTV-6443
            String integrationsConfig = new JSONObject().toString();
            if (Utils.isNotNull(schedule.getIntegrationsConfig())) {
                integrationsConfig = new JSONObject(schedule.getIntegrationsConfig()).toString();
            }

            String replyRespondent = Utils.isNotNull(schedule.getReplyToRespondent()) ? schedule.getReplyToRespondent() : "";
            result = jdbcTemplate.update(UPDATE_TRIGGER_SCHEDULE,
                    new Object[]{startDate, endDate, startTime, endTime,
                            schedule.getFrequency(), timezone, schedule.getChannel(), message, subject,
                            isFeedback, contacts, triggerConfig, status, Utils.isNotNull(schedule.getParticipantGroupId()) ? schedule.getParticipantGroupId() : 0,
                            headerData, webhookConfig, integrationsConfig, replyRespondent, scheduleId});
            logger.debug("end update the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to delete trigger schecdule details by triggerId
     *
     * @param scheduleId
     * @return
     */
    public int deleteTriggerSchedule(String scheduleId) {
        logger.debug("begin deleting the trigger schedule details");
        int result = 0;
        try {
            result = jdbcTemplate.update(DELETE_TRIGGER_SCHEDULE, new Object[]{scheduleId});
            logger.debug("end deleting the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to delete trigger schecdules details by triggerId
     *
     * @param businessUUID
     * @return
     */
    public int deleteTriggerSchedulesByTriggerId(String businessUUID, String triggerUUID, int businessId) {
        logger.debug("begin deleting the trigger schedule details");
        int result = 0;
        try {
            final String scheduleQuery = String.format(DELETE_TRIGGER_SCHEDULES_BY_TRIGGER_ID, (businessUUID.replaceAll("-", "_")));
            result = jdbcTemplate.update(scheduleQuery, new Object[]{triggerUUID, businessId});
            logger.debug("end deleting the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to get triggers schedule details by scheduleId
     *
     * @param scheduleId
     * @return
     */
    public Map getTriggerScheduleByUniqueID(String scheduleId) {
        logger.debug("begin retrieving the trigger schedule details by unique ID");
        Map scheduleMap = null;
        try {

            scheduleMap = jdbcTemplate.queryForMap(GET_TRIGGER_SCHEDULE_BY_UNIQUE_ID, new Object[]{scheduleId});
            logger.debug("end retrieving the trigger details by unique ID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return scheduleMap;
    }

    /**
     * Method to get triggers schedule details by scheduleId
     *
     * @param scheduleId
     * @return
     */
    public Map getTriggerScheduleById(int scheduleId) {
        logger.debug("begin retrieving the trigger schedule details by ID");
        Map scheduleMap = null;
        try {
            scheduleMap = jdbcTemplate.queryForMap(GET_TRIGGER_SCHEDULE_BY_ID, new Object[]{scheduleId});
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return scheduleMap;
    }

    /**
     * Method to get triggers schedule details by triggerId
     *
     * @param triggerId
     * @return
     */
    public List getTriggerScheduleByTriggerID(String triggerId, String businessUniqueId) {
        logger.debug("begin retrieving the trigger schedule details by triggerID");
        List triggerScheduleList = new ArrayList();
        try {
            int businessId = getBusinessIdByUUID(businessUniqueId);
            String query = String.format(GET_TRIGGER_SCHEDULE_BY_TRIGGER_ID, businessUniqueId.replaceAll("-", "_"));
            triggerScheduleList = jdbcTemplate.queryForList(query, new Object[]{triggerId, businessId});
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleList;
    }

    /**
     * Method to get triggers schedule details by triggerId
     *
     * @param triggerId
     * @return
     */
    public List getTriggerScheduleByTriggerID_V2(String triggerId, String businessUniqueId) {
        logger.debug("begin retrieving the trigger schedule details by triggerID");
        List triggerScheduleList = new ArrayList();
        try {
            int businessId = getBusinessIdByUUID(businessUniqueId);
            businessUniqueId = businessUniqueId.replaceAll("-", "_");
            String surveysTable = "surveys_" + businessUniqueId;
            String triggersTable = "trigger_" + businessUniqueId;
            //String query = String.format(GET_TRIGGER_SCHEDULE_JOIN_SURVEY_TABLE_BY_TRIGGER_ID, businessUniqueId.replaceAll("-", "_"));
            String query = "SELECT ts.id, s.to_date, ts.status, ts.start_date, ts.end_date, ts.start_time, ts.end_time, ts.frequency, ts.business_id, ts.survey_id, ts.timezone, ts.schedule_uuid, ts.channel, ts.message, ts.subject, ts.contacts, ts.add_feedback, ts.trigger_config, ts.deactivated_contacts, ts.participant_group_id, ts.header_data, ts.webhook_config, ts.reply_respondent, ts.integrations_config FROM trigger_schedule AS ts  JOIN  "+triggersTable+" AS t ON ts.trigger_id = t.id JOIN  "+surveysTable+" AS s ON t.survey_id = s.id  WHERE t.trigger_uuid = ? AND ts.business_id = ?";
            triggerScheduleList = jdbcTemplate.queryForList(query, triggerId, businessId);
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleList;
    }

    /**
     * Method to get triggers schedule details by triggerId & contains deactivated user email
     * DTV-3633, Remove user from access after triggers set, and then re-add the access filter as well as them to the trigger
     *
     * @param triggerId
     * @return
     */
    public List getEachTriggerScheduleByDeactivatedUserEmail(String triggerId, String businessUniqueId, String deactivatedUserEmail) {
        logger.info("begin retrieving the trigger schedule details by triggerID & deactivated user email");
        List triggerScheduleList = new ArrayList();
        try {
            int businessId = getBusinessIdByUUID(businessUniqueId);
            String query = String.format(GET_TRIGGER_SCHEDULES_BY_TRIGGER_ID_BY_DEACTIVATED_USER_EMAIL, businessUniqueId.replaceAll("-", "_"));
            String formattedEmail = "\"" + deactivatedUserEmail + "\"";
            logger.info("formattedEmail " + formattedEmail);
            triggerScheduleList = jdbcTemplate.queryForList(query, new Object[]{triggerId, businessId, formattedEmail});
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("end retrieving the trigger schedule details by triggerID & deactivated user email");
        return triggerScheduleList;
    }

    /**
     * Method to get triggers schedule start and end date by triggerId
     *
     * @param triggerId
     * @return
     */
    public Map getTriggerScheduleDate(String triggerId, String businessUniqueId) {
        logger.debug("begin retrieving the trigger schedule dates by triggerID");
        Map triggerScheduleMap = new HashMap();
        try {
            List triggerScheduleList = getTriggerScheduleByTriggerID(triggerId, businessUniqueId);
            if (triggerScheduleList.size() > 0) {
                Map triggerSchedule = (Map) triggerScheduleList.get(0);
                triggerScheduleMap.put(Constants.TRIGGER_START_DATE, triggerSchedule.get(Constants.START_DATE).toString());
                triggerScheduleMap.put(Constants.TRIGGER_START_TIME, triggerSchedule.get(Constants.START_TIME).toString());
                triggerScheduleMap.put(Constants.TRIGGER_END_DATE, triggerSchedule.get(Constants.END_DATE).toString());
                triggerScheduleMap.put(Constants.TRIGGER_END_TIME, triggerSchedule.get(Constants.END_TIME).toString());
                triggerScheduleMap.put(Constants.FREQUENCY, triggerSchedule.get(Constants.FREQUENCY).toString());
            }
            logger.debug("end retrieving the trigger dates by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerScheduleMap;
    }


    /**
     * method to update status of the trigger schedule by using ids
     *
     * @param businessId
     * @param surveyId
     * @param triggerId
     * @param state      active / inactive / completed
     * @return
     */
    public int updateScheduleStatusById(int businessId, int surveyId, int triggerId, String state) {
        logger.debug("begin updating the trigger status");
        int result = 0;
        try {
            result = jdbcTemplate.update(UPDATE_TRIGGER_SCHEDULE_STATUS, new Object[]{state, triggerId, businessId, surveyId});
            logger.debug("end updating the trigger status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }


    public int updateScheduleValuesByTriggerId(int businessId, int surveyId, int triggerId, String state, String startDate, String startTime, String endDate, String endTime, String frequency, String executionStartTime, String executionEndTime) {
        logger.debug("begin updating the trigger values");
        int result = 0;
        try {
            result = jdbcTemplate.update(UPDATE_TRIGGER_VALUES, new Object[]{state, startDate, startTime, endDate, endTime, frequency, executionStartTime, executionEndTime, triggerId, businessId, surveyId});
            logger.debug("end updating the trigger values");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * method to update status of the trigger schedule by using schduleId
     *
     * @param scheduleId
     * @param state      active / inactive / completed
     * @return
     */
    public int updateScheduleStatusByScheduleId(int scheduleId, String state) {
        logger.debug("begin updating the trigger status by sheduleid");
        int result = 0;
        try {
            result = jdbcTemplate.update(UPDATE_TRIGGER_SCHEDULE_STATUS_BY_SHEDULEID, new Object[]{state, scheduleId});
            logger.debug("end updating the trigger status by sheduleid");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }


    /**
     * Method to get triggers details using business uuid by triggerUUId
     *
     * @param businessUUID
     * @param triggerId
     * @return
     */
    public Map getTriggerMapByUUId(String businessUUID, String triggerId) {
        logger.debug("begin retrieving the trigger details by triggerUUID");
        Map triggerMap = null;
        try {
            String businessReplaced = (businessUUID.replaceAll("-", "_"));
            String query = String.format(GET_TRIGGER_BY_UUID, businessReplaced, businessReplaced);
            triggerMap = jdbcTemplate.queryForMap(query, new Object[]{triggerId});

            logger.debug("end retrieving the trigger details by triggerUUID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerMap;
    }

    /**
     * Method to get triggers details using business uuid by triggerId
     *
     * @param businessUUID
     * @param triggerId
     * @return
     */
    public Map getTriggerMapByTriggerId(String businessUUID, int triggerId) {
        logger.debug("begin retrieving the trigger details by triggerID");
        Map triggerMap = null;
        try {
            String query = String.format(GET_TRIGGER_BY_ID, (businessUUID.replaceAll("-", "_")));
            triggerMap = jdbcTemplate.queryForMap(query, new Object[]{triggerId});

            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggerMap;
    }

    /**
     * method to check if the trigger is exists in schedule table
     *
     * @param businessId
     * @param surveyId
     * @param triggerId
     * @return
     */
    public boolean getScheduledIdIfExists(int businessId, int surveyId, int triggerId) {
        logger.debug("begin check the trigger exists");
        int recordCount = 0;
        try {
            recordCount = jdbcTemplate.queryForObject(IS_SCHEDULE_EXISTS, new Object[]{triggerId, surveyId, businessId}, Integer.class);
            logger.debug("end  check the trigger exists");
        } catch (EmptyResultDataAccessException e) {
            logger.info("trigger schedule not available for triggerid " + triggerId);
            logger.error(e.getMessage());
//            e.printStackTrace();
        }
        if (recordCount > 0) {
            return true;
        }
        return false;
    }

    /**
     * Method to get active triggers scheduled
     *
     * @return
     */
    public List getActiveTriggerScheduled() {
        logger.info("begin retrieving the trigger schedule details by ID");
        List scheduleList = null;
        try {
            scheduleList = jdbcTemplate.queryForList(GET_ACTIVE_TRIGGER_EXECUTION);
            //logger.info("schedule list {}", scheduleList);
            logger.info("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return scheduleList;
    }

    /**
     * method to update trigger scheduler last processed time and date by using ids
     *
     * @param scheduleId
     * @return
     */
    public int updateLastProcessedDateTime(int scheduleId) {
        logger.debug("begin updating the trigger status");
        int result = 0;
        try {
            // get current date time in UTC
            String utcCurrentDateTime = Utils.getLocalDateTimeString();// Utils.getCurrentUTCDateAsString();
            // get date part from UTC current datetime
            String lastProcessedDate = utcCurrentDateTime.substring(0, 10);
            // get time part from UTC current datetime
            String lastProcessedTime = utcCurrentDateTime.substring(11);
            result = jdbcTemplate.update(UPDATE_LAST_PROCESSED_DATETIME, new Object[]{lastProcessedDate, lastProcessedTime, scheduleId});
            logger.debug("end updating the trigger status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * get businessId by using businessUniqueID
     *
     * @param businessId
     * @return
     */
    public String getBusinessUniqueIdById(int businessId) {
        logger.debug("begin get business id");
        String uuid = "";
        try {
            uuid = jdbcTemplate.queryForObject(GET_BUSINEES_UUID, new Object[]{businessId}, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get business id");
        return uuid;
    }

    /**
     * method to search participant header and returns its index
     *
     * @param participantGrpUUID
     * @param metaDataField
     * @return
     */
    public Map getParticipantHeaderIndex(String participantGrpUUID, String metaDataField) {
        Map resultMap = new HashMap();
        try {
//            String tableName = "participant_" + participantGrpUUID.replaceAll("-", "_");
            String tableName = String.format("participant_%s", participantGrpUUID.replaceAll("-", "_"));
            String keyIndexSql = "SELECT distinct json_search(upper(header), 'one', upper(?)) as 'Header' FROM " + tableName +
                    " where json_search(upper(header), 'one', upper(?)) is not null"; // to avoid null value
            resultMap = jdbcTemplate.queryForMap(keyIndexSql, new Object[]{metaDataField, metaDataField});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }

    /**
     * method to get participant ids list as string by using query condition
     *
     * @param participantGroupUUID
     * @param startDate
     * @param endDate
     * @param sqlWhere
     * @return
     */
    public String getParitipantsIdsString(String participantGroupUUID, String startDate, String endDate, String sqlWhere) {
        String sql = "";
        String tableName = String.format("participant_%s", participantGroupUUID);
        if (Utils.isNotNull(startDate) && Utils.isNotNull(endDate)) {
//            sql = "select json_arrayagg(id) from participant_" + participantGroupUUID + " where " + sqlWhere + " and created_time between '" + startDate + "' and '" + endDate + "' limit 100000";
            sql = "select json_arrayagg(id) from " + tableName + " where " + sqlWhere + " and created_time between ? and ? limit 100000";
        } else {
//            sql = "select json_arrayagg(id) from participant_" + participantGroupUUID + " where " + sqlWhere + " limit 100000";
            sql = "select json_arrayagg(id) from " + tableName + " where " + sqlWhere + " limit 100000";
        }
        String participantIdsString = (Utils.isNotNull(startDate) && Utils.isNotNull(endDate)) ? jdbcTemplate.queryForObject(sql, new Object[]{startDate, endDate}, String.class) : jdbcTemplate.queryForObject(sql, new Object[]{}, String.class);
        return participantIdsString;
    }

    /**
     * Method to get triggers notification details using business uuid by triggerId
     *
     * @param notifyId
     * @return
     */
    public Map getTriggerNotificationById(String notifyId) {
        logger.debug("begin retrieving the trigger details by triggerID");
        Map triggerMap = new HashMap();
        try {
            triggerMap = jdbcTemplate.queryForMap(GET_NOTIFICATION_BY_UUID, new Object[]{notifyId});
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            triggerMap = null;
        }
        return triggerMap;
    }

    /**
     * Method to update trigger schedule details by scheduleId
     *
     * @param notification
     * @param notificationId
     * @return
     */
    public int updateTriggerNotification(final TriggerNotification notification, String notificationId) {
        logger.debug("begin update the trigger schedule details");
        int result = -1;
        try {
            int status = Utils.isNotNull(notification.getStatus()) && notification.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
            result = jdbcTemplate.update(UPDATE_NOTIFICATION_BY_UUID,
                    new Object[]{notification.getChannel(), notification.getMessage(), notification.getSubject(), notification.getStatus(),
                            notification.isAddFeedback(), new JSONArray(notification.getContacts()).toString(), new Timestamp(System.currentTimeMillis()), notificationId});
            logger.debug("end update the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to get triggers notification details using business uuid by triggerId
     *
     * @param scheduleId
     * @return
     */
    public List<TriggerNotification> getNotificationDetailsByScheduleId(int scheduleId) {
        logger.debug("begin retrieving the trigger details by triggerID");
        List triggerNotificationList = new ArrayList();
        try {
//            String query = String.format(GET_NOTIFICATION_BY_SCHEDULEID, (businessUUID.replaceAll("-", "_")));
            List resultList = jdbcTemplate.queryForList(GET_NOTIFICATION_BY_SCHEDULEID, new Object[]{scheduleId});
            Iterator iterator = resultList.iterator();
            while (iterator.hasNext()) {
                Map triggerMap = (Map) iterator.next();
                TriggerNotification trigger = new TriggerNotification();
                trigger.setChannel(triggerMap.get(Constants.CHANNEL).toString());
                trigger.setStatus(((int) triggerMap.get(Constants.STATUS)) == 0 ? Constants.STATUS_INACTIVE : Constants.STATUS_ACTIVE);
                trigger.setMessage(triggerMap.get(Constants.MESSAGE).toString());
                trigger.setSubject(triggerMap.get(Constants.SUBJECT).toString());
                trigger.setContacts(Utils.isNotNull(triggerMap.get(Constants.CONTACTS)) ? mapper.readValue(triggerMap.get(Constants.CONTACTS).toString(), ArrayList.class) : new ArrayList<>());
                trigger.setAddFeedback((int) (triggerMap.get(Constants.ADD_FEEDBACK)) == 1 ? Boolean.TRUE : Boolean.FALSE);
//                trigger.setTriggerConfig(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONFIG)) ? mapper.readValue(triggerMap.get(Constants.TRIGGER_CONFIG).toString(), TriggerConfig.class) : new TriggerConfig());
                trigger.setNotificationId(triggerMap.get(Constants.NOTIFICATION_UUID).toString());
                triggerNotificationList.add(trigger);
            }
            logger.debug("end retrieving the trigger details by triggerID");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return triggerNotificationList;
    }


    /**
     * Method to delete trigger notification details by notify unique id
     *
     * @param notifyId
     * @return
     */
    public int deleteTriggerNotification(String notifyId) {
        logger.debug("begin deleting the trigger schedule details");
        int result = 0;
        try {
            result = jdbcTemplate.update(DELETE_TRIGGER_NOTIFICATION, new Object[]{notifyId});
            logger.debug("end deleting the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to get survey participants group ids from sheduled survey
     *
     * @param surveyId
     * @param businessId
     * @param businessUUID
     * @return
     */
    public List retrieveSurveyParticipantGroupIds(int surveyId, int businessId, String businessUUID) {
        List pariticipantGroupIDs = new ArrayList();
        String tableName = String.format("participant_group_%s", businessUUID.replaceAll("-", "_"));
        try {
            String sql = "select json_arrayagg( participant_group_uuid) from " + tableName +
                    " where id in (select distinct recipient_id from survey_schedule where survey_id=? and business_id = ?)";
            String recipientListString = jdbcTemplate.queryForObject(sql, new Object[]{surveyId, businessId}, String.class);
            pariticipantGroupIDs = (Utils.isNotNull(recipientListString)) ? mapper.readValue(recipientListString, ArrayList.class) : new ArrayList();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return pariticipantGroupIDs;
    }

    /**
     * method to get submission tokens of event for a given surveyId with its participantGroupIds only when feedback is completed.
     *
     * @param participantDataMap
     * @param surveyId
     * @param businessUUID
     * @return
     */
    public List getDynamicTokensByParticipantData(Map participantDataMap, int surveyId, String businessUUID) {
        List<String> dynamicTokens = new ArrayList<>();
        String tableName = String.format("token_%s", businessUUID.replaceAll("-", "_"));
        logger.info("begin retrieving the dynamic tokens by recipient data list");
        try {
            String sql = "select dynamic_token_id from " + tableName + " where participant_id in (:participantId) and participant_group_id = :participantGrpId and survey_id = (:surveyId) and last_action='completed'";
            Set keySet = participantDataMap.keySet();
            Iterator itr = keySet.iterator();
            while (itr.hasNext()) {
                String keyString = itr.next().toString();
                List participantDataList = (List) participantDataMap.get(keyString);
                if (participantDataList.size() > 0) {
                    MapSqlParameterSource recipientDataIds = new MapSqlParameterSource();
                    recipientDataIds.addValue("participantId", participantDataList);
                    recipientDataIds.addValue("surveyId", surveyId);
                    recipientDataIds.addValue("participantGrpId", keyString);

                    List dynamicTokenList = namedParameterJdbcTemplate.queryForList(sql, recipientDataIds);
                    Iterator<Map> tokenListIterator = dynamicTokenList.iterator();
                    while (tokenListIterator.hasNext()) {
                        Map tokenMap = tokenListIterator.next();
                        String dynamicToken = tokenMap.get("dynamic_token_id").toString();
                        dynamicTokens.add(dynamicToken);
                    }

                } else {
                    logger.info("Recipient data list is empty");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("end retrieving the dynamic tokens by recipient data list");
        return dynamicTokens;
    }

    /**
     * This function is use to get participant group uuid by id
     *
     * @param participantId
     * @param businessUUID
     * @return
     */
    public String getParticipantGroupUUIDById(int participantId, String businessUUID) {
        String resultData = "";
        try {
            String table_name = String.format("participant_group_%s", (businessUUID.replaceAll("-", "_")));
            String sql = "select participant_group_uuid from " + table_name + " where id= ? ";
            resultData = jdbcTemplate.queryForObject(sql, new Object[]{participantId}, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultData;

    }

    /**
     * Method to update trigger schedule details by scheduleId
     *
     * @param startTime
     * @param notificationId
     * @return
     */
    public int updateTriggerNotificationTime(String startTime, String notificationId) {
        logger.debug("begin update the trigger schedule details");
        int result = -1;
        try {
            result = jdbcTemplate.update(UPDATE_NOTIFICATION_TIME_BY_UUID, new Object[]{startTime, notificationId});
            logger.debug("end update the trigger schedule details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to get triggers active notification scheduled to send email/push notification
     *
     * @return
     */
    public List getActiveTriggerNotifications() {
        logger.debug("begin retrieving the active trigger notification");
        List resultList = new ArrayList();
        try {
            resultList = jdbcTemplate.queryForList(GET_ACTIVE_NOTIFICATION);
            logger.debug("end retrieving the active trigger notification");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
        }
        return resultList;
    }

    /**
     * Method to get triggers active notification scheduled to send email/push notification
     *
     * @return
     */
    public List getActiveTriggerNotificationsV1() {
        logger.debug("begin retrieving the active trigger notification");
        List resultList = new ArrayList();
        try {
            resultList = jdbcTemplate.queryForList(GET_ACTIVE_NOTIFICATION_V1);
            logger.debug("end retrieving the active trigger notification");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
        }
        return resultList;
    }

    /**
     * method to update status of the trigger schedule by using ids
     *
     * @param state 0:inactive / 1:completed
     * @return
     */
    public int updateNotificationStatusByIds(List notifyIds, String state) {
        logger.debug("begin updating the trigger status");
        int result = 0;
        try {
            if (notifyIds.size() > 0) {
                MapSqlParameterSource sqlParam = new MapSqlParameterSource();
                sqlParam.addValue("ids", notifyIds);
                sqlParam.addValue("state", state);
                result = namedParameterJdbcTemplate.update(UPDATE_TRIGGER_NOTIFICATION_STATUS, sqlParam);

            }
            logger.debug("end updating the trigger status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * get SurveyId by using surveyUniqueID
     *
     * @param surveyId
     * @return
     */
    public String getSurveyUniqueIdById(int surveyId, String businessUUID) {
        logger.debug("begin get survey id");
        String uuid = "";
        try {
            String query = String.format(GET_SURVEY_UUID, (businessUUID.replaceAll("-", "_")));
            uuid = jdbcTemplate.queryForObject(query, new Object[]{surveyId}, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get survey id");
        return uuid;
    }

    /**
     * get Survey details by using survey Id
     *
     * @param surveyId
     * @return
     */
    public Map getSurveyDetailById(int surveyId, String businessUUID) {
        logger.debug("begin get survey id");
        Map resultMap = new HashMap();
        try {
            String query = String.format(GET_SURVEY_DETAILS_BY_ID, (businessUUID.replaceAll("-", "_")));
            resultMap = jdbcTemplate.queryForMap(query, new Object[]{surveyId});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get survey id");
        return resultMap;
    }

    /**
     * method to get survey schedule details by using surveyId and businessId
     *
     * @param surveyId
     * @param businessUniqueId
     * @return
     */
    public Map<String, Object> getSurveyScheduleBySurveyId(int surveyId, String businessUniqueId) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try {
            int businessId = this.getBusinessIdByUUID(businessUniqueId);
            resultMap = jdbcTemplate.queryForMap(GET_SURVEY_SCHEDULE_BY_SURVEYID, new Object[]{businessId, surveyId});
        } catch (Exception e) {
            logger.error("exception at getSurveyScheduleBySurveyId. Msg {}", e.getMessage());
        }
        return resultMap;
    }

    /**
     * method to get submission token list as string by using query condition
     *
     * @param surveyUUID
     * @param sqlWhere
     * @return
     */
    public Map getEventSubmissionTokens(String surveyUUID, String sqlWhere, String lastExecutedDate) {
        Map tokenList = new HashMap();
        try {
            //String lastProcessDate = Utils.isNotNull(lastExecutedDate) ? " and created_time >='" + lastExecutedDate + "'" : " ";
            String lastProcessDate = Utils.isNotNull(lastExecutedDate) ? " and created_time >=?" : " ";
//        String sql = "select submission_token, id from event_" + surveyUUID.replaceAll("-","_") + " where " + sqlWhere + lastProcessDate +" limit 100000";
            String sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from event_" + surveyUUID.replaceAll("-", "_") + " where " + sqlWhere + lastProcessDate;
            logger.info("Sql event submission tokens {}", sql);
            tokenList = Utils.isNotNull(lastExecutedDate) ? jdbcTemplate.queryForMap(sql, new Object[]{lastExecutedDate}) : jdbcTemplate.queryForMap(sql);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return tokenList;
    }

    /**
     * get questionId by using QuestionUniqueID
     *
     * @param quesitonUUID
     * @return
     */
    public int getQuestionIdByUniqueId(String quesitonUUID, String businessUUID) {
        logger.debug("begin get question id");
        Integer id = null;
        try {
            String query = String.format(GET_QUESTION_ID, (businessUUID.replaceAll("-", "_")));
            id = jdbcTemplate.queryForObject(query, new Object[]{quesitonUUID}, Integer.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get question id");
        return id;
    }


    /**
     * get questionId index by using surveyId.
     * Note: In this query, question uuid is being sarched in survey table's question_data column(question_uuid will be same for both en and ar lang) , since json_search will be used only for array of strings
     *
     * @param businessUUID
     * @param surveyId
     * @return
     */
    public String getQuestionIndexBySurveyIdId(int surveyId, String businessUUID, String quesitonUUID) {
        logger.debug("begin get question index by survey id");
        String questionIndex = "";
        try {
            String query = String.format(GET_QUESTION_ID_INDEX, (businessUUID.replaceAll("-", "_")));
            String questionIndexString = jdbcTemplate.queryForObject(query, new Object[]{quesitonUUID, surveyId}, String.class); // output will be like "$[0].questionId"
            if (Utils.isNotNull(questionIndexString)) {
                String[] questionIdxArr = questionIndexString.split("\\.");
                questionIndex = questionIdxArr.length > 0 ? questionIdxArr[0] : "";
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get question index by survey  id");
        return questionIndex;
    }

    /**
     * method to get question options values
     * @param surveyId
     * @param businessUniqueId
     * @param questionUniqueId
     * @return
     */
    public Map getQuestionDataBySurveyId(int surveyId, String businessUniqueId, String questionUniqueId) {
        logger.debug("begin get question index by survey id - getRankingQuestionDataBySurveyIdId");

        List options = new ArrayList();
        List optionIds = new ArrayList();
        List otherOptionIds = new ArrayList();
        List otherOptions = new ArrayList();
        int index = -1;
        int questionId = -1;
        String type = "";
        String category = "";
        List subQuestionUniqueIds = new ArrayList();
        List subQuestionIds = new ArrayList();

        try {
            List surveyObjectList = getSurveyByID(surveyId, businessUniqueId);

            if (surveyObjectList != null && surveyObjectList.size() > 0) {
                Map surveyObject = (Map) surveyObjectList.get(0);
                List questionIds = (surveyObject.containsKey("question_ids")) ? mapper.readValue(surveyObject.get("question_ids").toString(), ArrayList.class) : new ArrayList();
                List questionDatas = (surveyObject.containsKey("question_data")) ? mapper.readValue(surveyObject.get("question_data").toString(), ArrayList.class) : new ArrayList();
                List questionGroupList = (surveyObject.containsKey("questions_group") && Utils.isNotNull(surveyObject.get("questions_group"))) ? mapper.readValue(surveyObject.get("questions_group").toString(), ArrayList.class) : new ArrayList();

                questionId = getQuestionIdByItsUniqueId(questionUniqueId, businessUniqueId);
                index = questionId > 0 ? questionIds.indexOf(questionId) : -1;

                //sometimes questionGroupList will be ["", "", ""]
                subQuestionIds = (index != -1 && index < questionGroupList.size() && questionGroupList.get(index) instanceof List) ? (List) questionGroupList.get(index) : new ArrayList(); // get list of sub question Ids for question Idx
                Map questionData = (index != -1 && index < questionDatas.size()) ? (Map) questionDatas.get(index) : new HashMap();
                type = questionData.containsKey("type") ? questionData.get("type").toString() : "";
                category = questionData.containsKey("category") && Utils.isNotNull(questionData.get("category")) ? questionData.get("category").toString() : "";

                subQuestionUniqueIds = questionData.containsKey("questionIds") && Utils.isNotNull(questionData.get("questionIds")) ?
                        (List)(questionData.get("questionIds")) : new ArrayList();

                if (type.equals("singleChoice") || type.equals("multiChoice") || type.equals("ranking") || type.equals(Constants.DROP_DOWN)
                        || type.equals(Constants.PICTURE_CHOICE) || type.equals(Constants.POLL)) {
                    //DTV-7707 ranking question type implemented
                    Map multiChoiceRecords = getMultipleChoiceOptions(type, businessUniqueId, questionId);
                    if (multiChoiceRecords != null) {
                        options = (multiChoiceRecords.containsKey("option_data")) && Utils.isNotNull(multiChoiceRecords.get("option_data"))
                                ? mapper.readValue(multiChoiceRecords.get("option_data").toString(), ArrayList.class) : new ArrayList();
                        otherOptions = (multiChoiceRecords.containsKey("other_option_data")) && Utils.isNotNull(multiChoiceRecords.get("other_option_data"))
                                ? mapper.readValue(multiChoiceRecords.get("other_option_data").toString(), ArrayList.class) : new ArrayList();
                        optionIds = (multiChoiceRecords.containsKey("option_ids")) && Utils.isNotNull(multiChoiceRecords.get("option_ids"))
                                ? mapper.readValue(multiChoiceRecords.get("option_ids").toString(), ArrayList.class) : new ArrayList();
                        otherOptionIds = (multiChoiceRecords.containsKey("other_option_ids") && Utils.isNotNull(multiChoiceRecords.get("other_option_ids"))
                                ? mapper.readValue(multiChoiceRecords.get("other_option_ids").toString(), ArrayList.class) : new ArrayList());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("exception at getRankingQuestionDataBySurveyIdId {}", e.getMessage());
        }

        Map returnMap = new HashMap();
        returnMap.put("options", options);
        returnMap.put("optionIds", optionIds);
        returnMap.put("otherOptionIds", otherOptionIds);
        returnMap.put("index", index);
        returnMap.put("questionId", questionId);
        returnMap.put("type", type);
        returnMap.put("category", category);
        returnMap.put("otherOptions", otherOptions);
        returnMap.put("subQuestionUniqueIds", subQuestionUniqueIds);
        returnMap.put("subQuestionIds", subQuestionIds);

        logger.debug("end get question index by survey  id - getRankingQuestionDataBySurveyIdId");
        return returnMap;
    }

    /**
     * method to route multiple choice options based on its type.
     * @param type
     * @param businessUniqueId
     * @param questionId
     * @return
     */
    private Map getMultipleChoiceOptions(String type, String businessUniqueId, int questionId){
       if(type.equals(Constants.RANKING))
           return  getRankingOptionsByQuestionId(businessUniqueId, questionId);
       else if(type.equals(Constants.DROP_DOWN))
           return  getDropDownOptionsByQuestionId(businessUniqueId, questionId);
       else
           return getMultiChoiceOptionsByQuestionId(businessUniqueId, questionId);
    }

    /**
     * method to get survey details by its primary id
     * @param surveyId
     * @param businessUUID
     * @return
     */
    public List getSurveyByID(int surveyId, String businessUUID){
        logger.info("getSurveyByID - start");
        try {
            String query = String.format(GET_SURVEY_BY_ID, ("surveys_" + businessUUID.replaceAll("-", "_")));
            return jdbcTemplate.queryForList(query, new Object[]{surveyId});
        }catch (Exception e){
            logger.error("exception at getSurveyByID, {}", e.getMessage());
        }
        logger.info("getSurveyByID - end");
        return null;
    }

    /**
     * method to get quetion primary id by its uniqueId
     * @param questionUniqueId
     * @param businessUniqueId
     * @return
     */
    public int getQuestionIdByItsUniqueId(String questionUniqueId, String businessUniqueId){
        logger.info("getQuestionIdByItsUniqueId - start");
        try{
            String tableName = "questions_" + businessUniqueId.replace("-", "_");
            String questionSql = String.format(GET_QUESTION_ID_BY_UUID, tableName);
            return jdbcTemplate.queryForObject(questionSql, new Object[]{questionUniqueId}, Integer.class);
        }catch(Exception e){
            logger.error("exception at getQuestionIdByItsUniqueId, {}", e.getMessage());
        }
        logger.info("getQuestionIdByItsUniqueId - end");
        return 0;
    }

    /**
     * method to get ranking question option data
     * @param businessUniqueId
     * @param questionId
     * @return
     */
    public Map getRankingOptionsByQuestionId(String businessUniqueId, int questionId){
        logger.info("getRankingOptionsByQuestionId - start");
        try {
            String multiChoiceTableName = "multiple_ranking_options_" + businessUniqueId.replace("-", "_");
            String sql = String.format(GET_MULTI_RANKING_OPTIONS_BY_QUESTION_ID, multiChoiceTableName);
            String multiChoiceSql = String.format(sql, new Object[]{multiChoiceTableName});
            return jdbcTemplate.queryForMap(multiChoiceSql, new Object[]{questionId});
        }catch (Exception e){
            logger.error("exception at getRankingOptionsByQuestionId, {}", e.getMessage());
        }
        logger.info("getRankingOptionsByQuestionId - end");
        return null;
    }

    /**
     * method to get dropdown question option data
     * @param businessUniqueId
     * @param questionId
     * @return
     */
    public Map getDropDownOptionsByQuestionId(String businessUniqueId, int questionId){
        logger.info("getDropDownOptionsByQuestionId - start");
        try {
            String dropDownOptionTable = "dropdown_options_" + businessUniqueId.replace("-", "_");
            String sql = String.format(GET_MULTI_RANKING_OPTIONS_BY_QUESTION_ID, dropDownOptionTable);
            String dropDownChoiceSql = String.format(sql, new Object[]{dropDownOptionTable});
            return jdbcTemplate.queryForMap(dropDownChoiceSql, new Object[]{questionId});
        }catch (Exception e){
            logger.error("exception at getDropDownOptionsByQuestionId, {}", e.getMessage());
        }
        logger.info("getDropDownOptionsByQuestionId - end");
        return null;
    }


    /**
     *
     * @param businessUUID
     * @param surveyUUID
     * @param questionId
     * @param requestLanguage
     * @return
     */
    public Map getDropdownOptionIdsAndLabelsByQuestionId(String businessUUID, String surveyUUID, int questionId, String requestLanguage) {
        Map dropdownMap = new HashMap();
        List labels = new ArrayList();
        List options = new ArrayList();
        List dropdownLabels = new ArrayList();
        String dropdownTable = "dropdown_options_" + businessUUID.replace("-", "_");
        String surveyTable = "surveys_" + businessUUID.replace("-", "_");
        try {
            String GET_DROPDOWN_OPTIONS_BY_QUESTION_ID = "select * from %s where question_id = ?";
            String dropdownSql = String.format(GET_DROPDOWN_OPTIONS_BY_QUESTION_ID, new Object[]{dropdownTable});
            String surveySql = String.format(GET_SURVEY_BY_UUID_TEMPLATE, new Object[]{surveyTable});
            List dropdownRecords = jdbcTemplate.queryForList(dropdownSql, new Object[]{questionId});
            if (dropdownRecords.size() > 0) {
                Map dropdownRecord = (Map) dropdownRecords.get(0);
                String optionIdString = dropdownRecord.get("option_ids").toString();
                String otherOptionDataString = (Utils.isNotNull(dropdownRecord.get("other_option_data"))) ? dropdownRecord.get(
                        "other_option_data").toString() : new JSONArray().toString();
                options = mapper.readValue(optionIdString, ArrayList.class);
                dropdownLabels = mapper.readValue(otherOptionDataString, ArrayList.class);
            }
            List surveyRecords = jdbcTemplate.queryForList(surveySql, new Object[]{surveyUUID});
            if (surveyRecords.size() > 0) {
                Map surveyRecord = (Map) surveyRecords.get(0);
                List languages = mapper.readValue(surveyRecord.get("languages").toString(), ArrayList.class);
                List questionIds = mapper.readValue(surveyRecord.get("question_ids").toString(), ArrayList.class);
                List multiQuestions = mapper.readValue(surveyRecord.get("multi_questions").toString(), ArrayList.class);
                int languageIndex = (languages.indexOf(requestLanguage) > -1) ? languages.indexOf(requestLanguage) : 0;
                int questionIndex = questionIds.indexOf(questionId);
                List questionDatas = (List) multiQuestions.get(languageIndex);
                Map questionData = (Map) questionDatas.get(questionIndex);
                labels = (List) questionData.get("options");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dropdownMap.put("labels", labels);
        dropdownMap.put("options", options);
        dropdownMap.put("otherLabels", dropdownLabels);
        return dropdownMap;
    }

    /**
     * method to get dropdown question option data by unique Id
     * @param businessUniqueId
     * @return
     */
    public Map getDropDownOptionsByQuestionUniqueId(String businessUniqueId, String questionUniqueId){
        logger.info("getDropDownOptionsByQuestionUniqueId - start");
        try {
            String sql = String.format(GET_MULTI_RANKING_OPTIONS_BY_QUESTION_UUID,
                    "dropdown_options_" + businessUniqueId.replace("-", "_"),
                    "questions_" + businessUniqueId.replace("-", "_"));
            return jdbcTemplate.queryForMap(sql, new Object[]{questionUniqueId});
        }catch (Exception e){
            logger.error("exception at getDropDownOptionsByQuestionId, {}", e.getMessage());
        }
        logger.info("getDropDownOptionsByQuestionUniqueId - end");
        return null;
    }

    /**
     * method to get question option ids and other option data
     *
     * @param questionId
     * @param businessUUID
     * @return
     */
    public Map getMultiChoiceOptionByQuestionUUID(String questionId, String businessUUID) {
        Map multiChoiceRecords = new HashMap();
        try {
            String multiChoiceTableName = String.format("multiple_ranking_options_%s", businessUUID.replace("-", "_"));
            String questionTableName = String.format("questions_%s", businessUUID.replace("-", "_"));

            String sql = String.format(GET_MULTI_RANKING_OPTIONS_BY_QUESTION_UUID, multiChoiceTableName, questionTableName);
            String multiChoiceSql = String.format(sql, new Object[]{multiChoiceTableName});
            multiChoiceRecords = jdbcTemplate.queryForMap(multiChoiceSql, new Object[]{questionId});
        } catch (Exception e) {
            logger.error("exception at getMultiChoiceOptionQuestion {}", e.getMessage());
        }
        return multiChoiceRecords;
    }


    /**
     * method to get multi choice question option data
     * @param businessUniqueId
     * @param questionId
     * @return
     */
    public Map getMultiChoiceOptionsByQuestionId(String businessUniqueId, int questionId){
        logger.info("getMultiChoiceOptionsByQuestionId - start");
        try {
            String multiChoiceTableName = "multiple_choice_options_" + businessUniqueId.replace("-", "_");
            String sql = String.format(GET_MULTI_CHOICE_OPTIONS_BY_QUESTION_ID, multiChoiceTableName);
            String multiChoiceSql = String.format(sql, new Object[]{multiChoiceTableName});
            return jdbcTemplate.queryForMap(multiChoiceSql, new Object[]{questionId});
        }catch (Exception e){
            logger.error("exception at getMultiChoiceOptionsByQuestionId, {}", e.getMessage());
        }
        logger.info("getMultiChoiceOptionsByQuestionId - end");
        return null;
    }

    /**
     * method to get participant name and submission token based on the trigger condition sub query
     *
     * @param query
     * @param businessUniqueId
     * @return
     */
    public List getMatchedParticipants(String query, String businessUniqueId) {
        List participantList = new ArrayList();
        try {
            if (Utils.notEmptyString(query)) {
                // String sql = "select name, submission_token as tokens, last_action_time from token_"+businessUniqueId.replaceAll("-","_")+ " where dynamic_token_id in ("+query+")";
                participantList = jdbcTemplate.queryForList(query);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return participantList;
    }

    /**
     * method to get participant name and submission token based on the token
     *
     * @param token
     * @param surveyUniqueId
     * @return
     */
    public Map getParticipantByToken(String token, String surveyUniqueId) {
        Map participantMap = new HashMap();
        String tableName = String.format("event_%s", surveyUniqueId.replaceAll("-", "_"));
        try {
            String sql = "select created_time, json_extract(event_metadata, '$.metaData') as metadata from " + tableName + " where submission_token = ?";
            participantMap = jdbcTemplate.queryForMap(sql, token);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return participantMap;
    }

    /**
     * function to return surveyStatus by userToken
     *
     * @param userToken
     * @return surveyStatus
     */

    public List getSurveyStatusByUserToken(String userToken, String businessUUID) {
        List result = new ArrayList();
        try {
            String GET_SURVEY_STATUS_BY_USERTOKEN = "select survey_id, last_action, participant_id, tag_id, participant_group_id, last_action_time from %s where dynamic_token_id = ?";
            String query = String.format(GET_SURVEY_STATUS_BY_USERTOKEN, "token_" + businessUUID.replaceAll("-", "_"));
            result = jdbcTemplate.queryForList(query, userToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * method to get users list based on business_uuid
     *
     * @param businessUUID
     * @return
     */
    public List getUsersByBusinessUUID(String businessUUID) {
        List usersList = new ArrayList();
        try {
            String query = String.format(GET_USERS_BY_BUSINESSUUID);
            usersList = jdbcTemplate.queryForList(query, new Object[]{businessUUID});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return usersList;
    }


    /**
     * method to get user survey filters based on survey id
     *
     * @param userId
     * @param surveyId
     * @param businessUUID
     * @return
     */
    public Map getMappedSurveyFiltersByUserId(int userId, int surveyId, String businessUUID) {
        Map surveyFilterMap = new HashMap();
        try {
            String businessUUIDReplaced = businessUUID.replaceAll("-", "_");
            String query = String.format(GET_SURVEY_FILTERS_BY_SURVEY_ID_LEFT_JOIN_USER_SURVEY_FILTER, businessUUIDReplaced, businessUUIDReplaced);
            List surveyFiltersList = jdbcTemplate.queryForList(query, new Object[]{surveyId, userId});
            if (surveyFiltersList.size() > 0) {
                surveyFilterMap = (Map) surveyFiltersList.get(0);
            }
        } catch (Exception e) {
            logger.error("Exception at getMappedSurveyFiltersByUserId. Msg {}", e.getMessage());
        }
        return surveyFilterMap;
    }


    /**
     * Method to get all token for a survey
     *
     * @param surveyUUID
     * @return
     */
    public List getAllTokensOfSsurveyId(String surveyUUID) {
        List tokens = new ArrayList();
        try {
            String eventTableName = String.format("event_%s", (surveyUUID.replace("-", "_")));
            String sql = String.format(GET_ALL_TOKENS_BY_SURVEY_ID, eventTableName);
            List records = jdbcTemplate.queryForList(sql);
            Iterator<Map> iterator = records.iterator();
            while (iterator.hasNext()) {
                Map eachRecord = iterator.next();
                tokens.add(eachRecord.get("submission_token").toString());
            }
        } catch (Exception e) {
//            e.printStackTrace();
            logger.error("exception at getAllTokensOfSsurveyId {}",e.getMessage());
        }
        return tokens;
    }

    /**
     * method to update trigger readable query into trigger table
     *
     * @param businessUUID
     * @param readableQuery
     * @param triggerId
     * @return
     */
    public int updateTriggerReadableQuery(String businessUUID, String readableQuery, int triggerId) {
        logger.debug("begin updating the trigger readable query");
        int result = 0;
        try {
            final String query = String.format(UPDATE_TRIGGER_READABLE_QUERY, (businessUUID.replaceAll("-", "_")));
            result = jdbcTemplate.update(query, new Object[]{readableQuery, triggerId});
            logger.debug("end updating the trigger readable query");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * method to get user mapped surveys on user id
     *
     * @param userId
     * @param businessUUID
     * @return
     */
    public Map getMappedSurveysByUserId(int userId, String businessUUID) {
        Map mappedSurveys = new HashMap();
        try {
            String businessUUIDReplaced = businessUUID.replaceAll("-", "_");
            String query = String.format(GET_MAPPED_SURVEYS_BY_USER_ID, businessUUIDReplaced);
            List surveyFiltersList = jdbcTemplate.queryForList(query, new Object[]{userId});
            if (surveyFiltersList.size() > 0) {
                mappedSurveys = (Map) surveyFiltersList.get(0);
            }
        } catch (Exception e) {
            logger.error("exception at getMappedSurveysByUserId {}",e.getMessage());
        }
        return mappedSurveys;
    }


    /**
     * method to get question option ids and other option data
     *
     * @param questionId
     * @param businessUUID
     * @return
     */
    public Map getMultiChoiceOptionQuestion(int questionId, String businessUUID) {
        Map multiChoiceRecords = new HashMap();
        try {
            String multiChoiceTableName = String.format("multiple_choice_options_%s", businessUUID.replace("-", "_"));
            String sql = String.format(GET_MULTI_CHOICE_OPTIONS_BY_QUESTION_ID, multiChoiceTableName);
            String multiChoiceSql = String.format(sql, new Object[]{multiChoiceTableName});
            multiChoiceRecords = jdbcTemplate.queryForMap(multiChoiceSql, new Object[]{questionId});
        } catch (Exception e) {
            logger.error("exception at getMultiChoiceOptionQuestion {}",e.getMessage());
        }
        return multiChoiceRecords;
    }

    /**
     * method to get question option ids and other option data
     *
     * @param questionId
     * @param businessUUID
     * @return
     */
    public Map getMultiChoiceOptionQuestion(String questionId, String businessUUID) {
        Map multiChoiceRecords = new HashMap();
        try {
            String multiChoiceTableName = String.format("multiple_choice_options_%s", businessUUID.replace("-", "_"));
            String questionTableName = String.format("questions_%s", businessUUID.replace("-", "_"));

            String sql = String.format(GET_MULTI_CHOICE_OPTIONS_BY_QUESTION_UUID, multiChoiceTableName, questionTableName);
            String multiChoiceSql = String.format(sql, new Object[]{multiChoiceTableName});
            multiChoiceRecords = jdbcTemplate.queryForMap(multiChoiceSql, new Object[]{questionId});
        } catch (Exception e) {
            logger.error("exception at getMultiChoiceOptionQuestion {}", e.getMessage());
        }
        return multiChoiceRecords;
    }

    /**
     * method to get survey details using survey unique id
     *
     * @param surveyUUID
     * @param businessUUID
     * @return
     */
    public Map getSurveyDetailsBySurveyUniqueId(String surveyUUID, String businessUUID) {
        Map surveyMap = new HashMap();
        try {
            String surveyTable = String.format("surveys_%s", businessUUID.replace("-", "_"));
            String surveySql = String.format(GET_SURVEY_BY_UUID, new Object[]{surveyTable});
            surveyMap = jdbcTemplate.queryForMap(surveySql, new Object[]{surveyUUID});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return surveyMap;
    }

    /**
     * method to get event using its submission token
     *
     * @param surveyUUID
     * @param respondentId
     * @return
     */
    public Map getEventBySubmissionToken(String surveyUUID, String respondentId) {
        Map feedbacks = new HashMap();
        try {
            String eventTableName = String.format("event_%s", surveyUUID.replaceAll("-", "_"));
            String surveySql = String.format(GET_EVENT_BY_TOKEN, eventTableName);
            feedbacks = jdbcTemplate.queryForMap(surveySql, new Object[]{respondentId});
        } catch (Exception e) {
            logger.error("exception at getEventBySubmissionToken {}", e.getMessage());
        }
        return feedbacks;
    }

    public int getQuestionIdByQuestionUUID(String questionUUID, String businessUUID) {
        int questionId = 0;
        try {
            String tableName = String.format("questions_%s", businessUUID.replace("-", "_"));
            String questionSql = String.format(GET_QUESTION_ID_BY_UUID, tableName);
            questionId = jdbcTemplate.queryForObject(questionSql, new Object[]{questionUUID}, Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionId;
    }

    /**
     * @param execStartTime
     * @param execEndTime
     * @param scheduleUUID
     */
    public void updateExecutionTimesByTriggerUUID(String execStartTime, String execEndTime, String scheduleUUID) {
        logger.info("begin update execution time ");
        try {
            int result = jdbcTemplate.update(UPDATE_TRIGGER_EXEC_TIME_BY_UUID, new Object[]{execStartTime, execEndTime, scheduleUUID});
            logger.info("end update execution time {}", result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param execStartTime
     * @param execEndTime
     * @param id
     */
    public void updateExecutionTimes(String execStartTime, String execEndTime, int id) {
        logger.info("begin update execution time ");
        try {
            int result = jdbcTemplate.update(UPDATE_TRIGGER_EXEC_TIME, new Object[]{execStartTime, execEndTime, null, id});
            logger.info("end update execution time {}", result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return
     */
    public List getActiveSchedules() {
        List schedules = new ArrayList();
        try {
            schedules = jdbcTemplate.queryForList(GET_ACTIVE_TRIGGERS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return schedules;

    }


    /**
     * @param scheduleId
     * @return
     */
    public List retrieveNotificationsForASchedule(int scheduleId, String triggerNotifyTime, String businessUUID) {
        List notifications = new ArrayList();
        try {

            /**
             *Added changes for DTV-5352, DTV-5351, In edge cases (around 12 AM IST),
             * Logic --> Aggregate case will trigger only once per day
             * Each day will have only triggerNotifyTime it will change only date changes as per user created timezone.
             * So checking for exisiting entries by triggerNotifyTime day start to triggerNotifyTime day end.
             *
             */
            String currentTime = Utils.emptyString(triggerNotifyTime) ? Utils.getCurrentDateInTimezone(Constants.UTC) : triggerNotifyTime;
            String startTime = currentTime.substring(0, 10) + " 00:00:00";
            String endTime = currentTime.substring(0, 10) + " 23:59:59";
            String sql = String.format(GET_NOTIFICATION_BY_SCHEDULEID_BYDAY_V1, businessUUID.replace("-","_"));
            notifications = jdbcTemplate.queryForList(sql, new Object[]{startTime, endTime, scheduleId});

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notifications;
    }

    /**
     * method to get list of token from notification
     *
     * @param scheduleId
     * @return
     */
    public List getNotificationSubmissionTokens(int scheduleId, String businessUUID) {
        List tokens = new ArrayList();
        try {
            String sql = String.format(GET_NOTIFICATION_TOKENS_BY_SCHEDULEID_V1, businessUUID.replaceAll("-","_"));
            String tokensList = jdbcTemplate.queryForObject(sql, new Object[]{scheduleId}, String.class);
            tokens = Utils.notEmptyString(tokensList) ? mapper.readValue(tokensList, ArrayList.class) : new ArrayList();

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return tokens;
    }

    /**
     * method to get submission token list as string by using query condition
     *
     * @param surveyUUID
     * @param sqlWhere
     * @return
     */
    public Map getEventSubmissionTokens(String surveyUUID, String sqlWhere, List<String> tokens, String triggerStartDate) {
        Map tokenList = new HashMap();
        String tableName = String.format("event_%s", surveyUUID.replaceAll("-", "_"));
        try {
//            String sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from event_" + surveyUUID.replaceAll("-", "_") + " where created_time>'"+triggerStartDate+"' and " + sqlWhere;
            String sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from " + tableName + " where created_time>(:triggerStartDate) and " + sqlWhere;
            if (tokens.size() > 0) {
                sql += " and submission_token not in (:tokens) ";
            }
            logger.info("Sql event submission tokens {}", sql);
//            tokenList = jdbcTemplate.queryForMap(sql);
            MapSqlParameterSource recipientDataIds = new MapSqlParameterSource();
            recipientDataIds.addValue("triggerStartDate", triggerStartDate);
            recipientDataIds.addValue("tokens", tokens);
            tokenList = namedParameterJdbcTemplate.queryForMap(sql, recipientDataIds);
            logger.info("Sql event submission tokens {}", sql);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return tokenList;
    }

    /**
     * method to get submission token list as string by using query condition
     *
     * @param surveyUUID
     * @param sqlWhere
     * @param scheduleID
     * @param triggerStartDate
     * @param businessUUID
     * @return
     */
    public Map getEventSubmissionTokens(String surveyUUID, String sqlWhere, int scheduleID, String triggerStartDate, String businessUUID) {
        logger.info("begin get event submission tokens");
        Map tokenList = new HashMap();
        String tableName = String.format("event_%s", surveyUUID.replaceAll("-", "_"));
        try {
            String sql = "";
            List existingTokens = this.getNotificationSubmissionTokens(scheduleID, businessUUID);
            if (Utils.notEmptyString(sqlWhere)) {
                sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from " + tableName + " where created_time > :createdTime  and  " + sqlWhere;
            } else {
//                sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from event_" + surveyUUID.replaceAll("-", "_") + " where created_time>'" + triggerStartDate + "'  and  submission_token not in (select submission_token from trigger_notification where schedule_id = ?)";
                sql = "select min(id) as min_id, max(id) as max_id, count(id) as id_count, json_arrayagg(submission_token) as tokens from " + tableName + " where created_time > :createdTime ";
            }
            if(existingTokens.size() > 0){
                sql = sql + " and  submission_token not in (:existingTokens)";
                logger.info("Sql event submission tokens {}", sql);
                MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
                sqlParameterSource.addValue("existingTokens", existingTokens);
                sqlParameterSource.addValue("createdTime", triggerStartDate);
                tokenList = namedParameterJdbcTemplate.queryForMap(sql, sqlParameterSource);
            }else {
                logger.info("Sql event submission tokens {}", sql);
                MapSqlParameterSource sqlParameterSource = new MapSqlParameterSource();
                sqlParameterSource.addValue("createdTime", triggerStartDate);
                tokenList = namedParameterJdbcTemplate.queryForMap(sql, sqlParameterSource);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        logger.info("end get event submission tokens");
        return tokenList;
    }

    /**
     * method to get count of records for given scheduleId from notification
     *
     * @param scheduleId
     * @param businessUUID
     * @return
     */
    public int getNotificationCountByScheduleId(int scheduleId, String businessUUID) {
        int result = 0;
        try {
            String sql = String.format(GET_NOTIFICATION_COUNT_BY_SCHEDULEID_V1, businessUUID.replaceAll("-","_"));
            result = jdbcTemplate.queryForObject(sql, new Object[]{scheduleId}, Integer.class);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return result;
    }

    /**
     * Method to list all businesses
     *
     * @return
     */
    public List getAllBusiness() {
        //SE-1091, DTV-7349 - Fetching active schedule businessId only
        String GET_ALL_BUSINESSS = "select business_uuid from business where business_id in (select distinct business_id from trigger_schedule WHERE STATUS ='active')";
        List<Map<String, Object>> dbMaps = new ArrayList<Map<String, Object>>();
        try {
            dbMaps = jdbcTemplate.queryForList(GET_ALL_BUSINESSS);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return dbMaps;
    }


    /**
     * Method to get expired surveys
     *
     * @param businessUUID
     * @return
     */
    public List getExpiredSurveys(String businessUUID) {
        List<Map<String, Object>> expiredSurveys = new ArrayList<Map<String, Object>>();
        try {
            String eachBusinessSurveysTable = String.format("surveys_%s", businessUUID.replace("-", "_"));
                String surveysLessThanTime = "select * from " + eachBusinessSurveysTable + " where to_date < current_timestamp";
            expiredSurveys = jdbcTemplate.queryForList(surveysLessThanTime);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return expiredSurveys;
    }

    /**
     * update trigger to inactive based on survey id
     *
     * @param surveyId
     * @param businessUUID
     */
    public void updateTriggerStatusBySurveyId(int surveyId, String businessUUID) {
        String updateTriggerStatusSql = String.format(UPDATE_TRIGGER_STATE, businessUUID.replace("-", "_"));
        try {
            int updateTriggerStatus = jdbcTemplate.update(updateTriggerStatusSql, new Object[]{Constants.STATUS_INACTIVE_0, surveyId});
        } catch (Exception e) {

            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * method to update sent status of the schedule by using scheduleId
     *
     * @param scheduleId
     * @param status     PROCESSING / COMPLETED
     * @return
     */
    public void updateScheduleSentStatusByScheduleId(int scheduleId, String status) {
        logger.debug("begin updating the trigger schedule sent status");
        try {
            jdbcTemplate.update(UPDATE_TRIGGER_SCHEDULE_SENT_STATUS_BY_SHEDULEID, new Object[]{status, scheduleId});
            logger.debug("end updating the trigger schedule sent status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * method to update sent status of the notification by using notifyId
     *
     * @param notifyId
     * @param status   PROCESSING / COMPLETED
     * @return
     */
    public void updateNotificationSentStatusByNotifyId(int notifyId, String status) {
        logger.debug("begin updating the trigger notification sent status");
        try {
            jdbcTemplate.update(UPDATE_TRIGGER_NOTIFICATION_SENT_STATUS, new Object[]{status, new Timestamp(System.currentTimeMillis()), notifyId});
            logger.debug("end updating the trigger notification sent status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * method to get role id of the user
     *
     * @param userId
     * @return
     */
    public int getRoleIdByUserId(int userId) {
        int roleId = -1;
        String getRoleIdSql = GET_ROLE_ID;
        try {
            roleId = jdbcTemplate.queryForObject(getRoleIdSql, new Object[]{userId}, Integer.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roleId;
    }

    /**
     * method to user details using userId
     *
     * @param businessUUID
     * @param userId
     * @return
     */
    public Map getUsersByBusinessUserId(String businessUUID, int userId) {
        Map resultUser = new HashMap();
        String getUserSql = GET_USER_DETAILS_BY_ID;
        try {
            resultUser = jdbcTemplate.queryForMap(getUserSql, new Object[]{businessUUID, userId});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultUser;
    }

    /**
     * Method to get all triggers details for the given surveyId & businessUUID
     *
     * @param businessUUID
     * @param surveyId
     * @return
     */
    public List getAllTriggersBySurveyId(String businessUUID, int surveyId) {
        logger.debug("begin retrieving the all trigger details by surveyId");
        List triggersList = new ArrayList();
        try {
            String query = String.format(GET_ALL_TRIGGERS_BY_SURVEYID, (businessUUID.replaceAll("-", "_")));
            triggersList = jdbcTemplate.queryForList(query, new Object[]{surveyId});

            logger.debug("end retrieving the all trigger details by surveyId");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return triggersList;
    }


    /**
     * Method to get all all active (sent status =null) notifications for the given scheduleId
     *
     * @param scheduleId
     * @return
     */
    public List getActiveNotificationsByScheduleId(int scheduleId) {
        logger.debug("begin retrieving the active notfications by scheduleId");
        List notificationsList = new ArrayList();
        try {
            notificationsList = jdbcTemplate.queryForList(GET_ACTIVE_NOTIFICATION_BY_SCHEDULEID, new Object[]{scheduleId});
            logger.debug("end retrieving the active notfications by scheduleId");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return notificationsList;
    }


    /**
     * updating the valid contacts in trigger_schedule & trigger_notification tables by for all trigger schedules
     *
     * @param triggerSchedule
     * @param validContacts
     * @param businessUUID
     * @return
     */
    public int updateContactsInTriggerNotificationsAndTriggerSchedule(List triggerSchedule, List validContacts, String businessUUID) {

        int result = 0;
        List scheduleContacts = new ArrayList();
        List deActivatedScheduleContacts = new ArrayList();
        List scheduleIndexes = new ArrayList();
        try {

            if (triggerSchedule.size() > 0) {
                Iterator iterator = triggerSchedule.iterator();
                while (iterator.hasNext()) {
                    Map triggerScheduleMap = (Map) iterator.next();
                    logger.info("triggerScheduleMap  " + triggerScheduleMap);
                    List eachDeActivatedScheduledContacts = new ArrayList();
                    List eachScheduledContacts = Utils.isNotNull(triggerScheduleMap.get("contacts")) ? mapper.readValue(triggerScheduleMap.get("contacts").toString(), ArrayList.class) : new ArrayList();
                    int size = eachScheduledContacts.size();
                    int scheduleId = Integer.parseInt(triggerScheduleMap.get("id").toString());

                    //checking existing users with valid users
                    for (int i = 0; i < size; i++) {
                        String eachUserEmail = eachScheduledContacts.get(i).toString();
                        if (!validContacts.contains(eachUserEmail)) {
                            eachDeActivatedScheduledContacts.add(eachUserEmail);
                        }
                    }
                    eachScheduledContacts.removeAll(eachDeActivatedScheduledContacts);


                    //Listing all the active notifications (sent status = null) for given schedule Id
                    List eachScheduleNotifications = this.getActiveNotificationsByScheduleId(scheduleId);
                    if (eachScheduleNotifications.size() > 0) {
                        Iterator notificationsIterator = eachScheduleNotifications.iterator();
                        List notificationContacts = new ArrayList();
                        List deActivatedNotificationContacts = new ArrayList();
                        List notificationIndexes = new ArrayList();

                        while (notificationsIterator.hasNext()) {
                            Map eachNotificationMap = (Map) notificationsIterator.next();
                            List eachDeActivatedNotificationContacts = new ArrayList();
                            List eachNotificationContacts = Utils.isNotNull(eachNotificationMap.get("contacts")) ? mapper.readValue(eachNotificationMap.get("contacts").toString(), ArrayList.class) : new ArrayList();
                            int notificationSize = eachNotificationContacts.size();
                            int id = Integer.parseInt(eachNotificationMap.get("id").toString());
                            //checking existing users with valid users
                            for (int i = 0; i < notificationSize; i++) {
                                String eachUserEmail = eachNotificationContacts.get(i).toString();
                                if (!validContacts.contains(eachUserEmail)) {
                                    eachDeActivatedNotificationContacts.add(eachUserEmail);
                                }
                            }

                            eachNotificationContacts.removeAll(eachDeActivatedNotificationContacts);
                            notificationContacts.add(new JSONArray(eachNotificationContacts));
                            deActivatedNotificationContacts.add(new JSONArray(eachDeActivatedNotificationContacts));
                            notificationIndexes.add(id);
                        }

                        String notificationUpdateSql = "update trigger_notification set contacts = ?, deactivated_contacts = ? where id = ? ";
                        int[] updateResult = jdbcTemplate.batchUpdate(notificationUpdateSql, new BatchPreparedStatementSetter() {
                            public void setValues(PreparedStatement statement, int index) throws SQLException {
                                statement.setString(1, notificationContacts.get(index).toString());
                                statement.setString(2, deActivatedNotificationContacts.get(index).toString());
                                statement.setInt(3, (Integer) notificationIndexes.get(index));
                            }

                            public int getBatchSize() {
                                return notificationIndexes.size();
                            }
                        });

                        //DTV-6955
                        String businessNotificationUpdateSql = "update trigger_notification_%s set contacts = ?, deactivated_contacts = ? where id = ? ";
                        String updateSql = String.format(businessNotificationUpdateSql, businessUUID.replaceAll("-","_"));
                        int[] updateResult1 = jdbcTemplate.batchUpdate(updateSql, new BatchPreparedStatementSetter() {
                            public void setValues(PreparedStatement statement, int index) throws SQLException {
                                statement.setString(1, notificationContacts.get(index).toString());
                                statement.setString(2, deActivatedNotificationContacts.get(index).toString());
                                statement.setInt(3, (Integer) notificationIndexes.get(index));
                            }

                            public int getBatchSize() {
                                return notificationIndexes.size();
                            }
                        });
                    }

                    scheduleContacts.add(new JSONArray(eachScheduledContacts));
                    deActivatedScheduleContacts.add(new JSONArray(eachDeActivatedScheduledContacts));
                    scheduleIndexes.add(scheduleId);
                }

                String scheduleUpdateSql = "update trigger_schedule set contacts = ?, deactivated_contacts = ? where id = ? ";
                int[] updateResult = jdbcTemplate.batchUpdate(scheduleUpdateSql, new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement statement, int index) throws SQLException {
                        statement.setString(1, scheduleContacts.get(index).toString());
                        statement.setString(2, deActivatedScheduleContacts.get(index).toString());
                        statement.setInt(3, (Integer) scheduleIndexes.get(index));
                    }

                    public int getBatchSize() {
                        return scheduleIndexes.size();
                    }
                });
            }

        } catch (Exception e) {
            logger.error("Exception at updateContactsInTriggerNotificationsAndTriggerSchedule. Msg {}", e.getMessage());
        }
        return result;
    }


    /**
     * updating the deactivated valid contacts in trigger_schedule
     * DTV-3633, Remove user from access after triggers set, and then re-add the access filter as well as them to the trigger
     *
     * @param triggerSchedule
     * @param deactivatedUserEmail
     * @return
     */
    public int updateDeactivateContactsInTriggerSchedule(List triggerSchedule, String deactivatedUserEmail) {

        int result = 0;
        List scheduleContacts = new ArrayList();
        List deActivatedScheduleContacts = new ArrayList();
        List scheduleIndexes = new ArrayList();
        try {

            if (triggerSchedule.size() > 0) {
                Iterator iterator = triggerSchedule.iterator();
                while (iterator.hasNext()) {
                    Map triggerScheduleMap = (Map) iterator.next();
                    logger.info("triggerScheduleMap  " + triggerScheduleMap);
                    List eachDeActivatedScheduledContacts = Utils.isNotNull(triggerScheduleMap.get("deactivated_contacts")) ? mapper.readValue(triggerScheduleMap.get("deactivated_contacts").toString(), ArrayList.class) : new ArrayList();
                    int scheduleId = Integer.parseInt(triggerScheduleMap.get("id").toString());

                    eachDeActivatedScheduledContacts.remove(deactivatedUserEmail);
                    deActivatedScheduleContacts.add(new JSONArray(eachDeActivatedScheduledContacts));
                    scheduleIndexes.add(scheduleId);
                }

                String scheduleUpdateSql = "update trigger_schedule set deactivated_contacts = ? where id = ? ";
                int[] updateResult = jdbcTemplate.batchUpdate(scheduleUpdateSql, new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement statement, int index) throws SQLException {
                        statement.setString(1, deActivatedScheduleContacts.get(index).toString());
                        statement.setInt(2, (Integer) scheduleIndexes.get(index));
                    }

                    public int getBatchSize() {
                        return scheduleIndexes.size();
                    }
                });
            }

        } catch (Exception e) {
            logger.error("Exception at updateDeactivateContactsInTriggerSchedule. Msg {}", e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to get token not available in notification table for given schedule id
     *
     * @param scheduleId
     * @param tokenLists
     * @param businessUUID
     * @return
     */
    public List checkNotificationTokensExistsForTheSchedule(int scheduleId, List tokenLists, String businessUUID) {
        try {
            //DTV-6955
            //String query = "select json_arrayagg(submission_token) tokens from trigger_notification where schedule_id = :scheduleId and submission_token in (:tokens)";
            String query = String.format("select json_arrayagg(submission_token) tokens from trigger_notification_%s where schedule_id = :scheduleId and submission_token in (:tokens)", businessUUID.replaceAll("-","_"));
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("tokens", tokenLists);
            params.addValue("scheduleId", scheduleId);
            Map tokenMap = namedParameterJdbcTemplate.queryForMap(query, params);
            logger.info("tokenMap   " + tokenMap);
            List token = (Utils.isNotNull(tokenMap) && tokenMap.containsKey("tokens") &&
                    Utils.isNotNull(tokenMap.get("tokens")) && Utils.notEmptyString(tokenMap.get("tokens").toString())) ?
                    mapper.readValue(tokenMap.get("tokens").toString(), ArrayList.class) : new ArrayList();

            if (token.size() > 0) {
                tokenLists.removeAll(token);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tokenLists;
    }

    /************************ participants *******************/

    /**
     * Method to create participant table using group business uuid
     *
     * @param listName
     * @param businessUUID
     * @return
     */
    public int createParticipantGroupData(final String listName, int createdBy, String businessUUID) {
        logger.debug("begin creating the participant group by business uuid");
        Integer particpantGroupid = 0;
        try {
            if (listName.length() > 0) {
                KeyHolder keyHolder = new GeneratedKeyHolder();
                final String query = String.format(CREATE_PARTICIPANT_GROUP, businessUUID.replaceAll("-", "_"));
                logger.debug("qury == " + query);

                JSONArray jsonArray = new JSONArray();
                jsonArray.put("id");
                jdbcTemplate.update(new PreparedStatementCreator() {
                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                        ps.setString(1, listName);
                        ps.setInt(2, 1);
                        ps.setString(3, Utils.generateUUID());
                        ps.setInt(4, createdBy);
                        ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                        ps.setString(6,jsonArray.toString());
                        return ps;
                    }
                }, keyHolder);
                particpantGroupid = keyHolder.getKey().intValue();

            } else {
                particpantGroupid = -1;
            }
            logger.debug("Participant group id ", particpantGroupid);
            logger.debug("end creating the participant group by business uuid");
        } catch (DataAccessException e) {
            logger.error(e.getRootCause().toString());
            particpantGroupid = -2;
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return particpantGroupid;
    }

    /**
     * Method to create Tag using business uuid and participant group data, return participantGroupId
     *
     * @param listName
     * @return
     */
    public int[] createParticipantGroupByBusinessUUID(String listName, String businessUUID, int createdBy) {
        logger.debug("begin creating the tags by  business uuid");
        //DTV-11303
        int[] resultArray = new int[2];
        Integer participantGroupId = createParticipantGroupData(listName, createdBy, businessUUID);
        resultArray[0] = participantGroupId;

        List idList = new ArrayList();
        idList.add(participantGroupId);
        try {
            Integer tagId = 0;
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            KeyHolder keyHolder = new GeneratedKeyHolder();
            final Timestamp timestamp = new Timestamp(cal.getTimeInMillis());
            final String query = String.format(CREATE_TAG_LIST, businessUUID.replaceAll("-", "_"));
            logger.debug("qury == " + query);
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, listName);
                    ps.setString(2, participantGroupId > 0 ? new JSONArray(idList.toString()).toString() : new JSONArray().toString());
                    ps.setInt(3, createdBy);
                    ps.setTimestamp(4, timestamp);
                    ps.setInt(5, 0);
                    ps.setTimestamp(6, timestamp);
                    ps.setString(7, Utils.generateUUID());
                    ps.setInt(8, 1);
                    return ps;
                }
            }, keyHolder);
            tagId = keyHolder.getKey().intValue();

            logger.debug("end creating the tag by business uuid");

            if (tagId > 0) {
                //updating usage table (lists) & sending mail after checking limit
                resultArray[1] = tagId;
            }

        } catch (DataAccessException e) {
            logger.error(e.getRootCause().toString());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return resultArray;
    }

    /**
     * Method to update tag name
     *
     * @param participantGroupId
     * @param businessUniqueId
     * @return
     */
    public void UpdateTagListNameByParticipantGroupId(Integer participantGroupId, String businessUniqueId, String tagName) {
        try {
            String qeury = String.format(UPDATE_TAG_LIST, businessUniqueId.replaceAll("-", "_"));
            int tagUniqueId = jdbcTemplate.update(qeury, new Object[]{tagName, participantGroupId});
            logger.info("result {}", tagUniqueId);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * method to create participants
     *
     * @param headerList
     * @param dataList
     * @param dataTypeList
     * @param participantGroupId
     * @param businessUniqueId
     */
    public void createParticipants(List headerList, List dataList, List dataTypeList, List phiDataList, int participantGroupId, String businessUniqueId, int createdBy) {
        String participantGroupUniqueId = this.getParticipantGroupUUIDById(participantGroupId, businessUniqueId);
        int result = 0;
        logger.info("begin create participant data");
        try {
            String table_name = String.format("participant_%s", (participantGroupUniqueId.replaceAll("-", "_")));

            final List MetaDataExistList = this.getMetaDataListByGroupID(table_name);
            if (MetaDataExistList.contains("DATE")) {
                int dateIndex = MetaDataExistList.indexOf("DATE");
                String eachDate = dataList.get(dateIndex).toString().length() > 0 ? dataList.get(dateIndex).toString().trim() : "";
                logger.info("inputDate format " + eachDate);
                if (Utils.notEmptyString(eachDate)) {
                    String formattedDate = Utils.getValidDateTime(eachDate, true, "-");
                    if (Utils.notEmptyString(formattedDate)) {
                        dataList.remove(dateIndex);
                        dataList.add(dateIndex, formattedDate);
                        logger.info("formatted Date " + formattedDate);
                    }
                }
            }

            final String query = String.format(CREATE_PARTICIPANTS, participantGroupUniqueId.replaceAll("-", "_"));
            logger.info("qury == " + query);
            final String participantUUID = Utils.generateUUID();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    //check if meta data exist then use that for add individual contact
//                    List data = dataList;
                    int dataSize = dataList.size();
                    int cnt = 0;

                    List metaDataArr = MetaDataExistList;
                    if (metaDataArr.size() > 0) {
                        if (metaDataArr.size() < dataSize) {
                            for (cnt = metaDataArr.size(); cnt < dataSize; cnt++) {
                                metaDataArr.add("STRING");
                                insertExtraFieldToParticipant(headerList.get(cnt).toString(), table_name);
                            }
                        } else if (metaDataArr.size() > dataSize) {
                            metaDataArr = dataTypeList;
                            removeExtraFieldToParticipant(headerList, table_name); //TODO
                        }
                    } else {
                        // first entry of participant table
                        metaDataArr = dataTypeList;
                    }
                    //set phi data
                    if (phiDataList.size() == 0 || Utils.isNull(phiDataList)) {
                        Iterator metadataItr = metaDataArr.iterator();
                        while (metadataItr.hasNext()) {
                            String eachMetadata = (String) metadataItr.next();
                            if (eachMetadata.equalsIgnoreCase("NAME") || eachMetadata.equalsIgnoreCase("EMAIL") || eachMetadata.equalsIgnoreCase("PHONE") || eachMetadata.equalsIgnoreCase("SURNAME") || eachMetadata.equalsIgnoreCase("MIDDLENAME")) {
                                phiDataList.add(true);
                            } else {
                                phiDataList.add(false);
                            }
                        }
                    }
                    PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, new JSONArray(dataList).toString());
                    ps.setString(2, new JSONArray(headerList).toString());
                    ps.setString(3, new JSONArray(metaDataArr).toString().toUpperCase());
                    ps.setString(4, new JSONArray(phiDataList).toString());
                    ps.setString(5, null);
                    ps.setString(6, participantUUID);
                    ps.setInt(7, createdBy);
                    ps.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                    return ps;
                }
            });

            logger.info("participant data {}", result);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        }
        logger.info("end create participant");
    }

    /**
     * Method to get distinct meta data type for given groupUUID
     *
     * @param tableName
     * @return
     */
    public List getMetaDataListByGroupID(String tableName) {
        logger.info("start getMetaDataListByGroupID");
        List resultList = new ArrayList();
        try {
            String sql = String.format(GET_PARTICIPANT_METADATA, tableName);
            List resultData = jdbcTemplate.queryForList(sql);
            if (resultData.size() > 0) {
                Map partMetaData = (Map) resultData.get(0);
                JSONArray metaDataArr = new JSONArray(partMetaData.get("meta_data").toString());
                for (int i = 0; i < metaDataArr.length(); i++) {
                    resultList.add(metaDataArr.get(i));
                }
                logger.info("meta data list == " + resultList.toString());
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("end getMetaDataListByGroupID");
        return resultList;
    }

    /**
     * function used for inserting fields into all participants during metadata questions processing
     *
     * @param header
     * @param tableName
     */
    public void insertExtraFieldToParticipant(String header, String tableName) {
        logger.info("begin insert extra field to participant");
        try {
            String recordsSql = String.format(GET_PARTICIPANT_DATA, tableName);
            List records = jdbcTemplate.queryForList(recordsSql);
            if (records.size() > 0) {
                Map firstRecord = (Map) records.get(0);
                int dataSize = mapper.readValue(firstRecord.get("data").toString(), ArrayList.class).size();
                boolean phiData = false;
                String updateSql = "update " + tableName + " set data = JSON_SET(data,'$[" + dataSize + "]',\"\"),  header = JSON_SET(header,'$[" + dataSize + "]',\"" + header + "\"),  meta_data = JSON_SET(meta_data,'$[" + dataSize + "]',\"STRING\"), phi_data = JSON_SET(phi_data,'$[" + dataSize + "]', " + phiData + ")";
                int updateResult = jdbcTemplate.update(updateSql);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end insert extra field to participant");
    }

    /**
     * Method to check if tag name is exists or not
     *
     * @param tagName
     * @param businessUniqueId
     * @return
     */
    public boolean checkIfTagNameExists(String tagName, String businessUniqueId) {
        logger.debug("check tag name exists");
        List result = new ArrayList();
        try {
            String query = String.format(IS_LIST_NAME_EXIST, businessUniqueId.replaceAll("-", "_"));
            result = jdbcTemplate.queryForList(query, new Object[]{tagName.toUpperCase()});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("check tag name result: " + result);
        if (result.size() > 0) {
            return true;
        }
        return false;
    }


    public List getContactsByTokens(String businessUUID, List tokens) {
        List result = new ArrayList();
        try {
            if (tokens.size() > 0) {
                String tableName = String.format("token_%s", businessUUID.replaceAll("-", "_"));
                String query = "select dynamic_id, survey_id, participant_id, participant_group_id, tag_id, name, email, phone, created_by, created_time, last_action, last_action_time, type, dynamic_token_id, metadata_time from " + tableName + " where dynamic_token_id in (:tokens)";
                MapSqlParameterSource params = new MapSqlParameterSource();
                params.addValue("tokens", tokens);
                result = namedParameterJdbcTemplate.queryForList(query, params);
                logger.info("result  " + result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to get participant groupid by tagid
     *
     * @param tagId
     * @param businessUUID
     * @return
     */
    public String getParticipantGroupUUIdByParticipantGrpID(int tagId, int participantGroupId, String businessUUID) {
        String resultData = "";
        try {
            logger.info("getParticipantGroupUUIdByParticipantGrpID tagid --  " + tagId);
            final String table_name = String.format("participant_group_%s", businessUUID.replace("-", "_"));
            businessUUID = businessUUID.replace("-", "_");
            String tag_tableName = String.format("tag_%s", businessUUID);
            String sql = "select participant_groups  from " + tag_tableName + " where id= ?";
            logger.info(sql);
            List result = jdbcTemplate.queryForList(sql, new Object[]{tagId});
            if (result.size() > 0) {
                Map dataResult = (Map) result.get(0);
                String participantGrps = dataResult.get("participant_groups").toString();
                logger.info(participantGrps.toString());
                JSONArray jsonArr = new JSONArray(participantGrps);
                for (int i = 0; i < jsonArr.length(); i++) {
                    int eachParticipantGrpId = Integer.parseInt(jsonArr.get(i).toString());
                    if (eachParticipantGrpId == participantGroupId) {
                        logger.info("participant group id: {}", eachParticipantGrpId);
                        String sqlSelect = "Select participant_group_uuid from " + table_name + " where id = ? and active = 1";
                        resultData = jdbcTemplate.queryForObject(sqlSelect, new Object[]{eachParticipantGrpId}, String.class);
                    }
                }
            }
            logger.info("getParticipantGroupUUIdByParticipantGrpID groupId {}", resultData);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultData;

    }

    /**
     * Method to get participants data by id
     *
     * @param groupUUID
     * @return
     */
    public List getParticipantDataById(int id, String groupUUID) {
        List resultData = null;
        try {
            logger.info("start get participant id " + id);
            String participantTableName = String.format("participant_%s", groupUUID.replace("-", "_"));
            String sql = "select * from " + participantTableName + " where id= ?";
            //logger.info(sql);
            resultData = jdbcTemplate.queryForList(sql, new Object[]{id});
            //logger.info("end get participant id {}",resultData.toString());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultData;
    }

    /**
     * Method to create participant table if not exists
     *
     * @param participantGroupUniqeId
     * @return
     */
    public int createParticipantTable(String participantGroupUniqeId) {
        int tableCreated = 1;
        String tableName = String.format("participant_%s", participantGroupUniqeId.replaceAll("-", "_"));
        logger.debug("begin creating participant table ", participantGroupUniqeId);
        try {
            String createSql = "CREATE TABLE " + tableName + " (`id` int(10) NOT NULL AUTO_INCREMENT," +
                    "`data` json NULL, `header` json NULL,`meta_data` json NULL, `phi_data` json NULL, question_metadata json DEFAULT NULL, participant_uuid varchar(100) NOT NULL," +
                    "`created_by` int(10) NOT NULL," +
                    "`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                    "`modified_by` int(10) NULL," +
                    "`modified_time` timestamp NULL," +
                    " PRIMARY KEY (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8";
            tableCreated = jdbcTemplate.update(createSql);
            logger.debug("Create table result {}", tableCreated);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end creating participant table");
        return tableCreated;
    }

    /**
     * function used for remove fields from all participants during metadata questions processing
     *
     * @param header
     * @param tableName
     */
    public void removeExtraFieldToParticipant(List header, String tableName) {
        logger.info("begin insert extra field to participant");
        try {
            String recordsSql = String.format("select header from %s", tableName);
            List records = jdbcTemplate.queryForList(recordsSql);
            if (records.size() > 0) {
                Map firstRecord = (Map) records.get(0);
                List existingHeaderList = mapper.readValue(firstRecord.get("header").toString(), ArrayList.class);
                // get header that needs to be removed
                existingHeaderList.removeAll(header);

                for (int i = 0; i < existingHeaderList.size(); i++) {
                    String eachHeaderToRemove = (String) existingHeaderList.get(i);
                    try {
                        String keyIndexSql = "SELECT json_search(upper(header), 'one', upper(?)) as 'Header' FROM " + tableName +
                                " where json_search(upper(header), 'one', upper(?)) is not null limit 1"; // to avoid null value
                        Map resultMap = jdbcTemplate.queryForMap(keyIndexSql, new Object[]{eachHeaderToRemove, eachHeaderToRemove});
                        if (resultMap != null && resultMap.containsKey("Header")) {
                            String hdrIndex = resultMap.get("Header").toString();
                            String updateSql = "update " + tableName + " set data = json_remove(data," + hdrIndex + "),  header = json_remove(header," + hdrIndex + "),  meta_data = JSON_remove(meta_data," + hdrIndex + ") where id > 0";
                            int updateResult = jdbcTemplate.update(updateSql);
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end insert extra field to participant");
    }

    /**
     * Get business name by business uuid
     *
     * @param businessUUID
     * @return
     */
    public String getBusinessNameByUniqueId(String businessUUID) {
        logger.debug("begin get business name");
        String businessName = "";
        try {
            businessName = jdbcTemplate.queryForObject(GET_BUSINESS_NAME, new Object[]{businessUUID}, String.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.debug("end get business name");
        return businessName;
    }

    /**
     * Return survey details by survey uuid for business
     *
     * @param programUUID
     * @param businessUUID
     * @return
     */
    public Map returnProgramByUUID(String programUUID, String businessUUID) {
        Map programMap = new HashMap();
        try {
            String businessUUIDParsed = businessUUID.replace("-", "_");
            String tableName = String.format("surveys_%s", businessUUIDParsed);
            String surveySql = String.format(GET_SURVEY_BY_UUID_TEMPLATE, new Object[]{tableName});
            List programs = jdbcTemplate.queryForList(surveySql, new Object[]{programUUID});
            if (programs.size() > 0) {
                programMap = (Map) programs.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return programMap;
    }

    /**
     * function to return surveyStatus by userToken
     *
     * @param userToken
     * @return surveyStatus
     */

    public List getSurveyDataByUserToken(String userToken, String businessUUID) {
        List result = new ArrayList();
        String tableName = String.format("token_%s", businessUUID.replaceAll("-", "_"));
        try {
            String query = String.format(GET_SURVEY_DATA_BY_USERTOKEN, tableName);
            result = jdbcTemplate.queryForList(query, userToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Method to get submission token for surveyuuid
     *
     * @param surveyUUID
     * @return
     */
    public List getTokensOfSurveyId(String surveyUUID) {
        List feedbackTokens = new ArrayList();
        try {
            String surveyUUIDHyphenReplaced = surveyUUID.replace("-", "_");
            String eventTableName = new StringBuilder("event_").append(surveyUUIDHyphenReplaced).toString();
            eventTableName = "`" + eventTableName + "`";
            String sql = "select submission_token, created_time from " + eventTableName + " order by created_time desc";
            feedbackTokens = jdbcTemplate.queryForList(sql);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbackTokens;
    }

    /**
     * Method to get question informations
     *
     * @param questionUUID
     * @param businessUUID
     * @return
     */
    public Map getQuestionDataByQuestionUUIDByBusinessId(String questionUUID, String businessUUID) {
        Map questionMap = new HashMap();
        try {
            String businessUUIDParsed = businessUUID.replace("-", "_");
            String tableName = String.format("questions_%s", businessUUIDParsed);
            String questionSql = String.format(GET_QUESTION_BY_UUID_TEMPLATE, new Object[]{tableName});
            List questions = jdbcTemplate.queryForList(questionSql, new Object[]{questionUUID});
            if (questions.size() > 0) {
                questionMap = (Map) questions.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionMap;
    }

    /**
     * Retrieve key metric by key metric id
     *
     * @param keyMetricId
     * @param businessUUID
     * @return
     */
    public Map retrieveJsonKeyMetricById(int keyMetricId, String businessUUID) {
        Map keyMetricMap = new HashMap();
        try {
            String tableName = String.format("key_metric_%s", businessUUID);
            String GET_KEY_METRIC_BY_ID = String.format(GET_KEY_METRIC_BY_ID_TEMPLATE, new Object[]{tableName});
            List keyMetricsList = jdbcTemplate.queryForList(GET_KEY_METRIC_BY_ID, new Object[]{keyMetricId});
            if (keyMetricsList.size() > 0) {
                keyMetricMap = (Map) keyMetricsList.get(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return keyMetricMap;
    }

    /**
     * Get multi choice options table data for question id
     *
     * @param eachQuestionId
     * @param businessUUID
     * @return
     */
    public List getMultiChoiceDataForQuestionId(int eachQuestionId, String businessUUID) {
        List choiceList = new ArrayList();
        try {

            String businessUUIDHyphenReplaced = businessUUID.replace("-", "_");
            String multiChoiceTableName = String.format("multiple_choice_options_%s", businessUUIDHyphenReplaced);

            String singleChoiceSql = "select * from " + multiChoiceTableName + " where question_id = ?";
            choiceList = jdbcTemplate.queryForList(singleChoiceSql, new Object[]{eachQuestionId});

        } catch (Exception e) {
            e.printStackTrace();
        }
        return choiceList;
    }

    /**
     * method to get participant header email and name column details by submission token
     *
     * @param token
     * @param businessUniqueId
     * @return
     */
    public Map getParticipantEmailNameColByToken(String token, String businessUniqueId) {
        Map participantHeaderMap = new HashMap();
        try {
            String businessReplaced = businessUniqueId.replaceAll("-", "_");
            String participantgroupTableName = String.format("participant_group_%s", businessReplaced);
            String tokenTableName = String.format("token_%s", businessReplaced);
            String sql = "select participant_group_uuid from " + participantgroupTableName + " where id IN (SELECT participant_group_id FROM " + tokenTableName + " where dynamic_token_id = ?)";
            String participantGroupUid = jdbcTemplate.queryForObject(sql, new Object[]{token}, String.class);
            if (participantGroupUid.length() > 0) {
                String headerSql = "SELECT distinct header, meta_data FROM participant_" + participantGroupUid.replaceAll("-", "_");
                List result = jdbcTemplate.queryForList(headerSql);
                Map headerMap = result.isEmpty() ? new HashMap() : (Map) result.get(0);

                if (headerMap != null && headerMap.containsKey("header")) {
                    List<String> headerList = headerMap.get("header") != null ? mapper.readValue(headerMap.get("header").toString(), ArrayList.class) : new ArrayList();
                    List metaDataList = headerMap.get("meta_data") != null ? mapper.readValue(headerMap.get("meta_data").toString(), ArrayList.class) : new ArrayList();

                    String nameCol = (metaDataList.indexOf("NAME") > -1) ? headerList.get(metaDataList.indexOf("NAME")) : "";
                    String emailCol = (metaDataList.indexOf("EMAIL") > -1) ? headerList.get(metaDataList.indexOf("EMAIL")) : "";

                    /*participantHeaderMap.put("name", StringUtils.capitalize(nameCol) + Constants.DELIMITER + "NAME");
                    participantHeaderMap.put("email", StringUtils.capitalize(emailCol) + Constants.DELIMITER + "EMAIL" );*/
                      participantHeaderMap.put("name", (nameCol) + Constants.DELIMITER + "NAME");
                    participantHeaderMap.put("email", (emailCol) + Constants.DELIMITER + "EMAIL" );

                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return participantHeaderMap;
    }



    /**
     * Method to create survey filter
     *
     * @param surveyFilterBean
     * @param businessUUID
     * @param userId
     * @param surveyId
     * @param roleId
     * @return
     */
    public String createSurveyFilter(final SurveyFilterBean surveyFilterBean, String businessUUID, final int userId, final int surveyId, int roleId, Map readableQuery) {
        logger.info("Create survey filter");
        String filterUUIDToReturn = "";
        int filterId = 0;
        final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String createdByUUID = surveyFilterBean.getCreatedBy();
        String tableName = String.format("survey_filters_%s", businessUUID.replace("-", "_"));
        final String temporaryFilter = Utils.isNotNull(surveyFilterBean.getTemporaryFilter()) ? surveyFilterBean.getTemporaryFilter() : "0";

        final JSONObject jsonObject = new JSONObject(readableQuery);
        final String filterUUID = Utils.generateUUID();
        filterUUIDToReturn = filterUUID;
        final String insertSurveyFilters = "insert into " + tableName + " (filter_name, filter_query, readable_query,survey_id, created_by, modified_by, created_time, survey_filter_uuid, temporary_filter) values (?,?,?,?,?,?,?,?,?)";
        try {
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(insertSurveyFilters, Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, surveyFilterBean.getFilterName());
                    ps.setString(2, surveyFilterBean.getFilterQuery());
                    ps.setString(3, jsonObject.toString());
                    ps.setInt(4, surveyId);
                    ps.setInt(5, userId);
                    ps.setInt(6, 0);
                    ps.setTimestamp(7, timestamp);
                    ps.setString(8, filterUUID);
                    ps.setString(9, temporaryFilter);
                    return ps;
                }
            }, keyHolder);

            filterId = keyHolder.getKey().intValue();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filterUUIDToReturn;
    }


    /**
     * Function to update a filter based upon the contents in the SurveyFilterBean
     *
     * @param surveyFilterBean
     * @param surveyFilterBean
     * @param businessUUID
     * @param userId
     * @return
     */
    public int updateSurveyFilter(String surveyFilterUUID, SurveyFilterBean surveyFilterBean, String businessUUID, int userId, Map readableQuery) {
        int result = 0;
        try {
            logger.info("begin update the filter");
            String tableName = String.format("survey_filters_%s", businessUUID.replace("-", "_"));
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String filterName = surveyFilterBean.getFilterName();
            String filterQuery = surveyFilterBean.getFilterQuery();
            String updateSurveyFiltersById = "update " + tableName + " set filter_name = ?, filter_query = ?, readable_query = ?, modified_time = ?, modified_by = ? where survey_filter_uuid = ?";
            result = jdbcTemplate.update(updateSurveyFiltersById, new Object[]{filterName, filterQuery, new JSONObject(readableQuery).toString(), timestamp, userId, surveyFilterUUID});
            logger.info("update result " + result);
            logger.info("end update the filter");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }



    /**
     * Function to delete business based upon filterId
     *
     * @param surveyFilterUUID
     * @return
     */
    public int deleteSurveyFilter(String surveyFilterUUID, String businessUUID) {
        int result = 0;
        logger.info("filter id " + surveyFilterUUID);
        try {
            String tableName = String.format("survey_filters_%s", businessUUID.replace("-", "_"));
            String deleteSql = "delete from " + tableName + " where survey_filter_uuid = ?";
            result = jdbcTemplate.update(deleteSql, new Object[]{surveyFilterUUID});

        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.getMessage());
            String message = e.getMessage();
            result = message.contains("Cannot delete or update a parent row") ? -1 : 0;
        }
        return result;
    }


    /**
     * updating active survey triggers usage count and sending email after reaching limit
     * addCount will be +1 while trigger status is active, addCount = -1 when deleting trigger & status is inactive
     *
     * @param businessId
     * @param addCount
     */
    public void updateTriggersUsageCount(int businessId, int addCount) {
        logger.info("start update active triggers usage count");

        try {
            //if usage
            String updateSql = "update business_usage set triggers = ? where business_id = ?";
            int result = jdbcTemplate.update(updateSql, addCount, businessId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end update active triggers usage count");
    }


    /**
     * updating emails usage count for triggers
     *
     * @param businessId
     * @param addCount
     */
    public void updateTriggersEmailUsageCount(int businessId, int addCount) {
        logger.info("start update emails usage count");
        try {
            String updateSql = "update business_usage set emails = ? where business_id = ?";
            int result = jdbcTemplate.update(updateSql, addCount, businessId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End update emails usage count");
    }

    /**
     * function to get usage email flag map from business_usage table
     *
     * @param businessId
     * @return
     */
    public Map getUsageMailFlagByBusinessId(int businessId) {
        Map usageMailFlagMap = new HashMap();
        logger.info("Start  get all active triggers  by business id");
        try {
            String sql = "select usage_email_flag from business_usage where business_id = ?";
            List usageMailList = jdbcTemplate.queryForList(sql, new Object[]{businessId});
            if (usageMailList.size() > 0) {
                Map usageMap = (Map) usageMailList.get(0);
                usageMailFlagMap = Utils.isNotNull(usageMap.get("usage_email_flag")) ? mapper.readValue(usageMap.get("usage_email_flag").toString(), HashMap.class) : new HashMap();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        //logger.info("End get all active triggers  by business id");
        return usageMailFlagMap;
    }


    /**
     * Function to get business usage map by business Id
     *
     * @param businessId
     * @return
     */

    public Map getBusinessUsageByBusinessId(Integer businessId) {
        Map businessUsageMap = new HashMap();
        logger.info("begin retrieving business usage by business id");
        try {

            String sql = "select business_usage_id, business_id, users, programs, responses, metrics, filters, triggers, emails, sms, api, lists, recipients, data_retention, current_month_start, current_month_end, usage_email_flag from business_usage where business_id = ?";
            List usageList = jdbcTemplate.queryForList(sql, new Object[]{businessId});
            if (usageList.size() > 0) {
                Map usageMap = (Map) usageList.get(0);
                int users = Utils.isNotNull(usageMap.get("users")) && Utils.notEmptyString(usageMap.get("users").toString()) ? Integer.parseInt(usageMap.get("users").toString()) : 0;
                int programs = Utils.isNotNull(usageMap.get("programs")) && Utils.notEmptyString(usageMap.get("programs").toString()) ? Integer.parseInt(usageMap.get("programs").toString()) : 0;
                int responses = Utils.isNotNull(usageMap.get("responses")) && Utils.notEmptyString(usageMap.get("responses").toString()) ? Integer.parseInt(usageMap.get("responses").toString()) : 0;
                int metrics = Utils.isNotNull(usageMap.get("metrics")) && Utils.notEmptyString(usageMap.get("metrics").toString()) ? Integer.parseInt(usageMap.get("metrics").toString()) : 0;
                int filters = Utils.isNotNull(usageMap.get("filters")) && Utils.notEmptyString(usageMap.get("filters").toString()) ? Integer.parseInt(usageMap.get("filters").toString()) : 0;
                int triggers = Utils.isNotNull(usageMap.get("triggers")) && Utils.notEmptyString(usageMap.get("triggers").toString()) ? Integer.parseInt(usageMap.get("triggers").toString()) : 0;
                int emails = Utils.isNotNull(usageMap.get("emails")) && Utils.notEmptyString(usageMap.get("emails").toString()) ? Integer.parseInt(usageMap.get("emails").toString()) : 0;
                int sms = Utils.isNotNull(usageMap.get("sms")) && Utils.notEmptyString(usageMap.get("sms").toString()) ? Integer.parseInt(usageMap.get("sms").toString()) : 0;
                int api = Utils.isNotNull(usageMap.get("api")) && Utils.notEmptyString(usageMap.get("api").toString()) ? Integer.parseInt(usageMap.get("api").toString()) : 0;
                int lists = Utils.isNotNull(usageMap.get("lists")) && Utils.notEmptyString(usageMap.get("lists").toString()) ? Integer.parseInt(usageMap.get("lists").toString()) : 0;
                int recipients = Utils.isNotNull(usageMap.get("recipients")) && Utils.notEmptyString(usageMap.get("recipients").toString()) ? Integer.parseInt(usageMap.get("recipients").toString()) : 0;
                int dataRetention = Utils.isNotNull(usageMap.get("data_retention")) && Utils.notEmptyString(usageMap.get("data_retention").toString()) ? Integer.parseInt(usageMap.get("data_retention").toString()) : 0;

                businessUsageMap.put("usersUsageCount", users);
                businessUsageMap.put("programsUsageCount", programs);
                businessUsageMap.put("responsesUsageCount", responses);
                businessUsageMap.put("metricsUsageCount", metrics);
                businessUsageMap.put("filtersUsageCount", filters);
                businessUsageMap.put("triggersUsageCount", triggers);
                businessUsageMap.put("emailsUsageCount", emails);
                businessUsageMap.put("smsUsageCount", sms);
                businessUsageMap.put("apiUsageCount", api);
                businessUsageMap.put("listsUsageCount", lists);
                businessUsageMap.put("recipientsUsageCount", recipients);
                businessUsageMap.put("dataRetentionUsageCount", dataRetention);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end retrieving business usage by business id");
        return businessUsageMap;
    }


    /**
     * Function to list configurations based on the business id
     *
     * @param businessId
     * @return
     */

    public List getBusinessConfigurationsByBusinessId(Integer businessId) {
        List result = new ArrayList();
        logger.info("begin retrieving configs by business id");
        try {
            String notification = "0";
            String sql = "select business_config_id, business_id, email, sms, qrcode, shareable_link, text_analytics, business_config_uuid, logic, respondent, segment, kiosk, metadata_question, mobile_app, emotion_analysis, intent_analysis, trigger_toggle, advanced_schedule, english_toggle, arabic_toggle, multi_surveys, bitly, web_hooks, preferred_metric, category_analysis, advanced_text_analytics, no_of_users, no_of_active_programs, no_of_responses, no_of_metrics, no_of_emails_sent, no_of_sms, no_of_api, no_of_filters, no_of_triggers, data_retention, no_of_lists, no_of_recipients, focus_metric from business_configurations where business_id = ?";
            result = jdbcTemplate.queryForList(sql, new Object[]{businessId});

            List businessConfigurationList = new ArrayList();
            Iterator<Map> iterator = result.iterator();
            while (iterator.hasNext()) {
                Map businessConfigMap = iterator.next();
                Object id = businessConfigMap.remove("business_config_uuid");
                businessConfigMap.put("businessConfigId", id);
                Object bizId = businessConfigMap.remove("business_id");
                businessConfigMap.put("businessId", bizId);
                Object sharableLink = businessConfigMap.remove("shareable_link");
                businessConfigMap.put("sharableLink", sharableLink);
                Object textAnalytics = businessConfigMap.remove("text_analytics");
                businessConfigMap.put("textAnalytics", textAnalytics);
                Object metadataQuestion = businessConfigMap.remove("metadata_question");
                businessConfigMap.put("metadataQuestion", metadataQuestion);
                Object mobileApp = businessConfigMap.remove("mobile_app");
                businessConfigMap.put("mobileApp", mobileApp);
                Object emotionAnalysis = businessConfigMap.remove("emotion_analysis");
                businessConfigMap.put("emotionAnalysis", emotionAnalysis);
                Object intentAnalysis = businessConfigMap.remove("intent_analysis");
                businessConfigMap.put("intentAnalysis", intentAnalysis);
                Object trigger = businessConfigMap.remove("trigger_toggle");
                businessConfigMap.put("trigger", trigger);
                Object advancedSchedule = businessConfigMap.remove("advanced_schedule");
                businessConfigMap.put("advancedSchedule", advancedSchedule);
                Object english = businessConfigMap.remove("english_toggle");
                businessConfigMap.put("english", english);
                Object arabic = businessConfigMap.remove("arabic_toggle");
                businessConfigMap.put("arabic", arabic);
                Object multiSurveys = businessConfigMap.remove("multi_surveys");
                businessConfigMap.put("multiSurveys", multiSurveys);
                Object webHooks = businessConfigMap.remove("web_hooks");
                businessConfigMap.put("webHooks", webHooks);
                Object preferredMetric = businessConfigMap.remove("preferred_metric");
                businessConfigMap.put("preferredMetric", preferredMetric);
                businessConfigMap.put("notification", notification);
                Object categoryAnalysis = businessConfigMap.remove("category_analysis");
                businessConfigMap.put("categoryAnalysis", categoryAnalysis);
                Object advancedTextAnalytics = businessConfigMap.remove("advanced_text_analytics");
                businessConfigMap.put("advancedTextAnalytics", advancedTextAnalytics);
                Object noOfUsers = businessConfigMap.remove("no_of_users");
                businessConfigMap.put("noOfUsers", noOfUsers);
                Object noOfActivePrograms = businessConfigMap.remove("no_of_active_programs");
                businessConfigMap.put("noOfActivePrograms", noOfActivePrograms);
                Object noOfResponses = businessConfigMap.remove("no_of_responses");
                businessConfigMap.put("noOfResponses", noOfResponses);
                Object noOfMetrics = businessConfigMap.remove("no_of_metrics");
                businessConfigMap.put("noOfMetrics", noOfMetrics);
                Object noOfEmailsSent = businessConfigMap.remove("no_of_emails_sent");
                businessConfigMap.put("noOfEmailsSent", noOfEmailsSent);
                Object noOfSms = businessConfigMap.remove("no_of_sms");
                businessConfigMap.put("noOfSms", noOfSms);
                Object noOfApi = businessConfigMap.remove("no_of_api");
                businessConfigMap.put("noOfApi", noOfApi);
                Object noOfFilters = businessConfigMap.remove("no_of_filters");
                businessConfigMap.put("noOfFilters", noOfFilters);
                Object noOfTriggers = businessConfigMap.remove("no_of_triggers");
                businessConfigMap.put("noOfTriggers", noOfTriggers);
                Object dataRetention = businessConfigMap.remove("data_retention");
                businessConfigMap.put("dataRetention", dataRetention);
                Object noOfLists = businessConfigMap.remove("no_of_lists");
                businessConfigMap.put("noOfLists", noOfLists);
                Object noOfRecipients = businessConfigMap.remove("no_of_recipients");
                businessConfigMap.put("noOfRecipients", noOfRecipients);
                Object focusMetric = businessConfigMap.remove("focus_metric");
                businessConfigMap.put("focusMetric", focusMetric);
                businessConfigurationList.add(businessConfigMap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end retrieving configs by business id");
        return result;
    }


    /**
     * function to update usage email flag map by businessId
     *
     * @param businessId
     * @param usageMailFlagMap
     * @return
     */
    public void updateUsageMailFlagByBusinessId(int businessId, Map usageMailFlagMap) {
        logger.info("Start  update usage mail flag  by business id");
        try {
            JSONObject jsonObject = new JSONObject(usageMailFlagMap);
            String sql = "update business_usage set usage_email_flag = ? where business_id = ?";
            int result = jdbcTemplate.update(sql, new Object[]{jsonObject.toString(), businessId});

        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("End update usage mail flag  by business id");
    }


    /**
     * Function to get king user based on businessId
     *
     * @param businessId
     * @return
     */

    public Map getKingUserByBusinessId(Integer businessId) {
        Map userMap = new HashMap();
        try {
            List resultList = jdbcTemplate.queryForList(GET_KING_USER_BY_BUSINESS_ID, new Object[]{businessId});
            if (resultList.size() > 0) {
                userMap = (Map) resultList.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userMap;
    }

    /**
     * Function to check hippa is enabled for business
     *
     * @param businessId
     * @return
     */

    public boolean isHippaEnableForBusiness(Integer businessId) {
        boolean isEnabled = false;
        logger.info("begin isHippaEnableForBusiness");
        try {
            List businessConfigList = jdbcTemplate.queryForList(GET_BUSINESS_CONFIGRUATIONS_BY_BUSINESSID, new Object[]{businessId});
            if (businessConfigList != null && businessConfigList.size() > 0) {
                Map businessConfigMap = (Map) businessConfigList.get(0);
                isEnabled = (businessConfigMap.containsKey("hippa") && Utils.isNotNull(businessConfigMap.get("hippa"))) ? businessConfigMap.get("hippa").equals("1") ? true : false : false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end isHippaEnableForBusiness");
        return isEnabled;
    }

    /**
     * method to get participant pesonal headers like email, phone, surname, middlenae and name column details by submission token
     *
     * @param token
     * @param businessUniqueId
     * @return
     */
    public List getParticipantPersonalInfoByToken(String token, String businessUniqueId) {
        List personalInfoColName = new ArrayList();
        try {
            String businessReplaced = businessUniqueId.replaceAll("-", "_");
            String participantGroupTableName = String.format("participant_group_%s", businessReplaced);
            String tokenTableName = String.format("token_%s", businessReplaced);
            String sql = "select participant_group_uuid from " + participantGroupTableName + " where id IN (SELECT participant_group_id FROM " + tokenTableName + " where dynamic_token_id = ?)";
            String participantGroupUid = jdbcTemplate.queryForObject(sql, new Object[]{token}, String.class);
            if (participantGroupUid.length() > 0) {
                String headerSql = "SELECT distinct header, upper(meta_data) as meta_data, phi_data FROM participant_" + participantGroupUid.replaceAll("-", "_");
                Map headerMap = jdbcTemplate.queryForMap(headerSql);
                if (headerMap != null && headerMap.containsKey("header")) {
                    List<String> headerList = headerMap.get("header") != null ? mapper.readValue(headerMap.get("header").toString(), ArrayList.class) : new ArrayList();
                    //List metaDataList = headerMap.get("meta_data") != null ? mapper.readValue(headerMap.get("meta_data").toString(), ArrayList.class) : new ArrayList();
                    List<Boolean> phiDataList = headerMap.get("phi_data") != null ? mapper.readValue(headerMap.get("phi_data").toString(), ArrayList.class) : new ArrayList();
                    for (int i = 0; i < headerList.size(); i++) {
                        boolean eachPhiData = phiDataList.get(i);
                        if (eachPhiData) {
                            String eachHeader = headerList.get(i);
                            personalInfoColName.add(eachHeader);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return personalInfoColName;
    }


    /**
     * Function to get userIds by userEmails
     *
     * @param businessId
     * @return
     */
    public List getUserIdsByEmailIds(List userEmails, int businessId) {
        List userIds = new ArrayList();
        try {
            String sql = "select u.user_id, u.country_id, u.full_name, u.first_name, u.last_name, u.username, u.user_db, u.user_email, u.user_uuid, u.password, u.phone, u.trial_flag, u.complete_social_signup, u.confirmation_status, u.password_update_count, u.created_by, u.created_time, u.modified_time, u.modified_by, u.user_status, u.eula_status, u.push_notification, u.thoughtful, u.title, u.cs_start_date, u.cs_end_date, u.user_role, u.alias_email, u.last_login_time, a.account_id, a.account_type, a.signup_type_id, a.account_user_id, a.account_business_id, a.account_role_id, a.account_country_id, a.account_emp_range from users  as u  left join account as a on u.user_id = a.account_user_id where account_business_id = :businessId and user_email in (:userEmails)";
            SqlParameterSource tokenParameterSource = new MapSqlParameterSource()
                    .addValue("businessId", businessId)
                    .addValue("userEmails", userEmails);
            List usersList = namedParameterJdbcTemplate.queryForList(sql, tokenParameterSource);

            if (usersList.size() > 0) {
                Iterator iterator = usersList.iterator();
                while (iterator.hasNext()) {
                    Map eachUser = (Map) iterator.next();
                    int eachUserId = Utils.isNotNull(eachUser.get("user_id")) ? Integer.parseInt(eachUser.get("user_id").toString()) : 0;
                    if (eachUserId > 0) {
                        userIds.add(eachUserId);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userIds;
    }


    /**
     * method to create notification data in trigger_notifications_buuid table for immediate & delayed  push notifications
     *
     * @param businessUUID
     * @param messages
     * @param surveyId
     * @param userIds
     * @param respondentName
     * @param surveyName
     * @param createdTime
     * @param respondentId
     */
    public int createNotificationData(String businessUUID, String messages, int surveyId, String userIds, String respondentName, String surveyName, String createdTime, String respondentId) {
        int insertionResult = 0;
        try {
            String INSERT_NOTIFICATION = "insert into %s (message, survey_id, user_ids, respondent_name, survey_name, created_time, respondent_id) values (?, ?, ?, ?, ?, ?, ?)";
            String tableName = String.format("trigger_notifications_%s", businessUUID.replace("-", "_"));
            String insertSQL = String.format(INSERT_NOTIFICATION, tableName);
            insertionResult = jdbcTemplate.update(insertSQL, new Object[]{(messages), surveyId, (userIds), respondentName, surveyName, createdTime, respondentId});
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return insertionResult;
    }

    /**
     * method to insert in task_manager table
     *
     * @param businessUUID
     * @param respondentId
     * @param triggerName
     * @return
     */
    public int createTask(String businessUUID, String respondentId, String triggerName, String responseDate) {
        int insertionResult = 0;
        try {
            String INSERT_TASK = "INSERT INTO %s (task_id, trigger_name, response_date, status, assignee, assignor) VALUES (?, ?, ?, ?, ?, ?)";
            String taskTableName = String.format("task_manager_%s", businessUUID.replace("-", "_"));
            String insertTaskSql = String.format(INSERT_TASK, taskTableName);
            insertionResult = jdbcTemplate.update(insertTaskSql, new Object[]{respondentId, triggerName, responseDate, "unassigned", new JSONObject(new HashMap()).toString(), new JSONObject(new HashMap()).toString()});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return insertionResult;
    }

    /**
     * method to get token not available in trigger_notification_businessUUID table for given survey id and tokens
     *
     * @param businessUUID
     * @param surveyId
     * @param tokenLists
     * @return
     */
    public List checkNotificationTokensExistsForThePushUpdate(String businessUUID, int surveyId, List tokenLists) {
        try {
            String tableName = String.format("trigger_notifications_%s", businessUUID.replaceAll("-", "_"));
            String query = "select json_arrayagg(respondent_id) tokens from " + tableName + " where survey_id = :surveyId and respondent_id in (:tokens)";

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("surveyId", surveyId);
            params.addValue("tokens", tokenLists);
            Map tokenMap = namedParameterJdbcTemplate.queryForMap(query, params);
            logger.info("tokenMap   " + tokenMap);
            List token = (Utils.isNotNull(tokenMap) && tokenMap.containsKey("tokens") &&
                    Utils.isNotNull(tokenMap.get("tokens")) && Utils.notEmptyString(tokenMap.get("tokens").toString())) ?
                    mapper.readValue(tokenMap.get("tokens").toString(), ArrayList.class) : new ArrayList();

            if (token.size() > 0) {
                tokenLists.removeAll(token);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tokenLists;
    }


    /**
     * method to create notification data in trigger_notifications_buuid table for aggregate push notifications
     *
     * @param businessUUID
     * @param messages
     * @param surveyId
     * @param userIds
     * @param respondentNames
     * @param surveyName
     * @param createdTime
     * @param respondentIds
     */
    public int createNotificationDataForAggregateCase(String businessUUID, List messages, int surveyId, String userIds, List respondentNames, String surveyName, String createdTime, List respondentIds) {
        int insertionResult = 0;
        try {
            String INSERT_NOTIFICATION = "insert into %s (message, survey_id, user_ids, respondent_name, survey_name, created_time, respondent_id) values (?, ?, ?, ?, ?, ?, ?)";

            String tableName = String.format("trigger_notifications_%s", businessUUID.replace("-", "_"));
            String insertSQL = String.format(INSERT_NOTIFICATION, tableName);

            int[] resultCnt = jdbcTemplate.batchUpdate(insertSQL, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement statement, int i) throws SQLException {
                    statement.setString(1, Utils.isNotNull(messages.get(i)) ? (messages.get(i)).toString() : new JSONArray().toString());
                    statement.setInt(2, surveyId);
                    statement.setString(3, userIds);
                    statement.setString(4, Utils.isNotNull(respondentNames.get(i)) ? respondentNames.get(i).toString() : "");
                    statement.setString(5, surveyName);
                    statement.setString(6, createdTime);
                    statement.setString(7, Utils.isNotNull(respondentIds.get(i)) ? respondentIds.get(i).toString() : "");
                }

                public int getBatchSize() {
                    return respondentIds.size();
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return insertionResult;
    }

    /**
     * updating lists usage count and sending email after reaching limit
     * addCount will be +1 while creating list, addCount = -1 when deleting each list
     *
     * @param businessId
     * @param addCount
     */
    public void updateTriggerListUsageCount(int businessId, int addCount) {
        logger.info("start update active triggers usage count");

        try {
            String updateSql = "update business_usage set lists = ? where business_id = ?";
            int result = jdbcTemplate.update(updateSql, addCount, businessId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("end update active triggers usage count");
    }

    /**
     * method to get sent status is processing in trigger schedule table
     *
     * @return
     */
    public Map getTriggerWithProcessingStatus() {
        Map resultMap = new HashMap();
        try {
            //String str = " select json_arrayagg(id) as id from trigger_schedule where status ='active' and timestampdiff(MINUTE, cast(last_processed_time as time), current_time) > 15 and (sent_status = 'processing' or sent_status = 'tobeprocess') and last_processed_date = current_date";
            String str = " select json_arrayagg(id) as id from trigger_schedule where status ='active' and TIMESTAMPDIFF(MINUTE, cast(CONCAT(last_processed_date,' ',last_processed_time) AS DATETIME), current_timestamp) > 15 and (sent_status = 'processing' or sent_status = 'tobeprocess')";
            resultMap = jdbcTemplate.queryForMap(str);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }


    /**
     * method to update sent_status to null
     *
     * @param ids
     */
    public void updateScheduleSentStatusToNull(List ids) {
        try {
            String str = "update trigger_schedule set sent_status = null where id in (:ids) and id > 0";
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("ids", ids);
            namedParameterJdbcTemplate.update(str, params);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }


    /**
     * Method to update triggers schedule sent_status to null after changing frequency
     *
     * @param triggerId
     * @param businessUniqueId
     * @param triggerId
     * @return
     */
    public void updateScheduleSentStatusByFrequency(int triggerId, String businessUniqueId, int surveyId) {
        try {
            int businessId = this.getBusinessIdByUUID(businessUniqueId);
            if (businessId > 0 && triggerId > 0 && surveyId > 0) {
                int result = jdbcTemplate.update(UPDATE_TRIGGER_SCHEDULE_SENT_STATUS, new Object[]{null, triggerId, businessId, surveyId});
            }
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * Method to update created trigger_schedules from  template_trigger_schedules in template_program_map
     *
     * @param businessId
     * @param templateId
     * @param programId
     * @return
     */
    public int updateTriggerSchedulesInTemplateProgramMap(int businessId, int templateId, int programId, int templateTriggerId, int triggerId, Map scheduleTriggerProgramMap) {
        int result = 0;
        try {
            if (scheduleTriggerProgramMap.size() > 0) {
                List templateScheduleIds = scheduleTriggerProgramMap.containsKey("templateScheduleIds") ? (List) scheduleTriggerProgramMap.get("templateScheduleIds") : new ArrayList();
                List triggerScheduleIds = scheduleTriggerProgramMap.containsKey("triggerScheduleIds") ? (List) scheduleTriggerProgramMap.get("triggerScheduleIds") : new ArrayList();
                List channels = scheduleTriggerProgramMap.containsKey("channels") ? (List) scheduleTriggerProgramMap.get("channels") : new ArrayList();
                List triggerMetadata = scheduleTriggerProgramMap.containsKey("triggerMetadata") ? (List) scheduleTriggerProgramMap.get("triggerMetadata") : new ArrayList();

                if (scheduleTriggerProgramMap.size() > 0) {
                    //final String insertSQL = "insert into template_program_map (template_id, business_id, program_id, template_trigger_id, template_schedule_id, channel, trigger_id, schedule_id, trigger_metadata) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    int[] updateResult = jdbcTemplate.batchUpdate(INSERT_TEMPLATE_PROGRAM_MAP, new BatchPreparedStatementSetter() {
                        public void setValues(PreparedStatement statement, int index) throws SQLException {
                            statement.setInt(1, templateId);
                            statement.setInt(2, businessId);
                            statement.setInt(3, programId);
                            statement.setInt(4, templateTriggerId);
                            statement.setInt(5, Utils.isNotNull(templateScheduleIds.get(index)) ? Integer.parseInt(templateScheduleIds.get(index).toString()) : 0);
                            statement.setString(6, channels.get(index).toString());
                            statement.setInt(7, triggerId);
                            statement.setInt(8, Utils.isNotNull(triggerScheduleIds.get(index)) ? Integer.parseInt(triggerScheduleIds.get(index).toString()) : 0);
                            statement.setString(9, Utils.isNotNull(triggerMetadata.get(index)) ? new JSONObject((Map) triggerMetadata.get(index)).toString() : new JSONObject().toString());
                        }

                        public int getBatchSize() {
                            return templateScheduleIds.size();
                        }
                    });
                    result = updateResult.length;
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * No schedules created for the given template trigger Id, updating created triggerId with templateId in template_program_map
     *
     * @param businessId
     * @param templateId
     * @param programId
     * @return
     */
    public int updateTriggerInTemplateTriggerMap(int businessId, int templateId, int programId, int templateTriggerId, int triggerId) {
        int result = 0;
        try {
            if (templateTriggerId > 0 && triggerId > 0) {
                jdbcTemplate.update(INSERT_TEMPLATE_PROGRAM_MAP, new Object[]{templateId, businessId, programId, templateTriggerId, null, null, triggerId, null, null});
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to return template program map by programId
     *
     * @param businessId
     * @param programId
     */
    public List returnTemplateProgramMapWithTriggersByProgramId(int businessId, int programId) {
        List templateProgramList = new ArrayList();
        try {
            if (programId > 0 && businessId > 0) {
                String sql = "select id, template_id, business_id, program_id, template_trigger_id, template_schedule_id, channel, trigger_id, schedule_id, trigger_metadata from template_program_map where business_id = ? and program_id = ? and trigger_id IS NOT NULL";
                templateProgramList = jdbcTemplate.queryForList(sql, new Object[]{businessId, programId});
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return templateProgramList;
    }

    /**
     * Method to get triggers details using business uuid by triggerId
     *
     * @param businessUUID
     * @param triggerIds
     * @return
     */
    public List getTriggersByTriggerIds(String businessUUID, List triggerIds) {
        logger.debug("begin retrieving the trigger details by triggerIDs");
        List triggers = new ArrayList();
        try {
            if (triggerIds.size() > 0) {
                String query = String.format(GET_TRIGGERS_BY_TRIGGER_IDS, (businessUUID.replaceAll("-", "_")));
                SqlParameterSource sqlParameterSource = new MapSqlParameterSource("triggerIds", triggerIds);
                triggers = namedParameterJdbcTemplate.queryForList(query, sqlParameterSource);
            }

        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.debug("end retrieving the trigger details by triggerIDs");
        return triggers;
    }

    /**
     * Method to update trigger conditions by updatedOrDeleteQuestionIds
     *
     * @param businessUUID
     * @param updatedOrDeletedQuestions
     * @param triggerIds
     * @return
     */
    public Map updateTriggerConditionsByQuestionIds(String businessUUID, List updatedOrDeletedQuestions, List triggerIds) {
        logger.debug("begin updating  the trigger condition by triggerIDs & updateOrDeletedQuestions");
        Map resultMap = new HashMap();
        try {

            //Fetching the triggers from trigger_buuid table by triggerIds().
            List triggers = new ArrayList();
            List updatedTriggersList = new ArrayList();
            if (updatedOrDeletedQuestions.size() > 0 && triggerIds.size() > 0) {

                triggers = this.getTriggersByTriggerIds(businessUUID, triggerIds);

                //Iterating all the triggers from trigger_buuid table and checking each triggercondition with deletedQuestions
                if (triggers.size() > 0) {
                    for (int k = 0; k < triggers.size(); k++) {

                        Map eachTriggerMap = (Map) triggers.get(k);
                        int eachTriggerId = Utils.isNotNull(eachTriggerMap.get("id")) ? Integer.parseInt(eachTriggerMap.get("id").toString()) : 0;
                        logger.info("eachTriggerId  " + eachTriggerId);
                        String eachTriggerCondition = Utils.isNotNull(eachTriggerMap.get("trigger_condition")) ? eachTriggerMap.get("trigger_condition").toString() : "";
                        List deactivatedConditions = eachTriggerMap.containsKey("deactivated_conditions") && Utils.isNotNull(eachTriggerMap.get("deactivated_conditions")) ? mapper.readValue(eachTriggerMap.get("deactivated_conditions").toString(), ArrayList.class) : new ArrayList();

                        List deactivatedConditionsNew = new ArrayList();
                        String[] andQueries = eachTriggerCondition.split("&&");
                        //tempList is immutable as it is created from Arrays.asList so creating another list from tempList to make mutable list
                        List tempList = Arrays.asList(andQueries);
                        List andQueriesList = new ArrayList(tempList);

                        StringBuilder newTriggerCondition = new StringBuilder();

                        for (int i = 0; i < andQueries.length; i++) {
                            String eachAndQuery = andQueries[i];

                            //Iterating through all deletedQuestions from program update to check trigger condition contains deleted or updated question
                            for (int j = 0; j < updatedOrDeletedQuestions.size(); j++) {
                                String eachQuestionUUID = updatedOrDeletedQuestions.get(j).toString();

                                //if condition contains removed questionUUID removing the condition and adding into deactivated_conditions
                                if (eachAndQuery.contains(eachQuestionUUID)) {
                                    logger.info("removed trigger condition  " + eachAndQuery);
                                    //adding to the exisiting deactivatedConditions (example : multiple update to the same program in draft)
                                    deactivatedConditionsNew.add(eachAndQuery);
                                    andQueriesList.remove(eachAndQuery);
                                    break;
                                }
                            }
                        }

                        //If condition is removed update the newly modified condition in trigger_condition triger_buuid table by triggerId
                        //Removed condition will be added in deactivated_conditions as another entry in triger_buuid table by triggerId
                        if (deactivatedConditionsNew.size() > 0) {
                            //to maintain old deactivated_condtions also while updating
                            deactivatedConditions.addAll(deactivatedConditionsNew);

                            //forming new trigger condition with remaining questions
                            for (int index = 0; index < andQueriesList.size(); index++) {
                                if (index < andQueriesList.size() - 1) {
                                    newTriggerCondition.append(andQueriesList.get(index).toString() + "&&");
                                } else {
                                    newTriggerCondition.append(andQueriesList.get(index).toString());
                                }
                            }

                            //updating the trigger with new trigger_condition & deactivated condition by triggerId
                            String updateSql = String.format(UPDATE_TRIGGER_DEACTIVATED_CONDITION_BY_ID, (businessUUID.replaceAll("-", "_")));
                            int result = jdbcTemplate.update(updateSql, new Object[]{newTriggerCondition.toString(), new JSONArray(deactivatedConditions).toString(), null, eachTriggerId});
                            if (result > 0) {
                                updatedTriggersList.add(eachTriggerId);
                            }
                        }

                    }
                }

                if (updatedTriggersList.size() > 0) {
                    resultMap.put("updatedTriggerIds", updatedTriggersList);
                    resultMap.put("success", true);
                }

                logger.debug("end updating  the trigger condition by triggerIDs & updateOrDeletedQuestions");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return resultMap;
    }


    /**
     * Method to update trigger notificaiton trigger status when trigger status is changed to inactive
     *
     * @param triggerId
     * @return
     */
    public int updateTriggerNoficiationTriggerStatus(String triggerId, String businessUniqueId, String triggerStatus) {
        logger.info("begin retrieving the trigger schedule details by triggerID");
        int result = 0;
        try {
            List triggerScheduleIdsList = new ArrayList();
            int businessId = getBusinessIdByUUID(businessUniqueId);
            String query = String.format(GET_TRIGGER_SCHEDULE_IDS_BY_TRIGGER_ID, businessUniqueId.replaceAll("-", "_"));
            List triggerScheduleList = jdbcTemplate.queryForList(query, new Object[]{triggerId, businessId});
            if (Utils.isNotNull(triggerScheduleList) && triggerScheduleList.size() > 0) {
                Map eachMap = (Map) triggerScheduleList.get(0);
                Object scheduleIdObj = eachMap.get("schedule_id");
                triggerScheduleIdsList = (Utils.isNotNull(scheduleIdObj)) ? mapper.readValue(scheduleIdObj.toString(), ArrayList.class) : new ArrayList();
            }

            /**
             * updating trigger_status based on state (active, inactive)
             * updating active trigger notifications when triggerStartTime > currentTimestamp and sent_status is null
             *
             * Example :
             * If trigger start time 10 PM, and turned off at 8 pm --> trigger notification will be disabled.
             * if same trigger is turn on at 9 pm --> 10 pm trigger notification will be enabled again.
             */
            if (triggerScheduleIdsList.size() > 0) {
                String statusStr = Utils.isNotNull(triggerStatus) && Utils.notEmptyString(triggerStatus) && triggerStatus.equals(Constants.STATUS_ACTIVE) ? "1" : "0";
                MapSqlParameterSource sqlParam = new MapSqlParameterSource();
                sqlParam.addValue("triggerStatus", statusStr);
                sqlParam.addValue("scheduleIds", triggerScheduleIdsList);
                result = namedParameterJdbcTemplate.update(UPDATE_TRIGGER_NOTIFICATION_TRIGGER_STATUS, sqlParam);
            }

            logger.info("end retrieving the trigger details by triggerID");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to check performance trigger condition
     *
     * @param tokens
     * @param businessUniqueId
     * @param surveyId
     * @param sqlwhere
     */
    public List<Map<String, Object>> checkPerformanceTriggerCondition(List tokens, String businessUniqueId, String surveyUniqueId, int scheduleId, int surveyId, String sqlwhere, String triggerStartDate, String startTime, String endTime) {
        logger.info("start checkPerformanceTriggerCondition");
        try {
            if (Utils.notEmptyString(sqlwhere)) {
                //get latest entry of submission token from event
                String token = getLatestSumbittedEventToken(surveyUniqueId);
                /*// check that token is avaliable in trigger notification table
                boolean isTokenExists = false;
                if(Utils.notEmptyString(token)) {
                    String notificatonQuery = "select count(1) from trigger_notification where submission_token = ? and schedule_id = ?";
                    isTokenExists = jdbcTemplate.queryForObject(notificatonQuery, new Object[]{token, scheduleId} ,Boolean.class);
                }*/
                //if not available then perfrom the calc
                if (Utils.notEmptyString(token)) {
                    String sqlQuery = "SELECT perf.completed AS responseCount,  (perf.completed  * 100 / perf.sent) responseRate,\n" +
                            "( perf.completed* 100 / (perf.completed + perf.opened)) completionRate,\n" +
                            "( (perf.opened - perf.completed)* 100 / (perf.completed + perf.opened)) AS 'abandonmentRate'\n" +
                            "FROM (" +
                            "SELECT COUNT(case when last_action='completed' then (dynamic_token_id) END) AS 'completed',\n" +
                            "COUNT(case when last_action ='opened' then  (dynamic_token_id)  END) AS 'opened',\n" +
                            "COUNT(case when last_action !='token' then (dynamic_token_id) END) AS 'sent'\n" +
                            "from token_" + businessUniqueId.replaceAll("-", "_") + " WHERE survey_id= :surveyId";
                    //add tokens if available (in case of metadata + questions + performance)
                    if (tokens.size() > 0) {
                        sqlQuery += " and dynamic_token_id in (:tokens)";
                    }
                    //add date conditions
                    if (Utils.notEmptyString(triggerStartDate)) {
                        sqlQuery += " and created_time > '" + triggerStartDate + "' ";
                    }
                    if (Utils.notEmptyString(startTime) && Utils.notEmptyString(endTime)) {
                        sqlQuery += " and created_time >= '" + startTime + "' and created_time <= '" + endTime + "'";
                    }
                    logger.info("PerformanceTriggerCondition qry {}", sqlQuery);
                    //close the query
                    sqlQuery += ") AS perf having " + sqlwhere; // eg: HAVING (abandonmentRate = -100 or responseCount > 2 or responseRate > 10 or completionRate > 50)
                    logger.info(" checkPerformanceTriggerCondition qry {}", sqlQuery);

                    MapSqlParameterSource sqlParam = new MapSqlParameterSource();
                    sqlParam.addValue("tokens", tokens);
                    sqlParam.addValue("surveyId", surveyId);
                    List<Map<String, Object>> result = namedParameterJdbcTemplate.queryForList(sqlQuery, sqlParam);
                    if (Utils.isNotNull(result) && result.size() > 0) {
                        //assign the token value in the response to insert in trigger notification table
                        Map resultMap = result.get(0);
                        resultMap.put("token", (tokens.size() > 0 ) ?  tokens : new ArrayList(Collections.singletonList(token)));
                    }
                    return result;
                }
            }

        } catch (Exception e) {
            logger.error("Exception at checkPerformanceTriggerCondition. Msg {}", e.getMessage());
        }
        logger.info("end checkPerformanceTriggerCondition");
        return new ArrayList();
    }

    /**
     *
     * method check if given token is exist in schedule table
     *
     * @param token
     * @return
     */
    public boolean isTokenExistInSchedule(String token, int scheduleId, String businessUUID) {
        try {
            //String notificatonQuery = "select count(1) from trigger_notification where submission_token = ? and schedule_id = ?";
            String notificatonQuery = String.format("select count(1) from trigger_notification_%s where submission_token = ? and schedule_id = ?", businessUUID.replaceAll("-","_"));
            return jdbcTemplate.queryForObject(notificatonQuery, new Object[]{token, scheduleId}, Boolean.class);
        } catch (Exception e) {
            logger.error("exception at getLatestSumbittedEventToken. Msg {}", e.getMessage());
        }
        return false;
    }

    /**
     * method ot get latest event token for the survey
     *
     * @param surveyUniqueId
     * @return
     */
    public String getLatestSumbittedEventToken(String surveyUniqueId) {
        try {
            String tokenQuery = "select submission_token from event_" + surveyUniqueId.replaceAll("-", "_") + " where submission_token is not null order by created_time desc limit 1";
            return jdbcTemplate.queryForObject(tokenQuery, String.class);
        } catch (Exception e) {
            logger.error("exception at getLatestSumbittedEventToken. Msg {}", e.getMessage());
        }
        return "";
    }

    /**
     * method ot get latest event token and its response count for the survey
     *
     * @param surveyUniqueId
     * @return
     */
    public Map getLatestResponseTokenAndCount(String surveyUniqueId) {
        try {
            String tokenQuery = "select submission_token as token, max(id) as totalResponseCount from event_" + surveyUniqueId.replaceAll("-", "_") + " where submission_token is not null  group by id order by created_time desc limit 1";
            List resultList = jdbcTemplate.queryForList(tokenQuery);
            if(resultList.size() > 0){
                return  (Map) resultList.get(0);
            }
        } catch (Exception e) {
            logger.error("exception at getLatestSumbittedEventToken. Msg {}", e.getMessage());
        }
        return new HashMap();
    }

    /**
     * Method to get nps count based on meta data tokens
     *
     * @param surveyUUId
     * @param questionIndex
     * @param fromDate
     * @param toDate
     * @param submissionTokens
     * @return
     */
    public double getNpsScoreForSurvey(String surveyUUId, int questionIndex, String fromDate, String toDate, List submissionTokens, String surveyType) {

        double npsScore = 0;
        String surveyUUIDHyphenReplaced = surveyUUId.replace("-", "_");
        try {

            String eventTblName = String.format("event_%s", surveyUUIDHyphenReplaced);
            String whereSql = "";
            String npssql = "select  count(case  when JSON_EXTRACT(data,'$[" + questionIndex + "]') in (7, 8) then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end )as 'passive' , " +
                    " count(case when (JSON_EXTRACT(data,'$[" + questionIndex + "]') < 7  and JSON_EXTRACT(data,'$[" + questionIndex + "]') != -1) then  (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end )as 'detractor', " +
                    " count(case when JSON_EXTRACT(data,'$[" + questionIndex + "]') > 8  then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end) as 'promoter', " +
                    " count(case when JSON_EXTRACT(data,'$[" + questionIndex + "]') > -1  then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end) as 'total' from " + eventTblName;

            if (submissionTokens.size() > 0) {
                whereSql = " where submission_token in (:submissionToken)";
            }
            if (!surveyType.equalsIgnoreCase("journey") && (Utils.isNotNull(fromDate) && Utils.isNotNull(toDate))) {
                whereSql = whereSql.length() > 0 ? whereSql + " and " : " where ";
                npssql += whereSql + " created_time between :fromDate and :toDate ";
            } else if (surveyType.equalsIgnoreCase("journey")) {
                npssql += whereSql;
            }

            String sql = "select round((`npscnt`.promoter/`npscnt`.total)*100 - (`npscnt`.detractor/`npscnt`.total)*100, 1) as 'npsscore' , `npscnt`.total from ( " + npssql + " ) as `npscnt` ";
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource("submissionToken", submissionTokens).addValue("fromDate", fromDate).addValue("toDate", toDate);
            Map resultMap = namedParameterJdbcTemplate.queryForMap(sql, sqlParameterSource);

            logger.info("nps score sql {}", sql);
            logger.info("nps score from date {} todate {}", fromDate, toDate);

            if (Utils.isNotNull(resultMap) && resultMap.size() > 0) {
                npsScore = Utils.isNotNull(resultMap.get("npsscore")) ? Double.parseDouble(resultMap.get("npsscore").toString()) : -500;
            }

            logger.info("npsScore eachNpsScore {}", npsScore);

        } catch (Exception e) {
            logger.error("getNpsScoreForSegments() : " + e.getMessage());
        }
        return npsScore;
    }

    /**
     * method to get quesitonId for given question uniqueID
     *
     * @param businessUUID
     * @param questionUUID
     * @return
     */
    public String getQestionIdForQUniqueId(String businessUUID, String questionUUID, boolean isMetric) {
        try {
            if (!isMetric) {
                String tableName = "questions_" + businessUUID.replace("-", "_");
                String questionSql = String.format(GET_QUESTION_ID_BY_UUID, tableName);
                return jdbcTemplate.queryForObject(questionSql, new Object[]{questionUUID}, String.class);
            } else if (isMetric) {
                String tableName = "key_metric_" + businessUUID.replace("-", "_");
                String questionSql = String.format("select key_metric_id from %s where key_metric_uuid = ?", tableName);
                return jdbcTemplate.queryForObject(questionSql, new Object[]{questionUUID}, String.class);
            }

        } catch (Exception e) {
            logger.error("exception at getQestionIndexForQId. Msg {}", e.getMessage());
        }
        return "0";
    }

    /**
     * method to get rating count of given metric question
     *
     * @param questionIndex
     * @param businessUniqueId
     * @param fromDate
     * @param toDate
     * @param tokenList
     * @param scale
     * @return
     */
    public double computeAggregateScoreForSurvey(int questionIndex, String businessUniqueId, String fromDate, String toDate, List tokenList, int scale) {
        logger.debug("getMetricQuestionsRatingCount begin");
        double result = 0.0;
        try {
            //begin
            /** query for 4 rating metric
             * SELECT (SUM(B.0+B.1+B.2+B.3)/B.total) as metricScore FROM (
             * select  count(case  when JSON_EXTRACT(data,'$[1]') = 0 then (JSON_EXTRACT(data,'$[1]')) END ) * 1 as '0' ,
             * count(case when JSON_EXTRACT(DATA,'$[1]') = 1 then  (JSON_EXTRACT(DATA,'$[1]')) END ) * 2 as '1',
             * count(case when JSON_EXTRACT(DATA,'$[1]') = 2  then (JSON_EXTRACT(data,'$[1]')) END) * 3 as '2',
             * COUNT(case when JSON_EXTRACT(data,'$[1]') = 3  then (JSON_EXTRACT(data,'$[1]')) END) * 4 as '3',
             * COUNT(case when JSON_EXTRACT(data,'$[1]') != -1 then (JSON_EXTRACT(data,'$[1]')) END) AS 'total'
             * from event_4dd16ef8_30d0_4869_8700_3cc852ee4da1 ) B;
             */
            StringBuilder queryStr = new StringBuilder();
            StringBuilder sumStr = new StringBuilder();
            String eventTblName = "event_" + businessUniqueId.replaceAll("-", "_");
            //form json_extract for rating question based on its scale
            for (int i = 0; i < scale; i++) {
                queryStr.append(" count(case  when JSON_EXTRACT(data,'$[" + questionIndex + "]') = " + i + " then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) END ) * " + (i + 1) + " as '" + i + "'");
                sumStr.append("B." + i);
                if (i < scale - 1) {
                    queryStr.append(" , ");
                    sumStr.append("+");
                }
                if (i < scale) {
                    queryStr.append(" COUNT(case when JSON_EXTRACT(data,'$[1]') != -1 then (JSON_EXTRACT(data,'$[1]')) END) AS 'total' from ").append(eventTblName);
                }
            }

            /*queryStr.append(" count(case  when JSON_EXTRACT(data,'$[" + questionIndex + "]') in (7, 8) then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end )as 'passive' , " +
                    " count(case when (JSON_EXTRACT(data,'$[" + questionIndex + "]') < 7  and JSON_EXTRACT(data,'$[" + questionIndex + "]') != -1) then  (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end )as 'detractor', " +
                    " count(case when JSON_EXTRACT(data,'$[" + questionIndex + "]') > 8  then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end) as 'promoter', " +
                    " count(case when JSON_EXTRACT(data,'$[" + questionIndex + "]') > -1  then (JSON_EXTRACT(data,'$[" + questionIndex + "]')) end) as 'totalNps'");*/

            //append dates
            if (Utils.isNotNull(fromDate) && Utils.isNotNull(toDate)) {
                queryStr.append(" and created_time between :fromDate and :toDate ");
            }
            if (tokenList.size() > 0) {
                queryStr.append(" and submission_token in (:tokenList) ");
            }

            String sql = "select (sum(" + sumStr.toString() + ")/B.total) as metricScore from ( select " + queryStr.toString() + ") B";
            logger.info("metric query {}", sql);
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource("tokenList", tokenList).addValue("fromDate", fromDate).addValue("toDate", toDate);
            Object object = namedParameterJdbcTemplate.queryForObject(sql, sqlParameterSource, Object.class);
            result = (Utils.isNotNull(object)) ? Double.valueOf(object.toString()) : 0;

        } catch (Exception e) {
            logger.error("Exception at getMetricQuestionsRatingCount. Msg {}", e.getMessage());
        }
        logger.debug("getMetricQuestionsRatingCount end");
        return result;
    }

    /**
     * @param surveyId
     * @param fromDate
     * @param toDate
     * @param questionIndex
     * @param scaleStr
     * @return
     */
    public List computeQuestionRatingsCounts(String surveyId, String fromDate, String toDate, int questionIndex, String scaleStr, List tokenList) {

        List countAtIndexes = new ArrayList();
        String surveyUUIDHyphenReplaced = surveyId.replace("-", "_");
        int scale = Utils.notEmptyString(scaleStr) ? Integer.parseInt(scaleStr) : 0;

        try {

            List ratingCnt = new ArrayList();
            String eventTblName = String.format("event_%s", surveyUUIDHyphenReplaced);
            //begin
            String sql = "select count(JSON_EXTRACT(data,'$[" + questionIndex + "]')) from " + eventTblName + " where JSON_EXTRACT(data,'$[" + questionIndex + "]') = :index";
            //date filter
            if (Utils.notEmptyString(fromDate) && Utils.notEmptyString(toDate)) {
                sql += " and created_time between :fromDate and :toDate ";
            }
            // dtmetadata token if available
            if (tokenList.size() > 0) {
                sql += "  and submission_token in (:tokenList) ";
            }

            for (int i = 0; i < scale; i++) {
                SqlParameterSource sqlParameterSource = new MapSqlParameterSource("tokenList", tokenList).addValue("index", i).addValue("fromDate", fromDate).addValue("toDate", toDate);
                Object object = namedParameterJdbcTemplate.queryForObject(sql, sqlParameterSource, Object.class);
                int eachIndexCount = (Utils.isNotNull(object)) ? Integer.parseInt(object.toString()) : 0;
                ratingCnt.add(eachIndexCount);
            }

            countAtIndexes.add(ratingCnt);
            //end

        } catch (Exception e) {
            logger.error("exception at computeQuestionRatingsCounts {}",e.getMessage());
        }
        logger.debug("countAtIndexes {}", countAtIndexes);
        return countAtIndexes;
    }

    /**
     * Get key_metric_id, scale for an survey by metric_map and metricname
     *
     * @param businessUUID
     * @param metricId
     * @return
     */
    public List getKeyMetricDetailsById(String businessUUID, int metricId) {
        List resultList = new ArrayList();
        try {
            if (metricId > 0) {
                String keyMetricTable = String.format("key_metric_%s", businessUUID.replace("-", "_"));
                String keyMetricSql = "select key_metric_id, key_metric_text, scale from " + keyMetricTable + " where key_metric_id = ?";
                resultList = jdbcTemplate.queryForList(keyMetricSql, new Object[]{metricId});
            }
            logger.info("metric resultList {}", resultList.toString());

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultList;
    }

    /**
     * Get business details from business by its primary id
     *
     * @param businessID
     * @return
     */
    public Map getBusinessDetailsById(int businessID) {
        Map resultMap = new HashMap();
        try {
            if (businessID > 0) {
                String businessSql = "select business_uuid, business_name from business where business_id = ?";
                List resultList = jdbcTemplate.queryForList(businessSql, new Object[]{businessID});
                if(resultList != null && resultList.size() > 0 ){
                    return (Map) resultList.get(0);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return resultMap;
    }


    /**
     * @param scheduleId
     * @return
     */
    public List retrieveLatestFrequencyNotificationsForASchedule(int scheduleId, String businessUUID) {
        List notifications = new ArrayList();
        try {
            String sql = String.format(GET_NOTIFICATION_BY_SCHEDULEID_LATEST_V1, businessUUID.replaceAll("-","_"));
            notifications = jdbcTemplate.queryForList(sql, new Object[]{scheduleId});

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notifications;
    }

    /**
     * method to fetch customized `from email` settings of a business
     *
     * @param businessId
     * @return
     */
    public Map getBusinessEmailSettings(int businessId) {
        Map resultMap = new HashMap();
        try {
            //query to fetch `from email` settings of a business
            String getFromEmailSettingSql = "SELECT business_email, trigger_email, alert_email, notification_email FROM email_settings WHERE business_id = ?";
            resultMap = jdbcTemplate.queryForMap(getFromEmailSettingSql, new Object[]{businessId});

            //sanitize result map
            if(resultMap.size() > 0) {
                Map businessEmail = mapper.readValue(resultMap.get("business_email").toString(), HashMap.class);
                resultMap.remove("business_email");
                resultMap.put("business_email", businessEmail);

                Map triggerEmail = mapper.readValue(resultMap.get("trigger_email").toString(), HashMap.class);
                resultMap.remove("trigger_email");
                resultMap.put("trigger_email", triggerEmail);

                Map alertEmail = mapper.readValue(resultMap.get("alert_email").toString(), HashMap.class);
                resultMap.remove("alert_email");
                resultMap.put("alert_email", alertEmail);

                Map notificationEmail = mapper.readValue(resultMap.get("notification_email").toString(), HashMap.class);
                resultMap.remove("notification_email");
                resultMap.put("notification_email", notificationEmail);
            }
        } catch (Exception e) {
            logger.info("No existing customized `from email` settings found for business {} ", businessId);
            //logger.error("exception at customizeFromEmailForBusiness(). Msg {}", e.getMessage());
        }
        return resultMap;
    }

    /**
     * get key metrics by name
     * @param businessUUID
     * @param keyMetricIds
     * @return
     */
    public Map getKeyMetricByNameForBusiness(String businessUUID, List keyMetricIds){
        Map keyMetricsMap = new HashMap();
        try {
            String tableName = "key_metric_" + businessUUID.replace("-","_");
            String keyMetricSql = "select  key_metric_text, key_metric_uuid from " + tableName + " where key_metric_id in (:keyMetricIds)";
            MapSqlParameterSource param = new MapSqlParameterSource();
            param.addValue("keyMetricIds", keyMetricIds);
            if(keyMetricIds.size() > 0) {
                List keyMetrics = namedParameterJdbcTemplate.queryForList(keyMetricSql, param);

                Iterator itr = keyMetrics.iterator();
                while (itr.hasNext()) {
                    Map eachMap = (Map) itr.next();
                    String uniqueId = eachMap.get("key_metric_uuid").toString();
                    String metricText = eachMap.get("key_metric_text").toString();
                    keyMetricsMap.put(uniqueId, metricText);
                }
            }

        }catch (Exception e){
            logger.error("exception at getKeyMetricByNameForBusiness. msg {}", e.getMessage());
        }
        return keyMetricsMap;
    }

    /**
     * method to get text analytics type either Basic or Advanced
     * @param businessId
     * @return
     */
    public String getTAType(int businessId){
        try {
            String query = "select advanced_text_analytics from business_configurations where business_id = ? and text_analytics = '1'";
            List resultList = jdbcTemplate.queryForList(query, new Object[]{businessId});
            if(resultList.size() > 0) {
                Map resultMap = (Map) resultList.get(0);
                if (resultMap.containsKey("advanced_text_analytics")) {
                    if (resultMap.get("advanced_text_analytics").equals("1")) {
                        logger.info("Advanced text analytics enabled for business {}", businessId);
                        return Constants.TA_ADVANCED;
                    } else {
                        logger.info("Basic text analytics enabled for business {}", businessId);
                        return Constants.TA_BASIC;
                    }
                }
            }
        }catch (Exception e){
            logger.error("exception at getTAType");
        }
        logger.info(" text analytics not enabled for business {}", businessId);
        return null;
    }

    /**
     * method to create notification data in trigger_notification_buuid table from trigger_notification by notificationUUID
     *
     * @param triggerNotificationUUID
     * @param businessUUID
     */
    public int createTriggerNotificationInBusinessUUIDTable(String triggerNotificationUUID, String businessUUID) {
        logger.info("Start createTriggerNotificationInBusinessUUID");
        int insertionResult = 0;
        try {
            if(businessUUID.length() > 0) {

                //DTV-8038, added action_type column
                String sql = "INSERT INTO trigger_notification_%s " +
                        "(id, channel, message, start_time, subject, STATUS, contacts, add_feedback, condition_query, submission_token, sent_status, schedule_id, notification_uuid, created_time, modified_time, deactivated_contacts, trigger_status, action_type) " +
                        " SELECT id, channel, message, start_time, subject, STATUS, contacts, add_feedback, condition_query, submission_token, sent_status, schedule_id, notification_uuid, created_time, modified_time, deactivated_contacts, trigger_status, action_type " +
                        " FROM trigger_notification where notification_uuid = ?";
                String INSERT_NOTIFICATION = String.format(sql, businessUUID.replace("-", "_"));
                int insertResult = jdbcTemplate.update(INSERT_NOTIFICATION, new Object[]{triggerNotificationUUID});
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        logger.info("End createTriggerNotificationInBusinessUUID");
        return insertionResult;
    }

    /**
     * method to update sent status of the notification by using notifyId
     *
     * @param notifyId
     * @param status   PROCESSING / COMPLETED
     * @return
     */
    public void updateBusinessNotificationSentStatusByNotifyId(int notifyId, String status, String businessUUID) {
        logger.debug("begin updating the trigger notification sent status");
        try {
            String sql = String.format(UPDATE_BUSINESS_TRIGGER_NOTIFICATION_SENT_STATUS, businessUUID.replaceAll("-","_"));
            jdbcTemplate.update(sql, new Object[]{status, new Timestamp(System.currentTimeMillis()), notifyId});
            logger.debug("end updating the trigger notification sent status");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Method to delete trigger notification details by notify unique id
     *
     * @param notifyId
     * @return
     */
    public int deleteTriggerNotificationById(int notifyId) {
        logger.debug("begin deleting the trigger notification details");
        int result = 0;
        try {
            result = jdbcTemplate.update(DELETE_TRIGGER_NOTIFICATION_BY_ID, new Object[]{notifyId});
            logger.debug("end deleting the trigger notification details");
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            result = -1;
        }
        return result;
    }

    /**
     * Method to update active triggers & trigger scheduels to inactive for expired surveys
     * @param businessUUID
     */
    public void updateTriggerStatusForExpiredBusiness(String businessUUID){
        try {
            String surveyTable = String.format("surveys_%s", businessUUID.replaceAll("-","_"));
            String triggerTable = String.format("trigger_%s", businessUUID.replaceAll("-","_"));

            String surveyIdSql = "select json_arrayagg(id) AS id from " + surveyTable + " where to_date < CURRENT_TIMESTAMP";
            List surveyIds  = new ArrayList();

            //another try catch block to catch null pointer exceptions
            try {
                String surveyIdStr = jdbcTemplate.queryForObject(surveyIdSql, String.class);
                surveyIds = Utils.notEmptyString(surveyIdStr) ? mapper.readValue(surveyIdStr, ArrayList.class) : new ArrayList();
            }catch (Exception e){
                logger.error(e.getMessage());
            }

            if(surveyIds.size() > 0){
                //updating only active triggers to inactive
                String triggerUpdateSql = "UPDATE " + triggerTable + " SET STATUS = '0' WHERE survey_id IN (:surveyIds) AND status = 1";
                MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
                mapSqlParameterSource.addValue("surveyIds", surveyIds);
                namedParameterJdbcTemplate.update(triggerUpdateSql, mapSqlParameterSource);

                //updating only active trigger_schedules to inactive
                String triggerScheduleUpdateSql = "UPDATE trigger_schedule SET STATUS = 'inactive'\n" +
                        " WHERE trigger_id IN (SELECT id FROM " + triggerTable + " WHERE survey_id IN (:surveyIds) AND STATUS = '1')";
                namedParameterJdbcTemplate.update(triggerScheduleUpdateSql, mapSqlParameterSource);

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Method to get trigger generated focus metric by survey id from focus_metric_{buuid} table
     * @return
     */
    public Map getTriggerFocusMetricBySurveyId(int surveyId, String businessUUID){
        logger.info("begin getTriggerFocusMetricBySurveyId");
        Map resultMap = new HashMap();
        try{
            if(businessUUID.length() > 0 && surveyId > 0){
                String sql = String.format("SELECT focus_metric_id, focus_metric, focus_metric_data, focus_metric_type, survey_id, created_by from focus_metric_%s WHERE survey_id = ? order by created_time desc", businessUUID.replaceAll("-","_"));
                List focusMetricsList = jdbcTemplate.queryForList(sql, new Object[]{surveyId});
                if(focusMetricsList.size() > 0){
                    resultMap = (Map) focusMetricsList.get(0);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("End getTriggerFocusMetricBySurveyId");
        return resultMap;
    }

    /**
     * Method to update focusMetric & focusMetricType in focus_metric_{buuid} table
     * @return
     */
    public int updateTriggerFocusMetricBySurveyId(int surveyId, String businessUUID, String focusMetric, String focusMetricType, int userId, int focusMetricId){
        int updateResult = 0;
        logger.info("begin updateTriggerFocusMetricBySurveyId");
        try{
            if(businessUUID.length() > 0 && surveyId > 0){
                String sql = String.format("update focus_metric_%s set focus_metric = ?, focus_metric_type = ?, modified_by = ?, modified_time = ? WHERE survey_id = ? and focus_metric_id = ?", businessUUID.replaceAll("-","_"));
                updateResult = jdbcTemplate.update(sql, new Object[]{focusMetric, focusMetricType, userId, new Timestamp(System.currentTimeMillis()), surveyId, focusMetricId});
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("End updateTriggerFocusMetricBySurveyId");
        return updateResult;
    }

    /**
     * Method to update focusMetric & focusMetricType in focus_metric_{buuid} table
     * @return
     */
    public int insertTriggerFocusMetricBySurveyId(int surveyId, String businessUUID, List<String> focusMetric, List<String> focusMetricType, int userId){
        int updateResult = 0;
        logger.info("Start insertTriggerFocusMetricBySurveyId");
        try{
            if(businessUUID.length() > 0 && surveyId > 0){
                String sql = String.format("insert into focus_metric_%s (focus_metric, focus_metric_type, survey_id, created_by, created_time) values (?,?,?,?,current_timestamp) ", businessUUID.replaceAll("-","_"));
                updateResult = jdbcTemplate.update(sql, new Object[]{new JSONArray(focusMetric).toString(), new JSONArray(focusMetricType).toString(), surveyId, userId});
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        logger.info("End insertTriggerFocusMetricBySurveyId");
        return updateResult;
    }

    /**
     * method to fetch integration by integration name
     *
     * @param integrationName
     * @return
     */
    public int getIntegrationIdByName(String integrationName) {
        int integrationId = 0;
        try {
            String sql = "SELECT id FROM workato_integrations WHERE integration = ?";
            Map workatoIntegrationMap = jdbcTemplate.queryForMap(sql, new Object[]{integrationName});
            integrationId = workatoIntegrationMap.containsKey("id") &&
                    Utils.isNotNull(workatoIntegrationMap.get("id")) ?
                    (int) workatoIntegrationMap.get("id") : 0;
        } catch (Exception e) {
            logger.error("Error in getIntegrationIdByName() : " + e);
            logger.info("Workato integration not present!");
        }
        return integrationId;
    }

    /**
     * method to update ticket id in participant table
     * @param ticketId
     * @param participantId
     * @param tableName
     */
    public void updateParticipantHeaderById(String ticketId, int participantId, String tableName) {
        try {
            String sql = "UPDATE "+tableName+" SET ticket_id = ? WHERE id = ?";
            int update = jdbcTemplate.update(sql, new Object[]{ticketId, participantId});
        } catch (Exception e) {
            logger.error("Error in updateTicketId() : " + e);
        }
    }


    /**
     * method to update ticket id in participant table
     * @param ticketId
     * @param participantId
     * @param tableName
     */
    public void updateParticipantHeaderById(String ticketId, int participantId, String tableName, Integer index) {
        try {
            if(index != null) { //DTV-10545: If incase there’s ever more than 1 ticket ID for the same response, show the same by separating with a comma.
                String alterSql = "UPDATE " + tableName +
                        " SET DATA = JSON_REPLACE(data, '$["+index+"]', CONCAT(JSON_UNQUOTE(JSON_EXTRACT(data, '$["+index+"]')), '', " +
                        " IF(JSON_UNQUOTE(JSON_EXTRACT(data, '$["+index+"]')) = '', :ticketId, ',"+ticketId+"'))) " +
                        "WHERE id = :participantId";
                MapSqlParameterSource param = new MapSqlParameterSource();
                param.addValue("ticketId", ticketId);
                param.addValue("participantId", participantId);
                param.addValue("index", index);
                logger.info("update ticket id query : " + alterSql);
                int result = namedParameterJdbcTemplate.update(alterSql, param);
            }
        } catch (Exception e) {
            logger.error("Error in updateTicketId() : " + e);
        }
    }

    /**
     * method to update ticket id in event table
     * @param ticketDetails
     * @param token
     * @param surveyUniqueId
     * @param headerName
     */
    public void updateEventMetadataByToken(String ticketDetails, String token, String surveyUniqueId, String headerName) {
        try {
            if(ticketDetails != null) {
                String alterSql = "UPDATE event_" + surveyUniqueId.replaceAll("-","_") +
                        " SET event_metadata =  JSON_SET(event_metadata, '$.metaData."+headerName+"', ?)  WHERE submission_token = ?";
                int result = jdbcTemplate.update(alterSql, new Object[]{ticketDetails, token});
            }
        } catch (Exception e) {
            logger.error("Error in updateTicketId() : " + e);
        }
    }

    /**
     * method to get ticket id from participant table for a speicific participantId
     * @param index
     * @param participantId
     * @param tableName
     */
    public String getRespondentTicketDetailsById(int participantId, String tableName, Integer index) {
        String dtworksTicketId = null;
        try {
            if(index != null) {
                String alterSql = " select  json_unquote(json_extract(data, '$["+index+"]')) from " + tableName +
                        " WHERE id = ?";
                dtworksTicketId = jdbcTemplate.queryForObject(alterSql, new Object[]{participantId}, String.class);
            }
        } catch (Exception e) {
            logger.error("Error in updateTicketId() : " + e);
        }
        return dtworksTicketId;
    }

    public void checkIfColumExistElseCreate(String columnName, String tableName, String alterColumnDetails){
        String checkColumnSqlQuery = "show columns from " + tableName + " like '" + columnName + "'";
        List columnList = jdbcTemplate.queryForList(checkColumnSqlQuery);
        if (columnList.size() > 0) {
            logger.info(columnName + " column already exist in  {}", tableName);
        }else{
            String alterSql = "alter table " + tableName +
                    " ADD COLUMN "+ columnName+ " "+ alterColumnDetails + ";";
            int result = jdbcTemplate.update(alterSql);
            logger.info(columnName + " column added in  {}", tableName);
        }
    }

    /**
     * method to check if ticketId header is exist else create
     * @param columnName
     * @param tableName
     * @return
     */
    public Integer checkIfHeaderExistElseCreate(String columnName, String tableName){
        Integer headerIdx = null;
        int hdrCnt = 0;
        try {
            String checkColumnSqlQuery = "select json_unquote(JSON_SEARCH(header, 'one', '"+columnName+"')) headerIdx, json_length(header) headerLength FROM " + tableName + " LIMIT 1;";
            Map resultMap = jdbcTemplate.queryForMap(checkColumnSqlQuery);
            hdrCnt = resultMap.containsKey("headerLength") && Utils.isNotNull(resultMap.get("headerLength")) ? Integer.parseInt(resultMap.get("headerLength").toString()) : 0;
            if (resultMap.containsKey("headerIdx") && !Utils.isNull(resultMap.get("headerIdx"))) {
                String indexString = resultMap.get("headerIdx").toString();
                String idx = indexString != null ? indexString.substring(indexString.indexOf("[") + 1, indexString.indexOf("]")) : "";
                headerIdx = Utils.notEmptyString(idx) ? Integer.parseInt(idx) : null;
                logger.info(columnName + " header already exist in  {}", tableName);
            } else {
                String alterSql = "UPDATE " + tableName + " SET data=JSON_ARRAY_APPEND(data, '$', \"\"), header=JSON_array_APPEND(header, '$', '" + columnName + "'), meta_data = JSON_array_APPEND(meta_data,'$', \"STRING\") WHERE id > 0";
                logger.info(columnName + " header added in  {}", tableName);
                int[] resultCount = jdbcTemplate.batchUpdate(alterSql);
                headerIdx = hdrCnt; //header count value got fetched before adding the ticketId. So, it will be the index of the ticketId (since jsonLength - 1 is index)
            }
        }catch (Exception e){
            logger.error("exception at checkIfHeaderExistElseCreate. msg {}", e.getMessage());
            e.printStackTrace();
        }
        return headerIdx;
    }
    /**
     * method to get source form event by token
     * @param surveyUniqueId
     * @param token
     * @return
     */
    public String getEventSourceByToken(String surveyUniqueId, String token){
        logger.info("getEventSourceByToken - start");
        try {
            String eventTable = "event_" + surveyUniqueId.replace("-", "_");
            String eventSql = String.format(GET_EVENT_SOURCE_BY_TOKEN, eventTable);
            return jdbcTemplate.queryForObject(eventSql, new Object[]{token}, String.class);
        }catch (Exception e){
            logger.error("exception at getEventSourceByToken, {}", e.getMessage());
        }
        logger.info("getEventSourceByToken - end");
        return "";
    }

    /**
     * Method to get userId who created the triggers by triggerId
     *
     * @param businessUUID
     * @param triggerId
     * @return
     */
    public int getUserByTriggerId(String businessUUID, int triggerId) {
        logger.debug("begin getUserByTriggerId");
        int userId = 0;
        try {
            String query = String.format(GET_USER_BY_TRIGGERID, (businessUUID.replaceAll("-", "_")));
            userId = jdbcTemplate.queryForObject(query, new Object[]{triggerId}, Integer.class);

            logger.debug("end getUserByTriggerId");
        } catch (EmptyResultDataAccessException e) {
            logger.error(e.getMessage());
        }
        return userId;
    }


    /**
     * method to get trigger details by passing triggerId
     *
     * @param triggerId
     * @param businessUniqueId
     */
    public Trigger getTriggerById(String triggerId, String businessUniqueId, String timezone, String userString) {

        Trigger trigger = new Trigger();
        try {
            int userId = getUserIdByUUID(userString);
            int roleId = getRoleIdByUserId(userId);

            Map triggerMap = getTriggerById(businessUniqueId, triggerId);
            if (Utils.isNotNull(triggerMap) && triggerMap.containsKey(Constants.TRIGGER_UUID)) {
                timezone = Utils.notEmptyString(timezone) ? timezone : Constants.TIMEZONE_AMERICA_LOSANGELES;
                trigger.setSurveyName(triggerMap.get(Constants.SURVEY_NAME).toString());
                trigger.setStatus(((int) triggerMap.get(Constants.STATUS)) == 0 ? Constants.STATUS_INACTIVE : Constants.STATUS_ACTIVE);
                trigger.setSurveyId(getSurveyUniqueIdById(Integer.parseInt(triggerMap.get(Constants.SURVEY_ID).toString()), businessUniqueId));
                //trigger.setSurveyId(triggerMap.get(Constants.SURVEY_ID).toString());
                trigger.setTriggerName(triggerMap.get(Constants.TRIGGER_NAME).toString());
                trigger.setTriggerCondition(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_CONDITION)) ? triggerMap.get(Constants.TRIGGER_CONDITION).toString() : Constants.EMPTY);
                trigger.setLastModified(Utils.isNotNull(triggerMap.get(Constants.MODIFIED_TIME)) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.MODIFIED_TIME).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerId(triggerMap.get(Constants.TRIGGER_UUID).toString());
                trigger.setBusinessUniqueId(businessUniqueId);
                trigger.setSurveyStartDate(triggerMap.containsKey(Constants.FROM_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.FROM_DATE).toString(), timezone) : Constants.EMPTY);
                trigger.setSurveyEndDate(triggerMap.containsKey(Constants.TO_DATE) ? Utils.getLocalDateTimeByTimezone(triggerMap.get(Constants.TO_DATE).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerFilterUUID(Utils.isNotNull(triggerMap.get(Constants.TRIGGER_FILTER_UUID)) ? triggerMap.get(Constants.TRIGGER_FILTER_UUID).toString() : Constants.EMPTY);
                ///Map triggerSchedule = triggerDAO.getTriggerScheduleDate(triggerId, businessUniqueId);

                int anonymousValue = (Utils.isNotNull(triggerMap.get(Constants.ANONYMOUS)) && Utils.isStringNumber(triggerMap.get(Constants.ANONYMOUS).toString())) ? Integer.parseInt(triggerMap.get(Constants.ANONYMOUS).toString()) : 0;
                trigger.setAnonymous((anonymousValue==1) ? true : false);


                String startDate = "";
                String startTime = "";
                String endDate = "";
                String endTime = "";
                if ((triggerMap.containsKey(Constants.START_DATE) && Utils.isNotNull(triggerMap.get(Constants.START_DATE))) && (triggerMap.containsKey(Constants.START_TIME) && (Utils.isNotNull(triggerMap.get(Constants.START_TIME))))) {

                    String convertedStartDate = Utils.getLocalDateStringFromUTCDateAndTime(triggerMap.get(Constants.START_DATE).toString(), triggerMap.get(Constants.START_TIME).toString(), timezone);
                    startDate = convertedStartDate.substring(0,10);
                    startTime = convertedStartDate.substring(11,19);
                    trigger.setTriggerStartDate(startDate);
                    trigger.setTriggerStartTime(startTime);
                }else{
                    trigger.setTriggerStartDate(Constants.EMPTY);
                    trigger.setTriggerStartTime(Constants.EMPTY);
                }

                if((triggerMap.containsKey(Constants.END_DATE) && Utils.isNotNull(triggerMap.get(Constants.END_DATE))) && (triggerMap.containsKey(Constants.END_TIME) && (Utils.isNotNull(triggerMap.get(Constants.END_TIME))))){

                    String convertedEndDate = Utils.getLocalDateStringFromUTCDateAndTime(triggerMap.get(Constants.END_DATE).toString(), triggerMap.get(Constants.END_TIME).toString(), timezone);
                    endDate = convertedEndDate.substring(0,10);
                    endTime = convertedEndDate.substring(11,19);
                    trigger.setTriggerEndDate(endDate);
                    trigger.setTriggerEndTime(endTime);
                }else{
                    trigger.setTriggerEndDate(Constants.EMPTY);
                    trigger.setTriggerEndTime(Constants.EMPTY);
                }

                if(roleId != 1 && roleId != 4){
                    Map userSurveyFilterMap = this.getMappedSurveyFiltersByUserId(userId, Integer.parseInt(triggerMap.get(Constants.SURVEY_ID).toString()) , businessUniqueId);
                    if(userSurveyFilterMap.size() > 0){
                        String filterQuery = Utils.isNotNull(userSurveyFilterMap.get("filter_query")) ? userSurveyFilterMap.get("filter_query").toString() : "";
                        String modifiedQuery = this.adjustFilterQuery(filterQuery);
                        trigger.setTriggerParentCondition(modifiedQuery);
                        trigger.setSurveyFilterId(Utils.isNotNull(userSurveyFilterMap.get("survey_filter_uuid")) ? userSurveyFilterMap.get("survey_filter_uuid").toString() : "");
                    }
                }

                /*trigger.setTriggerStartDate((triggerMap.containsKey(Constants.START_DATE) && Utils.isNotNull(triggerMap.get(Constants.START_DATE))) ? Utils.getLocalDateStringFromUTC(triggerMap.get(Constants.START_DATE).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerEndDate((triggerMap.containsKey(Constants.END_DATE) && Utils.isNotNull(triggerMap.get(Constants.END_DATE))) ? Utils.getLocalDateStringFromUTC(triggerMap.get(Constants.END_DATE).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerStartTime((triggerMap.containsKey(Constants.START_TIME) && Utils.isNotNull(Constants.START_TIME)) ? Utils.getLocalTimeStringFromUTC(triggerMap.get(Constants.START_TIME).toString(), timezone) : Constants.EMPTY);
                trigger.setTriggerEndTime((triggerMap.containsKey(Constants.END_TIME) && Utils.isNotNull(Constants.END_TIME)) ? Utils.getLocalTimeStringFromUTC(triggerMap.get(Constants.END_TIME).toString(), timezone) : Constants.EMPTY);*/

                trigger.setFrequency((triggerMap.containsKey(Constants.FREQUENCY) && Utils.isNotNull(triggerMap.get(Constants.FREQUENCY)))? triggerMap.get(Constants.FREQUENCY).toString() : Constants.EMPTY);
            }
        /*if(Utils.isNotNull(trigger) && Utils.isNotNull(trigger.getSurveyId())) {
            // get survey unique id and assign it to the trigger response.
            String surveyUUID = triggerDAO.getSurveyUniqueIdById(Integer.parseInt(trigger.getSurveyId()), businessUniqueId);
            trigger.setSurveyId(surveyUUID);
        }*/

        } catch (Exception e) {
            //e.printStackTrace();
            logger.error(e.getMessage());
        }
        return trigger;
    }


    /**
     * Method to update triggerV1 - DTV-11303
     * @param triggerName
     * @param modifiedTriggerQuery
     * @param surveyName
     * @param surveyId
     * @param userId
     * @param status
     * @param startDate
     * @param endDate
     * @param startTime
     * @param endTime
     * @param frequency
     * @param triggerId
     * @param businessUUID
     * @return
     */
    public int updateTriggerV1(String triggerName, String modifiedTriggerQuery, String surveyName, String surveyId, int userId, int status, String startDate, String endDate, String startTime, String endTime, String frequency, String triggerId, String businessUUID) {
        logger.info("updateTriggerV1 - start");
        int result = 0;
        try{
            final String query = String.format(UPDATE_TRIGGER, businessUUID.replaceAll("-","_"));
            result = jdbcTemplate.update(query, triggerName, modifiedTriggerQuery, surveyName, surveyId, userId, new Timestamp(System.currentTimeMillis()), status, startDate, endDate, startTime, endTime, frequency, triggerId);

        }catch (Exception e){
            logger.error("exception at updateTriggerV1. msg {}", e.getMessage());
        }
        logger.info("updateTriggerV1 - end");
        return result;
    }

    /**
     * DTV-11303,
     * @param triggerUUID
     * @param filterUUID
     * @param businessUUID
     */
    public void updateSurveyFilterUUIDByTriggerID(String triggerUUID, String filterUUID, String businessUUID) {
        logger.info("updateSurveyFilterUUIDByTriggerID - start");
        try{
            String updateSql = "update trigger_%s set trigger_filter_uuid = ? where trigger_uuid = ? ";
            final String query = String.format(updateSql, businessUUID.replaceAll("-", "_"));
            int res = jdbcTemplate.update(query, new Object[]{filterUUID, triggerUUID});
        }catch (Exception e){
            logger.error("exception at updateSurveyFilterUUIDByTriggerID. msg {}", e.getMessage());
        }
        logger.info("updateSurveyFilterUUIDByTriggerID - end");
    }

    /**
     * Function to check if audit program is enabled for a business
     *
     * @param businessUniqueId
     * @return
     */
    public boolean isDTWorksEnabledForBusiness(String businessUniqueId) {
        boolean isEnabled = false;
        logger.info("begin isDTWorksEnabledForBusiness");
        try {
            String sql = "select integrations, close_loop from business_configurations where business_uuid = ?";
            List businessConfigList = jdbcTemplate.queryForList(sql, new Object[]{businessUniqueId});
            if (businessConfigList != null && businessConfigList.size() > 0) {
                Map businessConfigMap = (Map) businessConfigList.get(0);
                isEnabled = businessConfigMap.containsKey("integrations") && Utils.isNotNull(businessConfigMap.get("integrations"))
                        && businessConfigMap.get("integrations").equals("1") && businessConfigMap.containsKey("close_loop") && Utils.isNotNull(businessConfigMap.get("close_loop"))
                        && businessConfigMap.get("close_loop").equals("1") ;
            }
        } catch (Exception e) {
            logger.error("Error while checking if DTWorks is enabled for business {} : msg {} ", businessUniqueId, e.getMessage());
        }
        logger.info("end isDTWorksEnabledForBusiness");
        return isEnabled;
    }

    /**
     * Method to insert failed trigger notification details - dtworks
     *
     * @param triggerNotification
     * @return
     */
    public int createFailedTriggerNotification(final TriggerNotification triggerNotification, String reason) {
        logger.info("begin creating the failed trigger details");
        int result = 0;
//        final String uuid = Utils.generateUUID();
        try {
            result = jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement statement = connection.prepareStatement(INSERT_FAILED_TRIGGER_NOTIFICATION, Statement.RETURN_GENERATED_KEYS);
                    logger.info("trigger notification Bean {}", triggerNotification.toString());
                    int status = Utils.isNotNull(triggerNotification.getStatus()) && triggerNotification.getStatus().equals(Constants.STATUS_ACTIVE) ? 1 : 0;
                    statement.setString(1, triggerNotification.getStartTime());
                    statement.setString(2, triggerNotification.getChannel());
                    statement.setString(3, triggerNotification.getMessage());
                    statement.setInt(4, status);
                    statement.setString(5, new JSONArray(triggerNotification.getContacts()).toString());
                    statement.setInt(6, triggerNotification.isAddFeedback() ? 1 : 0);
                    statement.setInt(7, Utils.isNotNull(triggerNotification.getScheduleId()) ? Integer.parseInt(triggerNotification.getScheduleId()) : 0);
                    statement.setString(8, triggerNotification.getNotificationId());
                    statement.setString(9, triggerNotification.getSubject());
                    statement.setString(10, triggerNotification.getConditionQuery());
                    statement.setString(11, triggerNotification.getSubmissionToken());
                    statement.setString(12, Utils.isNotNull(triggerNotification.getTriggerStatus()) ? (triggerNotification.getTriggerStatus()) : Constants.STATUS_ACTIVE_1);
                    statement.setString(13, Utils.isNotNull(triggerNotification.getActionType()) && Utils.notEmptyString(triggerNotification.getActionType()) ? (triggerNotification.getActionType()) : Constants.EMPTY);
                    statement.setInt(14, triggerNotification.getBusinessId());
                    statement.setString(15, reason);
                    return statement;
                }
            });

            logger.info("end creating failed trigger details");
        } catch (Exception e) {
            logger.error("Exception at createFailedTriggerNotification, {}",e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    /**
     * method to update sent status of the notification by using notifyId
     *
     * @param notifyUUID
     * @param sentStatus   PROCESSING / COMPLETED
     * @return
     */
    public void updateFailedNotificationSentStatusByNotifyId(String notifyUUID, String sentStatus, String businessUUID) {

        logger.debug("begin updating the failed trigger notification sent status");
        try {
            int status = sentStatus.equals(Constants.STATUS_COMPLETED) ? 1 : 0;
            String sql = String.format(UPDATE_FAILED_TRIGGER_NOTIFICATION_SENT_STATUS, businessUUID.replaceAll("-","_"));
            jdbcTemplate.update(sql, new Object[]{status, sentStatus, new Timestamp(System.currentTimeMillis()), notifyUUID});
            logger.debug("end updating the trigger notification sent status");
        } catch (Exception e) {
            logger.error("exception at updateFailedNotificationSentStatusByNotifyId {}",e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Method to get failted trigger notification details for a business and survey
     * @param businessId
     * @param programId
     * @return
     */
    public List getFailedDtworksTriggerNotifications(int businessId, int programId, int scheduleId) {
        logger.info("begin getFailedDtworksTriggerNotifications");
        try {
            List<Integer> scheduleIds = new ArrayList<>();
            if (programId > 0 && scheduleId == 0) {
                //fetch scheduleIds for given programId
                scheduleIds = getScheduledIdsByProgramId(businessId, programId);
            } else {
                scheduleIds.add(scheduleId);
            }
            logger.info("scheduleIds : {}", scheduleIds);
            MapSqlParameterSource param = new MapSqlParameterSource();
            param.addValue("businessId", businessId);
            param.addValue("scheduleIds", scheduleIds);
            List resultList = namedParameterJdbcTemplate.queryForList(GET_FAILED_TRIGGER_NOTIFICATION, param);
            logger.info("end getFailedDtworksTriggerNotifications");
            return resultList;
        } catch (Exception e) {
            logger.error("exception at getFailedDtworksTriggerNotifications {}", e.getMessage());
            e.printStackTrace();
        }
        return Collections.emptyList();
    }



     /* method to create profile in dt profile table
     * @param dtWorksProfileBean
     */
    public void createProfileInDT(DTWorksProfileBean dtWorksProfileBean){
        logger.info("createProfileInDT - start");
        try{
            String businessUniqueIdById = dtWorksProfileBean.getBusinessUniqueIdById();
            String updateSql = "insert into dtworks_profile_"+businessUniqueIdById.replaceAll("-","_")+" (personal_value, personal_value_type, tokens, profile_id, profile_no) values (?, ?, ? ,?,?) ";
            int[] updateResult = jdbcTemplate.batchUpdate(updateSql, new BatchPreparedStatementSetter() {
                List<String> uniqueValue = dtWorksProfileBean.getUniqueValue();
                List<String> type = dtWorksProfileBean.getType();
                public void setValues(PreparedStatement statement, int index) throws SQLException {
                    statement.setString(1, uniqueValue.get(index).toString());
                    statement.setString(2, type.get(index).toString());
                    statement.setString(3, new JSONArray(Collections.singletonList(dtWorksProfileBean.getToken())).toString());
                    statement.setInt(4, dtWorksProfileBean.getProfileId());
                    statement.setString(5, dtWorksProfileBean.getProfileNo());
                }
                public int getBatchSize() {
                    return uniqueValue.size();
                }
            });
            logger.info("Batch update result: {}", Arrays.toString(updateResult));
        }catch (Exception e){
            logger.error("exception at createProfileInDT. msg {}", e.getMessage());
        }
        logger.info("createProfileInDT - end");
    }

    /**
     * method to check if profile exists in dt profile table
     * @param uniqueValues
     * @param types
     * @param businessUniqueId
     * @return
     */
    public Map checkIfProfileExistsInDT(List<String> uniqueValues, List<String> types, String businessUniqueId){
        logger.info("checkIfProfileExistsInDT - start");
        try {
            if (uniqueValues.size() > 0) {
                createProfileTable(businessUniqueId);

                String updateSql = "select json_arrayagg(personal_value) uniqueValues, json_arrayagg(personal_value_type) types, json_arrayagg(profile_id) profileIds, " +
                        " json_arrayagg(profile_no) profileNos from dtworks_profile_" + businessUniqueId.replaceAll("-", "_") + " where " +
                        " personal_value in (:uniqueValues)  and personal_value_type in (:types) ";
                MapSqlParameterSource param = new MapSqlParameterSource();
                param.addValue("uniqueValues", uniqueValues);
                param.addValue("types", types);
                List profileList = namedParameterJdbcTemplate.queryForList(updateSql, param);
                if (profileList.size() > 0) {
                    Map resp = (Map) profileList.get(0);
                    if (resp.containsKey("uniqueValues") && resp.get("uniqueValues") != null) {
                        resp.put("uniqueValues", mapper.readValue(resp.get("uniqueValues").toString(), ArrayList.class));
                        resp.put("types", mapper.readValue(resp.get("types").toString(), ArrayList.class));
                        resp.put("profileIds", mapper.readValue(resp.get("profileIds").toString(), ArrayList.class));
                        resp.put("profileNos", mapper.readValue(resp.get("profileNos").toString(), ArrayList.class));
                        return resp;
                    }
                }
            }
        }catch (Exception e){
            logger.error("exception at checkIfProfileExistsInDT. msg {}", e.getMessage());
        }
        logger.info("checkIfProfileExistsInDT - end");
        return null;
    }

    /**
     * method to get trigger schedule primary id by using its uniqueId
     *
     * @param programId
     * @param businessId
     * @return
     */
    public List<Integer> getScheduledIdsByProgramId(int programId, int businessId) {
        logger.debug("start getScheduledIdsByProgramId");
        List<Integer> resultList = new ArrayList<>();
        try {
            String GET_TRIGGER_SCHEDULE_BY_PROGRAM_ID = "select json_arrayagg(id) as id from trigger_schedule where survey_id = ? and business_id = ? and status = 'active'";
            String result = jdbcTemplate.queryForObject(GET_TRIGGER_SCHEDULE_BY_PROGRAM_ID, new Object[]{programId, businessId}, String.class);
            resultList = Utils.notEmptyString(result) ? mapper.readValue(result, ArrayList.class) : new ArrayList();

        } catch (Exception e) {
            logger.error("exception at getScheduledIdsByProgramId {}", e.getMessage());
        }
        logger.debug("end getScheduledIdsByProgramId");
        return resultList;
    }

     /* Method to create participant table if not exists
     *
     * @param businessUniqueId
     * @return
     */
    public int createProfileTable(String businessUniqueId) {
        int tableCreated = -1;
        logger.info("begin creating profile table ", businessUniqueId);
        try {
            String createSql = "CREATE TABLE IF NOT EXISTS DT.`dtworks_profile_"+businessUniqueId.replaceAll("-","_")+"` (" +
                    "    `id` INT(10) NOT NULL AUTO_INCREMENT," +
                    "    `personal_value`  varchar(100) NULL DEFAULT NULL," +
                    "    `personal_value_type`  VARCHAR(20) NULL DEFAULT NULL," +
                    "    `profile_id` int(10) NULL DEFAULT NULL," +
                    "    `profile_no` VARCHAR(20) NULL DEFAULT NULL," +
                    "    `tokens` JSON DEFAULT NULL," +
                    "    `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP," +
                    "   `modified_time` timestamp NULL," +
                    "    PRIMARY KEY (`id`)" +
                    ") ENGINE=INNODB;";
            tableCreated = jdbcTemplate.update(createSql);
            logger.debug("Create table result {}", tableCreated);

        } catch (Exception e) {
            logger.error("Exception at createProfileTable. Msg {}",e.getMessage());
        }
        logger.info("end creating profile table");
        return tableCreated;
    }

    public String getMetaDataTypeForHeader(String participantGroupUUID, String metadataField) {
        String metaDataType = "";
        try {
            String sql = "select json_extract(meta_data,json_unquote(JSON_SEARCH(header, 'one', ?))) from participant_" +participantGroupUUID.replaceAll("-","_") +
                    " LIMIT 1 ";
            metaDataType = jdbcTemplate.queryForObject(sql, new Object[]{metadataField}, String.class);
        } catch (Exception e) {
            logger.error("Error in getMetaDataTypeForHeader() : " + e);
        }
        return metaDataType;
    }

    public List getParticipantDataFromHeader(String participantGroupUUID, String headerName) {
        logger.debug("getParticipantDataFromHeader - start");
        logger.debug("headerName : " + headerName);
        List participantData = new ArrayList();
        try {
            String sql = "select json_arrayagg(json_extract(data,json_unquote(JSON_SEARCH(header, 'one', ?)))) as data from participant_" +participantGroupUUID.replaceAll("-","_") ;
            Object participantObject = jdbcTemplate.queryForObject(sql, new Object[]{headerName}, Object.class);
            participantData = participantObject != null ? mapper.readValue(participantObject.toString(), ArrayList.class) : new ArrayList();

        } catch (Exception e) {
            logger.error("Error in getParticipantDataFromHeader() : " + e);
        }
        logger.info("getParticipantDataFromHeader - end");
        return participantData;
    }
}

