package com.dropthought.trigger.dao;

import com.dropthought.trigger.common.Utils;
import com.dropthought.trigger.model.DTWorksRecordResponseBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DTWorksDAO {
    /**
     * Logger Factory Initialized
     */
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Object Mapper
     */
    protected final ObjectMapper mapper = new ObjectMapper();
    /**
     * JdbcTemplate for DB Connectivity.
     */
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    private static  final String GET_DTWORKS_CONFIGS = "select user_email,password,tenant_id, access_token, refresh_token,expiration_time from config_dtworks where dt_business_id = ?";
    private static final String UPDATE_DTWORKS_CONFIGS = "update config_dtworks set access_token = ?, refresh_token = ?, expiration_time = ? where tenant_id = ?";

    private static final String UPDATE_DTWORKS_USER_CONFIGS = "update configs_dtworks_users set access_token = ?, refresh_token = ?, expiration_time = ? where dt_user_id = ?";

    public List getDTWorksConfigs(int businessId){
        try{
            return jdbcTemplate.queryForList(GET_DTWORKS_CONFIGS, new Object[]{businessId});

        }catch (Exception e){
            logger.error("getDTWorksConfigs: Error while retrieving the dtworks configs {}", e.getMessage());
        }
        return  null;
    }

    public void updateDTWorksConfigs(String newAccessToken, String newRefreshToken, String dateTime, String tenantId){
        try{
        int updateResult = jdbcTemplate.update(UPDATE_DTWORKS_CONFIGS, new Object[]{newAccessToken, newRefreshToken, dateTime, tenantId});
        }catch (Exception e){
            logger.error("updateDTWorksConfigs: Error while retrieving the dtworks configs {}", e.getMessage());
        }
    }

    public void updateDTWorksUserConfigs(String newAccessToken, String newRefreshToken, String dateTime, int userId){
        try{
            int updateResult = jdbcTemplate.update(UPDATE_DTWORKS_USER_CONFIGS, new Object[]{newAccessToken, newRefreshToken, dateTime, userId});
        }catch (Exception e){
            logger.error("updateDTWorksUserConfigs: Error while retrieving the dtworks configs {}", e.getMessage());
        }
    }

    /**
     * method to get user access details from dtworks user config
     * @param createdBy
     * @return
     */
    public Map getUserAccessByUserId(int createdBy) {
        Map userAccessMap = new HashMap();
        try {
            String query = "select access_token, refresh_token, expiration_time from configs_dtworks_users where dt_user_id = ?";
            userAccessMap = jdbcTemplate.queryForMap(query, new Object[]{createdBy});
        } catch (Exception e) {
            logger.error("exception at getUserAccessByUserId. Msg {}", e.getMessage());
        }
        return userAccessMap;
    }

    /**
     * method to get expired user access details from dtworks user config
     * @return
     */
    public List<Map<String, Object>> getExpiredUserAccessTokes() {
        List<Map<String, Object>> userAccessMap = new ArrayList<>();
        try {
            String query = "SELECT * FROM  configs_dtworks_users WHERE (expiration_time)  < CURRENT_TIMESTAMP " +
                    "AND expiration_time != '' and expiration_time IS NOT NULL; ";
            userAccessMap = jdbcTemplate.queryForList(query);
        } catch (Exception e) {
            logger.error("exception at getExpiredUserAccessTokes. Msg {}", e.getMessage());
        }
        return userAccessMap;
    }

    /**
     * function to create dtworks response table
     * @param businessUniqueId
     */
    public void createDTWorksResonseTable(String businessUniqueId, int businessId){
        try{
            String query = "CREATE TABLE IF NOT EXISTS dtworks_response_"+businessUniqueId.replaceAll("-","_")+
                    " ( `id` int(10) NOT NULL AUTO_INCREMENT, " +
                    " survey_id int(10) NOT NULL, " +
                    " trigger_schedule_id int(10) NULL DEFAULT NULL, " +
                    " meta_data_type varchar(10) NULL DEFAULT NULL, " +
                    " meta_data_value varchar(50) NULL DEFAULT NULL, " +
                    " token varchar(50) NULL DEFAULT NULL, " +
                    " response_data JSON NULL DEFAULT NULL, " +
                    " response_uuid varchar(50) NOT NULL, " +
                    " created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
                    " PRIMARY KEY (`id`)," +
                    " INDEX idx_"+businessId+"_dtworksResp (survey_id, meta_data_type, meta_data_value)" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=UTF8 ";
            jdbcTemplate.execute(query);
        }catch (Exception e){
            logger.error("createDTWorksResonseTable: Error while creating the dtworks response table {}", e.getMessage());
        }
    }

    /**
     * function to create response data in dtworks response table
     * @param dtWorksRecordResponseBean
     */
    public void createDTWorksResponseData(DTWorksRecordResponseBean dtWorksRecordResponseBean) {
        String businessUniqueId = dtWorksRecordResponseBean.getBusinesssUniqueId();
        try {
            String query = "INSERT INTO dtworks_response_" + businessUniqueId.replaceAll("-", "_") +
                    " (survey_id, trigger_schedule_id, meta_data_type, meta_data_value, token, response_uuid, response_data) VALUES (?,?,?,?,?,uuid(),?)";
            jdbcTemplate.update(query, new Object[]{dtWorksRecordResponseBean.getSurveyId(),dtWorksRecordResponseBean.getTriggerScheduleId(),
                    dtWorksRecordResponseBean.getMetaDataType(), dtWorksRecordResponseBean.getMetaDataValue(),
                    dtWorksRecordResponseBean.getToken(), new JSONObject(dtWorksRecordResponseBean.getResponseData()).toString()});
        } catch (Exception e) {
            logger.error("createDTWorksResponseData: Error while creating the dtworks response data {}", e.getMessage());
        }
    }

    public int getDTWorksUserByDTUserId(int dtuserId){
        try {
            String query = "select dtworks_user_id from configs_dtworks_users where dt_user_id = ? limit 1";
            return jdbcTemplate.queryForObject(query, new Object[]{dtuserId}, Integer.class);
        }catch (EmptyResultDataAccessException e){
            logger.error("getDTWorksUserByDTUserId: no record found {}", e.getMessage());
        }catch (Exception e){
            logger.error("getDTWorksUserByDTUserId: Error while retrieving the dtworks user id {}", e.getMessage());
        }
        return 0;

    }

    /**
     * function to get migrated users from dtworks
     * @param users
     * @return
     */
    public List getDTWorksMigratedUsers(List users) {
        try {
            String query = "select json_arrayagg(dt_user_id) from configs_dtworks_users where dt_user_id in (:users)";
            MapSqlParameterSource parm = new MapSqlParameterSource();
            parm.addValue("users", users);
            String reslut = namedParameterJdbcTemplate.queryForObject(query, parm, String.class);
            if(Utils.notEmptyString(reslut )){
                return mapper.readValue(reslut, List.class);
            }
        }catch (EmptyResultDataAccessException e){
            logger.error("getDTWorksUserByDTUserId: no record found {}", e.getMessage());
        }catch (Exception e){
            logger.error("getDTWorksUserByDTUserId: Error while retrieving the dtworks user id {}", e.getMessage());
        }
        return new ArrayList();
    }
}
