package com.dropthought.trigger.common;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.text.StrSubstitutor;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRules;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    /**
     * @param obj
     * @return
     */
    public static boolean isNotNull(Object obj) {
        if (obj != null) {
            if (obj.getClass().equals(String.class)) {
                return obj.toString().length() > 0;
            } else if (obj.getClass().equals(List.class) || obj.getClass().equals(ArrayList.class)) {
                return ((List) obj).size() > 0;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


    public static Boolean isNull(Object obj) {
        return (obj == null) ? true : false;
    }


    /**
     * Function to check if a string is not empty or not null
     *
     * @param input
     * @return
     */
    public static Boolean notEmptyString(String input) {
        Boolean value = false;
        try {
            if (input != null && input.length() > 0) {
                value = true;
            }
        } catch (Exception e) {
            // e.printStackTrace();
            value = false;
        }
        return value;
    }

    /**
     * Function to check if a string is empty or null
     *
     * @param input
     * @return
     */
    public static Boolean emptyString(String input) {
        Boolean value = false;
        try {
            input = input.trim();
            if (input == null || input.length() == 0) {
                value = true;
            }
        } catch (Exception e) {
            value = true;
        }
        return value;
    }

    /**
     * function to convert a datestring specified in a timezone to UTC in the yyyy-MM-dd HH:mm:ss format
     *
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getUTCDateAsString(String dateString, String timeZone) throws ParseException {
        String formattedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        Date date = dateFormat.parse(dateString);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        formattedDate = dateFormat.format(calendar.getTime());
        return formattedDate;
    }

    /**
     * function to convert a datestring specified in a timezone to UTC in the yyyy-MM-dd format
     *
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getUTCDateString(String dateString, String timeZone){
        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date date = dateFormat.parse(dateString);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            formattedDate = dateFormat.format(calendar.getTime());
        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     *
     * @param dateString
     * @param timeString
     * @param timeZone
     * @return
     */
    public static String getUTCDateStringFromDateTime(String dateString, String timeString, String timeZone){
        String formattedDate = "";
        try {
            String inputString = dateString + " "+timeString;
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date date = dateFormat.parse(inputString);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            formattedDate = dateFormat.format(calendar.getTime());


        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * function to convert a time specified in a timezone to UTC in the HH:mm:ss format
     *
     * @param timeString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getUTCTimeAsString(String timeString, String timeZone){
        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Date date = dateFormat.parse(timeString);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            formattedDate = dateFormat.format(calendar.getTime());
        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * function to check if number is convertible to String
     *
     * @param numberString
     * @return
     */
    public static boolean isStringNumber(String numberString) {
        boolean validInteger = true;
        try {
            Double number = Double.parseDouble(numberString);
        } catch (NumberFormatException e) {
            //e.printStackTrace();
            validInteger = false;
        }
        return validInteger;
    }

    /**
     * function to generate UUID
     *
     * @return
     */
    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * function to get UTC  current time in the HH:mm:ss format
     *
     * @return currentDate
     */
    public static String getCurrentUTCDateAsString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        return currentDate;
    }

    /**
     *
     * @param dateString
     * @param dateTime
     * @param timeZone
     * @return
     */
    public static String getLocalDateStringFromUTCDateAndTime(String dateString, String dateTime, String timeZone){
        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            String inputDate = dateString + " " + dateTime;
            Date date = dateFormat.parse(inputDate);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

            formattedDate = dateFormat.format(calendar.getTime());

        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * method to get local timezone date from UTC
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getLocalDateStringFromUTC(String dateString, String timeZone){

        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = dateFormat.parse(dateString);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

            formattedDate = dateFormat.format(calendar.getTime());
        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }
    /**
     * method to get local timezone time from UTC
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getLocalTimeStringFromUTC(String dateString, String timeZone) {

        String formattedDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = dateFormat.parse(dateString);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

            formattedDate = dateFormat.format(calendar.getTime());
        }catch (ParseException e){
            e.printStackTrace();
        }
        return formattedDate;
    }

    /**
     * function to find the difference between the dates
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static long differenceMinutes(String dateString1, String dateString2) {
        long numberOfMins = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            long difference = date2.getTime() - date1.getTime();
            numberOfMins = TimeUnit.MINUTES.convert( difference, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numberOfMins;
    }

    /**
     * method to get local timezone time
     * @return
     * @throws ParseException
     */
    public static String getLocalDateTimeString() {
        String formattedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        formattedDate = dateFormat.format(calendar.getTime());
        return formattedDate;
    }

    /**
     * Function to calculate the total number of pages given a list of items
     *
     * @param sizeOfItems
     * @return
     */
    public static Integer computeTotalPages(Integer sizeOfItems) {
        Integer totalPages = 1;
        Integer itemsPerPage = 10;
        totalPages = (sizeOfItems) / 10;
        if (sizeOfItems % itemsPerPage > 0) {
            totalPages = totalPages + 1;
        }
        return totalPages;
    }

    /**
     * compute particular page number for given index number
     * @param index
     * @return
     */
    public static int computePageNumber(int index) {
        int pageNumber = 1;
        try {
            int quotient = index / 10;
            pageNumber = quotient + 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pageNumber;
    }


    /**
     * method to get local date by timezone from utc
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getLocalDateTimeByTimezone(String dateString, String timeZone) throws ParseException {
        String formattedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = dateFormat.parse(dateString);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
        formattedDate = dateFormat.format(calendar.getTime());
        return formattedDate;
    }

    /**
     * Compare two dates in the format yyyy-MM-dd HH:mm:ss and check if date1 is greater or equal to date2
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static Boolean compareDates(String dateString1, String dateString2) {
        Boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) > 0) {
                returnValue = true;
            } else if (date1.compareTo(date2) == 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    /**
     * Compare two dates in the format yyyy-MM-dd and check if date1 is greater or equal to date2
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static Boolean compareOnlyDates(String dateString1, String dateString2) {
        Boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) > 0) {
                returnValue = true;
            } /*else if (date1.compareTo(date2) == 0) {
                returnValue = true;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }


    /**
     * dateString1 if it is greater it returns true
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static Boolean compareTimes(String dateString1, String dateString2) {
        Boolean returnValue = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            if (date1.compareTo(date2) > 0) {
                returnValue = true;
            } else if (date1.compareTo(date2) == 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;

    }


    /**
     * function to get current time in the HH:mm:ss format
     *
     * @return currentDate
     */
    public static String getUTCCurrentDateAsString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        return currentDate;
    }

    /**
     * Merges two lists to single list
     * and eliminates duplicates
     *
     * @param a
     * @param b
     * @return
     */
    public static List mergeListAndEliminateDuplicates(List a, List b) {
        List result = new ArrayList();
        result.addAll(a);
        result.addAll(b);
        return new ArrayList(new HashSet<>(result));
    }
    /**
     * This returns the list of tokens in a given
     * text based on a template ${token} & also eliminates duplicates
     * in the token
     *
     * @param text
     * @return List of tokens.
     */
    public static List getTokens(String text) {
        List result = new ArrayList();
        StringTokenizer st = new StringTokenizer(text, "${");
        while (st.hasMoreTokens()) {
            String value = st.nextToken().trim();
            if (isNotNull(value) && value.length() > 1 && value.contains("}")) {
                result.add(value.substring(0, value.lastIndexOf("}")));
            }
        }
        return new ArrayList(new HashSet<>(result));
    }
    /**
     * Replace tokens with values
     * and returns the new string
     *
     * @param text
     * @param tokenValue
     * @return
     */
    public static String replaceTokens(String text, Map<String, String> tokenValue) {
        StrSubstitutor sub = new StrSubstitutor(tokenValue);
        return sub.replace(text);

    }

    /**
     *
     * @param dateString
     * @param addHours
     * @return
     */
    public static String getFormattedDateTimeAddHours(String dateString, Integer addHours) {

        String result = null;
        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDateString);

            calendar.add(Calendar.HOUR, addHours);
            result = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }


    /**
     *
     * @param timeZone
     * @return
     * @throws ParseExeption
     */
    public static String getCurrentDateInTimezone(String timeZone){
        String formattedDate = "";
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            Calendar calendar = new GregorianCalendar();
            formattedDate = dateFormat.format(calendar.getTime());
        }catch (Exception e){
            e.printStackTrace();
        }
        return formattedDate;

    }


    /**
     * Handle date inputs to the survey filter in this format
     * 2020-7-1, 2020-7-1 14:59:59
     * <p>
     * Handle date inputs to the survey filter in this format
     * 2020-7-1, 2020-7-1 14:59:59 etc
     *
     * @param dateString
     * @return
     */
    /**
     * Return valid date and time for given date string
     *
     * @param dateToCheck
     * @return
     */
    public static String getValidDateTime(String dateToCheck, boolean formatChange, String changeFormat) {
        String validDate = "";
        try {
            String ymdFormat = "\\d{4}-\\d{2}-\\d{2}"; //2020-03-19
            String ymdFormat2 = "\\d{4}/\\d{2}/\\d{2}"; //2020/03/19
            String ymdFormat3 = "\\d{4}:\\d{2}:\\d{2}"; //2020:03:19
            String mdyFormat1 = "\\d{2}-\\d{2}-\\d{4}"; //07-02-2020
            String mdyFormat2 = "\\d{2}/\\d{2}/\\d{4}"; //07/02/2020
            String mdyFormat3 = "\\d{2}:\\d{2}:\\d{4}"; //07:02:2020
            String mdyFormat4 = "\\d{1}-\\d{1}-\\d{4}"; //7-2-2020
            String mdyFormat5 = "\\d{1}/\\d{1}/\\d{4}"; //7/2/2020
            String mdyFormat6 = "\\d{1}:\\d{1}:\\d{4}"; //7:2:2020
            String ymdFormat4 = "\\d{4}-\\d{1}-\\d{1}"; //2020-3-9
            String ymdFormat5 = "\\d{4}-\\d{1}-\\d{2}"; //2020-3-19
            String ymdFormat6 = "\\d{4}-\\d{2}-\\d{1}"; //2020-11-9

            String ymdHisFormat = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-03-19 22:44:00
            String ymdHisFormat2 = "\\d{4}:\\d{2}:\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020:03:19 22:44:00
            String ymdHisFormat3 = "\\d{4}/\\d{2}/\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020/03/19 22:44:00
            String mdyHisFormat1 = "\\d{2}-\\d{2}-\\d{4} \\d{2}:\\d{2}:\\d{2}"; //07-02-2020 22:44:00
            String mdyHisFormat2 = "\\d{2}:\\d{2}:\\d{4} \\d{2}:\\d{2}:\\d{2}"; //03:19:2020 22:44:00
            String mdyHisFormat3 = "\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}"; //07/02/2020 22:44:00
            String mdyHisFormat4 = "\\d{1}-\\d{1}-\\d{4} \\d{2}:\\d{2}:\\d{2}"; //7-2-2020 22:44:00
            String mdyHisFormat5 = "\\d{1}:\\d{1}:\\d{4} \\d{2}:\\d{2}:\\d{2}"; //3:9:2020 22:44:00
            String mdyHisFormat6 = "\\d{1}/\\d{1}/\\d{4} \\d{2}:\\d{2}:\\d{2}"; //7/2/2020 22:44:00
            String ymdHisFormat4 = "\\d{4}-\\d{1}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-3-9 22:44:00
            String ymdHisFormat5 = "\\d{4}-\\d{1}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-3-19 22:44:00
            String ymdHisFormat6 = "\\d{4}-\\d{2}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Pattern pattern1 = Pattern.compile(ymdFormat);
            Pattern pattern2 = Pattern.compile(ymdFormat2);
            Pattern pattern3 = Pattern.compile(ymdFormat3);
            Pattern pattern4 = Pattern.compile(mdyFormat1);
            Pattern pattern5 = Pattern.compile(mdyFormat2);
            Pattern pattern6 = Pattern.compile(mdyFormat3);

            Pattern pattern7 = Pattern.compile(ymdHisFormat);
            Pattern pattern8 = Pattern.compile(ymdHisFormat2);
            Pattern pattern9 = Pattern.compile(ymdHisFormat3);
            Pattern pattern10 = Pattern.compile(mdyHisFormat1);
            Pattern pattern11 = Pattern.compile(mdyHisFormat2);
            Pattern pattern12 = Pattern.compile(mdyHisFormat3);

            Pattern pattern13 = Pattern.compile(mdyFormat4);
            Pattern pattern14 = Pattern.compile(mdyFormat5);
            Pattern pattern15 = Pattern.compile(mdyFormat6);
            Pattern pattern16 = Pattern.compile(mdyHisFormat4);
            Pattern pattern17 = Pattern.compile(mdyHisFormat5);
            Pattern pattern18 = Pattern.compile(mdyHisFormat6);

            Pattern pattern19 = Pattern.compile(ymdFormat4);
            Pattern pattern20 = Pattern.compile(ymdFormat5);
            Pattern pattern21 = Pattern.compile(ymdFormat6);
            Pattern pattern22 = Pattern.compile(ymdHisFormat4);
            Pattern pattern23 = Pattern.compile(ymdHisFormat5);
            Pattern pattern24 = Pattern.compile(ymdHisFormat6);

            Matcher matcher1 = pattern1.matcher(dateToCheck);
            Matcher matcher2 = pattern2.matcher(dateToCheck);
            Matcher matcher3 = pattern3.matcher(dateToCheck);
            Matcher matcher4 = pattern4.matcher(dateToCheck);
            Matcher matcher5 = pattern5.matcher(dateToCheck);
            Matcher matcher6 = pattern6.matcher(dateToCheck);

            Matcher matcher7 = pattern7.matcher(dateToCheck);
            Matcher matcher8 = pattern8.matcher(dateToCheck);
            Matcher matcher9 = pattern9.matcher(dateToCheck);
            Matcher matcher10 = pattern10.matcher(dateToCheck);
            Matcher matcher11 = pattern11.matcher(dateToCheck);
            Matcher matcher12 = pattern12.matcher(dateToCheck);

            Matcher matcher13 = pattern13.matcher(dateToCheck);
            Matcher matcher14 = pattern14.matcher(dateToCheck);
            Matcher matcher15 = pattern15.matcher(dateToCheck);
            Matcher matcher16 = pattern16.matcher(dateToCheck);
            Matcher matcher17 = pattern17.matcher(dateToCheck);
            Matcher matcher18 = pattern18.matcher(dateToCheck);

            Matcher matcher19 = pattern19.matcher(dateToCheck);
            Matcher matcher20 = pattern20.matcher(dateToCheck);
            Matcher matcher21 = pattern21.matcher(dateToCheck);
            Matcher matcher22 = pattern22.matcher(dateToCheck);
            Matcher matcher23 = pattern23.matcher(dateToCheck);
            Matcher matcher24 = pattern24.matcher(dateToCheck);

            if (matcher1.matches() || matcher2.matches() || matcher3.matches()) {
                if (formatChange) {
                    dateToCheck = dateToCheck.replaceAll(":|/|-", changeFormat);
                }
                validDate = dateToCheck.concat(" 00:00:00");
            }
            if (matcher7.matches() || matcher8.matches() || matcher9.matches()) {
                if (formatChange) {
                    String[] splitDateToCheck = dateToCheck.split(" ");
                    String dateCheck = splitDateToCheck[0];
                    String TimeCheck = splitDateToCheck[1];

                    dateToCheck = dateCheck.replaceAll(":|/|-", changeFormat);
                    TimeCheck = TimeCheck.replaceAll("/|-", ":");
                    dateToCheck = dateToCheck.concat(" ").concat(TimeCheck);
                }
                validDate = dateToCheck;
            }
            if (matcher4.matches()) {
                SimpleDateFormat sdf4 = new SimpleDateFormat("MM-dd-yyyy");
                Date dateSdf4 = sdf4.parse(dateToCheck);
                validDate = sdf.format(dateSdf4);
            }
            if (matcher5.matches()) {
                SimpleDateFormat sdf5 = new SimpleDateFormat("MM/dd/yyyy");
                Date dateSdf5 = sdf5.parse(dateToCheck);
                validDate = sdf.format(dateSdf5);
            }
            if (matcher6.matches()) {
                SimpleDateFormat sdf6 = new SimpleDateFormat("MM:dd:yyyy");
                Date dateSdf6 = sdf6.parse(dateToCheck);
                validDate = sdf.format(dateSdf6);
            }

            if (matcher10.matches()) {
                SimpleDateFormat sdf10 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
                Date dateSdf10 = sdf10.parse(dateToCheck);
                validDate = sdf.format(dateSdf10);
            }
            if (matcher11.matches()) {
                SimpleDateFormat sdf11 = new SimpleDateFormat("MM:dd:yyyy HH:mm:ss");
                Date dateSdf11 = sdf11.parse(dateToCheck);
                validDate = sdf.format(dateSdf11);
            }
            if (matcher12.matches()) {
                SimpleDateFormat sdf12 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date dateSdf12 = sdf12.parse(dateToCheck);
                validDate = sdf.format(dateSdf12);
            }
            if (matcher13.matches()) {
                SimpleDateFormat sdf13 = new SimpleDateFormat("M-d-yyyy");
                Date dateSdf13 = sdf13.parse(dateToCheck);
                validDate = sdf.format(dateSdf13);
            }
            if (matcher14.matches()) {
                SimpleDateFormat sdf14 = new SimpleDateFormat("M/d/yyyy");
                Date dateSdf14 = sdf14.parse(dateToCheck);
                validDate = sdf.format(dateSdf14);
            }
            if (matcher15.matches()) {
                SimpleDateFormat sdf15 = new SimpleDateFormat("M:d:yyyy");
                Date dateSdf15 = sdf15.parse(dateToCheck);
                validDate = sdf.format(dateSdf15);
            }
            if (matcher16.matches()) {
                SimpleDateFormat sdf16 = new SimpleDateFormat("M-d-yyyy HH:mm:ss");
                Date dateSdf16 = sdf16.parse(dateToCheck);
                validDate = sdf.format(dateSdf16);
            }
            if (matcher17.matches()) {
                SimpleDateFormat sdf17 = new SimpleDateFormat("M:d:yyyy HH:mm:ss");
                Date dateSdf17 = sdf17.parse(dateToCheck);
                validDate = sdf.format(dateSdf17);
            }
            if (matcher18.matches()) {
                SimpleDateFormat sdf18 = new SimpleDateFormat("M/d/yyyy HH:mm:ss");
                Date dateSdf18 = sdf18.parse(dateToCheck);
                validDate = sdf.format(dateSdf18);
            }

            if(matcher19.matches()){
                try{
                    SimpleDateFormat sdf19 = new SimpleDateFormat("yyyy-M-d");
                    Date date1 = sdf19.parse(dateToCheck);
                    //convertedDate = date.toString();
                    validDate = sdf.format(date1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if(matcher20.matches()){
                try{
                    SimpleDateFormat sdf20 = new SimpleDateFormat("yyyy-M-dd");
                    //SimpleDateFormat sdf20 = new SimpleDateFormat("yyyy-M-d HH:mm:ss");
                    Date date2 = sdf20.parse(dateToCheck);
                    //convertedDate = date2.toString();
                    validDate = sdf.format(date2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (matcher21.matches()) {
                try {
                    SimpleDateFormat sdf21 = new SimpleDateFormat("yyyy-MM-d");
                    Date date3 = sdf21.parse(dateToCheck);
                    //convertedDate = date2.toString();
                    validDate = sdf.format(date3);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (matcher22.matches()) {
                try {
                    SimpleDateFormat sdf22 = new SimpleDateFormat("yyyy-M-d HH:mm:ss");
                    Date date4 = sdf22.parse(dateToCheck);
                    //convertedDate = date2.toString();
                    validDate = sdf.format(date4);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (matcher23.matches()) {
                try {
                    SimpleDateFormat sdf23 = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
                    Date date5 = sdf23.parse(dateToCheck);
                    //convertedDate = date2.toString();
                    validDate = sdf.format(date5);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (matcher24.matches()) {
                try {
                    SimpleDateFormat sdf24 = new SimpleDateFormat("yyyy-MM-d HH:mm:ss");
                    Date date6 = sdf24.parse(dateToCheck);
                    //convertedDate = date2.toString();
                    validDate = sdf.format(date6);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return validDate;
    }


    /**
     * function to find the difference between the dates
     *
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static long differenceDays(String dateString1, String dateString2) {
        long numberOfDays = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            long difference = date2.getTime() - date1.getTime();
            numberOfDays = difference / (24 * 60 * 60 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    /**
     * method to get current date and previous date in UTD
     * @return
     */
    public static Map getFormattedFromToDate() {
        Map result = new HashMap();
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));

            Calendar calendar = Calendar.getInstance();
            result.put("toDate", df.format(calendar.getTime())); // current date
            calendar.add(Calendar.HOUR, -24);
            result.put("fromDate", df.format(calendar.getTime())); // previous date

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get date string
     *
     * @param dateString
     * @param timeZone
     * @return
     * @throws ParseException
     */
    public static String getDateAsStringByTimezone(String dateString, String timeZone) throws ParseException {

        String formattedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = dateFormat.parse(dateString);

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        formattedDate = dateFormat.format(calendar.getTime());
        return formattedDate;
    }

    /**
     * Get response date format
     * ex : 29-Sep-2020 11:31 AM
     * @param dateString
     * @return
     */
    public static String getFormattedDate3(String dateString) {
        String result = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d = sdf.parse(dateString);
            sdf.applyPattern("dd-MMM-yyyy hh:mm a");
            result = sdf.format(d);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * function to check if the provided arguments are dates or not.
     * @param dateString1
     * @param dateString2
     * @return
     */
    public static boolean checkValidDate(String dateString1,String dateString2) {
        boolean validDate = false;
        try {
            String ymdFormat = "\\d{4}-\\d{1}-\\d{1}"; //2020-3-9
            String ymdFormat1 = "\\d{4}-\\d{1}-\\d{2}"; //2020-3-19
            String ymdFormat2 = "\\d{4}-\\d{2}-\\d{1}"; //2020-11-9
            String ymdFormat3 = "\\d{4}-\\d{2}-\\d{2}"; //2020-11-9
            String ymdHisFormat = "\\d{4}-\\d{1}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-3-9 22:44:00
            String ymdHisFormat1 = "\\d{4}-\\d{1}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-3-19 22:44:00
            String ymdHisFormat2 = "\\d{4}-\\d{2}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00
            String ymdHisFormat3 = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00

            Pattern pattern1 = Pattern.compile(ymdFormat);
            Pattern pattern2 = Pattern.compile(ymdHisFormat);
            Pattern pattern3 = Pattern.compile(ymdFormat1);
            Pattern pattern4 = Pattern.compile(ymdHisFormat1);
            Pattern pattern5 = Pattern.compile(ymdFormat2);
            Pattern pattern6 = Pattern.compile(ymdHisFormat2);
            Pattern pattern7 = Pattern.compile(ymdFormat3);
            Pattern pattern8 = Pattern.compile(ymdHisFormat3);

            Matcher matcher1 = pattern1.matcher(dateString1);
            Matcher matcher2 = pattern2.matcher(dateString1);
            Matcher matcher3 = pattern3.matcher(dateString1);
            Matcher matcher4 = pattern4.matcher(dateString1);
            Matcher matcher5 = pattern5.matcher(dateString1);
            Matcher matcher6 = pattern6.matcher(dateString1);
            Matcher matcher7 = pattern6.matcher(dateString1);
            Matcher matcher8 = pattern6.matcher(dateString1);

            Matcher matcher9 = pattern1.matcher(dateString2);
            Matcher matcher10 = pattern2.matcher(dateString2);
            Matcher matcher11 = pattern3.matcher(dateString2);
            Matcher matcher12 = pattern4.matcher(dateString2);
            Matcher matcher13 = pattern5.matcher(dateString2);
            Matcher matcher14 = pattern6.matcher(dateString2);
            Matcher matcher15 = pattern7.matcher(dateString2);
            Matcher matcher16 = pattern8.matcher(dateString2);

            if (matcher1.matches() || matcher2.matches() || matcher3.matches() || matcher4.matches() || matcher5.matches() || matcher6.matches() || matcher7.matches() || matcher8.matches()) {
                if(matcher9.matches() || matcher10.matches() || matcher11.matches() || matcher12.matches() || matcher13.matches() || matcher14.matches() || matcher15.matches() || matcher16.matches()) {
                    validDate = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return validDate;
    }



    /**
     * Handle date inputs to the survey filter in this format
     * 2020-7-1, 2020-7-1 14:59:59
     * <p>
     * Handle date inputs to the survey filter in this format
     * 2020-7-1, 2020-7-1 14:59:59 etc
     *
     * @param dateString
     * @return
     */
    public static String validateSurveyFilterDates(String dateString) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String convertedDate = dateString;

        String ymdFormat = "\\d{4}-\\d{1}-\\d{1}"; //2020-3-9
        String ymdFormat1 = "\\d{4}-\\d{1}-\\d{2}"; //2020-3-19
        String ymdFormat2 = "\\d{4}-\\d{2}-\\d{1}"; //2020-11-9
        String ymdHisFormat = "\\d{4}-\\d{1}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-3-9 22:44:00
        String ymdHisFormat1 = "\\d{4}-\\d{1}-\\d{2} \\d{2}:\\d{2}:\\d{2}"; //2020-3-19 22:44:00
        String ymdHisFormat2 = "\\d{4}-\\d{2}-\\d{1} \\d{2}:\\d{2}:\\d{2}"; //2020-11-9 22:44:00

        Pattern pattern1 = Pattern.compile(ymdFormat);
        Pattern pattern2 = Pattern.compile(ymdHisFormat);
        Pattern pattern3 = Pattern.compile(ymdFormat1);
        Pattern pattern4 = Pattern.compile(ymdHisFormat1);
        Pattern pattern5 = Pattern.compile(ymdFormat2);
        Pattern pattern6 = Pattern.compile(ymdHisFormat2);

        Matcher matcher1 = pattern1.matcher(dateString);
        Matcher matcher2 = pattern2.matcher(dateString);
        Matcher matcher3 = pattern3.matcher(dateString);
        Matcher matcher4 = pattern4.matcher(dateString);
        Matcher matcher5 = pattern5.matcher(dateString);
        Matcher matcher6 = pattern6.matcher(dateString);

        if(matcher1.matches()){
            try{
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-M-d");
                Date date1 = dateFormat1.parse(dateString);
                //convertedDate = date.toString();
                convertedDate = simpleDateFormat.format(date1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(matcher2.matches()){
            try{
                SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-M-d HH:mm:ss");
                Date date2 = dateFormat2.parse(dateString);
                //convertedDate = date2.toString();
                convertedDate = simpleDateFormat.format(date2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (matcher3.matches()) {
            try {
                SimpleDateFormat dateFormat3 = new SimpleDateFormat("yyyy-M-dd");
                Date date3 = dateFormat3.parse(dateString);
                //convertedDate = date2.toString();
                convertedDate = simpleDateFormat.format(date3);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (matcher4.matches()) {
            try {
                SimpleDateFormat dateFormat4 = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
                Date date4 = dateFormat4.parse(dateString);
                //convertedDate = date2.toString();
                convertedDate = simpleDateFormat.format(date4);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (matcher5.matches()) {
            try {
                SimpleDateFormat dateFormat5 = new SimpleDateFormat("yyyy-MM-d");
                Date date5 = dateFormat5.parse(dateString);
                //convertedDate = date2.toString();
                convertedDate = simpleDateFormat.format(date5);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (matcher6.matches()) {
            try {
                SimpleDateFormat dateFormat6 = new SimpleDateFormat("yyyy-MM-d HH:mm:ss");
                Date date6 = dateFormat6.parse(dateString);
                //convertedDate = date2.toString();
                convertedDate = simpleDateFormat.format(date6);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return convertedDate;

    }

    /**
     * URL encode text analytics categories and sentiments
     * @param textAnalyticsInput
     * @return
     */
    public static String URLEncodeForTextAnalytics(String textAnalyticsInput){
        String returnString = "";
        try{
            if(notEmptyString(textAnalyticsInput)){
                String[] listOfStrings = textAnalyticsInput.split(",");
                int length = listOfStrings.length;
                for(int i = 0;i<length;i++){
                    String eachString = listOfStrings[i];
                    if(!eachString.equals("all")){
                        returnString += URLEncoder.encode(eachString);
                        if(!(i==length-1)){
                            returnString += ",";
                        }
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return returnString;
    }

    /**
     * method to add given minutes to the given date
     * @param dateString
     * @param addMinutes
     * @return
     */
    public static String addMinutesToDate(String dateString, int addMinutes){
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            calendar.add(Calendar.MINUTE, addMinutes);
            dateString = df.format(calendar.getTime());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateString;
    }

    /**
     * method to convert timestamp to  yyyy-MM-dd HH:mm:ss
     *
     * @param timestamp
     * @return
     */
    public static String getFormattedDateFromTimestamp(String timestamp) {
        String formattedDateStr = "";
        if (notEmptyString(timestamp)) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date fromatedDt = dateFormat.parse(timestamp);
                formattedDateStr = dateFormat.format(fromatedDt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return formattedDateStr;
    }

    /**
     * function to decode base64 to String
     *
     * @param encodedString
     * @return
     */
    public static String decodeString(String encodedString) {
        String decodedString = "";
        if (encodedString.length() > 0) {
            byte[] valueDecoded = Base64.decodeBase64(encodedString.getBytes());
            decodedString = new String(valueDecoded, Charset.forName("UTF-8"));
        }
        return decodedString;
    }

    public static String removeExtraChar(String s) {
        //replace + and ( ) with space - dtworks now allow this in phone number
        return s.replaceAll("[+()]", "");
    }

    /**
     * Method to check if the date is in DST (Day Light Saving Time)
     * @param dateStr
     * @param timeZone
     * @return
     */
    public static boolean isDateInDST(String dateStr, String timeZone) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, formatter);

        ZoneId zoneId = ZoneId.of(timeZone);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, zoneId);

        ZoneRules zoneRules = zoneId.getRules();

        return zoneRules.isDaylightSavings(zonedDateTime.toInstant());
    }

    /**
     * function to remove html tags from a string using pattern matching
     * @param inputString
     * @return
     */
    public static String removeAllHtmlEntities(String inputString) {
        String cleanedString = "";
        if (Utils.notEmptyString(inputString)) {
            String htmlTagRegex = "<[^>]*>";
            Pattern pattern = Pattern.compile(htmlTagRegex);
            Matcher matcher = pattern.matcher(inputString);
            String htmlString = matcher.replaceAll("");

            Pattern pattern2 = Pattern.compile("&[a-zA-Z]+;");
            Matcher matcher2 = pattern2.matcher(htmlString);
            cleanedString = matcher2.replaceAll("");
        }
        return cleanedString;
    }
}
