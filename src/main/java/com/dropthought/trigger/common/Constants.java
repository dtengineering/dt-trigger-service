package com.dropthought.trigger.common;

import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String SUCCESS = "success";
    public static final String OK = "ok";
    public static final String FAILED = "failed";
    public static final String INVALID = "INVALID";
    public static final String ERROR = "error";
    public static final String RESULT = "result";
    public static final String ALL = "all";
    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_INACTIVE = "inactive";
    public static final String STATUS_EXPIRED= "expired";
    public static final String STATUS_COMPLETED = "completed";
    public static final String STATUS_PROCESSING="processing";
    public static final String STATUS_FAILED = "failed";
    public static final String STATUS_TO_BE_PROCESS="tobeprocess";
    public static final String STATUS_TODO="todo";
    public static final String OTHER="Other";

    public static final String STATUS_ACTIVE_1 = "1";
    public static final String STATUS_INACTIVE_0 = "0";
    public static final String STATUS_ACTIVE_2 = "2";
    public static final String EMPTY="";
    public static final String TIMEZONE_AMERICA_LOSANGELES="America/Los_Angeles";

    public static final String ID = "id";
    public static final String TRIGGER_UUID = "trigger_uuid";
    public static final String SURVEY_NAME = "survey_name";
    public static final String SURVEY_NAMES = "survey_names";
    public static final String TRIGGER_NAME = "trigger_name";
    public static final String TRIGGER_CONDITION = "trigger_condition";
    public static final String SURVEY_ID = "survey_id";
    public static final String SURVEY_UUID = "survey_uuid";
    public static final String STATUS = "status";
    public static final String CONTACTS = "contacts";
    public static final String DEACTIVATED_CONTACTS = "deactivated_contacts";
    public static final String CHANNEL = "channel";
    public static final String MESSAGE = "message";
    public static final String REPLYRESPONDENT = "reply_respondent";
    public static final String SUBJECT = "subject";
    public static final String SCHEDULE_ID = "schedule_id";
    public static final String ADD_FEEDBACK = "add_feedback";
    public static final String START_TIME = "start_time";
    public static final String CONDITION = "condition";
    public static final String START_DATE = "start_date";
    public static final String END_DATE = "end_date";
    public static final String END_TIME = "end_time";
    public static final String FREQUENCY = "frequency";
    public static final String LAST_PROCCESSED_DATE = "last_processed_date";
    public static final String LAST_PROCCESSED_TIME = "last_processed_time";
    public static final String BUSINESS_ID = "business_id";
    public static final String TRIGGER_CONFIG = "trigger_config";
    public static final String TIMEZONE = "timezone";
    public static final String FREQUENCY_SINGLE = "single";
    public static final String FREQUENCY_MULTIPLE = "multiple";
    public static final String CHANNEL_EMAIL = "email";
    public static final String CHANNEL_PUSH = "push";
    public static final String CHANNEL_REPLY = "reply";
    public static final String CHANNEL_WEBHOOK = "webhook";
    public static final String CHANNEL_HUBSPOT = "hubspot";
    public static final String CHANNEL_MICROSOFT_DYNAMICS = "microsoftDynamics";
    public static final String CHANNEL_SALESFORCE = "salesforce";
    public static final String CHANNEL_BAMBOOHR = "bambooHR";
    public static final String CHANNEL_SLACK = "slack";
    public static final String CHANNEL_FRESHDESK = "freshdesk";
    public static final String CHANNEL_ZENDESK = "zendesk";
    public static final String CHANNEL_JIRA = "jira";
    public static final String CHANNEL_FOCUS_AREA = "focusArea";
    public static final String CREATED_BY = "created_by";
    public static final String CREATED_TIME = "created_time";
    public static final String CHANNEL_DTWORKS = "dtworks";
    public static final String CHANNEL_247SOFTWARE = "software_247";
    public static final String MODIFIED_BY = "modified_by";
    public static final String MODIFIED_TIME = "modified_time";
    public static final String SCHEDULE_UUID="schedule_uuid";
    public static final String NOTIFICATION_UUID="notification_uuid";
    public static final String DTMETADATA ="dtmetadata";
    public static final String DTLINK = "dtlink";
    public static final String DTQR = "dtqr";
    public static final String DTKIOSK = "dtkiosk";
    public static final String DTWORKS_TICKET_ID = "dtWorksTicketId";
    public static final String SOFTWARE_INCIDENT_ID = "softwareIncidentId";
    public static final String DUMMY_EMAIL = "CAG@yopmail.com";
    public static final String DUMMY_PHONE = "91123456789";
    public static final String DUMMY = "dummy";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";

    public static final String SCORE="score";
    public static final String PERFORMANCE="dtPerformance";
    public static final String DT_AGGREGATE="dtAggregate";
    public static final String QUESTION = "question";
    public static final String OR=" or ";
    public static final String AND=" and ";
    public static final String CONDITIONQUERY ="conditionQuery";
    public static final String TOKENS="tokens";
    public static final String CONDITION_QUERY ="condition_query";
    public static final String TRIGGER_ID="trigger_id";
    public static final String SENT = "1";
    public static final String TOTAL_RECORDS = "totalRecordsState";
    public static final String ALL_RECORDS = "allRecords";
    public static final String TOTAL_PAGES= "totalPages";
    public static final String CURRENT_PAGE_RECORDS = "currentPageRecords";
    public static final String NAME = "Name";
    public static final String LAST_ACTION_TIME = "last_action_time";
    public static final String FROM_DATE = "from_date";
    public static final String TO_DATE = "to_date";
    public static final String TRIGGER_START_DATE = "triggerStartDate";
    public static final String TRIGGER_END_DATE = "triggerEndDate";
    public static final String TRIGGER_START_TIME = "triggerStartTime";
    public static final String TRIGGER_END_TIME = "triggerEndTime";
    public static final String HOST = "host";
    public static final String BETWEEN_OPERATOR="~";
    public static final String AND_OPERATOR="&&";
    public static final String OR_OPERATOR="||";
    public static final String READABLE_QUERY="readable_query";
    public static final String TRIGGER_FILTER_UUID="trigger_filter_uuid";
    public static final String CATEGORY="category";
    public static final String CATEGORIES="categories";
    public static final String SENTIMENT="sentiment";
    public static final String RESPONSES="responses";
    public static final String DOLLAR_DELIMITER="$$";
    public static final String QUESTION_ID="questionId";
    public static final String SUBMISSIONTOKEN="submissionToken";
    public static final String DT_QUERY="dtQuery";
    public static final String PERFORMANCE_QUERY="performanceQuery";
    public static final String METADATA="metadata";
    public static final String TXT_ANALYTICS="txtAnalytics";
    public static final String DT_PERFORMANCE="dtPerformanceQuery";
    public static final String FEEDBACK_LIST="feedbackDataList";
    public static final String PARTICIPANT_GROUP_ID="participant_group_id";
    public static final String HEADER_DATA="header_data";
    public static final String WEBHOOK_CONFIG="webhook_config";
    public static final String INTEGRATIONS_CONFIG="integrations_config";
    public static final String DEACTIVATED_CONDITIONS="deactivated_conditions";
    public static final String RELEVANT_METADA = "relevant_metadata";
    public static final String SAMPLE_CSV = "sample_csv";
    public static final String DELIMITER="~~";
    public static final String TA_BASIC="basic";
    public static final String TA_ADVANCED="advanced";
//    public static final String RANkKING="ranking";
    public static final String MULTI_CHOICE="multiChoice";
    public static final String SINGLE_CHOICE="singleChoice";
    public static final String OPEN_ENDED="open";
    public static final String RATING="rating";
    public static final String RANKING ="ranking";
    public static final String PICTURE_CHOICE ="pictureChoice";
    public static final String POLL ="poll";
    public static final String RATINGSLIDER = "ratingSlider";
    public static final String MATRIX_RATING="matrixRating";
    public static final String MATRIX_CHOICE="matrixChoice";
    public static final String MULTI_OPENENDED="multipleOpenEnded";
    public static final String CONTAINS="contains";
    public static final String NOTCONTAINS="donotcontain";
    public static final String ANSWERED="answered";
    public static final String UNANSWERED="unanswered";
    public static final String DROP_DOWN="dropdown";
    public static final String METADATA_TYPE ="metaDataType";
    public static final String QUESTION_TITLE ="questionTitle";
    public static final String QUESTION_TITLES ="questionTitles";
    public static final String TYPE = "type";

    //push notificaiton param
    public static final String MESSAGE_LIST = "messageList";
    public static final String USEREMAILS = "userEmails";
    public static final String RESPONDENT_ID = "respondentId";
    public static final String RESPONDENT_NAME = "respondentName";
    public static final String TIME_STAMP = "timestamp";
    public static final String SURVEYUUID = "surveyUUID";
    public static final String DEEP_LINK = "deepLink";
    public static final String DATA = "data";
    public static final String APPNAME = "appName";
    public static final String EMAILS = "emails";
    public static final String TITLE = "title";
    public static final String PUSHMESSAGE = "message";
    public static final String DEFAULT_LOGO = "https://dt-program-banner.s3.us-west-1.amazonaws.com/a597f131-1cfe-4382-9780-2189425da87d-image.png";
    public static final String DEFAULT_HEXCODE = "#4b3693";

    public static final String TOKEN_COUNT="tokens";
    public static final String FORMATTED_DATE="formattedDate";
    public static final String ISAGGREGATE="isAggregate";
    public static final String ISFREQUENCY="isFrequency";
    public static final String SUBMISSION_TOKEN="submission_token";
    public static final String ISCONDITIONMET="isConditionMet";

    public static final String EXEC_START_TIME = "exec_start_time";
    public static final String EXEC_END_TIME = "exec_end_time";
    public static final String LIST = "list";

    public static final String TRIGGER_NAME_EXIST ="TRIGGER_NAME_EXIST";
    public static final String NOT_FOUND="NOT_FOUND";

    //DTV-8038
    public static final String ACTION_TYPE = "action_type";
    public static final String ACTION_TYPE_INSERT = "insert";
    public static final String ACTION_TYPE_DELETE = "delete";
    public static final String UPLOAD ="upload";
    public static final String FILE = "file";

    public static String YYYY_MM_DD_HH_MM_SS="yyyy-MM-dd HH:mm:ss";
    public static String YYYY_MM_DD = "yyyy-MM-dd";
    public static String LANGUAGE_EN="en";
    public static String UTC = "UTC";

    public static final String BUSINESS_UUID = "business_uuid";
    public static final String BUSINESSID = "businessId";
    public static final String BUSINESSNAME = "businessName";
    public static final String SURVEYNAME = "surveyName";
    public static final String SURVEYID = "surveyId";
    public static final String FAILURE = "FAILURE";
    public static final String REST_CALL_STATUS = "STATUS";
    public static final String ANONYMOUS = "anonymous";
    public static final String TEMPLATE_NAME = "template_name";
    public static final String TEMPLATE_ID = "template_id";
    public static final String DROPTHOUGHT_TYPE="dropthought";

    public static final String POSITIVE ="positive";
    public static final String NEGATIVE ="negative";
    public static final String NEUTRAL ="neutral";
    public static final String NPS ="nps";
    public static final String METRIC ="metric";
    public static final String STATEMENT ="statement";
    public static final String RUNNER_TRANSACTION_ID = "rcorrelationId";
    public static final String TRANSACTION_ID = "correlationId";
    public static final String WORKER_TRANSACTION_ID = "worrelationId";

    public static final String WORKFLOWS = "workflows";

    public static final String TRIGGER_USAGE_KINGUSER_ERROR = "Your account has hit the limit of %s Triggers. \nPlease contact cs@dropthought.com to increase your limit. \nYou can also delete or turn off an active trigger to set a new one.";
    public static final String TRIGGER_USAGE_REGULARUSER_ERROR = "Your account has hit the limit of %s triggers. \nTo increase this, please contact your admin at %s. \nYou can also delete or turn off an active trigger to set a new one";

    public static final String EMAIL_USAGE_KINGUSER_ERROR = "Your account has hit the limit of %s email invites. \nPlease contact cs@dropthought.com to increase your limit.";
    public static final String EMAIL_USAGE_REGULARUSER_ERROR = "Your account has hit the limit of %s email invites. To increase this, please contact your admin at %s";

    public static final String LIST_USAGE_KINGUSER_ERROR = "You have reached the number of lists limits you have for your account. \nYou can delete a list to free up your limit or you can contact your Customer Success Executive at cs@dropthought.com to increase the limit.";
    public static final String LIST_USAGE_REGULARUSER_ERROR = "You have reached the number of lists limits you have for your account. \nYou can delete a list to free up your limit or you can contact your admin at %s to increase the limit.";

    public static final Map<String, Integer> questionTypes = new HashMap();
    public static final Map<String,String> DTWORSK_CONTACTPREFERENCE = new HashMap<>();
    public static final Map<String,String> DTWORSK_ADDRZONE = new HashMap<>();
    public static final Map<String,Integer> DTWORSK_STATEMENT = new HashMap<>();
    static {
        questionTypes.put(RATING, 1);
        questionTypes.put(OPEN_ENDED, 2);
        questionTypes.put(SINGLE_CHOICE, 3);
        questionTypes.put(MULTI_CHOICE, 4);
        questionTypes.put(NPS, 5);
        questionTypes.put(STATEMENT, 6);
        questionTypes.put(RANKING, 7);
        questionTypes.put(RATINGSLIDER, 8);
        questionTypes.put(DROP_DOWN, 9);
        questionTypes.put(MATRIX_RATING, 10);
        questionTypes.put(MATRIX_CHOICE, 11);
        questionTypes.put(MULTI_OPENENDED, 12);
        questionTypes.put(PICTURE_CHOICE, 13);
        questionTypes.put(FILE, 14);
        questionTypes.put(POLL, 15);

        DTWORSK_CONTACTPREFERENCE.put("email","CNT_PREF_EMAIL");
        DTWORSK_CONTACTPREFERENCE.put("sms","CNT_PREF_SMS");
        DTWORSK_CONTACTPREFERENCE.put("whatsapp","CNT_PREF_WHATSAPP");

        DTWORSK_ADDRZONE.put("Near The Social Tree", "NEAR_THE_SOCIAL_TREE");
        DTWORSK_ADDRZONE.put("Near The Cocoa Trees", "NEAR_THE_COCOA_TREES");
        DTWORSK_ADDRZONE.put("Behind Lacoste", "BEHIND_LACOSTE");
        DTWORSK_ADDRZONE.put("Beside Texas Chicken", "BESIDE_TEXAS_CHICKEN");
        DTWORSK_ADDRZONE.put("Beside Turtle", "BESIDE_TURTLE");

        DTWORSK_STATEMENT.put("sewage waters are coming out need to be cleaned", 17);
        DTWORSK_STATEMENT.put("pediatric nasal drops is always out of stock", 120);
        DTWORSK_STATEMENT.put("process advertising company registration applications crossed sla of 4 days", 125);
        DTWORSK_STATEMENT.put("hindering the promotion of malay language due to limited funding", 127);
        DTWORSK_STATEMENT.put("less employment opportunity is affecting the survival of the bruneians", 129);
    }
}
