package com.dropthought.trigger.constants;

import java.util.HashMap;
import java.util.Map;

public class DTConstants {

    public static final String UUID_REGREX = "^[0-9a-zA-Z]{8}-([0-9a-zA-Z]{4}-){3}[0-9a-zA-Z]{12}+$";

    public static final String ONLY_NUMBERS = "^[0-9]$";

    public static final String ONLY_ALPHABETS = "^[a-zA-Z]$";

    public static final Map<String, Object> INTERACTION_CODE_MAP = new HashMap<>();

    static { //have to pass code to dtworks interaction payload
        INTERACTION_CODE_MAP.put("Service Related ", "SERVICE_RELATED"); //interaction category
        INTERACTION_CODE_MAP.put("Application Maintenance", "ST_APPMAINTENANCE"); // service Type
        INTERACTION_CODE_MAP.put("Admin Services", "PST_ADMINSRV"); //service category
        INTERACTION_CODE_MAP.put("High", "PRTYHGH"); // priority high
        INTERACTION_CODE_MAP.put("Low", "PRTYLOW"); // priority low
        INTERACTION_CODE_MAP.put("Medium", "PRTYMED"); // priority medium
        INTERACTION_CODE_MAP.put("General", "GENERAL"); // interaction type
    }
}
