package com.dropthought.trigger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class AsyncConfiguration {
    private ThreadPoolTaskExecutor executor;

    @Bean
    public Executor asyncExecutor() {
        if(executor == null) {
            synchronized (this) {
                executor = new ThreadPoolTaskExecutor();
                /**
                 * If the number of threads is less than the corePoolSize, create a new Thread to run a new task.
                 * If the number of threads is equal (or greater than) the corePoolSize, put the task into the queue.
                 * If the queue is full, and the number of threads is less than the maxPoolSize, create a new thread to run tasks in.
                 * If the queue is full, and the number of threads is greater than or equal to maxPoolSize, reject the task.
                 */
                executor.setCorePoolSize(4);
                //executor.setMaxPoolSize(8);
                executor.setQueueCapacity(100);
                executor.setThreadNamePrefix("Executor::");
                executor.initialize();
            }
        }
        return executor;
    }
}
