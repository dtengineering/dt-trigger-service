package com.dropthought.trigger.config;

import com.dropthought.trigger.interceptor.TransactionInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.*;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

   /* @Autowired
    Environment env;
*/
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST","PUT", "DELETE");;
    }

    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new TransactionInterceptor());
    }*/

    /*@Bean
    public FilterRegistrationBean<CustomAPIFilter> filterRegistrationBean() {

        FilterRegistrationBean<CustomAPIFilter> registrationBean = new FilterRegistrationBean();
        CustomAPIFilter customURLFilter = new CustomAPIFilter(env);

        registrationBean.setFilter(customURLFilter);
        registrationBean.addUrlPatterns("/api/v1/*");
        return registrationBean;
    }*/
}
