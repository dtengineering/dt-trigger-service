package com.dropthought.trigger.service;


import com.dropthought.trigger.dao.TriggerDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.*;

import java.util.*;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class TriggerServiceTest {

    @InjectMocks
    private TriggerService triggerService;

    @Mock
    TriggerDAO triggerDAO;


    @BeforeEach
    public void before() throws NoSuchFieldException, IllegalAccessException {
        MockitoAnnotations.initMocks(this);
    }
    /**
     * Test method to test getQuestionSubQuery for open ended question responses with keywords based trigger
     */

    @Test
    public void testOpenEndedQuestionKeywordsQuestionSubQuery() {
        String query = "question.4d99a3c4-7b10-4fc7-af6f-29727a82f8bd.75bb54d7-776a-48dd-9d66-26cd8ffcdf06.responses=naveen,kumar,keywords,check,trigger";
        String businessUUID = "12d6f623-2163-4117-9048-ad6dfeaaf120";
        int surveyId = 482;
        Mockito.when(triggerDAO.getQuestionDataBySurveyId(anyInt(), anyString(), anyString())).thenReturn(mockOpenEndedQuestionData());
        String expectedQuery = triggerService.getQuestionSubQuery(query, businessUUID, surveyId);
        assert expectedQuery.equals(" (  data->'$[0]' LIKE '%naveen%' OR  data->'$[0]' LIKE '%kumar%' OR  data->'$[0]' LIKE '%keywords%' OR  data->'$[0]' LIKE '%check%' OR  data->'$[0]' LIKE '%trigger%'  ) ");
    }

    @Test
    public void testMultiOpenEndedQuestionKeywordsQuestionSubQuery() {
        String query = "question.edf01cb2-9349-47a2-8d18-f97393757d3d.c391b5be-d707-4a2c-a957-58c7ba98bab0.444ef4bb-5492-4429-8bd1-0c3758a64ab1+responses=naveen,moe1,moe2,open,check";
        String businessUUID = "12d6f623-2163-4117-9048-ad6dfeaaf120";
        int surveyId = 482;
        Mockito.when(triggerDAO.getQuestionDataBySurveyId(anyInt(), anyString(), anyString())).thenReturn(mockMultpleOpenEndedQuestionData());
        String expectedQuery = triggerService.getQuestionSubQuery(query, businessUUID, surveyId);
        assert expectedQuery.equals(" (  data->'$[2][0]' LIKE '%naveen%' OR  data->'$[2][0]' LIKE '%moe1%' OR  data->'$[2][0]' LIKE '%moe2%' OR  data->'$[2][0]' LIKE '%open%' OR  data->'$[2][0]' LIKE '%check%'  ) ");
    }

    /**
     * Test method to test getQuestionSubQuery for open ended question responses with keywords based trigger
     */
    private Map mockOpenEndedQuestionData(){
        Map openEndedQuestionData = new HashMap();
        openEndedQuestionData.put("questionId", 3702);
        openEndedQuestionData.put("otherOptionIds", "");
        openEndedQuestionData.put("subQuestionUniqueIds", new ArrayList<>());
        openEndedQuestionData.put("options", new ArrayList<>());
        openEndedQuestionData.put("index", 0);
        openEndedQuestionData.put("subQuestionIds", new ArrayList<>());
        openEndedQuestionData.put("type", "open");
        openEndedQuestionData.put("category", "");
        openEndedQuestionData.put("otherOptions", new ArrayList<>());
        openEndedQuestionData.put("optionIds", new ArrayList<>());
        return openEndedQuestionData;
    }

    /**
     * Test method to test getQuestionSubQuery for open ended question responses with keywords based trigger
     */
    private Map mockMultpleOpenEndedQuestionData(){
        Map multipleOpenEndedQuestionData = new HashMap();
        multipleOpenEndedQuestionData.put("questionId", 3704);
        multipleOpenEndedQuestionData.put("otherOptionIds", "");
        multipleOpenEndedQuestionData.put("subQuestionUniqueIds", Arrays.asList("444ef4bb-5492-4429-8bd1-0c3758a64ab1", "73f9769d-5ab4-4d4c-8def-c9d89daffc14", "bed5bc4e-899d-43a1-b817-48422c0d0c20"));
        multipleOpenEndedQuestionData.put("options", new ArrayList<>());
        multipleOpenEndedQuestionData.put("index", 2);
        multipleOpenEndedQuestionData.put("subQuestionIds", Arrays.asList(3705, 3706, 3707));
        multipleOpenEndedQuestionData.put("type", "multipleOpenEnded");
        multipleOpenEndedQuestionData.put("category", "");
        multipleOpenEndedQuestionData.put("otherOptions", new ArrayList<>());
        multipleOpenEndedQuestionData.put("optionIds", new ArrayList<>());

        return multipleOpenEndedQuestionData;
    }
}